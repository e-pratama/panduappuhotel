################################################################################
##              -= YOU MAY NOT REMOVE OR CHANGE THIS NOTICE =-                 #
## --------------------------------------------------------------------------- #
##  uHotelBooking  version 2.3.6                                               #
##  Developed by:  ApPHP <info@apphp.com>                                      #
##  License:       ApPHP uHotelBooking license v.1                             #
##  Site:          http://www.hotel-booking-script.com                         #
##  Copyright:     uHotelBooking (c) 2014 - 2015. All rights reserved.         #
##                                                                             #
##  Additional modules (embedded):                                             #
##  -- ApPHP EasyInstaller v2.0.5 (installation module)       http://apphp.com #
##  -- ApPHP Tabs v2.0.3 (tabs menu control)                  http://apphp.com #
##  -- TinyMCE (WYSIWYG editor)                   http://tinymce.moxiecode.com #
##  -- Crystal Project Icons (icons set)               http://www.everaldo.com #
##  -- Securimage v2.0 BETA (captcha script)         http://www.phpcaptcha.org #
##  -- jQuery 1.4.2 (New Wave Javascript)                    http://jquery.com #
##  -- Google AJAX Libraries API                  http://code.google.com/apis/ #
##  -- Lytebox v3.22                                       http://lytebox.com/ #
##  -- JsCalendar v1.0 (DHTML/JavaScript Calendar)      http://www.dynarch.com #
##  -- RokBox System 	                           http://www.rockettheme.com/ #
##  -- VideoBox	                           http://videobox-lb.sourceforge.net/ #
##  -- CrossSlide jQuery plugin v0.6.2 	                     by Tobia Conforto #
##  -- PHPMailer v5.2 https://code.google.com/a/apache-extras.org/p/phpmailer/ #
##  -- tFPDF v1.24 (PDF files generator (FPDF http://fpdf.org))    by Ian Back #
##  -- Ajax-PHP Rating Stars Script                     http://coursesweb.net/ #
##  -- TitanicThemes (template)      http://themeforest.net/user/TitanicThemes #
##                                                                             #
################################################################################


Thank you for using ApPHP.com software!
-----------------------------------------------------------------------------------

It's very easy to get started with uHotelBooking!!!

1. Installation:
   http://www.hotel-booking-script.com/index.php?page=installation 
   or docs/Installation.html

2. Getting Started:
   http://www.hotel-booking-script.com/index.php?page=getting_started
   or docs/GettingStarted.html

-----------------------------------------------------------------------------------
For more information visit: 
	site 	http://www.hotel-booking-script.com/index.php?page=examples
	forum 	http://www.apphp.net/forum/


