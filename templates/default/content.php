<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------

	// Prepare Main Content
	ob_start();
	if(Application::Get('page') != '' && Application::Get('page') != 'home'){							
		if(file_exists('page/'.Application::Get('page').'.php')){	 
			include_once('page/'.Application::Get('page').'.php');
		}else{
			include_once('page/404.php');
		}
	}else if(Application::Get('customer') != ''){					
		if(Modules::IsModuleInstalled('customers') && file_exists('customer/'.Application::Get('customer').'.php')){	
			include_once('customer/'.Application::Get('customer').'.php');
		}else{
			include_once('customer/404.php');
		}
	}else if((Application::Get('admin') != '') && file_exists('admin/'.Application::Get('admin').'.php')){
		include_once('admin/'.Application::Get('admin').'.php');
	}else{
		if(Application::Get('template') == 'admin'){
			include_once('admin/home.php');
		}else{
			include_once('page/pages.php');										
		}
	}
	$main_content = ob_get_contents();
	ob_end_clean();

?>
        
<!-- CONTENT -->
<div class="container">
    <div class="container pagecontainer offset-0">	

        <!-- FILTERS -->
        <div class="col-md-3 filters offset-0">

			<?php if(!empty($rooms_count['rooms'])){
				
				// Prepare property type
				$property_type_id = isset($_REQUEST['property_type_id']) ? (int)$_REQUEST['property_type_id'] : '';
				$property_types = Application::Get('property_types');
				$property_type = '';
				foreach($property_types as $key => $val){
					if(isset($val['id']) && $val['id'] == $property_type_id){
						$property_type = $val['property_code'];
						break;
					}
				}				

				if($property_type == 'hotels'){
					$found_property	= _FOUND_HOTELS;
				}else if($property_type == 'villas'){
					$found_property	= _FOUND_VILLAS;
				}else{
					$found_property	= _FOUND_PROPERTIES;
				}
				
				$currency_rate = Application::Get('currency_rate');
				$currency_format = get_currency_format();
			?>
				<div class="filtertip">
					<div class="padding20">
						<p class="size14"><?php echo $found_property; ?>: <b><?php echo $rooms_count['hotels'];?></b></p>
						<p class="size14"><?php echo _TOTAL_ROOMS; ?>: <b><?php echo $rooms_count['rooms'];?></b></p>
						<p class="size22 bold"><span class="size14 normal darkblue"><?php echo _STARTING;?></span> <span class="countprice"><?php echo Currencies::PriceFormat($rooms_count['min_price_per_hotel'] * $currency_rate, '', '', $currency_format);?></span> <span class="size14 normal darkblue">/<?php echo _DAY;?></span></p>
					</div>
					<div style="bottom: -9px;" class="tip-arrow"></div>
				</div>
			<?php } ?>

            <div class="hpadding20">
                <!-- LEFT COLUMN -->
                <?php
                    // Draw menu tree
                    Menu::DrawMenu('left');						
                ?>                            
                <!-- END OF LEFT COLUMN -->
            </div>
            <!-- END OF BOOK FILTERS -->	

            <div class="clearfix"></div>
            <br/><br/>
            
		</div>
		<!-- END OF FILTERS -->

        <!-- LIST CONTENT-->
        <div class="rightcontent col-md-9 offset-0">
            <div class="hpadding20">
            <!-- MAIN CONTENT -->
            <?php
				// Print Main Content
				echo $main_content;
            ?>
            </div>
        </div>
        <!-- END OF LIST CONTENT-->    

    </div>
</div>
<!-- END OF CONTENT -->

