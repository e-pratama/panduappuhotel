<?php
define("_2CO_NOTICE","2Check Out.com Inc.(Ohio USA) عبارت است از اجرا كننده تاييد شدهبراى كالاها وخدمات ");
define("_2CO_ORDER","2درخواست بواسطه Check Out.com");
define("_ABBREVIATION","اختصارات");
define("_ABOUT_US","درباره ما");
define("_ACCESS","رسيدن");
define("_ACCESSIBLE_BY","رسيدن بواسطه");
define("_ACCOUNTS","حسابها");
define("_ACCOUNTS_MANAGEMENT","مديريت حسابها");
define("_ACCOUNT_ALREADY_RESET","حساب شما مجددا تنظیم شد! برای اطلاعات بیشتر لطفا صندوق ورودی ایمیل خود را بازبینی کنید.");
define("_ACCOUNT_BALANCE","مانده حساب");
define("_ACCOUNT_CREATED_CONF_BY_ADMIN_MSG","حساب کاربری شما با موفقیت ایجاد شد!
در مدت چند دقیقه شما باید یک ایمیل حاوی اطلاعات ثبت نام خود دریافت کنید.
پس از تصویب ثبت نام شما توسط مدیر، قادر خواهید بود برای ورود به حساب خود اقدام کنید.");
define("_ACCOUNT_CREATED_CONF_BY_EMAIL_MSG","حساب کاربری شما با موفقیت ایجاد شد!
در مدت چند دقیقه شما باید یک ایمیل حاوی اطلاعات ثبت نام خود دریافت کنید.
این ثبت نام را با استفاده از کد تاییدی که به آدرس ایمیل شما ارسال شده کامل کنید.پس از آن شما قادر خواهید بود به حساب خود وارد شوید.");
define("_ACCOUNT_CREATED_CONF_MSG","حساب شما با موفقیت ایجاد شد.شما هم اکنون یک ایمیل حاوی جزییات حساب خود دریافت میکنید(ممکن است چند دقیقه طول بکشد). پس از تایید توسط مدیر شما قادر خواهید بود به حساب خود وارد شوید.");
define("_ACCOUNT_CREATED_MSG","حساب شما با موفقیت ایجاد شد.شما هم اکنون یک ایمیل حاوی جزییات حساب خود دریافت میکنید(ممکن است چند دقیقه طول بکشد). پس از تایید توسط مدیر شما قادر خواهید بود به حساب خود وارد شوید.");
define("_ACCOUNT_CREATED_NON_CONFIRM_LINK","براى ادامه (اينجا) كليك كنيد");
define("_ACCOUNT_CREATED_NON_CONFIRM_MSG","حساب شما با موفقیت ایجاد شد!
برای راحتی شما،در عرض چند دقیقه ایمیلی حاوی جزییات ثبت نام خود دریافت میکنید.(بدون نیاز به تایید)
شما الان میتوانید وارد حساب خود شوید.");
define("_ACCOUNT_CREATE_MSG","اين روند ثبت نام نياز به تأييد از طريق ايميل دارد ! لطفاً فرم زير را با اطلاعات صحيح پر كنيد. ");
define("_ACCOUNT_DETAILS","جزئيات حساب ");
define("_ACCOUNT_SUCCESSFULLY_RESET","شما با موفقیت حساب و رمز عبور خود را بازنشانی کردید.
رمز عبور موقت به ایمیل شما ارسال شد.");
define("_ACCOUNT_TYPE","نوع حساب");
define("_ACCOUNT_WAS_CREATED","حساب شما ايجاد شده است");
define("_ACCOUNT_WAS_DELETED","حساب کاربری شما با موفقیت حذف شد! در چند ثانیه، شما به طور خودکار به صفحه اصلی هدایت می شوید.     ");
define("_ACCOUNT_WAS_UPDATED","حساب کاربری شما با موفقیت به روز شد !");
define("_ACCOUT_CREATED_CONF_LINK","ثبت نام شما از قبل تاييد شده است ؟ براى ادامه اينجا كليك كنيد ");
define("_ACCOUT_CREATED_CONF_MSG","ثبت نام شما از قبل تاييد شده است ؟ براى ادامه اينجا كليك كنيد ");
define("_ACTIONS","إقدام");
define("_ACTIONS_WORD","إقدام");
define("_ACTION_REQUIRED","إقدام لازم ");
define("_ACTIVATION_EMAIL_ALREADY_SENT","ایمیل فعالسازی به آدرس ایمیل شما ارسال میشود.
لطفا بعدا دوباره امتحان کنید.");
define("_ACTIVATION_EMAIL_WAS_SENT","يك ايميل كه حاوى كليد فعال سازى است به _EMAIL_ ارسال شد. لطفاً ايميل خود را براى تكميل ثبت نام چك كنيد. ");
define("_ACTIVE","فعال");
define("_ADD","اضافه كردن");
define("_ADDED_BY","اضافه كردن بواسطه");
define("_ADDING_OPERATION_COMPLETED","عمليات اضافه كردن با موفقيت به اتمام رسيد ");
define("_ADDITIONAL_INFO","اطلاعات اضافى ");
define("_ADDITIONAL_MODULES","ماژول هاى اضافى ");
define("_ADDITIONAL_PAYMENT","پرداخت هاى اضافى");
define("_ADDITIONAL_PAYMENT_TOOLTIP","براى اجراى پرداخت هاى اضافى يا تخفيف هاى مدير لطفاً مقدار مناسب را در اين گزينه  بگذاريد ( منفى يا مثبت )");
define("_ADDRESS","آدرس");
define("_ADDRESS_2","آدرس (خط دوم). ");
define("_ADDRESS_EMPTY_ALERT","ادرس نميتواند خالي باشد لطفاً دوباره ادرس را وارد كنيد");
define("_ADD_DEFAULT_PERIODS","اضافه كردن دوره پيش فرض");
define("_ADD_FUNDS","موجودى /اضافه كردن موجودى");
define("_ADD_NEW","اضافه كردن جديد");
define("_ADD_NEW_MENU","اضافه كردن منو جديد");
define("_ADD_TO_CART","اضافه كردن به سبد");
define("_ADD_TO_MENU","اضافه كردن به منو ");
define("_ADMIN","مدير");
define("_ADMINISTRATOR_ONLY","فقط مدير ");
define("_ADMINS","مديران");
define("_ADMINS_AND_CUSTOMERS","مشتريان و مديران");
define("_ADMINS_MANAGEMENT","مديريت مديران");
define("_ADMINS_OWNERS_MANAGEMENT","مديريت.  مديران وصاحبان هتلها");
define("_ADMIN_EMAIL","ايميل مدير ");
define("_ADMIN_EMAIL_ALERT","اين ادرس ايميل بعنوان &#034; از &#034; در مكاتبات اين ايميل استفاده مى شود. لطفاً يك ادرس ايميل ديگر كه با اسم سايت شما مرتبط است انتخاب كنيد");
define("_ADMIN_EMAIL_EXISTS_ALERT","يك مدير با اين ادرس ايميل وجود دارد. لطفاً يك ادرس ايميل ديگر انتخاب كنيد. ");
define("_ADMIN_EMAIL_IS_EMPTY","ايميل مدير در اين سايت نبايد خالى باشد ! لطفاً دوباره وارد كنيد. ");
define("_ADMIN_EMAIL_WRONG","ايميل مدير در سايت اشتباه است ! لطفاً دوباره ايميل را وارد كنيد. ");
define("_ADMIN_FOLDER_CREATION_ERROR","يك خطا در ايجاد پوشه براى ويرايشگر در فايل image/upload رخ داده است. براى عملكرد برنامه به طور صحيح لطفاً فايل را بصورت دستى ايجاد كنيد. ");
define("_ADMIN_LOGIN","ورود به سيستم مدير ");
define("_ADMIN_MAILER_ALERT","لطفاً سيستم ايميلى را كه استفاده از ان  در پيام هاى ارسال شده را ترجيح مى دهيد انتخاب كنيد");
define("_ADMIN_PANEL","پنل مدير ");
define("_ADMIN_RESERVATION","رزروهاى مدير كل سايت. ");
define("_ADMIN_WELCOME_TEXT","به صفحه كنترل مدير خوش امديد اين صفحه به شما اجازه اضافه كردن ويرايش يا حذف محتواى سايت را مى دهد. با اين صفحه كنترل مى توانيد به راحتى مديرت مشتريان و رزروها و كل مديريت سايت هتل را داشته باشيد 
برخى از واحدها براى شما وجود دارد : تهيه پشتيبانى وبازيابى ، اخبار ، نصب وراه اندازى ويا لغو نصب. admin modules واحدهاى فهرست مدير heref= index .php
زبان ها / زبان هاى فهرست index .php شما مى توانيد اضافه يا حذف تنظيمات زبان و تغيير زبان و تغيير تنظيمات واژگان خود را داشته باشيد ( كلمات وعبارات استفاده شده توسط سيستم ) href= index .php 
تنظيمات فهرست admin = settings به شما اجازه تعيين تنظيمات مهم سايت را مى دهد a href=index.php
حساب من admin = my account امكان تغيير اطلاعات مخصوص شما وجود دارد 
منوها index.php فايل a href.= index.php
مديريت صفحات / اين مديريت براى ايجاد ومديريت ليست ها لينك ها و صفحات طراحى شده است admin=pages براى ايجاد وويرايش انواع اتاق ها وفصل ها وقيمت ها و رزروها وساير اطلاعات هتل از مديريت هتلها استفاده كنيد href= index.php?admin= hotel info
");
define("_ADULT","بزرگسال");
define("_ADULTS","بزرگسالان");
define("_ADVANCED","پيشرفته");
define("_AFTER","بعد");
define("_AFTER_DISCOUNT","بعد از تخفيف");
define("_AGENCIES","نمايندگى ها");
define("_AGENCY","نمايندگى");
define("_AGENCY_DETAILS","جزئيات نمايندگى");
define("_AGENCY_LOGIN","ورود به نمايندگى");
define("_AGENCY_PANEL","صفحه كنترل اژانس ");
define("_AGENT_COMMISION","صاحب هتل / ");
define("_AGES","Ewigkeit");
define("_AGREE_CONF_TEXT","من شرایط و قوانین رو خوندم و با اون ها موافقم");
define("_ALBUM","البوم");
define("_ALBUM_CODE","كد البوم");
define("_ALBUM_NAME","نام البوم ");
define("_ALERT_CANCEL_BOOKING","ايا مطمئن هستيد كه ميخواهيد اين رزرو را كنسل كنيد ؟");
define("_ALERT_REQUIRED_FILEDS","موارد مشخص شده با ستاره (*) اجباری هستند");
define("_ALERT_SAME_HOTEL_ROOMS","شما ممكن است فقط اتاقهاى مربوط به اين هتل را به سبد رزرو اضافه كنيد !");
define("_ALL","همه");
define("_ALLOW","اجازه دادن");
define("_ALLOW_COMMENTS","اجازه دادن به نظرات");
define("_ALL_AVAILABLE","همه در دسترس هستند");
define("_ALL_NEWS","همه خبرها");
define("_ALL_RIGHTS_RESERVED","تمام حقوق محفوظ است");
define("_ALREADY_HAVE_ACCOUNT","ايا حساب در سايت داريد ! ثبت نام اينجا");
define("_ALREADY_LOGGED","شما از قبل وارد سيستم شديد ");
define("_AMOUNT","مقدار");
define("_ANSWER","جواب");
define("_ANY","هر");
define("_APPLY","درخواست");
define("_APPLY_TO_ALL_LANGUAGES","إعمال بر تمام زبانها");
define("_APPLY_TO_ALL_PAGES","اعمال تغييرات به تمام صفحهات");
define("_APPROVE","تأييد");
define("_APPROVED","تأييد شد");
define("_APRIL","ارديبهشت ");
define("_ARTICLE","مقاله");
define("_ARTICLE_ID","كد مقاله");
define("_AUGUST","شهريور ");
define("_AUTHENTICATION","تصديق");
define("_AUTHORIZE_NET_NOTICE","The Authorize.ارائه دهنده  دروازه پرداخت");
define("_AUTHORIZE_NET_ORDER","Authorize.Net درخواست ");
define("_AUTOMATIC","Automatisch");
define("_AVAILABILITY","دسترسى");
define("_AVAILABILITY_ROOMS_NOTE","تعریف حداکثر تعداد اتاق های موجود برای رزرو برای یک روز یا تاریخ محدوده مشخص شده (حداکثر اتاق در دسترس بودن _MAX_) <br> برای ویرایش در دسترس بودن اتاق به سادگی مقدار را تغییر دهید در یک سلول روز و سپس دکمه &#034;ذخیره تغییرات&#034; کلیک کنید");
define("_AVAILABLE","در دسترس ");
define("_AVAILABLE_ROOMS","اتاقهاى موجود");
define("_BACKUP","پشتيبان");
define("_BACKUPS_EXISTING","پشتيبان گيرى موجود");
define("_BACKUP_AND_RESTORE","پشتيبان گيرى وبازيابى اطلاعات");
define("_BACKUP_CHOOSE_MSG","از لیست زیر یک نسخه پشتیبان تهیه کنید");
define("_BACKUP_DELETE_ALERT","آیا شما مطمئن هستید که میخواهید این نسخه پشتیبان تهیه را حذف کنید؟");
define("_BACKUP_EMPTY_MSG","هيچ نسخه پشتيبان گيرى وجود ندارد. ");
define("_BACKUP_EMPTY_NAME_ALERT","نام فايل پشتيبان نميتواند خالى باشد ! لطفاً دوباره وارد كنيد. ");
define("_BACKUP_EXECUTING_ERROR","يك خطا هنگام پشتيبان گيرى از سيستم رخ داده است ! لطفاً مجوز نوشتن در پوشه پشتيبان گيرى را چك كنيد وبعداً دوباره امتحان كنيد ");
define("_BACKUP_INSTALLATION","نصب وراه اندادى پشتيبان گيرى ");
define("_BACKUP_RESTORE","بازگرداندن پشتيبانى");
define("_BACKUP_RESTORE_ALERT","ايا شما مطمئن هستيد كه ميخواهيد اين نسخه پشتيبان گيرى را بازگردانيد ");
define("_BACKUP_RESTORE_NOTE","اين اقدام تنظيمات فعلى خود را بازنويسى خواهد كرد. ");
define("_BACKUP_RESTORING_ERROR","يك خطا در حال بازگرداندن فايل رخ داده است ! لطفاً بعداً دوباره امتحان كنيد. ");
define("_BACKUP_WAS_CREATED","پشتيبان گيرى _FILE_NAME_ با موفقيت ايجاد شد ");
define("_BACKUP_WAS_DELETED","حذف كردن پشتيبان گيرى _FILE_NAME_ با موفقيت انجام شد. ");
define("_BACKUP_WAS_RESTORED","باز گرداندن پشتيبان گيرى _FILE_NAME_ با موفقيت انجام شد ");
define("_BACKUP_YOUR_INSTALLATION","پشتيبان گيرى فعلى نصب");
define("_BACK_TO_ADMIN_PANEL","برگشت به پنل مديريت ");
define("_BALANCE","موجودى حساب");
define("_BANK_PAYMENT_INFO","بانك اطلاعات پرداخت");
define("_BANK_TRANSFER","انتقال بانكى");
define("_BANNERS","اگهى ها");
define("_BANNERS_MANAGEMENT","مديريت اگهى ها");
define("_BANNERS_SETTINGS","تنظيمات اگهى ها");
define("_BANNER_IMAGE","عكس اگهى ");
define("_BAN_ITEM","مورد ممنوعيت ");
define("_BAN_LIST","ليست ممنوعيت");
define("_BATHROOMS","حمام ها ");
define("_BEDS","تخت");
define("_BEFORE","قبل");
define("_BILLING_ADDRESS","ادرس صورت حساب");
define("_BILLING_DETAILS","جزئيات صورت حساب");
define("_BILLING_DETAILS_UPDATED","جزئيات صورت حساب شما به روز شده است. ");
define("_BIRTH_DATE","تاريخ تولد");
define("_BIRTH_DATE_VALID_ALERT","تاريخ تولداشتباه وارد شده ! لطفاً دوباره وارد نمائيد. ");
define("_BOOK","رزرو كن");
define("_BOOKING","سبد رزروها");
define("_BOOKINGS","رزروها");
define("_BOOKINGS_MANAGEMENT","مديريت رزروها");
define("_BOOKINGS_SETTINGS","تنظيمات رزروها ");
define("_BOOKING_CANCELED","رزرو كنسل شد ");
define("_BOOKING_CANCELED_SUCCESS","رزرو _BOOKING _ با موفقيت از سيستم لغو شد !");
define("_BOOKING_COMPLETED","رزرو تكميل شد ");
define("_BOOKING_DATE","تاريخ رزرو");
define("_BOOKING_DESCRIPTION","شرح رزرو ");
define("_BOOKING_DETAILS","جزئيات رزرو");
define("_BOOKING_NUMBER","شماره رزرو");
define("_BOOKING_PRICE","قيمت رزرو");
define("_BOOKING_SETTINGS","تنظيمات رزرو");
define("_BOOKING_STATUS","وضعيت رزرو");
define("_BOOKING_SUBTOTAL","مجموع كلى رزروها");
define("_BOOKING_WAS_CANCELED_MSG","رزرو شما كنسل شد ");
define("_BOOKING_WAS_COMPLETED_MSG","باتشكراز رزرو اتاق در هتل ما ! رزرو شما تكيل شده است. ");
define("_BOOK_NOW","الان رزرو كن");
define("_BOOK_ONE_NIGHT_ALERT","متأسفم ٫ شما حداقل ميتوانيد براى يك شب رزرو كنيد. ");
define("_BOTTOM","پايين");
define("_BUTTON_BACK","برگشت ");
define("_BUTTON_CANCEL","كنسل");
define("_BUTTON_CHANGE","تغيير");
define("_BUTTON_CHANGE_PASSWORD","تغيير رمز عبور");
define("_BUTTON_CREATE","ساختن");
define("_BUTTON_LOGIN","ورود به سيستم");
define("_BUTTON_LOGOUT","خروج از سيستم");
define("_BUTTON_RESET","تنظيم مجدد");
define("_BUTTON_REWRITE","بازنويسى واژه ها ");
define("_BUTTON_SAVE_CHANGES","ذخيره تغييرات");
define("_BUTTON_UPDATE","به روز رسانى");
define("_CACHE_LIFETIME","طول عمر نسخه ذخيره موقت ");
define("_CACHING","ذخيره موقت");
define("_CAMPAIGNS","پيشنهادها");
define("_CAMPAIGNS_MANAGEMENT","مديريت پيشنهادها");
define("_CAMPAIGNS_TOOLTIP","كلى / اجازه به رزرو در هر تاريخى را مى دهد ودر محدود زمانى فعال مى ماند 
هدف قرار گرفته شده / اجازه به رزرو در مدت زمان محدود را مى دهد وبصورت ( مرئى )تا تاريخ اول كه ابتدا است عمل مى كند");
define("_CANCELED","كنسل شد ");
define("_CANCELED_BY_ADMIN","اين رزرو توسط مدير نظاره گر بر سايت لغو شد ");
define("_CANCELED_BY_CUSTOMER","اين رزرو توسط مشترى لغو شده است ");
define("_CANNOT_REMOVE_FUNDS_ALERT","شما نميتوانيد اين موجودى را حذف كنيد. چون ممكن است به موجودى منفى براى مشترى منجر شود. ");
define("_CAN_USE_TAGS_MSG","شما ميتوانيد برخى تگ هاى مانند HTML ، استفاده كنيد ");
define("_CAPACITY","ظرفيت");
define("_CART_WAS_UPDATED","سبد خريد رزرو باموفقيت به روز رسانى شد ! ");
define("_CAR_BOOKING_COMPLETED","Booking Car Completed");
define("_CAR_OWNER_NOT_ASSIGNED","Sie hat immer noch nicht jedes Auto Agentur zugewiesen worden, um die Berichte zu sehen");
define("_CAR_TYPE","Car Type");
define("_CAR_WAS_COMPLETED_MSG","Thank you for reservation car! Your booking has been completed.");
define("_CATEGORIES","رده ها");
define("_CATEGORIES_MANAGEMENT","مديريت رده ها");
define("_CATEGORY","رده");
define("_CATEGORY_DESCRIPTION","توضيحات رده");
define("_CC_CARD_HOLDER_NAME_EMPTY","نام دارنده كارت اعتبارى وجود ندارد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_CC_CARD_INVALID_FORMAT","شماره كارت اعتبارى از لحاظ شكلى اشتباه است. لطفاً دوباره اطلاعات را وارد نمائيد ");
define("_CC_CARD_INVALID_NUMBER","شماره كارت اعتبارى درست نيست ! لطفاً دوباره وارد نمائيد");
define("_CC_CARD_NO_CVV_NUMBER","  كد CVV ! لطفاً دوباره وارد كنيد.  وارد نشده است ");
define("_CC_CARD_WRONG_EXPIRE_DATE","تاريخ انقضاى صلاحيت كارت اعتبارى صحيح نيست ! لطفاً دوباره اطلاعات را وارد نمائيد. ");
define("_CC_CARD_WRONG_LENGTH","شماره كارت اعتبارى طولانى است ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_CC_NO_CARD_NUMBER_PROVIDED","هيچ شماره كارت اعتبارى ارائه نشده ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_CC_NUMBER_INVALID","شماره كارت اعتبارى نامعتبر است ! لطفاً دوباره وارد كنيد. ");
define("_CC_UNKNOWN_CARD_TYPE","نوع كارت ناشناخته است ! لطفاً دوباره  اطلاعات را وارد كنيد ");
define("_CHANGES_SAVED","تغييرات ذخيره شدند. ");
define("_CHANGES_WERE_SAVED","تغييرات با موفقيت ذخيره شد! براى ديدن نتايج لطفاً صفحه اصلى را نوسازى كنيد. ");
define("_CHANGE_CUSTOMER","تغيير مشترى ");
define("_CHANGE_ORDER","تغيير ترتيب ");
define("_CHANGE_YOUR_PASSWORD","تغيير رمز عبور شما ");
define("_CHARGE_TYPE","نوع شارژ ");
define("_CHECKOUT","وارسى ");
define("_CHECK_AVAILABILITY","بررسى دردسترس بودن ");
define("_CHECK_HOTELS","بررسى هتلها");
define("_CHECK_IN","رسيدن");
define("_CHECK_NOW","بررسى همين الان");
define("_CHECK_OUT","ترك كردن ");
define("_CHECK_STATUS","بررسى وضعيت ");
define("_CHECK_VILLAS","جستجو در ويلا");
define("_CHILD","بچه");
define("_CHILDREN","بچه ها");
define("_CITY","شهر");
define("_CITY_EMPTY_ALERT","گزينه شهر نميتواند خالى بماند ! لطفاً دوباره وارد كنيد");
define("_CLEANED","تميز");
define("_CLEANUP","پاكسازى ");
define("_CLEANUP_TOOLTIP","استفاده از ويژگى (پاكسازى ) براى حذف رزروهايى كه در حالت انتظار ( موقت ) در سايت اينترنتى خاص خود ");
define("_CLEAN_CACHE","حذف نسخه موقت");
define("_CLICK_FOR_MORE_INFO","براى اطلاع بيشتر كليك كنيد ");
define("_CLICK_TO_COPY","براى كپى كليك كنيد");
define("_CLICK_TO_EDIT","براى ويرايش كليك كنيد ");
define("_CLICK_TO_INCREASE","براى بزرگ كردن كليك كنيد");
define("_CLICK_TO_MANAGE","براى مديريت كليك كنيد");
define("_CLICK_TO_PAY","براى پرداخت كليك كنيد");
define("_CLICK_TO_SEE_DESCR","براى ديدن توضيحات كليك كنيد ");
define("_CLICK_TO_SEE_PRICES","براى ديدن قيمت ها كليك كنيد ");
define("_CLICK_TO_VIEW","براى مشاهده كليك كنيد ");
define("_CLOSE","بستن");
define("_CLOSE_META_TAGS","Close META tags");
define("_CODE","كد");
define("_COLLAPSE_PANEL","بستن فهرست مرور ");
define("_COMMENTS","نظرات");
define("_COMMENTS_AWAITING_MODERATION_ALERT","نظرات در انتظار تایید توسط سرپرست");
define("_COMMENTS_LINK","نظرات (_COUNT_)");
define("_COMMENTS_MANAGEMENT","مديريت نظرات");
define("_COMMENTS_SETTINGS","تنظيمات نظرات");
define("_COMMENT_DELETED_SUCCESS","نظرات شما با موفقيت حذف شد ");
define("_COMMENT_LENGTH_ALERT","طول نظر بايد كمتر از حرف _LENGTH_ باشد");
define("_COMMENT_POSTED_SUCCESS","نظر شما با موفقيت ارسال شد ! ");
define("_COMMENT_SUBMITTED_SUCCESS","نظر شما با موفقيت ارسال شد وپس از بررسى مدير در سايت پست خواهد شد. ");
define("_COMMENT_TEXT","متن نظر ");
define("_COMPANY","شركت ");
define("_COMPLETED","(پرداخت ) تكميل شد");
define("_CONFIRMATION","تاكيد");
define("_CONFIRMATION_CODE","كد تاييد");
define("_CONFIRMED_ALREADY_MSG","حساب شما قبلاً تاييد شده ! براى ادامه اينجا كليك كنيد");
define("_CONFIRMED_SUCCESS_MSG","تشكر ازشما بابت تاييد ثبت نام خود ! شما ميتوانيد در حال حاضر به حساب كاربرى خود وارد شويد. براى ادامه اينجا كليك كنيد. ");
define("_CONFIRM_PASSWORD","تاييد رمز عبور ");
define("_CONFIRM_TERMS_CONDITIONS","شما بايد تاييد كنيد كه با شرايط و قوانين موافق هستيد. ");
define("_CONF_PASSWORD_IS_EMPTY","تاييد رمز عبور نمى تواند خالى باشد ! ");
define("_CONF_PASSWORD_MATCH","رمز عبور بايد مطابق با تاييد رمز عبور باشد ");
define("_CONTACTUS_DEFAULT_EMAIL_ALERT","شما اقدام به تغيير ادرس ايميل پيش فرض در سرويس تماس  با ما كرده ايد. براى ادامه اينجا كليك كنيد. ");
define("_CONTACT_INFORMATION","اطلاعات تماس");
define("_CONTACT_US","تماس باما");
define("_CONTACT_US_ALREADY_SENT","پيام شما از قبل ارسال شده ! لطفاً دوباره امتحان كنيد يا _WAIT_ ثانيه صبر كنيد. ");
define("_CONTACT_US_EMAIL_SENT","از تماس با ما متشكريم ! پيام شما با موفقيت ارسال شد. ");
define("_CONTACT_US_SETTINGS","تنظيمات تماس با ما ");
define("_CONTENT_TYPE","نوع محتوا ");
define("_CONTINUE_RESERVATION","ادامه دادن رزرو");
define("_COPYRIGHT","حقوق ملكيت ");
define("_COPY_TO_OTHERS","كپى به ديگران ");
define("_COPY_TO_OTHER_LANGS","كپى به زبانهاى ديگر ");
define("_COUNT","شماره");
define("_COUNTRIES","كشورها");
define("_COUNTRIES_MANAGEMENT","مديريت كشورها ");
define("_COUNTRY","كشور");
define("_COUNTRY_EMPTY_ALERT","گزينه كشور نمى تواند خالى بماند ! لطفاً دوباره وارد كنيد ");
define("_COUPONS","كوپن ها");
define("_COUPONS_MANAGEMENT","مديريت كوپن ها");
define("_COUPON_CODE","كد كوپن ");
define("_COUPON_FOR_SINGLE_HOTEL_ALERT","اين كوپن تخفيف مى توان فقط براى يك هتل استفاده شود ! ");
define("_COUPON_WAS_APPLIED","كوپن _COUPON_CODE_ با موفقيت اجرا شد. ");
define("_COUPON_WAS_REMOVED","كوپن با موفقيت حذف شد !");
define("_CREATED_DATE","تاريخ ساختن");
define("_CREATE_ACCOUNT","ساختن حساب");
define("_CREATE_ACCOUNT_NOTE","نكته : توصيه ميشود كه رمز عبور حداقل بايد ٦ حرف باشد ، وبايد با نام كاربرى اختلاف داشته باشد. ادرس ايميل شما بايد معتبر باشد. ما ايميل را براى مقاصد ارتباطى ( اطلاعيه هاى سيستم وغيره ) استفاده ميكنيم.بنابر اين  بايد ادرس پست الكترونيكى معتبر ارائه شود تا بتواند از سرويس هاى ما بطور صحيح استفاده كند. تمام  اطلاعات مخصوص شما محرمانه است. ما به هيچ وجه  فروش ، مبادله ، يا بازار را انجام نميدهيم. براى اطلاعات بيشتر درباره مسئوليات هر دو بخش  مى توانيد به ما مراجعه كنيد. ");
define("_CREATING_ACCOUNT_ERROR","يك خطا درحال ايجاد حساب رخ داده! لطفاً دوباره امتحان كنيد ويا اطلاعات درباره اين خطا را به مديريت سايت ارسال كنيد. ");
define("_CREATING_NEW_ACCOUNT","ايجاد يك حساب جديد ");
define("_CREDIT_CARD","كارت اعتبارى ");
define("_CREDIT_CARD_EXPIRES","منقضى");
define("_CREDIT_CARD_HOLDER_NAME","نام صاحب كارت ");
define("_CREDIT_CARD_NUMBER","شماره كارت اعتبارى ");
define("_CREDIT_CARD_TYPE","نوع كارت اعتبارى ً");
define("_CRONJOB_HTACCESS_BLOCK","براى جلوگير از دسترسى مستقيم به فايل cron.php در فايل htaccess ويا ");
define("_CRONJOB_NOTICE","cornjob به شما اجازه مى دهد تا برخى از دستورات ووظايف را بطور خودكار در سايت خود انجام دهيد. وبرنامه سايت نياز به اجرا در فايل corn.php بصورت دوره اى است براى بستن كمپين تخفيف منقضى شده ويا انجام عمليات مهم ديگر. روش توصيه شده براى راه اندازى corn.php راه اندازى cornjob است اگر از سيستم راه اندازى  لينكس يا يونكس استفاده مى كنيد. واگر به هر دليلى نتوانستيد cornjob را بر روى سرور خود اجرا كنيد با انتخاب كردن non - batch از زير مى توانيد  فايل corn.php را بصورت مستقيم از سايت راه اندازى كنيد. وبا اين روش با بازديد هر شخص براى اولين بار از سايت فايل corn.php يك بار عمل خواهد كرد ");
define("_CRON_JOBS","Cron Jobs وظايف خودكار موقت");
define("_CURRENCIES","ارز");
define("_CURRENCIES_DEFAULT_ALERT","ياداورى : بعد از تغيير ارز پيش فرض  ويرايش قيمت ارزبراى هر ارز بطور دستى ( نسبتاً به ارز پيش فرض جديد ) تعريف دوباره قيمت براى همه اتاقها در پول جديد");
define("_CURRENCIES_MANAGEMENT","مديريت ارزها");
define("_CURRENCY","ارز");
define("_CURRENT_NEXT_YEARS","براى سال جارى / سال آينده");
define("_CUSTOMER","مشترى");
define("_CUSTOMERS","مشتريان");
define("_CUSTOMERS_AWAITING_MODERATION_ALERT","COUNT_ مشترى / مشتريان وجود دارمد كه منتظر تصويب شما هستند. 
براى رسيدگى به انها اينجا كليك كنيد");
define("_CUSTOMERS_MANAGEMENT","مديريت مشتريان");
define("_CUSTOMERS_SETTINGS","تنظيمات مشتريان");
define("_CUSTOMER_DETAILS","جزئيات مشتريان");
define("_CUSTOMER_GROUP","گروه مشترى");
define("_CUSTOMER_GROUPS","گروه هاى مشتريان");
define("_CUSTOMER_LOGIN","ورود به سيستم مشترى");
define("_CUSTOMER_NAME","نام مشترى");
define("_CUSTOMER_PANEL","صفحه تحكم مشترى");
define("_CUSTOMER_PAYMENT_MODULES","مشترى & ماژول پرداخت");
define("_CUSTOMER_SUPPORT","پشتيبانى مشترى");
define("_CVV_CODE","CVV كد");
define("_DASHBOARD","صفحه كنترل");
define("_DATE","تاريخ");
define("_DATETIME_PRICE_FORMAT","تنظيمات تاريخ و وقت وقيمت");
define("_DATE_ADDED","تاريخ اضافه شدن");
define("_DATE_AND_TIME_SETTINGS","تنظيمات تاريخ & وقت");
define("_DATE_CREATED","تاريخ ايجاد شد");
define("_DATE_EMPTY_ALERT","گزينه تاريخ نمى تواند خالى بماند ! لطفاً دوباره وارد كنيد");
define("_DATE_FORMAT","فرمت تاريخ");
define("_DATE_MODIFIED","تاريخ تغيير داده شد");
define("_DATE_PAYMENT","تاريخ پرداخت");
define("_DATE_PUBLISHED","تاريخ انتشار");
define("_DATE_SUBSCRIBED","تاريخ مشترك");
define("_DAY","روز");
define("_DECEMBER","دى");
define("_DECIMALS","اعشار");
define("_DEFAULT","پيش فرض");
define("_DEFAULT_AVAILABILITY","دردسترس بودن فرضى ");
define("_DEFAULT_CURRENCY_DELETE_ALERT","شما نميتوانيد ارز پيش فرض را حذف كنيد ! ");
define("_DEFAULT_EMAIL_ALERT","شما بايد ادرس پست الكترونيكى پيش فرض مدير سايت را تغيير دهيد.  براى ادامه اينجا كليك كنيد. ");
define("_DEFAULT_HOTEL_DELETE_ALERT","شما نميتوانيد هتل پيش فرض را حذف كنيد. ");
define("_DEFAULT_OWN_EMAIL_ALERT","شما بايد ادرس پست الكترونيكى خود را تغيير دهيد. براى ادامه اينجا كليك كنيد");
define("_DEFAULT_PERIODS_ALERT","دوره هاى پيش فرض در صفحه قيمت اتاقها براى تعيين دوره هاى زمانى كه ممكن است در ان رزرو ها به اسانى با اعتماد بر قيمت هاى پيش فرض هر اتاق پر شود استفاده ميشود. (فقط با يك كليد ). ");
define("_DEFAULT_PERIODS_WERE_ADDED","دوره هاى پيش فرض با موفقيت اضافه شد.");
define("_DEFAULT_PRICE","قيمت پيش فرض ");
define("_DEFAULT_TEMPLATE","قالب پيش فرض");
define("_DEFINE","تعريف");
define("_DELETE_WARNING","ايا شما مطمئن هستيد كه ميخواهيد اين اطلاعات را حذف كنيد. ");
define("_DELETE_WARNING_COMMON","ايا شما مطمئن هستيد كه ميخواهيد اين اطلاعات را حذف كنيد. ");
define("_DELETE_WORD","حذف");
define("_DELETING_ACCOUNT_ERROR","يك خطا هنگام حذف حساب شما رخ داده ! لطفاً دوباره  بعداً امتحان كنيد. يا يك پيام درباره اين مشكل به مديريت سايت ارسال نمائيد   ");
define("_DELETING_OPERATION_COMPLETED","عمليات حذف با موفقيت انجام شد. ");
define("_DESCRIPTION","توصيف");
define("_DETAILS","جزئيات");
define("_DISABLED","غير فعال");
define("_DISCOUNT","تخفيف");
define("_DISCOUNT_BY_ADMIN","تخفيف بواسطه مدير سايت ");
define("_DISCOUNT_CAMPAIGN","كمپين تخفيف");
define("_DISCOUNT_CAMPAIGNS","كمپين تخفيفها");
define("_DISCOUNT_CAMPAIGN_TEXT","سوپر كمپين تخفيف ! الان از كاهش ويژه قيمت در هر رزرو اتاق در هتل ما بهره مند شويد. _FROM_ _TO_PERCENT_");
define("_DISCOUNT_STD_CAMPAIGN_TEXT","كمپين تخفيف بزرگ ! از تخفيف هاى قيمتهاى مخصوص هتل ما تا مدت مشخص شده استفاده كنيد. ");
define("_DISPLAY_ON","نمايش بر");
define("_DISTANCE_UNITS","Km");
define("_DOWN","پايين");
define("_DOWNLOAD","دانلود");
define("_DOWNLOAD_INVOICE","دانلود فاكتور");
define("_EARLY_BOOKING","رزرو زودهنگام ");
define("_ECHECK","E-Check");
define("_EDIT_MENUS","ويرايش منو");
define("_EDIT_MY_ACCOUNT","ويرايش حساب من");
define("_EDIT_PAGE","ويرايش صفحه");
define("_EDIT_WORD","ويرايش");
define("_EMAIL","ايميل");
define("_EMAILS_SENT_ERROR","يك خطا هنگام ارسال ايميل رخ داده ويا هيچ پيام ارسال شده اى وجود ندارد. لطفاً دوباره امتحان كنيد. ");
define("_EMAILS_SUCCESSFULLY_SENT","وضعيت: ايميل _SENT_ از _TOTAL_با موفقيت ارسال شد. ");
define("_EMAIL_ADDRESS","ادرس ايميل");
define("_EMAIL_BLOCKED","ايميل شما مسدود شده است. براى حل اين مشكل ، لطفاً با مديريت سايت تماس حاصل كنيد. ");
define("_EMAIL_EMPTY_ALERT","ادرس ايميل نمى تواند خالى بماند. لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_EMAIL_FROM","ادرس ايميل  (از )");
define("_EMAIL_IS_EMPTY","ادرس ايميل نمى تواند خالى بماند. لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_EMAIL_IS_WRONG","لطفا. ادرس يك ايميل معتبر را وارد كنيد. ");
define("_EMAIL_NOTIFICATIONS","ارسال اطلاعيه ها بواسطه ايميل");
define("_EMAIL_NOT_EXISTS","اين حساب ايميل در سيستم وجود ندارد. ");
define("_EMAIL_SEND_ERROR","خطايي هنگام ارسال ايميل رخ داده ! لطفاً تنظيمات ايميل خود و  ايميل دريافت كنندگان را بررسى كنيد. و بعداً دوباره امتحان كنيد. ");
define("_EMAIL_SETTINGS","تنظيمات ايميل");
define("_EMAIL_SUCCESSFULLY_SENT","ايميل با موفقيت ارسال شد ! ");
define("_EMAIL_TEMPLATES","قالب هاى ايميل");
define("_EMAIL_TEMPLATES_EDITOR","ويرايشگر قالب هاى ايميل");
define("_EMAIL_TO","ادرس ايميل(به)");
define("_EMAIL_VALID_ALERT","لطفاً ادرس يك ايميل معتبر را وارد كنيد ! ");
define("_EMPTY","خالى");
define("_ENTER_BOOKING_NUMBER","شماره مخصوص رزرو خود را وارد كنيد ");
define("_ENTER_CONFIRMATION_CODE","كد تاييد را وارد كنيد. ");
define("_ENTER_EMAIL_ADDRESS","(لطفا فقط آدرس ایمیل واقعی وارد کنید)");
define("_ENTIRE_SITE","كل سايت");
define("_EVENTS","حوادث");
define("_EVENT_REGISTRATION_COMPLETED","از حسن توجه شما تشكر ميكنيم ! شما با موفقيت در اين رويداد ثبت نام شديد   ");
define("_EVENT_USER_ALREADY_REGISTERED","يك كاربر باچنين ايميلى قبلاً در اين رويداد ثبت نام كرده ! لطفاً ادرس ايميل ديگرى را وارد كنيد ");
define("_EXPAND_PANEL","گسترش فهرست مرور");
define("_EXPIRED","منقضى");
define("_EXPORT","صادر كردن");
define("_EXTRAS","افزوده ها");
define("_EXTRAS_MANAGEMENT","مديريت افزوده ها ");
define("_EXTRAS_SUBTOTAL","جمع افزودنى ها");
define("_EXTRA_BED","تخت اضافى ");
define("_EXTRA_BEDS","تخت هاى اضافى ");
define("_EXTRA_BED_CHARGE","هزينه اضافى تخت");
define("_EX_OTELS_VILLAS_OR_APARTMENTS","Bsp .: Hotels, Villen oder Wohnungen");
define("_FACILITIES","امكانات ");
define("_FACILITIES_MANAGEMENT","مديريت امكانات");
define("_FAQ","سؤال هاى تكرارى ");
define("_FAQ_MANAGEMENT","مديريت سؤال هاى تكرارى ");
define("_FAQ_SETTINGS","تنظيمات سؤال هاى تكرارى ");
define("_FAX","فكس");
define("_FEATURED_OFFERS","پيشنهادات ويژه ");
define("_FEATURED_OFFERS_TEXT","شروع جستجوى خود با يك نگاه به بهترين قيمتها در سايت ما. ");
define("_FEATURED_OFFERS_WITH_BR","پيشنهادهاى ويژه ");
define("_FEBRUARY","اسفند");
define("_FIELD_CANNOT_BE_EMPTY","گزينه _FAILED_نمى تواند خالى بماند ! لطفاً دوباره  اطلاعات را وارد كنيد. ");
define("_FIELD_LENGTH_ALERT","طول رشته_FIELD_  بايد كتر از _LENGTH_ حروف باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_LENGTH_EXCEEDED","_FIELD_از حد مجاز تجاوز كرده است LENGTH_حرف ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MIN_LENGTH_ALERT","طول رشته_FIELD_ نمى تواند كمتر از يك حرف باشد_LENGTH_ ! لطفا دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_ALPHA","رشته_FIELD_ بايد از حروف تشكيل شده باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_ALPHA_NUMERIC","رشته_FIELD_ بايد حروف واعداد باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_BOOLEAN","Field _FIELD_ value must be 'yes' or 'no'! Please re-enter.");
define("_FIELD_MUST_BE_EMAIL","رشته_FIELD_بايد شكل ايميل درست باشد ! لطفاًدوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_FLOAT","رشته_FIELD_ بايد عدد اعشارى بافاصله باشد ! لطفاً اطلاعات را دوباره وارد كنيد. ");
define("_FIELD_MUST_BE_FLOAT_POSITIVE","رشته_FIELD_ بايد يك عدد اعشارى با فاصله و مقدار باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_IP_ADDRESS"," رشته_FIELD_ ادرس  اينترنت بايد درست باشد ! لطفاً اطلاعات را دوباره وارد كنيد. ");
define("_FIELD_MUST_BE_NUMERIC","رشته _FIELD_ بايد يك مقدار عددى باشد ! لطفاً دوباره اطلاعات را وارد كنيد");
define("_FIELD_MUST_BE_NUMERIC_POSITIVE","رشته_FIELD_ بايد يك مقدار عددى مثبت باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_PASSWORD"," رشته_FIELD_بايد حد اقل ٦ كاراكتر وشامل حروف واعداد باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_POSITIVE_INT","رشته_FIELD_ بايد يك عدد با ارزش مثبت باشد. لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_POSITIVE_INTEGER","رشته_FIELD_ بايد يك عدد با ارزش مثبت باشد.");
define("_FIELD_MUST_BE_SIZE_VALUE","Field _FIELD_ must be a valid HTML size property in 'px', 'pt', 'em' or '%' units! Please re-enter.");
define("_FIELD_MUST_BE_TEXT","رشته _FIELD_بايد متن باشد. لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_UNSIGNED_FLOAT","رشته _FIELD_بايد عدد عشرى  بافاصله بدون علامت باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_MUST_BE_UNSIGNED_INT","رشته _FIELD_بايد يك عدد صحيح بدون علامت باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FIELD_VALUE_EXCEEDED","رشته _FIELD_ از مقدار مجاز _MAX_گذشت. لطفاً دوباره اطلاعات وارد كنيد يا تعداد كلى اتاقها را تغيير دهيد. اينجا كليك كنيد. ");
define("_FIELD_VALUE_MINIMUM","رشته _FIELD_ نبايد كمتر از _MIN_ باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FILED_UNIQUE_VALUE_ALERT","_FAILED_ فقط مقادير منحصر به فرد مى پذيرد. لطفاً دوباره اطلاعات را وارد كنيد");
define("_FILE_DELETING_ERROR","يك خطا هنگام حذف فايل رخ داده ! لطفاً دوباره بعداً امتحان كنيد. ");
define("_FILTER_BY","تصفيه بواسطه");
define("_FINISH_DATE","تاريخ انتهاء");
define("_FINISH_PUBLISHING","پايان انتشار");
define("_FIRST_NAME","اسم اول");
define("_FIRST_NAME_EMPTY_ALERT","گزينه اسم اول نمى تواند خالى بماند ! دوباره اطلاعات را وارد كنيد ");
define("_FIRST_NIGHT","شب اول");
define("_FIXED_SUM","مبلغ ثابت");
define("_FOLLOW_US","مارا دنبال كنيد ");
define("_FOOTER_IS_EMPTY","بالا وپايين صفحه نميتواند خالى باشد ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_FORCE_SSL","عبور از ssl");
define("_FORCE_SSL_ALERT","انحصار دسترسى به سايت از طريق پروتوكل به خاطر مناطق انتخاب شدهSSL(https) در سايت است. شما ويا بازديدكنندگان سايت قادر به دسترسى به مناطق انتخاب شده درسايت را نداريد. توجه داشته      ssl باشيد كه در سرور شما غير فعال باشد. تا اين گزينه غير قابل اجرا باشد. ");
define("_FORGOT_PASSWORD","ايا رمز عبور را فراموش كرده ايد");
define("_FORM","شكل");
define("_FOR_BOOKING","براى رزرو");
define("_FOUND_HOTELS","هتلهايى كه يافت شد");
define("_FOUND_ROOMS","اتاقهاى كه يافت شد ");
define("_FR","جمعه");
define("_FRI","جمعه");
define("_FRIDAY","جمعه");
define("_FROM","از");
define("_FROM_PER_NIGHT","از امشب");
define("_FROM_TO_DATE_ALERT","تاريخ  )به ( بايد مساوى ويا عقب تر از ( از ) باشد ! لطفا دوباره اطلاعات را وارد كنيد");
define("_FULLY_BOOKED","به طور كامل رزرو شده / در دسترس نيست");
define("_FULL_PRICE","قيمت كلى");
define("_FUNDS","موجودى");
define("_FUNDS_INFORMATION","اطلاعات موجودى");
define("_GALLERY","گالرى");
define("_GALLERY_MANAGEMENT","مديريت گالرى ");
define("_GALLERY_SETTINGS","تنظيمات گالرى ");
define("_GENERAL","عام");
define("_GENERAL_INFO","اطلاعات عمومى");
define("_GENERAL_SETTINGS","تنظيمات عمومى");
define("_GENERATE","توليد ");
define("_GLOBAL","");
define("_GROUP","گروه");
define("_GROUP_NAME","نام گروه");
define("_GROUP_TIME_OVERLAPPING_ALERT","اين دوره زمانى ( به طور كامل يا جزئى ) از قبل براى گروهى مشخص انتخاب شده است ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_GUEST_TAX","ماليات مهمان");
define("_HDR_FOOTER_TEXT","متن پايين سايت ");
define("_HDR_HEADER_TEXT","متن بالاى سايت ");
define("_HDR_SLOGAN_TEXT","شعار سايت");
define("_HDR_TEMPLATE","قالب");
define("_HDR_TEXT_DIRECTION","اتجاه متن");
define("_HEADER","سربرگ");
define("_HEADERS_AND_FOOTERS","سربرگ و پايين سايت ");
define("_HEADER_IS_EMPTY","سربرگ نمى تواند خالى بماند ! لطفاً اطلاعات مناسب را وارد كنيد ");
define("_HELP","كمك");
define("_HIDDEN","پنهان");
define("_HIDE","پنهان كردن");
define("_HOME","صفحه اصلى ");
define("_HOTEL","هتل");
define("_HOTELOWNER_WELCOME_TEXT","به صفحه كنترل صاحب هتل خوش امديد از طريق اين صفحه كنترل مى توانيد به سادگى هتلها و مشتريان ورزروهاى خود و مديريت كامل هتل خود را داشته باشيد ");
define("_HOTELS","هتلها");
define("_HOTELS_AND_ROMS","هتلها و اتاقها");
define("_HOTELS_INFO","اطلاعات هتلها");
define("_HOTELS_MANAGEMENT","مديريت هتلها");
define("_HOTEL_DELETE_ALERT","ايا مطمئن هستيد كه مى خواهيد اين هتل را حذف كنيد ! ياداورى : بعد از انجام عمليات حذف اين هتل تمام اطلاعات مربوط به اين هتل محو وبازگرداندن انها ممكن نمى باشد. ");
define("_HOTEL_DESCRIPTION","توصيف هتل");
define("_HOTEL_FACILITIES","امكانات هتل");
define("_HOTEL_INFO","اطلاعات هتل");
define("_HOTEL_MANAGEMENT","مديريت هتل");
define("_HOTEL_OWNER","مالك هتل");
define("_HOTEL_OWNERS","مالكان هتل");
define("_HOTEL_RESERVATION_ID","كد رزرو هتل");
define("_HOT_DEALS","تخفيف هاى ويژه");
define("_HOUR","ساعت");
define("_HOURS","ساعتها");
define("_ICON_IMAGE","تصوير ايكون");
define("_IMAGE","تصوير ");
define("_IMAGES","تصاوير ");
define("_IMAGE_VERIFICATION","تصوير تحقيق");
define("_IMAGE_VERIFY_EMPTY","شما بايد تصوير كد امنيتى را وارد كنيد ! ");
define("_INCOME","درامد");
define("_INFO","اطلاعات");
define("_INFORMATION","اطلاعات");
define("_INFO_AND_STATISTICS","اطلاعات و امار ");
define("_INITIAL_FEE","هزينه اوليه");
define("_INSTALL","نصب كردن");
define("_INSTALLED","نصب شد ");
define("_INSTALL_PHP_EXISTS","فايل  <b>install.php</b>  يا پوشه <b>install/</b> هنوز هم وجود دارد ة. به دلايل امنيتى لطفاً انها را بلا فاصله حذف كنيد! ");
define("_INTEGRATION","يكپارچه سازى جعبه جستجو ");
define("_INTEGRATION_MESSAGE","كد زير را كپى كنيد وان را در محلى مناسب از وب سايت خود قرار دهيد تا سرويس جستجو را دريافت كنيد ");
define("_INTERNAL_USE_TOOLTIP","صرفاً جهت استفاده داخلى ");
define("_INVALID_FILE_SIZE","حجم فايل معتبر نيست: _FILE_SIZE_ (max. allowed: _MAX_ALLOWED_)");
define("_INVALID_IMAGE_FILE_TYPE","فايل اپلود شده فايل تصوير معتبر نيست.!لطفاً با يك فايل تصوير ديگر امتحان كنيد. ");
define("_INVOICE","صورت حساب");
define("_INVOICE_SENT_SUCCESS","صورت حساب با موفقيت به مشترى ارسال شد ! ");
define("_IN_PRODUCTS","در محصولات");
define("_IP_ADDRESS","ادرس اينترنت IP");
define("_IP_ADDRESS_BLOCKED","نشانى IP شما مسدود شده است ! براى حل اين مشكل با مدير سايت تماس بگيريد. ");
define("_IS_DEFAULT","پيش فرض");
define("_ITEMS","اقلام");
define("_ITEMS_LC","اقلام");
define("_ITEM_NAME","نام فقره");
define("_JANUARY","بهمن");
define("_JULY","مرداد");
define("_JUNE","تير");
define("_KEY","كليد");
define("_KEYWORDS","صفحه كليد ");
define("_KEY_DISPLAY_TYPE","نوع نمايش كليد ");
define("_LANGUAGE","زبان");
define("_LANGUAGES","زبان ها");
define("_LANGUAGES_SETTINGS","تنظيمات زبان ها");
define("_LANGUAGE_ADDED","زبان جديد با موفقيت اضافه شد ! ");
define("_LANGUAGE_ADD_NEW","اضافه كردن زبان جديد");
define("_LANGUAGE_EDIT","ويرايش زبان");
define("_LANGUAGE_EDITED","داده هاى زبان با موفقيت به روز رسانى شد ! ");
define("_LANGUAGE_NAME","نام زبان");
define("_LANG_ABBREV_EMPTY","مخفف زبان نمى تواند خالى بماند !");
define("_LANG_DELETED","زبان با موفقيت حذف شد ! ");
define("_LANG_DELETE_LAST_ERROR","شما نمى توانيد زبان اخرين را حذف كنيد !");
define("_LANG_DELETE_WARNING","مطمئنيد كه مى خواهيد اين زبان را حذف كنيد ؟ اين عمليات باعث حذف همه واژه هاى زبان مى شود !");
define("_LANG_MISSED","به علت به روز رسانى زبان موجود نمى باشد ! لطفاً دوباره تلاش كنيد. ");
define("_LANG_NAME_EMPTY","نام زبان نمى تواند خالى بماند !");
define("_LANG_NAME_EXISTS","زبانى با اين نام وجود دارد ! لطفاً يكى ديگر را انتخاب كنيد. ");
define("_LANG_NOT_DELETED","زبان حذف نشده است ! ");
define("_LANG_ORDER_CHANGED","ترتيب زبان با موفقيت تغيير داده شد !");
define("_LAST_CURRENCY_ALERT","شما نمى توانيد اخرين ارز فعال را حذف كنيد !");
define("_LAST_HOTEL_ALERT","شما نمى توانيد اخرين هتل فعال را حذف كنيد ! 
");
define("_LAST_HOTEL_PROPERTY_ALERT","Diese Eigenschaft Typ kann nicht gelöscht werden, weil sie in einer Eigenschaft zumindest beteiligt.");
define("_LAST_LOGGED_IP"," اخرين ورود IP");
define("_LAST_LOGIN","اخرين ورود به سيستم");
define("_LAST_MINUTE","اخرين دقيقه");
define("_LAST_NAME","نام خانوادگى");
define("_LAST_NAME_EMPTY_ALERT","نام خانوادگى نمى تواند خالى بماند ! ");
define("_LAST_RUN","اخرين اجرا");
define("_LAYOUT","خروج از سيستم ");
define("_LEAVE_YOUR_COMMENT","نظر خود را بده ");
define("_LEFT","چپ");
define("_LEFT_TO_RIGHT","LTR (از چپ به راست )");
define("_LEGEND","اسطوره");
define("_LEGEND_CANCELED","سفارش توسط مدير لغو شده واتاق دوباره در ليست جستجو در دسترس است ");
define("_LEGEND_COMPLETED","پول به صورت (كلى ويا جزئى ) پرداخت شد و رزرو با موفقيت انجام شد. ");
define("_LEGEND_PAYMENT_ERROR","يك خطا هنگام پردازش پرداخت از مشترى رخ داده ");
define("_LEGEND_PENDING","رزرو انجام شد اما تا الان حفظ وتاييد نشده است ");
define("_LEGEND_PREBOOKING","اين وضعيت سيستم براى رزرو در حال پيشرفت ويا تكميل نشده است. ( اتاق به سبد رزرو اضافه شده ولى عمليات به صورت نهايى تمام نشده است )");
define("_LEGEND_REFUNDED","سفارش باز پرداخت شده است واتاق دوباره در گزينه جستجو قابل دسترسى است");
define("_LEGEND_RESERVED","اتاق رزرو شد ، اما قيمت پرداخته نشده است (در حال انتظار) ");
define("_LETS_SOCIALIZE","شبكه هاى اجتماعى");
define("_LICENSE","پروانه");
define("_LINK","لينك");
define("_LINK_PARAMETER","متغيرها در لينك");
define("_LOADING","در حال بار گيرى ");
define("_LOCAL_TIME","وقت محلى");
define("_LOCATION","موقعيت جغرافى");
define("_LOCATIONS","موقعيتهاى جغرافى");
define("_LOCATION_NAME","نام موقعيت جغرافيايى");
define("_LOGIN","ورود به سيستم");
define("_LOGINS","ورودها به سيستم");
define("_LOGIN_PAGE_MSG","استفاده از نام كاربرى و رمز عبور ناظر كل  براى دسترسى به صفحه كنترل ناظر كل. برگشتن به سايت. صفحه اصلى. ");
define("_LOGO","Logo");
define("_LONG_DESCRIPTION","شرح طولانى ");
define("_LOOK_IN","جستجو در ");
define("_LOW_BALANCE","كمبود موجودى");
define("_MAILER","سيستم پيام رسانى ");
define("_MAIN","اصلى ");
define("_MAIN_ADMIN","مدير كل");
define("_MAKE_RESERVATION","رزرو كنيد");
define("_MANAGE_TEMPLATES","مديريت قالبها");
define("_MANUAL","Handbuch");
define("_MAPS","نقشه ها ");
define("_MAP_CODE","كد نقشه");
define("_MAP_OVERLAY","توزيع بر روى نقشه");
define("_MARCH","اسفند");
define("_MASS_MAIL","ايميل دسته جمعى");
define("_MASS_MAIL_ALERT","نكته : خدمات ميزبانى سايتها ى مشترك معمولا فقط مى توانند تا٢٠٠ پيامالكترونيكى در ساعت ارسال كنند ");
define("_MASS_MAIL_AND_TEMPLATES","پست گروهى وقالب ها");
define("_MAXIMUM_NIGHTS","حداكثر شب ها");
define("_MAXIMUM_NIGHTS_ALERT","_IN_HOTEL_  بيشترين مدت مجاز براى اقامت در هتل در اين دوره زمانى از يك شب_FROM_. الى_TO_ _NIGHTS_ در هر رزرو است   لطفاً دوباره اطلاعات را وارد كنيد ");
define("_MAX_ADULTS"," حداكثر تعداد بزرگسالان");
define("_MAX_ADULTS_ACCOMMODATE","حد اكثر تعداد بزرگسالان كه  در اين اتاق جا مى گيرند");
define("_MAX_CHARS","(اخرين: _MAX_CHARS_ حرف)");
define("_MAX_CHILDREN","حداكثر تعداد كودكان");
define("_MAX_CHILDREN_ACCOMMODATE"," حداكثر تعداد كودكان كه در اين اتاق جا مى گيرند ");
define("_MAX_EXTRA_BEDS","حد اكثر تعداد تخت هاى اضافى ");
define("_MAX_OCCUPANCY","حد اكثر تصرف");
define("_MAX_RESERVATIONS_ERROR","شما به حداكثر تعداد اتاقهايى كه اجازه به رزرو انها هست رسيديد اگر هنوز تمام نكرده ايد ! لطفاً براى ادامه رزرو اتاقهاى جديد حد اقل يك اتاق را رزرو كنيد ");
define("_MAY","خرداد");
define("_MD_BACKUP_AND_RESTORE","بواسطه Madule  نسخه  ماژول ،پشتيبانى وبازيابى اطلاعات ،شما مى توانيد يك نسخه پشتيبان از همه جداول پايگاه دادها به فايل بر سرور ميزبانى وب سايت بگيريد. و همچنين بازگرداندن پايگاه داده ها از فايل بر روى سرور كه از قبل اپلود شده است واز يكى ازفايل هاى نسخه پشتيبانى ذخيره شده بر سرور ");
define("_MD_BANNERS","ماژول اگهى ها به مدير اجازه نمايش تصاوير درسايت به سبك هاى تصادفى وچرخشى را مى دهد ");
define("_MD_BOOKINGS","ماژول رزرو ها به مالك سايت اجازه تعريف رزرو براى تمام اتاقها را مى دهد ، وپس از ان براى هر اتاق به صورت جداگانه قيمت بر اساس تاريخ واقامت مى گذارد. وهمچنين اجازه گرفتن رزروها از مشتريان ومديريت انها از طريق پنل كنترل مدير كل را مى دهد ");
define("_MD_COMMENTS","ماژول نظرات به بازديدكنندگان اجازه نظردا ن در مقالات را مى دهد وبه مدير كل سايت اجازه نظارت و كنترل نظرات را مى دهد");
define("_MD_CONTACT_US","ماژول تماس باما به اسانى اجازه ايجاد ونشر يك نمونه براى تماس بر سايت الكترونى با استفاده از كد ازپيش تعريف شده را مى دهد  مثل {module:contact_us}.");
define("_MD_CUSTOMERS","ماژول مشتريان به مدير اجازه كنترل مشتريان بر سايت را مى دهد مدير كل مى تواند ايجاد ويرايش و حذف حساب هاى مشتريان را انجام دهد. ومشتريان مى توانند براى رسيدن به حسابهاى خود در سايت ثبت نام كنند و وارد حساب شوند ");
define("_MD_FAQ","ماژول پرسشهاى متداول به مدير سايت اجازه مى دهد تا سؤالات وجواب هايشان را براى بازديدكنندگان در صفحه سؤالهاى متداول بگذارد ");
define("_MD_GALLERY","ماژول گالرى به مدير اجازه ايجاد طبقه بندى براى عكسهاو ويدئو ها ، اپلود محتواى طبقه بندى ها ونمايش اين محتويات ( ويدئو، عكس ) از طرف بازديد كنندگان سايت");
define("_MD_NEWS","ماژول اخبار و رويدادها به مدير سايت اجازه نشر اخبار و رويدادها بر سايت را مى دهد ونمايش  تازه ترين اخبار در بلوك جانبى سايت");
define("_MD_PAGES","ماژول صفحات به مدير اجازه ايجاد وذخيره محتويات صفحه را مى دهد");
define("_MD_RATINGS","ماژول  ارزيابى به بازديدكنندگان سايت اجازه ارزيابى هتلها را مى دهد. وتعداد ارا وميانگين ارزيابى ها در فايل هتل نمايش داده مى شود. ");
define("_MD_ROOMS","ماژول اتاقها به صاحب سايت اجازه مديريت اتاقهاى هتل ازطريق ايجاد ويرايش و حذف اتاقها و تعيين امكانات هر اتاق و قيمت ان اتاق وقابل دسترس بودن اتاق در مدت زمانى مشخص مى دهد. وغيره ");
define("_MD_REVIEWS","ماژول توصيفات به مدير سايت اجازه اضافه كردن و ويرايش توصيفات مشتريان را مى دهد و مديريت توصيفات مشتريان و نمايش انها در سمت بازديد كنندگان از وب سايت ");
define("_MEAL_PLANS","وعده هاى غذايى ");
define("_MEAL_PLANS_MANAGEMENT","مديريت وعده هاى غذايى ");
define("_MENUS","منو ها");
define("_MENUS_AND_PAGES","منوها وصفحات");
define("_MENUS_DISABLED_ALERT","لطفاً در نظر داشته باشيد كه بعضى از منوها براى اين قالب غير فعال مى شوند. ");
define("_MENU_ADD","اضافه كردن منو");
define("_MENU_CREATED","منو با موفقيت ايجاد شد ");
define("_MENU_DELETED","منو باموفقيت حذف شد ");
define("_MENU_DELETE_WARNING","ايا مطمئن هستيد كه مى خواهيد اين منو را حذف كنيد ؟ توجه : اين كار تمام محتويات اين فهرست را براى بازديدكنندگان از سايت نامرئى مى كند ");
define("_MENU_EDIT","ويرايش منو");
define("_MENU_LINK","لينك منو ");
define("_MENU_LINK_TEXT","لينك منو(حداكثر ٤٠ حرف)");
define("_MENU_MANAGEMENT","مديريت منوها ");
define("_MENU_MISSED","بعضى از منوها ابديت نشده اند ! لطفاً دوباره دوباره امتحان كنيد ");
define("_MENU_NAME","نام منو");
define("_MENU_NAME_EMPTY","نام منو نمى تواند خالى بماند! ");
define("_MENU_NOT_CREATED","نام منو ايجاد نشد ! ");
define("_MENU_NOT_DELETED","منو حذف نشد ");
define("_MENU_NOT_FOUND","هيچ منو يى پيدا نشد ");
define("_MENU_NOT_SAVED","منو ذخيره نشد ! ");
define("_MENU_ORDER","ترتيب منو");
define("_MENU_ORDER_CHANGED","تغيير ترتيب منو با موفقيت انجام شد !");
define("_MENU_SAVED","منو با موفقيت ذخيره شد ! ");
define("_MENU_TITLE","عنوان منو");
define("_MENU_WORD","منو");
define("_MESSAGE","پيام");
define("_MESSAGE_EMPTY_ALERT","پيام نمى تواند خالى بماند ! لطفاً دوباره وارد كنيد ");
define("_META_TAG","Meta Tag");
define("_META_TAGS","META Tags");
define("_METHOD","روش");
define("_MIN","حداقل");
define("_MINIMUM_NIGHTS","حداقل شبها ");
define("_MINIMUM_NIGHTS_ALERT","_IN_HOTEL_ كمترين حد مجاز براى اقامت در هتل در دوره زمانى محدود يك شب در هر رزرو _FROM _ الى _TO_ لطفاً دوباره اطلاعات را وارد كنيد ");
define("_MINUTES","دقايق");
define("_MO","Mo");
define("_MODULES","Modulesواحدهاى ");
define("_MODULES_MANAGEMENT","مديريت واحدهاى modules");
define("_MODULES_NOT_FOUND","هيچ واحدى ايجاد نشد ");
define("_MODULE_INSTALLED","Modules واحد باموفقيت نصب شد ");
define("_MODULE_INSTALL_ALERT","ايا مطمئنيد كه مى خواهيد اين واحد moduleرا نصب كنيد ");
define("_MODULE_UNINSTALLED","نصب واحد module با موفقيت كنسل شد ");
define("_MODULE_UNINSTALL_ALERT","ايا مطمئن هستيد كه مى خواهيد نصب اين واحد module را لغو كنيد ؟ اطلاعات مربوط به اين واحد به طور كلى از سايت حذف خواهد گرديد");
define("_MON","دوشنبه");
define("_MONDAY","دوشنبه");
define("_MONTH","ماه");
define("_MONTHS","ماه ها");
define("_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","Zimmer Belegung Monate");
define("_MS_ACTIVATE_BOOKINGS","مشخص ميكند واحد رزرو ( booking module) قابل دسترس بر كل سايت ، سمت بازديدكنندگان  / سمت سايت فقط است يا نه");
define("_MS_ADD_WATERMARK","انتخاب كنيد كه ايا مى خواهيد علامت به عكسهاى اتاق اضافه كنيد يا نه");
define("_MS_ADMIN_BOOKING_IN_PAST","مشخص مى كند كه رزرو از طرف مديران ومالكان هتل ها در تواريخ گذشته ( از اول ماه جارى ( مجاز مى باشد ");
define("_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","مشخص مى كند كه تغيير رمز عبور كاربران توسط مدير كل سايت مجاز مى باشد ");
define("_MS_ADMIN_CHANGE_USER_PASSWORD","مشخص مى كند كه تغيير رمز عبور كاربران توسط مدير كل سايت مجاز مى باشد ");
define("_MS_ALBUMS_PER_LINE","تعداد ايكون البوم عكس ها در هر خط");
define("_MS_ALBUM_ICON_HEIGHT","ارتفاع ايكون رتبه  بندى در البوم عكس ها ");
define("_MS_ALBUM_ICON_WIDTH","عرض ايكون رتبه بندى در البوم عكس ها ");
define("_MS_ALBUM_ITEMS_NUMERATION","مشخص مى كند  كه شماره بندى عناصر موجود در صفحه رتبه بندى البوم عكس ها ظاهر خواهد شد ");
define("_MS_ALBUM_KEY","كلمه اى كه با عكس هاى رتبه بندى تعيين شده از البوم عكس ها جايگزين خواهد شد ) كپى ان در صفحه ) ");
define("_MS_ALERT_ADMIN_NEW_REGISTRATION","مشخص مى كند كه اطلاع رسانى به مدير سايت هنگامى كه مشترى يك حساب جديد ايجاد مى كند ");
define("_MS_ALLOW_ADDING_BY_ADMIN","مشخص مى كند كه مدير سايت اجازه درست كردن حساب هاى جديد براى مشتريان را دارد يا نه");
define("_MS_ALLOW_AGENCIES","Allow Agencies ");
define("_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","مشخص مى كند كه اجازه به رزرو براى مشتريان بدون ايجاد حساب داده مى شود ");
define("_MS_ALLOW_CHILDREN_IN_ROOM","مشخص مى كند كه اجازه به بودن بچه ها در اتاق داده مى شود يا نه");
define("_MS_ALLOW_CUSTOMERS_LOGIN","مشخص مى كند كه اجازه به مشتريان جديد براى ورود به سيستم داده مى شود يا نه ");
define("_MS_ALLOW_CUSTOMERS_REGISTRATION"," مشخص مى كند كه اجازه دادن به ثبت نام مشتريان جديد");
define("_MS_ALLOW_CUST_RESET_PASSWORDS","مشخص مى كند كه اجازه بازيابى رمزهاى عبور مخصوص مشتريان را مى دهد ");
define("_MS_ALLOW_DEFAULT_PERIODS","مشخص مى كند كه اجازه به اضافه ومديريت دوره هاى پيش فرض  براى اتاقها ");
define("_MS_ALLOW_EXTRA_BEDS","مشخص مى كند كه ايا اجازه به اضافه كردن تخت هاى اضافى در اتاق هست يا نه");
define("_MS_ALLOW_PAYMENT_WITH_BALANCE","مشخص مى كند كه اجازه به نمايندگى براى پرداخت از موجودى براى رزرو  مى دهد ");
define("_MS_ALLOW_SYSTEM_SUGGESTION","مشخص مى كند كه به بازديد كنندگان خاصيت &#034; پيشنهاد از سايت &#034; نمايش مى دهد هنگامى كه در جستجو از اتاقها به نتيجه اى نمى رسند ");
define("_MS_AUTHORIZE_LOGIN_ID","مشخص مى كند  شماره ورود به سايت راAuthorize.Net API");
define("_MS_AUTHORIZE_TRANSACTION_KEY","تعين كليد عمليات سايت Authorize.Net");
define("_MS_AVAILABLE_UNTIL_APPROVAL","تعيين اجازه به نمايش اتاق هاى رزرو شده در نتايج جستجو  تا عمليات رزرو بطور كلى اتمام شود ");
define("_MS_BANK_TRANSFER_INFO","تعيين معلومات بانكى لازم :اسم بانك وشعبه و شماره حساب  و غيره ");
define("_MS_BANNERS_CAPTION_HTML","مشخص مى كند كه استعمال كدهاى HTML بر روى تصوير خودكار مجاز است يا نه");
define("_MS_BANNERS_IS_ACTIVE","تعيين واحد تبليغات فعال باشد يا نه ");
define("_MS_BOOKING_MODE","تعيين  هر وضعيت قابل دسترس در عمليات رزرو ");
define("_MS_BOOKING_NUMBER_TYPE","تعيين نوع شماره هاى رزرو ");
define("_MS_CHECK_PARTIALLY_OVERLAPPING","تعيين اجازه به انتخاب تاريخ هايى كه تداخل جزئى در سبد ها رادارند ");
define("_MS_COMMENTS_ALLOW","تعيين اجازه نظر دادن بر روى مقالات ");
define("_MS_COMMENTS_LENGTH","حداكثر طول يك نظر ");
define("_MS_COMMENTS_PAGE_SIZE","تعيين تعداد نظراتى كه در يك صفحه نمايش داده مى شود ");
define("_MS_CONTACT_US_KEY","كلمه اى كه با نمونه باماتماس بگيريد جايگزين خواهد شد ( كپى در صفحه ) ");
define("_MS_CUSTOMERS_CANCEL_RESERVATION","براى مشتريان امكان لغو رزروهايشان قبل از تاريخ رسيدن به هتل در اين تعداد از روزها مى باشد ");
define("_MS_CUSTOMERS_IMAGE_VERIFICATION","تعيين مى كند كه استفاده از كد عكسها براى تاييد در صفحه ثبت نام مشتريان ممكن است");
define("_MS_DEFAULT_PAYMENT_SYSTEM","تعيين سيستم پيش فرض براى پردازش پرداخت ");
define("_MS_DELAY_LENGTH","تعيين تاخيير بين پيام هاى ارسال شده الكترونى. &#034; با ثانيه &#034;.      ");
define("_MS_DELETE_PENDING_TIME","حداكثر زمان انتظار قبل از حذف نظرات &#034; با دقيقه");
define("_MS_EMAIL","ادرس ايميلى كه از ان براى دستيابى به اطلاعات ارسال شده  استفاده مى شود ");
define("_MS_FAQ_IS_ACTIVE","تعيين مى كند كه ايا سؤالهاى متداول فعال است يا نه");
define("_MS_FIRST_NIGHT_CALCULATING_TYPE","تعيين نوع &#034; شب اول &#034; ازناحيه محاسبه ارزش: واقعى يا متوسط");
define("_MS_GALLERY_KEY","كلمه اى كه در گالرى عكس جايگزين خواهد شد ( كپى ان در صفحه ) ");
define("_MS_GALLERY_WRAPPER","نوع پوشش گالرى عكس ");
define("_MS_IMAGE_GALLERY_TYPE","انواع مجاز گالرى عكس");
define("_MS_IMAGE_VERIFICATION_ALLOW","تعيين استفاده از كد عكس ها براى تاييد ( captcha(");
define("_MS_IS_SEND_DELAY","تعيين اجازه به فاصله زمانى بين پيام هاى الكترونى ارسال شده از سايت ");
define("_MS_ITEMS_COUNT_IN_ALBUM","تعيين اجازه نمايش تعداد عكس ها يا ويدئوها در زير البوم ");
define("_MS_MAXIMUM_ALLOWED_RESERVATIONS","تعيين حداكثر تعدادمجاز رزروهاى اتاق( تمام نشده ) براى هر مشترى ");
define("_MS_MAXIMUM_NIGHTS","تعيين حد اكثر تعدادشب ها در رزرو ( تعريف بر اساس سبد رزرو )");
define("_MS_MAX_ADULTS_IN_SEARCH","تعيين حداكثر تعداد بزرگسالان در فهرست كركرهاى در جستجو از اتاقها");
define("_MS_MAX_CHILDREN_IN_SEARCH","تعيين حداكثر تعداد بچه ها در فهرست كركره اى در جستجو از اتاقها");
define("_MS_MINIMUM_NIGHTS","تعيين حداقل تعداد شب ها در رزرو ( تعريف بر اساس سبد رزرو )");
define("_MS_MULTIPLE_ITEMS_PER_DAY","مشخص مى كند كه ايا به كاربران اجازه ارزيابى بيشتر از يك ايتم را مى دهد يا خير ");
define("_MS_NEWS_COUNT","مشخص مى كند چگونه عدد اخبار در بلوك اخبار نشان داده خواهد شد");
define("_MS_NEWS_HEADER_LENGTH","طول  هدر خبر را در بلوك اخبار تعيين مى كند ");
define("_MS_NEWS_RSS","با استفاده ار اس اس براى دسترسى به اخبار تعريف مى شود");
define("_MS_ONLINE_CREDIT_CARD_REQUIRED","مشخص مى كند كه جمع اورى اطلاعات براى درخواست اينترنتى مورد نياز است ");
define("_MS_PAYMENT_TYPE_2CO","مشخص مى كند كه اجازه پرداخت 2CO بواسطه ممكن است يا نه وكجا مى توان ان را اجرا كرد");
define("_MS_PAYMENT_TYPE_AUTHORIZE","مشخص مى كند كه اجازه پرداخت بواسطه Authorize.Net ممكن است وكجا مى توان ان را اجرا كرد");
define("_MS_PAYMENT_TYPE_BANK_TRANSFER","مشخص مى كند كه پرداخت از طريق حواله بانكى مجاز است وكجا مى توان ان را اجرا كرد ");
define("_MS_PAYMENT_TYPE_ONLINE","مشخص مى كندكه مجاز به  پرداخت از طريق _On-line Order است وكجا مى توان ان را اجرا كرد");
define("_MS_PAYMENT_TYPE_PAYPAL","مشخص مى كند كه مجاز به پرداخت از طريق PayPal است وكجا مى توان ان را اجرا كرد ");
define("_MS_PAYMENT_TYPE_POA","Specifies whether to allow 'Pay on Arrival' (POA) payment type and where apply it");
define("_MS_PAYPAL_EMAIL","تعيين ادرس ايميل حساب سايت بر PayPal");
define("_MS_PREBOOKING_ORDERS_TIMEOUT","تعيين مهلت رزروها &#034; رزرو از قبل &#034; قبل از حذف اين رزروها به صورت خودكار. ) در ساعت ( ");
define("_MS_PRE_MODERATION_ALLOW","اجازه به نظارت  مقدماتى بر نظرات ");
define("_MS_PRE_PAYMENT_TYPE","تعيين نوع پيش پرداخت (قيمت كامل ، شب اول فقط ، مبلغ ثابت ،يا درصد ). ");
define("_MS_PRE_PAYMENT_VALUE","تعيين مقدار پيش پرداخت به انواع &#034; مبلغ ثابت &#034; يا &#034; درصد &#034; &#034; ");
define("_MS_RATINGS_USER_TYPE","نوع كاربران كه مى توانند هتلها را ارزيابى كنند");
define("_MS_REG_CONFIRMATION","تعيين مى كند كه تاييد ) كدام نوع از )براى ثبت نام الزامى است");
define("_MS_REMEMBER_ME","اجازه به ويژگى ذخيره اطلاعات خواهد داد");
define("_MS_RESERVATION EXPIRED_ALERT","تشخيص مى شود كه ارسال هشدار از طريق ايميل به مشتريان هنگام تمام شدن تاريخ انقضاى رزرو  ");
define("_MS_RESERVATION_INITIAL_FEE","هزينه ) اوليه ( شروع - مبلغى كه به كل رزرو اضافه مى شود ( مقدار ثابت در ارز پيش فرض( ");
define("_MS_ROOMS_IN_SEARCH","تعيين مى كند كدام نوع از اتاقها در نتايج جستجو نمايش داده شود : همه اتاقها يا فقط اتاقهاى موجود (غير از اتاقهاى بطور كامل رزرو شده / غير موجود )");
define("_MS_ROTATE_DELAY","تعيين وقت تاخير چرخش بين تبليغات.   با ثانيه");
define("_MS_ROTATION_TYPE","نوع مختلف از چرخش تبليغات");
define("_MS_SEARCH_AVAILABILITY_PAGE_SIZE","تعيين تعداد اتاق / هتلها كه در يك صفحه درنتايج جستجو نمايش داده مى شود ");
define("_MS_SEND_ORDER_COPY_TO_ADMIN","مشخص كردن اجازه ارسال يك كپى از سفارش به مدير سايت يا مالك هتل را خواهد داشت ");
define("_MS_SHOW_BOOKING_STATUS_FORM","مشخص كردن نشان دادن نمونه رزرو در صفحه اصلى  يا نه ");
define("_MS_SHOW_DEFAULT_PRICES","مشخص كردن نشان دادن قيمتهاى پيش فرض در قسمت بازديد كنندگان در سايت يا نه");
define("_MS_SHOW_FULLY_BOOKED_ROOMS"," تعيين اجازه دادن به نمايش اتاقهاى كاملاً رزرو شده / وغير موجود در جستجو ");
define("_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","مشخص مى كند كه ايا بلوك عضويت در خبرنامه نشان داده شود يانه");
define("_MS_SHOW_NEWS_BLOCK","مشخص مى كند ايا بلوك جانبى اخبار نشان داده شود يا نه");
define("_MS_SHOW_RESERVATION_FORM","مشخص مى كند كه ايا فرم رزرو را در صفحه اصلى نشان دهد يا نه");
define("_MS_SHOW_ROOMS_OCCUPANCY","Gibt an, ob Belegung Beschreibung Seite auf dem Zimmer zu zeigen");
define("_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","Gibt die Anzahl der Monate, die auf Raumbelegung Tab angezeigt werden");
define("_MS_SPECIAL_TAX","Special Tax per Guest");
define("_MS_REVIEWS_KEY","كلمه اى كه با فهرست توصيف مشتريان جايگزين خواهد شد (كپى ان در صفحه");
define("_MS_TWO_CHECKOUT_VENDOR","تعيين شماره حساب فروشنده بر روى سايت پرداخت الكترونى 2CO VENDER ID");
define("_MS_USER_TYPE","انواع كاربران كه مى توانند نظرات خود را ارسال كنند. ");
define("_MS_VAT_INCLUDED_IN_PRICE","مشخص مى كند كه هزينه ماليات بر ارزش افزودهVAT ضمن قيمت اتاقها وديگر اضافه ها هست يا نه. ");
define("_MS_VAT_VALUE","تشخيص مقدار پيش فرض ماليات بر ارزش افزوده  بر سفارش VAT (به درصد )
&#034; تعيين براساس كشور &#034; ");
define("_MS_VIDEO_GALLERY_TYPE","انواع مجاز در گالرى ويدئو ");
define("_MS_WATERMARK_TEXT","متن علامت ابى watermark به عكسها اضافه خواهد شد ");
define("_MUST_BE_LOGGED","شما براى مشاهده اين صفحه بايد وارد سيستم شويد ! &#034; ورود به سيستم &#034; يا &#034; ايجاد حساب بصورت رايگان &#034; ");
define("_MY_ACCOUNT","حساب من");
define("_MY_BOOKINGS","رزرو من");
define("_MY_CAR_RENTALS","Mein Mietwagen");
define("_MY_ORDERS","درخواست هاى من");
define("_NAME","نام");
define("_NAME_A_Z","نام ها (ألف -ى )");
define("_NAME_Z_A","نام ها(ى- ألف)");
define("_NEED_ASSISTANCE","به كمك نياز دارى؟");
define("_NEED_ASSISTANCE_TEXT","تيم ما ٢٤/٧ در خدمت شما است تا در مسائل رزرو و پاسخ به سؤالات شما  به شما كمك كند ");
define("_NEVER","هرگز");
define("_NEWS","خبرها ");
define("_NEWSLETTER_PAGE_TEXT","براى دريافت خبرنامه ها ازسايت ما به اسانى ايميل خود را وارد كنيد وبر روى دكمه &#034; اشتراك &#034; كليك كنيد. 

اگر شما بعداً تصميم به متوقف كردن اشتراك ويا تغير نوع اخبار دريافت شده داريد به اسانى لينك موجود در اخر خبرنامه را دنبال كنيد و مشخصات خود را به روز رسانى كنيد ويا با گذاشتن تيك در كادر زير لغو اشتراك كنيد. ");
define("_NEWSLETTER_PRE_SUBSCRIBE_ALERT","براى تكميل روند روى دكمه ( اشتراك ) كليك كنيد ");
define("_NEWSLETTER_PRE_UNSUBSCRIBE_ALERT","لطفاً بر روى دكمه &#034; لغو اشتراك &#034; براى تكميل عمليات كليك كنيد ");
define("_NEWSLETTER_SUBSCRIBERS","مشتركين خبرنامه");
define("_NEWSLETTER_SUBSCRIBE_SUCCESS","تشكر از شما براى اشتراك در خبرنامه الكترونيكى ما. شما يك ايميل براى تاييد اشتراك خود را دريافت خواهيد كرد ");
define("_NEWSLETTER_SUBSCRIBE_TEXT","براى دريافت خبرنامه ها ازسايت ما به اسانى ايميل خود را وارد كنيد وبر روى دكمه &#034; اشتراك &#034; كليك كنيد. 

اگر شما بعداً تصميم به متوقف كردن اشتراك ويا تغير نوع اخبار دريافت شده داريد به اسانى لينك موجود در اخر خبرنامه را دنبال كنيد و مشخصات خود را به روز رسانى كنيد ويا با گذاشتن تيك در كادر زير لغو اشتراك كنيد. ");
define("_NEWSLETTER_SUBSCRIPTION_MANAGEMENT","مديريت اشتراكها در ليست خبرنامه ها ");
define("_NEWSLETTER_UNSUBSCRIBE_SUCCESS","شما موفق به لغو اشتراك از فهرست خبرنامه ها شديد. ");
define("_NEWSLETTER_UNSUBSCRIBE_TEXT","براى لغو اشتراك از خبرنامه هاى ما لطفاً ادرس ايميل خود را در زير وارد كنيد وسپس بر روى دكمه ' لغو اشتراك ' كليك كنيد. ");
define("_NEWS_AND_EVENTS","حوادث وخبرها");
define("_NEWS_MANAGEMENT","مديريت خبرها ");
define("_NEWS_SETTINGS","تنظيمات خبرها ");
define("_NEXT","بعدى");
define("_NIGHT","شب");
define("_NIGHTS","شب ها");
define("_NO","نه");
define("_NONE","هيچ");
define("_NOTICE_MODULES_CODE","براى اضافه كردن ماژول هاى موجود در اين صفحه ، فقط در اين متن كپى كنيد :");
define("_NOTIFICATION_MSG","لطفاً اطلاعات مخصوص طرح هاى ويژه و تخفيفها را برايم ارسال كن");
define("_NOTIFICATION_STATUS_CHANGED","تغيير وضعيت هشدارها");
define("_NOT_ALLOWED","ممنوع");
define("_NOT_AUTHORIZED","شما اجازه مشاهده اين صفحه را نداريد");
define("_NOT_AVAILABLE","در دسترس نيست");
define("_NOT_ENOUGH_MONEY_ALERT","مبلغ كافى در حساب شما  وجود ندارد ");
define("_NOT_GOOD","خوب نيست ");
define("_NOT_PAID_YET","هنوز پرداخت نشده ");
define("_NOVEMBER","اذر");
define("_NO_AVAILABLE","دردسترس نيست");
define("_NO_BOOKING_FOUND","شماره رزروى كه وارد كرديد در سيستم وجود ندارد ! لطفاً دوباره اطلاعات را وارد كنيد ");
define("_NO_COMMENTS_YET","هنوز هيچ نظرى وجود ندارد");
define("_NO_CUSTOMER_FOUND","هيچ مشترى وجود ندارد! ");
define("_NO_DEFAULT_PERIODS","زمان هاى پيش فرض براى اين هتل تعيين نشده است. براى اضافه انها اينجا كليك كنيد ");
define("_NO_NEWS","خبرى نيست ");
define("_NO_PAYMENT_METHODS_ALERT","هيچ گزينه اى براى پرداخت وجود ندارد ! لطفا دوباره بعداً امتحان كنيد يا با تيم پشتيبانى فنى ما تماس بگيريد. ");
define("_NO_RECORDS_FOUND","هيچ نتايجى وجود ندارد");
define("_NO_RECORDS_PROCESSED","هیچ ثبتی برای پردازش پیدا نشد!");
define("_NO_RECORDS_UPDATED","هيچ نتيجه اى به روزرسانى نشده !");
define("_NO_ROOMS_FOUND","متأسفيم. هيچ اتاقى مطابق گزينه هاى جستجو شما وجود ندارد   براى پيدا كردن اتاقهاى بيشتر گزينه هاى جستجوى خود را تغيير دهيد. ");
define("_NO_TEMPLATE","بدون قالب");
define("_NO_USER_EMAIL_EXISTS_ALERT","به نظر مى رسد كه شما قبلاً از طريق سايت ما اتاق رزرو كرده ايد. لطفاً براى تنظيم مجدد نام كاربرى و رمز عبور موقت اينجا كليك كنيد. ");
define("_NO_WRITE_ACCESS_ALERT","لطفاً از امتلاك صلاحيت نوشتن بر روى پوشه هاى ذيل مطمئن شويد :");
define("_OCCUPANCY","اشغال");
define("_OCTOBER","ابان");
define("_OFF","Off");
define("_OFFLINE_LOGIN_ALERT","براى ورود به صفحه مدير كل سايت ، ادرس داده شده را در مرورگر اينترنتى خود بنويسيد. http: // {your_site_address} /index.php؟admin=login?");
define("_OFFLINE_MESSAGE","نامه بدون اتصال اينترنتى");
define("_ON","On");
define("_ONLINE","انلاين");
define("_ONLINE_ORDER","سفارش اينترنتى");
define("_ONLY","فقط");
define("_OPEN","باز");
define("_OPEN_ALERT_WINDOW","باز كردن پنجره هشدار ");
define("_OPERATION_BLOCKED","اين عمليات در نسخه نمايشى مسدود است ! ");
define("_OPERATION_COMMON_COMPLETED","اين عمليات با موفقيت انجام شد !");
define("_OPERATION_WAS_ALREADY_COMPLETED","اين عمليات از قبل انجام شده است !");
define("_OR","يا");
define("_ORDER","ترتيب");
define("_ORDERS","سفارشات");
define("_ORDERS_COUNT","تعداد سفارشات");
define("_ORDER_DATE","تاريخ سفارش");
define("_ORDER_ERROR","نمى توان سفارش شما را تكميل كرد ! لطفاً دوباره بعداً امتحان كنيد. ");
define("_ORDER_NOW","اكنون سفارش دهيد ");
define("_ORDER_PLACED_MSG","متشكرم، درخواست شما در سايت دريافت شد وبه زودى پردازش خواهد شد. وشماره رزرو شما : BOOKING_NUMBER_.");
define("_ORDER_PRICE","قيمت درخواست");
define("_OTHER","ديگرى ");
define("_OUR_LOCATION","موقعيت ما");
define("_OWNER","مالك");
define("_OWNER_NOT_ASSIGNED","شما هنوز به هيچ هتلى براى ديدن گزارشات اختصاص داده نشده ايد. ");
define("_PACKAGES","بسته رزروها");
define("_PACKAGES_MANAGEMENT","مديريت بسته رزرو");
define("_PAGE","صفحه");
define("_PAGES","صفحات");
define("_PAGE_ADD_NEW","اضافه كردن صفحه جديد ");
define("_PAGE_CREATED","صفحه با موفقيت ايجاد شد");
define("_PAGE_DELETED","صفحه با موفقيت حذف شد");
define("_PAGE_DELETE_WARNING","ايا مطمئن هستيد كه مى خواهيد اين صفحه را حذف كنيد");
define("_PAGE_EDIT_HOME","ويرايش صفحه اصلى");
define("_PAGE_EDIT_PAGES","ويرايش صفحات");
define("_PAGE_EDIT_SYS_PAGES","ويرايش صفحات سيستم");
define("_PAGE_EXPIRED","صفحه مورد نظر شما در دسترس نمى باشد");
define("_PAGE_HEADER","هدر صفحه");
define("_PAGE_HEADER_EMPTY","هدر صفحه نمى تواند خالى بماند !");
define("_PAGE_KEY_EMPTY","كليد صفحه نمى تواند خالى بماند ! ");
define("_PAGE_LINK_TOO_LONG","لينك منو بيش از حد طولانى است !");
define("_PAGE_MANAGEMENT","مديريت صفحات ");
define("_PAGE_NOT_CREATED","صفحه ايجاد نشده است!");
define("_PAGE_NOT_DELETED","صفحه حذف نشده است ! ");
define("_PAGE_NOT_EXISTS","صفحه اى كه شما مى خواهيد به ان دسترسى پيدا كنيد موجود نمى باشد");
define("_PAGE_NOT_FOUND","صفحه پيدا نشد");
define("_PAGE_NOT_SAVED","صفحه ذخيره نشده است ! ");
define("_PAGE_ORDER_CHANGED","ترتيب صفحات باموفقيت انجام شد ! ");
define("_PAGE_REMOVED","صفحه باموفقيت حذف شد ! ");
define("_PAGE_REMOVE_WARNING","ايا شما مطمئن هستيد كه مى خواهيد اين صفحه را به سطل اشغال منتقل كنيد ؟");
define("_PAGE_RESTORED","صفحه با موفقيت بازگردانده شد !");
define("_PAGE_RESTORE_WARNING","ايا شما مطمئن هستيد كه مى خواهيد اين صفحه را باز گردانيد ؟ ");
define("_PAGE_SAVED","صفحه با موفقيت ذخيره شد !");
define("_PAGE_TEXT","متن صفحه");
define("_PAGE_TITLE","عنوان صفحه");
define("_PAGE_UNKNOWN","صفحه ناشناخته است");
define("_PAID_SUM","gezahlten Betrag");
define("_PARAMETER","پارامتر");
define("_PARTIALLY_AVAILABLE","به طور جزئى در دسترس است ");
define("_PARTIAL_PRICE","قيمت جزئى ");
define("_PASSWORD","رمز عبور");
define("_PASSWORD_ALREADY_SENT","رمز عبور شما قبلاً به ايميل شما ارسال شده است. لطفاً دوباره بعداً امتحان كنيد. ");
define("_PASSWORD_CHANGED","رمز عبور تغيير يافت");
define("_PASSWORD_DO_NOT_MATCH","رمز عبور و تاييد ان مطابقت ندارند ! ");
define("_PASSWORD_FORGOTTEN","رمز عبور فراموش شده است ");
define("_PASSWORD_FORGOTTEN_PAGE_MSG","استفاده از يك ادرس ايميل صحيح براى بازگردانى رمز عبور مدير كل سايت مخصوص صفحه مدير كل ");
define("_PASSWORD_IS_EMPTY","رمز عبور نمى تواند خالى بماند وبايد بيشتر از ٦ حرف باشد");
define("_PASSWORD_NOT_CHANGED","رمز عبور تغيير نكرده است. لطفاً دوباره تلاش كنيد !");
define("_PASSWORD_RECOVERY_MSG","براى بازيابى رمز عبور خود لطفاً    ادرس ايميل خود را وارد كنيد ولينك بازگردانى رمز عبور براى شما ايميل ميشود. ");
define("_PASSWORD_SUCCESSFULLY_SENT","رمز عبور توسط ايميل براى شما ارسال شد !");
define("_PAST_TIME_ALERT","شما نمى توانيد عمليات رزرو را در تاريخ هاى گذشته انجام دهيد ! لطفاً تاريخ هاى رزرو را دوباره انتخاب كنيد. ");
define("_PAYED_BY","پرداخت بواسطه");
define("_PAYMENT","پرداخت");
define("_PAYMENTS","پرداخت ها");
define("_PAYMENT_COMPANY_ACCOUNT","حساب پرداخت شركت");
define("_PAYMENT_DATE","تاريخ پرداخت");
define("_PAYMENT_DETAILS","جزئيات پرداخت");
define("_PAYMENT_ERROR","خطا هنگام پرداخت");
define("_PAYMENT_METHOD","روش پرداخت");
define("_PAYMENT_METHODS","روش هاى پرداخت");
define("_PAYMENT_REQUIRED","مستلزم پرداخت");
define("_PAYMENT_SUM","مجموع پرداخت");
define("_PAYMENT_TYPE","نوع پرداخت");
define("_PAYPAL","PayPal");
define("_PAYPAL_NOTICE","از وقت استفاده كنيد. پرداخت امن با استفاده از اطلاعات پرداخت ذخيره شده خود. پرداخت از طريق كارت اعتبارى ويا حساب بانكى PayPal يا موجودى حساب. ");
define("_PAYPAL_ORDER","درخواست از طريف PayPal");
define("_PAY_ON_ARRIVAL","پرداخت هنگام رسيدن");
define("_PAY_WITH_BALANCE","پرداخت از موجودى ");
define("_PC_BILLING_INFORMATION_TEXT","اطلاعات صورت حساب : ادرس ، شهر ، كشور و غيره");
define("_PC_BOOKING_DETAILS_TEXT","جزئيات سفارش ، ليستى از محصولات خريدارى شده ، وغيره");
define("_PC_BOOKING_NUMBER_TEXT","شماره دوخواست");
define("_PC_EVENT_TEXT","عنوان رويداد");
define("_PC_FIRST_NAME_TEXT","اسم اول مشترى ويا مدير");
define("_PC_HOTEL_INFO_TEXT","اطلاعات درباره هتل : نام ،ادرس ، تلفن ، فكس، و غيره");
define("_PC_LAST_NAME_TEXT","نام خانوادگى مشترى ويا مدير ");
define("_PC_PERSONAL_INFORMATION_TEXT","اطلاعات شخصى مشترى : نام ونام خانوادگى. وغيره");
define("_PC_REGISTRATION_CODE_TEXT","كد تاييدى حساب جديد");
define("_PC_STATUS_DESCRIPTION_TEXT","وصف وضعيت پرداخت");
define("_PC_USER_EMAIL_TEXT","ايميل كاربر ");
define("_PC_USER_NAME_TEXT","نام كاربرى  ورود به سيستم كاربر ");
define("_PC_USER_PASSWORD_TEXT","رمز عبور مشترى و مدير ");
define("_PC_WEB_SITE_BASED_URL_TEXT","ادرس پايه وب سايت ");
define("_PC_WEB_SITE_URL_TEXT","ادرس سايت");
define("_PC_YEAR_TEXT","سال جارى در فرمت YYYY");
define("_PENDING","در انتظار");
define("_PEOPLE_ARRIVING","رسيدن مردم");
define("_PEOPLE_DEPARTING","ترك كردن مردم ");
define("_PEOPLE_STAYING","ماندن مردم");
define("_PERCENTAGE_MAX_ALOWED_VALUE","حدا اكثر مقدار مجاز ٩٩٪‏ است ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_PERFORM_OPERATION_COMMON_ALERT","ايا شما مطمئن هستيد كه مى خواهيد اين عمليات را اجرا كنيد ؟ ");
define("_PERIODS","دوره هاى زمانى ");
define("_PERSONAL_DATA_SAVED","اطلاعات صورت حساب شما با موفقيت به روز رسانى شد. ");
define("_PERSONAL_DETAILS","جزئيات شخصى ");
define("_PERSONAL_INFORMATION","معلومات شخصى ");
define("_PERSON_PER_NIGHT","شخص / در شب");
define("_PER_NIGHT","در شب");
define("_PHONE","تلفن");
define("_PHONE_EMPTY_ALERT","رشته تلفن نمى تواند خالى بماند ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_PICK_DATE","تقويم را باز كنيد وتاريخ را انتخاب كنيد ");
define("_PLACEMENT","تعيين سطح");
define("_PLACE_ORDER","پيگيرى رزرو ");
define("_PLAY","روشن كردن ");
define("_POPULARITY","معروفيت");
define("_POPULAR_SEARCH","جستجوى شايع");
define("_POSTED_ON","پست شده در ");
define("_POST_COM_REGISTERED_ALERT","شما نياز به ثبت نام قبل از ارسال نظر خود را داريد ");
define("_PREBOOKING","رزرو قبلى");
define("_PREDEFINED_CONSTANTS","ثابت از پيش تعريف شده");
define("_PREFERENCES","ترجيحات");
define("_PREFERRED_LANGUAGE","زبان مرجح");
define("_PREVIEW","معاينه");
define("_PREVIOUS","قبلى");
define("_PRE_PAYMENT","پرداخت قبلى");
define("_PRICE","قيمت");
define("_PRICES","قيمت ها");
define("_PRICE_EMPTY_ALERT","رشته قيمت نمى تواند خالى بماند ! لطفاً دوباره اطلاعات را وارد كنيد. ");
define("_PRICE_FORMAT","فرمت قيمت");
define("_PRICE_FORMAT_ALERT","اجازه نمايش قيمتها در فرمت مناسب به بازديدكنندگان");
define("_PRICE_H_L","قيمت(از بالا)");
define("_PRICE_L_H","قيمت  ( از پايين )");
define("_PRINT","چاپ كردن");
define("_PRIVILEGES","امتيازات");
define("_PRIVILEGES_MANAGEMENT","مديريت امتيازات");
define("_PRODUCT","محصول");
define("_PRODUCTS","محصولات");
define("_PRODUCTS_COUNT","تعداد محصولات");
define("_PRODUCTS_MANAGEMENT","مديريت محصولات");
define("_PRODUCT_DESCRIPTION","توصيف محصول");
define("_PROMO_AND_DISCOUNTS","تبليغات وتخفيف ها");
define("_PROMO_CODE_OR_COUPON","كدهاى تبليغاتى ويا كارت تخفيف");
define("_PROMO_COUPON_NOTICE","اگر شما يك كد تبليغى ويا كوپن تخفيف داريد لطفاً ان را اينجا وارد كنيد. ");
define("_PROPERTY_TYPE","نوع ملك");
define("_PROPERTY_TYPES","انواع املاك");
define("_PUBLIC","عمومى");
define("_PUBLISHED","منتشر شده");
define("_PUBLISH_YOUR_COMMENT","انتشار نظر خود ");
define("_QTY","مقدار");
define("_QUANTITY","مقدار");
define("_QUESTION","سوال");
define("_QUESTIONS","سوالها");
define("_RATE","قيمت");
define("_RATE_PER_NIGHT","قيمت يك شب");
define("_RATE_PER_NIGHT_AVG","متوسط نرخ هرشب");
define("_RATINGS","سنجش ها");
define("_RATINGS_SETTINGS","تنظيمات سنجش ها");
define("_REACTIVATION_EMAIL","ارسال دوباره ايميل فعالسازى ");
define("_READY","اماده");
define("_READ_MORE","بيشتر بخوانيد");
define("_REAL_TIME_CAMPAIGN","كمپين زمان واقعى ");
define("_REASON","دليل");
define("_RECORD_WAS_DELETED_COMMON","ركورد با موفقيت حذف شد! ");
define("_REFRESH","تازه كردن");
define("_REFUNDED","بازگرداندن اموال");
define("_REGISTERED","ثبت شده");
define("_REGISTERED_FROM_IP","ثبت نام از ادرس اينترنت");
define("_REGISTRATIONS","ثبت نام ها");
define("_REGISTRATION_CODE","كد ثبت نام ");
define("_REGISTRATION_CONFIRMATION","تاييد ثبت نام ");
define("_REGISTRATION_FORM","نمونه ثبت نام ");
define("_REGISTRATION_NOT_COMPLETED","مراحل ثبت نام شما كامل نيست ! براى اطلاعات بيشتر لطفاً ايميل خود را بررسى كنيد ويا براى ارسال دوباره اينجا كليك كنيد ");
define("_REMEMBER_ME","ياداورى كن ");
define("_REMOVE","حذف");
define("_REMOVED","حذف شد");
define("_REMOVE_ACCOUNT","حذف حساب");
define("_REMOVE_ACCOUNT_ALERT","ايا مطمئنيد كه مى خواهيد اين حساب را حذف كنيد ؟ ");
define("_REMOVE_ACCOUNT_WARNING","اگر شما فكر مى كنيد كه اين سايت را دوباره استفاده نمى كنيد ومى خواهيد حساب خود را حذف كنيد مى توانيد ان را به عهده ما بسپاريد. به خاطر داشته باشيد كه شما قادر به فعالسازى حساب ويا بازيابى هريك از مطالب را نخواهيد بود اگر هنوز مى خواهيد حساب خود را حذف كنيد لطفاً روى دكمه &#034; حذف &#034; كليك كنيد. ");
define("_REMOVE_LAST_COUNTRY_ALERT","كشور انتخاب شده حذف نمى شود ، زيرا بايد حداقل يك كشور فعال براى عملكرد صحيح از سايت داشته باشيد ");
define("_REMOVE_ROOM_FROM_CART","حذف اتاق از بسته رزرو ");
define("_RENTAL_FROM","Vermietung von");
define("_RENTAL_TO","Vermietung an");
define("_REPORTS","گزارشات");
define("_RESEND_ACTIVATION_EMAIL","ارسال دوباره ايميل فعالسازى ");
define("_RESEND_ACTIVATION_EMAIL_MSG","لطفاً ادرس ايميل خود را وارد كنيد وپس از ان روى دكمه &#034; ارسال &#034; كليك كنيد. وبه زودى يك ايميل فعالسازى دريافت خواهيد كرد. ");
define("_RESERVATION","رزرو");
define("_RESERVATIONS","رزروها");
define("_RESERVATION_CART","بسته رزروها");
define("_RESERVATION_CART_IS_EMPTY_ALERT","بسته رزرو شما خالى است ! ");
define("_RESERVATION_DETAILS","جزئيات رزرو ");
define("_RESERVATION_NUMBER","Reservierungsnummer");
define("_RESERVED","رزرو شده");
define("_RESET","تنظيم مجدد");
define("_RESET_ACCOUNT","تنظيم مجدد حساب");
define("_RESTAURANT","رستوران");
define("_RESTORE","باز گرداندن");
define("_RETYPE_PASSWORD","تكرار كلمه مرور");
define("_RIGHT","راست");
define("_RIGHT_TO_LEFT","RTL (از راست به چپ)");
define("_ROLES_AND_PRIVILEGES","نقش و صلاحيت ها ");
define("_ROLES_MANAGEMENT","مديريت نقش ها ");
define("_ROOMS","اتاقها");
define("_ROOMS_AVAILABILITY","در دسترس بودن اتاقها");
define("_ROOMS_COUNT","تعداد اتاقها (در هتل)");
define("_ROOMS_FACILITIES","امكانات اتاقها");
define("_ROOMS_LAST","اخرين اتاق");
define("_ROOMS_LEFT","اتاقهاى در دسترس ");
define("_ROOMS_MANAGEMENT","مديريت اتاقها");
define("_ROOMS_OCCUPANCY","إشغال اتاقها");
define("_ROOMS_RESERVATION","رزرو اتاقها");
define("_ROOMS_SETTINGS","تنظيمات اتاقها");
define("_ROOM_AREA","مساحت اتاق");
define("_ROOM_DESCRIPTION","وصف اتاق");
define("_ROOM_DETAILS","جزئيات اتاق");
define("_ROOM_FACILITIES","امكانات اتاق");
define("_ROOM_FACILITIES_MANAGEMENT","مديريت امكانات اتاق");
define("_ROOM_LEFT","اتاق در دسترس");
define("_ROOM_NOT_FOUND","اتاق موجود نمى باشد");
define("_ROOM_NUMBERS","شماره هاى اتاق");
define("_ROOM_PRICE","قيمت اتاق");
define("_ROOM_PRICES_WERE_ADDED","قيمت اتاق براى دوره جديد باموفقيت اضافه شد ");
define("_ROOM_TYPE","نوع اتاق");
define("_ROOM_WAS_ADDED","اتاق با موفقيت به رزرو شما اضافه شد");
define("_ROOM_WAS_REMOVED","اتاق انتخاب شده با موفقيت از سبد رزرو مخصوص خود حذف شد ! ");
define("_ROWS","خطوط يا رديف");
define("_RSS_FEED_TYPE","نوع تغذيه");
define("_RSS_FILE_ERROR","نمى توان فايل RSS را براى اضافه كردن ايتم جديد باز كرد ! لطفاً صلاحيت نوشتن روى فايل feeds/ را بررسى كنيد ويا دوباره بعدا امتحان كنيد. ");
define("_RUN_CRON","اجراى corn");
define("_RUN_EVERY","اجراى كل");
define("_SA","شنبه");
define("_SAID","گفت");
define("_SAT","شنبه");
define("_SATURDAY","شنبه");
define("_SCHEDULED_CAMPAIGN","كمپين برنامه ريزى شده");
define("_SEARCH","جستجو");
define("_SEARCH_KEYWORDS","جستجو كلمات كليدى");
define("_SEARCH_RESULT_FOR","نتايج جستجو براى");
define("_SEARCH_ROOM_TIPS","يافتن  اتاقهاى بيشتر با تغيير گزينه ها جستجو

-كم كردن تعداد افراد بالغ در اتاق 
- كم كردن تعداد بچه ها در اتاقها 
- تغيير تاريخ ورود وخروج 
");
define("_SEC","Sec");
define("_SELECT","انتخاب كن");
define("_SELECTED_ROOMS","اتاقهاى انتخاب شده");
define("_SELECT_FILE_TO_UPLOAD","انتخاب فايل براى اپلود ");
define("_SELECT_HOTEL","انتخاب هتل");
define("_SELECT_LANG_TO_UPDATE","انتخاب زبان براى به روز رسانى");
define("_SELECT_LOCATION","انتخاب مكان");
define("_SELECT_REPORT_ALERT","لطفاً نوع گزارش را انتخاب كنيد ");
define("_SEND","ارسال");
define("_SENDING","ارسال");
define("_SEND_COPY_TO_ADMIN","ارسال يك كپى به مدير ");
define("_SEND_INVOICE","ارسال صورت حساب");
define("_SEO_LINKS_ALERT","اگر اين گزينه را انتخاب كنيد لطفاً مطمئن شويد كه بخش لينكهاى ) SEO) درفايل htaccess وجود ندارد");
define("_SEO_URLS","SEO URLs");
define("_SEPTEMBER","مهر");
define("_SERVER_INFO","اطلاعات سرور");
define("_SERVER_LOCALE","زبان سرور");
define("_SERVICE","سرويس");
define("_SERVICES","سرويس ها ");
define("_SETTINGS","تنظيمات");
define("_SETTINGS_SAVED","تغييرات ذخيره شد ! لطفاً براى ديدن نتايج صفحه اصلى را ابديت كنيد. ");
define("_SET_ADMIN","تعيين  مدير ");
define("_SET_DATE","تعيين تاريخ");
define("_SET_PERIODS","تعيين دوره ها");
define("_SET_TIME","تعيين زمان");
define("_SHORT_DESCRIPTION","شرح مختصر");
define("_SHOW","نمايش");
define("_SHOW_IN_SEARCH","نمايش در جستجو");
define("_SHOW_META_TAGS","نمايش تگ هاى META");
define("_SHOW_ON_DASHBOARD","نمايش در صفحه كنترل");
define("_SIDE_PANEL","پنل كنارى");
define("_SIMPLE","ساده");
define("_SITE_DEVELOPMENT_MODE_ALERT","اين سايت در حالت توسعه درحال اجرا است ! براى خروج از اين حالت ارزشSITE_MODE را در فايل inc/setting.inc.php تغيير دهيد");
define("_SITE_INFO","اطلاعات سايت");
define("_SITE_OFFLINE","سايت افلاين است");
define("_SITE_OFFLINE_ALERT","انتخاب كنيد كه ايا ورود به صفحه اول سايت براى بازديدكنندگان ممكن است يا خير. اگر ورود بازديدكنندگان به صفحه اول ممكن است ، صفحه اول نمايش داده خواهد شد. پيام در زير نمايش داده مى شود. ");
define("_SITE_OFFLINE_MESSAGE_ALERT","پيامى كه در صفحه اول سايت در حالت افلاين نمايش داده مى شود");
define("_SITE_PREVIEW","معاينه سايت");
define("_SITE_RANKS","رتبه سايت");
define("_SITE_RSS","سايتRSS");
define("_SITE_SETTINGS","تنظيمات سايت");
define("_SMTP_HOST","سرور ارسال ايميل");
define("_SMTP_PORT","خروجى ارسال ايميل SMTP port");
define("_SMTP_SECURE","امن SMTP");
define("_SORT_BY","مرتب سازى بر اساس");
define("_STANDARD","استاندارد");
define("_STANDARD_PRICE","قيمت استاندارد");
define("_STARS","ستاره ها");
define("_STARS_1_5","1 ستاره5  الى ستاره");
define("_STARS_5_1","5 ستاره 1  الى ستاره");
define("_STARTING_FROM","ابتدا از ");
define("_START_DATE","تاريخ شروع");
define("_START_FINISH_DATE_ERROR","تاريخ پايان بايد بعد از انتهاى تاريخ شروع باشد  ! لطفاً دوباره اطلاعات را وارد كنيد ");
define("_START_OVER","شروع از نو ");
define("_STATE","ايالت");
define("_STATES","ايالات");
define("_STATE_PROVINCE","ايالت / استان");
define("_STATISTICS","امار");
define("_STATUS","وضعيت");
define("_STOP","توقف");
define("_SU","Su");
define("_SUBJECT","موضوع");
define("_SUBJECT_EMPTY_ALERT","موضوع نمى تواند خالى بماند ! ");
define("_SUBMIT","ارسال");
define("_SUBMIT_BOOKING","ارسال رزرو");
define("_SUBMIT_PAYMENT","ارسال پرداخت");
define("_SUBSCRIBE","اشتراك");
define("_SUBSCRIBE_EMAIL_EXISTS_ALERT","فردی با این ایمیل در حال حاضر به خبرنامه ما مشترک است. لطفا یک آدرس ایمیل دیگر برای اشتراک را انتخاب نمایید");
define("_SUBSCRIBE_TO_NEWSLETTER","اشتراك در ليست خبرنامه ");
define("_SUBSCRIPTION_ALREADY_SENT","شما در حال حاضر در خبرنامه ما مشترك هستيد. لطفاً دوباره بعداً امتحان كنيد يا WAIT ثانيه صبر كنيد");
define("_SUBSCRIPTION_MANAGEMENT","مديريت مشتركين");
define("_SUBTOTAL","مبلغ كلى ");
define("_SUMMARY","خلاصه");
define("_SUN","يكشنبه");
define("_SUNDAY","يكشنبه");
define("_SWITCH_TO_EXPORT","تغيير به صادرات ");
define("_SWITCH_TO_NORMAL","تغيير به عادى ");
define("_SYMBOL","نماد");
define("_SYMBOL_PLACEMENT","قرار دادن نماد ");
define("_SYSTEM","سيستم ");
define("_SYSTEM_EMAIL_DELETE_ALERT","این قالب ایمیل توسط سیستم استفاده می شود و نمی توان ان را حذف كرد !");
define("_SYSTEM_MODULE","سيستم ماژول");
define("_SYSTEM_MODULES","سيستم ماژول ها");
define("_SYSTEM_MODULE_ACTIONS_BLOCKED","تمامی عملیات با ماژول سیستم مسدود شده اند!");
define("_SYSTEM_TEMPLATE","قالب سيستم");
define("_TAG","برچسب");
define("_TAG_TITLE_IS_EMPTY","برچسب >TITLE> نمى تواند خالى بماند ! لطفاً دوباره وارد كنيد");
define("_TARGET","هدف");
define("_TARGET_GROUP","گره هدف");
define("_TARGET_HOTEL","هتل هدف");
define("_TAXES","عوارض");
define("_TEMPLATES_STYLES","قالب ها والگوها");
define("_TEMPLATE_CODE","كد قالب");
define("_TEMPLATE_IS_EMPTY","قالب نمى تواند خالى بماند ! لطفاً دوباره اطلاعات را وارد كنيد");
define("_TERMS","شرايط ومقررات");
define("_REVIEWS","نظرات مشتريان ");
define("_REVIEWS_MANAGEMENT","مديريت نظرات مشتريان");
define("_REVIEWS_SETTINGS","تنظيمات نظرات مشتريان");
define("_TEST_EMAIL","ايميل ازمايشى ");
define("_TEST_MODE_ALERT","حالت تست در سبد رزرو روشن است ! براى تغيير حالت فعلى &#034; اينجا &#034; كليك كنيد ");
define("_TEST_MODE_ALERT_SHORT","توجه : سبد رزرو درحال اجرا در حالت تست است ! ");
define("_TEXT","متن");
define("_TH","پنجشنبه");
define("_THU","پنجشنبه");
define("_THUMBNAIL","كوچك");
define("_THURSDAY","پنجشنبه");
define("_TIME_PERIOD_OVERLAPPING_ALERT","اين دوره از زمان بطور ( كلى يا جزئى ) در حال حاضر انتخاب شده است ! لطفاً يكى ديگر را انتخاب كنيد ");
define("_TIME_ZONE","منطقه زمانى");
define("_TIPTRONIC","Tiptronic");
define("_TO","به");
define("_TODAY","امروز");
define("_TODAY_CHECKIN","ورود امروز");
define("_TODAY_CHECKOUT","خروج امروز");
define("_TODAY_TOP_DEALS","");
define("_TODAY_TOP_DEALS_TEXT","شروع جستجوى خود با يك نگاه به بهترين نرخ در سايت ما. ");
define("_TODAY_TOP_DEALS_WITH_BR","");
define("_TOP","بالا ترين");
define("_TOP_PANEL","پنل بالا");
define("_TOTAL","كلى");
define("_TOTAL_PRICE","قيمت كلى");
define("_TOTAL_ROOMS","تعداد كلى اتاقها ");
define("_TRANSACTION","معامله");
define("_TRANSLATE_VIA_GOOGLE","ترجمه از طريق گوگل");
define("_TRASH","حذف شده ها");
define("_TRASH_PAGES","صفحه هاى حذف شده");
define("_TRUNCATE_RELATED_TABLES","كوتاه كردن جداول مربوطه");
define("_TRY_LATER","يك خطا هنگام اجرا رخ داده. لطفاً دوباره بعداً امتحان كنيد !");
define("_TRY_SYSTEM_SUGGESTION","پيشنهادات سايت را امتحان كنيد");
define("_TU","سه شنبه");
define("_TUE","سه شنبه");
define("_TUESDAY","سه شنبه");
define("_TYPE","نوع");
define("_TYPE_CHARS","لطفاً حروفى را كه درتصوير مى بينيد وارد كنيد");
define("_UNCATEGORIZED","دسته بندى نشده");
define("_UNDEFINED","تعريف نشده");
define("_UNINSTALL","حذف");
define("_UNITS","واحد ها");
define("_UNIT_PRICE","واحد قيمت");
define("_UNKNOWN","نا شناخته");
define("_UNSUBSCRIBE","لغو اشتراك");
define("_UP","بالا");
define("_UPDATING_ACCOUNT","به روز رسانى حساب");
define("_UPDATING_ACCOUNT_ERROR","خطايى هنگام به روز رسانى حساب شما رخ داده ! لطفاً دوباره بعداً امتحان كنيد ويا اطلاعات مربوط به اين خطا را به مديريت سايت ارسال نمائيد. ");
define("_UPDATING_OPERATION_COMPLETED","عمليات به روز رسانى با موفقيت انجام شد ! ");
define("_UPLOAD","اپلود");
define("_UPLOAD_AND_PROCCESS","اپلود وفرايند. ");
define("_UPLOAD_FROM_FILE","اپلود از فايل ");
define("_URL","URL");
define("_USED_ON","استفاده شده در ");
define("_USERNAME","نام كاربرى ");
define("_USERNAME_AND_PASSWORD","نام كاربرى ورمز عبور ");
define("_USERNAME_EMPTY_ALERT","نام كاربرى نمى تواند خالى بماند ! لطفاً دوباره اطلاعات را وارد كنيد");
define("_USERNAME_LENGTH_ALERT","طول نام كاربرى نمى تواند كمتر از ٤ حرف باشد ! لطفاً دوباره اطلاعات را وارد كنيد");
define("_USERS","كاربران");
define("_USER_EMAIL_EXISTS_ALERT","يك كاربر با اين ايميل وجود دارد! لطفاً يكى ديگر را انتخاب كنيد. ");
define("_USER_EXISTS_ALERT","يك كاربر با همين نام كاربرى وجود دارد. يكى ديگر را انتخاب كنيد. ");
define("_USER_NAME","نام كاربرى ");
define("_USE_THIS_PASSWORD","اين رمز عبور را استفاده كن ");
define("_VALUE","ارزش");
define("_VAT","VATعوارض بر ارزش اضافه شده ");
define("_VAT_PERCENT","VAT نسبت ");
define("_VEHICLE","Fahrzeug");
define("_VERSION","نسخه");
define("_VERY_GOOD","خيلى خوب");
define("_VIDEO","ويدئو ");
define("_VIEW_ALL","نمايش همه");
define("_VIEW_WORD","نمايش");
define("_VILLA","ويلا");
define("_VILLAS","ويلا ها ");
define("_VISITOR","مهمان");
define("_VISITORS_RATING","ارزيابى مهمانها ");
define("_VISUAL_SETTINGS","تنظيمات بصرى (تنظيمات ويژوال )");
define("_VOCABULARY","واژه ها");
define("_VOC_KEYS_UPDATED","عمليات با موفقيت انجام شد ! زمينه هاى ابديت شده _KEYS_keys. براى ابديت صفحه &#034; اينجا &#034; كليك كنيد. ");
define("_VOC_KEY_UPDATED"," مقدارواژه ها با موفقيت به روز رسانى شد. ");
define("_VOC_KEY_VALUE_EMPTY","مقدار كليد نمى تواند خالى بماند. لطفاً دوباره وارد كنيد ");
define("_VOC_NOT_FOUND","هيچ كليدى وجود ندارد. ");
define("_VOC_UPDATED","واژه ها با موفقيت به روز رسانى شدند. براى ابديت سايت &#034; اينجا &#034; كليك كنيد");
define("_VOTE_NOT_REGISTERED","رأى شما ثبت نشده است ! براى رأى دادن بايد وارد سايت بشويد ");
define("_WE","ما");
define("_WEB_SITE","وب سايت");
define("_WED","چهارشنبه");
define("_WEDNESDAY","چهارشنبه");
define("_WEEK_START_DAY","روز شروع هفته");
define("_WELCOME_AGENCY_TEXT","<p>سلام <b>
_FIRST_NAME_ _LAST_NAME_!</b></p>
<p>
 به صفحه كنترل اژانس خوش امديد كه به شما اجازه نمايش وضعيت حسابو كنترل تنظيمات و رزروهايى كه انجام داديد را مى دهد. </p>

<p>_TODAY_ <br/>_LAST_LOGIN_</p>

•براى مشهده خلاصه اين حساب برروى &#034; صفحه كنترل &#034; كليك كنيد 
•&#034; تنظيم اطلاعات حساب &#034;به شما اجازه تغيير اطلاعات شخصى واطلاعات حساب را مى دهد 

•  رزروهاى من &#034; كه حاوى جزئيات رزروهايى كه انجام دادم 
");
define("_WELCOME_CUSTOMER_TEXT","<p>سلام <b> _FIRST_NAME_ _LAST_NAME_!</b></p>
<p>
 به صفحه كنترل مشترى خوش امديد كه به شما اجازه نمايش وضعيت حسابو كنترل تنظيمات و رزروهايى كه انجام داديد را مى دهد. </p>
<p>_TODAY_ <br/>_LAST_LOGIN_ </p>

<p><b>&#8226;</b>براى مشهده خلاصه اين حساب برروى صفحه كنترل  كليك <a href=&#034;index.php?customer=home&#034;>كنيد </a></p>

<p> <b>&#8226;</b><a href=&#034;index.php?customer=my_account&#034;> تنظيم اطلاعات حساب<a> به شما اجازه تغيير اطلاعات شخصى واطلاعات حساب را مى دهد </p>

<p><b>&#8226;</b> <a href=&#034;index.php?customer=my_bookings&#034;> رزروهاى</a>
 من كه حاوى جزئيات رزروهايى كه انجام دادم </p>");
define("_WHAT_IS_CVV"," رمز CVVچيست");
define("_WHOLE_SITE","سايت كامل");
define("_WIDGET_INTEGRATION_MESSAGE","شما مى توانيد اين موتور جستجوى هتلها را با سايت هاى ديگر ادغام كنيد. ");
define("_WIDGET_INTEGRATION_MESSAGE_HINT","نكته : براى نمايش تمام هتلهاى hsJsKey قابل دسترس ، ارزش رشته را خالى بگذاريد ويا شماره هتلها را با علامت كاما فاصله بگذاريد  مثال hsHotel IDs=&#034;2٫9٫12&#034;");
define("_WITHOUT_ACCOUNT","بدون حساب");
define("_WONDERFUL","شگفت انگيز ! ");
define("_WRONG_BOOKING_NUMBER","شماره رزروى كه وارد كرديد موجود نمى باشد ! لطفاً يك شماره رزرو صحيح وارد كنيد. ");
define("_WRONG_CHECKOUT_DATE_ALERT","تاريخى كه وارد كرديد درست نمى باشد ! لطفاً تاريخ ترك درست وارد كنيد ");
define("_WRONG_CODE_ALERT","متأسفم ، كدى كه وارد كرديد اشتباه است ! لطفاً دوباره بعداً امتحان كنيد ");
define("_WRONG_CONFIRMATION_CODE","كد تاييد اشتباه است ويا تاييد ثبت نام از قبل انجام شده است");
define("_WRONG_COUPON_CODE","اين كد كوپن منقضى ويا معتبر نمى باشد ! ");
define("_WRONG_FILE_TYPE","فايل اپلود شده PHP فايل لغت معتبر نمى باشد ");
define("_WRONG_LOGIN","اشتباه در نام كاربرى ورمز عبور ");
define("_WRONG_PARAMETER_PASSED","يك خطا در اطلاعات ثبت شد ! اتمام عمليات امكان پذير نيست");
define("_WYSIWYG_EDITOR","ويرايشگر متن هاWYSIWYG");
define("_YEAR","سال");
define("_YES","بله");
define("_YOUR_EMAIL","ايميل شما ");
define("_YOUR_NAME","نام شما");
define("_YOU_ARE_LOGGED_AS","شما وارد شديد با عنوان");
define("_YOU_MAY_ALSO_LIKE","شايد خوشتان بيايد");
define("_ZIPCODE_EMPTY_ALERT","كد پستى Zip/ نمى تواند خالى بماند");
define("_ZIP_CODE","كد پستى ");

?>