<?php

/**
 *	BanList Class
 *  -------------- 
 *  Description : encapsulates ban list properties
 *	Written by  : ApPHP
 *	Version     : 1.0.0
 *  Updated	    : 09.09.2012
 *	Usage       : Core Class (ALL)
 *	Differences : no
 *
 *	PUBLIC:				  	STATIC:				 	PRIVATE:
 * 	------------------	  	---------------     	---------------
 *	__construct				MassMailLogAddRecord
 *	__destruct
 *
 *  1.0.0
 *      - added maxlength validation to 'Reason' field
 *      - changed " with '
 *      - added 'maxlength' to textareas
 *      -
 *      -      
 *	
 **/


class MassMailLogs extends MicroGrid {
	
	protected $debug = false;
	
	//==========================================================================
    // Class Constructor
	//==========================================================================
	function __construct()
	{		
		parent::__construct();
		
		$this->params = array();
		

		$this->params['language_id'] = MicroGrid::GetParameter('language_id');
	
		$this->primaryKey 	= 'id';
		$this->tableName 	= TABLE_MASS_MAIL_LOGS;
		$this->dataSet 		= array();
		$this->error 		= '';
		$this->formActionURL = 'index.php?admin=mass_mail_log';
		$this->actions      = array('add'=>false, 'edit'=>false, 'details'=>false, 'delete'=>true);
		$this->actionIcons  = true;
		$this->allowRefresh = true;

		$this->allowLanguages = false;
		$this->languageId  	= ($this->params['language_id'] != '') ? $this->params['language_id'] : Languages::GetDefaultLang();
		$this->WHERE_CLAUSE = ''; // WHERE .... / 'WHERE language_id = \''.$this->languageId.'\'';
		$this->ORDER_CLAUSE = ''; // ORDER BY '.$this->tableName.'.date_created DESC
		
		$this->isAlterColorsAllowed = true;

		$this->isPagingAllowed = true;
		$this->pageSize = 20;

		$this->isSortingAllowed = true;

		$this->isFilteringAllowed = true;
		$datetime_format = get_datetime_format();
		$arr_email_types = array('test'=>_TEST_EMAIL, 'newsletter_subscribers'=>_NEWSLETTER_SUBSCRIBERS, 'all'=>_ALL, 'uncategorized'=>_UNCATEGORIZED, 'admins'=>_ADMINS);
		$arr_users = array();
		$sql = 'SELECT
					'.TABLE_ACCOUNTS.'.id,
					CONCAT('.TABLE_ACCOUNTS.'.first_name, \' \', '.TABLE_ACCOUNTS.'.last_name) as user_name
				FROM '.TABLE_ACCOUNTS.'
				WHERE 1';
		$result = database_query($sql, DATA_AND_ROWS, ALL_ROWS);
		if(!empty($result[1])){
			for($i = 0; $i < $result[1]; $i++){
				$arr_users[$result[0][$i]['id']] = $result[0][$i]['user_name'];
			}
		}
		// define filtering fields
		$this->arrFilteringFields = array(
			_USER_NAME  => array('table'=>$this->tableName, 'field'=>'account_id', 'type'=>'dropdownlist', 'source'=>$arr_users, 'sign'=>'=', 'width'=>'130px'),
		);

		// prepare languages array		
		/// $total_languages = Languages::GetAllActive();
		/// $arr_languages      = array();
		/// foreach($total_languages[0] as $key => $val){
		/// 	$arr_languages[$val['abbreviation']] = $val['lang_name'];
		/// }

		//---------------------------------------------------------------------- 
		// VIEW MODE
		//---------------------------------------------------------------------- 
		$this->VIEW_MODE_SQL = 'SELECT '.$this->tableName.'.'.$this->primaryKey.',
									'.$this->tableName.'.account_id,
									'.$this->tableName.'.email_to,
									'.$this->tableName.'.date_created,
									'.TABLE_EMAIL_TEMPLATES.'.template_name
								FROM '.$this->tableName.'
								LEFT JOIN '.TABLE_EMAIL_TEMPLATES.' ON '.TABLE_EMAIL_TEMPLATES.'.template_code = '.$this->tableName.'.email_template_code AND '.TABLE_EMAIL_TEMPLATES.'.language_id = \''.$this->languageId.'\'';
		// define view mode fields
		$this->arrViewModeFields = array(
			'account_id'    => array('title'=>_USER_NAME, 'type'=>'enum', 'align'=>'left', 'width'=>'120px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$arr_users),
			'email_to'		=> array('title'=>_EMAIL_TO, 'type'=>'enum', 'align'=>'left', 'width'=>'120px', 'sortable'=>'true', 'nowrap'=>'', 'visible'=>true, 'source'=>$arr_email_types),
			'template_name' => array('title'=>_EMAIL_TEMPLATES, 'type'=>'label', 'align'=>'left', 'width'=>'', 'height'=>'', 'maxlength'=>''),
			'date_created'  => array('title'=>_DATE_CREATED, 'type'=>'label', 'align'=>'center', 'width'=>'110px', 'format'=>'date', 'format_parameter'=>$datetime_format),
		);
		
		//---------------------------------------------------------------------- 
		// ADD MODE
		// Validation Type: alpha|numeric|float|alpha_numeric|text|email
		// Validation Sub-Type: positive (for numeric and float)
		// Ex.: 'validation_type'=>'numeric', 'validation_type'=>'numeric|positive'
		//---------------------------------------------------------------------- 
		// define add mode fields
		//$this->arrAddModeFields = array(		    
		//	'ban_item'      => array('title'=>_BAN_ITEM, 'type'=>'textbox',  'width'=>'210px', 'required'=>true, 'readonly'=>false, 'unique'=>true, 'maxlength'=>'70', 'default'=>'', 'validation_type'=>$item_validation_type),
		//	'ban_item_type' => array('title'=>_TYPE, 'type'=>'enum',     'required'=>true, 'readonly'=>false, 'width'=>'130px', 'source'=>$arr_ban_types),
		//	'ban_reason'    => array('title'=>_REASON, 'type'=>'textarea', 'width'=>'310px', 'height'=>'90px', 'required'=>false, 'maxlength'=>'255', 'validation_maxlength'=>'255', 'readonly'=>false, 'default'=>'Spam from this IP/Email', 'validation_type'=>''),
		//);

		//---------------------------------------------------------------------- 
		// EDIT MODE
		// Validation Type: alpha|numeric|float|alpha_numeric|text|email
		// Validation Sub-Type: positive (for numeric and float)
		// Ex.: 'validation_type'=>'numeric', 'validation_type'=>'numeric|positive'
		//---------------------------------------------------------------------- 
		//$this->EDIT_MODE_SQL = 'SELECT
		//						'.$this->tableName.'.'.$this->primaryKey.',
		//						'.$this->tableName.'.ban_item,
		//						'.$this->tableName.'.ban_item_type,
		//						'.$this->tableName.'.ban_reason
		//					FROM '.$this->tableName.'
		//					WHERE '.$this->tableName.'.'.$this->primaryKey.' = _RID_';		
		//// define edit mode fields
		//$this->arrEditModeFields = array(		
		//	'ban_item'      => array('title'=>_BAN_ITEM, 'type'=>'textbox',  'width'=>'210px', 'required'=>true, 'readonly'=>false, 'unique'=>true, 'maxlength'=>'70', 'default'=>'', 'validation_type'=>$item_validation_type),
		//	'ban_item_type' => array('title'=>_TYPE, 'type'=>'enum',     'required'=>true, 'readonly'=>false, 'width'=>'130px', 'source'=>$arr_ban_types),
		//	'ban_reason'    => array('title'=>_REASON, 'type'=>'textarea', 'width'=>'310px', 'height'=>'90px', 'required'=>false, 'maxlength'=>'255', 'validation_maxlength'=>'255', 'readonly'=>false, 'default'=>'Spam from this IP/Email', 'validation_type'=>''),
		//);

		////---------------------------------------------------------------------- 
		//// DETAILS MODE
		////----------------------------------------------------------------------
		//$this->DETAILS_MODE_SQL = $this->EDIT_MODE_SQL;
		//$this->arrDetailsModeFields = array(
		//	'ban_item'  	=> array('title'=>_BAN_ITEM, 'type'=>'label'),
		//	'ban_item_type' => array('title'=>_TYPE, 'type'=>'label'),
		//	'ban_reason'    => array('title'=>_REASON, 'type'=>'label'),
		//);

	}
	
	//==========================================================================
    // Class Destructor
	//==========================================================================
    function __destruct()
	{
		// echo 'this object has been destroyed';
    }
	
	//==========================================================================
    // Static Methods
	//==========================================================================
	/**
	 * Add new records
	 */
	public static function MassMailLogAddRecord($account_id = 0, $email_to = 'admins', $email_template_code = 'test_template')
	{
		if(!empty($account_id) && is_numeric($account_id)){
			$date_created = date('Y-m-d H:i:s');
			$sql_create = 'INSERT INTO '.TABLE_MASS_MAIL_LOGS.'(id, account_id, email_to, email_template_code, date_created) VALUES (NULL, \''.$account_id.'\', \''.$email_to.'\', \''.$email_template_code.'\', \''.$date_created.'\')';
			database_void_query($sql_create);
			return true;
		}
		return false;
	}
}
