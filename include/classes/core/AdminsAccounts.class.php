<?php

/**
 *	Class AdminsAccounts
 *  -------------- 
 *  Description : encapsulates Admins Accounts operations & properties
 *	Written by  : ApPHP
 *	Version     : 1.0.5
 *  Updated	    : 22.02.2016
 *	Usage       : Core (excepting MicroBlog)
 *	Differences : $PROJECT
 *	
 *	PUBLIC:				  	STATIC:				 	PRIVATE:
 * 	------------------	  	---------------     	---------------
 *	__construct                                     SetCompaniesViewState
 *	__destruct
 *	AfterInsertRecord
 *	AfterUpdateRecord
 *	AfterAddRecord
 *	AfterEditRecord
 *	AfterDetailsMode
 *	BeforeEditRecord
 *	BeforeDetailsRecord
 *
 *  1.0.5
 *      - changes in $login_type
 *      - added $page
 *      - ALTER TABLE  `<DB_PREFIX>accounts` CHANGE  `hotels`  `companies` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT  '';
 *      -
 *      -
 *  1.0.4
 *      - added SetLocale()
 *      - added default value for preferred lang
 *      - added username and passwrod generators
 *      - added '{ACCOUNT TYPE}' => 'admin'
 *      - added blank values for some datetime fields
 *  1.0.3
 *      - SQL _YES/_NO replaced with "enum"
 *      - last_login redone with date_lastlogin
 *      - added $PROJECT + changes for HotelSite
 *      - added $accountTypeOnChange
 *      - added AfterAddRecord/AfterEditRecord/AfterDetailsMode for HotelSite
 *  1.0.2
 *      - replaced " with '
 *      - improved working with #arr_languages
 *      - added patient=login
 *      - added sending emails in preferred language
 *      - removed clients for HotelSite
 *  1.0.1
 *      - added date_created, last_login labels in Edit Mode
 *      - get_date_format() -> get_datetime_format()
 *      - removed unnded 0000-00-00 from datetime fields
 *      - added maxlength for all fields
 *      - changed AfterInsertRecord send_email()
 *	
 **/

class AdminsAccounts extends MicroGrid {
	
	protected $debug = false;
	
	private $arrCompanies = array(); /* used to show companies where admin is owner */
	private $arrCompaniesSelected = array(); /* used to show selected companies (add mode) where admin is owner */
	private $additionalFields = '';
	private $accountTypeOnChange = '';
	private $sqlFieldDatetimeFormat = '';

	//------------------------------
	// MicroCMS, HotelSite, ShoppingCart, BusinessDirectory, MedicalAppointments
	private static $PROJECT = PROJECT_NAME;

	//==========================================================================
    // Class Constructor
	//	@param $login_type
	//	@param $page
	//==========================================================================
	function __construct($login_type = '', $page = '')	
	{
		parent::__construct();
		
		global $objSettings;

		$this->params = array();		
		if(isset($_POST['first_name'])) 	$this->params['first_name'] = prepare_input($_POST['first_name']);
		if(isset($_POST['last_name']))		$this->params['last_name']  = prepare_input($_POST['last_name']);
		if(isset($_POST['user_name']))  	$this->params['user_name']  = prepare_input($_POST['user_name']);
		if(isset($_POST['password']))		$this->params['password']   = prepare_input($_POST['password']);
		if(isset($_POST['email']))   		$this->params['email']      = prepare_input($_POST['email']);
		if(isset($_POST['preferred_language']))  $this->params['preferred_language'] = prepare_input($_POST['preferred_language']);
		if(isset($_POST['account_type']))   $this->params['account_type'] = prepare_input($_POST['account_type']);
		if(isset($_POST['date_created']))   $this->params['date_created'] = prepare_input($_POST['date_created']);
		if(isset($_POST['is_active']))      $this->params['is_active']    = (int)$_POST['is_active']; else $this->params['is_active'] = '0';
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if(isset($_POST['companies']))     $this->params['companies'] = prepare_input($_POST['companies']);
		} 
		
		$this->primaryKey 	= 'id';
		$this->tableName 	= TABLE_ACCOUNTS;
		$this->dataSet 		= array();
		$this->error 		= '';
		$this->page			= $page;
		
		$this->formActionURL = 'index.php?admin=admins_management';
		if($page == 'hotel_admins'){
			$this->formActionURL = 'index.php?admin=hotel_owners_management';
		}else if($page == 'agency_admins'){
			$this->formActionURL = 'index.php?admin=mod_car_rental_owners_management';
		}
		
		$this->actions      = array('add'=>true, 'edit'=>true, 'details'=>true, 'delete'=>true);
		$this->actionIcons  = true;
		$this->allowRefresh = true;

		$this->allowLanguages = false;

		$this->WHERE_CLAUSE = '';
		if($login_type == 'owner' || $login_type == 'mainadmin'){
			if($page == 'hotel_admins'){
				$this->WHERE_CLAUSE = "WHERE ".TABLE_ACCOUNTS.".account_type = 'hotelowner'";	
			}else if($page == 'agency_admins'){
				$this->WHERE_CLAUSE = "WHERE ".TABLE_ACCOUNTS.".account_type = 'agencyowner'";
			}else if($login_type == 'owner'){
				$this->WHERE_CLAUSE = "WHERE (".TABLE_ACCOUNTS.".account_type IN('mainadmin', 'admin'))";
			}else if($login_type == 'mainadmin'){
				$this->WHERE_CLAUSE = "WHERE (".TABLE_ACCOUNTS.".account_type IN('admin'))";
			}
		}else if($login_type == 'admin'){
			$this->WHERE_CLAUSE = "WHERE ".TABLE_ACCOUNTS.".account_type = 'admin'";
		}else if($login_type == 'hotelowner'){
			$this->WHERE_CLAUSE = "WHERE ".TABLE_ACCOUNTS.".account_type = 'hotelowner'";
		}else if($login_type == 'agencyowner'){
			$this->WHERE_CLAUSE = "WHERE ".TABLE_ACCOUNTS.".account_type = 'agencyowner'";
		}

		$this->ORDER_CLAUSE = 'ORDER BY id ASC';
		$this->isAlterColorsAllowed = true;
		$this->isPagingAllowed = true;
		$this->pageSize = 20;

		$this->isSortingAllowed = true;
		
		$this->isFilteringAllowed = true;
		// define filtering fields
		$this->arrFilteringFields = array(
			_FIRST_NAME => array('table'=>$this->tableName, 'field'=>'first_name', 'type'=>'text', 'sign'=>'like%', 'width'=>'80px'),
			_LAST_NAME  => array('table'=>$this->tableName, 'field'=>'last_name', 'type'=>'text', 'sign'=>'like%', 'width'=>'80px'),
			_EMAIL      => array('table'=>$this->tableName, 'field'=>'email', 'type'=>'text', 'sign'=>'like%', 'width'=>'100px'),
			_ACTIVE     => array('table'=>$this->tableName, 'field'=>'is_active', 'type'=>'dropdownlist', 'source'=>array('0'=>_NO, '1'=>_YES), 'sign'=>'=', 'width'=>'85px'),
		);

		// prepare languages array		
		$total_languages = Languages::GetAllActive();
		$arr_languages   = array();
		foreach($total_languages[0] as $key => $val){
			$arr_languages[$val['abbreviation']] = $val['lang_name'];
		}
		
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if($page == 'hotel_admins'){
				$arr_account_types['hotelowner'] = _HOTEL_OWNER;
			}else if($page == 'agency_admins'){
				$arr_account_types['agencyowner'] = _CAR_AGENCY_OWNER;
			}else{
				$arr_account_types = array('admin'=>_ADMIN, 'mainadmin'=>_MAIN_ADMIN);	
			}
		}else{
			$arr_account_types = array('admin'=>_ADMIN, 'mainadmin'=>_MAIN_ADMIN);	
		}
		
		$arr_is_active = array('0'=>'<span class=no>'._NO.'</span>', '1'=>'<span class=yes>'._YES.'</span>');
		$datetime_format = get_datetime_format();
		
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if($page == 'hotel_admins'){
				$companies = Hotels::GetAllActive();
				$total_companies = isset($companies[0]) ? $companies[0] : array();
			}else if($page == 'agency_admins'){
				$companies = CarAgencies::GetAllActive();
				$total_companies = isset($companies[0]) ? $companies[0] : array();
			}else{
				$total_companies = array();
			}
			
			if($page == 'hotel_admins'){
				if(isset($this->params['companies'])){
					foreach($total_companies as $key => $val){
						if(in_array($val['id'], $this->params['companies'])){
							$this->arrCompaniesSelected[$val['id']] = $val['name'];
						}
					}
				}
			}

			foreach($total_companies as $key => $val){
				$this->arrCompanies[$val['id']] = $val['name'];
			}

			$this->additionalFields = ', companies';
			$this->accountTypeOnChange = 'onchange="javascript:AccountType_OnChange(this.value)"';
		}

		if($objSettings->GetParameter('date_format') == 'mm/dd/yyyy'){
			$this->sqlFieldDatetimeFormat = '%b %d, %Y %H:%i';
		}else{
			$this->sqlFieldDatetimeFormat = '%d %b, %Y %H:%i';
		}
		$this->SetLocale(Application::Get('lc_time_name'));

		$autocomplete_companies = '<script>
		jQuery(document).ready(function(){
			var companiesPointAdd = null;
			jQuery("#autocomplete_companies").autocomplete({
				source: function(request, response){
					var token = "'.htmlentities(Application::Get('token')).'";
					jQuery.ajax({
						url: "ajax/hotel.ajax.php",
						global: false,
						type: "POST",
						data: ({
							token: token,
							act: "send",
							lang: "'.htmlentities(Application::Get('lang')).'",
							search : jQuery("#autocomplete_companies").val(),
						}),
						dataType: "json",
						async: true,
						error: function(html){
							console.log("AJAX: cannot connect to the server or server response error! Please try again later.");
						},
						success: function(data){
							if(data.length == 0){
								response({ label: "'.htmlentities(_NO_MATCHES_FOUND).'" });
							}else{
								response(jQuery.map(data, function(item){
									return{ hotel_name: item.hotel_name, hotel_id: item.hotel_id, label: item.label }
								}));
							}
						}
					});
				},
				minLength: 2,
				select: function(event, ui) {
					if(typeof(ui.item.hotel_id) != "undefined" && jQuery("#group_companies" + ui.item.hotel_id).length == 0){
						var idInput = "";
						var newInput = "";
						if(companiesPointAdd == null){
							idInput = "companies1";
							var tmpInput = jQuery("input[name=\'companies[]\']").last();
							if(jQuery("input[name=\'companies[]\']").length == 1){
								newInput = jQuery("<input id=\'" + idInput + "\' type=\'checkbox\' name=\'companies[]\' value=\'" + ui.item.hotel_id + "\' checked=\'checked\'/></input>");
							}else{
								jQuery("input[name=\'companies[]\']").each(function(a,b){var id = jQuery(b).val(); jQuery(b).parent().attr("id", "group_companies" + id);});
								if(jQuery("#group_companies" + ui.item.hotel_id).length != 0){
									jQuery("#autocomplete_companies").val("");
									return false;
								}
								idInput = "companies" + (jQuery("input[name=\'companies[]\']").length + 1);
								newInput = jQuery("<input id=\'" + idInput + "\' type=\'checkbox\' name=\'companies[]\' value=\'" + ui.item.hotel_id + "\' checked=\'checked\'/></input>");
								tmpInput.parent().after(newInput);
							}
							tmpInput.after(newInput);
							tmpInput.remove();
						}else{
							idInput = "companies" + (companiesPointAdd.parent().children("div").length + 1);
							newInput = jQuery("<input id=\'" + idInput + "\' type=\'checkbox\' name=\'companies[]\' value=\'" + ui.item.hotel_id + "\' checked=\'checked\'></input>");
							companiesPointAdd.after(newInput);
						}
						// Find the item in the DOM
						newInput = jQuery("#" + idInput);
						newInput.wrap("<div id=\'group_companies" + ui.item.hotel_id + "\' style=\'float:left;width:220px;\'></div>");
						newInput.after("<label for=\'" + idInput + "\'>" + ui.item.hotel_name + "</label>")
						companiesPointAdd = jQuery("#group_companies" + ui.item.hotel_id);
					}

					jQuery("#autocomplete_companies").val("");
					return false;
				}
			});
		});
		</script>';
		
		//---------------------------------------------------------------------- 
		// VIEW MODE
		//---------------------------------------------------------------------- 
		$this->VIEW_MODE_SQL = 'SELECT '.$this->primaryKey.',
									first_name,
		                            last_name,
									CONCAT(first_name, \' \', last_name) as full_name,
									user_name,
									email,
									preferred_language,
									account_type,
									IF(date_lastlogin = "0000-00-00 00:00:00", "<span class=gray>- never -</span>", DATE_FORMAT(date_lastlogin, \''.$this->sqlFieldDatetimeFormat.'\')) as date_lastlogin,
									is_active
									'.$this->additionalFields.'
								FROM '.$this->tableName;		
		// define view mode fields
		$this->arrViewModeFields = array(
			'full_name'   => array('title'=>_NAME, 'type'=>'label', 'align'=>'left', 'width'=>''),
			'email' 	  => array('title'=>_EMAIL_ADDRESS, 'type'=>'link', 'maxlength'=>'35', 'href'=>'mailto:{email}', 'align'=>'left', 'width'=>'250px'),
			'user_name'   => array('title'=>_USER_NAME,  'type'=>'label', 'align'=>'left', 'width'=>'150px'),
			'account_type' => array('title'=>_ACCOUNT_TYPE, 'type'=>'enum',  'align'=>'center', 'width'=>'120px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$arr_account_types),
			'is_active'   => array('title'=>_ACTIVE, 'type'=>'enum',  'align'=>'center', 'width'=>'80px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$arr_is_active),
			'date_lastlogin'  => array('title'=>_LAST_LOGIN, 'type'=>'label', 'align'=>'center', 'width'=>'110px', 'format'=>'date', 'format_parameter'=>$datetime_format),
			'id'          => array('title'=>'ID', 'type'=>'label', 'align'=>'center', 'width'=>'40px'),
		);
		
		//---------------------------------------------------------------------- 
		// ADD MODE
		//---------------------------------------------------------------------- 
		// define add mode fields
		$this->arrAddModeFields = array(
		    'separator_1'   =>array(
				'separator_info' => array('legend'=>_PERSONAL_DETAILS),
				'first_name'  	 => array('title'=>_FIRST_NAME,	'type'=>'textbox', 'width'=>'210px', 'required'=>true, 'maxlength'=>'32', 'validation_type'=>'text'),
				'last_name'    	 => array('title'=>_LAST_NAME, 	'type'=>'textbox', 'width'=>'210px', 'required'=>true, 'maxlength'=>'32', 'validation_type'=>'text'),
				'email' 		 => array('title'=>_EMAIL_ADDRESS,'type'=>'textbox', 'width'=>'210px', 'required'=>true, 'maxlength'=>'70', 'validation_type'=>'email', 'unique'=>true),
			),
		    'separator_2'   =>array(
				'separator_info' => array('legend'=>_ACCOUNT_DETAILS),
				'user_name'  	 => array('title'=>_USER_NAME,	'type'=>'textbox', 'width'=>'210px', 'required'=>true, 'maxlength'=>'32', 'validation_type'=>'alpha_numeric', 'unique'=>true, 'username_generator'=>true),
				'password'  	 => array('title'=>_PASSWORD, 	'type'=>'password', 'width'=>'210px', 'required'=>true, 'maxlength'=>'32', 'validation_type'=>'password', 'cryptography'=>PASSWORDS_ENCRYPTION, 'cryptography_type'=>PASSWORDS_ENCRYPTION_TYPE, 'aes_password'=>PASSWORDS_ENCRYPT_KEY, 'password_generator'=>true),
				'account_type'   => array('title'=>_ACCOUNT_TYPE, 'type'=>'enum', 'required'=>true, 'readonly'=>false, 'width'=>'120px', 'source'=>$arr_account_types, 'javascript_event'=>$this->accountTypeOnChange),
				'preferred_language' => array('title'=>_PREFERRED_LANGUAGE, 'type'=>'enum', 'required'=>true, 'readonly'=>false, 'width'=>'120px', 'default'=>Application::Get('lang'), 'source'=>$arr_languages),
			),
		    'separator_3'   =>array(
				'separator_info' => array('legend'=>_OTHER),
				'date_lastlogin' => array('title'=>'',      'type'=>'hidden',  'required'=>false, 'default'=>''),
				'date_created' 	 => array('title'=>'',      'type'=>'hidden',  'required'=>false, 'default'=>date('Y-m-d H:i:s')),
				'is_active'  	 => array('title'=>_ACTIVE,	'type'=>'checkbox', 'readonly'=>false, 'default'=>'1', 'true_value'=>'1', 'false_value'=>'0', 'unique'=>false),
			)
		);
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if($page == 'hotel_admins'){
				$this->arrAddModeFields['separator_3']['autocomplete_companies'] = array('title'=>_EX_HOTEL_OR_LOCATION , 'type'=>'textbox', 'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'javascript_event'=>'', 'post_html'=>$autocomplete_companies);
			}
			$this->arrAddModeFields['separator_3']['companies'] = array('title'=>($page == 'hotel_admins') ? _HOTELS : _CAR_AGENCIES, 'type'=>'enum',  'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'source'=>$page == 'hotel_admins' ? $this->arrCompaniesSelected : $this->arrCompanies, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>'', 'view_type'=>'checkboxes', 'multi_select'=>true);
		} 


		//---------------------------------------------------------------------- 
		// EDIT MODE
		//---------------------------------------------------------------------- 
		$this->EDIT_MODE_SQL = 'SELECT
								'.$this->tableName.'.'.$this->primaryKey.',
								'.$this->tableName.'.first_name,
								'.$this->tableName.'.last_name,
								'.$this->tableName.'.user_name,
								'.$this->tableName.'.password,
								'.$this->tableName.'.email,
								'.$this->tableName.'.account_type,
								'.$this->tableName.'.preferred_language,
								IF('.$this->tableName.'.date_created = "0000-00-00 00:00:00", "<span class=gray>- unknown -</span>", DATE_FORMAT('.$this->tableName.'.date_created, \''.$this->sqlFieldDatetimeFormat.'\')) as date_created,
								IF('.$this->tableName.'.date_lastlogin = "0000-00-00 00:00:00", "<span class=gray>- never -</span>", DATE_FORMAT('.$this->tableName.'.date_lastlogin, \''.$this->sqlFieldDatetimeFormat.'\')) as date_lastlogin,
								'.$this->tableName.'.is_active
								'.$this->additionalFields.'
							FROM '.$this->tableName.'
							WHERE '.$this->tableName.'.'.$this->primaryKey.' = _RID_';		
		// define edit mode fields
		$this->arrEditModeFields = array(
		    'separator_1'   =>array(
				'separator_info' => array('legend'=>_PERSONAL_DETAILS),
				'first_name'  	 => array('title'=>_FIRST_NAME,	'type'=>'textbox', 'width'=>'210px', 'maxlength'=>'32', 'required'=>true, 'validation_type'=>'text'),
				'last_name'    	 => array('title'=>_LAST_NAME, 	'type'=>'textbox', 'width'=>'210px', 'maxlength'=>'32', 'required'=>true, 'validation_type'=>'text'),
				'email' 		 => array('title'=>_EMAIL_ADDRESS,'type'=>'textbox', 'width'=>'210px', 'maxlength'=>'70', 'required'=>true, 'validation_type'=>'email', 'unique'=>true),
			),
		    'separator_2'   =>array(
				'separator_info' => array('legend'=>_ACCOUNT_DETAILS),
				'user_name'  	 => array('title'=>_USER_NAME,	'type'=>'textbox', 'width'=>'210px', 'maxlength'=>'32', 'required'=>true, 'readonly'=>true, 'validation_type'=>'alpha_numeric', 'unique'=>true),
				'account_type'   => array('title'=>_ACCOUNT_TYPE, 'type'=>'enum', 'width'=>'120px', 'required'=>true, 'maxlength'=>'32', 'readonly'=>(($login_type == 'owner')?false:true), 'source'=>$arr_account_types, 'javascript_event'=>$this->accountTypeOnChange),
				'preferred_language' => array('title'=>_PREFERRED_LANGUAGE, 'type'=>'enum', 'width'=>'120px', 'required'=>true, 'readonly'=>false, 'source'=>$arr_languages),
			),
		    'separator_3'   =>array(
				'separator_info'   => array('legend'=>_OTHER),
				'date_created' => array('title'=>_DATE_CREATED, 'type'=>'label'),
				'date_lastlogin'  => array('title'=>_LAST_LOGIN, 'type'=>'label'),
				'is_active'  	   => array('title'=>_ACTIVE, 'type'=>'checkbox', 'true_value'=>'1', 'false_value'=>'0'),
			)
		);
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if($page == 'hotel_admins'){
				$this->arrEditModeFields['separator_3']['autocomplete_companies'] = array('title'=>_EX_HOTEL_OR_LOCATION , 'type'=>'textbox', 'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'javascript_event'=>'', 'post_html'=>$autocomplete_companies);
			}
			$this->arrEditModeFields['separator_3']['companies'] = array('title'=>($page == 'hotel_admins') ? _HOTELS : _CAR_AGENCIES, 'type'=>'enum',  'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'source'=>$this->arrCompanies, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>'', 'view_type'=>'checkboxes', 'multi_select'=>true);
		} 

		//---------------------------------------------------------------------- 
		// DETAILS MODE
		//----------------------------------------------------------------------
		$this->DETAILS_MODE_SQL = 'SELECT
								'.$this->tableName.'.'.$this->primaryKey.',
								'.$this->tableName.'.first_name,
								'.$this->tableName.'.last_name,
								'.$this->tableName.'.user_name,
								'.$this->tableName.'.password,
								'.$this->tableName.'.email,
								'.$this->tableName.'.preferred_language,
								'.$this->tableName.'.account_type,
								IF('.$this->tableName.'.date_created = "0000-00-00 00:00:00", "<span class=gray>- unknown -</span>", DATE_FORMAT('.$this->tableName.'.date_created, \''.$this->sqlFieldDatetimeFormat.'\')) as date_created,
								IF('.$this->tableName.'.date_lastlogin = "0000-00-00 00:00:00", "<span class=gray>- never -</span>", DATE_FORMAT('.$this->tableName.'.date_lastlogin, \''.$this->sqlFieldDatetimeFormat.'\')) as date_lastlogin,
								'.$this->tableName.'.is_active
								'.$this->additionalFields.'
							FROM '.$this->tableName.'
							WHERE '.$this->tableName.'.'.$this->primaryKey.' = _RID_';		
		$this->arrDetailsModeFields = array(
		    'separator_1'   =>array(
				'separator_info' => array('legend'=>_PERSONAL_DETAILS),
				'first_name'  	=> array('title'=>_FIRST_NAME,	'type'=>'label'),
				'last_name'    	=> array('title'=>_LAST_NAME, 'type'=>'label'),
				'email'     	=> array('title'=>_EMAIL_ADDRESS, 	 'type'=>'label'),
			),
		    'separator_2'   =>array(
				'separator_info' => array('legend'=>_ACCOUNT_DETAILS),
				'user_name'   	=> array('title'=>_USER_NAME, 'type'=>'label'),
				'account_type'  => array('title'=>_ACCOUNT_TYPE, 'type'=>'enum', 'source'=>$arr_account_types),
				'preferred_language' => array('title'=>_PREFERRED_LANGUAGE, 'type'=>'enum', 'source'=>$arr_languages),
			),
		    'separator_3'   =>array(
				'separator_info' => array('legend'=>_OTHER),
				'date_created'   => array('title'=>_DATE_CREATED, 'type'=>'label'),
				'date_lastlogin' => array('title'=>_LAST_LOGIN, 'type'=>'label'),
				'is_active'  	 => array('title'=>_ACTIVE, 'type'=>'enum', 'source'=>$arr_is_active),
			)
		);
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if($page == 'hotel_admins'){
				$this->arrAddModeFields['separator_3']['autocomplete_companies'] = array('title'=>_EX_HOTEL_OR_LOCATION , 'type'=>'textbox', 'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'javascript_event'=>'', 'post_html'=>$autocomplete_companies);
			}
			$this->arrDetailsModeFields['separator_3']['companies'] = array('title'=>($page == 'hotel_admins') ? _HOTELS : _CAR_AGENCIES, 'type'=>'enum',  'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'source'=>$this->arrCompanies, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>'', 'view_type'=>'checkboxes', 'multi_select'=>true);
		} 
	}
	
	//==========================================================================
    // Class Destructor
	//==========================================================================
    function __destruct()
	{
		// echo 'this object has been destroyed';
    }

	/**
	 * After-Addition operation
	 */
	public function AfterInsertRecord()
	{
		global $objSettings, $objSiteDescription;
		
		////////////////////////////////////////////////////////////
		send_email(
			$this->params['email'],
			$objSettings->GetParameter('admin_email'),
			'new_account_created_by_admin',
			array(
				'{FIRST NAME}'   => $this->params['first_name'],
				'{LAST NAME}'    => $this->params['last_name'],
				'{USER NAME}'    => $this->params['user_name'],
				'{USER PASSWORD}' => $this->params['password'],
				'{WEB SITE}'     => $_SERVER['SERVER_NAME'],
				'{BASE URL}'     => APPHP_BASE,
				'{YEAR}'         => date('Y'),
				'customer=login' => 'admin=login',
				'user=login'     => 'admin=login',
				'patient=login'  => 'admin=login',
				'{ACCOUNT TYPE}' => 'admin'
			),
			$this->params['preferred_language']
		);
		////////////////////////////////////////////////////////////
	}

	/**
	 * After-Updating operation
	 */
	public function AfterUpdateRecord()
	{
		global $objLogin;		
		$objLogin->UpdateLoggedEmail($this->params['email']);
	}
	
	/**
	 * After drawing Add Mode
	 */
	public function AfterAddRecord()
	{
		$this->SetCompaniesViewState();
	}
	
	/**
	 * After drawing Edit Mode
	 */
	public function AfterEditRecord()
	{
		$this->SetCompaniesViewState();
	}
	
	/**
	 * After drawing Details Mode
	 */
	public function AfterDetailsMode()
	{
		if(isset($this->result[0][0]['account_type']) && ! in_array($this->result[0][0]['account_type'], array('hotelowner', 'agencyowner'))) $this->SetCompaniesViewState(true);
	}
	
	/**
	 * Before drawing Edit Mode
	 */
	public function BeforeEditRecord()
	{
		$sql = 'SELECT
			'.$this->tableName.'.id,
			'.$this->tableName.'.companies
		FROM '.$this->tableName.'
		WHERE '.$this->tableName.'.id = '.(int)$this->curRecordId;
		$account = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($account[1] > 0){
			$hotels = @unserialize($account[0]['companies']);
			if(!empty($hotels)){
				$where = TABLE_HOTELS.'.id IN ('.implode(',',$hotels).')';
				$arr_hotels = Hotels::GetAllActive($where);
				if($arr_hotels[1] > 0){
					foreach($arr_hotels[0] as $key => $val){
						$arr_companies[$val['id']] = $val['name'];
					}
				}
				$this->arrEditModeFields['separator_3']['companies'] = array('title'=>($this->page == 'hotel_admins') ? _HOTELS : _CAR_AGENCIES, 'type'=>'enum',  'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'source'=>$arr_companies, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>'', 'view_type'=>'checkboxes', 'multi_select'=>true);
			}
		}

		return true;
	}
	
	/**
	 * Before drawing Details Mode
	 */
	public function BeforeDetailsRecord()
	{
		$sql = 'SELECT
			'.$this->tableName.'.id,
			'.$this->tableName.'.companies
		FROM '.$this->tableName.'
		WHERE '.$this->tableName.'.id = '.(int)$this->curRecordId;
		$account = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($account[1] > 0){
			$hotels = @unserialize($account[0]['companies']);
			if(!empty($hotels)){
				$where = TABLE_HOTELS.'.id IN ('.implode(',',$hotels).')';
				$arr_hotels = Hotels::GetAllActive($where);
				if($arr_hotels[1] > 0){
					foreach($arr_hotels[0] as $key => $val){
						$arr_companies[$val['id']] = $val['name'];
					}
				}
				$this->arrDetailsModeFields['separator_3']['companies'] = array('title'=>($this->page == 'hotel_admins') ? _HOTELS : _CAR_AGENCIES, 'type'=>'enum',  'width'=>'', 'required'=>false, 'readonly'=>false, 'default'=>'', 'source'=>$arr_companies, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>'', 'view_type'=>'checkboxes', 'multi_select'=>true);
			}
		}

		return true;
	}
	
	/**
	 * Set companies view state
	 * 		@param $force_hidding
	 */
	private function SetCompaniesViewState($force_hidding = false)
	{
		if(in_array(self::$PROJECT, array('HotelSite', 'HotelBooking'))){
			if($force_hidding){
				echo '<script type="text/javascript">jQuery("#mg_row_autocomplete_companies").hide();jQuery("#mg_row_companies").hide();</script>';
			}else{
				echo '<script type="text/javascript">if(jQuery("#account_type").val() != "hotelowner" && jQuery("#account_type").val() != "agencyowner"){ jQuery("#mg_row_autocomplete_companies").hide();jQuery("#mg_row_companies").hide(); }</script>';
			}			
		}				
	}
	
}
