<?php

/**
 *	Class CarRental
 *  -------------- 
 *  Description : encapsulates bookings properties
 *	Usage       : HotelBooking ONLY
 *  Updated	    : 11.02.2016
 *	Written by  : ApPHP
 *
 *	PUBLIC:						STATIC:						PRIVATE:                    PROTECTED
 *  -----------					-----------					-----------                 -------------------
 * __construct					GetReservationInfo
 * __destruct					DrawCheckList
 * DrawCarRentalInvoice
 * GetCarReservationList
 * UpdatePaymentDate
 *  
 **/


class CarRental extends MicroGrid {
	
	protected $debug = false;
	
	//------------------------------
	public $message;
	
	//------------------------------
	private $user_id;
	//private $rooms_amount;
	//private $reservation_number;
	private $reservation_status;
	//private $booking_customer_id;
	//private $revert_sum;
	//private $allow_payment_with_balance;
	//private $fieldDateFormat;
	//private $vat_included_in_price;
	private $online_credit_card_required;
	private $customers_cancel_reservation;
	private $default_currency_info;
	private $currencyFormat;
	private $arr_payment_types;
	private $arr_car_agencies;
	private $arr_agencies_locations;
	private $arr_vehicles;
	private $arr_vehicle_types;
	private $arr_payment_methods;
	private $statuses_vm;
	private $sqlFieldDatetimeFormat = '';
	
	//==========================================================================
    // Class Constructor
	//==========================================================================
	function __construct($user_id = '') {
		
		parent::__construct();
		
		global $objSettings, $objLogin;

		$this->params = array();		
		if(isset($_POST['status']))               $this->params['status'] = prepare_input($_POST['status']);
		if(isset($_POST['additional_info']))      $this->params['additional_info'] = prepare_input($_POST['additional_info']);
		if(isset($_POST['additional_payment']))   $this->params['additional_payment'] = prepare_input($_POST['additional_payment']);

		$this->vat_included_in_price      	= ModulesSettings::Get('car_rental', 'vat_included_in_price');
		$this->online_credit_card_required 	= ModulesSettings::Get('car_rental', 'online_credit_card_required');
		$this->customers_cancel_reservation = ModulesSettings::Get('car_rental', 'customers_cancel_reservation');

		$this->params['language_id'] = MicroGrid::GetParameter('language_id');
		$rid = self::GetParameter('rid');
	
		$this->primaryKey 	= 'id';
		$this->tableName 	= TABLE_CAR_AGENCY_RESERVATIONS;
		
		//////////////////// TIL HERE
		
		$this->dataSet 		= array();
		$this->error 		= '';
		$this->message      = '';
		$this->reservation_number = '';
		$this->reservation_status = '';
		$this->booking_customer_id = '';
		$this->revert_sum = 0;
		$this->allow_payment_with_balance = ModulesSettings::Get('car_rental', 'allow_payment_with_balance') == 'yes' ? true : false;
		
		$arr_statuses = array('0'=>_PREORDERING, '1'=>_PENDING, '2'=>_RESERVED, '3'=>_COMPLETED, '4'=>_REFUNDED);
		$this->statuses_vm = array('0' => '<span style=color:#222222>'._PREORDERING.'</span>',
								 '1' => '<span style=color:#0000a3>'._PENDING.'</span>',
								 '2' => '<span style=color:#a3a300>'._RESERVED.'</span>',
								 '3' => '<span style=color:#00a300>'._COMPLETED.'</span>',
								 '4' => '<span style=color:#660000>'._REFUNDED.'</span>',
								 '5' => '<span style=color:#a30000>'._PAYMENT_ERROR.'!</span>',
								 '6' => '<span style=color:#939393>'._CANCELED.'</span>',
								 '-1' => _UNKNOWN);

		$this->arr_payment_types = array('0'=>_PAY_ON_ARRIVAL, '1'=>_ONLINE, '2'=>_PAYPAL, '3'=>'2CO', '4'=>'Authorize.Net', '5'=>_BANK_TRANSFER, '6'=>_BALANCE, '7'=>_UNKNOWN);
		$this->arr_payment_methods = array('0'=>_PAYMENT_COMPANY_ACCOUNT, '1'=>_CREDIT_CARD, '2'=>_ECHECK, '3'=>_UNKNOWN);
		$allow_editing = $allow_deleting = $allow_cancelation = true;

		if($user_id != ''){
			$this->user_id = $user_id;
			$this->page = 'customer=my_car_rentals';
			$this->actions = array('add'=>false, 'edit'=>false, 'details'=>false, 'delete'=>false);
			$arr_statuses_filter = array('1'=>_PENDING, '2'=>_RESERVED, '3'=>_COMPLETED, '4'=>_REFUNDED);
			$arr_statuses_edit = array('1'=>_PENDING, '2'=>_RESERVED, '3'=>_COMPLETED);
			$arr_statuses_edit_completed = array('2'=>_COMPLETED, '3'=>_REFUNDED);
		}else{
			$this->user_id = '';
			$this->page = 'admin=mod_car_rental_reservations';
			
			$allow_editing = !$objLogin->HasPrivileges('edit_car_reservations') ? false : true;
			$allow_deleting = !$objLogin->HasPrivileges('delete_car_reservations') ? false : true;
			$allow_cancelation = !$objLogin->HasPrivileges('cancel_car_reservations') ? false : true; 
			
			$this->actions = array('add'=>false, 'edit'=>$allow_editing, 'details'=>false, 'delete'=>$allow_deleting);
			$arr_statuses_filter = array('0'=>_PREORDERING, '1'=>_PENDING, '2'=>_RESERVED, '3'=>_COMPLETED, '4'=>_REFUNDED, '5'=>_PAYMENT_ERROR, '6'=>_CANCELED);
			$arr_statuses_edit = array('1'=>_PENDING, '2'=>_RESERVED, '3'=>_COMPLETED, '4'=>_REFUNDED);
			$arr_statuses_edit_completed = array('3'=>_COMPLETED, '4'=>_REFUNDED);
		}
		$this->actionIcons  = true;
		$this->allowRefresh = true;
		$this->formActionURL = 'index.php?'.$this->page;		

		$this->allowLanguages = false;
		$this->languageId  	= ''; // ($this->params['language_id'] != '') ? $this->params['language_id'] : Languages::GetDefaultLang();

		$this->WHERE_CLAUSE = '';
		$agencies_list = '';
		if($this->user_id != ''){
			//'.$this->tableName.'.is_admin_reservation = 0 AND
			$this->WHERE_CLAUSE = 'WHERE '.$this->tableName.'.status <> 0 AND
			                             '.$this->tableName.'.customer_id = '.(int)$this->user_id;
		}else if($objLogin->IsLoggedInAs('agencyowner')){
			$agencies = $objLogin->AssignedToCarAgencies();
			$agencies_list = implode(',', $agencies);
			if(!empty($agencies_list)) $this->WHERE_CLAUSE .= 'WHERE '.$this->tableName.'.car_agency_id IN ('.$agencies_list.') ';
		}
		$this->GROUP_BY_CLAUSE = '';//'GROUP BY '.$this->tableName.'.reservation_number';
		$this->ORDER_CLAUSE = 'ORDER BY '.$this->tableName.'.created_date DESC'; // ORDER BY date_created DESC
		
		$this->isAlterColorsAllowed = true;
		$this->isPagingAllowed = true;
		$this->pageSize = 30;
		$this->isSortingAllowed = true;		
		$this->isExportingAllowed = ($user_id != '') ? false : true;
		$this->arrExportingTypes = array('csv'=>true);
		
		$date_format_settings = get_date_format('view', true);

		// prepare agencies array		
		$total_agencies = CarAgencies::GetAllActive((!empty($agencies_list) ? TABLE_CAR_AGENCIES.'.id IN ('.$agencies_list.')' : ''));
		$this->arr_car_agencies = array();
		foreach($total_agencies[0] as $key => $val){
			$this->arr_car_agencies[$val['id']] = $val['name'];
		}

		// prepare vehicle types array
		$total_vehicle_types = CarAgenciesVehicleTypes::GetAllActive();
		$this->arr_vehicle_types = array();
		foreach($total_vehicle_types[0] as $key => $val){
			$this->arr_vehicle_types[$val['id']] = $val['name'];
		}

		// prepare vehicles array
		$total_vehicles = Vehicles::GetAllActive();
		$this->arr_vehicles = array();
		foreach($total_vehicles[0] as $key => $val){
			$this->arr_vehicles[$val['id']] = $val['make'].' '.$val['model'];
		}
		
		// prepare countries array		
		$total_countries = Countries::GetAllCountries('priority_order DESC, name ASC');
		$arr_countries = array();
		foreach($total_countries[0] as $key => $val){
			$arr_countries[$val['abbrv']] = $val['name'];
		}
		
		// prepare locations array		
		$total_agencies_locations = CarAgenciesLocations::GetAgenciesLocations();
		$this->arr_agencies_locations = array();
		foreach($total_agencies_locations[0] as $key => $val){
			$this->arr_agencies_locations[$val['id']] = $val['name'];
		}		

		$this->default_currency_info = Currencies::GetDefaultCurrencyInfo();
		if($objSettings->GetParameter('date_format') == 'mm/dd/yyyy'){
			$this->fieldDateFormat = 'M d, Y';
			$this->sqlFieldDateFormat = '%b %d, %Y';
			$this->sqlFieldDatetimeFormat = '%b %d, %Y %H:%i';
		}else{
			$this->fieldDateFormat = 'd M, Y';
			$this->sqlFieldDateFormat = '%d %b, %Y';
			$this->sqlFieldDatetimeFormat = '%d %b, %Y %H:%i';
		}
		$this->currencyFormat = get_currency_format();

		$this->isFilteringAllowed = true;
		$this->maxFilteringColumns = 4; 
		// define filtering fields
		$this->arrFilteringFields = array(
			_RESERVATION.' #' => array('table'=>$this->tableName, 'field'=>'reservation_number', 'type'=>'text', 'sign'=>'like%', 'width'=>'90px'),
			_CAR_AGENCY 	  => array('table'=>$this->tableName, 'field'=>'car_agency_id', 'type'=>'dropdownlist', 'source'=>$this->arr_car_agencies, 'sign'=>'=', 'width'=>'', 'visible'=>(($this->user_id != '') ? false : true)),
		);
		
		if(empty($this->user_id)) $this->arrFilteringFields[_CUSTOMER] = array('table'=>TABLE_CUSTOMERS, 'field'=>'last_name', 'type'=>'text', 'sign'=>'like%', 'width'=>'90px');
		if(empty($this->user_id)) $this->arrFilteringFields[_STATUS] = array('table'=>$this->tableName, 'field'=>'status', 'type'=>'dropdownlist', 'source'=>$arr_statuses_filter, 'sign'=>'=', 'width'=>'120px');

		$this->arrFilteringFields[_FROM] = array('table'=>$this->tableName, 'field'=>'date_from', 'type'=>'calendar', 'date_format'=>$date_format_settings, 'sign'=>'>=', 'width'=>'100px', 'visible'=>true);
		$this->arrFilteringFields[_TO] = array('table'=>$this->tableName, 'field'=>'date_to', 'type'=>'calendar', 'date_format'=>$date_format_settings, 'sign'=>'<=', 'width'=>'100px', 'visible'=>true);

		$this->isAggregateAllowed = true;
		// define aggregate fields for View Mode
		$this->arrAggregateFields = array(
			'tp_w_currency' => array('function'=>'SUM', 'align'=>'center', 'aggregate_by'=>'tp_wo_default_currency', 'decimal_place'=>2, 'sign'=>$this->default_currency_info['symbol']),
			//'tpaid_w_currency' => array('function'=>'SUM', 'align'=>'center', 'aggregate_by'=>'tp_wo_default_currency', 'decimal_place'=>2, 'sign'=>$this->default_currency_info['symbol']),
		);

		//---------------------------------------------------------------------- 
		// VIEW MODE
		//----------------------------------------------------------------------

        // set locale time names
		$this->SetLocale(Application::Get('lc_time_name'));
		
		$this->VIEW_MODE_SQL = 'SELECT
								'.$this->tableName.'.'.$this->primaryKey.',
								'.$this->tableName.'.reservation_number,
								'.$this->tableName.'.location_from_id,
								'.$this->tableName.'.location_to_id,
								CONCAT(DATE_FORMAT('.$this->tableName.'.date_from, "'.$this->sqlFieldDateFormat.'"), " ", DATE_FORMAT('.$this->tableName.'.time_from, "%H:%i")) date_from,
								CONCAT(DATE_FORMAT('.$this->tableName.'.date_to, "'.$this->sqlFieldDateFormat.'"), " ", DATE_FORMAT('.$this->tableName.'.time_to, "%H:%i")) date_to,
								CONCAT(DATE_FORMAT('.$this->tableName.'.date_from, "'.$this->sqlFieldDateFormat.'"), " ", DATE_FORMAT('.$this->tableName.'.time_from, "%H:%i"), "<br>", DATE_FORMAT('.$this->tableName.'.date_to, "'.$this->sqlFieldDateFormat.'"), " ", DATE_FORMAT('.$this->tableName.'.time_to, "%H:%i")) date_from_to,
								'.$this->tableName.'.car_agency_id,
								'.$this->tableName.'.car_vehicle_type_id,
								'.$this->tableName.'.car_vehicle_id,
								DATE_FORMAT('.$this->tableName.'.created_date, "'.$this->sqlFieldDateFormat.'") as created_date_formated,
								'.$this->tableName.'.customer_id,
								IF('.$this->tableName.'.is_admin_reservation = 0, CONCAT("<a href=\"javascript:void(\'customer|view\')\" onclick=\"open_popup(\'popup.ajax.php\',\'customer\',\'",'.$this->tableName.'.customer_id,"\',\'",'.TABLE_CUSTOMERS.'.customer_type,"\',\''.Application::Get('token').'\')\">", CONCAT('.TABLE_CUSTOMERS.'.last_name, " ", '.TABLE_CUSTOMERS.'.first_name, " (", '.TABLE_CUSTOMERS.'.b_country, ")"), "</a>"), "{administrator}") as customer_name,
								'.$this->tableName.'.payment_type,
								'.$this->tableName.'.payment_method,
								IF('.$this->tableName.'.status > 6, -1, '.$this->tableName.'.status) as status,
								'.TABLE_CURRENCIES.'.symbol,
								CASE
									WHEN '.TABLE_CURRENCIES.'.symbol_placement = \'before\' THEN
										CONCAT('.TABLE_CURRENCIES.'.symbol, '.$this->tableName.'.reservation_total_price) 
					                ELSE
										CONCAT('.$this->tableName.'.reservation_total_price, '.TABLE_CURRENCIES.'.symbol)
								END as tp_w_currency,
								CASE
									WHEN '.TABLE_CURRENCIES.'.symbol_placement = \'before\' THEN
										CONCAT('.TABLE_CURRENCIES.'.symbol, ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment)) 
					                ELSE
										CONCAT(('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment), '.TABLE_CURRENCIES.'.symbol)
								END as tpaid_w_currency,
								CONCAT("<nobr><a href=\"javascript:void(\'description\')\" title=\"'._DESCRIPTION.'\" onclick=\"javascript:__mgDoPostBack(\''.$this->tableName.'\', \'description\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\')\">[ ", "'._DESCRIPTION.'", " ]</a></nobr>") as link_order_description,
								('.$this->tableName.'.reservation_total_price) as tp_wo_currency,
								(('.$this->tableName.'.reservation_total_price) / '.TABLE_CURRENCIES.'.rate) as tp_wo_default_currency,
								IF(
									'.$this->tableName.'.status = 2 AND ((SELECT DATEDIFF(cr.date_from, \''.@date('Y-m-d').'\') FROM '.TABLE_CAR_AGENCY_RESERVATIONS.' cr WHERE cr.date_from > \''.@date('Y-m-d').'\' AND cr.reservation_number = '.$this->tableName.'.reservation_number LIMIT 0, 1) >= '.$this->customers_cancel_reservation.'),
									CONCAT("<nobr><a href=\"javascript:void(0);\" title=\"'._BUTTON_CANCEL.'\" onclick=\"javascript:__mgMyDoPostBack(\''.TABLE_CAR_AGENCY_RESERVATIONS.'\', \'cancel\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\');\">[ '._BUTTON_CANCEL.' ]</a></nobr>"),
									"<span class=lightgray>'._BUTTON_CANCEL.'</span>"
								) as link_cust_order_cancel,
								IF('.$this->tableName.'.status != 6, CONCAT("<nobr><a href=\"javascript:void(0);\" title=\"'._BUTTON_CANCEL.'\" onclick=\"javascript:__mgMyDoPostBack(\''.TABLE_CAR_AGENCY_RESERVATIONS.'\', \'cancel\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\');\">[ '._BUTTON_CANCEL.' ]</a></nobr>"), "<span class=lightgray>'._BUTTON_CANCEL.'</span>") as link_admin_order_cancel,
								IF(('.$this->tableName.'.status = 2 OR '.$this->tableName.'.status = 3), CONCAT("<nobr><a href=\"javascript:void(\'invoice\')\" title=\"'._INVOICE.'\" onclick=\"javascript:__mgDoPostBack(\''.$this->tableName.'\', \'invoice\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\')\">[ ", "'._INVOICE.'", " ]</a></nobr>"), "<span class=lightgray>'._INVOICE.'</span>") as link_order_invoice
							FROM '.$this->tableName.'
								LEFT OUTER JOIN '.TABLE_CURRENCIES.' ON '.$this->tableName.'.currency = '.TABLE_CURRENCIES.'.code
								LEFT OUTER JOIN '.TABLE_CUSTOMERS.' ON '.$this->tableName.'.customer_id = '.TABLE_CUSTOMERS.'.id
							';		


					//			'.$this->tableName.'.reservation_description,
					//			'.$this->tableName.'.additional_info,
					//			'.$this->tableName.'.reservation_price,
					//			'.$this->tableName.'.vat_fee,
					//			'.$this->tableName.'.vat_percent,
					//			'.$this->tableName.'.currency,
					//			'.$this->tableName.'.transaction_number,
					//			DATE_FORMAT('.$this->tableName.'.payment_date, "'.$this->sqlFieldDateFormat.'") as payment_date_formated,
					
					//			('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment) as tp_wo_currency,
					//			IF(('.$this->tableName.'.status = 2 OR '.$this->tableName.'.status = 3), CONCAT("<nobr><a href=\"javascript:void(\'invoice\')\" title=\"'._INVOICE.'\" onclick=\"javascript:__mgDoPostBack(\''.$this->tableName.'\', \'invoice\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\')\">[ ", "'._INVOICE.'", " ]</a></nobr>"), "<span class=lightgray>'._INVOICE.'</span>") as link_order_invoice,
					//			CONCAT("<nobr><a href=\"javascript:void(\'description\')\" title=\"'._DESCRIPTION.'\" onclick=\"javascript:__mgDoPostBack(\''.$this->tableName.'\', \'description\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\')\">[ ", "'._DESCRIPTION.'", " ]</a></nobr>") as link_order_description,
					//			IF(
					//				'.$this->tableName.'.status = 2 AND ((SELECT DATEDIFF(br.checkin, \''.@date('Y-m-d').'\') FROM '.TABLE_BOOKINGS_ROOMS.' br WHERE br.checkin > \''.@date('Y-m-d').'\' AND br.reservation_number = '.$this->tableName.'.reservation_number ORDER BY br.checkin ASC LIMIT 0, 1) >= '.$this->customers_cancel_reservation.'),
					//				CONCAT("<nobr><a href=\"javascript:void(0);\" title=\"'._BUTTON_CANCEL.'\" onclick=\"javascript:__mgMyDoPostBack(\''.TABLE_BOOKINGS.'\', \'cancel\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\');\">[ '._BUTTON_CANCEL.' ]</a></nobr>"),
					//				"<span class=lightgray>'._BUTTON_CANCEL.'</span>"
					//			) as link_cust_order_cancel,
					//			IF('.$this->tableName.'.status != 6, CONCAT("<nobr><a href=\"javascript:void(0);\" title=\"'._BUTTON_CANCEL.'\" onclick=\"javascript:__mgMyDoPostBack(\''.TABLE_BOOKINGS.'\', \'cancel\', \'", '.$this->tableName.'.'.$this->primaryKey.', "\');\">[ '._BUTTON_CANCEL.' ]</a></nobr>"), "<span class=lightgray>'._BUTTON_CANCEL.'</span>") as link_admin_order_cancel,
							
		// define view mode fields
		if($this->user_id != ''){
			$this->arrViewModeFields = array(
				//'created_date_formated'   	=> array('title'=>_DATE_CREATED, 'type'=>'label', 'align'=>'center', 'width'=>'80px', 'height'=>'', 'nowrap'=>'nowrap', 'maxlength'=>''),
				'reservation_number'   	  	=> array('title'=>_RESERVATION_NUMBER, 'type'=>'label', 'align'=>'center', 'width'=>'90px', 'height'=>'', 'maxlength'=>''),
				'location_from_id'   		=> array('title'=>_FROM, 'type'=>'enum',  'align'=>'center', 'width'=>'70px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_agencies_locations),
				'location_to_id'   			=> array('title'=>_TO, 'type'=>'enum',  'align'=>'center', 'width'=>'70px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_agencies_locations),
				//'date_from'    				=> array('title'=>_DATE.'/'._TIME, 'type'=>'label', 'align'=>'center', 'width'=>'120px', 'height'=>'', 'maxlength'=>''),
				//'date_to'    				=> array('title'=>_DATE.'/'._TIME, 'type'=>'label', 'align'=>'center', 'width'=>'120px', 'height'=>'', 'maxlength'=>''),
				'date_from_to'    			=> array('title'=>_DATE.'/'._TIME, 'type'=>'label', 'align'=>'center', 'width'=>'120px', 'height'=>'', 'maxlength'=>''),
				'car_vehicle_id'   			=> array('title'=>_VEHICLE, 'type'=>'enum',  'align'=>'center', 'width'=>'110px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_vehicles),
				//'car_vehicle_type_id'   	=> array('title'=>_VEHICLE_TYPE, 'type'=>'enum',  'align'=>'center', 'width'=>'', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_vehicle_types),
				'car_agency_id'    	  		=> array('title'=>_CAR_AGENCY, 'type'=>'enum',  'align'=>'center', 'width'=>'', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_car_agencies),
				//'customer_name'   		  	=> array('title'=>_CUSTOMER, 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>''),
				'payment_type'           	=> array('title'=>_METHOD, 'type'=>'enum',  'align'=>'center', 'width'=>'100px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>'', 'source'=>$this->arr_payment_types),
				'tp_w_currency'   		  	=> array('title'=>_PAYMENT, 'type'=>'label', 'align'=>'right', 'width'=>'70px', 'height'=>'', 'maxlength'=>'', 'sort_by'=>'tp_wo_currency', 'sort_type'=>'numeric', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
				'tpaid_w_currency'   		=> array('title'=>_PAID_SUM, 'type'=>'label', 'align'=>'right', 'width'=>'70px', 'height'=>'', 'maxlength'=>'', 'sort_by'=>'tp_wo_currency', 'sort_type'=>'numeric', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
				'status'                 	=> array('title'=>_STATUS, 'type'=>'enum',  'align'=>'center', 'width'=>'100px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->statuses_vm),
				'link_order_description'    => array('title'=>'', 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>'', 'nowrap'=>'nowrap'),
				'link_order_invoice'     	=> array('title'=>'', 'type'=>'label', 'align'=>'center', 'width'=>'75px', 'height'=>'', 'maxlength'=>'', 'nowrap'=>'nowrap'),
				'link_cust_order_cancel'    => array('title'=>'', 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>'', 'nowrap'=>'nowrap', 'visible'=>(($this->customers_cancel_reservation > '0') ? true : false)),
			);			
		}else{
			$this->arrViewModeFields = array(
				'created_date_formated'   	=> array('title'=>_DATE_CREATED, 'type'=>'label', 'align'=>'center', 'width'=>'80px', 'height'=>'', 'nowrap'=>'nowrap', 'maxlength'=>''),
				'reservation_number'   	  	=> array('title'=>_RESERVATION_NUMBER, 'type'=>'label', 'align'=>'center', 'width'=>'90px', 'height'=>'', 'maxlength'=>''),
				'location_from_id'   		=> array('title'=>_FROM, 'type'=>'enum',  'align'=>'center', 'width'=>'70px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_agencies_locations),
				'location_to_id'   			=> array('title'=>_TO, 'type'=>'enum',  'align'=>'center', 'width'=>'70px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_agencies_locations),
				'date_from_to'    			=> array('title'=>_DATE.'/'._TIME, 'type'=>'label', 'align'=>'center', 'width'=>'120px', 'height'=>'', 'maxlength'=>''),
				'car_vehicle_id'   			=> array('title'=>_VEHICLE, 'type'=>'enum',  'align'=>'center', 'width'=>'110px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_vehicles),
				//'car_vehicle_type_id'   	=> array('title'=>_VEHICLE_TYPE, 'type'=>'enum',  'align'=>'center', 'width'=>'', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_vehicle_types),
				'payment_type'            	=> array('title'=>_METHOD, 'type'=>'enum',  'align'=>'center', 'width'=>'100px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>'', 'source'=>$this->arr_payment_types),
				'car_agency_id'    	  		=> array('title'=>_CAR_AGENCY, 'type'=>'enum',  'align'=>'center', 'width'=>'', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->arr_car_agencies),
				'customer_name'   		  	=> array('title'=>_CUSTOMER, 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>''),
				'tp_w_currency'   		  	=> array('title'=>_PAYMENT, 'type'=>'label', 'align'=>'right', 'width'=>'70px', 'height'=>'', 'maxlength'=>'', 'sort_by'=>'tp_wo_currency', 'sort_type'=>'numeric', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
				'tpaid_w_currency'   		=> array('title'=>_PAID_SUM, 'type'=>'label', 'align'=>'right', 'width'=>'70px', 'height'=>'', 'maxlength'=>'', 'sort_by'=>'tp_wo_currency', 'sort_type'=>'numeric', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
				'status'                  	=> array('title'=>_STATUS, 'type'=>'enum',  'align'=>'center', 'width'=>'100px', 'sortable'=>true, 'nowrap'=>'', 'visible'=>true, 'source'=>$this->statuses_vm),
				'link_order_description'    => array('title'=>'', 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>''),
				'link_order_invoice'      	=> array('title'=>'', 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>''),
				'link_admin_order_cancel'   => array('title'=>'', 'type'=>'label', 'align'=>'center', 'width'=>'', 'height'=>'', 'maxlength'=>'', 'visible'=>$allow_cancelation),
			);						
		}
		
		//---------------------------------------------------------------------- 
		// ADD MODE
		//---------------------------------------------------------------------- 
		// define add mode fields
		$this->arrAddModeFields = array(
			
		);

		//---------------------------------------------------------------------- 
		// EDIT MODE
		//---------------------------------------------------------------------- 
		$this->EDIT_MODE_SQL = 'SELECT
								'.$this->tableName.'.'.$this->primaryKey.',
								'.$this->tableName.'.reservation_number,
								'.$this->tableName.'.car_agency_id,
								'.$this->tableName.'.reservation_number as reservation_number_label,
								'.$this->tableName.'.reservation_description,
								'.$this->tableName.'.additional_info,
								'.$this->tableName.'.reservation_price,
								CASE
									WHEN '.TABLE_CURRENCIES.'.symbol_placement = "before" THEN CONCAT('.TABLE_CURRENCIES.'.symbol, '.$this->tableName.'.reservation_paid)
					                ELSE CONCAT('.$this->tableName.'.reservation_paid, '.TABLE_CURRENCIES.'.symbol)
								END as reservation_paid,
								CONCAT(
									'.TABLE_CURRENCIES.'.symbol,
									IF(
										('.$this->tableName.'.reservation_total_price - ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment) > 0),
										('.$this->tableName.'.reservation_total_price - ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment)),
										0
									)									
								) as mod_have_to_pay,
								'.$this->tableName.'.additional_payment,
								'.$this->tableName.'.vat_fee,
								'.$this->tableName.'.vat_percent,
								CASE
									WHEN '.TABLE_CURRENCIES.'.symbol_placement = "before" THEN CONCAT('.TABLE_CURRENCIES.'.symbol, '.$this->tableName.'.vat_fee)
					                ELSE CONCAT('.$this->tableName.'.vat_fee, '.TABLE_CURRENCIES.'.symbol)
								END as mod_vat_fee,
								'.$this->tableName.'.currency,
								'.$this->tableName.'.customer_id,								
								'.$this->tableName.'.cc_type,
								'.$this->tableName.'.cc_holder_name,
								IF(
									LENGTH(AES_DECRYPT('.$this->tableName.'.cc_number, "'.PASSWORDS_ENCRYPT_KEY.'")) = 4,
									CONCAT("...", AES_DECRYPT('.$this->tableName.'.cc_number, "'.PASSWORDS_ENCRYPT_KEY.'"), " ('._CLEANED.')"),
									AES_DECRYPT('.$this->tableName.'.cc_number, "'.PASSWORDS_ENCRYPT_KEY.'")
								) as m_cc_number,								
								AES_DECRYPT('.$this->tableName.'.cc_cvv_code, "'.PASSWORDS_ENCRYPT_KEY.'") as m_cc_cvv_code,
								'.$this->tableName.'.cc_expires_month,
								'.$this->tableName.'.cc_expires_year,
								IF('.$this->tableName.'.cc_expires_month != "", CONCAT('.$this->tableName.'.cc_expires_month, "/", '.$this->tableName.'.cc_expires_year), "") as m_cc_expires_date,
								DATE_FORMAT('.$this->tableName.'.created_date, "'.$this->sqlFieldDatetimeFormat.'") as created_date_formated,
								DATE_FORMAT('.$this->tableName.'.payment_date, "'.$this->sqlFieldDatetimeFormat.'") as payment_date_formated,
								'.$this->tableName.'.transaction_number,
								'.$this->tableName.'.payment_date,
								'.$this->tableName.'.payment_type,
								'.$this->tableName.'.payment_method,
								'.$this->tableName.'.status,
								'.$this->tableName.'.status_description,
								
								"" as hold
							FROM '.$this->tableName.'
								LEFT OUTER JOIN '.TABLE_CURRENCIES.' ON '.$this->tableName.'.currency = '.TABLE_CURRENCIES.'.code
								LEFT OUTER JOIN '.TABLE_CUSTOMERS.' ON '.$this->tableName.'.customer_id = '.TABLE_CUSTOMERS.'.id
							';

		if($this->user_id != ''){
			//'.$this->tableName.'.is_admin_reservation = 0 AND
			$WHERE_CLAUSE = 'WHERE '.$this->tableName.'.customer_id = '.$this->user_id.' AND
			                       '.$this->tableName.'.'.$this->primaryKey.' = _RID_';
			$this->EDIT_MODE_SQL = $this->EDIT_MODE_SQL.' WHERE TRUE = FALSE';					   
		}else{
			$WHERE_CLAUSE = 'WHERE '.$this->tableName.'.'.$this->primaryKey.' = _RID_';
			$this->EDIT_MODE_SQL = $this->EDIT_MODE_SQL.$WHERE_CLAUSE;
		}		

		// prepare trigger
		$sql = 'SELECT
					vat_percent,
					status,
					payment_type,
					IF(TRIM(cc_number) = "" OR LENGTH(AES_DECRYPT(cc_number, "'.PASSWORDS_ENCRYPT_KEY.'")) <= 4, "hide", "show") as cc_number_trigger
				FROM '.$this->tableName.' WHERE id = '.(int)$rid;
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($result[1] > 0){
			$cc_number_trigger = $result[0]['cc_number_trigger'];
			$status_trigger = $result[0]['status'];
			$payment_type = $result[0]['payment_type'];
			$vat_percent = ' ('.Currencies::PriceFormat($result[0]['vat_percent'], '%', 'after', $this->currencyFormat, $this->GetVatPercentDecimalPoints($result[0]['vat_percent'])).')';			
		}else{
			$cc_number_trigger = 'hide';
			$status_trigger = '0';
			$payment_type = '0';
			$vat_percent = '';
		}		

		// define edit mode fields
		$this->arrEditModeFields = array(
			'reservation_number_label' 	=> array('title'=>_RESERVATION_NUMBER, 'type'=>'label'),			
			'reservation_number'     	=> array('title'=>'', 'type'=>'hidden', 'required'=>false, 'default'=>''),
			'customer_id'        		=> array('title'=>'', 'type'=>'hidden', 'required'=>false, 'default'=>''),
			
			'created_date_formated' 	=> array('title'=>_DATE_CREATED, 'type'=>'label'),
			'payment_date_formated' 	=> array('title'=>_DATE_PAYMENT, 'type'=>'label'),
			'payment_type'       		=> array('title'=>_PAYMENT_TYPE, 'type'=>'enum', 'width'=>'', 'required'=>true, 'readonly'=>true, 'default'=>'', 'source'=>$this->arr_payment_types, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>''),
			'payment_method'     		=> array('title'=>_PAYMENT_METHOD, 'type'=>'enum', 'width'=>'', 'required'=>true, 'readonly'=>true, 'default'=>'', 'source'=>$this->arr_payment_methods, 'default_option'=>'', 'unique'=>false, 'javascript_event'=>''),
			'mod_vat_fee'        		=> array('title'=>_VAT, 'type'=>'label', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2', 'post_html'=>$vat_percent),
			'reservation_paid'    		=> array('title'=>_PAID_SUM, 'type'=>'label', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
			'additional_payment' 		=> array('title'=>_ADDITIONAL_PAYMENT, 'header_tooltip'=>_ADDITIONAL_PAYMENT_TOOLTIP, 'type'=>'textbox',  'width'=>'100px', 'required'=>false, 'readonly'=>(($status_trigger == '2' || $status_trigger == '3') ? false : true), 'maxlength'=>'10', 'default'=>'', 'validation_type'=>'float', 'validation_maximum'=>'10000000', 'unique'=>false, 'visible'=>true, 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
			'mod_have_to_pay'    		=> array('title'=>_PAYMENT_REQUIRED, 'type'=>'label', 'format'=>'currency', 'format_parameter'=>$this->currencyFormat.'|2'),
			'status'  			 		=> array('title'=>_STATUS, 'type'=>'enum', 'width'=>'', 'required'=>true, 'readonly'=>(($status_trigger >= '3') ? true : false), 'source'=>$arr_statuses_edit),
		);
		if($this->user_id != ''){
			$this->arrEditModeFields['status'] = array('title'=>_STATUS, 'type'=>'enum', 'width'=>'', 'required'=>true, 'readonly'=>(($status_trigger >= '3') ? true : false), 'source'=>$arr_statuses_edit);
		}else{			
			$this->arrEditModeFields['status'] = array('title'=>_STATUS, 'type'=>'enum', 'width'=>'', 'required'=>true, 'readonly'=>false, 'source'=>(($status_trigger >= '3') ? $arr_statuses_edit_completed : $arr_statuses_edit));
			$this->arrEditModeFields['status_description'] = array('title'=>_STATUS.' ('._DESCRIPTION.')', 'type'=>'label');
			
			if($payment_type == '1'){ // on-line orders only!
				$this->arrEditModeFields['cc_type'] 		= array('title'=>_CREDIT_CARD_TYPE, 'type'=>'label');
				$this->arrEditModeFields['cc_holder_name'] 	= array('title'=>_CREDIT_CARD_HOLDER_NAME, 'type'=>'label');
				$this->arrEditModeFields['m_cc_number'] 	= array('title'=>_CREDIT_CARD_NUMBER, 'type'=>'label', 'post_html'=>(($cc_number_trigger == 'show') ? '&nbsp;[ <a href="javascript:void(0);" onclick="if(confirm(\''._PERFORM_OPERATION_COMMON_ALERT.'\')) __mgDoPostBack(\''.$this->tableName.'\', \'clean_credit_card\', \''.$rid.'\')">'._REMOVE.'</a> ]' : ''));
				$this->arrEditModeFields['m_cc_expires_date'] = array('title'=>_CREDIT_CARD_EXPIRES, 'type'=>'label');
				$this->arrEditModeFields['m_cc_cvv_code'] 	= array('title'=>_CVV_CODE, 'type'=>'label');
			}
			$this->arrEditModeFields['additional_info'] = array('title'=>_ADDITIONAL_INFO, 'type'=>'textarea', 'width'=>'390px', 'height'=>'90px', 'editor_type'=>'simple', 'readonly'=>false, 'default'=>'', 'required'=>false, 'validation_type'=>'', 'validation_maxlength'=>'1024', 'unique'=>false);
		}

		//---------------------------------------------------------------------- 
		// DETAILS MODE
		//----------------------------------------------------------------------		
		$this->DETAILS_MODE_SQL = $this->VIEW_MODE_SQL.$WHERE_CLAUSE;

		$this->arrDetailsModeFields = array(

			'reservation_number'  	 => array('title'=>_RESERVATION_NUMBER, 'type'=>'label'),
			'reservation_description'  => array('title'=>_DESCRIPTION, 'type'=>'label'),
			'reservation_price'  		 => array('title'=>_ORDER_PRICE, 'type'=>'label'),
			'vat_fee'  		     => array('title'=>_VAT, 'type'=>'label'),
			'vat_percent'  		 => array('title'=>_VAT_PERCENT, 'type'=>'label'),
			'reservation_paid'  		 => array('title'=>_PAYMENT_SUM, 'type'=>'label'),
			'currency'  		 => array('title'=>_CURRENCY, 'type'=>'label'),
			'customer_name'      => array('title'=>_CUSTOMER, 'type'=>'label'),
			'transaction_number' => array('title'=>_TRANSACTION, 'type'=>'label'),
			'payment_date_formated' => array('title'=>_DATE, 'type'=>'label'),
			'payment_type'       => array('title'=>_PAYMENT_TYPE, 'type'=>'enum', 'source'=>$this->arr_payment_types),
			'payment_method'     => array('title'=>_PAYMENT_METHOD, 'type'=>'enum', 'source'=>$this->arr_payment_methods),
			'status'  	         => array('title'=>_STATUS, 'type'=>'label'),
			'additional_info'    => array('title'=>_ADDITIONAL_INFO, 'type'=>'label'),
		);
	}
	
	//==========================================================================
    // Class Destructor
	//==========================================================================
    function __destruct()
	{
		// echo 'this object has been destroyed';
    }
	
	/**
	 *	Cancel record
	 */
	public function CancelRecord($rid)
	{
		global $objLogin;
		
		if(strtolower(SITE_MODE) == 'demo'){
			$this->error = _OPERATION_BLOCKED;
			return false;
		}

		if($objLogin->IsLoggedInAsAdmin() && !$objLogin->HasPrivileges('cancel_car_reservations')){
			return false;
		}
		
		$sql = 'UPDATE '.$this->tableName.'
				SET
					status = 6,
					status_changed = \''.date('Y-m-d H:i:s').'\',
					status_description = \''.encode_text(($objLogin->IsLoggedInAsAdmin() ? _CANCELED_BY_ADMIN : _CANCELED_BY_CUSTOMER)).'\'
				WHERE '.$this->primaryKey.' = '.(int)$rid;
		if(!database_void_query($sql)) return false;
		
		// Check if this is "by balance" payment and return money back to account balance 
		$this->ReturnMoneyToBalance($rid);
		
		return true; 
	}	
	
	/**
	 *	Draws order invoice
	 */
	public function DrawCarRentalInvoice($rid, $text_only = false, $type = 'html', $draw = true)
	{
		global $objSiteDescription, $objSettings, $objLogin;
		
		$output = '';
		$output_pdf = array();
		$oid = isset($rid) ? (int)$rid : '0';
		$language_id = Languages::GetDefaultLang();
		$agencies_count = CarAgencies::AgenciesCount();
		
		$sql = 'SELECT
					'.$this->tableName.'.'.$this->primaryKey.',
					'.$this->tableName.'.reservation_number,
					'.$this->tableName.'.reservation_description,
					'.$this->tableName.'.additional_info,
					'.$this->tableName.'.car_agency_id,
					'.$this->tableName.'.reservation_price,
					'.$this->tableName.'.vat_fee,
					'.$this->tableName.'.vat_percent,
					'.$this->tableName.'.reservation_paid,
					'.$this->tableName.'.pre_payment_type,
					'.$this->tableName.'.pre_payment_value,
					'.$this->tableName.'.status,
					IF(
						('.$this->tableName.'.reservation_total_price - ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment) > 0),
						('.$this->tableName.'.reservation_total_price - ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment)),
						0
					) as mod_have_to_pay,								
					'.$this->tableName.'.additional_payment,
					'.$this->tableName.'.extras,
					'.$this->tableName.'.extras_fee,
					'.$this->tableName.'.cc_type,
					'.$this->tableName.'.cc_holder_name,
					'.$this->tableName.'.cc_expires_month,
					'.$this->tableName.'.cc_expires_year,
					'.$this->tableName.'.cc_cvv_code, 					
					'.$this->tableName.'.currency,
					'.$this->tableName.'.customer_id,
					'.$this->tableName.'.transaction_number,
					'.$this->tableName.'.is_admin_reservation,
					DATE_FORMAT('.$this->tableName.'.created_date, \''.$this->sqlFieldDatetimeFormat.'\') as created_date_formated,					
					DATE_FORMAT('.$this->tableName.'.payment_date, \''.$this->sqlFieldDatetimeFormat.'\') as payment_date_formated,					
					'.$this->tableName.'.payment_type,
					'.$this->tableName.'.payment_method,
					'.TABLE_CURRENCIES.'.symbol,
					'.TABLE_CURRENCIES.'.symbol_placement,
					CONCAT("<a href=\"index.php?'.$this->page.'&mg_action=description&oid=", '.$this->tableName.'.'.$this->primaryKey.', "\">", "'._DESCRIPTION.'", "</a>") as link_order_description,
					'.TABLE_CUSTOMERS.'.first_name,
					'.TABLE_CUSTOMERS.'.last_name,					
					'.TABLE_CUSTOMERS.'.email as customer_email,
					'.TABLE_CUSTOMERS.'.company as customer_company,
					'.TABLE_CUSTOMERS.'.b_address,
					'.TABLE_CUSTOMERS.'.b_address_2,
					'.TABLE_CUSTOMERS.'.b_city,
					'.TABLE_CUSTOMERS.'.b_state,
					'.TABLE_CUSTOMERS.'.b_zipcode,
					'.TABLE_CUSTOMERS.'.phone,
					'.TABLE_CUSTOMERS.'.fax, 
					'.TABLE_COUNTRIES.'.name as country_name
				FROM '.$this->tableName.'
					LEFT OUTER JOIN '.TABLE_CURRENCIES.' ON '.$this->tableName.'.currency = '.TABLE_CURRENCIES.'.code
					LEFT OUTER JOIN '.TABLE_CUSTOMERS.' ON '.$this->tableName.'.customer_id = '.TABLE_CUSTOMERS.'.id
					LEFT OUTER JOIN '.TABLE_COUNTRIES.' ON '.TABLE_CUSTOMERS.'.b_country = '.TABLE_COUNTRIES.'.abbrv AND '.TABLE_COUNTRIES.'.is_active = 1
				WHERE
					'.$this->tableName.'.'.$this->primaryKey.' = '.(int)$oid;

				if($this->user_id != ''){
					$sql .= ' AND '.$this->tableName.'.is_admin_reservation = 0 AND '.$this->tableName.'.customer_id = '.(int)$this->user_id;
				}
					
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		//echo database_error();
		//dbug($result);
		if($result[1] > 0){
			// for pdf export
			if($type == 'pdf'){
				$output_pdf['is_admin_reservation'] = ($result[0]['is_admin_reservation'] == '1') ? '1' : '0';
				$output_pdf['first_name'] = ($result[0]['is_admin_reservation'] == '0') ? _FIRST_NAME.': '.$result[0]['first_name'] : '';
				$output_pdf['last_name'] = ($result[0]['is_admin_reservation'] == '0') ? _LAST_NAME.': '.$result[0]['last_name'] : '';
				$output_pdf['email'] = ($result[0]['is_admin_reservation'] == '0') ? _EMAIL_ADDRESS.': '.$result[0]['customer_email'] : '';
				$output_pdf['company'] = ($result[0]['is_admin_reservation'] == '0') ? _COMPANY.': '.$result[0]['customer_company'] : '';
				$output_pdf['phones'] = '';
				if($result[0]['phone'] != '' || $result[0]['fax'] != ''){
					if($result[0]['phone'] != '') $output_pdf['phones'] .= _PHONE.': '.$result[0]['phone'];
					if($result[0]['phone'] != '' && $result[0]['fax'] != '') $output_pdf['phones'] .= ', ';
					if($result[0]['fax'] != '') $output_pdf['phones'] .= _FAX.': '.$result[0]['fax'];
				} 				
				$output_pdf['address'] = ($result[0]['is_admin_reservation'] == '0') ? _ADDRESS.': '.$result[0]['b_address'].' '.$result[0]['b_address_2'] : '';
				$output_pdf['city'] = ($result[0]['is_admin_reservation'] == '0') ? $result[0]['b_city'].' '.$result[0]['b_state'] : '';
				$output_pdf['country'] = ($result[0]['is_admin_reservation'] == '0') ? $result[0]['country_name'].' '.$result[0]['b_zipcode'] : '';			
				$output_pdf['created_date'] = $result[0]['created_date_formated'];
				$output_pdf['reservation_status'] = strip_tags($this->statuses_vm[$result[0]['status']]);
				$output_pdf['reservation_number'] = $result[0]['reservation_number'];
				$output_pdf['reservation_description'] = $result[0]['reservation_description'];
				$output_pdf['transaction_number'] = $result[0]['transaction_number'];
				$output_pdf['payment_date'] = $result[0]['payment_date_formated'];
				$output_pdf['payment_type'] = $this->arr_payment_types[$result[0]['payment_type']];
				$output_pdf['payment_method'] = $this->arr_payment_methods[$result[0]['payment_method']];				
				$output_pdf['booking_price'] = Currencies::PriceFormat($result[0]['reservation_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
				$output_pdf['booking_currency'] = $result[0]['symbol'];

				$output_pdf['booking_subtotal'] = Currencies::PriceFormat($result[0]['reservation_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
				$output_pdf['extras_subtotal'] = (!empty($result[0]['extras'])) ? Currencies::PriceFormat($result[0]['extras_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat) : '';
				$output_pdf['vat_fee'] = ($this->vat_included_in_price == 'no') ? (Currencies::PriceFormat($result[0]['vat_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.Currencies::PriceFormat($result[0]['vat_percent'], '%', 'after', $this->currencyFormat, $this->GetVatPercentDecimalPoints($result[0]['vat_percent'])).') ') : '';

				if($result[0]['pre_payment_type'] == 'percentage' && $result[0]['pre_payment_value'] > 0 && $result[0]['pre_payment_value'] < 100){
					$output_pdf['pre_payment'] = Currencies::PriceFormat($result[0]['reservation_paid'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.$result[0]['pre_payment_value'].'%)';
				}else if($result[0]['pre_payment_type'] == 'fixed sum' && $result[0]['pre_payment_value'] > 0){
					$output_pdf['pre_payment'] = Currencies::PriceFormat($result[0]['reservation_paid'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.Currencies::PriceFormat($result[0]['pre_payment_value'], $result[0]['symbol'], $result[0]['symbol_placement']).')';
				}else{
					$output_pdf['pre_payment'] = _FULL_PRICE;
				}
				$output_pdf['additional_payment'] = Currencies::PriceFormat($result[0]['additional_payment'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
				$output_pdf['total'] = Currencies::PriceFormat($result[0]['reservation_paid'] + $result[0]['additional_payment'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);				
				$output_pdf['payment_required'] = ($result[0]['mod_have_to_pay'] != 0) ? Currencies::PriceFormat($result[0]['mod_have_to_pay'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat) : '';
			}
			

			$part = '<table '.((Application::Get('lang_dir') == 'rtl') ? 'dir="rtl"' : '').' width="100%" border="0" cellspacing="0" cellpadding="0">';
			if($text_only && ModulesSettings::Get('car_rental', 'mode') == 'TEST MODE'){
				$part .= '<tr><td colspan="2"><div style="text-align:center;padding:10px;color:#a60000;border:1px dashed #a60000;width:100px">TEST MODE!</div></td></tr>';
			}

			$part .= '<tr><td colspan="2">'._DATE_CREATED.': '.$result[0]['created_date_formated'].'</td></tr>
				      <tr><td colspan="2" nowrap="nowrap" height="10px"></td></tr>';
			$part .= '<tr>
					<td valign="top">						
						<h3>'._CUSTOMER_DETAILS.':</h3>';
						if($result[0]['is_admin_reservation'] == '1'){							
							$part .= _ADMIN_RESERVATION.'<br />';
						}else{
							$part .= _FIRST_NAME.': '.$result[0]['first_name'].'<br />';         
							$part .= _LAST_NAME.': '.$result[0]['last_name'].'<br />';           
							$part .= _EMAIL_ADDRESS.': '.$result[0]['customer_email'].'<br />';  
							if($result[0]['customer_company'] != '') $part .= _COMPANY.': '.$result[0]['customer_company'].'<br />';
							if($result[0]['phone'] != '' || $result[0]['fax'] != ''){
								if($result[0]['phone'] != '') $part .= _PHONE.': '.$result[0]['phone'];
								if($result[0]['phone'] != '' && $result[0]['fax'] != '') $part .= ', ';
								if($result[0]['fax'] != '') $part .= _FAX.': '.$result[0]['fax'];
								$part .= '<br />';	
							} 
							$part .= _ADDRESS.': '.$result[0]['b_address'].' '.$result[0]['b_address_2'].'<br />';  
							$part .= $result[0]['b_city'].' '.$result[0]['b_state'].'<br />';    
							$part .= $result[0]['country_name'].' '.$result[0]['b_zipcode'];	 
						}
			$part .= '</td>
					<td valign="top" align="'.Application::Get('defined_right').'">';
					
						$agency_info = CarAgencies::GetAgencyFullInfo($result[0]['car_agency_id'], $language_id);
						$part .= '<h3>'.$agency_info['name'].'</h3>';
						$part .= _ADDRESS.': '.$agency_info['address'].'<br />';
						$part .= _PHONE.': '.$agency_info['phone'].'<br />';
						$part .= _FAX.': '.$agency_info['fax'].'<br />';
						$part .= _EMAIL_ADDRESS.': '.$agency_info['email'].'<br />';
					
					$part .= '</td>
				</tr>
				<tr><td colspan="2" nowrap="nowrap" height="10px"></td></tr>
				<tr>
					<td colspan="2">';						
						$part .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border:1px solid #d1d2d3">';
						$part .= '<tr style=background-color:#e1e2e3;font-weight:bold;font-size:13px;"><th align="left" colspan="2">&nbsp;<b>'._BOOKING_DETAILS.'</b></th></tr>';
						$part .= '<tr><td width="25%">&nbsp;'._STATUS.': </td><td><b>'.strip_tags($this->statuses_vm[$result[0]['status']]).'</b></td></tr>';
						$part .= '<tr><td width="25%">&nbsp;'._RESERVATION_NUMBER.': </td><td>'.$result[0]['reservation_number'].'</td></tr>';
						$part .= '<tr><td>&nbsp;'._DESCRIPTION.': </td><td>'.$result[0]['reservation_description'].'</td></tr>';
						$part .= '</table><br />';									
						
						$part .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border:1px solid #d1d2d3">';
						$part .= '<tr style="background-color:#e1e2e3;font-weight:bold;font-size:13px;"><th align="left" colspan="2">&nbsp;<b>'._PAYMENT_DETAILS.'</b></th></tr>';
						$part .= '<tr><td width="25%">&nbsp;'._TRANSACTION.': </td><td>'.$result[0]['transaction_number'].'</td></tr>';
						$part .= '<tr><td>&nbsp;'._DATE_PAYMENT.': </td><td>'.$result[0]['payment_date_formated'].'</td></tr>';
						$part .= '<tr><td>&nbsp;'._PAYMENT_TYPE.': </td><td>'.$this->arr_payment_types[$result[0]['payment_type']].'</td></tr>';
						$part .= '<tr><td>&nbsp;'._PAYMENT_METHOD.': </td><td>'.$this->arr_payment_methods[$result[0]['payment_method']].'</td></tr>';
						$part .= '<tr><td>&nbsp;'._PRICE.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						//$part .= '<tr><td>&nbsp;'._SUBTOTAL.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						//if(!empty($result[0]['extras'])) $part .= '<tr><td>&nbsp;'._EXTRAS_SUBTOTAL.': </td><td>'.Currencies::PriceFormat($result[0]['extras_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						if($this->vat_included_in_price == 'no') $part .= '<tr><td>&nbsp;'._VAT.': </td><td>'.Currencies::PriceFormat($result[0]['vat_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.Currencies::PriceFormat($result[0]['vat_percent'], '%', 'after', $this->currencyFormat, $this->GetVatPercentDecimalPoints($result[0]['vat_percent'])).')</td></tr>';
						$part .= '<tr><td>&nbsp;'._PAYMENT_SUM.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_paid'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						
						if($result[0]['pre_payment_type'] == 'percentage' && $result[0]['pre_payment_value'] > 0 && $result[0]['pre_payment_value'] < 100){
							$part .= '<tr><td>&nbsp;'._PRE_PAYMENT.'</td><td>'.Currencies::PriceFormat($result[0]['reservation_paid'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.$result[0]['pre_payment_value'].'%)</td></tr>';
						}else if($result[0]['pre_payment_type'] == 'fixed sum' && $result[0]['pre_payment_value'] > 0){
							$part .= '<tr><td>&nbsp;'._PRE_PAYMENT.'</td><td>'.Currencies::PriceFormat($result[0]['reservation_paid'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.Currencies::PriceFormat($result[0]['pre_payment_value'], $result[0]['symbol'], $result[0]['symbol_placement']).')</td></tr>';
						}else{
							$part .= '<tr><td>&nbsp;'._PRE_PAYMENT.'</td><td>'._FULL_PRICE.'</td></tr>';
						}
						$part .= '<tr><td>&nbsp;'._ADDITIONAL_PAYMENT.': </td><td>'.Currencies::PriceFormat($result[0]['additional_payment'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						$part .= '<tr><td>&nbsp;'._TOTAL.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_paid'] + $result[0]['additional_payment'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						if($result[0]['mod_have_to_pay'] != 0){
							$part .= '<tr><td style="color:#960000">&nbsp;'._PAYMENT_REQUIRED.': </td><td style="color:#960000">'.Currencies::PriceFormat($result[0]['mod_have_to_pay'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
						}			
						$part .= '</table><br />';															
				$part .= '</td>';
				$part .= '</tr>';
				$part .= '</table>';
				
			$content = @file_get_contents('html/templates/invoice_car.tpl');
			if($content){
				$content = str_replace('_TOP_PART_', $part, $content);
				$content = str_replace('_CAR_DETAILS_', $this->GetCarReservationList($oid, $language_id), $content);
				$content = str_replace('_YOUR_COMPANY_NAME_', $objSiteDescription->GetParameter('header_text'), $content);
				$content = str_replace('_ADMIN_EMAIL_', $objSettings->GetParameter('admin_email'), $content);
			}
			$output .= '<div id="divInvoiceContent">'.$content.'</div>';
		}
		
		if(!$text_only){
			$output .= '<table width="100%" border="0">';
			$output .= '<tr><td colspan="2">&nbsp;</tr>';
			$output .= '<tr>';
			$output .= '  <td colspan="2" align="left"><input type="button" class="mgrid_button" name="btnBack" value="'._BUTTON_BACK.'" onclick="javascript:appGoTo(\''.$this->page.'\');"></td>';
			$output .= '</tr>';			
			$output .= '</table>';
		}
		
		if($draw){
			echo $output;
		}else{
			return ($type == 'pdf') ? $output_pdf : $output;
		}
	}
	
	/**
	 * Send invoice to customer
	 * 		@param $rid
	 */
	public function SendInvoice($rid)
	{
		global $objSettings;
		
		if(strtolower(SITE_MODE) == "demo"){
			$this->error = _OPERATION_BLOCKED;
			return false;
		}
		
		$sql = 'SELECT
					b.reservation_number,
					IF(is_admin_reservation = 1, a.email, c.email) as email,
					IF(is_admin_reservation = 1, a.preferred_language, c.preferred_language) as preferred_language
				FROM '.TABLE_CAR_AGENCY_RESERVATIONS.' b
					LEFT OUTER JOIN '.TABLE_CUSTOMERS.' c ON b.customer_id = c.id
					LEFT OUTER JOIN '.TABLE_ACCOUNTS.' a ON b.customer_id = a.id
				WHERE b.id = '.(int)$rid;		
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($result[1] > 0){
			
			$recipient = $result[0]['email'];
			$sender    = $objSettings->GetParameter('admin_email');
			$subject   = _INVOICE.' ('._FOR_BOOKING.$result[0]['reservation_number'].')';
			$body      = $this->DrawCarRentalInvoice($rid, true, 'html', false);
			$preferred_language = $result[0]['preferred_language'];
			//$body      = str_replace('<br />', '', $body);
			send_email_wo_template(
				$recipient,
				$sender,
				$subject,
				$body,
				$preferred_language
			);
			
			return true;
		}
		
		$this->error = _EMAILS_SENT_ERROR;
		return false;		
	}
	
	/**
	 * Draws Booking Description
	 * 		@param $rid
	 * 		@param $mode
	 * 		@param $draw
	 */
	public function DrawBookingDescription($rid, $mode = '', $draw = true)
	{
		global $objLogin;
		
		$output = '';
		$content = '';
		$oid = isset($rid) ? (int)$rid : '0';
		$language_id = Application::Get('lang');

		$sql = 'SELECT
				'.$this->tableName.'.'.$this->primaryKey.',
				'.$this->tableName.'.reservation_number,
				'.$this->tableName.'.reservation_description,
				'.$this->tableName.'.additional_info,
				'.$this->tableName.'.reservation_price,
				'.$this->tableName.'.reservation_total_price,
				'.$this->tableName.'.vat_fee,
				'.$this->tableName.'.vat_percent,
				'.$this->tableName.'.reservation_paid,
				'.$this->tableName.'.pre_payment_type,
				'.$this->tableName.'.pre_payment_value,
				'.$this->tableName.'.additional_payment,
				'.$this->tableName.'.extras,
				'.$this->tableName.'.extras_fee,
				'.$this->tableName.'.cc_type,
				'.$this->tableName.'.cc_holder_name,
				IF((('.$this->tableName.'.reservation_price) + '.$this->tableName.'.extras_fee + '.$this->tableName.'.vat_fee - ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment) > 0),
					(('.$this->tableName.'.reservation_price) + '.$this->tableName.'.extras_fee + '.$this->tableName.'.vat_fee - ('.$this->tableName.'.reservation_paid + '.$this->tableName.'.additional_payment)),
					0
				) as mod_have_to_pay,								
				CASE
					WHEN LENGTH(AES_DECRYPT('.$this->tableName.'.cc_number, \''.PASSWORDS_ENCRYPT_KEY.'\')) = 4
						THEN CONCAT(\'...\', AES_DECRYPT('.$this->tableName.'.cc_number, \''.PASSWORDS_ENCRYPT_KEY.'\'))
					ELSE AES_DECRYPT('.$this->tableName.'.cc_number, \''.PASSWORDS_ENCRYPT_KEY.'\')
				END as cc_number,
				CONCAT(\'...\', SUBSTRING(AES_DECRYPT(cc_number, \''.PASSWORDS_ENCRYPT_KEY.'\'), -4)) as cc_number_for_customer,
				CASE
					WHEN LENGTH(AES_DECRYPT('.$this->tableName.'.cc_number, \''.PASSWORDS_ENCRYPT_KEY.'\')) = 4
						THEN \' ('._CLEANED.')\'
					ELSE \'\'
				END as cc_number_cleaned,								
				'.$this->tableName.'.cc_expires_month,
				'.$this->tableName.'.cc_expires_year,
				AES_DECRYPT(cc_cvv_code, \''.PASSWORDS_ENCRYPT_KEY.'\') as cc_cvv_code, 
				'.$this->tableName.'.currency,
				'.$this->tableName.'.customer_id,
				'.$this->tableName.'.transaction_number,
				'.$this->tableName.'.payment_date, 
				DATE_FORMAT('.$this->tableName.'.created_date, \''.(($this->fieldDateFormat == 'M d, Y') ? '%b %d, %Y %h:%i %p' : '%d %b %Y %h:%i %p').'\') as created_date_formated,
				DATE_FORMAT('.$this->tableName.'.payment_date, \''.(($this->fieldDateFormat == 'M d, Y') ? '%b %d, %Y %h:%i %p' : '%d %b %Y %h:%i %p').'\') as payment_date_formated,
				'.$this->tableName.'.payment_type,
				'.$this->tableName.'.payment_method,
				IF('.$this->tableName.'.status > 6, -1, '.$this->tableName.'.status) as status,
				CASE
					WHEN '.$this->tableName.'.is_admin_reservation = 0 THEN
						CASE
							WHEN '.TABLE_CUSTOMERS.'.user_name != \'\' THEN '.TABLE_CUSTOMERS.'.user_name
							ELSE \'without_account\'
						END
					ELSE \'admin\'
				END as customer_name,
				'.TABLE_CURRENCIES.'.symbol,
				'.TABLE_CURRENCIES.'.symbol_placement,
				CONCAT("<a href=\"index.php?'.$this->page.'&mg_action=description&oid=", '.$this->tableName.'.'.$this->primaryKey.', "\">", "'._DESCRIPTION.'", "</a>") as link_order_description
			FROM '.$this->tableName.'
				LEFT OUTER JOIN '.TABLE_CURRENCIES.' ON '.$this->tableName.'.currency = '.TABLE_CURRENCIES.'.code
				LEFT OUTER JOIN '.TABLE_CUSTOMERS.' ON '.$this->tableName.'.customer_id = '.TABLE_CUSTOMERS.'.id
			WHERE
				'.$this->tableName.'.'.$this->primaryKey.' = '.(int)$oid;
				
			if($this->user_id != ''){
				$sql .= ' AND '.$this->tableName.'.is_admin_reservation = 0 AND '.$this->tableName.'.customer_id = '.(int)$this->user_id;
			}
					
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($result[1] > 0){
			$content .= '<table '.((Application::Get('lang_dir') == 'rtl') ? 'dir="rtl"' : '').' width="100%" border="0">';
			$content .= '<tr><td width="210px">'._RESERVATION_NUMBER.': </td><td>'.$result[0]['reservation_number'].'</td></tr>';
			$content .= '<tr><td>'._DESCRIPTION.': </td><td>'.$result[0]['reservation_description'].'</td></tr>';
			$content .= '<tr><td>'._TRANSACTION.': </td><td>'.$result[0]['transaction_number'].'</td></tr>';
			$content .= '<tr><td>'._DATE_CREATED.': </td><td>'.$result[0]['created_date_formated'].'</td></tr>';
			$content .= '<tr><td>'._DATE_PAYMENT.': </td><td>'.($result[0]['payment_date_formated'] ? $result[0]['payment_date_formated'] : '--').'</td></tr>';
			if($this->user_id == ''){
				$content .= '<tr><td>'._CUSTOMER.': </td><td>';
				if($result[0]['customer_name'] == 'without_account'){
					$content .= '[ '._WITHOUT_ACCOUNT.' ]';
				}else if($result[0]['customer_name'] == 'admin'){
					$content .= _ADMIN;
				}else{
					$content .= $result[0]['customer_name'];
				}
				$content .= '</td></tr>';
			}
			$content .= '<tr><td>'._PAYMENT_TYPE.': </td><td>'.$this->arr_payment_types[$result[0]['payment_type']].'</td></tr>';
			$content .= '<tr><td>'._PAYMENT_METHOD.': </td><td>'.$this->arr_payment_methods[$result[0]['payment_method']].'</td></tr>';
			
			if($result[0]['payment_type'] == '1' && empty($mode)){
				// always show cc info, even if collecting is not requieed
				// $this->collect_credit_card == 'yes'
				$content .= '<tr><td>'._CREDIT_CARD_TYPE.': </td><td>'.$result[0]['cc_type'].'</td></tr>';
				$content .= '<tr><td>'._CREDIT_CARD_HOLDER_NAME.': </td><td>'.$result[0]['cc_holder_name'].'</td></tr>';
				if($this->user_id == ''){
					$content .= '<tr><td>'._CREDIT_CARD_NUMBER.': </td><td>'.$result[0]['cc_number'].$result[0]['cc_number_cleaned'].'</td></tr>';
					$content .= '<tr><td>'._CREDIT_CARD_EXPIRES.': </td><td>'.(($result[0]['cc_expires_month'] != '') ? $result[0]['cc_expires_month'].'/'.$result[0]['cc_expires_year'] : '').'</td></tr>';
					$content .= '<tr><td>'._CVV_CODE.': </td><td>'.$result[0]['cc_cvv_code'].'</td></tr>';				
				}else{
					$content .= '<tr><td>'._CREDIT_CARD_NUMBER.': </td><td>'.$result[0]['cc_number_for_customer'].'</td></tr>';
				}
			}

			$content .= '<tr><td>'._BOOKING_PRICE.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
			
			$content .= '<tr><td>'._BOOKING_SUBTOTAL.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';	

			if(!empty($result[0]['extras'])) $content .= '<tr><td>'._EXTRAS_SUBTOTAL.': </td><td>'.Currencies::PriceFormat($result[0]['extras_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';			
			if($this->vat_included_in_price == 'no') $content .= '<tr><td>'._VAT.': </td><td>'.Currencies::PriceFormat($result[0]['vat_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' ('.Currencies::PriceFormat($result[0]['vat_percent'], '%', 'right', $this->currencyFormat, $this->GetVatPercentDecimalPoints($result[0]['vat_percent'])).')</td></tr>';

			$reservation_price_plus_vat = Currencies::PriceFormat($result[0]['reservation_price'] + $result[0]['extras_fee'] + $result[0]['vat_fee'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
			$reservation_paid = Currencies::PriceFormat($result[0]['reservation_paid'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
			$reservation_paid_plus_additional = Currencies::PriceFormat($result[0]['reservation_paid'] + $result[0]['additional_payment'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
			$have_to_pay = Currencies::PriceFormat($result[0]['reservation_price'] + $result[0]['extras_fee'] + $result[0]['vat_fee'] - ($result[0]['reservation_paid'] + $result[0]['additional_payment']), $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
			$additional_payment = Currencies::PriceFormat($result[0]['additional_payment'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat);
            $link_to_payment = '&nbsp;&nbsp;&nbsp; <a href="index.php?page=cars_prepayment&rn='.$result[0]['reservation_number'].'" style="text-decoration:none;">'._CLICK_TO_PAY.' <img src="images/ppc_icons/logo_paypal.gif" alt="paypal" style="margin-top:-2px;margin-left:5px;"></a>';

			if($result[0]['pre_payment_type'] == 'percentage' && $result[0]['pre_payment_value'] > 0 && $result[0]['pre_payment_value'] < 100){
				$content .= '<tr><td>'._PAYMENT_SUM.': </td><td>'.$reservation_price_plus_vat.'</td></tr>';
				$content .= '<tr><td>'._PRE_PAYMENT.': </td><td>'.$reservation_paid.' ('._PARTIAL_PRICE.' - '.$result[0]['pre_payment_value'].'%)</td></tr>';
				if($result[0]['additional_payment'] != 0) $content .= '<tr><td>'._ADDITIONAL_PAYMENT.': </td><td>'.$additional_payment.'</td></tr>';
				if($have_to_pay > 0) $content .= '<tr><td style="color:#a60000">'._PAYMENT_REQUIRED.': </td><td style="color:#a60000">'.$have_to_pay.' '.$link_to_payment.'</td></tr>';
			}else if($result[0]['pre_payment_type'] == 'fixed sum' && $result[0]['pre_payment_value'] > 0){
				$content .= '<tr><td>'._PAYMENT_SUM.': </td><td>'.$reservation_price_plus_vat.'</td></tr>';
				$content .= '<tr><td>'._PRE_PAYMENT.': </td><td>'.$reservation_paid.' ('._PARTIAL_PRICE.' - '.Currencies::PriceFormat($result[0]['pre_payment_value'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).')</td></tr>';
				if($result[0]['additional_payment'] != 0) $content .= '<tr><td>'._ADDITIONAL_PAYMENT.': </td><td>'.$additional_payment.'</td></tr>';
				if($have_to_pay > 0) $content .= '<tr><td style="color:#a60000">'._PAYMENT_REQUIRED.': </td><td style="color:#a60000">'.$have_to_pay.' '.$link_to_payment.'</td></tr>';
			}else{
				$content .= '<tr><td>'._PAYMENT_SUM.': </td><td>'.$reservation_paid.'</td></tr>';
				$content .= '<tr><td>'._PRE_PAYMENT.': </td><td>'._FULL_PRICE.'</td></tr>';				
				if($result[0]['additional_payment'] != 0) $content .= '<tr><td>'._ADDITIONAL_PAYMENT.': </td><td>'.$additional_payment.'</td></tr>';
			}
			$content .= '<tr><td>'._TOTAL.': </td><td>'.$reservation_paid_plus_additional.'</td></tr>';
			//$content .= '<tr><td>'._TOTAL.': </td><td>'.Currencies::PriceFormat($result[0]['reservation_total_price'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).'</td></tr>';
			
			// Allow to pay required/additional sum if order is not canceled
			if($result[0]['mod_have_to_pay'] != 0 && $result[0]['status'] != 6){
				$content .= '<tr><td style="color:#960000">'._PAYMENT_REQUIRED.': </td><td style="color:#960000">'.Currencies::PriceFormat($result[0]['mod_have_to_pay'], $result[0]['symbol'], $result[0]['symbol_placement'], $this->currencyFormat).' '.$link_to_payment.'</td></tr>';
			}			

			$content .= '<tr><td>'._STATUS.': </td><td>'.$this->statuses_vm[$result[0]['status']].'</td></tr>';
			if($result[0]['additional_info'] != '') $content .= '<tr><td>'._ADDITIONAL_INFO.': </td><td>'.$result[0]['additional_info'].'</td></tr>';
			$content .= '<tr><td colspan="2">&nbsp;</tr>';
			$content .= '</table>';
			
			$content .= Extras::GetExtrasList(unserialize($result[0]['extras']), $result[0]['currency'], '', (($objLogin->IsLoggedInAsAdmin()) ? 'edit' : 'details'), $oid, 'index.php?admin=mod_car_rental_reservations');
		}else{
			$content .= draw_important_message(_WRONG_PARAMETER_PASSED, false);
		}

		$content .= $this->GetCarReservationList($oid, $language_id, (($objLogin->IsLoggedInAsAdmin()) ? 'edit' : 'details'));

		$output .= '<div id="divDescriptionContent">'.$content.'</div>';
		if(empty($mode)){
			$output .= '<div>';
			$output .= '<br /><input type="button" class="mgrid_button" name="btnBack" value="'._BUTTON_BACK.'" onclick="javascript:appGoTo(\''.$this->page.'\');">';
			$output .= '</div>';			
		}
		
		if($draw) echo $output;
		else return $output;
	}

	/**
	 * Before-Delete record
	 */
    public function BeforeDeleteRecord()
	{
		$oid = MicroGrid::GetParameter('rid');
		$sql = 'SELECT
					reservation_number,
					customer_id,
					is_admin_reservation,
					payment_type,
					status,
					(reservation_paid + additional_payment) as revert_sum
				FROM '.TABLE_CAR_AGENCY_RESERVATIONS.'
				WHERE id = '.(int)$oid;
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($result[1] > 0){
			
			$this->reservation_number = $result[0]['reservation_number'];
			$this->reservation_status = $result[0]['status'];
			$this->booking_customer_id = $result[0]['customer_id'];
			
			// If paid by balance ('payment_type' == '6') and order is not canceled ('status' != '6')
			if($result[0]['payment_type'] == '6' && $result[0]['status'] != '6'){
				$this->revert_sum = $result[0]['revert_sum'];	
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 *	After-Delete record
	 */	
	public function AfterDeleteRecord()
	{
		global $objLogin;

		$sql = 'DELETE FROM '.TABLE_BOOKINGS_ROOMS.' WHERE reservation_number = \''.encode_text($this->reservation_number).'\'';
		if($this->user_id != ''){
			$sql .= ' AND '.$this->tableName.'.is_admin_reservation = 0 AND '.$this->tableName.'.customer_id = '.(int)$this->user_id;
		}		
		if(!database_void_query($sql)){ /* echo 'error!'; */ }	 

		// Check if this is "by balance" payment and return money back to account balance
		if($this->allow_payment_with_balance && !empty($this->revert_sum)){
			$customer_type = Customers::GetCustomerInfo($this->booking_customer_id, 'customer_type');		
			// Check if this is "agency payment"
			if($customer_type == 1){
				$sql = 'UPDATE '.TABLE_CUSTOMERS.' SET balance = balance + '.$this->revert_sum.' WHERE id = '.(int)$this->booking_customer_id;
				database_void_query($sql);
			}
		}
	}

	/**
	 * Trigger method - allows to work with View Mode items
	 */
	protected function OnItemCreated_ViewMode($field_name, &$field_value)
	{
		if($field_name == 'customer_name' && $field_value == '{administrator}'){
			$field_value = _ADMIN;			
		}
    }
	
	/**
	 *	Update Payment Date
	 * 		@param $rid
	 */
	public function UpdatePaymentDate($rid)
	{
		$sql = 'UPDATE '.$this->tableName.'
				SET payment_date = \''.date('Y-m-d H:i:s').'\'
				WHERE
					'.$this->primaryKey.' = '.(int)$rid.' AND 
					status = 3 AND
					(payment_date = \'\' OR payment_date = \'0000-00-00\')';
		database_void_query($sql);		
	}
	
	
	/**
	 *	Cleans credit card info
	 * 		@param $rid
	 */
	public function CleanUpCreditCardInfo($rid)
	{
		if(strtolower(SITE_MODE) == 'demo'){
			$this->error = _OPERATION_BLOCKED;
			return false;
		}

		$sql = 'UPDATE '.$this->tableName.'
				SET
					cc_number = AES_ENCRYPT(SUBSTRING(AES_DECRYPT(cc_number, \''.PASSWORDS_ENCRYPT_KEY.'\'), -4), \''.PASSWORDS_ENCRYPT_KEY.'\'),
					cc_cvv_code = \'\',
					cc_expires_month = \'\',
					cc_expires_year = \'\'
				WHERE '.$this->primaryKey.' = '.(int)$rid;
		if(database_void_query($sql)){
			return true;
		}else{
			$this->error = _TRY_LATER;
		}		
	}
	
	/**
	 * Returns Rooms list for booking
	 * 		@param $oid
	 * 		@param $language_id
	 * 		@param $mode
	 */
	public function GetCarReservationList($oid, $language_id, $mode = 'details')
	{
		$output = '';
		$data = array();

        // display list of cars in order		
		$sql = 'SELECT
					'.$this->tableName. '.*,
					CONCAT(DATE_FORMAT('.$this->tableName.'.date_from, "'.$this->sqlFieldDateFormat.'"), " ", DATE_FORMAT('.$this->tableName.'.time_from, "%H:%i")) date_from,
					CONCAT(DATE_FORMAT('.$this->tableName.'.date_to, "'.$this->sqlFieldDateFormat.'"), " ", DATE_FORMAT('.$this->tableName.'.time_to, "%H:%i")) date_to
				FROM '.$this->tableName.'
				WHERE '.$this->tableName.'.id = '.(int)$oid;
		
		$result = database_query($sql, DATA_AND_ROWS, ALL_ROWS);		
		//dbug($result);
		
		if($result[1] > 0){
			$reservations_total = 0;

			$output .= '<h4>'._RESERVATION_DETAILS.'</h4>';
			$output .= '<table '.((Application::Get('lang_dir') == 'rtl') ? 'dir="rtl"' : '').' style="border:1px solid #d1d2d3" width="100%" border="0" cellspacing="0" cellpadding="3" class="tblReservationDetails">';
			$output .= '<thead><tr style=background-color:#e1e2e3;font-weight:bold;font-size:13px;">';
			$output .= '<th align="center"> # </th>';
			$output .= '<th align="left" width="100px">'._VEHICLE.'</th>';
			$output .= '<th align="left" width="120px">'._VEHICLE_TYPE.'</th>';
			$output .= '<th align="left">'._CAR_AGENCY.'</th>';
			$output .= '<th align="left" width="140px">'._PICKING_UP.'</th>';
			$output .= '<th align="left" width="140px">'._DROPPING_OFF.'</th>';
			$output .= '<th align="left" width="120px">'._FROM.'</th>';
			$output .= '<th align="left" width="120px">'._TO.'</th>';
			$output .= '<th align="right" width="120px">'._PRICE.'</th>';
			$output .= '<th width="5px" nowrap="nowrap"></th>';
			$output .= '</tr></thead>';
			
			for($i=0; $i < $result[1]; $i++){				
				$car_vehicle = isset($this->arr_vehicles[$result[0][$i]['car_vehicle_id']]) ? $this->arr_vehicles[$result[0][$i]['car_vehicle_id']] : ''; 
				$car_type = isset($this->arr_vehicle_types[$result[0][$i]['car_vehicle_type_id']]) ? $this->arr_vehicle_types[$result[0][$i]['car_vehicle_type_id']] : '';
				$car_agency = isset($this->arr_car_agencies[$result[0][$i]['car_agency_id']]) ? $this->arr_car_agencies[$result[0][$i]['car_agency_id']] : '';
				$picking_up = isset($this->arr_agencies_locations[$result[0][$i]['location_from_id']]) ? $this->arr_agencies_locations[$result[0][$i]['location_from_id']] : '';
				$dropping_off = isset($this->arr_agencies_locations[$result[0][$i]['location_to_id']]) ? $this->arr_agencies_locations[$result[0][$i]['location_to_id']] : '';
				$date_from = isset($result[0][$i]['date_from']) ? $result[0][$i]['date_from'] : '';
				$date_to = isset($result[0][$i]['date_to']) ? $result[0][$i]['date_to'] : '';								
				$price = isset($result[0][$i]['reservation_total_price']) ? $result[0][$i]['reservation_total_price'] : '';
				
				if($mode == 'invoice'){
					$data[$i]['car_vehicle'] = $car_vehicle;
					$data[$i]['car_type'] = $car_type;
					$data[$i]['car_agency'] = $car_agency;
					$data[$i]['picking_up'] = decode_text($picking_up);
					$data[$i]['dropping_off'] = decode_text($dropping_off);
					$data[$i]['date_from'] = $result[0][$i]['date_from'];
					$data[$i]['date_to'] = $result[0][$i]['date_to'];
					$data[$i]['reservation_total_price'] = $result[0][$i]['reservation_total_price'];
				}			

				$output .= '<tr>';
				$output .= '<td align="center" width="40px">'.($i+1).'.</td>';
				$output .= '<td align="left">'.$car_vehicle.'</td>';
				$output .= '<td align="left">'.$car_type.'</td>';
				$output .= '<td align="left">'.$car_agency.'</td>';
				$output .= '<td align="left">'.$picking_up.'</td>';
				$output .= '<td align="left">'.$dropping_off.'</td>';
				$output .= '<td align="left">'.$date_from.'</td>';
				$output .= '<td align="left">'.$date_to.'</td>';
				$output .= '<td align="right">'.$price.'</td>';
				$output .= '</tr>';
			}
			$output .= '</table>';			
		}		
		
		if($mode == 'invoice') return $data;
		else return $output;
	}
	
	//==========================================================================
    // Static Methods
	//==========================================================================
	/**
	 * Remove expired 'Prebooking' bookings
	 */
	public static function RemoveExpired()
	{
		global $objSettings;
		
		$prebooking_orders_timeout = (int)ModulesSettings::Get('car_rental', 'prebooking_orders_timeout');
		$sender = $objSettings->GetParameter('admin_email');
		// preapre datetime format
		if($objSettings->GetParameter('date_format') == 'mm/dd/yyyy'){
			$fieldDateFormat = 'M d, Y';
		}else{
			$fieldDateFormat = 'd M, Y';
		}
		$currencyFormat = get_currency_format();
		$language_id = Languages::GetDefaultLang();

		if($prebooking_orders_timeout > 0){
			
			$sql_delete = 'DELETE FROM '.TABLE_BOOKINGS.' WHERE status = 0 AND TIMESTAMPDIFF(HOUR, created_date, \''.date('Y-m-d H:i:s').'\') >= '.(int)$prebooking_orders_timeout;
			
			if(ModulesSettings::Get('car_rental', 'reservation_expired_alert') == 'yes'){
				$sql = 'SELECT
							'.TABLE_BOOKINGS.'.customer_id,
							'.TABLE_CUSTOMERS.'.first_name,
							'.TABLE_CUSTOMERS.'.last_name,
							'.TABLE_CUSTOMERS.'.preferred_language,
							'.TABLE_CUSTOMERS.'.email,
							'.TABLE_BOOKINGS.'.reservation_number,
							'.TABLE_BOOKINGS.'.created_date,
							'.TABLE_BOOKINGS.'.reservation_description,
							'.TABLE_BOOKINGS.'.rooms_amount,
							'.TABLE_BOOKINGS.'.reservation_price,
							'.TABLE_BOOKINGS.'.currency
						FROM '.TABLE_BOOKINGS.'
							LEFT OUTER JOIN '.TABLE_CUSTOMERS.' ON '.TABLE_BOOKINGS.'.customer_id = '.TABLE_CUSTOMERS.'.id
						WHERE '.TABLE_BOOKINGS.'.status = 0 AND
							TIMESTAMPDIFF(HOUR, '.TABLE_BOOKINGS.'.created_date, \''.date('Y-m-d H:i:s').'\') >= '.(int)$prebooking_orders_timeout;
				
				$result = database_query($sql, DATA_AND_ROWS, ALL_ROWS);
				if($result[1] > 0){
					$hotel_description = '';

						$hotel_info = Hotels::GetHotelFullInfo(0, $language_id);
						$hotel_description .= $hotel_info['name'].'<br>';
						$hotel_description .= $hotel_info['address'].'<br>';
						$hotel_description .= _PHONE.':'.$hotel_info['phone'];
						if($hotel_info['fax'] != '') $hotel_description .= ', '._FAX.':'.$hotel_info['fax'];

	
					for($i=0; $i < $result[1]; $i++){	
						$booking_details  = _RESERVATION_DESCRIPTION.': '.$result[0][$i]['reservation_description'].'<br />';
						$booking_details .= _CREATED_DATE.': '.format_datetime($result[0][$i]['created_date'], $fieldDateFormat.' H:i:s', '', true).'<br />';
						$booking_details .= _ROOMS.': '.$result[0][$i]['rooms_amount'].'<br />';
						$booking_details .= _BOOKING_PRICE.': '.Currencies::PriceFormat($result[0][$i]['reservation_price'], $result[0][$i]['currency'], 'left', $currencyFormat).'<br />';
						
						$recipient = $result[0][$i]['email'];
						$preferred_language = $result[0][$i]['preferred_language'];
						send_email(
							$recipient,
							$sender,
							'reservation_expired',
							array(
								'{FIRST NAME}' => $result[0][$i]['first_name'],
								'{LAST NAME}'  => $result[0][$i]['last_name'],
								'{BOOKING DETAILS}' => $booking_details,
								'{HOTEL INFO}' => ((!empty($hotel_description)) ? '<br>-----<br>'.$hotel_description : ''),
							),
							$preferred_language
						);
					}	
					return database_void_query($sql_delete);
				}							
			}else{
				return database_void_query($sql_delete);
			}			
		}
		return false;
	}
	
	/**
	 * Draw booking status
	 * 		@param $reservation_number
	 * 		@param $draw
	 */
	public function DrawBookingStatus($reservation_number = '', $draw = true)
	{			
		global $objSettings;
		$output = '';
		
		$sql = 'SELECT b.id
				FROM '.TABLE_BOOKINGS.' b
				WHERE
					(b.status = 2 OR b.status = 3) AND
					 b.reservation_number = \''.$reservation_number.'\'';
						
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);
		if($result[1] > 0){
			$output .= '<div style="float:'.Application::Get('defined_right').'"><a style="text-decoration:none;" href="javascript:void(\'booking|preview\')" onclick="javascript:appPreview(\'booking\');"><img src="images/printer.png" alt="" /> '._PRINT.'</a></div>';
			$output .= $this->DrawBookingDescription($result[0]['id'], 'check booking', false);
		}else{
			$this->error = draw_important_message(_NO_BOOKING_FOUND, false);
			$output .= $this->error;
		}
		
		if($draw) echo $output;
		else return $output;
	}	
	
	/**
	 *	Draws booking status block
	 *		@param $draw
	 */
	public static function DrawBookingStatusBlock($draw = true)
	{
		$output = '<form action="index.php?page=check_status" id="frmCheckBooking" name="frmCheckBooking" method="post">
			'.draw_hidden_field('task', 'check_status', false, 'task').'
			'.draw_token_field(false).'
			<table cellspacing="2" border="0">
			<tr><td>'._ENTER_RESERVATION_NUMBER.':</td></tr>
			<tr><td><input type="text" name="reservation_number" id="frmCheckBooking_reservation_number" maxlength="20" autocomplete="off" value="" /></td></tr>
			<tr><td style="height:3px"></td></tr>
			<tr><td><input class="form_button" type="submit" value="'._CHECK_STATUS.'" /></td></tr>
			</table>
		</form>';
	
		if($draw) echo $output;
		else return $output;
	}
	

	/**
	 * Get Vat Percent decimal points
	 * 		@param $vat_percent
	 */
	private function GetVatPercentDecimalPoints($vat_percent = '0')
	{
		return (substr($vat_percent, -1) == '0') ? 2 : 3;
	}
	
	/**
	 * Returns money to balance after canceled or deleted order
	 * @param int $rid		Order ID
	 * @return bool
	*/
	private function ReturnMoneyToBalance($rid = '0')
	{
		if(!$this->allow_payment_with_balance){
			return false;
		}
		
		// Check if this is "by balance" payment and return money back to account balance 
		$sql = 'SELECT
					customer_id,
					(reservation_paid + additional_payment) as revert_sum
				FROM '.TABLE_BOOKINGS.'
				WHERE id = '.(int)$rid.' AND payment_type = 6';		
		
		$result = database_query($sql, DATA_AND_ROWS, FIRST_ROW_ONLY);		
		if($result[1] > 0){
			$customer_id = $result[0]['customer_id'];
			$revert_sum = $result[0]['revert_sum'];
			$customer_type = Customers::GetCustomerInfo($customer_id, 'customer_type');
			
			// Check if this is "agency payment"
			if($customer_type == 1){
				$sql = 'UPDATE '.TABLE_CUSTOMERS.' SET balance = balance + '.$revert_sum.' WHERE id = '.(int)$customer_id;
				database_void_query($sql);
			}
		}
		
		return true;
	}
	
	/**
	 * Draws checkin/checkout list
	 * @param string $type
	 * @param string $date
	 */
	public static function DrawCheckList($type = 'checkin', $date = '')
	{
		global $objLogin;
		
		$output = '';
		$date_sql = empty($date) ? date('Y-m-d') : '';
		$where_clause = '';
		$language_id = Application::Get('lang');
	
		
		if($objLogin->IsLoggedInAs('agencyowner')){
			$hotels = $objLogin->AssignedToHotels();
			$agencies_list = implode(',', $hotels);
			if(!empty($agencies_list)) $where_clause .= ' AND br.hotel_id IN ('.$agencies_list.') ';
		}
		
		$sql = "SELECT
					b.reservation_number,
					b.additional_info,
					IF(
						b.is_admin_reservation = 1,
						'Admin',
						CONCAT(c.first_name, ' ', c.last_name)
					) as full_name,
					c.id as customer_id,
					c.customer_type,
					b.is_admin_reservation,
					br.checkin,
					br.checkout,
					br.room_numbers,
					r.room_type,
					hd.name as hotel_name
				FROM ".TABLE_BOOKINGS." b
					INNER JOIN ".TABLE_BOOKINGS_ROOMS." br ON b.reservation_number = br.reservation_number
					INNER JOIN ".TABLE_ROOMS." r ON br.room_id = r.id
					LEFT OUTER JOIN ".TABLE_CUSTOMERS." c ON b.customer_id = c.id
					LEFT OUTER JOIN ".TABLE_HOTELS_DESCRIPTION." hd ON r.hotel_id = hd.hotel_id AND hd.language_id = '".encode_text($language_id)."'
				WHERE
					1 = 1
					".$where_clause."
					".($type == 'checkout' ? " AND br.checkout = '".$date_sql."'" : " AND br.checkin = '".$date_sql."'");
					
		$result = database_query($sql, DATA_AND_ROWS, ALL_ROWS);

		$output = '<table class="tbl-dashboard-checkin" width="98%">';
		$output .= '<caption>'.($type == 'checkout' ? _TODAY_CHECKOUT : _TODAY_CHECKIN).'</caption>';
		$output .= '<thead>
			<tr>
				<th align="left">'._VISITOR.'</th>
				<th>'._RESERVATION_NUMBER.'</th>
				<th width="85px">'._CHECK_IN.'</th>
				<th width="85px">'._CHECK_OUT.'</th>
				<th>'._ROOM_TYPE.'</th>
				<th>'._ROOM_NUMBERS.'</th>
				<th>'._CAR_AGENCY.'</th>
				<th>'._INFO.'</th>
			<tr>
			</thead>';
		if($result[1] > 0){ 
			for($i = 0; $i < $result[1]; $i++){
				$output .= '<tr>
						<td>'.(!$result[0][$i]['is_admin_reservation'] ? '<a href="javascript:void(\'customer|view\')" onclick="open_popup(\'popup.ajax.php\',\'customer\',\''.$result[0][$i]['customer_id'].'\',\''.$result[0][$i]['customer_type'].'\',\''.Application::Get('token').'\')">'.htmlspecialchars($result[0][$i]['full_name']).'</a>' : htmlspecialchars($result[0][$i]['full_name'])).'</td>
						<td align="center">'.htmlspecialchars($result[0][$i]['reservation_number']).'</td>
						<td align="center">'.htmlspecialchars($result[0][$i]['checkin']).'</td>
						<td align="center">'.htmlspecialchars($result[0][$i]['checkout']).'</td>
						<td align="center">'.htmlspecialchars($result[0][$i]['room_type']).'</td>
						<td align="center">'.htmlspecialchars($result[0][$i]['hotel_name']).'</td>
						<td>'.htmlspecialchars($result[0][$i]['room_numbers']).'</td>
						<td>'.htmlspecialchars($result[0][$i]['additional_info']).'</td>
					<tr>';
			}
		}else{
			$output .= '<tr><td colspan="7">'._NO_RECORDS_FOUND.'</td></tr>';
		}
		$output .= '</table>';
		
		return $output;		
	}	
	
	/**
	 * Prepare Invoice Download
	 * 		@param $rid
	 */
	public function PrepareInvoiceDownload($rid)
	{
		if(strtolower(SITE_MODE) == "demo"){
			$this->error = _OPERATION_BLOCKED;
			return false;
		}

		global $objSettings;
		
		$data = $this->DrawCarRentalInvoice($rid, false, 'pdf', false);
		$agency_info = CarAgencies::GetAgencyFullInfo();
		
		//$pdf = new FPDF('P', 'pt', 'Letter'); /* Create fpdf object */		
        define('FPDF_FONTPATH', 'modules/tfpdf/font/');
        $pdf = new tFPDF();

        // Add a Unicode font (uses UTF-8)
        $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $pdf->SetFont('DejaVu','',9);        
		$pdf->SetTextColor(22,22,22);        /* Set the font color */		
		$pdf->SetFont('DejaVu', '', 24);     /* Set base font to start */		
		$pdf->AddPage();                     /* Add a new page to the document */		
		$pdf->SetXY(5, 5);                   /* Set the x,y coordinates of the cursor */
		
		$pdf->Cell(0,15, _INVOICE, 'B', 0, 'L');
		$pdf->Cell(0,5, '', 0, 1, 'L');
		
		$pdf->SetFont('DejaVu','',9);        /* Reset the font */
		$pdf->SetXY(5,20);                   /* Reset the cursor, write again. */		
		$pdf->Cell(0,10, _FOR_BOOKING.$data['reservation_number'], 0, 0, 'L');    $pdf->Cell(0,10, _DATE_CREATED.': '.$data['created_date'], 0, 1, 'R');

		$pdf->SetXY(5, 28);                  /* Reset the cursor, write again. */		
		$pdf->SetFont('DejaVu','',11);       /* Reset the font */
		$pdf->Cell(0,10, _CUSTOMER_DETAILS.':', 0, 0, 'L');    $pdf->Cell(0,10, $agency_info['name'], 0, 1, 'R');
		
		$pdf->SetFont('DejaVu','',9);        /* Reset the font */
		if($data['is_admin_reservation'] == '1'){
			$pdf->Cell(0,5, _ADMIN_RESERVATION, 0, 0, 'L');     if(!empty($agency_info['address'])) $pdf->Cell(0,5, _ADDRESS.': '.str_replace("\r\n", ' ', $agency_info['address']), 0, 1, 'R');
																if(!empty($agency_info['phone'])) $pdf->Cell(0,5, _PHONE.': '.$agency_info['phone'], 0, 1, 'R');
																if(!empty($agency_info['fax'])) $pdf->Cell(0,5, _FAX.': '.$agency_info['fax'], 0, 1, 'R');
																if(!empty($agency_info['email'])) $pdf->Cell(0,5, _EMAIL_ADDRESS.': '.$agency_info['email'], 0, 1, 'R');			
		}else{
			$pdf->Cell(0,5, $data['first_name'], 0, 0, 'L');    $pdf->Cell(0,5, _ADDRESS.': '.$agency_info['address'], 0, 1, 'R');
			$pdf->Cell(0,5, $data['last_name'], 0, 0, 'L');     $pdf->Cell(0,5, _PHONE.': '.$agency_info['phone'], 0, 1, 'R');
			$pdf->Cell(0,5, $data['email'], 0, 0, 'L');         $pdf->Cell(0,5, _FAX.': '.$agency_info['fax'], 0, 1, 'R');
			$pdf->Cell(0,5, $data['company'], 0, 0, 'L');       $pdf->Cell(0,5, _EMAIL_ADDRESS.': '.$agency_info['email'], 0, 1, 'R');
			$pdf->Cell(0,5, $data['phones'], 0, 2, 'L');          
			$pdf->Cell(0,5, $data['address'], 0, 2, 'L');          
			$pdf->Cell(0,5, $data['city'], 0, 2, 'L');
			$pdf->Cell(0,5, $data['country'], 0, 2, 'L');			
		}
		$pdf->Cell(0,5, '', 0, 1, 'L');
		
		// here table
		$pdf->SetFillColor(225,226,227); $pdf->SetFont('DejaVu','',9);
		$pdf->Cell(0,5, _BOOKING_DETAILS, 1, 2, 'L', true);
		// ----------------		
		$pdf->SetFont('DejaVu','',9);      /* Reset the font */
		$pdf->Cell(70,5, _STATUS.': ', 'L', 0, 'L');          	 $pdf->Cell(0,5, $data['reservation_status'], 'R', 1, 'L');
		$pdf->Cell(70,5, _RESERVATION_NUMBER.': ', 'L', 0, 'L'); $pdf->Cell(0,5, $data['reservation_number'], 'R', 1, 'L');
		$pdf->Cell(70,5, _DESCRIPTION.': ', 'LB', 0, 'L');     	 $pdf->Cell(0,5, $data['reservation_description'], 'RB', 1, 'L');
		$pdf->Cell(0,5, '', 0, 1, 'L');
		
		// payment details table
		$pdf->SetFillColor(225,226,227); $pdf->SetFont('DejaVu','',9);
		$pdf->Cell(0,5, _PAYMENT_DETAILS, 1, 2, 'L', true);
		// ----------------		
		$pdf->SetFont('DejaVu','',9);      /* Reset the font */
		$pdf->Cell(70,5, _TRANSACTION.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['transaction_number'], 'R', 1, 'L');
		$pdf->Cell(70,5, _DATE_PAYMENT.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['payment_date'], 'R', 1, 'L');
		$pdf->Cell(70,5, _PAYMENT_TYPE.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['payment_type'], 'R', 1, 'L');
		$pdf->Cell(70,5, _PAYMENT_METHOD.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['payment_method'], 'R', 1, 'L');
		$pdf->Cell(70,5, _BOOKING_PRICE.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['booking_price'], 'R', 1, 'L');
		$pdf->Cell(70,5, _BOOKING_SUBTOTAL.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['booking_subtotal'], 'R', 1, 'L');
		if($data['extras_subtotal'] != ''){ $pdf->Cell(70,5, _EXTRAS_SUBTOTAL.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['extras_subtotal'], 'R', 1, 'L'); }
		if($data['vat_fee'] != ''){ $pdf->Cell(70,5, _VAT.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['vat_fee'], 'R', 1, 'L'); }
		$pdf->Cell(70,5, _PRE_PAYMENT.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['pre_payment'], 'R', 1, 'L');
		$pdf->Cell(70,5, _ADDITIONAL_PAYMENT.': ', 'L', 0, 'L');   $pdf->Cell(0,5, $data['additional_payment'], 'R', 1, 'L');
		$pdf->Cell(70,5, _TOTAL.': ', 'LB', 0, 'L');   
		if($data['payment_required'] != ''){
			$pdf->Cell(0,5, $data['total'].' ('._PAYMENT_REQUIRED.' '.$data['payment_required'].')', 'RB', 1, 'L');			
		}else{
			$pdf->Cell(0,5, $data['total'], 'RB', 1, 'L');			
		}
		$pdf->Cell(0,5, '', 0, 1, 'L');

		// table (reservation details)
		$car_list = $this->GetCarReservationList($rid, Languages::GetDefaultLang(), 'invoice');
		$pdf->Cell(0,6, _RESERVATION_DETAILS, 0, 1, 'L');
		$pdf->SetFillColor(225,226,227); $pdf->SetFont('DejaVu','',9);
		$pdf->Cell(4,6, '#', 'LBT', 0, 'L', true);
		$pdf->Cell(16,6, _VEHICLE, 'BT', 0, 'L', true);
		$pdf->Cell(20,6, _VEHICLE_TYPE, 'BT', 0, 'L', true);
		$pdf->Cell(35,6, _CAR_AGENCY, 'BT', 0, 'L', true);
		$pdf->Cell(25,6, _PICKING_UP, 'BT', 0, 'L', true);
		$pdf->Cell(25,6, _DROPPING_OFF, 'BT', 0, 'L', true);
		$pdf->Cell(28,6, _FROM, 'BT', 0, 'L', true);
		$pdf->Cell(28,6, _TO, 'BT', 0, 'L', true);
		$pdf->Cell(0,6, _PRICE, 'BTR', 1, 'R', true);
		
		// ----------------
		$pdf->SetFont('DejaVu','',9);      /* Reset the font */
		$total_price = 0;
		for($i = 0; $i < count($car_list); $i++){
			$pdf->SetFont('DejaVu','',8);
			$pdf->Cell(4,6, ($i+1).'.', 'BL', 0, 'L');
			$pdf->Cell(16,6, $car_list[$i]['car_vehicle'], 'B', 0, 'L');
			$pdf->Cell(20,6, $car_list[$i]['car_type'], 'B', 0, 'L');
			$pdf->Cell(35,6, $car_list[$i]['car_agency'], 'B', 0, 'L');
			$pdf->Cell(25,6, $car_list[$i]['picking_up'], 'B', 0, 'L');
			$pdf->Cell(25,6, $car_list[$i]['dropping_off'], 'B', 0, 'L');
			$pdf->Cell(28,6, $car_list[$i]['date_from'], 'B', 0, 'L');
			$pdf->Cell(28,6, $car_list[$i]['date_to'], 'B', 0, 'L');
			$pdf->Cell(0,6, $car_list[$i]['reservation_total_price'], 'BR', 1, 'R');
			//$total_price += ($car_list[$i]['reservation_total_price']);
		}
		$pdf->SetFont('DejaVu','',9);      /* Reset the font */
		//$pdf->Cell(153,6, '', 0, 0, 'L');
		//$pdf->Cell(29,6, _TOTAL.': ', 'LBT', 0, 'L', true);
		//$pdf->Cell(15,6, $data['booking_currency'].number_format($total_room_price, 2), 'BTR', 0, 'R', true);
		
		// close the document and save to the filesystem with the name simple.pdf
		$pdf->Output('tmp/export/car_invoice.pdf','F');

		$this->message = _DOWNLOAD_INVOICE.' (PDF): <a href="javascript:void(\'pdf\')" onclick="javascript:appGoToPage(\'index.php?admin=export&file=car_invoice.pdf\');window.setTimeout(function(){location.reload()},2000);">'._FOR_BOOKING.$data['reservation_number'].'</a>';
		
		return true;
	}	
 	
}
