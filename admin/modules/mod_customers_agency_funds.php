<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------
	
if($objLogin->IsLoggedInAs('owner','mainadmin') && Modules::IsModuleInstalled('customers') && ModulesSettings::Get('customers', 'allow_agencies') == 'yes'){
	
	$action 	= MicroGrid::GetParameter('action');
	$rid    	= MicroGrid::GetParameter('rid');
	$agency_id	= (int)MicroGrid::GetParameter('aid', false);
	$mode   	= 'view';
	$msg 		= '';
	
	$objCustomers = new Customers();
	$agency_info = $objCustomers->GetCustomerInfo($agency_id);
	
	if(!empty($agency_id) && count($agency_info) > 0){	
		$objCustomerFunds = new CustomerFunds($agency_id);
		
		if($action=='add'){		
			$mode = 'add';
		}else if($action=='create'){
			if($objCustomerFunds->addrecord()){
				$msg = draw_success_message(_ADDING_OPERATION_COMPLETED, false);
				$mode = 'view';
			}else{
				$msg = draw_important_message($objCustomerFunds->error, false);
				$mode = 'add';
			}
		}else if($action=='edit'){
			$mode = 'edit';
		}else if($action=='update'){
			if($objCustomerFunds->UpdateRecord($rid)){
				$msg = draw_success_message(_UPDATING_OPERATION_COMPLETED, false);
				$mode = 'view';
			}else{
				$msg = draw_important_message($objCustomerFunds->error, false);
				$mode = 'edit';
			}		
		}else if($action=='delete'){
			if($objCustomerFunds->DeleteRecord($rid)){
				$msg = draw_success_message(_DELETING_OPERATION_COMPLETED, false);
			}else{
				$msg = draw_important_message($objCustomerFunds->error, false);
			}
			$mode = 'view';
		}else if($action=='details'){		
			$mode = 'details';		
		}else if($action=='cancel_add'){		
			$mode = 'view';		
		}else if($action=='cancel_edit'){				
			$mode = 'view';
		}
		
		$agency_name = isset($agency_info['company']) ? $agency_info['company'] : '';

		// Start main content
		draw_title_bar(
			prepare_breadcrumbs(array(_ACCOUNTS=>'',_CUSTOMERS_MANAGEMENT=>'',_AGENCIES=>'',$agency_name=>'',_BALANCE=>'',ucfirst($action)=>'')),
			prepare_permanent_link('index.php?admin=mod_customers_agencies', _BUTTON_BACK)
		);
		
		//if($objSession->IsMessage('notice')) echo $objSession->GetMessage('notice');
		echo $msg;
	
		draw_content_start();	
		if($mode == 'view'){
			$objCustomerFunds->DrawViewMode();
		}else if($mode == 'add'){		
			$objCustomerFunds->DrawAddMode();		
		}else if($mode == 'edit'){		
			$objCustomerFunds->DrawEditMode($rid);		
		}else if($mode == 'details'){		
			$objCustomerFunds->DrawDetailsMode($rid);		
		}
		
		draw_content_end();
	}else{
		draw_title_bar(_ADMIN);
		draw_important_message(_WRONG_PARAMETER_PASSED);		
	}
}else{
	draw_title_bar(_ADMIN);
	draw_important_message(_NOT_AUTHORIZED);
}
