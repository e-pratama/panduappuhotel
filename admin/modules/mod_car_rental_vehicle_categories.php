<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------

if(
	Modules::IsModuleInstalled('car_rental') &&
	($objLogin->IsLoggedInAs('owner','mainadmin') ||
	($objLogin->IsLoggedInAs('agencyowner') && ModulesSettings::Get('car_rental', 'is_active') != 'no'))
){	

	$action 	= MicroGrid::GetParameter('action');
	$rid    	= MicroGrid::GetParameter('rid');
	$mode   	= 'view';
	$msg 		= '';
	
	$objVehicleCategories = new VehicleCategories();

	if($action=='add'){		
		$mode = 'add';
	}else if($action=='create'){
		if($objVehicleCategories->AddRecord()){
			$msg = draw_success_message(_ADDING_OPERATION_COMPLETED, false);
			$mode = 'view';
		}else{
			$msg = draw_important_message($objVehicleCategories->error, false);
			$mode = 'add';
		}
	}else if($action=='edit'){
		$mode = 'edit';
	}else if($action=='update'){
		if($objVehicleCategories->UpdateRecord($rid)){
			$msg = draw_success_message(_UPDATING_OPERATION_COMPLETED, false);
			$mode = 'view';
		}else{
			$msg = draw_important_message($objVehicleCategories->error, false);
			$mode = 'edit';
		}		
	}else if($action=='delete'){
		if($objVehicleCategories->DeleteRecord($rid)){
			$msg = draw_success_message(_DELETING_OPERATION_COMPLETED, false);
		}else{
			$msg = draw_important_message($objVehicleCategories->error, false);
		}
		$mode = 'view';
	}else if($action=='details'){		
		$mode = 'details';		
	}else if($action=='cancel_add'){		
		$mode = 'view';		
	}else if($action=='cancel_edit'){				
		$mode = 'view';
	}
	
	// Start main content
	draw_title_bar(
		prepare_breadcrumbs(array(_CAR_RENTAL=>'',_SETTINGS=>'',_VEHICLE_CATEGORIES=>'',ucfirst($action)=>''))
	);	
	
	//if($objSession->IsMessage('notice')) echo $objSession->GetMessage('notice');
	echo $msg;

	draw_content_start();	
	$allow_viewing = true;
	if($objLogin->IsLoggedInAs('agencyowner')){
		$agencies_list = implode(',', $objLogin->AssignedToCarAgencies());
		if(empty($agencies_list)){
			$allow_viewing = false;
			echo draw_important_message(_CAR_OWNER_NOT_ASSIGNED, false);
		}
	}
	
	if($allow_viewing){
		if($mode == 'view'){
			$objVehicleCategories->DrawViewMode();	
		}else if($mode == 'add'){		
			$objVehicleCategories->DrawAddMode();		
		}else if($mode == 'edit'){		
			$objVehicleCategories->DrawEditMode($rid);		
		}else if($mode == 'details'){		
			$objVehicleCategories->DrawDetailsMode($rid);		
		}
	}
	draw_content_end();

}else{
	draw_title_bar(_ADMIN);
	draw_important_message(_NOT_AUTHORIZED);
}
