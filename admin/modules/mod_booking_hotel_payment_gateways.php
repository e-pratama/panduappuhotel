<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------

if($objLogin->IsLoggedInAs('hotelowner') && Modules::IsModuleInstalled('booking')){

	$action 	= MicroGrid::GetParameter('action');
	$rid    	= MicroGrid::GetParameter('rid');
	$mode   	= 'view';
	$msg 		= '';
	
	$objHotelPaymentGateways = new HotelPaymentGateways();
    
	if($action=='add'){		
		$mode = 'view';
	}else if($action=='create'){
        $mode = 'view';
	}else if($action=='edit'){
		$mode = 'edit';
	}else if($action=='update'){
        $info = $objHotelPaymentGateways->GetInfoByID($rid);
        $hotel_id = isset($info['hotel_id']) ? $info['hotel_id'] : '';                
        if(in_array($hotel_id, $objLogin->AssignedToHotels())){
            if($objHotelPaymentGateways->UpdateRecord($rid)){
                $msg = draw_success_message(_UPDATING_OPERATION_COMPLETED, false);
                $mode = 'view';
            }else{
                $msg = draw_important_message($objHotelPaymentGateways->error, false);
                $mode = 'edit';
            }
        }else{
            $msg = draw_important_message(_WRONG_PARAMETER_PASSED, false);
            $mode = 'view';
        }        
	}else if($action=='delete'){
		$mode = 'view';
	}else if($action=='details'){		
		$mode = 'details';		
	}else if($action=='cancel_add'){		
		$mode = 'view';		
	}else if($action=='cancel_edit'){				
		$mode = 'view';
    }else{
        $action = '';
	}
	
	// Start main content
	draw_title_bar(prepare_breadcrumbs(array(_BOOKINGS=>'',_SETTINGS=>'',_HOTEL_PAYMENT_GATEWAYS=>'',ucfirst($action)=>'')));
    	
	//if($objSession->IsMessage('notice')) echo $objSession->GetMessage('notice');
	echo $msg;

	draw_content_start();	
	if($mode == 'view'){		
		$objHotelPaymentGateways->DrawViewMode();	
	}else if($mode == 'edit'){		
		$objHotelPaymentGateways->DrawEditMode($rid);		
	}
	draw_content_end();	

}else{
	draw_title_bar(_ADMIN);
	draw_important_message(_NOT_AUTHORIZED);
}

