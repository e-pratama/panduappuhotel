<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------
	
if(Modules::IsModuleInstalled('rooms') && 
  ($objLogin->IsLoggedInAs('owner','mainadmin') || ($objLogin->IsLoggedInAs('hotelowner') && $objLogin->HasPrivileges('edit_hotel_rooms')))
){
	
	$action 	= MicroGrid::GetParameter('action');
	$rid    	= MicroGrid::GetParameter('rid');
	$mode   = 'view';
	$msg 	= '';
	
	$hotel_id = Rooms::GetRoomInfo($rid, 'hotel_id');	
	if(in_array($rid, array(0, -1)) || (!empty($hotel_id) && $objLogin->AssignedToHotel($hotel_id))){

		$objRooms = new Rooms();
	
		if($action=='add'){		
			$mode = 'add';
		}else if($action=='create'){
			if($objRooms->AddRecord()){
				$msg = draw_success_message(_ADDING_OPERATION_COMPLETED, false);
				$mode = 'view';
			}else{
				$msg = draw_important_message($objRooms->error, false);
				$mode = 'add';
			}
		}else if($action=='edit'){
			$mode = 'edit';
		}else if($action=='update'){
			if($objRooms->UpdateRecord($rid)){
				$msg = draw_success_message(_UPDATING_OPERATION_COMPLETED, false);
				$mode = 'view';
			}else{
				$msg = draw_important_message($objRooms->error, false);
				$mode = 'edit';
			}		
		}else if($action=='delete'){
			if($objRooms->DeleteRecord($rid)){
				$msg = draw_success_message(_DELETING_OPERATION_COMPLETED, false);
			}else{
				$msg = draw_important_message($objRooms->error, false);
			}
			$mode = 'view';
		}else if($action=='details'){		
			$mode = 'details';		
		}else if($action=='cancel_add'){		
			$mode = 'view';		
		}else if($action=='cancel_edit'){				
			$mode = 'view';
		}
		
		// Start main content
		draw_title_bar(prepare_breadcrumbs(array(_HOTELS_MANAGEMENT=>'',_HOTELS_AND_ROMS=>'',_ROOMS_MANAGEMENT=>'',ucfirst($action)=>'')));
		
		//if($objSession->IsMessage('notice')) echo $objSession->GetMessage('notice');
		echo $msg;
	
		draw_content_start();	
	
		$allow_viewing = true;
		if($objLogin->IsLoggedInAs('hotelowner')){
			$hotels_list = implode(',', $objLogin->AssignedToHotels());
			if(empty($hotels_list)){
				$allow_viewing = false;
				echo draw_important_message(_OWNER_NOT_ASSIGNED, false);
			}
		}
		
		if($allow_viewing){
			if($mode == 'view'){		
				$objRooms->DrawViewMode();				
			}else if($mode == 'add'){
				$objRooms->DrawAddMode();		
			}else if($mode == 'edit'){		
				$objRooms->DrawEditMode($rid);				
			}else if($mode == 'details'){		
				$objRooms->DrawDetailsMode($rid);		
			}
			
			// Discount type pre/postfix handler
			if(in_array($mode, array('add', 'edit', 'details'))){
				echo '<script type="text/javascript">
						$(document).ready(function(){
							setDiscountType("'.$objRooms->GetFieldInfo('discount_type').'");
							$("#discount_type").change(function(){
								setDiscountType($(this).val());
							});							
							function setDiscountType(dtype){
								if(dtype == "1"){
									$(".discount-night-price").html("");
									$(".discount-night-percent").html("%");
								}else{
									$(".discount-night-price").html($(".discount-night-price").data("currency"));
									$(".discount-night-percent").html("");									
								}								
							}
						});
				</script>';
			}
		}
		draw_content_end();

	}else{
		draw_title_bar(
			prepare_breadcrumbs(array(_HOTELS_MANAGEMENT=>'',_ROOMS_MANAGEMENT=>''))
		);
		draw_important_message(_WRONG_PARAMETER_PASSED);		
	}	
}else{
	draw_title_bar(_ADMIN);
	draw_important_message(_NOT_AUTHORIZED);
}

