<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------

if($objLogin->IsLoggedInAsAdmin()){
	
	$task = isset($_GET['task']) ? prepare_input($_GET['task']) : '';
	$alert_state = Session::Get('alert_state');

	if($task == 'close_alert'){
		$alert_state = 'hidden';
	    Session::Set('alert_state', 'hidden');
	}else if($task == 'open_alert'){
		$alert_state = '';
		Session::Set('alert_state', '');
	}

    draw_title_bar(prepare_breadcrumbs(array(_GENERAL=>'',_HOME=>'')));
    
    ///database_void_query("INSERT INTO `aphs_vocabulary` (`id`, `language_id`, `key_value`, `key_text`) VALUES (NULL, 'en', '_PREFERENCES', 'Preferences'), (NULL, 'es', '_PREFERENCES', 'Preferencias'), (NULL, 'de', '_PREFERENCES', 'Einstellungen')");
    
    // draw important messages 
	// ---------------------------------------------------------------------
    $actions_msg = array();
    if($objLogin->IsLoggedInAs('owner', 'mainadmin') && (file_exists('install.php') || file_exists('install/'))){
    	$actions_msg[] = '<span class="darkred">'._INSTALL_PHP_EXISTS.'</span>';
    }

	if($objLogin->IsLoggedInAs('owner', 'mainadmin')){
		if(SITE_MODE == 'development') $actions_msg[] = '<span class="darkred">'._SITE_DEVELOPMENT_MODE_ALERT.'</span>';		
	}

	// test mode alert
	if(Modules::IsModuleInstalled('booking') && $objLogin->IsLoggedInAs('owner', 'mainadmin', 'admin', 'hotelowner')){
		if(ModulesSettings::Get('booking', 'mode') == 'TEST MODE'){
			$actions_msg[] = '<span class="darkred">'._TEST_MODE_ALERT.'</span>';
		}        
	}

	if($objLogin->IsLoggedInAs('owner', 'mainadmin')){
		$arr_folders = array('images/upload/', 'images/flags/', 'images/hotels/', 'images/rooms/', 'images/banners/', 'images/gallery/', 'tmp/backup/', 'tmp/export/', 'tmp/cache/', 'tmp/logs/', 'feeds/');
		$arr_folders_not_writable = '';
		foreach($arr_folders as $folder){
			if(!is_writable($folder)){
				if($arr_folders_not_writable != '') $arr_folders_not_writable .= ', ';
				$arr_folders_not_writable .= $folder;
			}
		}
		if($arr_folders_not_writable != ''){
			$actions_msg[] = _NO_WRITE_ACCESS_ALERT.' <b>'.$arr_folders_not_writable.'</b>';
		}
    
		$admin_email = $objSettings->GetParameter('admin_email');    
		if($objLogin->IsLoggedInAs('owner', 'mainadmin') && ($admin_email == '' || preg_match('/yourdomain/i', $admin_email))){
			$actions_msg[] = _DEFAULT_EMAIL_ALERT;
		}
	}
	
	$own_email = $objLogin->GetLoggedEmail();    
	if($own_email == '' || preg_match('/yourdomain/i', $own_email)){
		$actions_msg[] = _DEFAULT_OWN_EMAIL_ALERT;
	}
    
	if($objLogin->IsLoggedInAs('owner', 'mainadmin') && Modules::IsModuleInstalled('contact_us')){
        $admin_email_to = ModulesSettings::Get('contact_us', 'email');
        if($admin_email_to == '' || preg_match('/yourdomain/i', $admin_email_to)){
            $actions_msg[] = _CONTACTUS_DEFAULT_EMAIL_ALERT;
        }
    }    
	
	if($objLogin->IsLoggedInAs('owner', 'mainadmin', 'admin') && Modules::IsModuleInstalled('comments')){
		$comments_allow	= ModulesSettings::Get('comments', 'comments_allow');
		$comments_count = Comments::AwaitingModerationCount();
		if($comments_allow == 'yes' && $comments_count > 0){
			$actions_msg[] = str_replace('_COUNT_', $comments_count, _COMMENTS_AWAITING_MODERATION_ALERT);			
		}
	}
	
	if($objLogin->IsLoggedInAs('owner', 'mainadmin') && ModulesSettings::Get('customers', 'reg_confirmation') == 'by admin'){
		$customers_count = Customers::AwaitingAprovalCount();
		if($customers_count > 0){
			$actions_msg[] = str_replace('_COUNT_', $customers_count, _CUSTOMERS_AWAITING_MODERATION_ALERT);			
		}
	}	
	
    if(count($actions_msg) > 0){
		if($alert_state == ''){
			$msg = '<div id="divAlertMessages">
				<img src="images/close.png" alt="" style="cursor:pointer;float:'.Application::Get('defined_right').';margin-right:-3px;" title="'._HIDE.'" onclick="javascript:appGoTo(\'admin=home\',\'&task=close_alert\')" />
				<img src="images/action_required.png" alt="" style="margin-bottom:-3px;" /> &nbsp;&nbsp;<b>'._ACTION_REQUIRED.'</b>: 
				<ul>';
				foreach($actions_msg as $single_msg){
					$msg .= '<li>'.$single_msg.'</li>';
				}
			$msg .= '</ul></div>';
			draw_important_message($msg, true, false);        			
		}else{
			echo '<div id="divAlertRequired"><a href="javascript:void(0);" onclick="javascript:appGoTo(\'admin=home\',\'&task=open_alert\')">'._OPEN_ALERT_WINDOW.'</a></div>';
		}
    }

	# Draw Check-In/Check-Out information for all admins excluding car agency owners
	if(!$objLogin->IsLoggedInAs('agencyowner')){
		$msg = '<div style="padding:9px;">
		<p>'._TODAY.': <b>'.format_datetime(date('Y-m-d H:i:s'), '', '', true).'</b></p>
		
		<table width="100%">
		<tr>
			<td valign="top">'.Bookings::DrawCheckList('checkin').'</td>
			<td valign="top">'.Bookings::DrawCheckList('checkout').'</td>
		</tr>
		</table>
		</div>';
		draw_message($msg, true, false);
	}

	$msg = '<div style="padding:9px;">
	<div class="site_version">'._VERSION.': '.CURRENT_VERSION.'</div>
	<p>'._LAST_LOGIN.': <b>'.format_datetime($objLogin->GetLastLoginTime(), '', _NEVER, true).'</b></p>';
	if($objLogin->IsLoggedInAs('agencyowner')){
		$msg .= '<br>'._AGENCYOWNER_WELCOME_TEXT;
	}else if ($objLogin->IsLoggedInAs('hotelowner')){
		$msg .= '<br>'._HOTELOWNER_WELCOME_TEXT;
	}else{
		$msg .= _ADMIN_WELCOME_TEXT;
	}	
	
	$msg .= '</div>';
	draw_default_message($msg, true, false);

    // draw dashboard modules
	$objModules = new Modules();
	echo '<div style="padding:2px 2px 40px 2px;">';
	$objModules->DrawModulesOnDashboard();
	echo '<div style="clear:both;"></div>';
	echo '</div>';

	echo '<div style="text-align:right;padding:20px 18px 0 0;vertical-align:bottom;">'.$objSiteDescription->DrawFooter(false).'</div>';
	
}else{
	draw_title_bar(_ADMIN);
	draw_important_message(_NOT_AUTHORIZED);
}
