<?php
/**
* @project uHotelBooking
* @copyright (c) 2016 ApPHP
* @author ApPHP <info@apphp.com>
* @site http://www.hotel-booking-script.com
* @license http://hotel-booking-script.com/license.php
*/

// *** Make sure the file isn't accessed directly
defined('APPHP_EXEC') or die('Restricted Access');
//--------------------------------------------------------------------------

$room_id 				= isset($_POST['room_id']) ? prepare_input($_POST['room_id']) : '';

if(CALENDAR_HOTEL == 'new'){

	$checkin_date		 	= isset($_POST['checkin_date']) ? prepare_input($_POST['checkin_date']) : date('m/d/Y');
	$checkin_parts			= explode('/', $checkin_date);
	$checkin_month 			= isset($checkin_parts[0]) ? convert_to_decimal((int)$checkin_parts[0]) : '';
	$checkin_day 			= isset($checkin_parts[1]) ? convert_to_decimal((int)$checkin_parts[1]) : '';
	$checkin_year 			= isset($checkin_parts[2]) ? (int)$checkin_parts[2] : '';

	$checkout_date			= isset($_POST['checkout_date']) ? prepare_input($_POST['checkout_date']) : date('m/d/Y', time() + (24 * 60 * 60));
	$checkout_parts			= explode('/', $checkout_date);
	$checkout_month 		= isset($checkout_parts[0]) ? convert_to_decimal((int)$checkout_parts[0]) : '';
	$checkout_day			= isset($checkout_parts[1]) ? convert_to_decimal((int)$checkout_parts[1]) : '';
	$checkout_year 			= isset($checkout_parts[2]) ? (int)$checkout_parts[2] : '';
	
	$checkin_year_month 	= $checkin_year.'-'.(int)$checkin_month;
	$checkout_year_month 	= $checkout_year.'-'.(int)$checkout_month;

	// max date - 2 years
	$max_date_unix = mktime(0, 0, 0, date('m'), date('d'), date('Y') + 2) + (24 * 3600);
	$min_date_unix = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
}else{
	$checkin_year_month 	= isset($_POST['checkin_year_month']) ? prepare_input($_POST['checkin_year_month']) : date('Y').'-'.(int)date('m');
	$checkin_year_month_parts = explode('-', $checkin_year_month);
	$checkin_year 			= isset($checkin_year_month_parts[0]) ? $checkin_year_month_parts[0] : '';
	$checkin_month 			= isset($checkin_year_month_parts[1]) ? convert_to_decimal($checkin_year_month_parts[1]) : '';
	$checkin_day 			= isset($_POST['checkin_monthday']) ? convert_to_decimal($_POST['checkin_monthday']) : date('d');

	$curr_date 				= mktime(0, 0, 0, date('m'), date('d')+1, date('y'));
	$checkout_year_month 	= isset($_POST['checkout_year_month']) ? prepare_input($_POST['checkout_year_month']) : date('Y').'-'.(int)date('m');
	$checkout_year_month_parts = explode('-', $checkout_year_month);
	$checkout_year 			= isset($checkout_year_month_parts[0]) ? $checkout_year_month_parts[0] : '';
	$checkout_month 		= isset($checkout_year_month_parts[1]) ? convert_to_decimal($checkout_year_month_parts[1]) : '';
	$checkout_day 			= isset($_POST['checkout_monthday']) ? convert_to_decimal($_POST['checkout_monthday']) : date('d', $curr_date);
}

$max_adults 			= isset($_POST['max_adults']) ? (int)$_POST['max_adults'] : '';
$max_children 			= isset($_POST['max_children']) ? (int)$_POST['max_children'] : '';
$sort_by                = isset($_POST['sort_by']) && in_array($_POST['sort_by'], array('stars-1-5', 'stars-5-1', 'name-a-z', 'name-z-a', 'price-l-h', 'price-h-l')) ? prepare_input($_POST['sort_by']) : '';
$hotel_sel_id           = isset($_POST['hotel_sel_id']) ? prepare_input($_POST['hotel_sel_id']) : '';
$hotel_sel_loc_id       = isset($_POST['hotel_sel_loc_id']) ? prepare_input($_POST['hotel_sel_loc_id']) : '';
$property_type_id       = isset($_REQUEST['property_type_id']) ? (int)$_REQUEST['property_type_id'] : '';

// Prepare property ID
$property_types = Application::Get('property_types');
if(!in_sub_array('id', $property_type_id, $property_types)){
	$property_type_id = isset($property_types[0]['id']) ? $property_types[0]['id']: 0;
}

$nights = nights_diff($checkin_year.'-'.$checkin_month.'-'.$checkin_day, $checkout_year.'-'.$checkout_month.'-'.$checkout_day);
$search_availability_period = ModulesSettings::Get('rooms', 'search_availability_period');
$search_availability_period_in_days = ($search_availability_period * 365) + 1;

draw_title_bar(_AVAILABLE_ROOMS);

// Check if there is a page
if($nights > $search_availability_period_in_days){
	draw_important_message(str_replace('_DAYS_', $search_availability_period_in_days, _MAXIMUM_PERIOD_ALERT));
}elseif((CALENDAR_HOTEL == 'new' && (empty($checkin_date) || empty($checkout_date))) || CALENDAR_HOTEL != 'new' && ($checkin_year_month == '0' || $checkin_day == '0' || $checkout_year_month == '0' || $checkout_day == '0')){
	draw_important_message(_WRONG_PARAMETER_PASSED);
}else if(CALENDAR_HOTEL == 'new' && $min_date_unix > mktime(0, 0, 0, $checkin_month, $checkin_day, $checkin_year)){
	draw_important_message(_WRONG_PARAMETER_PASSED);
}else if(CALENDAR_HOTEL == 'new' && $max_date_unix < mktime(0, 0, 0, $checkout_month, $checkout_day, $checkout_year)){
	draw_important_message(_WRONG_PARAMETER_PASSED);
}else if(!checkdate($checkout_month, $checkout_day, $checkout_year)){
	draw_important_message(_WRONG_CHECKOUT_DATE_ALERT);
}else if(ModulesSettings::Get('booking', 'allow_booking_in_past') != 'yes' && $checkin_year.$checkin_month.$checkin_day < date('Ymd')){
	draw_important_message(_PAST_TIME_ALERT);		
}else if($nights < 1){
	draw_important_message(_BOOK_ONE_NIGHT_ALERT);
}else if(Modules::IsModuleInstalled('booking')){

	$min_nights = ModulesSettings::Get('booking', 'minimum_nights');
	$max_nights = ModulesSettings::Get('booking', 'maximum_nights');	

	if($nights < $min_nights){		
		echo draw_important_message(str_replace('_DAYS_', $min_nights, _MINIMUM_PERIOD_ALERT));
	}else if($nights > $max_nights){
		echo draw_important_message(str_replace('_DAYS_', $max_nights, _MAXIMUM_PERIOD_ALERT));
	}else{		
		$min_max_hotels = array();
	
		// -----------------------------------------------------
		// Find general min night via all packages
		// -----------------------------------------------------
		$min_nights_packages = Packages::GetMinimumNights($checkin_year.'-'.$checkin_month.'-'.$checkin_day, $checkout_year.'-'.$checkout_month.'-'.$checkout_day, $hotel_sel_id, true);
		///dbug($min_nights_packages);
		if(!empty($min_nights_packages) && is_array($min_nights_packages)){
			$packages_min_nights = '';
			foreach($min_nights_packages as $key => $package){
				if(!empty($package['hotel_id'])){				
					if($package['minimum_nights'] < $packages_min_nights || $packages_min_nights === ''){
						$packages_min_nights = (int)$package['minimum_nights'];
					}
					$min_max_hotels[$package['hotel_id']] = array(
						'package_name' 		=> $package['package_name'],
						'minimum_nights' 	=> $package['minimum_nights'],
						'maximum_nights' 	=> '',
						'partial_nights'	=> '',
						'start_date'		=> $package['start_date'],
						'finish_date'		=> $package['finish_date'],
						'hotel_name' 		=> $package['hotel_name'],
					);
				}
			}
			
			if(!empty($packages_min_nights) && $packages_min_nights < $min_nights){
				$min_nights = $packages_min_nights;
			}
		}
		
		// -----------------------------------------------------
		// Find general max night via all packages
		// -----------------------------------------------------
		$max_nights_packages = Packages::GetMaximumNights($checkin_year.'-'.$checkin_month.'-'.$checkin_day, $checkout_year.'-'.$checkout_month.'-'.$checkout_day, $hotel_sel_id, true);
		//dbug($max_nights_packages,1);
		if(!empty($max_nights_packages) && is_array($max_nights_packages)){
			$packages_max_nights = '';
			foreach($max_nights_packages as $key => $package){
				if(!empty($package['hotel_id'])){
					if($package['maximum_nights'] > $packages_max_nights || $packages_max_nights === ''){
						$packages_max_nights = (int)$package['maximum_nights'];
					}
					
					if(!isset($min_max_hotels[$package['hotel_id']])){
						$min_max_hotels[$package['hotel_id']] = array(
							'package_name' 		=> $package['package_name'],
							'minimum_nights' 	=> '',
							'maximum_nights' 	=> $package['maximum_nights'],
							'partial_nights'	=> '',
							'start_date'		=> $package['start_date'],
							'finish_date'		=> $package['finish_date'],
							'hotel_name' 		=> $package['hotel_name'],					
						);					
					}else{
						$min_max_hotels[$package['hotel_id']]['maximum_nights'] = $package['maximum_nights'];
					}
				}
			}
			
			if(!empty($packages_max_nights) && $packages_max_nights > $max_nights){
				$max_nights = $packages_max_nights;
			}
		}
	
		// -----------------------------------------------------
		// Handle partial settings
		// -----------------------------------------------------
		$check_partially_overlapping = ModulesSettings::Get('rooms', 'check_partially_overlapping');
		$partial_nights = 0;
		// [#001] force check for patial package period of time
		if($check_partially_overlapping == 'yes'){
			foreach($min_nights_packages as $key => $package){
				if(empty($package['hotel_id'])){
					continue;
				}
				$from_part_package = strtotime($package['start_date']);
				$to_part_package = strtotime($package['finish_date']);
				
				$partial_nights = 0;
				for($i = 0; $i < $nights; $i++){
					$part_reservation_date = strtotime($checkin_year.'-'.$checkin_month.'-'.$checkin_day . '+'. $i .'day');
					if($from_part_package <= $part_reservation_date && $to_part_package >= $part_reservation_date){
						$partial_nights++;
					}
				}
				$min_max_hotels[$package['hotel_id']]['partial_nights'] = $partial_nights;
			}			
		}
		
		
		foreach($min_max_hotels as $key => $min_max_hotel){
				
			$min_max_alert = '';
			if(!empty($min_max_hotel['minimum_nights']) && $nights < $min_max_hotel['minimum_nights']){				
				$min_max_alert = draw_important_message(
					str_replace(array('_IN_HOTEL_', '_PACKAGE_NAME_', '_NIGHTS_', '_FROM_', '_TO_'),
								array($min_max_hotel['hotel_name'].'. ', $min_max_hotel['package_name'], '<b>'.$min_max_hotel['minimum_nights'].'</b>', '<b>'.format_date($min_max_hotel['start_date']).'</b>', '<b>'.format_date($min_max_hotel['finish_date']).'</b>'),
								_MINIMUM_NIGHTS_ALERT
					), false
				);
			}else if(!empty($min_max_hotel['maximum_nights']) && $nights > $min_max_hotel['maximum_nights']){
				$min_max_alert = draw_important_message(
					str_replace(array('_IN_HOTEL_', '_PACKAGE_NAME_', '_NIGHTS_', '_FROM_', '_TO_'),
								array($min_max_hotel['hotel_name'].'. ', $min_max_hotel['package_name'], '<b>'.$min_max_hotel['maximum_nights'].'</b>', '<b>'.format_date($min_max_hotel['start_date']).'</b>', '<b>'.format_date($min_max_hotel['finish_date']).'</b>'),
								_MAXIMUM_NIGHTS_ALERT
					), false
				);
		    }else if($check_partially_overlapping == 'yes' && !empty($min_max_hotel['partial_nights']) && $min_max_hotel['partial_nights'] < $min_max_hotel['minimum_nights']){
				// [#002] force check for patial package period of time
		        $min_max_alert = draw_important_message(
					str_replace(array('_IN_HOTEL_', '_PACKAGE_NAME_', '_NIGHTS_', '_FROM_', '_TO_'),
								array($min_max_hotel['hotel_name'].'. ', $min_max_hotel['package_name'], '<b>'.$min_max_hotel['minimum_nights'].'</b>', '<b>'.format_date($min_max_hotel['start_date']).'</b>', '<b>'.format_date($min_max_hotel['finish_date']).'</b>'),
								_MINIMUM_NIGHTS_ALERT
					), false
		        );
			}
			
			$min_max_hotels[$key]['alert'] = !empty($min_max_alert) ? $min_max_alert : '';
		}
		
		$nights_text = ($nights > 1) ? $nights.' '._NIGHTS : $nights.' '._NIGHT;
		
		draw_content_start();
		if($objSettings->GetParameter('date_format') == 'mm/dd/yyyy'){
			draw_sub_title_bar(_FROM.': '.get_month_local($checkin_month).' '.$checkin_day.', '.$checkin_year.' '._TO.': '.get_month_local($checkout_month).' '.$checkout_day.', '.$checkout_year.' ('.$nights_text.')', true, 'h4');
		}else{
			draw_sub_title_bar(_FROM.': '.$checkin_day.' '.get_month_local($checkin_month).' '.$checkin_year.' '._TO.': '.$checkout_day.' '.get_month_local($checkout_month).' '.$checkout_year.' ('.$nights_text.')', true, 'h4');
		}
		
		$objRooms = new Rooms();
		$params = array(
			'room_id'     		=> $room_id,
			'from_date'	  		=> $checkin_year.'-'.$checkin_month.'-'.$checkin_day,
			'to_date'	  		=> $checkout_year.'-'.$checkout_month.'-'.$checkout_day,
			'nights'	  		=> $nights,
			'from_year'	  		=> $checkin_year,
			'from_month'  		=> $checkin_month,
			'from_day'	  		=> $checkin_day,
			'to_year'	  		=> $checkout_year,
			'to_month'	  		=> $checkout_month,
			'to_day'	  		=> $checkout_day,
			'max_adults'  		=> $max_adults,
			'max_children'     	=> $max_children,
			'sort_by'          	=> $sort_by,
			'hotel_sel_id'     	=> $hotel_sel_id,
			'hotel_sel_loc_id' 	=> $hotel_sel_loc_id,
            'property_type_id' 	=> $property_type_id,
			'min_max_hotels'	=> $min_max_hotels
		);
		
		$rooms_count = $objRooms->SearchFor($params);
		
		echo '<div class="line-hor"></div>';
		
		if($rooms_count['rooms'] > 0){
			$objRooms->DrawSearchResult($params, $rooms_count['rooms']);			
		}else{
			draw_important_message(_NO_ROOMS_FOUND);
			draw_message(_SEARCH_ROOM_TIPS);
			
			if(ModulesSettings::Get('rooms', 'allow_system_suggestion') == 'yes'){
				Rooms::DrawTrySystemSuggestionForm($room_id, $checkin_day, $checkin_year_month, $checkout_day, $checkout_year_month, $max_adults, $max_children);				
			}
		}
		draw_content_end();	
	}
}
