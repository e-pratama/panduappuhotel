DROP TABLE IF EXISTS phpn_accounts;

CREATE TABLE `phpn_accounts` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(70) CHARACTER SET latin1 NOT NULL,
  `account_type` enum('owner','mainadmin','admin','hotelowner','agencyowner') CHARACTER SET latin1 NOT NULL DEFAULT 'mainadmin',
  `companies` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `preferred_language` varchar(2) CHARACTER SET latin1 NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_accounts VALUES("1","John","Smith","Pandu2016","êL£Â Dv•>¿û´æs‚´h30jýQ^GÏÐK;%","eko.pratama142213@gmail.com","owner","","en","0000-00-00 00:00:00","2017-02-11 02:20:58","1");



DROP TABLE IF EXISTS phpn_banlist;

CREATE TABLE `phpn_banlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ban_item` varchar(70) CHARACTER SET latin1 NOT NULL,
  `ban_item_type` enum('IP','Email') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IP',
  `ban_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ban_ip` (`ban_item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_banners;

CREATE TABLE `phpn_banners` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `image_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image_file_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` tinyint(1) NOT NULL DEFAULT '0',
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `priority_order` (`priority_order`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_banners VALUES("1","home","	","pg4vm28qp5z3zfbuitty_thumb.jpg","1","","1");
INSERT INTO phpn_banners VALUES("2","home","e2e353ee2sy2fvblpnuz.jpg","e2e353ee2sy2fvblpnuz_thumb.jpg","2","","1");
INSERT INTO phpn_banners VALUES("3","home","frvlt9j4m0esctex9v26.jpg","frvlt9j4m0esctex9v26_thumb.jpg","3","","1");
INSERT INTO phpn_banners VALUES("4","check_hotels","qs80zz4tc7eu0v9p688u.jpg","qs80zz4tc7eu0v9p688u_thumb.jpg","4","","1");
INSERT INTO phpn_banners VALUES("5","check_hotels","egkfkiris0uw320kfuam.jpg","egkfkiris0uw320kfuam_thumb.jpg","5","","1");
INSERT INTO phpn_banners VALUES("6","check_hotels","ejqc949g9ei0mmlf6rib.jpg","ejqc949g9ei0mmlf6rib_thumb.jpg","6","","1");
INSERT INTO phpn_banners VALUES("7","check_hotels","xc6rigv51e0tkz96we95.jpg","xc6rigv51e0tkz96we95_thumb.jpg","7","","1");
INSERT INTO phpn_banners VALUES("8","check_hotels","eb7i97qa7dkzjv2v3hfd.jpg","eb7i97qa7dkzjv2v3hfd_thumb.jpg","8","","1");
INSERT INTO phpn_banners VALUES("9","check_villas","i8bsw4pz0y0dsgc5vsw5.jpg","i8bsw4pz0y0dsgc5vsw5_thumb.jpg","9","","1");
INSERT INTO phpn_banners VALUES("10","check_villas","x4cwt3lk20jepz400kpp.jpg","x4cwt3lk20jepz400kpp_thumb.jpg","10","","1");
INSERT INTO phpn_banners VALUES("11","check_villas","cojxrhefh5mvktc00ene.jpg","cojxrhefh5mvktc00ene_thumb.jpg","11","","1");
INSERT INTO phpn_banners VALUES("12","check_villas","gz7lozdt09kfp6yezws8.jpg","gz7lozdt09kfp6yezws8_thumb.jpg","12","","1");
INSERT INTO phpn_banners VALUES("13","check_villas","evozjo72xl4npba9hukw.jpg","evozjo72xl4npba9hukw_thumb.jpg","13","","1");



DROP TABLE IF EXISTS phpn_banners_description;

CREATE TABLE `phpn_banners_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `banner_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `image_text` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_banners_description VALUES("1","1","en","<span class=\"lato size28 slim caps white\">Italy</span><br><br><br><span class=\"lato size100 slim caps white\">Rome</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("2","1","es","<span class=\"lato size28 slim caps white\">Italia</span><br><br><br><span class=\"lato size100 slim caps white\">Roma</span><br><span class=\"lato size20 normal caps white\">de</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("3","1","de","<span class=\"lato size28 slim caps white\">Italien</span><br><br><br><span class=\"lato size100 slim caps white\">Rom</span><br><span class=\"lato size20 normal caps white\">von</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("4","2","en","<span class=\"lato size28 slim caps white\">France</span><br><br><br><span class=\"lato size100 slim caps white\">Paris</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("5","2","es","<span class=\"lato size28 slim caps white\">Francia</span><br><br><br><span class=\"lato size100 slim caps white\">París</span><br><span class=\"lato size20 normal caps white\">de</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("6","2","de","<span class=\"lato size28 slim caps white\">Frankreich</span><br><br><br><span class=\"lato size100 slim caps white\">Paris</span><br><span class=\"lato size20 normal caps white\">von</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("7","3","en","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("8","3","es","<span class=\"lato size28 slim caps white\">Grecia</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">de</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("9","3","de","<span class=\"lato size28 slim caps white\">Griechenland</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">von</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("10","4","en","<span class=\"lato size28 slim caps white\">Italy</span><br><br><br><span class=\"lato size100 slim caps white\">Rome</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("11","4","es","<span class=\"lato size28 slim caps white\">Italia</span><br><br><br><span class=\"lato size100 slim caps white\">Roma</span><br><span class=\"lato size20 normal caps white\">de</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("12","4","de","<span class=\"lato size28 slim caps white\">Italien</span><br><br><br><span class=\"lato size100 slim caps white\">Rom</span><br><span class=\"lato size20 normal caps white\">von</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("14","5","en","<span class=\"lato size28 slim caps white\">France</span><br><br><br><span class=\"lato size100 slim caps white\">Paris</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("15","5","es","<span class=\"lato size28 slim caps white\">Francia</span><br><br><br><span class=\"lato size100 slim caps white\">París</span><br><span class=\"lato size20 normal caps white\">de</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("16","5","de","<span class=\"lato size28 slim caps white\">Frankreich</span><br><br><br><span class=\"lato size100 slim caps white\">Paris</span><br><span class=\"lato size20 normal caps white\">von</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("17","6","en","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Zakynthos</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("18","6","es","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Zakynthos</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("19","6","de","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Zakynthos</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("20","7","en","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("21","7","es","<span class=\"lato size28 slim caps white\">Grecia</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">de</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("22","7","de","<span class=\"lato size28 slim caps white\">Griechenland</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">von</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("23","8","en","<span class=\"lato size18 slim caps white\">Islands</span><br><br><br><span class=\"lato size65 slim caps white\">Bahamas</span><br><span class=\"lato size13 normal caps white\">from</span><br><br><span class=\"lato size40 slim caps yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("24","8","es","<span class=\"lato size18 slim caps white\">Islands</span><br><br><br><span class=\"lato size65 slim caps white\">Bahamas</span><br><span class=\"lato size13 normal caps white\">from</span><br><br><span class=\"lato size40 slim caps yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("25","8","de","<span class=\"lato size18 slim caps white\">Islands</span><br><br><br><span class=\"lato size65 slim caps white\">Bahamas</span><br><span class=\"lato size13 normal caps white\">from</span><br><br><span class=\"lato size40 slim caps yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("26","9","en","<span class=\"lato size28 slim caps white\">Pafagen/Laster</span><br><br><br><span class=\"lato size65 slim caps white\">Lakatya</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("27","9","es","<span class=\"lato size28 slim caps white\">Pafagen/Laster</span><br><br><br><span class=\"lato size65 slim caps white\">Lakatya</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("28","9","de","<span class=\"lato size28 slim caps white\">Pafagen/Laster</span><br><br><br><span class=\"lato size65 slim caps white\">Lakatya</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("29","10","en","<span class=\"lato size28 slim caps white\">Protos</span><br><br><br><span class=\"lato size65 slim caps white\">Best Hotel</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("30","10","es","<span class=\"lato size28 slim caps white\">Protos</span><br><br><br><span class=\"lato size65 slim caps white\">Best Hotel</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("31","10","de","<span class=\"lato size28 slim caps white\">Protos</span><br><br><br><span class=\"lato size65 slim caps white\">Best Hotel</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("32","11","en","<span class=\"lato size28 slim caps white\">Reposton</span><br><br><br><span class=\"lato size65 slim caps white\">Chartin</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase white\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("33","11","es","<span class=\"lato size28 slim caps white\">Reposton</span><br><br><br><span class=\"lato size65 slim caps white\">Chartin</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase white\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("34","11","de","<span class=\"lato size28 slim caps white\">Reposton</span><br><br><br><span class=\"lato size65 slim caps white\">Chartin</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase white\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("35","12","en","<span class=\"lato size28 slim caps white\">Chalston</span><br><br><br><span class=\"lato size65 slim caps white\">Beverly hills</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel & Resort</span>");
INSERT INTO phpn_banners_description VALUES("36","12","es","<span class=\"lato size28 slim caps white\">Chalston</span><br><br><br><span class=\"lato size65 slim caps white\">Beverly hills</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel & Resort</span>");
INSERT INTO phpn_banners_description VALUES("37","12","de","<span class=\"lato size28 slim caps white\">Chalston</span><br><br><br><span class=\"lato size65 slim caps white\">Beverly hills</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase yellow\">Hotel & Resort</span>");
INSERT INTO phpn_banners_description VALUES("38","13","en","<span class=\"lato size28 slim caps white\">Aristo</span><br><br><br><span class=\"lato size65 slim caps white\">Best Season</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase white\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("39","13","es","<span class=\"lato size28 slim caps white\">Aristo</span><br><br><br><span class=\"lato size65 slim caps white\">Best Season</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase white\">Hotel</span>");
INSERT INTO phpn_banners_description VALUES("40","13","de","<span class=\"lato size28 slim caps white\">Aristo</span><br><br><br><span class=\"lato size65 slim caps white\">Best Season</span><br><span class=\"lato size30 normal caps orange\">*****</span><br><br><span class=\"lato size48 slim uppercase white\">Hotel</span>");



DROP TABLE IF EXISTS phpn_bookings;

CREATE TABLE `phpn_bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_number` varchar(20) CHARACTER SET latin1 NOT NULL,
  `hotel_reservation_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `booking_description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `discount_percent` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `discount_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pre_payment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pre_payment_value` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_percent` decimal(5,3) unsigned NOT NULL DEFAULT '0.000',
  `initial_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `guest_tax` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `payment_sum` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `additional_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'USD',
  `rooms_amount` tinyint(4) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `is_admin_reservation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `transaction_number` varchar(30) CHARACTER SET latin1 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - POA, 1 - Online Order, 2 - PayPal, 3 - 2CO, 4 - Authorize.Net, 5 - Bank Transfer, 6 - Account Balance',
  `payment_method` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - Payment Company Account, 1 - Credit Card, 2 - E-Check',
  `coupon_code` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `discount_campaign_id` int(10) DEFAULT '0',
  `additional_info` text COLLATE utf8_unicode_ci NOT NULL,
  `extras` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extras_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `cc_type` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_holder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cc_number` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cc_expires_month` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_expires_year` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_cvv_code` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - prebooking, 1 - pending, 2 - reserved, 3 - completed, 4 - refunded, 5 - payment error, 6 - canceled',
  `status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email_sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `payment_type` (`payment_type`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_bookings_rooms;

CREATE TABLE `phpn_bookings_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `room_id` int(11) NOT NULL DEFAULT '0',
  `room_numbers` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `checkin` date DEFAULT NULL,
  `checkout` date DEFAULT NULL,
  `adults` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `children` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rooms` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `extra_beds` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extra_beds_charge` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `meal_plan_id` int(11) unsigned NOT NULL DEFAULT '0',
  `meal_plan_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `booking_number` (`booking_number`),
  KEY `room_id` (`room_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_campaigns;

CREATE TABLE `phpn_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `campaign_type` enum('global','standard') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `campaign_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `discount_percent` decimal(5,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `target_group_id` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_campaigns VALUES("1","1","0","global","Campaign #4 Sep 2015","2015-09-07","2015-10-07","10.00","1");



DROP TABLE IF EXISTS phpn_car_agencies;

CREATE TABLE `phpn_car_agencies` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logo_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `map_code` text COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies VALUES("1","1-800-123-4567","1-800-123-6789","info@car_agency1.com","company_logo_1.png","<iframe width=\"100%\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Hotel+Circle+South,+San+Diego,+California,+United+States&aq=1&sll=33.981232,-84.173813&sspn=0.12868,0.307274&ie=UTF8&hq=&hnear=Hotel+Cir+S,+San+Diego,+California&ll=32.759,-117.177036&spn=0.017215,0.038409&z=14&output=embed\"></iframe>","0","1");
INSERT INTO phpn_car_agencies VALUES("2","1-800-123-2222","1-800-123-2223","info@car_agency2.com","company_logo_2.png","<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2948.0965968856144!2d-71.0666208!3d42.361780599999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e3709a3c7713c5%3A0x922cf2c02c6d4e5a!2s5+Blossom+St%2C+Charles+River+Plaza+Shopping+Center%2C+Boston%2C+MA+02114!5e0!3m2!1sen!2s!4v1405938488556\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\"></iframe>","0","1");



DROP TABLE IF EXISTS phpn_car_agencies_description;

CREATE TABLE `phpn_car_agencies_description` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `terms_and_conditions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies_description VALUES("1","1","en","Transportation Agency 1","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("2","1","es","Transportation Agency 1","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("3","1","de","Transportation Agency 1","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("4","2","en","Transportation Agency 2","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("5","2","es","Transportation Agency 2","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("6","2","de","Transportation Agency 2","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");



DROP TABLE IF EXISTS phpn_car_agencies_locations;

CREATE TABLE `phpn_car_agencies_locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies_locations VALUES("1","US","2","1");
INSERT INTO phpn_car_agencies_locations VALUES("2","US","3","1");



DROP TABLE IF EXISTS phpn_car_agencies_locations_description;

CREATE TABLE `phpn_car_agencies_locations_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_location_id` (`agency_location_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies_locations_description VALUES("1","1","en","Boston");
INSERT INTO phpn_car_agencies_locations_description VALUES("2","1","es","Boston");
INSERT INTO phpn_car_agencies_locations_description VALUES("3","1","de","Boston");
INSERT INTO phpn_car_agencies_locations_description VALUES("4","2","en","New York City");
INSERT INTO phpn_car_agencies_locations_description VALUES("5","2","es","New York City");
INSERT INTO phpn_car_agencies_locations_description VALUES("6","2","de","New York City");



DROP TABLE IF EXISTS phpn_car_agency_locations;

CREATE TABLE `phpn_car_agency_locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `agency_location_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_locations VALUES("1","1","1","0","1");
INSERT INTO phpn_car_agency_locations VALUES("2","1","2","1","1");
INSERT INTO phpn_car_agency_locations VALUES("3","2","1","1","1");



DROP TABLE IF EXISTS phpn_car_agency_makes;

CREATE TABLE `phpn_car_agency_makes` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_makes VALUES("1","1","0");
INSERT INTO phpn_car_agency_makes VALUES("2","1","1");
INSERT INTO phpn_car_agency_makes VALUES("3","1","2");
INSERT INTO phpn_car_agency_makes VALUES("4","1","3");
INSERT INTO phpn_car_agency_makes VALUES("5","1","4");
INSERT INTO phpn_car_agency_makes VALUES("6","1","5");
INSERT INTO phpn_car_agency_makes VALUES("7","1","6");
INSERT INTO phpn_car_agency_makes VALUES("8","1","7");
INSERT INTO phpn_car_agency_makes VALUES("9","1","8");
INSERT INTO phpn_car_agency_makes VALUES("10","1","9");
INSERT INTO phpn_car_agency_makes VALUES("11","1","10");
INSERT INTO phpn_car_agency_makes VALUES("12","1","11");
INSERT INTO phpn_car_agency_makes VALUES("13","1","12");
INSERT INTO phpn_car_agency_makes VALUES("14","2","0");
INSERT INTO phpn_car_agency_makes VALUES("15","2","1");
INSERT INTO phpn_car_agency_makes VALUES("16","2","2");
INSERT INTO phpn_car_agency_makes VALUES("17","2","3");
INSERT INTO phpn_car_agency_makes VALUES("18","2","4");
INSERT INTO phpn_car_agency_makes VALUES("19","2","5");
INSERT INTO phpn_car_agency_makes VALUES("20","2","6");
INSERT INTO phpn_car_agency_makes VALUES("21","2","7");
INSERT INTO phpn_car_agency_makes VALUES("22","2","8");
INSERT INTO phpn_car_agency_makes VALUES("23","2","9");
INSERT INTO phpn_car_agency_makes VALUES("24","2","10");
INSERT INTO phpn_car_agency_makes VALUES("25","2","11");
INSERT INTO phpn_car_agency_makes VALUES("26","2","12");
INSERT INTO phpn_car_agency_makes VALUES("27","3","0");
INSERT INTO phpn_car_agency_makes VALUES("28","3","1");
INSERT INTO phpn_car_agency_makes VALUES("29","3","2");
INSERT INTO phpn_car_agency_makes VALUES("30","3","3");
INSERT INTO phpn_car_agency_makes VALUES("31","3","4");
INSERT INTO phpn_car_agency_makes VALUES("32","3","5");
INSERT INTO phpn_car_agency_makes VALUES("33","3","6");
INSERT INTO phpn_car_agency_makes VALUES("34","3","7");
INSERT INTO phpn_car_agency_makes VALUES("35","3","8");
INSERT INTO phpn_car_agency_makes VALUES("36","3","9");
INSERT INTO phpn_car_agency_makes VALUES("37","3","10");
INSERT INTO phpn_car_agency_makes VALUES("38","3","11");
INSERT INTO phpn_car_agency_makes VALUES("39","3","12");



DROP TABLE IF EXISTS phpn_car_agency_makes_description;

CREATE TABLE `phpn_car_agency_makes_description` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `make_id` smallint(6) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `make_id` (`make_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_makes_description VALUES("1","1","en","Audi");
INSERT INTO phpn_car_agency_makes_description VALUES("2","2","en","BMW");
INSERT INTO phpn_car_agency_makes_description VALUES("3","3","en","Dodge");
INSERT INTO phpn_car_agency_makes_description VALUES("4","4","en","GMC");
INSERT INTO phpn_car_agency_makes_description VALUES("5","5","en","Jeep");
INSERT INTO phpn_car_agency_makes_description VALUES("6","6","en","Ford");
INSERT INTO phpn_car_agency_makes_description VALUES("7","7","en","Opel");
INSERT INTO phpn_car_agency_makes_description VALUES("8","8","en","Mazda");
INSERT INTO phpn_car_agency_makes_description VALUES("9","9","en","Mercedes-Benz");
INSERT INTO phpn_car_agency_makes_description VALUES("10","10","en","Mitsubishi");
INSERT INTO phpn_car_agency_makes_description VALUES("11","11","en","Renault");
INSERT INTO phpn_car_agency_makes_description VALUES("12","12","en","Subaru");
INSERT INTO phpn_car_agency_makes_description VALUES("13","13","en","Toyota");
INSERT INTO phpn_car_agency_makes_description VALUES("14","1","es","Audi");
INSERT INTO phpn_car_agency_makes_description VALUES("15","2","es","BMW");
INSERT INTO phpn_car_agency_makes_description VALUES("16","3","es","Dodge");
INSERT INTO phpn_car_agency_makes_description VALUES("17","4","es","GMC");
INSERT INTO phpn_car_agency_makes_description VALUES("18","5","es","Jeep");
INSERT INTO phpn_car_agency_makes_description VALUES("19","6","es","Ford");
INSERT INTO phpn_car_agency_makes_description VALUES("20","7","es","Opel");
INSERT INTO phpn_car_agency_makes_description VALUES("21","8","es","Mazda");
INSERT INTO phpn_car_agency_makes_description VALUES("22","9","es","Mercedes-Benz");
INSERT INTO phpn_car_agency_makes_description VALUES("23","10","es","Mitsubishi");
INSERT INTO phpn_car_agency_makes_description VALUES("24","11","es","Renault");
INSERT INTO phpn_car_agency_makes_description VALUES("25","12","es","Subaru");
INSERT INTO phpn_car_agency_makes_description VALUES("26","13","es","Toyota");
INSERT INTO phpn_car_agency_makes_description VALUES("27","1","de","Audi");
INSERT INTO phpn_car_agency_makes_description VALUES("28","2","de","BMW");
INSERT INTO phpn_car_agency_makes_description VALUES("29","3","de","Dodge");
INSERT INTO phpn_car_agency_makes_description VALUES("30","4","de","GMC");
INSERT INTO phpn_car_agency_makes_description VALUES("31","5","de","Jeep");
INSERT INTO phpn_car_agency_makes_description VALUES("32","6","de","Ford");
INSERT INTO phpn_car_agency_makes_description VALUES("33","7","de","Opel");
INSERT INTO phpn_car_agency_makes_description VALUES("34","8","de","Mazda");
INSERT INTO phpn_car_agency_makes_description VALUES("35","9","de","Mercedes-Benz");
INSERT INTO phpn_car_agency_makes_description VALUES("36","10","de","Mitsubishi");
INSERT INTO phpn_car_agency_makes_description VALUES("37","11","de","Renault");
INSERT INTO phpn_car_agency_makes_description VALUES("38","12","de","Subaru");
INSERT INTO phpn_car_agency_makes_description VALUES("39","13","de","Toyota");



DROP TABLE IF EXISTS phpn_car_agency_payment_gateways;

CREATE TABLE `phpn_car_agency_payment_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL DEFAULT '0',
  `payment_type` varchar(20) NOT NULL DEFAULT '',
  `payment_type_name` varchar(50) NOT NULL DEFAULT '',
  `api_login` varchar(40) NOT NULL DEFAULT '',
  `api_key` varchar(40) NOT NULL DEFAULT '',
  `payment_info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO phpn_car_agency_payment_gateways VALUES("1","1","paypal","PayPal","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("2","1","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("3","1","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("4","1","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_car_agency_payment_gateways VALUES("5","2","paypal","PayPal","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("6","2","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("7","2","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("8","2","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");



DROP TABLE IF EXISTS phpn_car_agency_vehicle_categories;

CREATE TABLE `phpn_car_agency_vehicle_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicle_categories VALUES("1","0","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("2","1","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("3","2","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("4","3","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("5","4","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("6","5","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("7","6","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("8","7","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("9","8","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("10","9","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("11","10","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("12","11","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("13","12","1");



DROP TABLE IF EXISTS phpn_car_agency_vehicle_categories_description;

CREATE TABLE `phpn_car_agency_vehicle_categories_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_vehicle_category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_vehicle_category_id` (`agency_vehicle_category_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("1","1","en","Economy","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("2","1","es","Economy","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("3","1","de","Economy","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("4","2","en","Compact","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("5","2","es","Compact","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("6","2","de","Compact","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("7","3","en","Midsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("8","3","es","Midsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("9","3","de","Midsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("10","4","en","Standard","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("11","4","es","Standard","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("12","4","de","Standard","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("13","5","en","Fullsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("14","5","es","Fullsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("15","5","de","Fullsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("16","6","en","Premium","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("17","6","es","Premium","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("18","6","de","Premium","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("19","7","en","Luxury","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("20","7","es","Luxury","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("21","7","de","Luxury","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("22","8","en","Convertible","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("23","8","es","Convertible","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("24","8","de","Convertible","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("25","9","en","Minivan","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("26","9","es","Minivan","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("27","9","de","Minivan","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("28","10","en","Minibus","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("29","10","es","Minibus","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("30","10","de","Minibus","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("31","11","en","Sport Utility","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("32","12","es","Sport Utility","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("33","12","de","Sport Utility","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("34","13","en","Sport Car","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("35","13","es","Sport Car","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("36","13","de","Sport Car","");



DROP TABLE IF EXISTS phpn_car_agency_vehicle_types;

CREATE TABLE `phpn_car_agency_vehicle_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `vehicle_category_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_thumb` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `passengers` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `luggages` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `doors` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicle_types VALUES("1","1","1","vehicle_type_1.jpg","vehicle_type_1_thumb.jpg","5","5","4","0","1");
INSERT INTO phpn_car_agency_vehicle_types VALUES("2","1","2","vehicle_type_2.jpg","vehicle_type_2_thumb.jpg","5","5","4","1","1");
INSERT INTO phpn_car_agency_vehicle_types VALUES("3","2","2","vehicle_type_3.jpg","vehicle_type_3_thumb.jpg","5","4","4","2","1");



DROP TABLE IF EXISTS phpn_car_agency_vehicles;

CREATE TABLE `phpn_car_agency_vehicles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `vehicle_type_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `make_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `registration_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mileage` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fuel_type_id` tinyint(2) NOT NULL,
  `transmission` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_thumb` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price_per_day` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `price_per_hour` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `default_distance` int(10) unsigned NOT NULL DEFAULT '0',
  `distance_extra_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicles VALUES("1","1","1","1","730i","AB 5632","30000","1","automatic","vehicle_1.jpg","vehicle_1_thumb.jpg","312.00","13.00","150","14.00","0","1");
INSERT INTO phpn_car_agency_vehicles VALUES("2","1","2","8","6","12345-AA","20000","1","automatic","vehicle_2.jpg","vehicle_2_thumb.jpg","288.00","12.00","150","15.00","1","1");
INSERT INTO phpn_car_agency_vehicles VALUES("3","1","2","6","Fiesta","AB1 888C","30000","1","automatic","vehicle_3.jpg","vehicle_3_thumb.jpg","336.00","14.00","150","15.00","2","1");
INSERT INTO phpn_car_agency_vehicles VALUES("4","2","1","6","Mondeo","AB123 888","20000","2","manual","vehicle_4.jpg","vehicle_4_thumb.jpg","336.00","14.00","150","15.00","3","1");
INSERT INTO phpn_car_agency_vehicles VALUES("5","2","2","6","Ka","ABV 123","250000","2","automatic","vehicle_5.jpg","vehicle_5_thumb.jpg","336.00","14.00","150","15.00","4","1");



DROP TABLE IF EXISTS phpn_car_agency_vehicles_description;

CREATE TABLE `phpn_car_agency_vehicles_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_vehicle_id` (`agency_vehicle_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicles_description VALUES("1","1","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("2","1","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("3","1","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("4","2","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("5","2","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("6","2","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("7","3","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("8","3","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("9","3","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("10","4","en","Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("11","4","es","Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("12","4","de","Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("13","5","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("14","5","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("15","5","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo.");



DROP TABLE IF EXISTS phpn_car_reservations;

CREATE TABLE `phpn_car_reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `car_agency_id` int(11) NOT NULL DEFAULT '0',
  `car_vehicle_type_id` int(11) NOT NULL DEFAULT '0',
  `car_vehicle_id` int(11) NOT NULL DEFAULT '0',
  `location_from_id` int(11) NOT NULL DEFAULT '0',
  `location_to_id` int(11) NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `reservation_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reservation_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pre_payment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'full price',
  `pre_payment_value` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_percent` decimal(5,3) unsigned NOT NULL DEFAULT '0.000',
  `extras` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extras_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `reservation_total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reservation_paid` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `additional_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `is_admin_reservation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - POA, 1 - Online Order, 2 - PayPal, 3 - 2CO, 4 - Authorize.Net, 5 - Bank Transfer, 6 - Account Balance',
  `payment_method` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - Payment Company Account, 1 - Credit Card, 2 - E-Check',
  `transaction_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `additional_info` text COLLATE utf8_unicode_ci NOT NULL,
  `cc_type` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_holder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cc_number` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cc_expires_month` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_expires_year` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_cvv_code` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - preordering, 1 - pending, 2 - reserved, 3 - completed, 4 - refunded, 5 - payment error, 6 - canceled',
  `status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email_sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `payment_type` (`payment_type`),
  KEY `car_agency_id` (`car_agency_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_reservations VALUES("1","12WS34ED99","1","1","1","0","0","2016-03-11","12:00:00","2016-03-11","15:00:00","Car Rental","42.00","full price","0.00","0.00","0.000","","0.00","42.00","0.00","0.00","USD","1","0","0000-00-00 00:00:00","0","0","","","","","","","","","1","0000-00-00 00:00:00","","0","2016-03-09 00:00:00");
INSERT INTO phpn_car_reservations VALUES("2","RRW5T4ED67","2","2","2","0","0","2016-03-12","11:00:00","2016-03-12","12:00:00","Car Rental","42.00","full price","0.00","0.00","0.000","","0.00","42.00","0.00","0.00","USD","1","0","0000-00-00 00:00:00","0","0","","","","","","","","","1","0000-00-00 00:00:00","","0","2016-03-09 00:00:00");



DROP TABLE IF EXISTS phpn_comments;

CREATE TABLE `phpn_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(70) CHARACTER SET latin1 NOT NULL,
  `comment_text` text COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`,`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_comments VALUES("1","25","1","customer1","","Thanks! Very cool site.","2013-11-12 14:34:40","1","2013-11-12 14:34:47");



DROP TABLE IF EXISTS phpn_countries;

CREATE TABLE `phpn_countries` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `abbrv` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vat_value` decimal(5,3) NOT NULL DEFAULT '0.000',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `abbrv` (`abbrv`)
) ENGINE=MyISAM AUTO_INCREMENT=238 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_countries VALUES("1","AF","Afghanistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("2","AL","Albania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("3","DZ","Algeria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("4","AS","American Samoa","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("5","AD","Andorra","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("6","AO","Angola","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("7","AI","Anguilla","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("8","AQ","Antarctica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("9","AG","Antigua and Barbuda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("10","AR","Argentina","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("11","AM","Armenia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("12","AW","Aruba","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("13","AU","Australia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("14","AT","Austria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("15","AZ","Azerbaijan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("16","BS","Bahamas","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("17","BH","Bahrain","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("18","BD","Bangladesh","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("19","BB","Barbados","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("20","BY","Belarus","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("21","BE","Belgium","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("22","BZ","Belize","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("23","BJ","Benin","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("24","BM","Bermuda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("25","BT","Bhutan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("26","BO","Bolivia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("27","BA","Bosnia and Herzegowina","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("28","BW","Botswana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("29","BV","Bouvet Island","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("30","BR","Brazil","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("31","IO","British Indian Ocean Territory","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("32","VG","British Virgin Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("33","BN","Brunei Darussalam","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("34","BG","Bulgaria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("35","BF","Burkina Faso","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("36","BI","Burundi","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("37","KH","Cambodia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("38","CM","Cameroon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("39","CA","Canada","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("40","CV","Cape Verde","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("41","KY","Cayman Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("42","CF","Central African Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("43","TD","Chad","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("44","CL","Chile","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("45","CN","China","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("46","CX","Christmas Island","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("47","CC","Cocos (Keeling) Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("48","CO","Colombia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("49","KM","Comoros","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("50","CG","Congo","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("51","CK","Cook Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("52","CR","Costa Rica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("53","CI","Cote D\'ivoire","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("54","HR","Croatia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("55","CU","Cuba","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("56","CY","Cyprus","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("57","CZ","Czech Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("58","DK","Denmark","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("59","DJ","Djibouti","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("60","DM","Dominica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("61","DO","Dominican Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("62","TP","East Timor","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("63","EC","Ecuador","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("64","EG","Egypt","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("65","SV","El Salvador","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("66","GQ","Equatorial Guinea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("67","ER","Eritrea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("68","EE","Estonia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("69","ET","Ethiopia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("70","FK","Falkland Islands (Malvinas)","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("71","FO","Faroe Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("72","FJ","Fiji","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("73","FI","Finland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("74","FR","France","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("75","GF","French Guiana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("76","PF","French Polynesia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("77","TF","French Southern Territories","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("78","GA","Gabon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("79","GM","Gambia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("80","GE","Georgia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("81","DE","Germany","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("82","GH","Ghana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("83","GI","Gibraltar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("84","GR","Greece","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("85","GL","Greenland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("86","GD","Grenada","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("87","GP","Guadeloupe","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("88","GU","Guam","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("89","GT","Guatemala","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("90","GN","Guinea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("91","GW","Guinea-Bissau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("92","GY","Guyana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("93","HT","Haiti","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("94","HM","Heard and McDonald Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("95","HN","Honduras","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("96","HK","Hong Kong","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("97","HU","Hungary","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("98","IS","Iceland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("99","IN","India","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("100","ID","Indonesia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("101","IQ","Iraq","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("102","IE","Ireland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("103","IR","Islamic Republic of Iran","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("104","IL","Israel","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("105","IT","Italy","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("106","JM","Jamaica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("107","JP","Japan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("108","JO","Jordan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("109","KZ","Kazakhstan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("110","KE","Kenya","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("111","KI","Kiribati","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("112","KP","Korea, Dem. Peoples Rep of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("113","KR","Korea, Republic of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("114","KW","Kuwait","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("115","KG","Kyrgyzstan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("116","LA","Laos","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("117","LV","Latvia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("118","LB","Lebanon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("119","LS","Lesotho","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("120","LR","Liberia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("121","LY","Libyan Arab Jamahiriya","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("122","LI","Liechtenstein","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("123","LT","Lithuania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("124","LU","Luxembourg","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("125","MO","Macau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("126","MK","Macedonia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("127","MG","Madagascar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("128","MW","Malawi","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("129","MY","Malaysia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("130","MV","Maldives","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("131","ML","Mali","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("132","MT","Malta","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("133","MH","Marshall Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("134","MQ","Martinique","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("135","MR","Mauritania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("136","MU","Mauritius","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("137","YT","Mayotte","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("138","MX","Mexico","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("139","FM","Micronesia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("140","MD","Moldova, Republic of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("141","MC","Monaco","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("142","MN","Mongolia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("143","MS","Montserrat","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("144","MA","Morocco","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("145","MZ","Mozambique","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("146","MM","Myanmar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("147","NA","Namibia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("148","NR","Nauru","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("149","NP","Nepal","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("150","NL","Netherlands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("151","AN","Netherlands Antilles","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("152","NC","New Caledonia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("153","NZ","New Zealand","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("154","NI","Nicaragua","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("155","NE","Niger","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("156","NG","Nigeria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("157","NU","Niue","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("158","NF","Norfolk Island","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("159","MP","Northern Mariana Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("160","NO","Norway","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("161","OM","Oman","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("162","PK","Pakistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("163","PW","Palau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("164","PA","Panama","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("165","PG","Papua New Guinea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("166","PY","Paraguay","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("167","PE","Peru","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("168","PH","Philippines","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("169","PN","Pitcairn","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("170","PL","Poland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("171","PT","Portugal","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("172","PR","Puerto Rico","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("173","QA","Qatar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("174","RE","Reunion","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("175","RO","Romania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("176","RU","Russian Federation","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("177","RW","Rwanda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("178","LC","Saint Lucia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("179","WS","Samoa","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("180","SM","San Marino","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("181","ST","Sao Tome and Principe","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("182","SA","Saudi Arabia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("183","SN","Senegal","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("184","RS","Republic of Serbia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("185","SC","Seychelles","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("186","SL","Sierra Leone","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("187","SG","Singapore","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("188","SK","Slovakia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("189","SI","Slovenia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("190","SB","Solomon Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("191","SO","Somalia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("192","ZA","South Africa","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("193","ES","Spain","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("194","LK","Sri Lanka","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("195","SH","St. Helena","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("196","KN","St. Kitts and Nevis","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("197","PM","St. Pierre and Miquelon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("198","VC","St. Vincent and the Grenadines","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("199","SD","Sudan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("200","SR","Suriname","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("201","SJ","Svalbard and Jan Mayen Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("202","SZ","Swaziland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("203","SE","Sweden","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("204","CH","Switzerland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("205","SY","Syrian Arab Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("206","TW","Taiwan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("207","TJ","Tajikistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("208","TZ","Tanzania, United Republic of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("209","TH","Thailand","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("210","TG","Togo","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("211","TK","Tokelau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("212","TO","Tonga","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("213","TT","Trinidad and Tobago","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("214","TN","Tunisia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("215","TR","Turkey","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("216","TM","Turkmenistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("217","TC","Turks and Caicos Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("218","TV","Tuvalu","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("219","UG","Uganda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("220","UA","Ukraine","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("221","AE","United Arab Emirates","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("222","GB","United Kingdom (GB)","1","0","0.000","999");
INSERT INTO phpn_countries VALUES("224","US","United States","1","1","0.000","1000");
INSERT INTO phpn_countries VALUES("225","VI","United States Virgin Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("226","UY","Uruguay","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("227","UZ","Uzbekistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("228","VU","Vanuatu","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("229","VA","Vatican City State","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("230","VE","Venezuela","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("231","VN","Vietnam","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("232","WF","Wallis And Futuna Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("233","EH","Western Sahara","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("234","YE","Yemen","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("235","ZR","Zaire","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("236","ZM","Zambia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("237","ZW","Zimbabwe","1","0","0.000","0");



DROP TABLE IF EXISTS phpn_coupons;

CREATE TABLE `phpn_coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `coupon_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_started` date NOT NULL,
  `date_finished` date NOT NULL,
  `discount_percent` tinyint(2) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_currencies;

CREATE TABLE `phpn_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(3) CHARACTER SET latin1 NOT NULL,
  `rate` double(10,4) NOT NULL DEFAULT '1.0000',
  `decimals` tinyint(1) NOT NULL DEFAULT '2',
  `symbol_placement` enum('before','after') CHARACTER SET latin1 NOT NULL DEFAULT 'before',
  `primary_order` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_currencies VALUES("1","US Dollar","$","USD","1.0000","2","before","1","1","1");
INSERT INTO phpn_currencies VALUES("2","Euro","€","EUR","0.7691","2","before","2","0","1");
INSERT INTO phpn_currencies VALUES("3","GB Pound","£","GBP","0.6555","2","before","3","0","1");
INSERT INTO phpn_currencies VALUES("4","AU Dollar","A$","AUD","0.9742","2","before","4","0","1");
INSERT INTO phpn_currencies VALUES("5","CA Dollar","C$","CAD","1.0400","2","before","5","0","1");



DROP TABLE IF EXISTS phpn_customer_funds;

CREATE TABLE `phpn_customer_funds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) NOT NULL DEFAULT '0',
  `admin_id` int(10) NOT NULL DEFAULT '0',
  `funds` decimal(10,2) NOT NULL DEFAULT '0.00',
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_customer_groups;

CREATE TABLE `phpn_customer_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_customer_groups VALUES("1","General","General purpose only");



DROP TABLE IF EXISTS phpn_customers;

CREATE TABLE `phpn_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - private person, 1- agency',
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL DEFAULT '0000-00-00',
  `company` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logo` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `b_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_address_2` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_city` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_state` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `b_zipcode` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `user_password` varchar(50) CHARACTER SET latin1 NOT NULL,
  `profile_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `profile_photo_thumb` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `preferred_language` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `date_created` datetime NOT NULL,
  `date_lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `registered_from_ip` varchar(15) CHARACTER SET latin1 NOT NULL,
  `last_logged_ip` varchar(15) CHARACTER SET latin1 NOT NULL DEFAULT '000.000.000.000',
  `email_notifications` tinyint(1) NOT NULL DEFAULT '0',
  `notification_status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `orders_count` smallint(6) NOT NULL DEFAULT '0',
  `rooms_count` smallint(6) NOT NULL DEFAULT '0',
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - registration pending, 1 - active customer',
  `is_removed` tinyint(4) NOT NULL DEFAULT '0',
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `registration_code` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `b_country` (`b_country`),
  KEY `status` (`is_active`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_customers VALUES("1","1","0","John","Smith","0000-00-00","","","","","","","CA","US","","","","john.smith@email.com","customer1","","customer1.png","customer1_thumb.png","en","2016-04-20 11:00:06","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("2","1","0","Stephan","","0000-00-00","","","","","","","","DE","","","","stephan@email.com","customer2","","customer2.png","customer2_thumb.png","en","2016-04-21 12:00:02","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("3","1","0","Robert","","0000-00-00","","","","","","","","GB","","","","robert@email.com","customer3","","","","en","2016-04-22 13:00:03","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("4","1","0","Debora","","0000-00-00","","","","","","","FL","US","","","","debora@email.com","customer4","","customer4.png","customer4_thumb.png","en","2016-04-23 14:00:04","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");



DROP TABLE IF EXISTS phpn_email_templates;

CREATE TABLE `phpn_email_templates` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `template_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `template_name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `template_subject` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `template_content` text COLLATE utf8_unicode_ci NOT NULL,
  `is_system_template` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_email_templates VALUES("1","en","new_account_created","Email for new customer","Your account has been created","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYou login: {USER NAME}\nYou password: {USER PASSWORD}\n\nYou may follow the link below to log into your account:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("2","es","new_account_created","Email para el nuevo cliente","Tu cuenta ha sido creada","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nFelicitaciones por la creación de su nueva cuenta.\n\nPor favor, mantenga este e-mail para sus registros, ya que contiene una información importante que usted pueda necesitar, si surge algún problema u olvidas tu contraseña.\n\nUsted entrada: {USER NAME}\nUsted contraseña: {USER PASSWORD}\n\nUsted puede seguir el siguiente enlace para acceder a su cuenta: <a href=\"{BASE URL}index.php?customer=login\">Acceso Clientes</a>\n\nPD Recuerde, nosotros nunca venderemos su nombre o dirección de correo electrónico.\n\nDisfrute!\n-\nSinceramente,\nSoporte al cliente","1");
INSERT INTO phpn_email_templates VALUES("3","de","new_account_created","E-Mail für neuen Kunden","Ihr Konto wurde erstellt","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nHerzlichen Glückwunsch zu Ihrem neuen Konto erstellen.\n\nBitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information, die Sie benötigen, sollten Sie Probleme auftreten oder Ihr Passwort vergessen enthält.\n\nSie Login: {USER NAME}\nIhr Passwort: {USER PASSWORD}\n\nSie können den folgenden Link in Ihren Account einloggen:\n<a href=\"{BASE URL}index.php?customer=login\">Kunden-Login</a>\n\nP.S. Denken Sie daran, wir werden nie verkaufen Ihren Namen oder E-Mail-Adresse.\n\nViel Spaß!\n-\nMit freundlichen Grüßen,\nKunden-Support","1");
INSERT INTO phpn_email_templates VALUES("4","en","new_account_created_confirm_by_admin","Email for new user (admin approval required)","Your account has been created (approval required)","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYour login: {USER NAME}\nYour password: {USER PASSWORD}\n\nAfter your registration will be approved by administrator,  you could log into your account with a following link:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("5","es","new_account_created_confirm_by_admin","E-mail para el nuevo usuario (requiere aprobación del administrador)","Su cuenta ha sido creada (requiere aprobación)","Estimados <b>{FIRST NAME} {LAST NAME}</b>!\n\nFelicitaciones por la creación de su nueva cuenta.\n\nPor favor, mantenga este e-mail para sus registros, ya que contiene una información importante que usted pueda necesitar, si surge algún problema u olvidas tu contraseña.\n\nSu entrada: {USER NAME}\nSu contraseña: {USER PASSWORD}\n\nDespués de su registro será aprobado por el administrador, puede acceder a su cuenta con el siguiente enlace: <a href=\"\n{BASE URL}index.php?customer=login\">Acceso Clientes</a>\n\nPD Recuerde, nosotros nunca venderemos su nombre o dirección de correo electrónico.\n\nDisfrute!\n-\nAtentamente,\nAtención al cliente","1");
INSERT INTO phpn_email_templates VALUES("6","de","new_account_created_confirm_by_admin","E-Mail für neue Benutzer (admin Genehmigung erforderlich)","Ihr Konto wurde erstellt (Bestätigung erforderlich)","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nHerzlichen Glückwunsch zu Ihrem neuen Konto erstellen.\n\nBitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information, die Sie benötigen, sollten Sie Probleme auftreten oder Ihr Passwort vergessen enthält.\n\nIhre Login: {USER NAME}\nIhr Passwort: {USER PASSWORD}\n\nNach Ihrer Anmeldung vom Administrator genehmigt werden, könnten Sie sich in Ihrem Konto mit folgendem Link anmelden:\n<a href=\"{BASE URL}index.php?customer=login\">Kunden-Login</a>\n\nP.S. Denken Sie daran, wir werden nie verkaufen Ihren Namen oder E-Mail-Adresse.\n\nViel Spaß!\n-\nMit freundlichen Grüßen,\nKunden-Support","1");
INSERT INTO phpn_email_templates VALUES("7","en","new_account_created_confirm_by_email","Email for new user (email confirmation required)","Your account has been created (confirmation required)","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYour login: {USER NAME}\nYour password: {USER PASSWORD}\n\nIn order to become authorized member, you will need to confirm your registration. You may follow the link below to access the confirmation page:\n<a href=\"{BASE URL}index.php?customer=confirm_registration&c={REGISTRATION CODE}\">Confirm Registration</a>\n\nP.S. Remember, we will never sell your personal information or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("8","es","new_account_created_confirm_by_email","Correo electrónico para el nuevo usuario (se requiere confirmación)","Su cuenta ha sido creada (requiere confirmación por e-mail)","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nFelicitaciones por la creación de su nueva cuenta.\n\nPor favor, mantenga este e-mail para sus registros, ya que contiene una información importante que usted pueda necesitar, si surge algún problema u olvidas tu contraseña.\n\nUsted entrada: {USER NAME}\nUsted contraseña: {USER PASSWORD}\n\nPara convertirse en miembro autorizado, tendrá que confirmar su registro. Por favor siga el siguiente enlace para acceder a la página de confirmación: <a href=\"\n{BASE URL}index.php?customer=confirm_registration&c={REGISTRATION CODE}\">Confirmar registro</a>\n\nPD Recuerde, nosotros nunca venderemos su nombre o dirección de correo electrónico.\n\nDisfrute!\n-\nSinceramente,\nSoporte al cliente","1");
INSERT INTO phpn_email_templates VALUES("9","de","new_account_created_confirm_by_email","E-Mail für neue Benutzer (E-Mail eine Bestätigung erforderlich)","Ihr Konto wurde erstellt (Bestätigung erforderlich)","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nHerzlichen Glückwunsch zu Ihrem neuen Konto erstellen.\n\nBitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information, die Sie benötigen, sollten Sie Probleme auftreten oder Ihr Passwort vergessen enthält.\n\nIhre Login: {USER NAME}\nIhr Passwort: {USER PASSWORD}\n\nUm Mitglied zu werden ermächtigt, müssen Sie Ihre Registrierung bestätigen. Sie können folgen Sie den untenstehenden Link, um die Bestätigungs-Seite zu erreichen: <a href=\"{BASE URL}index.php?customer=confirm_registration&c={REGISTRATION CODE}\">Bestätigung der Registrierung</a>\n\nP.S. Denken Sie daran, wir werden nie verkaufen Ihre persönlichen Informationen oder E-Mail-Adresse.\n\nViel Spaß!\n-\nMit freundlichen Grüßen,\nKunden-Support","1");
INSERT INTO phpn_email_templates VALUES("10","en","new_account_created_by_admin","Email for new user (account created by admin)","Your account has been created by admin","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nOur administrator just created a new account for you.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYou login: {USER NAME}\nYou password: {USER PASSWORD}\n\nYou may follow the link below to log into your account:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("11","es","new_account_created_by_admin","Correo electrónico para el nuevo usuario (cuenta creada por el admin)","Su cuenta ha sido creada por admin","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nNuestro administrador acaba de crear una cuenta nueva para usted.\n\nPor favor, mantenga este e-mail para sus registros, ya que contiene una información importante que usted pueda necesitar, si surge algún problema u olvidas tu contraseña.\n\nUsted entrada: {USER NAME}\nUsted contraseña: {USER PASSWORD}\n\nUsted puede seguir el siguiente enlace para acceder a su cuenta: <a href=\"{BASE URL}index.php?customer=login\">Acceso Clientes</a>\n\nPD Recuerde, nosotros nunca venderemos su nombre o dirección de correo electrónico.\n\nDisfrute!\n-\nAtentamente,\nAtención al cliente","1");
INSERT INTO phpn_email_templates VALUES("12","de","new_account_created_by_admin","E-Mail für neue Benutzer (Account erstellt von admin)","Ihr Konto wurde von admin erstellt","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nUnser Administrator gerade erstellt ein neues Konto für Sie.\n\nBitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information, die Sie benötigen, sollten Sie Probleme auftreten oder Ihr Passwort vergessen enthält.\n\nSie Login: {USER NAME}\nIhr Passwort: {USER PASSWORD}\n\nSie können den folgenden Link in Ihren Account einloggen: <a href=\"{BASE URL}index.php?customer=login\">Kunden-Login</a>\n\nP.S. Denken Sie daran, wir werden nie verkaufen Ihren Namen oder E-Mail-Adresse.\n\nViel Spaß!\n-\nMit freundlichen Grüßen,\nKunden-Support","1");
INSERT INTO phpn_email_templates VALUES("13","de","new_account_created_notify_admin","Neues Konto erstellt wurde (benachrichtigen admin)","Neues Konto erstellt wurde","Hallo Admin!\n\nEin neuer Benutzer hat bei Ihnen vor Ort registriert worden.\nDiese Email enthält einen Benutzer-Account Details:\n\nName: {FIRST NAME} {LAST NAME}\nE-Mail: {USER EMAIL}\nBenutzername: {USER NAME}\n\nP.S. Bitte überprüfen Sie, ob es nicht erforderlich, Ihre Zustimmung zur Aktivierung.","1");
INSERT INTO phpn_email_templates VALUES("14","en","new_account_created_notify_admin","New account has been created (notify admin)","New account has been created","Hello Admin!\n\nA new user has been registered at your site.\nThis email contains a user account details:\n\nName: {FIRST NAME} {LAST NAME}\nEmail: {USER EMAIL}\nUsername: {USER NAME}\n\nP.S. Please check if it doesn\'t require your approval for activation","1");
INSERT INTO phpn_email_templates VALUES("15","es","new_account_created_notify_admin","Nueva cuenta se ha creado (notificar admin)","Nueva cuenta se ha creado","Hola admin!\n\nUn nuevo usuario ha sido registrado en su sitio.\nEste correo electrónico contiene una información de la cuenta de usuario:\n\nNombre: {FIRST NAME} {LAST NAME}\nE-mail: {USER EMAIL}\nNombre de usuario: {USER NAME}\n\nPD Por favor, compruebe si no se requiere su aprobación para la activación.","1");
INSERT INTO phpn_email_templates VALUES("16","en","password_forgotten","Email for customer or admin forgotten password","Forgotten Password","Hello <b>{USER NAME}</b>!\n\nYou or someone else asked for your login info on our site:\n{WEB SITE}\n\nYour Login Info:\n\nUsername: {USER NAME}\nPassword: {USER PASSWORD}\n\n\nBest regards,\n{WEB SITE}","1");
INSERT INTO phpn_email_templates VALUES("17","es","password_forgotten","Email para el cliente o administrador contraseña olvidada","He olvidado la contraseña","Hola <b>{USER NAME}</b>!\n\nUsted o alguien más pide su información de acceso en nuestro sitio:\n{WEB SITE}\n\nSu usuario Info:\n\nNombre de usuario: {USER NAME}\nContraseña: {USER PASSWORD}\n\n\nSaludos cordiales,\n{WEB SITE}","1");
INSERT INTO phpn_email_templates VALUES("18","de","password_forgotten","E-Mail-Client oder für admin vergessenes Passwort","Passwort vergessen?","Hallo <b>{USER NAME}</b>!\n\nSie oder jemand anderes baten um Ihre Login-Informationen auf unserer Website:\n{WEB SITE}\n\nIhre Login-Infos:\n\nBenutzername: {USER NAME}\nPasswort: {USER PASSWORD}\n\n\nMit freundlichen Grüßen,\n{WEB SITE}\n","1");
INSERT INTO phpn_email_templates VALUES("19","en","password_changed_by_admin","Password changed by admin","Your password has been changed","Hello <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour password was changed by administrator of the site:\n{WEB SITE}\n\nHere your new login info:\n-\nUsername: {USER NAME} \nPassword: {USER PASSWORD}\n\n-\nBest regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("20","es","password_changed_by_admin","Contraseña cambiada por admin","Su contraseña ha sido cambiada","Hola <b>{FIRST NAME} {LAST NAME}</b>!\n\nSu contraseña se ha cambiado por el administrador del sitio:\n{WEB SITE}\n\nAquí la información de su nuevo inicio de sesión:\n-\nNombre de usuario: {USER NAME} \nContraseña: {USER PASSWORD}\n\n-\nSaludos cordiales,\nAdministración","1");
INSERT INTO phpn_email_templates VALUES("21","de","password_changed_by_admin","Kennwort von admin geändert","Ihr Passwort wurde geändert","Hallo <b>{FIRST NAME} {LAST NAME}</b>!\n\nIhr Passwort wurde durch den Administrator der Website geändert:\n{WEB SITE}\n\nHier werden Ihre neuen Login-Info:\n-\nBenutzername: {USER NAME} \nKennwort: {USER PASSWORD}\n\n-\nMit freundlichen Grüßen,\nVerwaltung","1");
INSERT INTO phpn_email_templates VALUES("22","en","registration_approved_by_admin","Email for new customer (registration was approved by admin)","Your registration has been approved","Dear <b>{FIRST NAME} {LAST NAME}!</b>\n\nCongratulations! This e-mail is to confirm that your registration at {WEB SITE} has been approved.\n\nYou can now login in to your account now.\n\nThank you for choosing {WEB SITE}.\n-\nSincerely,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("23","es","registration_approved_by_admin","E-mail para clientes nuevos (el registro fue aprobado por el administrador)","Su registro ha sido aprobado","Estimado <b>{FIRST NAME} {LAST NAME}!</b>\n\n¡Felicitaciones! Este e-mail es para confirmar que su inscripción en {WEB SITE} ha sido aprobado.\n\nAhora puede acceder a su cuenta ahora.\n\nGracias por elegir {WEB SITE}.\n-\nAtentamente,\nadministración","1");
INSERT INTO phpn_email_templates VALUES("24","de","registration_approved_by_admin","E-Mail für neue Kunden (Registrierung wurde von admin zugelassen)","Ihre Registrierung genehmigt wurde","Sehr geehrte <b>{FIRST NAME} {LAST NAME}!</b>\n\nHerzlichen Glückwunsch! Diese E-Mail ist zu bestätigen, dass Ihre Anmeldung bei {WEB SITE} genehmigt wurde.\n\nSie können jetzt in Ihr Konto jetzt einloggen.\n\nDanke für die Wahl {WEB SITE}.\n-\nMit freundlichen Grüßen,\nVerwaltung","1");
INSERT INTO phpn_email_templates VALUES("25","en","account_deleted_by_user","Account removed email (by customer)","Your account has been removed","Dear {USER NAME}!\n\nYour account was removed.\n\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("26","es","account_deleted_by_user","Cuenta Eliminada Email (por el cliente)","Su cuenta ha sido fue eliminado","Estimado {USER NAME}!\n\nTu cuenta ha sido eliminada.\n\n-\nAtentamente,\nAtención al cliente","1");
INSERT INTO phpn_email_templates VALUES("27","de","account_deleted_by_user","Konto entfernt E-Mail (vom Client)","Ihr Konto wurde entfernt","Sehr geehrte {USER NAME}!\n\nIhr Konto wurde entfernt.\n\n-\nMit freundlichen Grüßen,\nKunden-Support","1");
INSERT INTO phpn_email_templates VALUES("28","es","new_account_created_without","Correo electrónico para el nuevo cliente/devuelto (sin cuenta)","Su información de contacto ha sido aceptada","Querido <b>{FIRST NAME} {LAST NAME}</b>!\n\nGracias por enviarnos su información de contacto. Ahora puede completar su reserva - sólo tienes que seguir las instrucciones que aparecen en la página de pago.\n\nPor favor, recuerde que aún no tiene cuenta en nuestro sitio, siempre se puede crear con facilidad. Para ello basta con seguir este enlace e introducir toda la información necesaria para crear una cuenta nueva: <a href=\"{BASE URL}index.php?client=create_account\">Crear una cuenta</a>\n\nPD Recuerde, nosotros nunca venderemos su nombre o dirección de correo electrónico.\n\nDisfrutar!\n-\nAtentamente,\nAtención al cliente","1");
INSERT INTO phpn_email_templates VALUES("29","de","new_account_created_without","E-Mail für neue/kehrte Kunden (ohne Rechnung)","Ihre Kontaktinformationen angenommen wurde","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nVielen Dank für die Zusendung Ihrer Kontaktdaten. Sie können nun vervollständigen Sie Ihre Buchung - folgen Sie einfach den Anweisungen auf der Checkout-Seite.\n\nBitte denken Sie daran, dass auch Sie nicht über Konto auf unserer Website, können Sie immer schaffen es mit leicht. Um dies zu tun folgen Sie einfach diesem Link und geben Sie alle benötigten Informationen auf neue Rechnung erstellen: <a href=\"{BASE URL}index.php?customer=create_account\">Konto erstellen</a>\n\nP.S. Denken Sie daran, wir werden nie verkaufen Ihren Namen oder E-Mail-Adresse.\n\nGenießen!\n-\nMit freundlichen Grüßen,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("30","en","new_account_created_without","Email for new/returned customer (without account)","Your contact information has been accepted","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for sending us your contact information. You may now complete your booking - just follow the instructions on the checkout page.\n\nPlease remember that even you don\'t have account on our site, you may always create it with easily. To do it simply follow this link and enter all needed information to create a new account: <a href=\"{BASE URL}index.php?customer=create_account\">Create Account</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("31","en","order_placed_online","Email for online placed orders (not paid yet)","Your order has been placed in our system!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for reservation request!\n\nYour order {BOOKING NUMBER} has been placed in our system and will be processed shortly.\n{STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\nP.S. Please keep this email for your records, as it contains an important information that you may\nneed.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("32","es","order_placed_online","Email para pedidos en línea (aún sin pagar)","Su pedido ha sido realizado en nuestro sistema!","Estimado <b>{LAST NAME} {FIRST NAME}</b>!\n\nGracias por solicitud de reserva!\n\nSu {BOOKING NUMBER} pedido ha sido realizado en nuestro sistema y se procesará en breve.\n{STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\nPD Por favor, mantenga este e-mail para sus registros, ya que contiene una información importante que puede\nnecesitan.\n\n-\nAtentamente,\nAtención al cliente\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("33","de","order_placed_online","E-Mail für Bestellungen online gestellt (noch nicht bezahlt)","Ihre Bestellung wurde in unserem System gelegt!","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nVielen Dank für Reservierungs-Anfrage!\n\nIhre Bestellung {BOOKING NUMBER} hat in unserem System platziert und wird in Kürze bearbeitet werden.\n{STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\nP.S. Bitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information enthält, dass Sie möglicherweise\nbenötigen.\n\n-\nMit freundlichen Grüßen,\nKunden-Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("34","en","order_paid","Email for orders paid via payment processing systems","Your order {BOOKING NUMBER} has been paid and received by the system!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for reservation!\n\nYour order {BOOKING NUMBER} has been completed!\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\n{BILLING INFORMATION}\n\nP.S. Please keep this email for your records, as it contains an important information that you may need.\nP.P.S You may always check your booking status here:\n<a href=\"{BASE URL}index.php?page=check_status\">Check Status</a>\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("35","es","order_paid","Email para los pedidos pagados a través de sistemas de procesamiento de pagos","Su pedido {BOOKING NUMBER} ha sido pagado y recibido por el sistema!","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nGracias por su reservación!\n\nSu {BOOKING NUMBER} fin se ha terminado!\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\n{BILLING INFORMATION}\n\nPD Por favor, mantenga este e-mail para sus registros, ya que contiene una información importante que usted pueda necesitar.\nPPD Usted siempre puede revisar su estado de su reserva aquí: <a href=\"{BASE URL}index.php?page=check_status\">Comprobar el estado de</a>\n\n-\nAtentamente,\nAtención al cliente\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("36","de","order_paid","E-Mail für Bestellungen via Zahlungsabwicklung Systeme bezahlt","Ihre Bestellung {BOOKING NUMBER} wurde bezahlt und erhalten vom System!","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nVielen Dank für die Reservierung!\n\nIhre Bestellung {BOOKING NUMBER} ist abgeschlossen!\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\n{BILLING INFORMATION}\n\nP.S. Bitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information, die Sie benötigen enthält.\nP.P.S. Sie können jederzeit überprüfen Sie Ihre Buchung Status hier: <a href=\"{BASE URL}index.php?page=check_status\">Status überprüfen</a>\n\n-\nMit freundlichen Grüßen,\nKunden-Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("37","en","events_new_registration","Events - new member has registered (member copy)","You have been successfully registered to the event!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on registering to {EVENT}.\n\nPlease keep this email for your records, as it contains an important information that you may need.\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("38","es","events_new_registration","Eventos - nuevo miembro se ha registrado (copia de miembro)","Se le ha registrado con éxito en el evento!","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nFelicitaciones por el registro de {EVENT}.\n\nPor favor, mantenga este e-mail para sus registros, ya que contiene una información importante que usted pueda necesitar.\n\n-\nSaludos cordiales,\nAdministración","1");
INSERT INTO phpn_email_templates VALUES("39","de","events_new_registration","Veranstaltungen - neues Mitglied registriert hat (Mitglied Kopie)","Sie haben erfolgreich an der Veranstaltung angemeldet haben!","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nHerzlichen Glückwunsch zu der Registrierung zu {EVENT}.\n\nBitte bewahren Sie diese E-Mail für Ihre Aufzeichnungen, da sie eine wichtige Information, die Sie benötigen enthält.\n\n-\nMit besten Grüßen,\nVerwaltung","1");
INSERT INTO phpn_email_templates VALUES("40","en","order_canceled","Reservation has been canceled by Customer/Administrator","Your order has been canceled!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order {BOOKING NUMBER} has been canceled.\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("41","es","order_canceled","Reserva ha sido cancelada por Cliente/Administrator","Su pedido ha sido cancelado!","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nSu pedido {BOOKING_NUMBER} ha sido cancelado.\n\n{BOOKING DETAILS}\n\nPD Por favor, no dude en contactar con nosotros si tiene alguna pregunta.\n\n-\nAtentamente,\nSoporte al Cliente\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("42","de","order_canceled","Die Reservierung muss durch Kunden/Administrator wurde abgesagt","Ihre Bestellung wurde abgesagt!","Sehr geehrte <b>{FIRST NAME} {LAST NAME}</b>!\n\nIhre Bestellung {BOOKING NUMBER} wurde abgebrochen.\n\n{BOOKING DETAILS}\n\nP.S. Bitte zögern Sie nicht uns zu kontaktieren, wenn Sie Fragen haben.\n\n-\nMit freundlichen Grüßen,\nKunden-Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("43","de","order_refunded","Die Reservierung wurde erstattet Administrator","Ihre Bestellung wurde erstattet","Liebe <b>{FIRST NAME} {LAST NAME}</b>!\n\nIhre Bestellung {BOOKING NUMBER} wurde erstattet!\n\n{BOOKING DETAILS}\n\n-\nMit freundlichen Grüßen,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("44","en","order_refunded","Reservation has been refunded by administrator","Your order  has been refunded","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order {BOOKING NUMBER} has been refunded!\n\n{BOOKING DETAILS}\n\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("45","es","order_refunded","Reserva ha sido reembolsado por el administrador","Su orden ha sido reembolsado","Querido <b>{FIRST NAME} {LAST NAME}</b>!\n\nSu orden {BOOKING NUMBER} ha sido devuelto!\n\n{BOOKING DETAILS}\n\n-\nAtentamente,\nAtención al cliente","1");
INSERT INTO phpn_email_templates VALUES("46","de","order_status_changed","Reservation Status wurde geändert","Ihre Reservierung Status wurde geändert.","Liebe <b>{FIRST NAME} {LAST NAME}</b>!\n\nDer Status Ihrer Reservierung #{BOOKING NUMBER} hat {STATUS DESCRIPTION} geändert worden!\n\n-\nMit freundlichen Grüßen,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("47","en","order_status_changed","Reservation status has been changed","Your reservation status has been changed.","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThe status of your reservation #{BOOKING NUMBER} has been changed to {STATUS DESCRIPTION}!\n\n-\nSincerely,\nCustomer Support\n","1");
INSERT INTO phpn_email_templates VALUES("48","es","order_status_changed","Estado de la reserva se ha cambiado","Su estado de la reserva se ha cambiado.","Querido <b>{FIRST NAME} {LAST NAME}</b>!\n\nEl estado de su reserva #{BOOKING NUMBER} se ha cambiado a {STATUS DESCRIPTION}!\n\n-\nAtentamente,\nAtención al cliente","1");
INSERT INTO phpn_email_templates VALUES("49","en","payment_error","Customer payment has been failed for some reason","Your payment has been failed","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThe payment for your booking {BOOKING NUMBER} has been failed. The reason was: {STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("50","es","payment_error","De pago del cliente ha sido fallado por alguna razón","Su pago se ha fallado","Estimado <b>{FIRST NAME} {LAST NAME}</b>!\n\nEl pago de su reserva {BOOKING NUMBER} se ha fallado. La razón era: {STATUS DESCRIPTION}\n \n{BOOKING DETAILS}\n\nPD Por favor, no dude en contactar con nosotros si tiene alguna pregunta.\n\n-\nAtentamente,\nSoporte al Cliente\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("51","de","payment_error","Client-Zahlung aus irgendeinem Grund nicht bestanden","Ihre Zahlung wurde nicht","Sehr geehrte <b>{FIRST NAME} {LAST NAME}</b>!\n\nDie Zahlung für Ihre Buchung {BOOKING NUMBER} wurde fehlgeschlagen. Der Grund war: {STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\nP.S. Bitte zögern Sie nicht uns zu kontaktieren, wenn Sie Fragen haben.\n\n-\nMit freundlichen Grüßen,\nKunden-Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("52","en","subscription_to_newsletter","Newsletter - new member has subscribed (member copy)","You have been subscribed to the Newsletter","Hello!\n\nYou are receiving this email because you, or someone using this email address, subscribed to the Newsletter of {WEB SITE}\n\nIf you do not wish to receive such emails in the future, please click this link: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_unsubscribe&email={USER EMAIL}\">Unsubscribe</a>\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("53","es","subscription_to_newsletter","Newsletter - nuevo miembro se ha suscrito (copia miembros)","Que se han suscrito al boletín de noticias","Hola!\n\nUsted está recibiendo este correo electrónico porque usted o alguien que use la siguiente dirección de correo electrónico, suscritas al boletín de noticias de {WEB SITE}\n\nSi usted no desea recibir dichos correos electrónicos en el futuro, por favor haga clic en este enlace: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_unsubscribe&email={USER EMAIL}\">Cancelar la suscripción</a>\n\n-\nSaludos cordiales,\nadministración","1");
INSERT INTO phpn_email_templates VALUES("54","de","subscription_to_newsletter","Newsletter - neues Mitglied abonniert hat (Mitglied Kopie)","Sie haben den Newsletter abonniert haben","Hallo!\n\nSie erhalten diese E-Mail, weil Sie oder jemand mit dieser E-Mail-Adresse, den Newsletter von {WEB SITE} abonniert\n\nWenn Sie nicht möchten, dass solche E-Mails in der Zukunft erhalten, klicken Sie bitte diesen Link: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_unsubscribe&email={USER EMAIL}\">Abmelden</a>\n\n-\nMit freundlichen Grüßen,\nVerwaltung","1");
INSERT INTO phpn_email_templates VALUES("55","en","unsubscription_from_newsletter","Newsletter - member has unsubscribed (member copy)","You have been unsubscribed from the Newsletter","Hello!\n\nYou are receiving this email because you, or someone using this email address, unsubscribed from the Newsletter of {WEB SITE}\n\nYou can always restore your subscription, using the link below: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_subscribe&email={USER EMAIL}\">Subscribe</a>\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("56","es","unsubscription_from_newsletter","Newsletter - miembro ha dado de baja (miembro de la copia)","Se le ha cancelado la suscripción a la Newsletter","Hola!\n\nUsted está recibiendo este correo electrónico porque usted o alguien que use la siguiente dirección de correo electrónico, de baja de la Newsletter de {WEB SITE}\n\nSiempre podrá restaurar su suscripción, usando el siguiente enlace: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_subscribe&email={USER EMAIL}\">Suscribirse</a>\n\n-\nSaludos cordiales,\nadministración","1");
INSERT INTO phpn_email_templates VALUES("57","de","unsubscription_from_newsletter","Newsletter - Mitglied hat sich abgemeldet (Mitglied Kopie)","Sie haben den Newsletter abbestellt worden","Hallo!\n\nSie erhalten diese E-Mail, weil Sie oder jemand mit dieser E-Mail-Adresse, von der Newsletter von {WEB SITE} abgemeldet\n\nSie können immer wieder Ihr Abonnement, über den Link unten: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_subscribe&email={USER EMAIL}\">Abonnieren</a>\n\n-\nMit freundlichen Grüßen,\nVerwaltung","1");
INSERT INTO phpn_email_templates VALUES("58","en","reservation_expired","Reservation has been expired","Your reservation has been expired!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order reservation has been expired.\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("59","es","reservation_expired","Reserva ha expirado","Su reserva ha sido vencido!","Querido <b>{FIRST NAME} {LAST NAME}</b>!\n\nSu reserva orden ha sido vencido.\n\n{BOOKING DETAILS}\n\nPD Por favor, no dude en contactar con nosotros si tiene alguna pregunta.\n\n-\nAtentamente, \nAtención al cliente\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("60","de","reservation_expired","Reservierung ist abgelaufen","Ihre Reservierung ist abgelaufen!","Lieber <b>{FIRST NAME} {LAST NAME}</b>!\n\nIhre Bestellung Reservierung ist abgelaufen.\n\n{BOOKING DETAILS}\n\nP.S. Bitte zögern Sie nicht uns zu kontaktieren, wenn Sie Fragen haben.\n\n-\nMit freundlichen Grüßen, \nKunden-Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("61","en","test_template","Testing Email","Testing Email","Hello <b>{USER NAME}</b>!\n\nThis a testing email.\n\nBest regards,\n{WEB SITE}","0");
INSERT INTO phpn_email_templates VALUES("62","es","test_template","Prueba de correo electrónico","Prueba de correo electrónico","Hola <b>{USER NAME}</b>!\n\nEsta es una prueba de correo electrónico.\n\nSaludos cordiales,\n{WEB SITE}","0");
INSERT INTO phpn_email_templates VALUES("63","de","test_template","Testen E-Mail","Testen E-Mail","Hallo <b>{USER NAME}</b>!\n\nDies ist ein Test E-Mail.\n\nMit freundlichen Grüßen,\n{WEB SITE}","0");



DROP TABLE IF EXISTS phpn_events_registered;

CREATE TABLE `phpn_events_registered` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL DEFAULT '0',
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_extras;

CREATE TABLE `phpn_extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `maximum_count` smallint(6) unsigned NOT NULL DEFAULT '0',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_extras VALUES("1","10.00","1","2","1");
INSERT INTO phpn_extras VALUES("2","30.00","5","1","1");



DROP TABLE IF EXISTS phpn_extras_description;

CREATE TABLE `phpn_extras_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `extra_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_extras_description VALUES("1","1","en","Wireless Internet Access","Wireless Internet Access (24 hour period)	");
INSERT INTO phpn_extras_description VALUES("2","1","es","Acceso inalámbrico a Internet","Acceso inalámbrico a Internet (período de 24 horas)");
INSERT INTO phpn_extras_description VALUES("3","1","de","WLAN","WLAN (24 Stunden)");
INSERT INTO phpn_extras_description VALUES("4","2","en","Airport Pickup","Airport Pickup (1 car with 5 seater)");
INSERT INTO phpn_extras_description VALUES("5","2","es","Recogida en el aeropuerto","Recogida en el aeropuerto (1 coche con 5 plazas)");
INSERT INTO phpn_extras_description VALUES("6","2","de","Abholung vom Flughafen","Abholung vom Flughafen (1 Fahrzeug mit 5-Sitzer)");



DROP TABLE IF EXISTS phpn_faq_categories;

CREATE TABLE `phpn_faq_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_faq_category_items;

CREATE TABLE `phpn_faq_category_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `faq_question` text COLLATE utf8_unicode_ci NOT NULL,
  `faq_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_gallery_album_items;

CREATE TABLE `phpn_gallery_album_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `album_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `item_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_file_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `album_code` (`album_code`),
  KEY `priority_order` (`priority_order`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_album_items VALUES("1","dkw3vvot","http://www.youtube.com/watch?v=5VIV8nt2KkU","","5","1");
INSERT INTO phpn_gallery_album_items VALUES("2","afbirxww","home.jpg","home_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("3","7u9sfhaz","img1_1.jpg","img1_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("4","7u9sfhaz","img1_2.jpg","img1_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("5","7u9sfhaz","img1_3.jpg","img1_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("6","0bxbqgps","img2_1.jpg","img2_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("7","0bxbqgps","img2_2.jpg","img2_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("8","0bxbqgps","img2_3.jpg","img2_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("9","6z5i5ikr","img3_1.jpg","img3_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("10","6z5i5ikr","img3_2.jpg","img3_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("11","6z5i5ikr","img3_3.jpg","img3_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("12","gvgbrtmc","img4_1.jpg","img4_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("13","gvgbrtmc","img4_2.jpg","img4_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("14","gvgbrtmc","img4_3.jpg","img4_3_thumb.jpg","3","1");



DROP TABLE IF EXISTS phpn_gallery_album_items_description;

CREATE TABLE `phpn_gallery_album_items_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `gallery_album_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `album_code` (`gallery_album_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_album_items_description VALUES("1","1","es","Mi video del hotel","");
INSERT INTO phpn_gallery_album_items_description VALUES("2","1","en","My Hotel Video","");
INSERT INTO phpn_gallery_album_items_description VALUES("3","1","de","My Hotel Video","");
INSERT INTO phpn_gallery_album_items_description VALUES("4","2","es","Foto #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("5","2","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("6","2","de","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("7","3","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("8","3","es","Foto #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("9","3","de","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("10","4","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("11","4","es","Foto #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("12","4","de","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("13","5","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("14","5","es","Foto #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("15","5","de","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("16","6","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("17","6","es","Foto #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("18","6","de","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("19","7","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("20","7","es","Foto #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("21","7","de","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("22","8","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("23","8","es","Foto #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("24","8","de","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("25","9","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("26","9","es","Foto #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("27","9","de","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("28","10","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("29","10","es","Foto #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("30","10","de","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("31","11","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("32","11","es","Foto #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("33","11","de","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("34","12","de","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("35","12","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("36","12","es","Foto #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("37","13","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("38","13","es","Foto #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("39","13","de","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("40","14","en","","");
INSERT INTO phpn_gallery_album_items_description VALUES("41","14","es","","");
INSERT INTO phpn_gallery_album_items_description VALUES("42","14","de","","");



DROP TABLE IF EXISTS phpn_gallery_albums;

CREATE TABLE `phpn_gallery_albums` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `album_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `album_type` enum('images','video') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'images',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_albums VALUES("1","afbirxww","images","1","1");
INSERT INTO phpn_gallery_albums VALUES("2","dkw3vvot","video","11","1");
INSERT INTO phpn_gallery_albums VALUES("3","7u9sfhaz","images","3","1");
INSERT INTO phpn_gallery_albums VALUES("4","0bxbqgps","images","5","1");
INSERT INTO phpn_gallery_albums VALUES("5","6z5i5ikr","images","7","1");
INSERT INTO phpn_gallery_albums VALUES("6","gvgbrtmc","images","9","1");



DROP TABLE IF EXISTS phpn_gallery_albums_description;

CREATE TABLE `phpn_gallery_albums_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `gallery_album_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_albums_description VALUES("1","1","es","General de las imágenes","General de las imágenes");
INSERT INTO phpn_gallery_albums_description VALUES("2","1","en","General Images","General Images");
INSERT INTO phpn_gallery_albums_description VALUES("3","1","de","General Images","General Images");
INSERT INTO phpn_gallery_albums_description VALUES("4","2","en","General Video","General Video");
INSERT INTO phpn_gallery_albums_description VALUES("5","2","es","Para Video","Para Video");
INSERT INTO phpn_gallery_albums_description VALUES("6","2","de","General Video","General Video");
INSERT INTO phpn_gallery_albums_description VALUES("7","3","en","Single Rooms","Single Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("8","3","es","Habitaciones individuales","Habitaciones individuales");
INSERT INTO phpn_gallery_albums_description VALUES("9","3","de","Single Rooms","Single Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("10","4","en","Double Rooms","Double Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("11","4","es","Habitaciones Dobles","Habitaciones Dobles");
INSERT INTO phpn_gallery_albums_description VALUES("12","4","de","Double Rooms","Double Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("13","5","en","Superior Rooms","Superior Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("14","5","es","Habitaciones Superiores","Habitaciones Superiores");
INSERT INTO phpn_gallery_albums_description VALUES("15","5","de","Superior Rooms","Superior Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("16","6","en","Luxury Rooms","Luxury Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("17","6","es","Habitaciones de Lujo","Habitaciones de Lujo");
INSERT INTO phpn_gallery_albums_description VALUES("18","6","de","Luxury Rooms","Luxury Rooms");



DROP TABLE IF EXISTS phpn_hotel_images;

CREATE TABLE `phpn_hotel_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL DEFAULT '0',
  `item_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_file_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `priority_order` (`priority_order`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotel_images VALUES("1","1","s9cehwe5z9rk9wsfya6c.jpg","s9cehwe5z9rk9wsfya6c_thumb.jpg","","4","1");
INSERT INTO phpn_hotel_images VALUES("2","1","mxzbx64q3vq12q2t7sbp.jpg","mxzbx64q3vq12q2t7sbp_thumb.jpg","","3","1");
INSERT INTO phpn_hotel_images VALUES("3","1","jg8e1p4dqsu3r6aca1yp.jpg","jg8e1p4dqsu3r6aca1yp_thumb.jpg","","2","1");
INSERT INTO phpn_hotel_images VALUES("4","1","btqjbk705mq89nydo2zb.jpg","btqjbk705mq89nydo2zb_thumb.jpg","","1","1");
INSERT INTO phpn_hotel_images VALUES("5","1","yu0rcyapecboxi0o6td1.jpg","yu0rcyapecboxi0o6td1_thumb.jpg","","0","1");



DROP TABLE IF EXISTS phpn_hotel_payment_gateways;

CREATE TABLE `phpn_hotel_payment_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL DEFAULT '0',
  `payment_type` varchar(20) NOT NULL DEFAULT '',
  `payment_type_name` varchar(50) NOT NULL DEFAULT '',
  `api_login` varchar(40) NOT NULL DEFAULT '',
  `api_key` varchar(40) NOT NULL DEFAULT '',
  `payment_info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO phpn_hotel_payment_gateways VALUES("1","1","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("2","1","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("3","1","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("4","1","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_hotel_payment_gateways VALUES("5","2","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("6","2","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("7","2","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("8","2","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_hotel_payment_gateways VALUES("9","3","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("10","3","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("11","3","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("12","3","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_hotel_payment_gateways VALUES("13","4","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("14","4","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("15","4","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("16","4","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");



DROP TABLE IF EXISTS phpn_hotel_periods;

CREATE TABLE `phpn_hotel_periods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL DEFAULT '0',
  `period_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `finish_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_hotels;

CREATE TABLE `phpn_hotels` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `hotel_location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `property_type_id` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `hotel_group_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time_zone` varchar(5) CHARACTER SET latin1 NOT NULL,
  `map_code` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longitude` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `latitude_center` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longitude_center` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `distance_center` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hotel_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hotel_image_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `agent_commision` decimal(4,1) unsigned NOT NULL DEFAULT '0.0',
  `stars` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `lowest_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `facilities` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `hotel_location_id` (`hotel_location_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels VALUES("1","1","2","1","1-800-123-4567","1-800-123-6789","info@hotel1.com","-8","<iframe width=\"100%\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Hotel+Circle+South,+San+Diego,+California,+United+States&aq=1&sll=33.981232,-84.173813&sspn=0.12868,0.307274&ie=UTF8&hq=&hnear=Hotel+Cir+S,+San+Diego,+California&ll=32.759,-117.177036&spn=0.017215,0.038409&z=14&output=embed\"></iframe>","32.7742488","-117.1411815","","","","hotel_1_a2i7oas7jv6kpwqpjobx.jpg","hotel_1_a2i7oas7jv6kpwqpjobx_thumb.jpg","9.0","4","77.00","a:9:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"5\";i:3;s:1:\"6\";i:4;s:1:\"9\";i:5;s:2:\"10\";i:6;s:2:\"11\";i:7;s:2:\"14\";i:8;s:2:\"16\";}","1","0","1");
INSERT INTO phpn_hotels VALUES("2","2","1","1","1-800-123-2222","1-800-123-2223","info@hotel2.com","-6","<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2948.0965968856144!2d-71.0666208!3d42.361780599999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e3709a3c7713c5%3A0x922cf2c02c6d4e5a!2s5+Blossom+St%2C+Charles+River+Plaza+Shopping+Center%2C+Boston%2C+MA+02114!5e0!3m2!1sen!2s!4v1405938488556\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\"></iframe>","","","","","","hotel_2_s8vxx53oepp2owuetbdk.jpg","hotel_2_s8vxx53oepp2owuetbdk_thumb.jpg","1.5","4","99.00","a:8:{i:0;s:1:\"4\";i:1;s:1:\"6\";i:2;s:1:\"9\";i:3;s:2:\"11\";i:4;s:2:\"12\";i:5;s:2:\"15\";i:6;s:2:\"16\";i:7;s:2:\"17\";}","2","0","1");
INSERT INTO phpn_hotels VALUES("3","3","1","2","1-800-123-2233","1-800-123-1234","info@hotel3.com","-8","<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3021.7983721254104!2d-73.9780617!3d40.7664592!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258f72e9171bb%3A0x3adf1f82b9f2dc62!2sCentral+Park+S%2C+New+York%2C+NY+10019!5e0!3m2!1sen!2s!4v1406544095540\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\"></iframe>","","","","","","hotel_3_ei2xdnx51qmjyc6i8hej.jpg","hotel_3_ei2xdnx51qmjyc6i8hej_thumb.jpg","0.0","5","69.00","a:11:{i:0;s:1:\"1\";i:1;s:1:\"3\";i:2;s:1:\"4\";i:3;s:1:\"8\";i:4;s:1:\"9\";i:5;s:2:\"12\";i:6;s:2:\"14\";i:7;s:2:\"15\";i:8;s:2:\"17\";i:9;s:2:\"19\";i:10;s:2:\"22\";}","3","0","1");
INSERT INTO phpn_hotels VALUES("4","4","1","0","0123123","0123123","test@gmail.com","7","","","","","","","hotel_-1_j6prcg8czli19ydw2uia.jpg","hotel_-1_j6prcg8czli19ydw2uia_thumb.jpg","0.0","0","0.00","a:16:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"5\";i:4;s:1:\"7\";i:5;s:1:\"8\";i:6;s:2:\"10\";i:7;s:2:\"12\";i:8;s:2:\"13\";i:9;s:2:\"15\";i:10;s:2:\"16\";i:11;s:2:\"18\";i:12;s:2:\"19\";i:13;s:2:\"21\";i:14;s:2:\"22\";i:15;s:2:\"23\";}","1","1","1");



DROP TABLE IF EXISTS phpn_hotels_description;

CREATE TABLE `phpn_hotels_description` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '1',
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `name_center_point` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `preferences` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_description VALUES("1","1","en","Rotem King Villa","San Diego, CA 92108, USA","","<p>Step into a world of ease, luxury and Swiss hospitality at The Stamford, our 5-star deluxe hotel. Located in the heart of the city amidst world-class shopping, entertainment and the CBD. The hotel is seated at Turn 9 of the F1 race and 20 minutes away from the Airport.</p>","");
INSERT INTO phpn_hotels_description VALUES("2","1","es","Rotem Hotel Villa","San Diego, CA 92108, USA","","<p>Entra en un mundo de comodidad, lujo y hospitalidad suiza en The Stamford, el hotel de lujo de 5 estrellas. Situado en el corazón de la ciudad en medio de compras de clase mundial, el entretenimiento y el CDB. El hotel está sentado en la curva 9 de la carrera de F1 y 20 minutos del Aeropuerto.</p>","");
INSERT INTO phpn_hotels_description VALUES("3","1","de","Rotem King Villa","San Diego, CA 92108, USA","","<p>Treten Sie ein in eine Welt der Leichtigkeit, Luxus und Schweizer Gastfreundschaft im Stamford, unsere 5-Sterne-Deluxe-Hotel. Das Hotel liegt im Herzen der Stadt inmitten der Weltklasse Shopping, Entertainment und der CBD. Das Hotel befindet sich in Kurve 9 des F1-Rennen sitzen und 20 Minuten vom Flughafen entfernt.</p>","");
INSERT INTO phpn_hotels_description VALUES("4","2","en","Wyndham Boston Hill","5 Blossom Street, Boston, MA 02114","","<p>The Museum of Science and TD Bank Garden are a short distance from this downtown Boston hotel which features completely non-smoking rooms, an on-site restaurant and many modern amenities. Thoughtful amenities at the Wyndham Boston Beacon Hill include a 24-hour fitness center, a seasonal outdoor swimming pool and modern business facilities. Guests can also enjoy on-site dining at Dean\'s List restaurant, which serves small plates and has an extensive drink menu.</p>\n<p>The Boston Wyndham Beacon Hill is near the famed Faneuil Hall and Quincy Market. Boston Common, the Hynes Convention Center and the Boston Public Gardens are also close to the hotel.</p>","Here you may found our hotel preferences.");
INSERT INTO phpn_hotels_description VALUES("5","2","es","Wyndham Boston Hill","5 Blossom Street, Boston, MA 02114","","<p>El Museo de la Ciencia y TD Bank Garden están a poca distancia de este hotel del centro de Boston, que cuenta con habitaciones totalmente para no fumadores, un restaurante del hotel y todas las comodidades modernas. Entre las instalaciones del Wyndham Boston Beacon Hill incluyen un gimnasio abierto las 24 horas, una piscina exterior de temporada y modernas instalaciones de negocios. Los huéspedes también pueden disfrutar de una cena en el lugar en el restaurante Lista del Decano, que sirve platos pequeños y tiene una extensa carta de bebidas.</p>\n<p>El Boston Wyndham Beacon Hill se encuentra cerca del famoso Faneuil Hall y Quincy Market. Boston Common, el Centro de Convenciones Hynes y el Jardín Público de Boston también se encuentran cerca del hotel.</p>","Aquí usted puede encontrado nuestras preferencias de hotel.");
INSERT INTO phpn_hotels_description VALUES("6","2","de","Wyndham Boston Hill","5 Blossom Street, Boston, MA 02114","","<p>Das Museum für Wissenschaft und TD Bank Garden sind nur eine kurze Entfernung von diesem Innenstadt von Boston, die komplett Nichtraucher-Zimmer, ein Restaurant und zahlreiche moderne Annehmlichkeiten bietet. Zu den Annehmlichkeiten im Wyndham Boston Beacon Hill gehören eine 24-Stunden-Fitnesscenter, einen saisonalen Außenpool und moderne Business-Einrichtungen. Die Gäste können auch auf ein Restaurant genießen bei Liste Restaurant Deans, die kleinen Platten serviert und eine umfangreiche Getränkekarte.</p>\n<p>Die Wyndham Boston Beacon Hill ist in der Nähe der berühmten Faneuil Hall und Quincy Market. Boston Common, dem Hynes Convention Center und die Boston Public Gardens sind auch in der Nähe des Hotels.</p>","Hier können Sie unsere Hotelpräferenzen gefunden.");
INSERT INTO phpn_hotels_description VALUES("7","3","en","Park Lany Hotel","37 Central Park South, New York City, NY 10019","","<p>The Park Lany Hotel is a deluxe hotel with a European atmosphere, providing views of Central Park and the New York skyline. It is only a 2-minute walk from the elegant shops of 5th Avenue and the 59th Street N,Q,R subway station. Spacious and bright, the rooms at the Park Lany Hotel are fitted with oversized windows and fine sheets. Amenities include flat-screen TVs and access to the on-site fitness center.</p>\n<p> </p>\n<p>Enjoy a seasonal menu created by Jeff Cristelli at The Park Room Restaurant, or select from a wide variety of cocktails at Harry\'s New York Bar. New York’s Museum of Modern Art, Radio City Music Hall, and Rockefeller Center, are all within a 10-minute walk. With the park right in front of the hotel, jogging and recreational activities are easily accessible.</p>","If in your dream you see a big hotel with good furniture, ahead you will have the life full of pleasure. Stay in shabby dirty hotel with the sick owner dreams to failure or receiving a sad vital lesson.");
INSERT INTO phpn_hotels_description VALUES("8","3","es","Park Lany Hotel","37 Central Park South, New York City, NY 10019","","<p>El Parque Lany Hotel es un hotel de lujo con un ambiente europeo, ofrece vistas a Central Park y el horizonte de Nueva York. Es sólo un 2 minutos a pie de las elegantes tiendas de la 5 ª Avenida y la calle 59 N, Q, R estación de metro. Amplias y luminosas, las habitaciones del Park Lany Hotel están equipadas con ventanas de gran tamaño y las hojas finas. Las comodidades incluyen TV de pantalla plana y acceso a un gimnasio en el lugar.</p>\n<p> </p>\n<p>Disfrute de un menú de temporada creada por Jeff Cristelli en The Park Room Restaurant, o elegir entre una amplia variedad de cócteles en casa de Harry New York Bar. Museo de Arte Moderno, el Radio City Music Hall y el Rockefeller Center de Nueva York, se encuentran a 10 minutos a pie. Con el parque justo en frente del hotel, footing y actividades recreativas son fácilmente accesibles.</p>","Si en su sueño se ve un gran hotel con un buen mobiliario, por delante tendrá la vida llena de placer. Permanezca en mal estado sucio hotel con el dueño enfermo sueña al fracaso o al recibir una lección vital triste.");
INSERT INTO phpn_hotels_description VALUES("9","3","de","Park Lany Hotel","37 Central Park South, New York City, NY 10019","","<p>Das Park Hotel ist ein Lany-Deluxe-Hotel mit europäischer Atmosphäre, die den Blick auf den Central Park und die Skyline von New York. Es ist nur 2 Gehminuten von den eleganten Geschäften der 5th Avenue und der 59th Street N, Q, R U-Bahn-Station. Geräumig und hell sind die Zimmer des Park Hotels Lany mit übergroßen Fenstern und Feinbleche ausgestattet. Zur Ausstattung gehören Flachbild-TV und Zugang zum hoteleigenen Fitnesscenter .</p>\n<p> </p>\n<p>Genießen Sie ein saisonales Menü von Jeff Cristelli im The Park Room Restaurant, oder aus einer Vielzahl von Cocktails in Harrys New York Bar wählen. Museum of Modern Art, die Radio City Music Hall und das Rockefeller Center in New York, sind alle innerhalb von 10 Gehminuten. Mit dem Park direkt vor dem Hotel, Jogging-und Freizeitaktivitäten sind leicht zugänglich.</p>","Wenn in Ihrem Traum sehen Sie ein großes Hotel mit guten Möbeln, vor Ihnen das Leben voller Freude zu haben. Bleiben Sie in schäbigen dreckiges Hotel mit dem kranken Besitzer träumt zum Scheitern oder Empfangen eines traurigen wichtige Lektion.");
INSERT INTO phpn_hotels_description VALUES("10","4","en","TEST","TEST","TEST","<p>TEST</p>","TEST");
INSERT INTO phpn_hotels_description VALUES("11","4","es","TEST","TEST","TEST","","TEST");
INSERT INTO phpn_hotels_description VALUES("12","4","de","TEST","TEST","TEST","<p>TEST</p>","TEST");



DROP TABLE IF EXISTS phpn_hotels_locations;

CREATE TABLE `phpn_hotels_locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_locations VALUES("1","US","9","1");
INSERT INTO phpn_hotels_locations VALUES("2","US","2","1");
INSERT INTO phpn_hotels_locations VALUES("3","US","3","1");
INSERT INTO phpn_hotels_locations VALUES("4","ID","0","1");



DROP TABLE IF EXISTS phpn_hotels_locations_description;

CREATE TABLE `phpn_hotels_locations_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_location_id` (`hotel_location_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_locations_description VALUES("1","1","es","Ronston");
INSERT INTO phpn_hotels_locations_description VALUES("2","1","de","Ronston");
INSERT INTO phpn_hotels_locations_description VALUES("3","1","en","Ronston");
INSERT INTO phpn_hotels_locations_description VALUES("4","2","en","Boston");
INSERT INTO phpn_hotels_locations_description VALUES("5","2","es","Boston");
INSERT INTO phpn_hotels_locations_description VALUES("6","2","de","Boston");
INSERT INTO phpn_hotels_locations_description VALUES("7","3","en","New York City");
INSERT INTO phpn_hotels_locations_description VALUES("8","3","es","New York City");
INSERT INTO phpn_hotels_locations_description VALUES("9","3","de","New York City");
INSERT INTO phpn_hotels_locations_description VALUES("10","4","en","Lombok");
INSERT INTO phpn_hotels_locations_description VALUES("11","4","es","Lombok");
INSERT INTO phpn_hotels_locations_description VALUES("12","4","de","Lombok");



DROP TABLE IF EXISTS phpn_hotels_property_types;

CREATE TABLE `phpn_hotels_property_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `property_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_property_types VALUES("1","hotels","1","1","1");
INSERT INTO phpn_hotels_property_types VALUES("2","villas","2","0","1");



DROP TABLE IF EXISTS phpn_hotels_property_types_description;

CREATE TABLE `phpn_hotels_property_types_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_property_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_property_id` (`hotel_property_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_property_types_description VALUES("1","1","en","Hotels");
INSERT INTO phpn_hotels_property_types_description VALUES("2","1","es","Hoteles");
INSERT INTO phpn_hotels_property_types_description VALUES("3","1","de","Hotels");
INSERT INTO phpn_hotels_property_types_description VALUES("4","2","en","Villas");
INSERT INTO phpn_hotels_property_types_description VALUES("5","2","es","Villas");
INSERT INTO phpn_hotels_property_types_description VALUES("6","2","de","Villas");



DROP TABLE IF EXISTS phpn_languages;

CREATE TABLE `phpn_languages` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `lang_name_en` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `lc_time_name` varchar(5) CHARACTER SET latin1 NOT NULL DEFAULT 'en_US',
  `lang_dir` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'ltr',
  `icon_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `used_on` enum('front-end','back-end','global') CHARACTER SET latin1 NOT NULL DEFAULT 'global',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_languages VALUES("1","English","English","en","en_US","ltr","en.gif","1","global","1","1");
INSERT INTO phpn_languages VALUES("2","Español","Spanish","es","es_ES","ltr","es.gif","2","global","0","1");
INSERT INTO phpn_languages VALUES("3","Deutsch","German","de","de_DE","ltr","de.gif","3","global","0","1");



DROP TABLE IF EXISTS phpn_mass_mail_log;

CREATE TABLE `phpn_mass_mail_log` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `email_to` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email_template_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_mass_mail_log VALUES("1","1","admins","test_template","2016-02-01 01:12:20");
INSERT INTO phpn_mass_mail_log VALUES("2","1","test","test_template","2016-03-01 01:12:20");
INSERT INTO phpn_mass_mail_log VALUES("3","1","all","test_template","2016-03-02 02:22:40");



DROP TABLE IF EXISTS phpn_meal_plans;

CREATE TABLE `phpn_meal_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `charge_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Per person per night',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_meal_plans VALUES("1","1","0.00","0","0","1","1");
INSERT INTO phpn_meal_plans VALUES("2","1","10.00","0","1","1","0");
INSERT INTO phpn_meal_plans VALUES("3","1","22.00","0","2","1","0");
INSERT INTO phpn_meal_plans VALUES("5","2","19.00","0","0","1","1");
INSERT INTO phpn_meal_plans VALUES("6","2","33.00","0","1","1","0");
INSERT INTO phpn_meal_plans VALUES("7","3","39.90","0","0","1","0");



DROP TABLE IF EXISTS phpn_meal_plans_description;

CREATE TABLE `phpn_meal_plans_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meal_plan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_meal_plans_description VALUES("1","1","en","Breakfast (Included)","One meal supplied");
INSERT INTO phpn_meal_plans_description VALUES("2","1","es","Desayuno (Incluido)","");
INSERT INTO phpn_meal_plans_description VALUES("3","1","de","Frühstück (Im Lieferumfang)","");
INSERT INTO phpn_meal_plans_description VALUES("4","2","en","Half Board","Two meals (no lunch) supplied");
INSERT INTO phpn_meal_plans_description VALUES("5","2","es","Media pensión","");
INSERT INTO phpn_meal_plans_description VALUES("6","2","de","Halbpension","");
INSERT INTO phpn_meal_plans_description VALUES("7","3","en","Full Board","Three meals supplied");
INSERT INTO phpn_meal_plans_description VALUES("8","3","es","Pensión Completa","");
INSERT INTO phpn_meal_plans_description VALUES("9","3","de","Vollpension","");
INSERT INTO phpn_meal_plans_description VALUES("10","5","en","Breakfast","");
INSERT INTO phpn_meal_plans_description VALUES("11","5","es","Desayuno","");
INSERT INTO phpn_meal_plans_description VALUES("12","5","de","Frühstück","");
INSERT INTO phpn_meal_plans_description VALUES("13","6","en","Full Board","");
INSERT INTO phpn_meal_plans_description VALUES("14","6","es","Pensión Completa","");
INSERT INTO phpn_meal_plans_description VALUES("15","6","de","Vollpension","");
INSERT INTO phpn_meal_plans_description VALUES("16","7","en","Full Board","");
INSERT INTO phpn_meal_plans_description VALUES("17","7","es","Full Board","");
INSERT INTO phpn_meal_plans_description VALUES("18","7","de","Full Board","");



DROP TABLE IF EXISTS phpn_menus;

CREATE TABLE `phpn_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `menu_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_placement` enum('','left','top','right','bottom','hidden') CHARACTER SET latin1 NOT NULL,
  `menu_order` tinyint(3) DEFAULT '1',
  `access_level` enum('public','registered') CHARACTER SET latin1 NOT NULL DEFAULT 'public',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_menus VALUES("1","AMM8WBAKJ9","en","Information","left","1","public");
INSERT INTO phpn_menus VALUES("2","AMM8WBAKJ9","es","Información","left","1","public");
INSERT INTO phpn_menus VALUES("3","AMM8WBAKJ9","de","Information","left","1","public");



DROP TABLE IF EXISTS phpn_modules;

CREATE TABLE `phpn_modules` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name_const` varchar(20) CHARACTER SET latin1 NOT NULL,
  `description_const` varchar(30) CHARACTER SET latin1 NOT NULL,
  `icon_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `module_tables` varchar(255) CHARACTER SET latin1 NOT NULL,
  `dependent_modules` varchar(20) CHARACTER SET latin1 NOT NULL,
  `settings_page` varchar(30) CHARACTER SET latin1 NOT NULL,
  `settings_const` varchar(30) CHARACTER SET latin1 NOT NULL,
  `settings_access_by` varchar(50) CHARACTER SET latin1 NOT NULL,
  `management_page` varchar(125) CHARACTER SET latin1 NOT NULL,
  `management_const` varchar(125) CHARACTER SET latin1 NOT NULL,
  `management_access_by` varchar(50) CHARACTER SET latin1 NOT NULL,
  `is_installed` tinyint(1) NOT NULL DEFAULT '0',
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_on_dashboard` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `priority_order` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_modules VALUES("1","rooms","_ROOMS","_MD_ROOMS","rooms.png","rooms,rooms_availabilities,rooms_description,rooms_prices,room_facilities,room_facilities_description","","mod_rooms_settings","_ROOMS_SETTINGS","owner,mainadmin","mod_rooms_management","_ROOMS_MANAGEMENT","owner,mainadmin","1","1","1","0");
INSERT INTO phpn_modules VALUES("2","pages","_PAGES","_MD_PAGES","pages.png","pages,menus","","","","owner,mainadmin","pages","_PAGE_EDIT_PAGES","owner,mainadmin","1","1","1","1");
INSERT INTO phpn_modules VALUES("3","customers","_CUSTOMERS","_MD_CUSTOMERS","customers.png","customers","","mod_customers_settings","_CUSTOMERS_SETTINGS","owner,mainadmin","","","","1","0","0","0");
INSERT INTO phpn_modules VALUES("4","booking","_BOOKINGS","_MD_BOOKINGS","booking.png","bookings,bookings_rooms,extras","","mod_booking_settings","_BOOKINGS_SETTINGS","owner,mainadmin","","","","1","0","1","1");
INSERT INTO phpn_modules VALUES("5","car_rental","_CAR_RENTAL","_MD_CAR_RENTAL","car_rental.png","car_agencies,car_agencies_description","","","","owner,mainadmin","mod_car_rental_agencies","","owner,mainadmin","1","0","1","2");
INSERT INTO phpn_modules VALUES("6","contact_us","_CONTACT_US","_MD_CONTACT_US","contact_us.png","","","mod_contact_us_settings","_CONTACT_US_SETTINGS","owner,mainadmin","","","","1","0","0","3");
INSERT INTO phpn_modules VALUES("7","comments","_COMMENTS","_MD_COMMENTS","comments.png","comments","","mod_comments_settings","_COMMENTS_SETTINGS","owner,mainadmin","mod_comments_management","_COMMENTS_MANAGEMENT","owner,mainadmin","1","0","0","4");
INSERT INTO phpn_modules VALUES("8","ratings","_RATINGS","_MD_RATINGS","ratings.png","ratings_items,ratings_users","","mod_ratings_settings","_RATINGS_SETTINGS","owner,mainadmin","","","","1","0","0","5");
INSERT INTO phpn_modules VALUES("9","testimonials","_TESTIMONIALS","_MD_TESTIMONIALS","testimonials.png","testimonials","","mod_testimonials_settings","_TESTIMONIALS_SETTINGS","owner,mainadmin","mod_testimonials_management","_TESTIMONIALS_MANAGEMENT","owner,mainadmin","1","0","0","6");
INSERT INTO phpn_modules VALUES("10","reviews","_REVIEWS","_MD_REVIEWS","reviews.png","reviews","","mod_reviews_settings","_REVIEWS_SETTINGS","owner,mainadmin","mod_reviews_management","_REVIEWS_MANAGEMENT","owner,mainadmin,hotelowner","1","0","1","7");
INSERT INTO phpn_modules VALUES("11","news","_NEWS","_MD_NEWS","news.png","news,events_registered,news_subscribed","","mod_news_settings","_NEWS_SETTINGS","owner,mainadmin","mod_news_management,mod_news_subscribed","_NEWS_MANAGEMENT,_SUBSCRIPTION_MANAGEMENT","owner,mainadmin","1","0","0","8");
INSERT INTO phpn_modules VALUES("12","gallery","_GALLERY","_MD_GALLERY","gallery.png","gallery_albums,gallery_images","","mod_gallery_settings","_GALLERY_SETTINGS","owner,mainadmin","mod_gallery_management","_GALLERY_MANAGEMENT","owner,mainadmin","1","0","0","9");
INSERT INTO phpn_modules VALUES("13","banners","_BANNERS","_MD_BANNERS","banners.png","banners","","mod_banners_settings","_BANNERS_SETTINGS","owner,mainadmin","mod_banners_management","_BANNERS_MANAGEMENT","owner,mainadmin","1","0","0","10");
INSERT INTO phpn_modules VALUES("14","faq","_FAQ","_MD_FAQ","faq.png","faq_categories,faq_category_items","","mod_faq_settings","_FAQ_SETTINGS","owner,mainadmin","mod_faq_management","_FAQ_MANAGEMENT","owner,mainadmin","1","0","0","11");
INSERT INTO phpn_modules VALUES("15","backup","_BACKUP_AND_RESTORE","_MD_BACKUP_AND_RESTORE","backup.png","","","mod_backup_installation","_BACKUP_INSTALLATION","owner","mod_backup_restore","_BACKUP_RESTORE","owner,mainadmin","1","0","0","12");



DROP TABLE IF EXISTS phpn_modules_settings;

CREATE TABLE `phpn_modules_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(20) CHARACTER SET latin1 NOT NULL,
  `settings_key` varchar(40) CHARACTER SET latin1 NOT NULL,
  `settings_value` text COLLATE utf8_unicode_ci NOT NULL,
  `settings_name_const` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `settings_description_const` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `key_display_type` enum('string','email','numeric','unsigned float','integer','positive integer','unsigned integer','enum','yes/no','html size','text') CHARACTER SET latin1 NOT NULL,
  `key_is_required` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `key_display_source` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'for ''enum'' field type',
  PRIMARY KEY (`id`),
  KEY `module_name` (`module_name`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_modules_settings VALUES("1","banners","is_active","yes","_MSN_BANNERS_IS_ACTIVE","_MS_BANNERS_IS_ACTIVE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("2","banners","rotation_type","slide show","_MSN_ROTATION_TYPE","_MS_ROTATION_TYPE","enum","1","random image,slide show");
INSERT INTO phpn_modules_settings VALUES("3","banners","rotate_delay","9","_MSN_ROTATE_DELAY","_MS_ROTATE_DELAY","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("4","banners","slideshow_caption_html","no","_MSN_BANNERS_CAPTION_HTML","_MS_BANNERS_CAPTION_HTML","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("5","booking","is_active","global","_MSN_ACTIVATE_BOOKINGS","_MS_ACTIVATE_BOOKINGS","enum","1","front-end,back-end,global,no");
INSERT INTO phpn_modules_settings VALUES("6","booking","payment_type_poa","Frontend & Backend","_MSN_PAYMENT_TYPE_POA","_MS_PAYMENT_TYPE_POA","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("7","booking","payment_type_online","Frontend & Backend","_MSN_PAYMENT_TYPE_ONLINE","_MS_PAYMENT_TYPE_ONLINE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("8","booking","online_credit_card_required","yes","_MSN_ONLINE_CREDIT_CARD_REQUIRED","_MS_ONLINE_CREDIT_CARD_REQUIRED","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("9","booking","payment_type_bank_transfer","Frontend & Backend","_MSN_PAYMENT_TYPE_BANK_TRANSFER","_MS_PAYMENT_TYPE_BANK_TRANSFER","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("10","booking","bank_transfer_info","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n","_MSN_BANK_TRANSFER_INFO","_MS_BANK_TRANSFER_INFO","text","0","");
INSERT INTO phpn_modules_settings VALUES("11","booking","payment_type_paypal","Frontend & Backend","_MSN_PAYMENT_TYPE_PAYPAL","_MS_PAYMENT_TYPE_PAYPAL","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("12","booking","paypal_email","paypal@yourdomain.com","_MSN_PAYPAL_EMAIL","_MS_PAYPAL_EMAIL","email","1","");
INSERT INTO phpn_modules_settings VALUES("13","booking","payment_type_2co","Frontend & Backend","_MSN_PAYMENT_TYPE_2CO","_MS_PAYMENT_TYPE_2CO","enum","0","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("14","booking","two_checkout_vendor","Your 2CO Vendor ID here","_MSN_TWO_CHECKOUT_VENDOR","_MS_TWO_CHECKOUT_VENDOR","string","1","");
INSERT INTO phpn_modules_settings VALUES("15","booking","payment_type_authorize","Frontend & Backend","_MSN_PAYMENT_TYPE_AUTHORIZE","_MS_PAYMENT_TYPE_AUTHORIZE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("16","booking","authorize_login_id","Your API Login ID here","_MSN_AUTHORIZE_LOGIN_ID","_MS_AUTHORIZE_LOGIN_ID","string","1","");
INSERT INTO phpn_modules_settings VALUES("17","booking","authorize_transaction_key","Your Transaction Key here","_MSN_AUTHORIZE_TRANSACTION_KEY","_MS_AUTHORIZE_TRANSACTION_KEY","string","1","");
INSERT INTO phpn_modules_settings VALUES("18","booking","default_payment_system","paypal","_MSN_DEFAULT_PAYMENT_SYSTEM","_MS_DEFAULT_PAYMENT_SYSTEM","enum","1","poa,online,bank transfer,paypal,2co,authorize.net");
INSERT INTO phpn_modules_settings VALUES("19","booking","allow_separate_gateways","no","_MSN_SEPARATE_GATEWAYS","_MS_SEPARATE_GATEWAYS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("20","booking","send_order_copy_to_admin","yes","_MSN_SEND_ORDER_COPY_TO_ADMIN","_MS_SEND_ORDER_COPY_TO_ADMIN","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("21","booking","allow_booking_without_account","yes","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("22","booking","pre_payment_type","percentage","_MSN_PRE_PAYMENT_TYPE","_MS_PRE_PAYMENT_TYPE","enum","1","full price,first night,fixed sum,percentage");
INSERT INTO phpn_modules_settings VALUES("23","booking","pre_payment_value","10","_MSN_PRE_PAYMENT_VALUE","_MS_PRE_PAYMENT_VALUE","enum","0","1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,99,100,125,150,175,200,250,500,750,1000");
INSERT INTO phpn_modules_settings VALUES("24","booking","vat_value","0","_MSN_VAT_VALUE","_MS_VAT_VALUE","unsigned float","0","");
INSERT INTO phpn_modules_settings VALUES("25","booking","minimum_nights","1","_MSN_MINIMUM_NIGHTS","_MS_MINIMUM_NIGHTS","enum","1","1,2,3,4,5,6,7,8,9,10,14,21,28,30,45,60,90");
INSERT INTO phpn_modules_settings VALUES("26","booking","maximum_nights","90","_MSN_MAXIMUM_NIGHTS","_MS_MAXIMUM_NIGHTS","enum","1","1,2,3,4,5,6,7,8,9,10,14,21,28,30,45,60,90,120,150,180,240,360");
INSERT INTO phpn_modules_settings VALUES("27","booking","mode","REAL MODE","_MSN_BOOKING_MODE","_MS_BOOKING_MODE","enum","1","TEST MODE,REAL MODE");
INSERT INTO phpn_modules_settings VALUES("28","booking","show_fully_booked_rooms","no","_MSN_SHOW_FULLY_BOOKED_ROOMS","_MS_SHOW_FULLY_BOOKED_ROOMS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("29","booking","prebooking_orders_timeout","2","_MSN_PREBOOKING_ORDERS_TIMEOUT","_MS_PREBOOKING_ORDERS_TIMEOUT","enum","1","0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,22,24,36,48,72");
INSERT INTO phpn_modules_settings VALUES("30","booking","customers_cancel_reservation","7","_MSN_CUSTOMERS_CANCEL_RESERVATION","_MS_CUSTOMERS_CANCEL_RESERVATION","enum","1","0,1,2,3,4,5,6,7,10,14,21,30,45,60");
INSERT INTO phpn_modules_settings VALUES("31","booking","show_reservation_form","yes","_MSN_SHOW_RESERVATION_FORM","_MS_SHOW_RESERVATION_FORM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("32","booking","booking_initial_fee","0","_MSN_RESERVATION_INITIAL_FEE","_MS_RESERVATION_INITIAL_FEE","unsigned float","1","");
INSERT INTO phpn_modules_settings VALUES("33","booking","booking_number_type","random","_MSN_BOOKING_NUMBER_TYPE","_MS_BOOKING_NUMBER_TYPE","enum","1","random,sequential");
INSERT INTO phpn_modules_settings VALUES("34","booking","vat_included_in_price","no","_MSN_VAT_INCLUDED_IN_PRICE","_MS_VAT_INCLUDED_IN_PRICE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("35","booking","show_booking_status_form","yes","_MSN_SHOW_BOOKING_STATUS_FORM","_MS_SHOW_BOOKING_STATUS_FORM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("36","booking","maximum_allowed_reservations","10","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","_MS_MAXIMUM_ALLOWED_RESERVATIONS","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("37","booking","first_night_calculating_type","real","_MSN_FIRST_NIGHT_CALCULATING_TYPE","_MS_FIRST_NIGHT_CALCULATING_TYPE","enum","1","real,average");
INSERT INTO phpn_modules_settings VALUES("38","booking","available_until_approval","no","_MSN_AVAILABLE_UNTIL_APPROVAL","_MS_AVAILABLE_UNTIL_APPROVAL","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("39","booking","reservation_expired_alert","no","_MSN_RESERVATION_EXPIRED_ALERT","_MS_RESERVATION EXPIRED_ALERT","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("40","booking","allow_booking_in_past","no","_MSN_ADMIN_BOOKING_IN_PAST","_MS_ADMIN_BOOKING_IN_PAST","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("41","booking","allow_payment_with_balance","yes","_MSN_ALLOW_PAYMENT_WITH_BALANCE","_MS_ALLOW_PAYMENT_WITH_BALANCE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("42","booking","special_tax","0","_MSN_SPECIAL_TAX","_MS_SPECIAL_TAX","unsigned float","1","");
INSERT INTO phpn_modules_settings VALUES("43","car_rental","is_active","yes","_MSN_ACTIVATE_CAR_RENTAL","_MS_ACTIVATE_CAR_RENTAL","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("44","car_rental","allow_separate_gateways","yes","_MSN_TC_SEPARATE_GATEWAYS","_MS_TC_SEPARATE_GATEWAYS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("45","car_rental","payment_type_poa","Frontend & Backend","_MSN_PAYMENT_TYPE_POA","_MS_PAYMENT_TYPE_POA","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("46","car_rental","payment_type_online","Frontend & Backend","_MSN_PAYMENT_TYPE_ONLINE","_MS_PAYMENT_TYPE_ONLINE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("47","car_rental","online_credit_card_required","yes","_MSN_ONLINE_CREDIT_CARD_REQUIRED","_MS_ONLINE_CREDIT_CARD_REQUIRED","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("48","car_rental","payment_type_bank_transfer","Frontend & Backend","_MSN_PAYMENT_TYPE_BANK_TRANSFER","_MS_PAYMENT_TYPE_BANK_TRANSFER","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("49","car_rental","bank_transfer_info","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n","_MSN_BANK_TRANSFER_INFO","_MS_BANK_TRANSFER_INFO","text","0","");
INSERT INTO phpn_modules_settings VALUES("50","car_rental","payment_type_paypal","Frontend & Backend","_MSN_PAYMENT_TYPE_PAYPAL","_MS_PAYMENT_TYPE_PAYPAL","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("51","car_rental","paypal_email","paypal@yourdomain.com","_MSN_PAYPAL_EMAIL","_MS_PAYPAL_EMAIL","email","1","");
INSERT INTO phpn_modules_settings VALUES("52","car_rental","payment_type_2co","Frontend & Backend","_MSN_PAYMENT_TYPE_2CO","_MS_PAYMENT_TYPE_2CO","enum","0","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("53","car_rental","two_checkout_vendor","Your 2CO Vendor ID here","_MSN_TWO_CHECKOUT_VENDOR","_MS_TWO_CHECKOUT_VENDOR","string","1","");
INSERT INTO phpn_modules_settings VALUES("54","car_rental","payment_type_authorize","Frontend & Backend","_MSN_PAYMENT_TYPE_AUTHORIZE","_MS_PAYMENT_TYPE_AUTHORIZE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("55","car_rental","authorize_login_id","Your API Login ID here","_MSN_AUTHORIZE_LOGIN_ID","_MS_AUTHORIZE_LOGIN_ID","string","1","");
INSERT INTO phpn_modules_settings VALUES("56","car_rental","authorize_transaction_key","Your Transaction Key here","_MSN_AUTHORIZE_TRANSACTION_KEY","_MS_AUTHORIZE_TRANSACTION_KEY","string","1","");
INSERT INTO phpn_modules_settings VALUES("57","car_rental","default_payment_system","paypal","_MSN_DEFAULT_PAYMENT_SYSTEM","_MS_DEFAULT_PAYMENT_SYSTEM","enum","1","poa,online,bank transfer,paypal,2co,authorize.net");
INSERT INTO phpn_modules_settings VALUES("58","car_rental","pre_payment_type","percentage","_MSN_PRE_PAYMENT_TYPE","_MS_PRE_PAYMENT_TYPE","enum","1","full price,first night,fixed sum,percentage");
INSERT INTO phpn_modules_settings VALUES("59","car_rental","pre_payment_value","10","_MSN_PRE_PAYMENT_VALUE","_MS_PRE_PAYMENT_VALUE","enum","0","1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,99,100,125,150,175,200,250,500,750,1000");
INSERT INTO phpn_modules_settings VALUES("60","car_rental","vat_value","0","_MSN_VAT_VALUE","_MS_VAT_VALUE","unsigned float","0","");
INSERT INTO phpn_modules_settings VALUES("61","car_rental","mode","REAL MODE","_MSN_BOOKING_MODE","_MS_BOOKING_MODE","enum","1","TEST MODE,REAL MODE");
INSERT INTO phpn_modules_settings VALUES("62","car_rental","show_agency_in_search_result","yes","_MSN_AGENCY_IN_SEARCH_RESULT","_MS_AGENCY_IN_SEARCH_RESULT","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("63","car_rental","default_contant_email","","_MSN_TC_DEFAULT_CONTACT_EMAIL","_MS_TC_DEFAULT_CONTACT_EMAIL","email","0","");
INSERT INTO phpn_modules_settings VALUES("64","car_rental","default_contant_phone","","_MSN_TC_DEFAULT_CONTACT_PHONE","_MS_TC_DEFAULT_CONTACT_PHONE","string","0","");
INSERT INTO phpn_modules_settings VALUES("65","car_rental","customers_cancel_reservation","7","_MSN_CUSTOMERS_CANCEL_RESERVATION","_MS_CUSTOMERS_CANCEL_RESERVATION","enum","1","0,1,2,3,4,5,6,7,10,14,21,30,45,60");
INSERT INTO phpn_modules_settings VALUES("66","car_rental","vat_included_in_price","no","_MSN_VAT_INCLUDED_IN_PRICE","_MS_VAT_INCLUDED_IN_PRICE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("67","car_rental","allow_reservation_without_account","yes","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("68","car_rental","allow_payment_with_balance","yes","_MSN_ALLOW_PAYMENT_WITH_BALANCE","_MS_ALLOW_PAYMENT_WITH_BALANCE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("69","car_rental","prebooking_orders_timeout","2","_MSN_PREBOOKING_ORDERS_TIMEOUT","_MS_PREBOOKING_ORDERS_TIMEOUT","enum","1","0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,22,24,36,48,72");
INSERT INTO phpn_modules_settings VALUES("70","car_rental","reservation_expired_alert","no","_MSN_RESERVATION_EXPIRED_ALERT","_MS_RESERVATION EXPIRED_ALERT","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("71","car_rental","send_order_copy_to_admin","yes","_MSN_SEND_ORDER_COPY_TO_ADMIN","_MS_SEND_ORDER_COPY_TO_ADMIN","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("72","car_rental","reservation_number_type","random","_MSN_RESERVATION_NUMBER_TYPE","_MS_RESERVATION_NUMBER_TYPE","enum","1","random,sequential");
INSERT INTO phpn_modules_settings VALUES("73","comments","comments_allow","yes","_MSN_COMMENTS_ALLOW","_MS_COMMENTS_ALLOW","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("74","comments","user_type","all","_MSN_USER_TYPE","_MS_USER_TYPE","enum","1","all,registered");
INSERT INTO phpn_modules_settings VALUES("75","comments","comment_length","500","_MSN_COMMENTS_LENGTH","_MS_COMMENTS_LENGTH","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("76","comments","image_verification_allow","yes","_MSN_IMAGE_VERIFICATION_ALLOW","_MS_IMAGE_VERIFICATION_ALLOW","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("77","comments","page_size","20","_MSN_COMMENTS_PAGE_SIZE","_MS_COMMENTS_PAGE_SIZE","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("78","comments","pre_moderation_allow","yes","_MSN_PRE_MODERATION_ALLOW","_MS_PRE_MODERATION_ALLOW","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("79","comments","delete_pending_time","2","_MSN_DELETE_PENDING_TIME","_MS_DELETE_PENDING_TIME","enum","1","0,1,2,3,4,5,6,7,8,9,10,15,20,30,45,60,120,180");
INSERT INTO phpn_modules_settings VALUES("80","contact_us","key","{module:contact_us}","_MSN_CONTACT_US_KEY","_MS_CONTACT_US_KEY","enum","1","{module:contact_us}");
INSERT INTO phpn_modules_settings VALUES("81","contact_us","email","contact@yourdomain.com","_MSN_EMAIL","_MS_EMAIL","email","1","");
INSERT INTO phpn_modules_settings VALUES("82","contact_us","is_send_delay","yes","_MSN_IS_SEND_DELAY","_MS_IS_SEND_DELAY","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("83","contact_us","delay_length","20","_MSN_DELAY_LENGTH","_MS_DELAY_LENGTH","positive integer","0","");
INSERT INTO phpn_modules_settings VALUES("84","contact_us","image_verification_allow","yes","_MSN_IMAGE_VERIFICATION_ALLOW","_MS_IMAGE_VERIFICATION_ALLOW","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("85","customers","allow_adding_by_admin","yes","_MSN_ALLOW_ADDING_BY_ADMIN","_MS_ALLOW_ADDING_BY_ADMIN","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("86","customers","reg_confirmation","by admin","_MSN_REG_CONFIRMATION","_MS_REG_CONFIRMATION","enum","0","automatic,by email,by admin");
INSERT INTO phpn_modules_settings VALUES("87","customers","image_verification_allow","yes","_MSN_CUSTOMERS_IMAGE_VERIFICATION","_MS_CUSTOMERS_IMAGE_VERIFICATION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("88","customers","allow_login","yes","_MSN_ALLOW_CUSTOMERS_LOGIN","_MS_ALLOW_CUSTOMERS_LOGIN","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("89","customers","allow_registration","yes","_MSN_ALLOW_CUSTOMERS_REGISTRATION","_MS_ALLOW_CUSTOMERS_REGISTRATION","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("90","customers","password_changing_by_admin","yes","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("91","customers","allow_reset_passwords","yes","_MSN_ALLOW_CUST_RESET_PASSWORDS","_MS_ALLOW_CUST_RESET_PASSWORDS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("92","customers","admin_alert_new_registration","yes","_MSN_ALERT_ADMIN_NEW_REGISTRATION","_MS_ALERT_ADMIN_NEW_REGISTRATION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("93","customers","remember_me_allow","yes","_MSN_REMEMBER_ME","_MS_REMEMBER_ME","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("94","customers","allow_agencies","yes","_MSN_ALLOW_AGENCIES","_MS_ALLOW_AGENCIES","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("95","faq","is_active","yes","_MSN_FAQ_IS_ACTIVE","_MS_FAQ_IS_ACTIVE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("96","gallery","key","{module:gallery}","_MSN_GALLERY_KEY","_MS_GALLERY_KEY","enum","1","{module:gallery}");
INSERT INTO phpn_modules_settings VALUES("97","gallery","album_key","{module:album=CODE}","_MSN_ALBUM_KEY","_MS_ALBUM_KEY","enum","1","{module:album=CODE}");
INSERT INTO phpn_modules_settings VALUES("98","gallery","image_gallery_type","lytebox","_MSN_IMAGE_GALLERY_TYPE","_MS_IMAGE_GALLERY_TYPE","enum","1","lytebox,rokbox");
INSERT INTO phpn_modules_settings VALUES("99","gallery","album_icon_width","140px","_MSN_ALBUM_ICON_WIDTH","_MS_ALBUM_ICON_WIDTH","html size","1","");
INSERT INTO phpn_modules_settings VALUES("100","gallery","album_icon_height","105px","_MSN_ALBUM_ICON_HEIGHT","_MS_ALBUM_ICON_HEIGHT","html size","1","");
INSERT INTO phpn_modules_settings VALUES("101","gallery","albums_per_line","4","_MSN_ALBUMS_PER_LINE","_MS_ALBUMS_PER_LINE","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("102","gallery","video_gallery_type","rokbox","_MSN_VIDEO_GALLERY_TYPE","_MS_VIDEO_GALLERY_TYPE","enum","1","rokbox,videobox");
INSERT INTO phpn_modules_settings VALUES("103","gallery","wrapper","table","_MSN_GALLERY_WRAPPER","_MS_GALLERY_WRAPPER","enum","1","table,div");
INSERT INTO phpn_modules_settings VALUES("104","gallery","show_items_count_in_album","yes","_MSN_ITEMS_COUNT_IN_ALBUM","_MS_ITEMS_COUNT_IN_ALBUM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("105","gallery","show_items_numeration_in_album","yes","_MSN_ALBUM_ITEMS_NUMERATION","_MS_ALBUM_ITEMS_NUMERATION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("106","news","news_count","5","_MSN_NEWS_COUNT","_MS_NEWS_COUNT","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("107","news","news_header_length","80","_MSN_NEWS_HEADER_LENGTH","_MS_NEWS_HEADER_LENGTH","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("108","news","news_rss","yes","_MSN_NEWS_RSS","_MS_NEWS_RSS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("109","news","show_news_block","left side","_MSN_SHOW_NEWS_BLOCK","_MS_SHOW_NEWS_BLOCK","enum","1","no,left side,right side");
INSERT INTO phpn_modules_settings VALUES("110","news","show_newsletter_subscribe_block","no","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","enum","1","no,left side,right side");
INSERT INTO phpn_modules_settings VALUES("111","ratings","user_type","registered","_MSN_RATINGS_USER_TYPE","_MS_RATINGS_USER_TYPE","enum","1","all,registered");
INSERT INTO phpn_modules_settings VALUES("112","ratings","multiple_items_per_day","yes","_MSN_MULTIPLE_ITEMS_PER_DAY","_MS_MULTIPLE_ITEMS_PER_DAY","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("113","reviews","key","{module:reviews}","_MSN_REVIEWS_KEY","_MS_REVIEWS_KEY","enum","1","{module:reviews}");
INSERT INTO phpn_modules_settings VALUES("114","rooms","search_availability_period","1","_MSN_SEARCH_AVAILABILITY_PERIOD","_MS_SEARCH_AVAILABILITY_PERIOD","enum","1","1,2,3");
INSERT INTO phpn_modules_settings VALUES("115","rooms","search_availability_page_size","20","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","enum","1","1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,250,500,1000");
INSERT INTO phpn_modules_settings VALUES("116","rooms","show_room_types_in_search","all","_MSN_ROOMS_IN_SEARCH","_MS_ROOMS_IN_SEARCH","enum","1","all,available only");
INSERT INTO phpn_modules_settings VALUES("117","rooms","allow_children","yes","_MSN_ALLOW_CHILDREN_IN_ROOM","_MS_ALLOW_CHILDREN_IN_ROOM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("118","rooms","allow_system_suggestion","yes","_MSN_ALLOW_SYSTEM_SUGGESTION","_MS_ALLOW_SYSTEM_SUGGESTION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("119","rooms","allow_extra_beds","yes","_MSN_ALLOW_EXTRA_BEDS","_MS_ALLOW_EXTRA_BEDS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("120","rooms","show_default_prices","yes","_MSN_SHOW_DEFAULT_PRICES","_MS_SHOW_DEFAULT_PRICES","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("121","rooms","allow_default_periods","yes","_MSN_ALLOW_DEFAULT_PERIODS","_MS_ALLOW_DEFAULT_PERIODS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("122","rooms","watermark","no","_MSN_ADD_WATERMARK","_MS_ADD_WATERMARK","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("123","rooms","watermark_text","","_MSN_WATERMARK_TEXT","_MS_WATERMARK_TEXT","string","0","");
INSERT INTO phpn_modules_settings VALUES("124","rooms","max_adults","8","_MSN_MAX_ADULTS_IN_SEARCH","_MS_MAX_ADULTS_IN_SEARCH","enum","1","1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
INSERT INTO phpn_modules_settings VALUES("125","rooms","max_children","3","_MSN_MAX_CHILDREN_IN_SEARCH","_MS_MAX_CHILDREN_IN_SEARCH","enum","1","1,2,3,4,5");
INSERT INTO phpn_modules_settings VALUES("126","rooms","check_partially_overlapping","yes","_MSN_CHECK_PARTIALLY_OVERLAPPING","_MS_CHECK_PARTIALLY_OVERLAPPING","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("127","rooms","show_rooms_occupancy","yes","_MSN_SHOW_ROOMS_OCCUPANCY","_MS_SHOW_ROOMS_OCCUPANCY","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("128","rooms","show_rooms_occupancy_months","6","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","enum","1","3,6,9,12");
INSERT INTO phpn_modules_settings VALUES("129","testimonials","key","{module:testimonials}","_MSN_TESTIMONIALS_KEY","_MS_TESTIMONIALS_KEY","enum","1","{module:testimonials}");



DROP TABLE IF EXISTS phpn_news;

CREATE TABLE `phpn_news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `type` enum('news','events','last_minute') CHARACTER SET latin1 NOT NULL DEFAULT 'news',
  `header_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_text` text COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_news VALUES("1","txj17hkwau","en","news","New version 2.2.6 of uHotelBooking has been released","<p>New version 2.2.6 of uHotelBooking has been released and available now for downloading. There are many improvements and new features added you will like. This version may be installed as a new version or updated, if you have the previous version already installed. All recent changes can be viewed <a href=\"http://www.hotel-booking-script.com/index.php?page=history_of_changes\">here</a>.</p>","2013-11-21 19:39:45","1");
INSERT INTO phpn_news VALUES("2","txj17hkwau","es","news","Nueva versión 2.2.6 de uHotelBooking ha sido puesto en libertad.","<p>Nueva versión 2.2.6 del uHotelBooking ha sido liberado y disponible para su descarga. Hay muchas mejoras y nuevas características añadidas que te guste. Esta versión se puede instalar como una versión nueva o actualizada, si usted tiene la versión anterior already instalado. Todos los cambios recientes se pueden ver <a href=\"http://www.hotel-booking-script.com/index.php?page=history_of_changes\">aquí</a>.</p>","2013-11-21 19:42:12","1");
INSERT INTO phpn_news VALUES("3","txj17hkwau","de","news","Neue Version 2.2.6 von uHotelBooking ist veröffentlicht worden","<p>Neue Version 2.2.6 von uHotelBooking ist erschienen und ab sofort zum Download bereit. Es gibt viele Verbesserungen und neue Funktionen hinzugefügt werden Sie mögen. Diese Version kann als eine neue Version installiert oder aktualisiert werden, wenn Sie über die vorherige Version already installiert. Alle Änderungen können <a href=\"http://www.hotel-booking-script.com/index.php?page=history_of_changes\">hiere</a> eingesehen werden.</p>","2013-11-21 19:42:33","1");



DROP TABLE IF EXISTS phpn_news_subscribed;

CREATE TABLE `phpn_news_subscribed` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `date_subscribed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_packages;

CREATE TABLE `phpn_packages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `package_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `finish_date` date NOT NULL DEFAULT '0000-00-00',
  `minimum_nights` tinyint(1) NOT NULL DEFAULT '0',
  `maximum_nights` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_pages;

CREATE TABLE `phpn_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `content_type` enum('article','link','') CHARACTER SET latin1 NOT NULL DEFAULT 'article',
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_target` enum('','_self','_blank') COLLATE utf8_unicode_ci NOT NULL,
  `page_key` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_text` text COLLATE utf8_unicode_ci,
  `menu_id` int(11) DEFAULT '0',
  `menu_link` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `tag_description` text COLLATE utf8_unicode_ci NOT NULL,
  `comments_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_publishing` date NOT NULL DEFAULT '0000-00-00',
  `is_home` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_removed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_system_page` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `system_page` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `show_in_search` tinyint(1) NOT NULL DEFAULT '1',
  `status_changed` datetime NOT NULL,
  `access_level` enum('public','registered') CHARACTER SET latin1 NOT NULL DEFAULT 'public',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `is_published` (`is_published`),
  KEY `is_removed` (`is_removed`),
  KEY `language_id` (`language_id`),
  KEY `comments_allowed` (`comments_allowed`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_pages VALUES("1","rpo5bahloy","en","article","","_self","uHotelBooking-is-happy-to-welcome-you","uHotelBooking is happy to welcome you!","<p><img class=\"img-indent\" src=\"images/upload/img1.png\" border=\"0\" alt=\"\" align=\"left\" /></p>\n<p>Come alone or bring your family with you, stay here for a night or for weeks, stay here while on business trip or at some kind of conference - either way our hotel is the best possible variant.</p>\n<p>Our site makes it simple to book your next hotel stay. Whether you\'re traveling for business or pleasure, we provide you with some of the best hotel deals around. Discover cheap rates on rooms near the places you like, tucked away in the mountains, in the heart of the city, or scattered in the countryside. With our site, your hotel reservation options are endless.</p>","0","","uHotelBooking","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2016-02-03 17:11:01","0000-00-00","1","0","1","0","","1","2010-04-24 16:55:05","public","0");
INSERT INTO phpn_pages VALUES("2","rpo5bahloy","es","article","","_self","uHotelBooking-se-complace-en-darle-la","uHotelBooking se complace en darle la bienvenida!","<p><img class=\"img-indent\" src=\"images/upload/img1.png\" border=\"0\" alt=\"\" align=\"left\" /></p>\n<p>Venga solo o traer a su familia con usted, quédese aquí por una noche o por semana, estancia aquí en viaje de negocios o en algún tipo de conferencia - en cualquier caso, nuestro hotel es la mejor variante posible.</p>\n<p>ONuestro sitio hace que sea sencillo para reservar su estadía en el hotel. Ya sea que viaje por negocios o por placer, le ofrecemos algunas de las mejores ofertas de hoteles en los alrededores. Descubre tarifas económicas en habitaciones cerca de los lugares que le gusten, escondido en las montañas, en el corazón de la ciudad, o dispersas en el campo. Con nuestro sitio, las opciones de reservas de hotel son infinitas.</p>","0","","uHotelBooking","php sitio del hotel, sitio de reserva de hotel en línea","uHotelBooking","0","2011-05-01 11:16:22","2016-02-03 17:12:48","0000-00-00","1","0","1","0","","1","2010-04-24 16:55:12","public","0");
INSERT INTO phpn_pages VALUES("3","rpo5bahloy","de","article","","_self","uHotelBooking-ist-glücklich-Sie-zu-empfangen","uHotelBooking ist glücklich, Sie zu empfangen!","<p><img class=\"img-indent\" src=\"images/upload/img1.png\" border=\"0\" alt=\"\" align=\"left\" /></p>\n<p>Kommen Sie alleine oder bringen Sie Ihre Familie mit Ihnen, bleiben Sie hier für eine Nacht oder für Wochen, bleiben hier während einer Geschäftsreise oder bei irgendeiner Art von Konferenz - so oder so unser Hotel die beste Variante ist.</p>\n<p>Unsere Website macht es einfach, Ihren nächsten Hotelaufenthalt zu buchen. Ob Sie geschäftlich oder privat unterwegs sind, stellen wir Ihnen einige der besten Hotelangebote um. Entdecken Sie günstige Raten für Zimmer in der Nähe der Orte, die Sie mögen, versteckt in den Bergen entfernt, im Herzen der Stadt, oder in der Landschaft verstreut. Mit unserer Website, sind Ihre Reservierungsmöglichkeiten endlos.</p>","0","","uHotelBooking","php Hotel Website, Hotel online buchen Website","uHotelBooking","0","2011-05-01 11:16:22","2016-02-03 17:13:30","0000-00-00","1","0","1","0","","1","2010-04-24 16:55:05","public","0");
INSERT INTO phpn_pages VALUES("4","99fnhie8in","en","article","","_self","Installation","Installation","<p>Software requirements: PHP 5.0 or later version.</p>\n<p>A new installation of uHotelBooking is a very straight forward process:</p>\n<p><strong>Step 1</strong>. Uncompressing downloaded file.<br />-<br /> Uncompress the uHotelBooking version 4.x.x script archive. The archive will create<br /> a directory called \"uHotelBooking_2xx\"<br /><br /><br /><strong>Step 2</strong>. Uploading files.<br />-<br /> Upload content of this folder (all files and directories it includes) to your <br /> document root (public_html, www, httpdocs etc.) or your booking script directory using FTP.<br /> Pay attention to DON\'T use the capital letters in the name of the folder (for Linux users).</p>\n<p>public_html/<br /> or<br /> public_html/{hotel-site directory}/<br /> <br /> Rename default.htaccess into .htaccess if you need to add PHP5 handler.</p>\n<p><br /><strong>Step 3</strong>. Creating database.<br />-<br /> Using your hosting Control Panel, phpMyAdmin or another tool, create your database<br /> and user, and assign that user to the database. Write down the name of the<br /> database, username, and password for the site installation procedure.</p>\n<p><br /><strong>Step 4</strong>. Running install.php file.<br />-<br /> Now you can run install.php file. To do this, open a browser and type in Address Bar</p>\n<p>http://{www.mydomain.com}/install.php<br /> or<br /> http://{www.mydomain.com}/{hotel-site directory}/install.php</p>\n<p>Follow instructions on the screen. You will be asked to enter: database host,<br /> database name, username and password. Also you need to enter admin username and<br /> admin password, that will be used to get access to administration area of the<br /> site.</p>\n<p><br /><strong>Step 5</strong>. Setting up access permissions.<br />-<br /> Check access permissions to images/uploads/. You need to have 755 permissions <br /> to this folder.</p>\n<p><br /><strong>Step 6</strong>. Deleting install.php file.<br />-<br /> After successful installation you will get an appropriate message and warning to<br /> remove install.php file. For security reasons, please delete install file<br /> immediately.</p>\n<p><br />Congratulations, you now have uHotelBooking installed!</p>","1","Installation","Installation of uHotelBooking","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-21 19:18:35","0000-00-00","0","0","1","0","","1","2012-04-23 20:08:59","public","0");
INSERT INTO phpn_pages VALUES("5","99fnhie8in","es","article","","_self","Instalación","Instalación","<p>Requisitos de software: PHP 5.0 o una versión posterior.</p>\n<p>Una nueva instalación de uHotelBooking es un proceso muy sencillo:</p>\n<p><strong>Paso 1</strong>. Descomprimir el archivo descargado.</p>\n<p>Descomprimir el archivo uHotelBooking versión 4.xx secuencia de comandos. El archivo se creará un directorio llamado \"uHotelBooking_2xx\".<br /><br /><strong>Paso 2</strong>. Carga de archivos.<br /><br />Subir el contenido de esta carpeta (todos los archivos y directorios que incluye) a la raíz del documento (www public_html, httpdocs etc) o el directorio de secuencias de comandos de reserva a través de FTP. Preste atención a NO usar las letras mayúsculas en el nombre de la carpeta (para usuarios de Linux).</p>\n<p>public_html/<br /> or<br /> public_html/{hotel-site directorio}/<br /> <br /> Cambiar el nombre de default.htaccess en. htaccess si es necesario agregar controlador de PHP5.</p>\n<p><br /><strong>Paso 3</strong>. Creación de base de datos.<br /><br />Usando su Panel de Control, phpMyAdmin o cualquier otra herramienta, crear su base de datos y el usuario, y asignar a ese usuario a la base de datos. Anote el nombre de la base de datos, nombre de usuario y una contraseña para el procedimiento de instalación del sitio.</p>\n<p><br />Paso 4. Ejecutar el archivo install.php.<br /><br />Ahora puedes ejecutar el archivo install.php. Para ello, abra un navegador y escriba en la barra de direcciones</p>\n<p>http://{www.mydomain.com}/install.php<br /> or<br /> http://{www.mydomain.com}/{hotel-site directorio}/install.php</p>\n<p>Siga las instrucciones en la pantalla. Se le pedirá que introduzca: base de datos de host, nombre de base de datos, nombre de usuario y contraseña. También es necesario introducir nombre de usuario y contraseña de administrador, que se utilizará para tener acceso al área de administración del sitio.</p>\n<p><br /><strong>Paso 5</strong>. Configuración de permisos de acceso.<br /><br />Compruebe los permisos de acceso a las images/uploads/. Usted necesita tener 755 permisos a esta carpeta.</p>\n<p><br /><strong>Paso 6</strong>. Borrar el archivo install.php.<br /><br />Después de la instalación aparecerá un mensaje apropiado y alerta a eliminar el archivo install.php. Por razones de seguridad, por favor elimine el archivo de instalación de inmediato.</p>\n<p><br />Felicitaciones, ahora tiene uHotelBooking Instalado!</p>","2","Instalación","uHotelBooking","php sitio del hotel, sitio de reserva de hotel en línea","uHotelBooking","0","2011-05-01 11:16:22","2013-11-21 19:18:45","0000-00-00","0","0","1","0","","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("6","99fnhie8in","de","article","","_self","Installation","Installation","<p>Software-Anforderungen: PHP 5.0 oder höher Version.</p>\n<p>Eine neue Installation von uHotelBooking ist ein sehr geradlinig Prozess:</p>\n<p><strong><span style=\"text-decoration: underline;\">Schritt 1</span></strong>. Dekomprimieren heruntergeladene Datei.<br /><br />Entpacken Sie das uHotelBooking Version 4.xx Skript-Archiv. Das Archivwirderstellt ein Verzeichnis namens \"uHotelBooking_2xx\".<br /><br /><br /><strong><span style=\"text-decoration: underline;\">Schritt 2</span></strong>. Hochladen von Dateien.<br /><br />Hochladen Inhalt dieses Ordners (alle Dateien und Verzeichnisse es umfasst), um Ihre Document-Root (public_html, www, httpdocs usw.) oder Ihre Reservierung Skript direkt per FTP. Achten Sie auf Don\'t Gebrauch die Großbuchstaben im Namen des Ordners (für Linux-Anwender).</p>\n<p>public_html/<br /> or<br /> public_html/{hotel-site Verzeichnis}/<br /> <br />Benennen Sie in default.htaccess. htaccess, wenn Sie PHP5 Handler hinzufügen müssen.</p>\n<p><br /><strong>Schritt 3</strong>. Erstellen Datenbank.<br /><br />Mit Ihrem Hosting Control Panel, phpMyAdmin oder ein anderes Tool, erstellen Sie Ihre Datenbank und Benutzer, und weisen Sie die Benutzer auf die Datenbank. Notieren Sie den Namen des Datenbank, den Benutzernamen und das Kennwort für den Einbau vor Ort Verfahren.</p>\n<p><br /><span style=\"text-decoration: underline;\"><strong>Schritt 4</strong></span>. Running install.php Datei..<br /><br />Jetzt können Sie install.php Datei. Dazu öffnen Sie einen Browser und geben Sie Adresse Bar</p>\n<p>http://{www.mydomain.com}/install.php<br /> or<br /> http://{www.mydomain.com}/{hotel-site Verzeichnis}/install.php</p>\n<p>Befolgen Sie die Anweisungen auf dem Bildschirm. Sie werden aufgefordert, einzugeben: Datenbank-Host,<br />Datenbankname, Benutzername und Passwort. Darüber hinaus müssen Sie Benutzernamen admin und    geben Sie Admin-Passwort, mit denen der Zugriff auf Administrationsbereich der erhalten werden Website.</p>\n<p><br /><span style=\"text-decoration: underline;\"><strong>Schritt 5</strong></span>. Einrichten von Zugriffsberechtigungen.<br /><br />Überprüfen Sie die Zugriffsberechtigungen auf images/uploads/. Sie müssen zu 755 Berechtigungen für diesen Ordner haben.</p>\n<p><br /><span style=\"text-decoration: underline;\"><strong>Schritt 6</strong></span>. Löschen install.php.<br /><br />Nach erfolgreicher Installation erhalten Sie eine entsprechende Meldung und Warnung an install.php Datei zu entfernen. Aus Sicherheitsgründen bitte löschen Installationsdatei sofort.</p>\n<p><br />Herzlichen Glückwunsch, Sie haben jetzt uHotelBooking Installiert!</p>","3","Installation","Installation","php Hotel Website, Hotel online buchen Website","uHotelBooking","0","2011-05-01 11:16:22","2013-11-21 19:18:58","0000-00-00","0","0","1","0","","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("7","afd4vgf5yt","en","article","","_self","Gallery","Gallery","<p>{module:gallery}</p>","0","Gallery","Gallery","php hotel site, hotel online booking site","View our video and image galleries","0","2011-05-01 11:16:22","2013-11-06 16:59:27","0000-00-00","0","0","1","1","gallery","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("8","afd4vgf5yt","es","article","","_self","Galería","Galería","<p>{module:gallery}</p>","0","Galería","Galería","php sitio del hotel, sitio de reserva de hotel en línea","Ver nuestra galería de imágenes de vídeo y","0","2011-05-01 11:16:22","2013-11-06 16:59:12","0000-00-00","0","0","1","1","gallery","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("9","afd4vgf5yt","de","article","","_self","Galerie","Galerie","<p>{module:gallery}</p>","0","Galerie","Galerie","php Hotel Website, Hotel online buchen Website","Sehen Sie sich unsere Videos und Bildergalerien","0","2011-05-01 11:16:22","2013-11-06 16:58:58","0000-00-00","0","0","1","1","gallery","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("10","op8uy67ydd","en","article","","_self","Testimonials","Testimonials","<p>{module:testimonials}</p>","0","Testimonials","Testimonials","php hotel site, hotel online booking site","What our clients are saying","0","2011-05-01 11:16:22","2013-11-06 17:01:53","0000-00-00","0","0","1","1","testimonials","1","0000-00-00 00:00:00","public","3");
INSERT INTO phpn_pages VALUES("11","op8uy67ydd","es","article","","_self","Testimonios","Testimonios","<p>{module:testimonials}</p>","0","Testimonios","Testimonios","php sitio del hotel, sitio de reserva de hotel en línea","Lo que nuestros clientes dicen","0","2011-05-01 11:16:22","2013-11-06 17:01:41","0000-00-00","0","0","1","1","testimonials","1","0000-00-00 00:00:00","public","3");
INSERT INTO phpn_pages VALUES("12","op8uy67ydd","de","article","","_self","Zeugnisse","Zeugnisse","<p>{module:testimonials}</p>","0","Zeugnisse","Zeugnisse","php Hotel Website, Hotel online buchen Website","Was unsere Kunden sagen","0","2011-05-01 11:16:22","2013-11-21 19:23:21","0000-00-00","0","0","1","1","testimonials","1","0000-00-00 00:00:00","public","3");
INSERT INTO phpn_pages VALUES("13","87ghtyfd5t","en","article","","_self","We-offer-several-kinds-of-rooms","We offer several kinds of rooms","We offer several kinds of rooms and apartments, in various sizes and  price ranges; single rooms in shared kitchens and studio, couples and  family apartments (1 or 2 bedroom). Many apartments are accessible for  wheelchair users.<br>{module:rooms}","0","Rooms","Rooms","php hotel site, hotel online booking site","Check different rooms in our hotel","0","2011-05-01 11:16:22","2013-11-19 19:06:56","0000-00-00","0","0","1","1","rooms","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("14","87ghtyfd5t","es","article","","_self","Ofrecemos-varios-tipos-de-habitaciones","Ofrecemos varios tipos de habitaciones","Ofrecemos varios tipos de habitaciones y apartamentos, en diversos tamaños y precios; habitaciones individuales en cocinas compartidas y estudio, parejas y apartamentos de la familia (1 o 2 dormitorios). Muchos de los apartamentos son accesibles para usuarios de sillas de ruedas.<br>{module:rooms}","0","Habitaciones","Ofrecemos varios tipos de habitaciones","php sitio del hotel, sitio de reserva de hotel en línea","Compruebe distintas habitaciones de nuestro hotel","0","2011-05-01 11:16:22","2013-11-21 19:21:32","0000-00-00","0","0","1","1","rooms","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("15","87ghtyfd5t","de","article","","_self","Zimmer","Zimmer","Wir bieten verschiedene Arten von Zimmern und Apartments in verschiedenen Größen und Preisklassen ; Einzelzimmer in Gemeinschaftsküchen und Studio, Paare und Familien-Appartements (1 oder 2 Schlafzimmer). Viele Wohnungen sind für Rollstuhlfahrer zugänglich.<br>{module:rooms}","0","Zimmer","Zimmer","php Hotel Website, Hotel online buchen Website","Prüfen Sie verschiedene Zimmer in unserem Hotel","0","2011-05-01 11:16:22","2013-11-21 19:22:25","0000-00-00","0","0","1","1","rooms","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("16","45tfrtbfg8","en","article","","_self","Today’s-featured-menu-item","Today’s featured menu item","<p><img style=\"margin-right: 7px;\" src=\"images/upload/restaurant_dishes.jpg\" border=\"0\" alt=\"\" vspace=\"5\" align=\"left\" /></p>\n<h3 class=\"extra-wrap\">Foie gras!</h3>\n<div class=\"extra-wrap\">\n<ul class=\"list2\">\n<li>Nice and tasty! </li>\n<li>Made from French ingredients! </li>\n<li>Cooked by Italian chef! </li>\n<li>Awarded by world’s assosiation of chef! </li>\n<li>Proved to be good for your health!</li>\n</ul>\n</div>\n<div><strong class=\"txt2\">AS LOW AS €19!</strong></div>\n<p><br /><br /><br /><br /></p>\n<h3><br />Menu/Specials</h3>\n<div class=\"extra-wrap\">\n<ul>\n<li>LYNAGH’S BEER CHEESE <br />Our own recipe, made with Guinness Stout served w/ carrots, celery &amp; crackers. -- $4.99 </li>\n<li>SALSA <br />TAME OR FLAME Homemade salsa served with tortilla chips. The TAME is HOT!!! -- $2.99 </li>\n<li>SPINACH ARTICHOKE DIP <br />Served with tortilla chips. -- $6.49 </li>\n<li>DOC BILL\'S PUB PRETZELS <br />Two jumbo pretzels deep fried and served with hot homemade beer cheese. -- $5.49 </li>\n<li>ULTIMATE IRISH <br />Take the Irish Nacho, add red onions and our famous chili. -- $9.99 </li>\n<li>SPICY QUESO BEEF DIP <br />Ground beef, queso, Mexican spices, jalapenos, and sour cream. That’s gotta be good! -- $6.49 </li>\n<li>DELUXE NACHOS <br />Tortillas smothered with chili, cheese, lettuce tomatoes, jalapenos &amp; sour cream.</li>\n</ul>\n</div>","0","Restaurant","Restaurant","php hotel site, hotel online booking site","Today’s featured menu item","0","2011-05-01 11:16:22","2013-11-12 14:04:55","0000-00-00","0","0","1","1","restaurant","1","0000-00-00 00:00:00","public","2");
INSERT INTO phpn_pages VALUES("17","45tfrtbfg8","es","article","","_self","Elemento-de-menú-aparece-hoy","Elemento de menú aparece hoy","<p><img src=\"images/upload/restaurant_dishes.jpg\" border=\"0\" alt=\"\" hspace=\"5\" vspace=\"5\" align=\"left\" /></p>\n<div class=\"extra-wrap\">\n<h5>Foie gras!</h5>\n<ul class=\"list2\">\n<li>Niza y sabroso! </li>\n<li>Elaborado con ingredientes francés! </li>\n<li>Cocinados por el chef italiano! </li>\n<li>Demuestran ser buenos para tu salud!</li>\n</ul>\n<div class=\"aligncenter\"><strong class=\"txt2\">Más bajo de €19!</strong></div>\n</div>\n<p> </p>\n<h3><br /><br /><br /></h3>\n<h3>Menú / Especiales</h3>\n<div class=\"extra-wrap\">\n<ul>\n<li>LYNAGH’S BEER CHEESE <br />Our own recipe, made with Guinness Stout served w/ carrots, celery &amp; crackers. -- $4.99 </li>\n<li>SALSA <br />TAME OR FLAME Homemade salsa served with tortilla chips. The TAME is HOT!!! -- $2.99 </li>\n<li>SPINACH ARTICHOKE DIP <br />Served with tortilla chips. -- $6.49 </li>\n<li>DOC BILL\'S PUB PRETZELS <br />Two jumbo pretzels deep fried and served with hot homemade beer cheese. -- $5.49 </li>\n<li>ULTIMATE IRISH <br />Take the Irish Nacho, add red onions and our famous chili. -- $9.99 </li>\n<li>SPICY QUESO BEEF DIP <br />Ground beef, queso, Mexican spices, jalapenos, and sour cream. That’s gotta be good! -- $6.49 </li>\n<li>DELUXE NACHOS <br />Tortillas smothered with chili, cheese, lettuce tomatoes, jalapenos &amp; sour cream.</li>\n</ul>\n</div>","0","Restaurante","Restaurante","php sitio del hotel, sitio de reserva de hotel en línea","Elemento de menú aparece hoy","0","2011-05-01 11:16:22","2013-11-12 14:00:54","0000-00-00","0","0","1","1","restaurant","1","0000-00-00 00:00:00","public","2");
INSERT INTO phpn_pages VALUES("18","45tfrtbfg8","de","article","","_self","Restaurant","Restaurant","<p><img style=\"margin-right: 7px;\" src=\"images/upload/restaurant_dishes.jpg\" border=\"0\" alt=\"\" vspace=\"5\" align=\"left\" /></p>\n<h3 class=\"extra-wrap\">Stopfleber!</h3>\n<div class=\"extra-wrap\">\n<ul class=\"list2\">\n<li>Nizza und lecker!<br />Aus Französisch Zutaten!<br />Gekochtes vom italienischen Küchenchef!<br />Ausgezeichnet vom weltweit Hauptverbandes der Chef!<br />Erwies sich als gut für Ihre Gesundheit!</li>\n</ul>\n</div>\n<div><strong class=\"txt2\">so niedrig wie 19 €!</strong></div>\n<p><br /><br /><br /><br /></p>\n<h3>Menü/Specials</h3>\n<div class=\"extra-wrap\">\n<ul>\n<li>Lynagh Bier KÄSE<br />Unser eigenes Rezept, mit Guinness Stout gemacht serviert w/Karotten, Sellerie &amp; Cracker. - $4,99 </li>\n<li>SALSA<br />TAME oder Flamme Hausgemachte Salsa serviert mit Tortilla Chips. Die TAME ist heiß! - $2,99 </li>\n<li>SPINAT ARTISCHOCKE DIP<br />Serviert mit Tortilla Chips. - $6,49 </li>\n<li>DOC BILL\'s Pub PRETZELS<br />Zwei Jumbo-Brezeln gebacken und serviert mit heißer hausgemachtes Bier Käse. - $5,49 </li>\n<li>ULTIMATE IRISH<br />Nehmen Sie die irischen Nacho, roten Zwiebeln hinzufügen und unser berühmtes Chili. - $9.99 </li>\n<li>SPICY QUESO BEEF DIP<br />Hackfleisch, queso, mexikanische Gewürze, Jalapenos und Sauerrahm. Das musst gut sein! - $6,49 </li>\n<li>DELUXE NACHOS<br />Tortillas mit Chili, Käse, Salat Tomaten, Jalapenos &amp; Sauerrahm erstickt.</li>\n</ul>\n</div>","0","Restaurant","Restaurant","php Hotel Website, Hotel online buchen Website","Die heutige vorgestellten Menüeintrag","0","2011-05-01 11:16:22","2013-11-21 19:22:56","0000-00-00","0","0","1","1","restaurant","1","0000-00-00 00:00:00","public","2");
INSERT INTO phpn_pages VALUES("19","s3d4fder56","en","article","","_self","About-Us","About Us","<p>{module:about_us}</p>","0","About Us","About Us","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:02","0000-00-00","0","0","1","1","about_us","1","0000-00-00 00:00:00","public","4");
INSERT INTO phpn_pages VALUES("20","s3d4fder56","es","article","","_self","Quiénes-somos","Quiénes somos","<p>{module:about_us}</p>","0","Quiénes somos","Quiénes somos","php sitio del hotel, sitio de reserva de hotel en línea","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:10","0000-00-00","0","0","1","1","about_us","1","0000-00-00 00:00:00","public","4");
INSERT INTO phpn_pages VALUES("21","s3d4fder56","de","article","","_self","Über-uns","Über uns","<p>{module:about_us}</p>","0","Über uns","Über uns","php Hotel Website, Hotel online buchen Website","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:17","0000-00-00","0","0","1","1","about_us","1","0000-00-00 00:00:00","public","4");
INSERT INTO phpn_pages VALUES("22","90jhtyu78y","en","article","","_self","Terms-and-Conditions","Terms and Conditions","<h4>Conditions of Purchase and Money Back Guarantee</h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in enim sed arcu congue mollis. Mauris sed elementum nulla. Donec eleifend nunc dapibus turpis euismod at commodo mi pulvinar. Praesent vitae metus ligula. Maecenas commodo massa id arcu luctus posuere. Praesent adipiscing scelerisque nisi id accumsan.</p>\n<ul>\n<li>Sed posuere, sem mollis eleifend placerat, nisl magna dapibus nunc, in mattis augue urna ac dui. Nunc mollis venenatis mi. </li>\n<li>A elementum nulla mollis in. Maecenas et mi augue. Nulla euismod mauris sit amet mauris ullamcorper lobortis. </li>\n<li>Vivamus nec ligula nulla. Curabitur non sapien nec lectus euismod consectetur. Morbi ut vestibulum risus. </li>\n</ul>\n<h4><br />Detailed Conditions</h4>\n<p><br />Cras elit purus, dapibus et cursus vel, eleifend interdum neque. Aenean nec magna sit amet felis pellentesque sollicitudin. Praesent ut enim est, quis ornare massa:</p>\n<ul>\n<li>Sed ultrices turpis at dolor dictum eu sollicitudin leo gravida. Praesent leo leo, malesuada nec facilisis non, lobortis eget lacus. </li>\n<li>Donec at orci odio. Aliquam eu nulla felis, eget volutpat enim. Vivamus ullamcorper ligula eu sapien rutrum et hendrerit neque convallis. Sed fringilla tristique arcu, a interdum erat fringilla non. Nunc sit amet sodales leo. </li>\n<li>Quisque luctus lacus nulla. Duis iaculis porttitor velit et feugiat. Nam sed velit libero. Praesent metus mauris, fermentum nec consequat vel, bibendum vel sem. </li>\n</ul>\n<p><br />Etiam auctor est et leo tristique ut scelerisque sapien bibendum. Suspendisse tellus urna, pellentesque eget pellentesque a, dictum in massa.</p>","0","Terms and Conditions","Terms and Conditions","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 19:32:11","0000-00-00","0","0","1","1","terms_and_conditions","1","0000-00-00 00:00:00","public","6");
INSERT INTO phpn_pages VALUES("23","90jhtyu78y","es","article","","_self","Términos-y-Condiciones","Términos y Condiciones","<h4>Conditions of Purchase and Money Back Guarantee</h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in enim sed arcu congue mollis. Mauris sed elementum nulla. Donec eleifend nunc dapibus turpis euismod at commodo mi pulvinar. Praesent vitae metus ligula. Maecenas commodo massa id arcu luctus posuere. Praesent adipiscing scelerisque nisi id accumsan.</p>\n<ul>\n<li>Sed posuere, sem mollis eleifend placerat, nisl magna dapibus nunc, in mattis augue urna ac dui. Nunc mollis venenatis mi. </li>\n<li>A elementum nulla mollis in. Maecenas et mi augue. Nulla euismod mauris sit amet mauris ullamcorper lobortis. </li>\n<li>Vivamus nec ligula nulla. Curabitur non sapien nec lectus euismod consectetur. Morbi ut vestibulum risus. <br /><br /></li>\n</ul>\n<h4>Detailed Conditions<br /><br /></h4>\n<p>Cras elit purus, dapibus et cursus vel, eleifend interdum neque. Aenean nec magna sit amet felis pellentesque sollicitudin. Praesent ut enim est, quis ornare massa:</p>\n<ul>\n<li>Sed ultrices turpis at dolor dictum eu sollicitudin leo gravida. Praesent leo leo, malesuada nec facilisis non, lobortis eget lacus. </li>\n<li>Donec at orci odio. Aliquam eu nulla felis, eget volutpat enim. Vivamus ullamcorper ligula eu sapien rutrum et hendrerit neque convallis. Sed fringilla tristique arcu, a interdum erat fringilla non. Nunc sit amet sodales leo. </li>\n<li>Quisque luctus lacus nulla. Duis iaculis porttitor velit et feugiat. Nam sed velit libero. Praesent metus mauris, fermentum nec consequat vel, bibendum vel sem. </li>\n</ul>\n<p>Etiam auctor est et leo tristique ut scelerisque sapien bibendum. Suspendisse tellus urna, pellentesque eget pellentesque a, dictum in massa.</p>","0","Términos y Condiciones","Términos y Condiciones","php sitio del hotel, sitio de reserva de hotel en línea","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:57","0000-00-00","0","0","1","1","terms_and_conditions","1","0000-00-00 00:00:00","public","6");
INSERT INTO phpn_pages VALUES("24","90jhtyu78y","de","article","","_self","Allgemeine-Geschäftsbedingungen","Allgemeine Geschäftsbedingungen","<h4>Einkaufs-und Geld-Zurück-Garantie</h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in enim sed arcu congue mollis. Mauris sed elementum nulla. Donec eleifend nunc dapibus turpis euismod at commodo mi pulvinar. Praesent vitae metus ligula. Maecenas commodo massa id arcu luctus posuere. Praesent adipiscing scelerisque nisi id accumsan.</p>\n<ul>\n<li>Sed posuere, sem mollis eleifend placerat, nisl magna dapibus nunc, in mattis augue urna ac dui. Nunc mollis venenatis mi. </li>\n<li>A elementum nulla mollis in. Maecenas et mi augue. Nulla euismod mauris sit amet mauris ullamcorper lobortis. </li>\n<li>Vivamus nec ligula nulla. Curabitur non sapien nec lectus euismod consectetur. Morbi ut vestibulum risus. </li>\n</ul>\n<h4><br />Detaillierte Bedingungen<br /><br /></h4>\n<p>Cras elit purus, dapibus et cursus vel, eleifend interdum neque. Aenean nec magna sit amet felis pellentesque sollicitudin. Praesent ut enim est, quis ornare massa:</p>\n<ul>\n<li>Sed ultrices turpis at dolor dictum eu sollicitudin leo gravida. Praesent leo leo, malesuada nec facilisis non, lobortis eget lacus. </li>\n<li>Donec at orci odio. Aliquam eu nulla felis, eget volutpat enim. Vivamus ullamcorper ligula eu sapien rutrum et hendrerit neque convallis. Sed fringilla tristique arcu, a interdum erat fringilla non. Nunc sit amet sodales leo. </li>\n<li>Quisque luctus lacus nulla. Duis iaculis porttitor velit et feugiat. Nam sed velit libero. Praesent metus mauris, fermentum nec consequat vel, bibendum vel sem. </li>\n</ul>\n<p><br />Etiam auctor est et leo tristique ut scelerisque sapien bibendum. Suspendisse tellus urna, pellentesque eget pellentesque a, dictum in massa.</p>","0","Allgemeine Geschäftsbedin","Allgemeine Geschäftsbedingungen","php Hotel Website, Hotel online buchen Website","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:03:04","0000-00-00","0","0","1","1","terms_and_conditions","1","0000-00-00 00:00:00","public","6");
INSERT INTO phpn_pages VALUES("25","zxcs3d4fd5","en","article","","_self","Test-Page","Test Page","<p>Test page with comments</p>","1","Test Page","Test Page","php hotel site, hotel online booking site","uHotelBooking","1","2011-05-01 11:16:22","2013-11-21 19:16:15","0000-00-00","0","0","1","0","","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("26","zxcs3d4fd5","es","article","","_self","Página-de-prueba","Página de prueba","<p>Prueba de la página con comentarios</p>","2","Página de prueba","Página de prueba","php sitio del hotel, sitio de reserva de hotel en línea","uHotelBooking","1","2011-05-01 11:16:22","2013-11-21 19:16:54","0000-00-00","0","0","1","0","","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("27","zxcs3d4fd5","de","article","","_self","Testseite","Testseite","<p>Testseite mit Kommentaren</p>","3","Testseite","Testseite","php Hotel Website, Hotel online buchen Website","uHotelBooking","1","2011-05-01 11:16:22","2013-11-21 19:17:45","0000-00-00","0","0","1","0","","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("28","q8mv7zrzmo","en","article","","_self","Contact-Us","Contact Us","<p>{module:contact_us}</p>","0","Contact Us","Contact Us","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:41","0000-00-00","0","0","1","1","contact_us","1","0000-00-00 00:00:00","public","5");
INSERT INTO phpn_pages VALUES("29","q8mv7zrzmo","es","article","","_self","Contáctenos","Contáctenos","<p>{module:contact_us}</p>","0","Contáctenos","Contáctenos","php sitio del hotel, sitio de reserva de hotel en línea","uHotelBooking","0","2011-05-01 11:16:22","2013-11-21 19:22:07","0000-00-00","0","0","1","1","contact_us","1","0000-00-00 00:00:00","public","5");
INSERT INTO phpn_pages VALUES("30","q8mv7zrzmo","de","article","","_self","Schreiben-Sie-uns","Schreiben Sie uns","<p>{module:contact_us}</p>","0","Kontakt","Schreiben Sie uns","php Hotel Website, Hotel online buchen Website","uHotelBooking","0","2011-05-01 11:16:22","2013-11-21 19:23:52","0000-00-00","0","0","1","1","contact_us","1","0000-00-00 00:00:00","public","5");



DROP TABLE IF EXISTS phpn_privileges;

CREATE TABLE `phpn_privileges` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_privileges VALUES("1","add_menus","Add Menus","Add Menus on the site");
INSERT INTO phpn_privileges VALUES("2","edit_menus","Edit Menus","Edit Menus on the site");
INSERT INTO phpn_privileges VALUES("3","delete_menus","Delete Menus","Delete Menus from the site");
INSERT INTO phpn_privileges VALUES("4","add_pages","Add Pages","Add Pages on the site");
INSERT INTO phpn_privileges VALUES("5","edit_pages","Edit Pages","Edit Pages on the site");
INSERT INTO phpn_privileges VALUES("6","delete_pages","Delete Pages","Delete Pages from the site");
INSERT INTO phpn_privileges VALUES("7","edit_hotel_info","Manage Hotels","See and modify the hotels info");
INSERT INTO phpn_privileges VALUES("8","edit_hotel_rooms","Manage Hotel Rooms","See and modify the hotel rooms info");
INSERT INTO phpn_privileges VALUES("9","view_hotel_reports","See Hotel Reports","See only reports related to assigned hotel");
INSERT INTO phpn_privileges VALUES("10","edit_bookings","Edit Bookings","Edit bookings on the site");
INSERT INTO phpn_privileges VALUES("11","cancel_bookings","Cancel Bookings","Cancel bookings on the site");
INSERT INTO phpn_privileges VALUES("12","delete_bookings","Delete Bookings","Delete bookings from the site");
INSERT INTO phpn_privileges VALUES("13","edit_car_agency_info","Manage Car Agencies","See and modify the car agency info");
INSERT INTO phpn_privileges VALUES("14","edit_car_agency_vehicles","Manage Car Agencies Vehicles","See and modify the car agency vehicles");
INSERT INTO phpn_privileges VALUES("15","edit_car_reservations","Edit Car Reservations","Edit car reservations on the site");
INSERT INTO phpn_privileges VALUES("16","cancel_car_reservations","Cancel Car Reservations","Cancel car reservations on the site");
INSERT INTO phpn_privileges VALUES("17","delete_car_reservations","Delete Car Reservations","Delete car reservations from the site");



DROP TABLE IF EXISTS phpn_ratings_items;

CREATE TABLE `phpn_ratings_items` (
  `item` varchar(200) NOT NULL DEFAULT '',
  `totalrate` int(10) NOT NULL DEFAULT '0',
  `nrrates` int(9) NOT NULL DEFAULT '1',
  PRIMARY KEY (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS phpn_ratings_users;

CREATE TABLE `phpn_ratings_users` (
  `day` int(2) DEFAULT NULL,
  `rater` varchar(15) DEFAULT NULL,
  `item` varchar(200) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS phpn_reviews;

CREATE TABLE `phpn_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `positive_comments` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `negative_comments` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rating_cleanliness` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_room_comfort` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_location` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_service` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_sleep_quality` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_price` float(2,1) NOT NULL DEFAULT '0.0',
  `evaluation` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0 - Not Recommend, 1 - Not Good, 2 - Neutral, 3 - Good, 4 - Very Good, 5 - Wonderful',
  `image_file_1` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_1_thumb` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_2` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_2_thumb` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_3` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_3_thumb` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `priority_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO phpn_reviews VALUES("1","1","1","Great experience","Nice large hotel located on the Red Sea. The breakfast buffet is GREAT! Good location, good beach, good facilities. Staff was more or less friendly, with some exceptions. Nice breakfasts.","Noisy at evenings, especially when lots of people arrive. The hotel facilities are outdated. When the hotel is heavily booked it becomes too crowded everywhere and not nice to be.","2.0","3.0","4.0","4.1","4.2","4.0","4","","","","","","","2013-01-02 12:03:00","1","0");
INSERT INTO phpn_reviews VALUES("2","1","2","I liked it very mutch!","It is close to everything but if you go in the lower season the pool won\'t be ready even though the temperature was quiet high already.","Your front staff was wonderful, i believe her name was Moran, please thank her from us.","4.4","2.3","3.0","5.0","4.0","4.0","5","","","","","","","2013-01-04 13:04:00","1","1");
INSERT INTO phpn_reviews VALUES("3","1","3","Excellent hotel, very good!","The view from our balcony in room # 409, was terrific. It was centrally located to everything on and around the port area. Wonderful service and everything was very clean.","The breakfast was below average, although not bad. If back in this town we would stay there again.","4.0","5.0","4.1","4.3","4.0","3.0","3","","","","","","","2013-01-04 15:04:00","1","2");
INSERT INTO phpn_reviews VALUES("4","1","4","Great experience","Excellent hotel, friendly staff would def go there again. Breakfast is from 7:00 until 12:00, which is Great. Breakfast is really good and worth the cost.","","4.1","3.2","4.0","4.0","5.0","4.0","4","","","","","","","2013-01-05 12:05:00","1","3");
INSERT INTO phpn_reviews VALUES("5","2","1","Super, great experience","Rooms clean tidy and serviced daily. Pool stunning.","Nice location, but the hotel is so old...","3.2","4.2","4.0","2.5","5.0","4.0","3","","","","","","","2013-01-06 16:05:00","1","4");



DROP TABLE IF EXISTS phpn_role_privileges;

CREATE TABLE `phpn_role_privileges` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(5) NOT NULL,
  `privilege_id` int(5) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_role_privileges VALUES("1","1","1","1");
INSERT INTO phpn_role_privileges VALUES("2","1","2","1");
INSERT INTO phpn_role_privileges VALUES("3","1","3","1");
INSERT INTO phpn_role_privileges VALUES("4","1","4","1");
INSERT INTO phpn_role_privileges VALUES("5","1","5","1");
INSERT INTO phpn_role_privileges VALUES("6","1","6","1");
INSERT INTO phpn_role_privileges VALUES("7","1","10","1");
INSERT INTO phpn_role_privileges VALUES("8","1","11","1");
INSERT INTO phpn_role_privileges VALUES("9","1","12","1");
INSERT INTO phpn_role_privileges VALUES("10","1","13","1");
INSERT INTO phpn_role_privileges VALUES("11","1","14","1");
INSERT INTO phpn_role_privileges VALUES("12","1","15","1");
INSERT INTO phpn_role_privileges VALUES("13","1","16","1");
INSERT INTO phpn_role_privileges VALUES("14","1","17","1");
INSERT INTO phpn_role_privileges VALUES("15","2","1","1");
INSERT INTO phpn_role_privileges VALUES("16","2","2","1");
INSERT INTO phpn_role_privileges VALUES("17","2","3","1");
INSERT INTO phpn_role_privileges VALUES("18","2","4","1");
INSERT INTO phpn_role_privileges VALUES("19","2","5","1");
INSERT INTO phpn_role_privileges VALUES("20","2","6","1");
INSERT INTO phpn_role_privileges VALUES("21","2","10","1");
INSERT INTO phpn_role_privileges VALUES("22","2","11","1");
INSERT INTO phpn_role_privileges VALUES("23","2","12","1");
INSERT INTO phpn_role_privileges VALUES("24","2","13","1");
INSERT INTO phpn_role_privileges VALUES("25","2","14","1");
INSERT INTO phpn_role_privileges VALUES("26","2","15","1");
INSERT INTO phpn_role_privileges VALUES("27","2","16","1");
INSERT INTO phpn_role_privileges VALUES("28","2","17","1");
INSERT INTO phpn_role_privileges VALUES("29","3","1","0");
INSERT INTO phpn_role_privileges VALUES("30","3","2","1");
INSERT INTO phpn_role_privileges VALUES("31","3","3","0");
INSERT INTO phpn_role_privileges VALUES("32","3","4","1");
INSERT INTO phpn_role_privileges VALUES("33","3","5","1");
INSERT INTO phpn_role_privileges VALUES("34","3","6","0");
INSERT INTO phpn_role_privileges VALUES("35","3","10","1");
INSERT INTO phpn_role_privileges VALUES("36","3","11","0");
INSERT INTO phpn_role_privileges VALUES("37","3","12","0");
INSERT INTO phpn_role_privileges VALUES("38","3","13","0");
INSERT INTO phpn_role_privileges VALUES("39","3","14","0");
INSERT INTO phpn_role_privileges VALUES("40","3","15","0");
INSERT INTO phpn_role_privileges VALUES("41","3","16","0");
INSERT INTO phpn_role_privileges VALUES("42","3","17","0");
INSERT INTO phpn_role_privileges VALUES("43","4","7","1");
INSERT INTO phpn_role_privileges VALUES("44","4","8","1");
INSERT INTO phpn_role_privileges VALUES("45","4","9","1");
INSERT INTO phpn_role_privileges VALUES("46","4","10","1");
INSERT INTO phpn_role_privileges VALUES("47","4","11","1");
INSERT INTO phpn_role_privileges VALUES("48","4","12","1");
INSERT INTO phpn_role_privileges VALUES("49","5","13","1");
INSERT INTO phpn_role_privileges VALUES("50","5","14","1");
INSERT INTO phpn_role_privileges VALUES("51","5","15","1");
INSERT INTO phpn_role_privileges VALUES("52","5","16","1");
INSERT INTO phpn_role_privileges VALUES("53","5","17","1");



DROP TABLE IF EXISTS phpn_roles;

CREATE TABLE `phpn_roles` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_roles VALUES("1","owner","Site Owner","Site Owner is the owner of the site, has all privileges and could not be removed.");
INSERT INTO phpn_roles VALUES("2","mainadmin","Main Admin","The \"Main Administrator\" user has top privileges like Site Owner and may be removed only by him.");
INSERT INTO phpn_roles VALUES("3","admin","Simple Admin","The \"Simple Admin\" is required to assist the Main Admins, has different privileges and may be created by Site Owner or Main Admins.");
INSERT INTO phpn_roles VALUES("4","hotelowner","Hotel Owner","The \"Hotel Owner\" is the owner of the hotel, has special privileges to the hotels/rooms he/she assigned to.");
INSERT INTO phpn_roles VALUES("5","agencyowner","Car Agency Owner","The \"Car Agency Owner\" is the owner of the car agency, has special privileges to the agency/cars he/she assigned to.");



DROP TABLE IF EXISTS phpn_room_facilities;

CREATE TABLE `phpn_room_facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_room_facilities VALUES("1","1","bar","1");
INSERT INTO phpn_room_facilities VALUES("2","2","microwave","1");
INSERT INTO phpn_room_facilities VALUES("3","3","washing","1");
INSERT INTO phpn_room_facilities VALUES("4","4","pets","1");
INSERT INTO phpn_room_facilities VALUES("5","5","roomservice","1");
INSERT INTO phpn_room_facilities VALUES("6","6","fridge","1");
INSERT INTO phpn_room_facilities VALUES("7","7","tv","1");
INSERT INTO phpn_room_facilities VALUES("8","8","pool","1");
INSERT INTO phpn_room_facilities VALUES("9","9","grill","1");
INSERT INTO phpn_room_facilities VALUES("10","10","garden","1");
INSERT INTO phpn_room_facilities VALUES("11","11","kitchen","1");
INSERT INTO phpn_room_facilities VALUES("12","12","internet","1");
INSERT INTO phpn_room_facilities VALUES("13","13","parking","1");
INSERT INTO phpn_room_facilities VALUES("14","14","childcare","1");
INSERT INTO phpn_room_facilities VALUES("15","15","internet","1");
INSERT INTO phpn_room_facilities VALUES("16","16","hairdryer","1");
INSERT INTO phpn_room_facilities VALUES("17","17","conferenceroom","1");
INSERT INTO phpn_room_facilities VALUES("18","18","air","1");
INSERT INTO phpn_room_facilities VALUES("19","19","playground","1");
INSERT INTO phpn_room_facilities VALUES("21","20","fitness","1");
INSERT INTO phpn_room_facilities VALUES("22","21","breakfast","1");
INSERT INTO phpn_room_facilities VALUES("23","22","spa","1");
INSERT INTO phpn_room_facilities VALUES("24","23","safe","1");
INSERT INTO phpn_room_facilities VALUES("25","24","cafe","1");



DROP TABLE IF EXISTS phpn_room_facilities_description;

CREATE TABLE `phpn_room_facilities_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_facility_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_room_facilities_description VALUES("1","1","en","Bar","");
INSERT INTO phpn_room_facilities_description VALUES("2","1","es","Bar","");
INSERT INTO phpn_room_facilities_description VALUES("3","1","de","Bar","");
INSERT INTO phpn_room_facilities_description VALUES("4","2","en","Microwave","");
INSERT INTO phpn_room_facilities_description VALUES("5","2","es","Microonda","");
INSERT INTO phpn_room_facilities_description VALUES("6","2","de","Mikrowelle","");
INSERT INTO phpn_room_facilities_description VALUES("7","3","es","Lavado","");
INSERT INTO phpn_room_facilities_description VALUES("8","3","en","Washing","");
INSERT INTO phpn_room_facilities_description VALUES("9","3","de","Wasch","");
INSERT INTO phpn_room_facilities_description VALUES("10","4","en","Pets allowed","");
INSERT INTO phpn_room_facilities_description VALUES("11","4","es","Se admiten mascotas","");
INSERT INTO phpn_room_facilities_description VALUES("12","4","de","Haustiere erlaubt","");
INSERT INTO phpn_room_facilities_description VALUES("13","5","en","Room Service","");
INSERT INTO phpn_room_facilities_description VALUES("14","5","es","Servicio de habitaciones","");
INSERT INTO phpn_room_facilities_description VALUES("15","5","de","Zimmerservice","");
INSERT INTO phpn_room_facilities_description VALUES("16","6","en","Fridge","");
INSERT INTO phpn_room_facilities_description VALUES("17","6","es","Nevera","");
INSERT INTO phpn_room_facilities_description VALUES("18","6","de","Kühlschrank","");
INSERT INTO phpn_room_facilities_description VALUES("19","7","en","TV","");
INSERT INTO phpn_room_facilities_description VALUES("20","7","es","TV","");
INSERT INTO phpn_room_facilities_description VALUES("21","7","de","Fernseher","");
INSERT INTO phpn_room_facilities_description VALUES("22","8","en","Pool","");
INSERT INTO phpn_room_facilities_description VALUES("23","8","es","Piscina","");
INSERT INTO phpn_room_facilities_description VALUES("24","8","de","Schwimmbad","");
INSERT INTO phpn_room_facilities_description VALUES("25","9","en","Grill","");
INSERT INTO phpn_room_facilities_description VALUES("26","9","es","Parrilla","");
INSERT INTO phpn_room_facilities_description VALUES("27","9","de","Grill","");
INSERT INTO phpn_room_facilities_description VALUES("28","10","es","Jardín","");
INSERT INTO phpn_room_facilities_description VALUES("29","10","en","Garden","");
INSERT INTO phpn_room_facilities_description VALUES("30","10","de","Garten","");
INSERT INTO phpn_room_facilities_description VALUES("31","11","en","Kitchen","");
INSERT INTO phpn_room_facilities_description VALUES("32","11","es","Cocina","");
INSERT INTO phpn_room_facilities_description VALUES("33","11","de","Küche","");
INSERT INTO phpn_room_facilities_description VALUES("34","12","en","Internet","");
INSERT INTO phpn_room_facilities_description VALUES("35","12","es","Internet","");
INSERT INTO phpn_room_facilities_description VALUES("36","12","de","Internet","");
INSERT INTO phpn_room_facilities_description VALUES("37","13","en","Parking Included","");
INSERT INTO phpn_room_facilities_description VALUES("38","13","es","Parking incluido","");
INSERT INTO phpn_room_facilities_description VALUES("39","13","de","Parkplätze inklusive","");
INSERT INTO phpn_room_facilities_description VALUES("40","14","en","Family/Kid Friendly","");
INSERT INTO phpn_room_facilities_description VALUES("41","14","es","Family/Kid Friendly","");
INSERT INTO phpn_room_facilities_description VALUES("42","14","de","Familien-/Kinderfreundlich","");
INSERT INTO phpn_room_facilities_description VALUES("43","15","en","Wireless Internet","");
INSERT INTO phpn_room_facilities_description VALUES("44","15","es","Internet inalámbrico","");
INSERT INTO phpn_room_facilities_description VALUES("45","15","de","WLAN","");
INSERT INTO phpn_room_facilities_description VALUES("46","16","en","Washer/Dryer","");
INSERT INTO phpn_room_facilities_description VALUES("47","16","es","Lavadora/Secadora","");
INSERT INTO phpn_room_facilities_description VALUES("48","16","de","Waschmaschine/Trockner","");
INSERT INTO phpn_room_facilities_description VALUES("49","17","en","Suitable for Events","");
INSERT INTO phpn_room_facilities_description VALUES("50","17","es","Apto para Eventos","");
INSERT INTO phpn_room_facilities_description VALUES("51","17","de","Geeignet für Events","");
INSERT INTO phpn_room_facilities_description VALUES("52","18","en","Air Conditioning","");
INSERT INTO phpn_room_facilities_description VALUES("53","18","es","Aire acondicionado","");
INSERT INTO phpn_room_facilities_description VALUES("54","18","de","Klimaanlage","");
INSERT INTO phpn_room_facilities_description VALUES("55","19","en","Playground","");
INSERT INTO phpn_room_facilities_description VALUES("56","19","es","Patio de recreo","");
INSERT INTO phpn_room_facilities_description VALUES("57","19","de","Spielplatz","");
INSERT INTO phpn_room_facilities_description VALUES("63","21","de","Fitness","");
INSERT INTO phpn_room_facilities_description VALUES("62","21","es","Aptitud","");
INSERT INTO phpn_room_facilities_description VALUES("61","21","en","Fitness","");
INSERT INTO phpn_room_facilities_description VALUES("64","22","en","Breakfast","");
INSERT INTO phpn_room_facilities_description VALUES("65","22","es","Desayuno","");
INSERT INTO phpn_room_facilities_description VALUES("66","22","de","Frühstück","");
INSERT INTO phpn_room_facilities_description VALUES("67","23","en","SPA","");
INSERT INTO phpn_room_facilities_description VALUES("68","23","es","SPA","");
INSERT INTO phpn_room_facilities_description VALUES("69","23","de","SPA","");
INSERT INTO phpn_room_facilities_description VALUES("70","24","en","Safe","");
INSERT INTO phpn_room_facilities_description VALUES("71","24","es","Seguro","");
INSERT INTO phpn_room_facilities_description VALUES("72","24","de","Sicher","");
INSERT INTO phpn_room_facilities_description VALUES("73","25","en","Cafe","");
INSERT INTO phpn_room_facilities_description VALUES("74","25","es","Café","");
INSERT INTO phpn_room_facilities_description VALUES("75","25","de","Café","");



DROP TABLE IF EXISTS phpn_rooms;

CREATE TABLE `phpn_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `room_type` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `room_short_description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `room_long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `room_count` smallint(6) NOT NULL,
  `max_adults` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `max_extra_beds` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `max_children` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `extra_bed_charge` decimal(10,2) unsigned NOT NULL,
  `discount_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - fixed price, 1 - percentage',
  `discount_night_3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_night_4` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_night_5` decimal(10,2) NOT NULL DEFAULT '0.00',
  `default_availability` tinyint(1) NOT NULL DEFAULT '1',
  `beds` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `bathrooms` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `room_area` decimal(4,1) unsigned NOT NULL DEFAULT '0.0',
  `facilities` varchar(400) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_icon_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_1` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_1_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_2` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_2_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_3` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_3_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_4` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_4_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_5` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_5_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms VALUES("1","1","Single","","Single","15","1","0","0","55.00","22.00","0","0.00","0.00","0.00","1","1","1","20.0","a:7:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"7\";i:3;s:1:\"8\";i:4;s:1:\"9\";i:5;s:2:\"11\";i:6;s:2:\"15\";}","single_icon.png","single_icon_thumb.jpg","single_1.jpg","single_1_thumb.jpg","single_2.jpg","single_2_thumb.jpg","single_3.jpg","single_3_thumb.jpg","single_4.jpg","single_4_thumb.jpg","single_5.jpg","single_5_thumb.jpg","1","1");
INSERT INTO phpn_rooms VALUES("2","1","Double","","Double","10","2","1","1","80.00","30.00","0","0.00","0.00","0.00","1","2","2","25.0","a:8:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"7\";i:3;s:1:\"8\";i:4;s:2:\"10\";i:5;s:2:\"13\";i:6;s:2:\"16\";i:7;s:2:\"17\";}","double_icon.png","double_icon_thumb.jpg","double_1.jpg","double_1_thumb.jpg","double_2.jpg","double_2_thumb.jpg","double_3.jpg","double_3_thumb.jpg","","","","","2","1");
INSERT INTO phpn_rooms VALUES("3","1","Superior","","Superior","5","3","1","1","140.00","50.00","0","0.00","0.00","0.00","1","2","1","35.0","a:11:{i:0;s:1:\"4\";i:1;s:1:\"5\";i:2;s:1:\"7\";i:3;s:1:\"8\";i:4;s:1:\"9\";i:5;s:2:\"12\";i:6;s:2:\"15\";i:7;s:2:\"16\";i:8;s:2:\"19\";i:9;s:2:\"22\";i:10;s:2:\"24\";}","superior_icon.png","superior_icon_thumb.jpg","superior_1.jpg","superior_1_thumb.jpg","superior_2.jpg","superior_2_thumb.jpg","superior_3.jpg","superior_3_thumb.jpg","","","","","3","1");
INSERT INTO phpn_rooms VALUES("4","1","Luxury","","Luxury","3","4","2","2","190.00","80.00","0","0.00","0.00","0.00","1","2","2","60.0","a:12:{i:0;s:1:\"4\";i:1;s:1:\"7\";i:2;s:1:\"9\";i:3;s:2:\"10\";i:4;s:2:\"11\";i:5;s:2:\"12\";i:6;s:2:\"13\";i:7;s:2:\"15\";i:8;s:2:\"16\";i:9;s:2:\"17\";i:10;s:2:\"18\";i:11;s:2:\"19\";}","luxury_icon.png","luxury_icon_thumb.jpg","luxury_1.jpg","luxury_1_thumb.jpg","luxury_2.jpg","luxury_2_thumb.jpg","luxury_3.jpg","luxury_3_thumb.jpg","","","","","4","1");
INSERT INTO phpn_rooms VALUES("5","2","Standard Room","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest.</p>","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest, flat screen TV, mini-fridge, executive desk, wireless internet, all tastefully decorated with climate control at your fingertips. Exercise in our state of the art fitness facility, swim in our heated indoor swimming pool or relax in our sauna, all free of cost for our guests.</p>","12","2","1","0","78.00","50.00","0","0.00","0.00","0.00","1","1","1","44.0","a:9:{i:0;s:1:\"4\";i:1;s:2:\"11\";i:2;s:2:\"12\";i:3;s:2:\"13\";i:4;s:2:\"14\";i:5;s:2:\"15\";i:6;s:2:\"16\";i:7;s:2:\"17\";i:8;s:2:\"18\";}","2_icon_myotb7mfgwxraknnm9t5.jpg","2_icon_myotb7mfgwxraknnm9t5_thumb.jpg","2_view1_kf99txs4x0o848fuggnm.jpg","2_view1_kf99txs4x0o848fuggnm_thumb.jpg","2_view2_eksssaik1p04kxo6ktyb.jpg","2_view2_eksssaik1p04kxo6ktyb_thumb.jpg","","","","","","","0","1");
INSERT INTO phpn_rooms VALUES("6","2","Double Room with Two Double Beds","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests.</p>","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests. The standard double room is ideal for a brief stay of up to a few days. It doesn\'t contain a kitchen, so guests can\'t cook their own meals.</p>","10","4","1","1","119.00","80.00","0","0.00","0.00","0.00","1","2","2","90.0","a:11:{i:0;s:1:\"2\";i:1;s:1:\"4\";i:2;s:1:\"5\";i:3;s:1:\"9\";i:4;s:2:\"12\";i:5;s:2:\"15\";i:6;s:2:\"18\";i:7;s:2:\"19\";i:8;s:2:\"21\";i:9;s:2:\"23\";i:10;s:2:\"24\";}","2_icon_n7bisjedxa1pntimfdnq.jpg","2_icon_n7bisjedxa1pntimfdnq_thumb.jpg","2_view1_dlcv6g4vvq5exgwp1b4x.jpg","2_view1_dlcv6g4vvq5exgwp1b4x_thumb.jpg","2_view2_upvdxolmdb8edlp8wl7b.jpg","2_view2_upvdxolmdb8edlp8wl7b_thumb.jpg","","","","","","","1","1");
INSERT INTO phpn_rooms VALUES("7","3","Junior Suite","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.</p>","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.<br /> For your comfort and convenience, most also include a fully equipped European-style kitchen with appliances by Sub-Zero, and a sitting area with a twin-size sofa bed. All of our New York hotel guests enjoy full access to our health club and spa.</p>","5","1","1","0","75.00","45.00","0","0.00","0.00","0.00","1","1","1","99.0","a:11:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"5\";i:3;s:1:\"6\";i:4;s:1:\"7\";i:5;s:1:\"8\";i:6;s:1:\"9\";i:7;s:2:\"10\";i:8;s:2:\"11\";i:9;s:2:\"12\";i:10;s:2:\"14\";}","3_icon_ddgadcswt83f03qun4px.jpg","3_icon_ddgadcswt83f03qun4px_thumb.jpg","3_view1_sdkpabusey0lcjr3vk9d.jpg","3_view1_sdkpabusey0lcjr3vk9d_thumb.jpg","3_view2_hq0t4mk73y1rblglwbea.jpg","3_view2_hq0t4mk73y1rblglwbea_thumb.jpg","3_view3_xrc4yfscghg6n1y5f652.jpg","3_view3_xrc4yfscghg6n1y5f652_thumb.jpg","","","","","0","1");
INSERT INTO phpn_rooms VALUES("8","3","Executive Double Room","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of London. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of London. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>\n<p> </p>\n<p>As you would expect from Guoman, all our 50 Executive Double bedrooms also benefit from fast, complimentary BT Wi-fi and entertainment at your fingertips with a 32-inch LCD TV. You’ll appreciate extra space to unwind and recharge for the day ahead with beautifully designed bathrooms featuring walk-in power showers and mist-free mirrors. Welcome details that provide an oasis of calm in the centre of London.</p>","12","3","1","1","109.00","50.00","0","0.00","0.00","0.00","1","2","2","112.0","a:9:{i:0;s:1:\"1\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"8\";i:4;s:1:\"9\";i:5;s:2:\"10\";i:6;s:2:\"13\";i:7;s:2:\"15\";i:8;s:2:\"17\";}","3_icon_du43vsrg9nv5slkhgpbk.jpg","3_icon_du43vsrg9nv5slkhgpbk_thumb.jpg","3_view1_sdkpabusey0lcjr3vk9d.jpg","3_view1_sdkpabusey0lcjr3vk9d_thumb.jpg","3_view2_hq0t4mk73y1rblglwbea.jpg","3_view2_hq0t4mk73y1rblglwbea_thumb.jpg","","","","","","","2","1");
INSERT INTO phpn_rooms VALUES("9","4","Single Bed","<p>TEST</p>","<p>TEST</p>","1","2","2","2","0.00","0.00","0","0.00","0.00","0.00","1","6","1","0.0","a:5:{i:0;s:1:\"2\";i:1;s:1:\"7\";i:2;s:2:\"11\";i:3;s:2:\"12\";i:4;s:2:\"15\";}","4_icon_ajh71v9s8wnmuusoqhl7.jpg","4_icon_ajh71v9s8wnmuusoqhl7_thumb.jpg","","","4_view2_bo7wiyk0g7gl8qik0zfz.jpg","4_view2_bo7wiyk0g7gl8qik0zfz_thumb.jpg","","","4_view4_o2wrsec2er226sw1g17a.jpg","4_view4_o2wrsec2er226sw1g17a_thumb.jpg","","","0","1");



DROP TABLE IF EXISTS phpn_rooms_availabilities;

CREATE TABLE `phpn_rooms_availabilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned NOT NULL DEFAULT '0',
  `y` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - current year, 1 - next year',
  `m` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `d1` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d2` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d3` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d4` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d5` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d6` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d7` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d8` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d9` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d10` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d11` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d12` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d13` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d14` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d15` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d16` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d17` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d18` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d19` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d20` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d21` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d22` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d23` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d24` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d25` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d26` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d27` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d28` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d29` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d30` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d31` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `y` (`y`),
  KEY `m` (`m`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms_availabilities VALUES("1","1","0","1","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("2","1","0","2","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("3","1","0","3","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("4","1","0","4","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("5","1","0","5","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("6","1","0","6","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("7","1","0","7","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("8","1","0","8","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("9","1","0","9","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("10","1","0","10","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("11","1","0","11","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("12","1","0","12","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("13","1","1","1","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("14","1","1","2","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("15","1","1","3","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("16","1","1","4","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("17","1","1","5","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("18","1","1","6","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("19","1","1","7","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("20","1","1","8","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("21","1","1","9","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("22","1","1","10","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("23","1","1","11","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("24","1","1","12","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("25","2","0","1","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("26","2","0","2","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("27","2","0","3","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("28","2","0","4","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("29","2","0","5","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("30","2","0","6","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("31","2","0","7","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("32","2","0","8","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","2","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("33","2","0","9","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("34","2","0","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("35","2","0","11","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("36","2","0","12","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("37","2","1","1","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("38","2","1","2","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("39","2","1","3","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("40","2","1","4","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("41","2","1","5","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("42","2","1","6","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("43","2","1","7","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("44","2","1","8","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("45","2","1","9","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("46","2","1","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("47","2","1","11","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("48","2","1","12","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("49","3","0","1","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("50","3","0","2","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("51","3","0","3","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("52","3","0","4","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("53","3","0","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("54","3","0","6","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("55","3","0","7","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("56","3","0","8","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("57","3","0","9","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("58","3","0","10","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("59","3","0","11","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("60","3","0","12","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("61","3","1","1","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("62","3","1","2","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("63","3","1","3","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("64","3","1","4","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("65","3","1","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("66","3","1","6","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("67","3","1","7","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("68","3","1","8","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("69","3","1","9","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("70","3","1","10","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("71","3","1","11","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("72","3","1","12","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("73","4","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("74","4","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("75","4","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("76","4","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("77","4","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("78","4","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("79","4","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("80","4","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("81","4","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("82","4","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("83","4","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("84","4","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("85","4","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("86","4","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("87","4","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("88","4","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("89","4","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("90","4","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("91","4","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("92","4","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("93","4","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("94","4","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("95","4","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("96","4","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("97","5","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("98","5","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("99","5","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("100","5","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("101","5","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("102","5","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("103","5","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("104","5","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("105","5","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("106","5","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("107","5","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("108","5","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("109","5","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("110","5","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("111","5","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("112","5","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("113","5","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("114","5","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("115","5","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("116","5","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("117","5","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("118","5","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("119","5","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("120","5","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("121","6","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("122","6","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("123","6","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("124","6","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("125","6","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("126","6","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("127","6","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("128","6","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("129","6","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("130","6","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("131","6","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("132","6","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("133","6","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("134","6","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("135","6","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("136","6","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("137","6","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("138","6","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("139","6","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("140","6","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("141","6","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("142","6","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("143","6","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("144","6","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("145","7","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("146","7","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("147","7","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("148","7","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("149","7","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("150","7","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("151","7","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("152","7","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("153","7","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("154","7","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("155","7","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("156","7","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("157","7","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("158","7","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("159","7","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("160","7","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("161","7","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("162","7","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("163","7","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("164","7","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("165","7","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("166","7","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("167","7","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("168","7","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("169","8","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("170","8","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("171","8","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("172","8","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("173","8","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("174","8","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("175","8","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("176","8","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("177","8","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("178","8","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("179","8","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("180","8","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("181","8","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("182","8","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("183","8","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("184","8","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("185","8","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("186","8","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("187","8","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("188","8","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("189","8","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("190","8","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("191","8","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("192","8","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("193","9","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("194","9","0","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("195","9","0","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("196","9","0","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("197","9","0","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("198","9","0","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("199","9","0","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("200","9","0","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("201","9","0","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("202","9","0","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("203","9","0","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("204","9","0","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("205","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("206","9","1","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("207","9","1","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("208","9","1","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("209","9","1","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("210","9","1","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("211","9","1","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("212","9","1","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("213","9","1","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("214","9","1","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("215","9","1","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("216","9","1","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");



DROP TABLE IF EXISTS phpn_rooms_description;

CREATE TABLE `phpn_rooms_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `room_id` int(10) NOT NULL DEFAULT '0',
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `room_type` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `room_short_description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `room_long_description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms_description VALUES("1","1","en","Single","<p>Rooms measuring 15 m² equipped with all the details expected of a superior 4 star hotel. Services: Wake up call service, Customer service, Laundry service and express laundry, Concierge service, Pillow menu.</p>","<p><strong>Description:</strong><br />Rooms measuring 15 m² equipped with all the details expected of a superior 4 star hotel.</p>\n<p><strong>Services: </strong>\n<ul class=\"services\">\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("2","1","es","Single","<p>Habitaciones de unos 15 m², equipada con todos los detalles de un 4 estrellas superior Hotel. Servicios: Servicio despertador servicio, Servicio al cliente, Servicio de lavandería y lavandería express.</p>","<p><strong>Descripción:</strong><br /> Habitaciones de unos 15 m², equipada con todos los detalles de un 4 estrellas superior Hotel.</p>\n<p><strong>Servicios:</strong><ul class=\"services\">\n<li>Servicio despertador servicio</li>\n<li>Servicio al cliente</li>\n<li>Servicio de lavandería y lavandería express</li>\n<li>Conserjería de servicios</li>\n<li>Carta de almohadas</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("3","1","de","Single","<p>Zimmer 15 m² Messung mit allen Details eines 4-Sterne-Superior-Hotel erwartet. Dienstleistungen: Weckdienst, Customer Service, Wäsche-Service und Express-Wäsche-, Concierge-Service, Auswahl an Kissen.</p>","<p><strong>Beschreibung:</strong><br /> Zimmer 15 m² Messung mit allen Details eines 4-Sterne-Superior-Hotel erwartet.</p>\n<p><strong>Dienstleistungen:</strong>\n<ul class=\"services\">\n<li>Weckdienst</li>\n<li>Kundendienst</li>\n<li>Wäscherei-Service und Express-Wäsche</li>\n<li>Conciergeservice Service</li>\n<li>Kissenmenü</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("4","2","en","Double","<p>Modern and functional rooms measuring approximately 20-25 m² equipped with all the details expected of the hotel. The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres ideal for sports teams.</p>","<p><strong>Description</strong>:<br />Modern and functional rooms measuring approximately 20-25 m² equipped with all the details expected of the hotel. <br /><br />The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 meters ideal for sports teams and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).<br /><br /><strong>Services</strong>:\n<ul class=\"services\">\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("5","2","es","Double","<p>Habitaciones modernas y funcionales de aproximadamente 20-25 m&sup2; equipadas con todos los detalles del hotel. Las habitaciones tienen una cama king o queen size o dos camas individuales, adem&aacute;s de camas de 1 por 2,2 metros ideal para equipos deportivos.</p>","<p><strong>Descripción </strong>:<br />\nHabitaciones modernas y funcionales de aproximadamente 20-25 m² equipadas con todos los detalles del hotel.\n<br /><br />\nLas habitaciones tienen una cama king o queen size o dos camas individuales, además de camas de 1 por 2,2 metros ideal para equipos deportivos y puntos de vista de las calles de los patios interiores tranquila (Solicitud de disponibilidad a la llegada al hotel).\n<br /><br />\n<strong>Servicios</strong>:\n<ul class=\"services\">\n<li>Servicio despertador servicio</li>\n<li>Servicio al cliente</li>\n<li>Servicio de lavandería y lavandería express</li>\n<li>Conserjería de servicios</li>\n<li>Carta de almohadas</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("6","2","de","Double","<p>Moderne und funktionale Zimmer Messung ca. 20-25 m&sup2; mit allen Details des Hotel erwartet. Die Zimmer verf&uuml;gen &uuml;ber ein King oder Queen-Size-Bett oder zwei Einzelbetten, zus&auml;tzlich zu Betten Messung ideal f&uuml;r Sport-Teams.</p>","<p><strong>Beschreibung</strong>:<br />\nModerne und funktionale Zimmer Messung ca. 20-25 m² mit allen Details des Hotel erwartet.\n<br /><br />\nDie Zimmer verfügen über ein King oder Queen-Size-Bett oder zwei Einzelbetten, zusätzlich zu Betten der Größe von 1 um 2,2 Meter ideal für Sport-Teams und Blick auf den Straßen der ruhigen Innenhöfen (Anfrage Verfügbarkeit bei Ankunft im Hotel).\n<br /><br />\n<strong>Dienstleistungen</strong>:\n<ul class=\"services\">\n<li>Weckdienst</li>\n<li>Kundendienst</li>\n<li>Wäscherei-Service und Express-Wäsche</li>\n<li>Conciergeservice Service</li>\n<li>Kissenmenü</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("7","3","en","Superior","<p>Spacious rooms with exquisite decor, measuring approximately 25-30 m&sup2; and equipped with all the details expected of the hotel hotel. The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres.</p>","<p><strong>Description</strong>:\n<br />\nSpacious rooms with exquisite decor, measuring approximately 25-30 m² and equipped with all the details expected of the hotel hotel.\n<br /><br />\nThe rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres ideal for sports teams and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).\n<br /><br />\n<strong>Services</strong>:\n<ul class=\"services\">\n<li>24 hour room service</li>\n<li>Wake up call service</li>\n<li>Serviexpress customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul>\n</p>");
INSERT INTO phpn_rooms_description VALUES("8","3","es","Superior","<p>Amplias habitaciones con una decoraci&oacute;n exquisita, de 25-30 m&sup2; equipadas con todos los detalles del hotel hotel. Las habitaciones tienen una cama king o queen size o dos camas individuales, adem&aacute;s de camas de 1 por 2,2 metros.</p>","<p><strong>Descripción</strong>:\n<br />\nAmplias habitaciones con una decoración exquisita, de 25-30 m² equipadas con todos los detalles del hotel hotel.\n<br /><br />\nLas habitaciones tienen una cama king o queen size o dos camas individuales, además de camas de 1 por 2,2 metros ideal para equipos deportivos y puntos de vista de las calles de los patios interiores tranquila (Solicitud de disponibilidad a la llegada al hotel).\n<br /><br />\n<strong>Servicios</strong>:\n<ul class=\"services\">\n<li>24 horas de servicio sala de</li>\n<li>Servicio despertador servicio</li>\n<li>\"Serviexpress\" de servicio al cliente</li>\n<li>Servicio de lavandería y lavandería express</li>\n<li>Conserjería de servicios</li>\n<li>Carta de almohadas</li>\n</ul>\n</p>");
INSERT INTO phpn_rooms_description VALUES("9","3","de","Superior","<p>Spacious rooms with exquisite decor, measuring approximately 25-30 m&sup2; and equipped with all the details expected of the hotel hotel. The rooms have a king or queen size bed or two single beds.</p>","<p><strong>Beschreibung</strong>:\n<br />\nGeräumige Zimmer mit exquisiter Ausstattung, Mess ca. 25-30 m² und mit allen Details des Hotels Hotel erwartet.\n<br /><br />\nDie Zimmer verfügen über ein King oder Queen-Size-Bett oder zwei Einzelbetten, zusätzlich zu Betten der Größe von 1 um 2,2 Meter ideal für Sport-Teams und Blick auf den Straßen der ruhigen Innenhöfen (Anfrage Verfügbarkeit bei Ankunft im Hotel).\n<br /><br />\n<strong>Dienstleistungen</strong>:\n<ul class=\"services\">\n<li>24 Stunden Zimmer-Service</li>\n<li>Weckdienst</li>\n<li>\"Serviexpress\" Dienst am Kunden</li>\n<li>Wäscherei-Service und Express-Wäsche</li>\n<li>Conciergeservice Service</li>\n<li>Kissenmenü</li>\n</ul>\n</p>");
INSERT INTO phpn_rooms_description VALUES("10","4","en","Luxury","<p>Spacious rooms with exquisite decor measuring approximately 25-30 m&sup2; and equipped with all the details expected of a superior 4 star Hotel. The rooms have a king or queen size bed or two single beds, and views of the streets.</p>","<p><strong>Description</strong>:\n<br />\nSpacious rooms with exquisite decor measuring approximately 25-30 m² and equipped with all the details expected of a superior 4 star Hotel.\n<br /><br />\nThe rooms have a king or queen size bed or two single beds, and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).\n<br /><br />\n<strong>Services</strong>:\n<br />\n<ul class=\"services\">\n<li>Private reception located on the 6th floor</li>\n<li>Welcome courtesy replaced daily</li>\n<li>Buffet breakfast in private room between 7am and 11am</li>\n<li>Free Open Bar available on the Luxury Service floor, from 16:30 to 22:30</li>\n<li>24 hour room service</li>\n<li>10% discount on lunches and dinners at our Diábolo restaurant, after consultation and booking at the Royal Service reception and subject to availability</li>\n<li>Free 30 minute use per stay of computers in the Business Centre (hall), including printer and 25 photocopies</li>\n<li>Free 8 minute Ultra Violet Ray session per stay</li>\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n<li>Turn down service</li>\n</ul>\n* The Luxury Service is closed from Friday at 12:00 until Sunday at 15:00.\n</p>");
INSERT INTO phpn_rooms_description VALUES("11","4","es","Luxury","<p>Amplias habitaciones con una decoraci&oacute;n exquisita de 25-30 m&sup2; y est&aacute;n equipadas con todos los detalles de un 4 estrellas superior Hotel. Las habitaciones tienen una cama king o queen size o dos camas individuales.</p>","<p><strong>Descripción</strong>:\n<br />\nAmplias habitaciones con una decoración exquisita de 25-30 m² y están equipadas con todos los detalles de un 4 estrellas superior Hotel.\n<br /><br />\nLas habitaciones tienen una cama king o queen size o dos camas individuales, y vistas de las calles de los patios interiores tranquila (Solicitud de disponibilidad a la llegada al hotel).\n<br /><br />\n<strong>Servicios</strong>:\n<br />\n<ul class=\"services\">\n<li>Recepción privada situada en la planta sexta</li>\n<li>Bienvenido cortesía reposición diaria</li>\n<li>Desayuno Buffet en habitación privada 07 a.m.-11 a.m.</li>\n<li>Libre Open Bar se encuentra en la planta noble de servicio, 16:30-22:30</li>\n<li>24 horas de servicio sala de</li>\n<li>10% de descuento en almuerzos y cenas en nuestro restaurante Diábolo, previa consulta y reserva en la recepción de Servicio Real y sujeto a disponibilidad</li>\n<li>Libre uso de 30 minutos por estancia de los ordenadores en el Centro de Negocios (vestíbulo), incluyendo impresoras y fotocopias de los 25</li>\n<li>Libre 8 minutos Ultra Violeta Ray período de sesiones por estancia</li>\n<li>Servicio despertador servicio</li>\n<li>Servicio al cliente</li>\n<li>Servicio de lavandería y lavandería express</li>\n<li>Conserjería de servicios</li>\n<li>Carta de almohadas</li>\n<li>Ponga Servicio de cobertura</li>\n</ul>\n* El servicio de lujo está cerrado desde el viernes hasta el domingo a las 12:00 a las 15:00.\n</p>");
INSERT INTO phpn_rooms_description VALUES("12","4","de","Luxury","<p>Ger&auml;umige Zimmer mit exquisiter Ausstattung messung ca. 25-30 m&sup2; gro&szlig; und mit Allen Details eines 4-Sterne-Superior-Hotel expected. Die Zimmer verf&uuml;gen &Uuml;ber Ein K&ouml;nig Oder Queen-Size-Bett und Blick Auf Die Strassen.</p>","<p><strong>Beschreibung</strong>:\n<br />\nGeräumige Zimmer mit exquisiter Ausstattung Messung ca. 25-30 m² groß und mit allen Details eines 4-Sterne-Superior-Hotel Meliá Hotel erwartet.\n<br /><br />\nDie Zimmer verfügen über ein King oder Queen-Size-Bett oder zwei Einzelbetten und Blick auf den Straßen der ruhigen Innenhöfen (Anfrage Verfügbarkeit bei Ankunft im Hotel).\n<br /><br />\n<strong>Dienstleistungen</strong>:\n<br />\n<ul class=\"services\">\n<li>Private Empfang auf der 6. Etage Willkommen Höflichkeit ersetzt täglich</li>\n<li>Buffet Frühstück im privaten Raum 7.00 bis 11.00 Uhr</li>\n<li>Kostenlose Open Bar auf dem Luxus-Service Stock, von 16.30 bis 22.30 Uhr</li>\n<li>24 Stunden Zimmer-Service</li>\n<li>10% Rabatt auf Mittag-und Abendessen in unserem Restaurant Diabolo, nach Absprache und Reservierung im Royal Service Rezeption und je nach Verfügbarkeit</li>\n<li>Kostenlose 30 Minuten Nutzung pro Aufenthalt von Computern im Business Center (Halle), einschließlich Drucker und 25 Kopien</li>\n<li>Freie 8 Minuten Ultra Violet Ray-Sitzung pro Aufenthalt</li>\n<li>Weckdienst</li>\n<li>Kundendienst</li>\n<li>Wäscherei-Service und Express-Wäsche</li>\n<li>Conciergeservice Service</li>\n<li>Kissenmenü</li>\n<li>Schalten Down-Service</li>\n</ul>\n* El servicio de lujo está cerrado desde el viernes hasta el domingo a las 12:00 a las 15:00.\n</p>");
INSERT INTO phpn_rooms_description VALUES("13","5","en","Standard Room","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest.</p>","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest, flat screen TV, mini-fridge, executive desk, wireless internet, all tastefully decorated with climate control at your fingertips. Exercise in our state of the art fitness facility, swim in our heated indoor swimming pool or relax in our sauna, all free of cost for our guests.</p>");
INSERT INTO phpn_rooms_description VALUES("14","5","es","Habitación Estándar","<p>Relájese en nuestra habitación estándar, con dos camas queen size, muebles a medida, colchones con acolchado de gel con infusión cubierto con edredones de lujo y fundas nórdicas limpia para todos los huéspedes.</p>","<p>Relájese en nuestra habitación estándar, con dos camas queen size, muebles a medida, colchones con acolchado de gel con infusión cubierto con edredones de lujo y fundas nórdicas limpia para todos los huéspedes, TV pantalla plana, mini-nevera, escritorio ejecutivo, internet inalámbrico, todo con buen gusto decorado con climatizador a su alcance. Ejercicio en nuestro estado del arte gimnasio, nadar en la piscina cubierta climatizada o relajarse en la sauna, todo sin costo para nuestros huéspedes.</p>");
INSERT INTO phpn_rooms_description VALUES("15","5","de","Standard Zimmer","<p>Entspannen Sie in unserem Standard-Zimmer, komplett mit zwei Queen-Size-Betten, individuelle Möbel, gekrönt Gel-infundiert Matratzen mit Bettdecken und Bettbezüge sauber für jeden Gast.</p>","<p>Entspannen Sie in unserem Standard-Zimmer, komplett mit zwei Queen-Size-Betten, individuelle Möbel, gekrönt Gel-infundiert Matratzen mit Bettdecken und Bettbezüge sauber für jeden Gast, Flachbild-TV, Mini-Kühlschrank, Executive Schreibtisch, Wireless Internet, alle geschmackvoll Klimaanlagen an Ihren Fingerspitzen verziert. Übung in unserem Stand der Technik: Fitnessmöglichkeiten, schwimmen im beheizten Innenpool oder entspannen Sie in unserer Sauna, alle kostenlos für unsere Gäste.</p>");
INSERT INTO phpn_rooms_description VALUES("16","6","en","Double Room with Two Double Beds","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests.</p>","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests. The standard double room is ideal for a brief stay of up to a few days. It doesn\'t contain a kitchen, so guests can\'t cook their own meals.</p>");
INSERT INTO phpn_rooms_description VALUES("17","6","es","Habitación Doble con dos camas dobles","<p>Una habitación de hotel estándar es una habitación de hotel alargada diseñada para dos personas, aunque algunas habitaciones dobles estándar tienen capacidad para hasta cuatro personas. La habitación de hotel estándar doble está disponible en muchos hoteles de servicio completo, con la exclusión de las instalaciones que albergan sólo suites o huéspedes de estadías prolongadas.</p>","<p>Una habitación de hotel estándar es una habitación de hotel alargada diseñada para dos personas, aunque algunas habitaciones dobles estándar tienen capacidad para hasta cuatro personas. La habitación de hotel estándar doble está disponible en muchos hoteles de servicio completo, con la exclusión de las instalaciones que albergan sólo suites o huéspedes de estadías prolongadas. La habitación doble estándar es ideal para una breve estancia de hasta unos pocos días. No contiene una cocina, así que los huéspedes no pueden cocinar sus propias comidas.</p>");
INSERT INTO phpn_rooms_description VALUES("18","6","de","Double Room with Two Double BedsDoppelzimmer mit zwei Doppelbetten","<p>Ein Standard-Hotelzimmer ist eine längliche Hotelzimmer für zwei Personen ausgelegt, auch wenn einige Standard-Doppelzimmer für bis zu vier Personen. Die Standard-Doppelhotelzimmer ist in vielen Hotels mit umfassendem Service zur Verfügung, mit Ausnahme von Einrichtungen, die nur Suiten oder längere Aufenthalte Gäste beherbergen.</p>","<p>Ein Standard-Hotelzimmer ist eine längliche Hotelzimmer für zwei Personen ausgelegt, auch wenn einige Standard-Doppelzimmer für bis zu vier Personen. Die Standard-Doppelhotelzimmer ist in vielen Hotels mit umfassendem Service zur Verfügung, mit Ausnahme von Einrichtungen, die nur Suiten oder längere Aufenthalte Gäste beherbergen. Die Standard-Doppelzimmer ist ideal für einen kurzen Aufenthalt von bis zu wenigen Tagen. Es enthält nicht eine Küche, so können die Gäste ihre eigenen Mahlzeiten nicht kochen.</p>");
INSERT INTO phpn_rooms_description VALUES("19","7","en","Junior Suite","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.</p>","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.<br /> For your comfort and convenience, most also include a fully equipped European-style kitchen with appliances by Sub-Zero, and a sitting area with a twin-size sofa bed. All of our New York hotel guests enjoy full access to our health club and spa.</p>");
INSERT INTO phpn_rooms_description VALUES("20","7","es","Junior Suite","<p>Nuestro hotel espaciosas Junior Suites son de 460 pies cuadrados (43 metros cuadrados) con ventanas de piso a techo y vistas a Manhattan desde nuestras suites de hotel de Nueva York. Las Junior Suites ofrecen lujosas camas king, de 55 pulgadas HDTV de pantalla plana con reproductor de Blu-ray y soporte para iPod, baños de mármol y un amplio walk-in closet.</p>","<p>Nuestro hotel espaciosas Junior Suites son de 460 pies cuadrados (43 metros cuadrados) con ventanas de piso a techo y vistas a Manhattan desde nuestras suites de hotel de Nueva York. Las Junior Suites ofrecen lujosas camas king, de 55 pulgadas HDTV de pantalla plana con reproductor de Blu-ray y soporte para iPod, baños de mármol y un amplio walk-in closet.</p>\n<p>Para su comodidad y conveniencia, la mayoría también incluyen una cocina de estilo europeo, totalmente equipada con electrodomésticos de Sub-Zero, y una zona de estar con un sofá cama doble de tamaño. Todos los huéspedes del hotel de Nueva York de un acceso completo a nuestro club de salud y spa.</p>");
INSERT INTO phpn_rooms_description VALUES("21","7","de","Junior Suite","<p>Unsere geräumigen Junior-Hotel-Suiten sind 460 Quadratmeter (43 qm) mit vom Boden bis zur Decke reichenden Fenster mit Blick auf Manhattan aus unserer Hotelsuiten in New York. Junior Suiten bieten luxuriöse Kingsize-Betten, 55-Zoll-Flachbildfernseher mit Blu-ray-Player und iPod-Dockingstation, Marmorbäder und einen großen begehbaren Kleiderschrank.</p>","<p>Unsere geräumigen Junior-Hotel-Suiten sind 460 Quadratmeter (43 qm) mit vom Boden bis zur Decke reichenden Fenster mit Blick auf Manhattan aus unserer Hotelsuiten in New York. Junior Suiten bieten luxuriöse Kingsize-Betten, 55-Zoll-Flachbildfernseher mit Blu-ray-Player und iPod-Dockingstation, Marmorbäder und einen großen begehbaren Kleiderschrank.</p>\n<p>Für Ihren Komfort und Bequemlichkeit sind die meisten auch eine voll ausgestattete europäischen Stil Küche mit Geräten von Sub-Zero und einen Sitzbereich mit einem Doppelschlafsofa. Alle unsere Hotelgäste New York genießen Sie vollen Zugang zu unserem Health Club und Spa.</p>");
INSERT INTO phpn_rooms_description VALUES("22","8","en","Executive Double Room","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of NY. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of NY. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>\n<p>As you would expect from Guoman, all our 50 Executive Double bedrooms also benefit from fast, complimentary BT Wi-fi and entertainment at your fingertips with a 32-inch LCD TV. You’ll appreciate extra space to unwind and recharge for the day ahead with beautifully designed bathrooms featuring walk-in power showers and mist-free mirrors. Welcome details that provide an oasis of calm in the centre of London.</p>");
INSERT INTO phpn_rooms_description VALUES("23","8","es","Executive Double Room","<p>Impresionantes habitaciones dobles ejecutivas son el escenario perfecto en el corazón de Londres. Las características exclusivas que no encontrará en las habitaciones Doble Cumberland habitaciones incluyen albornoces y zapatillas de lujo, agua mineral de cortesía y acceso completo a la Guoman Club Lounge, con periódicos gratuitos y refrescos disponibles durante todo el día.</p>","<p>Impresionantes habitaciones dobles ejecutivas son el escenario perfecto en el corazón. Las características exclusivas que no encontrará en las habitaciones Doble Cumberland habitaciones incluyen albornoces y zapatillas de lujo, agua mineral de cortesía y acceso completo a la Guoman Club Lounge, con periódicos gratuitos y refrescos disponibles durante todo el día.</p>\n<p>Como era de esperar de Guoman, todas nuestras 50 habitaciones dobles ejecutivas también se benefician de la rápida, conexión BT Wi-fi y el entretenimiento a tu alcance con un televisor LCD de 32 pulgadas. Usted apreciará el espacio extra para relajarse y recargar para el día siguiente, con baños de bonito diseño con duchas a presión ras de suelo y espejos antivaho. Detalles de bienvenida que proporcionan un oasis de calma en el centro de Londres.</p>");
INSERT INTO phpn_rooms_description VALUES("24","8","de","Executive Double Room","<p>Atemberaubende Executive Doppelzimmer bieten die perfekte Lage im Herzen von New York. Exklusive Funktionen, die Sie nicht in Doppel Cumberland Zimmer zu finden sind luxuriöse Bademäntel und Hausschuhe, Mineralwasser und vollen Zugriff auf die Guoman Club-Lounge mit kostenlosen Zeitungen und Erfrischungen den ganzen Tag.</p>","<p>Atemberaubende Executive Doppelzimmer bieten die perfekte Lage im Herzen von New York. Exklusive Funktionen, die Sie nicht in Doppel Cumberland Zimmer zu finden sind luxuriöse Bademäntel und Hausschuhe, Mineralwasser und vollen Zugriff auf die Guoman Club-Lounge mit kostenlosen Zeitungen und Erfrischungen den ganzen Tag.</p>\n<p>Wie Sie aus Guoman erwarten, alle unsere 50 Executive Doppelzimmer auch schnelle, kostenlose BT Wi-Fi-und Unterhaltungs an Ihren Fingerspitzen mit einem 32-Zoll-LCD-TV profitieren. Sie werden mehr Platz um sich zu entspannen und neue Kräfte für den kommenden Tag mit wunderschön gestaltete Bäder mit begehbarer Massageduschen und beschlagfreien Spiegel zu schätzen wissen. Willkommen Details, die eine Oase der Ruhe im Zentrum von London zu liefern.</p>");
INSERT INTO phpn_rooms_description VALUES("25","9","en","Single Bed","<p>TEST</p>","<p>TEST</p>");
INSERT INTO phpn_rooms_description VALUES("26","9","es","Single Bed","<p>TEST</p>","<p>TEST</p>");
INSERT INTO phpn_rooms_description VALUES("27","9","de","Single Bed","<p>TEST</p>","<p>TEST</p>");



DROP TABLE IF EXISTS phpn_rooms_prices;

CREATE TABLE `phpn_rooms_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date DEFAULT '0000-00-00',
  `date_to` date DEFAULT '0000-00-00',
  `adults` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `children` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extra_bed_charge` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `mon` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tue` decimal(10,2) NOT NULL DEFAULT '0.00',
  `wed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `thu` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fri` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sat` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sun` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms_prices VALUES("1","1","0000-00-00","0000-00-00","1","0","22.00","55.00","55.00","55.00","55.00","55.00","55.00","55.00","1");
INSERT INTO phpn_rooms_prices VALUES("2","2","0000-00-00","0000-00-00","2","1","30.00","80.00","80.00","80.00","80.00","80.00","80.00","80.00","1");
INSERT INTO phpn_rooms_prices VALUES("3","3","0000-00-00","0000-00-00","3","1","50.00","140.00","140.00","140.00","140.00","140.00","140.00","140.00","1");
INSERT INTO phpn_rooms_prices VALUES("4","4","0000-00-00","0000-00-00","4","2","90.00","190.00","190.00","190.00","190.00","190.00","190.00","190.00","1");
INSERT INTO phpn_rooms_prices VALUES("5","5","0000-00-00","0000-00-00","1","0","22.00","55.00","55.00","55.00","55.00","55.00","55.00","55.00","1");
INSERT INTO phpn_rooms_prices VALUES("6","6","0000-00-00","0000-00-00","2","1","30.00","80.00","80.00","80.00","80.00","80.00","80.00","80.00","1");
INSERT INTO phpn_rooms_prices VALUES("7","7","0000-00-00","0000-00-00","3","1","50.00","140.00","140.00","140.00","140.00","140.00","140.00","140.00","1");
INSERT INTO phpn_rooms_prices VALUES("8","8","0000-00-00","0000-00-00","4","2","90.00","190.00","190.00","190.00","190.00","190.00","190.00","190.00","1");
INSERT INTO phpn_rooms_prices VALUES("9","9","0000-00-00","0000-00-00","2","2","0.00","0.00","0.00","0.00","0.00","0.00","0.00","0.00","1");



DROP TABLE IF EXISTS phpn_search_wordlist;

CREATE TABLE `phpn_search_wordlist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `word_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `word_count` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `word_text` (`word_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_settings;

CREATE TABLE `phpn_settings` (
  `id` smallint(6) NOT NULL,
  `template` varchar(32) CHARACTER SET latin1 NOT NULL,
  `ssl_mode` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - no, 1 - entire site, 2 - admin, 3 - customer & payment modules',
  `seo_urls` tinyint(1) NOT NULL DEFAULT '1',
  `date_format` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dd/mm/yyyy',
  `time_zone` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `price_format` enum('european','american') CHARACTER SET latin1 NOT NULL,
  `week_start_day` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `admin_email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `mailer` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT 'php_mail_standard',
  `mailer_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mailer_wysiwyg_type` enum('none','tinymce') CHARACTER SET latin1 NOT NULL DEFAULT 'none',
  `smtp_secure` enum('ssl','tls','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ssl',
  `smtp_host` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_port` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_password` varchar(50) CHARACTER SET latin1 NOT NULL,
  `wysiwyg_type` enum('none','tinymce') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tinymce',
  `rss_feed` tinyint(1) NOT NULL DEFAULT '1',
  `rss_feed_type` enum('rss1','rss2','atom') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'rss1',
  `rss_last_ids` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_offline` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `caching_allowed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cache_lifetime` tinyint(3) unsigned NOT NULL DEFAULT '5' COMMENT 'in minutes',
  `offline_message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_rank` varchar(2) CHARACTER SET latin1 NOT NULL,
  `alexa_rank` varchar(12) CHARACTER SET latin1 NOT NULL,
  `cron_type` enum('batch','non-batch','stop') CHARACTER SET latin1 NOT NULL DEFAULT 'non-batch',
  `cron_run_last_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cron_run_period` enum('minute','hour') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'minute',
  `cron_run_period_value` smallint(6) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_settings VALUES("0","default","0","0","mm/dd/yyyy","0","american","1","info@yourdomain.com","php","php_mail_standard","none","ssl","","","","","tinymce","1","rss2","90-54-48-19-16-13-10-7-4-1","0","0","5","Our website is currently offline for maintenance. Please visit us later.","-1","0","stop","2013-11-21 16:21:27","hour","24");



DROP TABLE IF EXISTS phpn_site_description;

CREATE TABLE `phpn_site_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `header_text` text COLLATE utf8_unicode_ci NOT NULL,
  `slogan_text` text COLLATE utf8_unicode_ci NOT NULL,
  `footer_text` text COLLATE utf8_unicode_ci NOT NULL,
  `tag_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_description` text COLLATE utf8_unicode_ci NOT NULL,
  `tag_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_site_description VALUES("1","en","uHotelBooking","your slogan here...","uHotelBooking © <a class=\"footer_link\" href=\"http://www.hotel-booking-script.com/index.php\">ApPHP</a>","uHotelBooking","uHotelBooking","php hotel script, hotel online booking site");
INSERT INTO phpn_site_description VALUES("2","es","uHotelBooking","su lema aquí...","uHotelBooking © <a class=\"footer_link\" href=\"http://www.hotel-booking-script.com/index.php\">ApPHP</a>","uHotelBooking","uHotelBooking","php sitio del hotel, sitio de reserva de hotel en línea");
INSERT INTO phpn_site_description VALUES("3","de","uHotelBooking","Ihr Slogan hier ...","uHotelBooking © <a class=\"footer_link\" href=\"http://www.hotel-booking-script.com/index.php\">ApPHP</a>","uHotelBooking","uHotelBooking","php Hotel Website, Hotel online buchen Website");



DROP TABLE IF EXISTS phpn_states;

CREATE TABLE `phpn_states` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `country_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `abbrv` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_states VALUES("1","224","AL","Alabama","1","1");
INSERT INTO phpn_states VALUES("2","224","AK","Alaska","1","2");
INSERT INTO phpn_states VALUES("3","224","AZ","Arizona","1","3");
INSERT INTO phpn_states VALUES("4","224","AR","Arkansas","1","4");
INSERT INTO phpn_states VALUES("5","224","CA","California","1","5");
INSERT INTO phpn_states VALUES("6","224","CO","Colorado","1","6");
INSERT INTO phpn_states VALUES("7","224","CT","Connecticut","1","7");
INSERT INTO phpn_states VALUES("8","224","DE","Delaware","1","8");
INSERT INTO phpn_states VALUES("9","224","DC","District of Columbia","1","9");
INSERT INTO phpn_states VALUES("10","224","FL","Florida","1","10");
INSERT INTO phpn_states VALUES("11","224","GA","Georgia","1","11");
INSERT INTO phpn_states VALUES("12","224","HI","Hawaii","1","12");
INSERT INTO phpn_states VALUES("13","224","ID","Idaho","1","13");
INSERT INTO phpn_states VALUES("14","224","IL","Illinois","1","14");
INSERT INTO phpn_states VALUES("15","224","IN","Indiana","1","15");
INSERT INTO phpn_states VALUES("16","224","IA","Iowa","1","16");
INSERT INTO phpn_states VALUES("17","224","KS","Kansas","1","17");
INSERT INTO phpn_states VALUES("18","224","KY","Kentucky","1","18");
INSERT INTO phpn_states VALUES("19","224","LA","Louisiana","1","19");
INSERT INTO phpn_states VALUES("20","224","ME","Maine","1","20");
INSERT INTO phpn_states VALUES("21","224","MD","Maryland","1","21");
INSERT INTO phpn_states VALUES("22","224","MA","Massachusetts","1","22");
INSERT INTO phpn_states VALUES("23","224","MI","Michigan","1","22");
INSERT INTO phpn_states VALUES("24","224","MN","Minnesota","1","23");
INSERT INTO phpn_states VALUES("25","224","MS","Mississippi","1","25");
INSERT INTO phpn_states VALUES("26","224","MT","Montana","1","27");
INSERT INTO phpn_states VALUES("27","224","MO","Missouri","1","26");
INSERT INTO phpn_states VALUES("28","224","NE","Nebraska","1","28");
INSERT INTO phpn_states VALUES("29","224","NV","Nevada","1","29");
INSERT INTO phpn_states VALUES("30","224","NH","New Hampshire","1","30");
INSERT INTO phpn_states VALUES("31","224","NJ","New Jersey","1","31");
INSERT INTO phpn_states VALUES("32","224","NM","New Mexico","1","32");
INSERT INTO phpn_states VALUES("33","224","NY","New York","1","33");
INSERT INTO phpn_states VALUES("34","224","NC","North Carolina","1","34");
INSERT INTO phpn_states VALUES("35","224","ND","North Dakota","1","35");
INSERT INTO phpn_states VALUES("36","224","OH","Ohio","1","36");
INSERT INTO phpn_states VALUES("37","224","OK","Oklahoma","1","37");
INSERT INTO phpn_states VALUES("38","224","OR","Oregon","1","38");
INSERT INTO phpn_states VALUES("39","224","PA","Pennsylvania","1","39");
INSERT INTO phpn_states VALUES("40","224","RI","Rhode Island","1","40");
INSERT INTO phpn_states VALUES("41","224","SC","South Carolina","1","41");
INSERT INTO phpn_states VALUES("42","224","SD","South Dakota","1","42");
INSERT INTO phpn_states VALUES("43","224","TN","Tennessee","1","43");
INSERT INTO phpn_states VALUES("44","224","TX","Texas","1","44");
INSERT INTO phpn_states VALUES("45","224","UT","Utah","1","45");
INSERT INTO phpn_states VALUES("46","224","VT","Vermont","1","46");
INSERT INTO phpn_states VALUES("47","224","VA","Virginia","1","47");
INSERT INTO phpn_states VALUES("48","224","WA","Washington","1","48");
INSERT INTO phpn_states VALUES("49","224","WV","West Virginia","1","49");
INSERT INTO phpn_states VALUES("50","224","WI","Wisconsin","1","50");
INSERT INTO phpn_states VALUES("51","224","WY","Wyoming","1","51");
INSERT INTO phpn_states VALUES("52","13","NSW","New South Wales","1","1");
INSERT INTO phpn_states VALUES("53","13","QLD","Queensland","1","2");
INSERT INTO phpn_states VALUES("54","13","SA","South Australia","1","3");
INSERT INTO phpn_states VALUES("55","13","TAS","Tasmania","1","4");
INSERT INTO phpn_states VALUES("56","13","VIC","Victoria","1","5");
INSERT INTO phpn_states VALUES("57","13","WA","Western Australia","1","6");
INSERT INTO phpn_states VALUES("58","39","AB","Alberta","1","1");
INSERT INTO phpn_states VALUES("59","39","BC","British Columbia","1","2");
INSERT INTO phpn_states VALUES("60","39","ON","Ontario","1","6");
INSERT INTO phpn_states VALUES("61","39","QC","Quebec","1","8");
INSERT INTO phpn_states VALUES("62","39","NS","Nova Scotia","1","5");
INSERT INTO phpn_states VALUES("63","39","NB","New Brunswick","1","4");
INSERT INTO phpn_states VALUES("64","39","MB","Manitoba","1","3");
INSERT INTO phpn_states VALUES("65","39","PE","Prince Edward Island","1","7");
INSERT INTO phpn_states VALUES("66","39","SK","Saskatchewan","1","9");
INSERT INTO phpn_states VALUES("67","39","NL","Newfoundland and Labrador","1","10");



DROP TABLE IF EXISTS phpn_testimonials;

CREATE TABLE `phpn_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author_country` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author_email` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `testimonial_text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `priority_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO phpn_testimonials VALUES("1","Roberto","IT","Rome","roberto@email.com","Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.","1","0");
INSERT INTO phpn_testimonials VALUES("2","Hantz","DE","Munich","hantz@email.com","Typi non habent claritatem insitam est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.","1","1");
INSERT INTO phpn_testimonials VALUES("3","Lilian","GB","London","lilian@email.com","Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.","1","3");
INSERT INTO phpn_testimonials VALUES("4","Debora","US","","debora@email.com","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1","2");



DROP TABLE IF EXISTS phpn_vocabulary;

CREATE TABLE `phpn_vocabulary` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `language_id` varchar(3) CHARACTER SET latin1 NOT NULL,
  `key_value` varchar(50) CHARACTER SET latin1 NOT NULL,
  `key_text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voc_item` (`language_id`,`key_value`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4751 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_vocabulary VALUES("1","de","_2CO_NOTICE","2CheckOut.com Inc. (Ohio, USA) is an authorized retailer for goods and services.");
INSERT INTO phpn_vocabulary VALUES("2","en","_2CO_NOTICE","2CheckOut.com Inc. (Ohio, USA) is an authorized retailer for goods and services.");
INSERT INTO phpn_vocabulary VALUES("3","es","_2CO_NOTICE","2CheckOut.com Inc. (Ohio, EE.UU.) es un vendedor autorizado de bienes y servicios.");
INSERT INTO phpn_vocabulary VALUES("4","de","_2CO_ORDER","2CO Order");
INSERT INTO phpn_vocabulary VALUES("5","en","_2CO_ORDER","2CO Order");
INSERT INTO phpn_vocabulary VALUES("6","es","_2CO_ORDER","2CO Orden");
INSERT INTO phpn_vocabulary VALUES("7","de","_ABBREVIATION","Abbreviation");
INSERT INTO phpn_vocabulary VALUES("8","en","_ABBREVIATION","Abbreviation");
INSERT INTO phpn_vocabulary VALUES("9","es","_ABBREVIATION","Abreviatura");
INSERT INTO phpn_vocabulary VALUES("10","de","_ABOUT_US","About Us");
INSERT INTO phpn_vocabulary VALUES("11","en","_ABOUT_US","About Us");
INSERT INTO phpn_vocabulary VALUES("12","es","_ABOUT_US","Quiénes somos");
INSERT INTO phpn_vocabulary VALUES("13","de","_ACCESS","Access");
INSERT INTO phpn_vocabulary VALUES("14","en","_ACCESS","Access");
INSERT INTO phpn_vocabulary VALUES("15","es","_ACCESS","Acceso");
INSERT INTO phpn_vocabulary VALUES("16","de","_ACCESSIBLE_BY","Erreichbar mit");
INSERT INTO phpn_vocabulary VALUES("17","en","_ACCESSIBLE_BY","Accessible By");
INSERT INTO phpn_vocabulary VALUES("18","es","_ACCESSIBLE_BY","Accesible En");
INSERT INTO phpn_vocabulary VALUES("19","de","_ACCOUNTS","Konten");
INSERT INTO phpn_vocabulary VALUES("20","en","_ACCOUNTS","Accounts");
INSERT INTO phpn_vocabulary VALUES("21","es","_ACCOUNTS","Cuentas");
INSERT INTO phpn_vocabulary VALUES("22","de","_ACCOUNTS_MANAGEMENT","Konten");
INSERT INTO phpn_vocabulary VALUES("23","en","_ACCOUNTS_MANAGEMENT","Accounts");
INSERT INTO phpn_vocabulary VALUES("24","es","_ACCOUNTS_MANAGEMENT","Gestión de Cuentas");
INSERT INTO phpn_vocabulary VALUES("25","de","_ACCOUNT_ALREADY_RESET","Ihr Konto wurde bereits zurückgesetzt! Bitte überprüfen Sie Ihre E-Mail-Posteingang für weitere Informationen.");
INSERT INTO phpn_vocabulary VALUES("26","en","_ACCOUNT_ALREADY_RESET","Your account is already reset! Please check your email inbox for more information.");
INSERT INTO phpn_vocabulary VALUES("27","es","_ACCOUNT_ALREADY_RESET","Tu cuenta se ha reiniciado ya! Por favor, revise su buzón de correo electrónico para obtener más información.");
INSERT INTO phpn_vocabulary VALUES("28","de","_ACCOUNT_CREATED_CONF_BY_ADMIN_MSG","Ihr Konto wurde erfolgreich erstellt! In wenigen Minuten sollten Sie eine E-Mail mit den Angaben von Ihrem Konto. <br> Nach der Genehmigung Ihrer Registrierung durch den Administrator, werden Sie in der Lage, in Ihrem Konto anmelden.");
INSERT INTO phpn_vocabulary VALUES("29","en","_ACCOUNT_CREATED_CONF_BY_ADMIN_MSG","Your account has been successfully created! In a few minutes you should receive an email, containing the details of your account. <br><br> After approval your registration by administrator, you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("30","es","_ACCOUNT_CREATED_CONF_BY_ADMIN_MSG","Su cuenta ha sido creada con éxito! En pocos minutos, usted debe recibir un correo electrónico, que contiene los detalles de su cuenta. <br> Después de la aprobación de su registro por el administrador, usted será capaz de acceder a su cuenta.");
INSERT INTO phpn_vocabulary VALUES("31","de","_ACCOUNT_CREATED_CONF_BY_EMAIL_MSG","Ihr Konto wurde erfolgreich erstellt! In wenigen Minuten sollten Sie eine E-Mail mit den Angaben Ihrer Registrierung. <br><br> füllen Sie dieses Anmeldeformular mit den Bestätigungs-Code, dass die angegebene E-Mail-Adresse gesendet wurde, und du wirst in der Lage sein in Ihrem Konto anmelden.");
INSERT INTO phpn_vocabulary VALUES("32","en","_ACCOUNT_CREATED_CONF_BY_EMAIL_MSG","Your account has been successfully created! In a few minutes you should receive an email, containing the details of your registration. <br><br> Complete this registration, using the confirmation code that was sent to the provided email address, and you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("33","es","_ACCOUNT_CREATED_CONF_BY_EMAIL_MSG","Su cuenta ha sido creada con éxito! En pocos minutos, usted debe recibir un correo electrónico, que contiene los detalles de su registro. <br><br> completar este registro, utilizando el código de confirmación que fue enviado a la dirección de correo electrónico proporcionado, y que será capaz de acceder a su cuenta.");
INSERT INTO phpn_vocabulary VALUES("34","de","_ACCOUNT_CREATED_CONF_MSG","Ihr Konto wurde erfolgreich erstellt. <b>Sie erhalten nun eine Bestätigungsmail</b> mit den Angaben von Ihrem Konto (es kann ein paar Minuten dauern). <br><br> Nach der Bestätigung werden Sie in Ihr Konto einloggen.");
INSERT INTO phpn_vocabulary VALUES("35","en","_ACCOUNT_CREATED_CONF_MSG","Your account has been successfully created. <b>You will receive now an email</b>, containing the details of your account (it may take a few minutes).<br><br>After approval by an administrator, you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("36","es","_ACCOUNT_CREATED_CONF_MSG","Su cuenta se ha creado correctamente. <b>recibirán ahora un correo electrónico</b>,que contiene los detalles de su cuenta (puede tardar unos minutos).<br><br>Después de la aprobación de un administrador, podrá acceder a su cuenta .");
INSERT INTO phpn_vocabulary VALUES("37","de","_ACCOUNT_CREATED_MSG","Ihr Konto wurde erfolgreich erstellt. <b>Sie erhalten nun eine Bestätigungsmail</b> mit den Angaben von Ihrem Konto (es kann ein paar Minuten dauern).<br /><br />Nach der Bestätigung werden Sie in Ihr Konto einloggen.");
INSERT INTO phpn_vocabulary VALUES("38","en","_ACCOUNT_CREATED_MSG","Your account has been successfully created. <b>You will receive now a confirmation email</b>, containing the details of your account (it may take a few minutes). <br /><br />After completing the confirmation you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("39","es","_ACCOUNT_CREATED_MSG","Su cuenta se creó correctamente. <b>Usted recibirá ahora una-mail de confirmación </b>, que contiene los detalles de su cuenta (puede tardar unos minutos). <br /> <br /> Después de completar la confirmación que será capaz de acceder a su cuenta.");
INSERT INTO phpn_vocabulary VALUES("40","de","_ACCOUNT_CREATED_NON_CONFIRM_LINK","Klicken Sie <a href=index.php?customer=login>hier</a>");
INSERT INTO phpn_vocabulary VALUES("41","en","_ACCOUNT_CREATED_NON_CONFIRM_LINK","Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("42","es","_ACCOUNT_CREATED_NON_CONFIRM_LINK","Haga clic <a href=index.php?customer=login>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("43","de","_ACCOUNT_CREATED_NON_CONFIRM_MSG","Ihr Konto wurde erfolgreich erstellt! Für Ihre Bequemlichkeit in wenigen Minuten erhalten Sie eine E-Mail mit den Angaben der Anmeldung (keine Bestätigung erforderlich). <br><br> Sie können in Ihrem Konto jetzt anmelden.");
INSERT INTO phpn_vocabulary VALUES("44","en","_ACCOUNT_CREATED_NON_CONFIRM_MSG","Your account has been successfully created! For your convenience in a few minutes you will receive an email, containing the details of your registration (no confirmation required). <br><br>You may log into your account now.");
INSERT INTO phpn_vocabulary VALUES("45","es","_ACCOUNT_CREATED_NON_CONFIRM_MSG","Su cuenta ha sido creada con éxito! Para su comodidad, en pocos minutos recibirá un correo electrónico, que contiene los datos de su inscripción (no se requiere confirmación). <br><br> Usted puede acceder a su cuenta ahora.");
INSERT INTO phpn_vocabulary VALUES("46","de","_ACCOUNT_CREATE_MSG","Diese Registrierung erfordert eine Bestätigung per E-Mail! <br> Bitte füllen Sie das untenstehende Formular mit korrekten Informationen.");
INSERT INTO phpn_vocabulary VALUES("47","en","_ACCOUNT_CREATE_MSG","This registration process requires confirmation via email! <br />Please fill out the form below with correct information.");
INSERT INTO phpn_vocabulary VALUES("48","es","_ACCOUNT_CREATE_MSG","Este proceso de registro requiere la confirmación por correo electrónico! <br /> Por favor, rellene el siguiente formulario con la información correcta.");
INSERT INTO phpn_vocabulary VALUES("49","de","_ACCOUNT_DETAILS","Kontodetails");
INSERT INTO phpn_vocabulary VALUES("50","en","_ACCOUNT_DETAILS","Account Details");
INSERT INTO phpn_vocabulary VALUES("51","es","_ACCOUNT_DETAILS","Detalles de la cuenta");
INSERT INTO phpn_vocabulary VALUES("52","de","_ACCOUNT_SUCCESSFULLY_RESET","Sie haben erfolgreich zurückgesetzt Ihr Konto und Benutzernamen mit temporären Passwort an Ihre E-Mail gesendet wurde.");
INSERT INTO phpn_vocabulary VALUES("53","en","_ACCOUNT_SUCCESSFULLY_RESET","You have successfully reset your account and username with temporary password have been sent to your email.");
INSERT INTO phpn_vocabulary VALUES("54","es","_ACCOUNT_SUCCESSFULLY_RESET","Ha restablecer su cuenta y nombre de usuario con una contraseña temporal se han enviado a su correo electrónico.");
INSERT INTO phpn_vocabulary VALUES("55","de","_ACCOUNT_TYPE","Kontotyp");
INSERT INTO phpn_vocabulary VALUES("56","en","_ACCOUNT_TYPE","Account type");
INSERT INTO phpn_vocabulary VALUES("57","es","_ACCOUNT_TYPE","Tipo de cuenta");
INSERT INTO phpn_vocabulary VALUES("58","de","_ACCOUNT_WAS_CREATED","Ihr Konto wurde erstellt");
INSERT INTO phpn_vocabulary VALUES("59","en","_ACCOUNT_WAS_CREATED","Your account has been created");
INSERT INTO phpn_vocabulary VALUES("60","es","_ACCOUNT_WAS_CREATED","Tu cuenta ha sido creada");
INSERT INTO phpn_vocabulary VALUES("61","de","_ACCOUNT_WAS_DELETED","Ihr Konto wurde erfolgreich entfernt! Innerhalb von Sekunden werden Sie automatisch auf die Startseite umgeleitet.");
INSERT INTO phpn_vocabulary VALUES("62","en","_ACCOUNT_WAS_DELETED","Your account has been successfully removed! In seconds, you will be automatically redirected to the homepage.");
INSERT INTO phpn_vocabulary VALUES("63","es","_ACCOUNT_WAS_DELETED","Su cuenta se ha eliminado correctamente! En cuestión de segundos, se le redirigirá automáticamente a la página principal.");
INSERT INTO phpn_vocabulary VALUES("64","de","_ACCOUNT_WAS_UPDATED","Ihr Konto wurde erfolgreich aktualisiert!");
INSERT INTO phpn_vocabulary VALUES("65","en","_ACCOUNT_WAS_UPDATED","Your account has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("66","es","_ACCOUNT_WAS_UPDATED","Su cuenta ha sido actualizada con éxito!");
INSERT INTO phpn_vocabulary VALUES("67","de","_ACCOUT_CREATED_CONF_LINK","Bereits bestätigt Ihre Registrierung? Klicken Sie <a href=index.php?customer=login>hier</a>");
INSERT INTO phpn_vocabulary VALUES("68","en","_ACCOUT_CREATED_CONF_LINK","Already confirmed your registration? Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("69","es","_ACCOUT_CREATED_CONF_LINK","Ya confirmaron su registro? Haga clic <a href=index.php?customer=login>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("70","de","_ACCOUT_CREATED_CONF_MSG","Bereits bestätigt Ihre Registrierung? Klicken Sie <a href=index.php?customer=login>hier</a>, um fortzufahren.");
INSERT INTO phpn_vocabulary VALUES("71","en","_ACCOUT_CREATED_CONF_MSG","Already confirmed your registration? Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("72","es","_ACCOUT_CREATED_CONF_MSG","Ya confirmaron su registro? Haga clic  <a href=index.php?customer=login>aquí</a>  para continuar.");
INSERT INTO phpn_vocabulary VALUES("73","de","_ACTIONS","Aktion");
INSERT INTO phpn_vocabulary VALUES("74","en","_ACTIONS","Action");
INSERT INTO phpn_vocabulary VALUES("75","es","_ACTIONS","Acción");
INSERT INTO phpn_vocabulary VALUES("76","de","_ACTIONS_WORD","Aktion");
INSERT INTO phpn_vocabulary VALUES("77","en","_ACTIONS_WORD","Action");
INSERT INTO phpn_vocabulary VALUES("78","es","_ACTIONS_WORD","Acción");
INSERT INTO phpn_vocabulary VALUES("79","de","_ACTION_REQUIRED","MASSNAHMEN ERFORDERLICH");
INSERT INTO phpn_vocabulary VALUES("80","en","_ACTION_REQUIRED","ACTION REQUIRED");
INSERT INTO phpn_vocabulary VALUES("81","es","_ACTION_REQUIRED","MEDIDAS QUE SE SOLICITAN");
INSERT INTO phpn_vocabulary VALUES("82","de","_ACTIVATION_EMAIL_ALREADY_SENT","Die Aktivierung per E-Mail wurde bereits auf Ihre E-Mail geschickt. Bitte versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("83","en","_ACTIVATION_EMAIL_ALREADY_SENT","The activation email has been already sent to your email. Please try again later.");
INSERT INTO phpn_vocabulary VALUES("84","es","_ACTIVATION_EMAIL_ALREADY_SENT","El email de activación ya fue enviado a su correo electrónico. Por favor, inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("85","de","_ACTIVATION_EMAIL_WAS_SENT","Eine Email wurde an _EMAIL_ wurde mit einem Aktivierungsschlüssel gesendet. Bitte überprüfen Sie Ihre E-Mail um die Registrierung abzuschließen.");
INSERT INTO phpn_vocabulary VALUES("86","en","_ACTIVATION_EMAIL_WAS_SENT","An email has been sent to _EMAIL_ with an activation key. Please check your mail to complete registration.");
INSERT INTO phpn_vocabulary VALUES("87","es","_ACTIVATION_EMAIL_WAS_SENT","Un correo electrónico ha sido enviado a _EMAIL_ con una clave de activación. Por favor, revise su correo para completar el registro.");
INSERT INTO phpn_vocabulary VALUES("88","de","_ACTIVE","Aktiv");
INSERT INTO phpn_vocabulary VALUES("89","en","_ACTIVE","Active");
INSERT INTO phpn_vocabulary VALUES("90","es","_ACTIVE","Activo");
INSERT INTO phpn_vocabulary VALUES("91","de","_ADD","Hinzufügen");
INSERT INTO phpn_vocabulary VALUES("92","en","_ADD","Add");
INSERT INTO phpn_vocabulary VALUES("93","es","_ADD","Añadir");
INSERT INTO phpn_vocabulary VALUES("94","de","_ADDING_OPERATION_COMPLETED","Die Zugabe Vorgang wurde erfolgreich abgeschlossen!");
INSERT INTO phpn_vocabulary VALUES("95","en","_ADDING_OPERATION_COMPLETED","The adding operation completed successfully!");
INSERT INTO phpn_vocabulary VALUES("96","es","_ADDING_OPERATION_COMPLETED","La operación terminó con éxito agregando!");
INSERT INTO phpn_vocabulary VALUES("97","de","_ADDITIONAL_INFO","Zusätzliche Informationen");
INSERT INTO phpn_vocabulary VALUES("98","en","_ADDITIONAL_INFO","Additional Info");
INSERT INTO phpn_vocabulary VALUES("99","es","_ADDITIONAL_INFO","Información Adicional");
INSERT INTO phpn_vocabulary VALUES("100","de","_ADDITIONAL_MODULES","Zusätzliche Module");
INSERT INTO phpn_vocabulary VALUES("101","en","_ADDITIONAL_MODULES","Additional Modules");
INSERT INTO phpn_vocabulary VALUES("102","es","_ADDITIONAL_MODULES","Los módulos adicionales");
INSERT INTO phpn_vocabulary VALUES("103","de","_ADDITIONAL_PAYMENT","Zusätzliche Zahlung");
INSERT INTO phpn_vocabulary VALUES("104","en","_ADDITIONAL_PAYMENT","Additional Payment");
INSERT INTO phpn_vocabulary VALUES("105","es","_ADDITIONAL_PAYMENT","El pago adicional");
INSERT INTO phpn_vocabulary VALUES("106","de","_ADDITIONAL_PAYMENT_TOOLTIP","Um eine zusätzliche Zahlung oder admin Rabatt geben in diesem Feld einen entsprechenden Wert (positiv oder negativ) gelten.");
INSERT INTO phpn_vocabulary VALUES("107","en","_ADDITIONAL_PAYMENT_TOOLTIP","To apply an additional payment or admin discount enter into this field an appropriate value (positive or negative).");
INSERT INTO phpn_vocabulary VALUES("108","es","_ADDITIONAL_PAYMENT_TOOLTIP","Para solicitar un pago adicional o un descuento de administración entrar en este campo un valor adecuado (positivo o negativo).");
INSERT INTO phpn_vocabulary VALUES("109","de","_ADDRESS","Adresse");
INSERT INTO phpn_vocabulary VALUES("110","en","_ADDRESS","Address");
INSERT INTO phpn_vocabulary VALUES("111","es","_ADDRESS","Dirección");
INSERT INTO phpn_vocabulary VALUES("112","de","_ADDRESS_2","Adresse (Zeile 2)");
INSERT INTO phpn_vocabulary VALUES("113","en","_ADDRESS_2","Address (line 2)");
INSERT INTO phpn_vocabulary VALUES("114","es","_ADDRESS_2","Dirección (línea 2)");
INSERT INTO phpn_vocabulary VALUES("115","de","_ADDRESS_EMPTY_ALERT","Adresse darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("116","en","_ADDRESS_EMPTY_ALERT","Address cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("117","es","_ADDRESS_EMPTY_ALERT","La dirección no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("118","de","_ADD_DEFAULT_PERIODS","In Standard-Perioden");
INSERT INTO phpn_vocabulary VALUES("119","en","_ADD_DEFAULT_PERIODS","Add Default Periods");
INSERT INTO phpn_vocabulary VALUES("120","es","_ADD_DEFAULT_PERIODS","Añadir períodos predeterminados");
INSERT INTO phpn_vocabulary VALUES("121","de","_ADD_NEW","Neu hinzufügen");
INSERT INTO phpn_vocabulary VALUES("122","en","_ADD_NEW","Add New");
INSERT INTO phpn_vocabulary VALUES("123","es","_ADD_NEW","Agregar nuevo");
INSERT INTO phpn_vocabulary VALUES("124","de","_ADD_NEW_MENU","Schreibe Neues Menü");
INSERT INTO phpn_vocabulary VALUES("125","en","_ADD_NEW_MENU","Add New Menu");
INSERT INTO phpn_vocabulary VALUES("126","es","_ADD_NEW_MENU","Añadir un nuevo menú");
INSERT INTO phpn_vocabulary VALUES("127","de","_ADD_TO_CART","In den Warenkorb");
INSERT INTO phpn_vocabulary VALUES("128","en","_ADD_TO_CART","Add to Cart");
INSERT INTO phpn_vocabulary VALUES("129","es","_ADD_TO_CART","Añadir a la cesta");
INSERT INTO phpn_vocabulary VALUES("130","de","_ADD_TO_MENU","Ins Menü");
INSERT INTO phpn_vocabulary VALUES("131","en","_ADD_TO_MENU","Add To Menu");
INSERT INTO phpn_vocabulary VALUES("132","es","_ADD_TO_MENU","Añadir al menú");
INSERT INTO phpn_vocabulary VALUES("133","de","_ADMIN","Admin");
INSERT INTO phpn_vocabulary VALUES("134","en","_ADMIN","Admin");
INSERT INTO phpn_vocabulary VALUES("135","es","_ADMIN","Admin");
INSERT INTO phpn_vocabulary VALUES("136","de","_ADMINISTRATOR_ONLY","Nur Administrator");
INSERT INTO phpn_vocabulary VALUES("137","en","_ADMINISTRATOR_ONLY","Administrator Only");
INSERT INTO phpn_vocabulary VALUES("138","es","_ADMINISTRATOR_ONLY","Administrador con permiso de");
INSERT INTO phpn_vocabulary VALUES("139","de","_ADMINS","Admins");
INSERT INTO phpn_vocabulary VALUES("140","en","_ADMINS","Admins");
INSERT INTO phpn_vocabulary VALUES("141","es","_ADMINS","Administradores");
INSERT INTO phpn_vocabulary VALUES("142","de","_ADMINS_AND_CUSTOMERS","Kunden & Admins");
INSERT INTO phpn_vocabulary VALUES("143","en","_ADMINS_AND_CUSTOMERS","Customers & Admins");
INSERT INTO phpn_vocabulary VALUES("144","es","_ADMINS_AND_CUSTOMERS","Clientes y Administradores");
INSERT INTO phpn_vocabulary VALUES("145","de","_ADMINS_MANAGEMENT","Admins Management");
INSERT INTO phpn_vocabulary VALUES("146","en","_ADMINS_MANAGEMENT","Admins Management");
INSERT INTO phpn_vocabulary VALUES("147","es","_ADMINS_MANAGEMENT","Administradores de Gestión de");
INSERT INTO phpn_vocabulary VALUES("148","de","_ADMINS_OWNERS_MANAGEMENT","Admins & Hotel Inhaber Geschäftsführung");
INSERT INTO phpn_vocabulary VALUES("149","en","_ADMINS_OWNERS_MANAGEMENT","Admins & Hotel Owners Management");
INSERT INTO phpn_vocabulary VALUES("150","es","_ADMINS_OWNERS_MANAGEMENT","Administradores & Hotel Propietario Gestión");
INSERT INTO phpn_vocabulary VALUES("151","de","_ADMIN_EMAIL","Admin E-Mail");
INSERT INTO phpn_vocabulary VALUES("152","en","_ADMIN_EMAIL","Admin Email");
INSERT INTO phpn_vocabulary VALUES("153","es","_ADMIN_EMAIL","Admin Email");
INSERT INTO phpn_vocabulary VALUES("154","de","_ADMIN_EMAIL_ALERT","Diese E-Mail wird als \"Von\"-Adresse für das System E-Mail-Benachrichtigungen verwendet. Stellen Sie sicher, dass Sie hier geben Sie eine gültige E-Mail-Adresse auf Domain Ihrer Website basiert");
INSERT INTO phpn_vocabulary VALUES("155","en","_ADMIN_EMAIL_ALERT","This email is used as \"From\" address for the system email notifications. Make sure, that you write here a valid email address based on domain of your site");
INSERT INTO phpn_vocabulary VALUES("156","es","_ADMIN_EMAIL_ALERT","Este correo electrónico se utiliza como dirección \"De\" para las notificaciones del sistema de correo electrónico. Asegúrese de que usted escribe aquí una dirección válida de correo electrónico basado en dominio de su sitio");
INSERT INTO phpn_vocabulary VALUES("157","de","_ADMIN_EMAIL_EXISTS_ALERT","Administrator mit solchen E-Mail ist bereits vorhanden! Bitte wählen Sie einen anderen.");
INSERT INTO phpn_vocabulary VALUES("158","en","_ADMIN_EMAIL_EXISTS_ALERT","Administrator with such email already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("159","es","_ADMIN_EMAIL_EXISTS_ALERT","Administrador de correo electrónico ya existe! Por favor, elija otra.");
INSERT INTO phpn_vocabulary VALUES("160","de","_ADMIN_EMAIL_IS_EMPTY","Admin E-Mail darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("161","en","_ADMIN_EMAIL_IS_EMPTY","Admin email must not be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("162","es","_ADMIN_EMAIL_IS_EMPTY","Administrador de correo electrónico no debe estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("163","de","_ADMIN_EMAIL_WRONG","Admin E-Mail in falsche Format! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("164","en","_ADMIN_EMAIL_WRONG","Admin email in wrong format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("165","es","_ADMIN_EMAIL_WRONG","Administrador de correo electrónico en formato incorrecto! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("166","de","_ADMIN_FOLDER_CREATION_ERROR","Fehler beim Ordner zu diesem Autor in <b>images/upload/</b> Verzeichnis. Für eine stabile Arbeit des Skripts erstellen Sie bitte diesen Ordner manuell.");
INSERT INTO phpn_vocabulary VALUES("167","en","_ADMIN_FOLDER_CREATION_ERROR","Failed to create folder for this author in <b>images/upload/</b> directory. For stable work of the script please create this folder manually.");
INSERT INTO phpn_vocabulary VALUES("168","es","_ADMIN_FOLDER_CREATION_ERROR","No se pudo crear la carpeta para este autor en <b>images/upload/</b> directorio. Para el trabajo estable de la secuencia de comandos por favor crear esta carpeta de forma manual.");
INSERT INTO phpn_vocabulary VALUES("169","de","_ADMIN_LOGIN","Admin Login");
INSERT INTO phpn_vocabulary VALUES("170","en","_ADMIN_LOGIN","Admin Login");
INSERT INTO phpn_vocabulary VALUES("171","es","_ADMIN_LOGIN","Admin Login");
INSERT INTO phpn_vocabulary VALUES("172","de","_ADMIN_MAILER_ALERT","Wählen Sie die Mailer Sie für die Zustellung von E-Mails vor Ort bevorzugen.");
INSERT INTO phpn_vocabulary VALUES("173","en","_ADMIN_MAILER_ALERT","Select which mailer you prefer to use for the delivery of site emails.");
INSERT INTO phpn_vocabulary VALUES("174","es","_ADMIN_MAILER_ALERT","Seleccione el programa de correo que usted prefiere utilizar para la entrega de mensajes de correo electrónico del sitio.");
INSERT INTO phpn_vocabulary VALUES("175","de","_ADMIN_PANEL","Admin-Panel");
INSERT INTO phpn_vocabulary VALUES("176","en","_ADMIN_PANEL","Admin Panel");
INSERT INTO phpn_vocabulary VALUES("177","es","_ADMIN_PANEL","Panel de Administración");
INSERT INTO phpn_vocabulary VALUES("178","de","_ADMIN_RESERVATION","Admin Reservierung");
INSERT INTO phpn_vocabulary VALUES("179","en","_ADMIN_RESERVATION","Admin Reservation");
INSERT INTO phpn_vocabulary VALUES("180","es","_ADMIN_RESERVATION","Administración de reservas");
INSERT INTO phpn_vocabulary VALUES("181","de","_ADMIN_WELCOME_TEXT","<p>Willkommen Administrator Control Panel, mit dem Sie hinzufügen, bearbeiten oder löschen Website-Content. Mit dieser Administrator Control Panel können Sie einfach verwalten Kunden, Reservierungen und eine vollständige Hotel Bauleitung.</p><p><b>•</b> Es gibt einige Module für Sie: Backup & Restore, News. Installation oder Deinstallation von ihnen ist möglich aus <a href=\'index.php?admin=modules\'>Module Menu</a>.</p><p><b>•</b> Im <a href=\'index.php?admin=languages\'>Menü Sprachen</a> Sie können hinzufügen / entfernen oder ändern Sprache Sprache ändern und bearbeiten Sie Ihren Wortschatz (die Wörter und Sätze, die vom System verwendet).</p><p><b>•</b> <a href=\'index.php?admin=settings\'>Das Menü Einstellungen</a> können Sie wichtige Einstellungen für die Site zu definieren.</p><p><b>•</b> Im <a href=\'index.php?admin=my_account\'>Ihr Konto</a> gibt es eine Möglichkeit, um Ihre Daten ändern.</p><p><b>•</b> <a href=\'index.php?admin=menus\'>Menüs</a> und <a href=\'index.php?admin=pages\'>Seiten-Management</a> sind zur Erstellung und Verwaltung Menüs, Links und Seiten gestaltet.</p><p><b>•</b> So erstellen und bearbeiten Zimmertypen, Verfügbarkeiten, Preise, Buchungen und weitere Info zum Hotel, verwenden <a href=\'index.php?admin=hotel_info\'>Hotel Management</a>, <a href=\'index.php?admin=rooms_management\'>Zimmer Management</a> und <a href=\'index.php?admin=mod_booking_bookings\'>Buchungen</a> Menüs.</p>");
INSERT INTO phpn_vocabulary VALUES("182","en","_ADMIN_WELCOME_TEXT","<p>Welcome to Administrator Control Panel that allows you to add, edit or delete site content. With this Administrator Control Panel you can easy manage customers, reservations and perform a full hotel site management.</p><p><b>&#8226;</b> There are some modules for you: Backup & Restore, News. Installation or un-installation of them is possible from <a href=\'index.php?admin=modules\'>Modules Menu</a>.</p><p><b>&#8226;</b> In <a href=\'index.php?admin=languages\'>Languages Menu</a> you may add/remove language or change language settings and edit your vocabulary (the words and phrases, used by the system).</p><p><b>&#8226;</b> <a href=\'index.php?admin=settings\'>Settings Menu</a> allows you to define important settings for the site.</p><p><b>&#8226;</b> In <a href=\'index.php?admin=my_account\'>My Account</a> there is a possibility to change your info.</p><p><b>&#8226;</b> <a href=\'index.php?admin=menus\'>Menus</a> and <a href=\'index.php?admin=pages\'>Pages Management</a> are designed for creating and managing menus, links and pages.</p><p><b>&#8226;</b> To create and edit room types, seasons, prices, bookings and other hotel info, use <a href=\'index.php?admin=hotel_info\'>Hotel Management</a>, <a href=\'index.php?admin=rooms_management\'>Rooms Management</a> and <a href=\'index.php?admin=mod_booking_bookings\'>Bookings</a> menus.</p>");
INSERT INTO phpn_vocabulary VALUES("183","es","_ADMIN_WELCOME_TEXT","<p>Bienvenido al Administrador del panel de control que le permite agregar, editar o eliminar contenido del sitio. Con este Panel de Control del Administrador usted puede fácilmente administrar clientes, reservas y realizar una gestión hotelera de todo el sitio.</p>\n<p><b>&#8226;</b> Hay algunos módulos para usted: Copia de seguridad y restauración, Noticias. La instalación o desinstalación de ellos es posible de <a href=\'index.php?admin=modules\'>módulos Menú</a>.</p>\n<p><b>&#8226;</b> En <a href=\'index.php?admin=languages\'>Idiomas del menú</a> que puede agregar o quitar idioma o cambiar la configuración de idioma y modificar su vocabulario (las palabras y frases, utilizadas por el sistema).</p>\n<p><b>&#8226;</b> <a href=\'index.php?admin=settings\'>menú de configuración</a> le permite definir opciones importantes para el sitio.</p>\n<p><b>&#8226;</b> En <a href=\'index.php?admin=my_account\'>Mi Cuenta</a> hay una posibilidad de cambiar su información.</p>\n<p><b>&#8226;</b> <a href=\'index.php?admin=menus\'>menús</a> y <a href=\'index.php?admin=pages\'>Páginas de Gestión</a> están diseñados para crear y gestionar los menús, enlaces y páginas. </p>\n<p><b>&#8226;</b> Para crear y editar tipos de habitación, temporadas, precios, reservas y otros datos del hotel, el uso <a href=\'index.php?admin=hotel_info\'>Hotel Management</a>, <a href=\'index.php?admin=rooms_management\'>Habitaciones Gestión</a> y <a href=\'index.php?admin=mod_booking_bookings\'>reservas</a> menús.</p>");
INSERT INTO phpn_vocabulary VALUES("184","de","_ADULT","Erwachsene");
INSERT INTO phpn_vocabulary VALUES("185","en","_ADULT","Adult");
INSERT INTO phpn_vocabulary VALUES("186","es","_ADULT","Adulto");
INSERT INTO phpn_vocabulary VALUES("187","de","_ADULTS","Erwachsene");
INSERT INTO phpn_vocabulary VALUES("188","en","_ADULTS","Adults");
INSERT INTO phpn_vocabulary VALUES("189","es","_ADULTS","Adultos");
INSERT INTO phpn_vocabulary VALUES("190","de","_ADVANCED","Fortgeschrittene");
INSERT INTO phpn_vocabulary VALUES("191","en","_ADVANCED","Advanced");
INSERT INTO phpn_vocabulary VALUES("192","es","_ADVANCED","Avanzada");
INSERT INTO phpn_vocabulary VALUES("193","de","_AFTER","Nach");
INSERT INTO phpn_vocabulary VALUES("194","en","_AFTER","After");
INSERT INTO phpn_vocabulary VALUES("195","es","_AFTER","Después");
INSERT INTO phpn_vocabulary VALUES("196","de","_AFTER_DISCOUNT","nach Rabatt");
INSERT INTO phpn_vocabulary VALUES("197","en","_AFTER_DISCOUNT","after discount");
INSERT INTO phpn_vocabulary VALUES("198","es","_AFTER_DISCOUNT","después del descuento");
INSERT INTO phpn_vocabulary VALUES("199","de","_AGENT_COMMISION","Hotel Management/Agent Provision");
INSERT INTO phpn_vocabulary VALUES("200","en","_AGENT_COMMISION","Hotel Owner/Agent Commision");
INSERT INTO phpn_vocabulary VALUES("201","es","_AGENT_COMMISION","Hotel Propietario/Agente Commision");
INSERT INTO phpn_vocabulary VALUES("202","de","_AGREE_CONF_TEXT","Ich habe gelesen und akzeptiere AGB");
INSERT INTO phpn_vocabulary VALUES("203","en","_AGREE_CONF_TEXT","I have read and AGREE with Terms & Conditions");
INSERT INTO phpn_vocabulary VALUES("204","es","_AGREE_CONF_TEXT","He leído y de acuerdo con Términos y Condiciones");
INSERT INTO phpn_vocabulary VALUES("205","de","_ALBUM","Album");
INSERT INTO phpn_vocabulary VALUES("206","en","_ALBUM","Album");
INSERT INTO phpn_vocabulary VALUES("207","es","_ALBUM","Album");
INSERT INTO phpn_vocabulary VALUES("208","de","_ALBUM_CODE","Album Code");
INSERT INTO phpn_vocabulary VALUES("209","en","_ALBUM_CODE","Album Code");
INSERT INTO phpn_vocabulary VALUES("210","es","_ALBUM_CODE","Album Código");
INSERT INTO phpn_vocabulary VALUES("211","de","_ALBUM_NAME","Albumname");
INSERT INTO phpn_vocabulary VALUES("212","en","_ALBUM_NAME","Album Name");
INSERT INTO phpn_vocabulary VALUES("213","es","_ALBUM_NAME","Nombre del álbum");
INSERT INTO phpn_vocabulary VALUES("214","de","_ALERT_CANCEL_BOOKING","Sind Sie sicher, dass Sie diese Buchung stornieren?");
INSERT INTO phpn_vocabulary VALUES("215","en","_ALERT_CANCEL_BOOKING","Are you sure you want to cancel this booking?");
INSERT INTO phpn_vocabulary VALUES("216","es","_ALERT_CANCEL_BOOKING","¿Está seguro que desea cancelar esta reserva?");
INSERT INTO phpn_vocabulary VALUES("217","de","_ALERT_REQUIRED_FILEDS","Felder mit einem Sternchen (*) sind erforderlich");
INSERT INTO phpn_vocabulary VALUES("218","en","_ALERT_REQUIRED_FILEDS","Items marked with an asterisk (*) are required");
INSERT INTO phpn_vocabulary VALUES("219","es","_ALERT_REQUIRED_FILEDS","Los elementos marcados con un asterisco (*) son obligatorios");
INSERT INTO phpn_vocabulary VALUES("220","de","_ALL","Alle");
INSERT INTO phpn_vocabulary VALUES("221","en","_ALL","All");
INSERT INTO phpn_vocabulary VALUES("222","es","_ALL","Todo");
INSERT INTO phpn_vocabulary VALUES("223","de","_ALLOW","Erlauben");
INSERT INTO phpn_vocabulary VALUES("224","en","_ALLOW","Allow");
INSERT INTO phpn_vocabulary VALUES("225","es","_ALLOW","Permitir");
INSERT INTO phpn_vocabulary VALUES("226","de","_ALLOW_COMMENTS","Kommentare zulassen");
INSERT INTO phpn_vocabulary VALUES("227","en","_ALLOW_COMMENTS","Allow comments");
INSERT INTO phpn_vocabulary VALUES("228","es","_ALLOW_COMMENTS","Permitir comentarios");
INSERT INTO phpn_vocabulary VALUES("229","de","_ALL_AVAILABLE","Alle verfügbaren");
INSERT INTO phpn_vocabulary VALUES("230","en","_ALL_AVAILABLE","All Available");
INSERT INTO phpn_vocabulary VALUES("231","es","_ALL_AVAILABLE","Todos los disponibles");
INSERT INTO phpn_vocabulary VALUES("232","de","_ALREADY_HAVE_ACCOUNT","Sie haben bereits ein Konto? <a href=\'index.php?customer=login\'>Melden Sie sich hier</a>");
INSERT INTO phpn_vocabulary VALUES("233","en","_ALREADY_HAVE_ACCOUNT","Already have an account? <a href=\'index.php?customer=login\'>Login here</a>");
INSERT INTO phpn_vocabulary VALUES("234","es","_ALREADY_HAVE_ACCOUNT","¿Ya tienes una cuenta? <a href=\'index.php?customer=login\'>Entre aquí</a>");
INSERT INTO phpn_vocabulary VALUES("235","de","_ALREADY_LOGGED","Sie sind bereits angemeldet!");
INSERT INTO phpn_vocabulary VALUES("236","en","_ALREADY_LOGGED","You are already logged in!");
INSERT INTO phpn_vocabulary VALUES("237","es","_ALREADY_LOGGED","Usted ya está registrado!");
INSERT INTO phpn_vocabulary VALUES("238","de","_AMOUNT","Amount");
INSERT INTO phpn_vocabulary VALUES("239","en","_AMOUNT","Amount");
INSERT INTO phpn_vocabulary VALUES("240","es","_AMOUNT","Cantidad");
INSERT INTO phpn_vocabulary VALUES("241","de","_ANSWER","Antwort");
INSERT INTO phpn_vocabulary VALUES("242","en","_ANSWER","Answer");
INSERT INTO phpn_vocabulary VALUES("243","es","_ANSWER","Respuesta");
INSERT INTO phpn_vocabulary VALUES("244","de","_ANY","keine");
INSERT INTO phpn_vocabulary VALUES("245","en","_ANY","Any");
INSERT INTO phpn_vocabulary VALUES("246","es","_ANY","Cualquier");
INSERT INTO phpn_vocabulary VALUES("247","de","_APPLY","zutreffen");
INSERT INTO phpn_vocabulary VALUES("248","en","_APPLY","Apply");
INSERT INTO phpn_vocabulary VALUES("249","es","_APPLY","Aplicar");
INSERT INTO phpn_vocabulary VALUES("250","de","_APPLY_TO_ALL_LANGUAGES","Gelten für alle Sprachen");
INSERT INTO phpn_vocabulary VALUES("251","en","_APPLY_TO_ALL_LANGUAGES","Apply to all languages");
INSERT INTO phpn_vocabulary VALUES("252","es","_APPLY_TO_ALL_LANGUAGES","Se aplican a todos los idiomas");
INSERT INTO phpn_vocabulary VALUES("253","de","_APPLY_TO_ALL_PAGES","Änderungen für alle Seiten");
INSERT INTO phpn_vocabulary VALUES("254","en","_APPLY_TO_ALL_PAGES","Apply changes to all pages");
INSERT INTO phpn_vocabulary VALUES("255","es","_APPLY_TO_ALL_PAGES","Aplicar cambios a todas las páginas");
INSERT INTO phpn_vocabulary VALUES("256","de","_APPROVE","Genehmigen");
INSERT INTO phpn_vocabulary VALUES("257","en","_APPROVE","Approve");
INSERT INTO phpn_vocabulary VALUES("258","es","_APPROVE","Aprobar");
INSERT INTO phpn_vocabulary VALUES("259","de","_APPROVED","Genehmigt");
INSERT INTO phpn_vocabulary VALUES("260","en","_APPROVED","Approved");
INSERT INTO phpn_vocabulary VALUES("261","es","_APPROVED","Aprobado");
INSERT INTO phpn_vocabulary VALUES("262","de","_APRIL","April");
INSERT INTO phpn_vocabulary VALUES("263","en","_APRIL","April");
INSERT INTO phpn_vocabulary VALUES("264","es","_APRIL","Abril");
INSERT INTO phpn_vocabulary VALUES("265","de","_ARTICLE","Artikel");
INSERT INTO phpn_vocabulary VALUES("266","en","_ARTICLE","Article");
INSERT INTO phpn_vocabulary VALUES("267","es","_ARTICLE","Artículo");
INSERT INTO phpn_vocabulary VALUES("268","de","_ARTICLE_ID","Artikel-ID");
INSERT INTO phpn_vocabulary VALUES("269","en","_ARTICLE_ID","Article ID");
INSERT INTO phpn_vocabulary VALUES("270","es","_ARTICLE_ID","Id. de artículo");
INSERT INTO phpn_vocabulary VALUES("271","de","_AUGUST","August");
INSERT INTO phpn_vocabulary VALUES("272","en","_AUGUST","August");
INSERT INTO phpn_vocabulary VALUES("273","es","_AUGUST","Agosto");
INSERT INTO phpn_vocabulary VALUES("274","de","_AUTHENTICATION","Authentifizierung");
INSERT INTO phpn_vocabulary VALUES("275","en","_AUTHENTICATION","Authentication");
INSERT INTO phpn_vocabulary VALUES("276","es","_AUTHENTICATION","Autenticación");
INSERT INTO phpn_vocabulary VALUES("277","de","_AUTHORIZE_NET_NOTICE","Die Authorize.Net Payment Gateway Service Provider.");
INSERT INTO phpn_vocabulary VALUES("278","en","_AUTHORIZE_NET_NOTICE","The Authorize.Net payment gateway service provider.");
INSERT INTO phpn_vocabulary VALUES("279","es","_AUTHORIZE_NET_NOTICE","El pago Authorize.Net portal de proveedores de servicios.");
INSERT INTO phpn_vocabulary VALUES("280","de","_AUTHORIZE_NET_ORDER","Authorize.Net Auftrag");
INSERT INTO phpn_vocabulary VALUES("281","en","_AUTHORIZE_NET_ORDER","Authorize.Net Order");
INSERT INTO phpn_vocabulary VALUES("282","es","_AUTHORIZE_NET_ORDER","Authorize.Net Orden");
INSERT INTO phpn_vocabulary VALUES("283","de","_AVAILABILITY","Verfügbarkeit");
INSERT INTO phpn_vocabulary VALUES("284","en","_AVAILABILITY","Availability");
INSERT INTO phpn_vocabulary VALUES("285","es","_AVAILABILITY","Disponibilidad");
INSERT INTO phpn_vocabulary VALUES("286","de","_AVAILABILITY_ROOMS_NOTE","Definieren Sie eine maximale Anzahl von Zimmern zur Verfügung für die Buchung für einen bestimmten Tag oder den Zeitraum (maximale Verfügbarkeit _MAX_ Zimmer)<br>Um die Verfügbarkeit zu bearbeiten ändern Sie einfach den Wert in einer Zelle Tag und klicken Sie dann auf \"Änderungen speichern\"-Taste");
INSERT INTO phpn_vocabulary VALUES("287","en","_AVAILABILITY_ROOMS_NOTE","Define a maximum number of rooms available for booking for a specified day or date range (maximum availability _MAX_ rooms)<br>To edit room availability simply change the value in a day cell and then click \'Save Changes\' button");
INSERT INTO phpn_vocabulary VALUES("288","es","_AVAILABILITY_ROOMS_NOTE","Definir un número máximo de habitaciones disponibles para la reserva de un día específico o rango de fechas (máximo habitaciones _MAX_ disponibilidad)<br>Para modificar la disponibilidad de habitaciones basta con cambiar el valor de una celda día y luego haga clic en \'Guardar cambios\'");
INSERT INTO phpn_vocabulary VALUES("289","de","_AVAILABLE","verfügbar");
INSERT INTO phpn_vocabulary VALUES("290","en","_AVAILABLE","available");
INSERT INTO phpn_vocabulary VALUES("291","es","_AVAILABLE","disponible");
INSERT INTO phpn_vocabulary VALUES("292","de","_AVAILABLE_ROOMS","Freie Zimmer");
INSERT INTO phpn_vocabulary VALUES("293","en","_AVAILABLE_ROOMS","Available Rooms");
INSERT INTO phpn_vocabulary VALUES("294","es","_AVAILABLE_ROOMS","Habitaciones disponibles");
INSERT INTO phpn_vocabulary VALUES("295","de","_BACKUP","Backup");
INSERT INTO phpn_vocabulary VALUES("296","en","_BACKUP","Backup");
INSERT INTO phpn_vocabulary VALUES("297","es","_BACKUP","Reserva");
INSERT INTO phpn_vocabulary VALUES("298","de","_BACKUPS_EXISTING","Vorhandene Backups");
INSERT INTO phpn_vocabulary VALUES("299","en","_BACKUPS_EXISTING","Existing Backups");
INSERT INTO phpn_vocabulary VALUES("300","es","_BACKUPS_EXISTING","Copias de seguridad existentes");
INSERT INTO phpn_vocabulary VALUES("301","de","_BACKUP_AND_RESTORE","Backup &amp; Restore");
INSERT INTO phpn_vocabulary VALUES("302","en","_BACKUP_AND_RESTORE","Backup & Restore");
INSERT INTO phpn_vocabulary VALUES("303","es","_BACKUP_AND_RESTORE","Backup & Restore");
INSERT INTO phpn_vocabulary VALUES("304","de","_BACKUP_CHOOSE_MSG","Wählen Sie ein Backup von der Liste unten");
INSERT INTO phpn_vocabulary VALUES("305","en","_BACKUP_CHOOSE_MSG","Choose a backup from the list below");
INSERT INTO phpn_vocabulary VALUES("306","es","_BACKUP_CHOOSE_MSG","Elija una copia de seguridad de la siguiente lista");
INSERT INTO phpn_vocabulary VALUES("307","de","_BACKUP_DELETE_ALERT","Sind Sie sicher");
INSERT INTO phpn_vocabulary VALUES("308","en","_BACKUP_DELETE_ALERT","Are you sure you want to delete this backup?");
INSERT INTO phpn_vocabulary VALUES("309","es","_BACKUP_DELETE_ALERT","¿Estás seguro de que quieres eliminar esta copia de seguridad?");
INSERT INTO phpn_vocabulary VALUES("310","de","_BACKUP_EMPTY_MSG","Keine vorhandenen Backups gefunden.");
INSERT INTO phpn_vocabulary VALUES("311","en","_BACKUP_EMPTY_MSG","No existing backups found.");
INSERT INTO phpn_vocabulary VALUES("312","es","_BACKUP_EMPTY_MSG","No hay copias de seguridad todavía.");
INSERT INTO phpn_vocabulary VALUES("313","de","_BACKUP_EMPTY_NAME_ALERT","Namen der Backup-Datei darf nicht leer sein! Bitte geben Sie erneut.");
INSERT INTO phpn_vocabulary VALUES("314","en","_BACKUP_EMPTY_NAME_ALERT","Name of backup file cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("315","es","_BACKUP_EMPTY_NAME_ALERT","Nombre del archivo de copia de seguridad no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("316","de","_BACKUP_EXECUTING_ERROR","Fehler beim Backup des Systems. Bitte versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("317","en","_BACKUP_EXECUTING_ERROR","An error occurred while backup the system! Please check write permissions to backup folder or try again later.");
INSERT INTO phpn_vocabulary VALUES("318","es","_BACKUP_EXECUTING_ERROR","Se produjo un error mientras copia de seguridad del sistema! Por favor, compruebe los permisos de escritura a la carpeta de copia de seguridad o vuelva a intentarlo más tarde.");
INSERT INTO phpn_vocabulary VALUES("319","de","_BACKUP_INSTALLATION","Backup-Installation");
INSERT INTO phpn_vocabulary VALUES("320","en","_BACKUP_INSTALLATION","Backup Installation");
INSERT INTO phpn_vocabulary VALUES("321","es","_BACKUP_INSTALLATION","Instalación de copia de seguridad");
INSERT INTO phpn_vocabulary VALUES("322","de","_BACKUP_RESTORE","Backup wiederherstellen");
INSERT INTO phpn_vocabulary VALUES("323","en","_BACKUP_RESTORE","Backup Restore");
INSERT INTO phpn_vocabulary VALUES("324","es","_BACKUP_RESTORE","Restaurar copia de seguridad");
INSERT INTO phpn_vocabulary VALUES("325","de","_BACKUP_RESTORE_ALERT","Sind Sie sicher");
INSERT INTO phpn_vocabulary VALUES("326","en","_BACKUP_RESTORE_ALERT","Are you sure you want to restore this backup");
INSERT INTO phpn_vocabulary VALUES("327","es","_BACKUP_RESTORE_ALERT","¿Estás seguro de que desea restaurar esta copia de seguridad");
INSERT INTO phpn_vocabulary VALUES("328","de","_BACKUP_RESTORE_NOTE","Wiederherstellen einer früheren Backup. <b>Hinweis: Dies wird die Einstellungen löschen Sie alle aktuellen.</b>");
INSERT INTO phpn_vocabulary VALUES("329","en","_BACKUP_RESTORE_NOTE","Remember: this action will rewrite all your current settings!");
INSERT INTO phpn_vocabulary VALUES("330","es","_BACKUP_RESTORE_NOTE","Recuerde: esta acción se volverá a escribir toda la configuración actual!");
INSERT INTO phpn_vocabulary VALUES("331","de","_BACKUP_RESTORING_ERROR","Fehler beim Wiederherstellen-Datei! Bitte versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("332","en","_BACKUP_RESTORING_ERROR","An error occurred while restoring file! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("333","es","_BACKUP_RESTORING_ERROR","Se produjo un error mientras se restaura el archivo! Por favor, inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("334","de","_BACKUP_WAS_CREATED","Backup _FILE_NAME_ wurde erfolgreich erstellt.");
INSERT INTO phpn_vocabulary VALUES("335","en","_BACKUP_WAS_CREATED","Backup _FILE_NAME_ has been successfully created.");
INSERT INTO phpn_vocabulary VALUES("336","es","_BACKUP_WAS_CREATED","_FILE_NAME_ De copia de seguridad se ha creado correctamente.");
INSERT INTO phpn_vocabulary VALUES("337","de","_BACKUP_WAS_DELETED","Backup _FILE_NAME_ wurde erfolgreich gelöscht.");
INSERT INTO phpn_vocabulary VALUES("338","en","_BACKUP_WAS_DELETED","Backup _FILE_NAME_ has been successfully deleted.");
INSERT INTO phpn_vocabulary VALUES("339","es","_BACKUP_WAS_DELETED","_FILE_NAME_ De copia de seguridad ha sido borrado.");
INSERT INTO phpn_vocabulary VALUES("340","de","_BACKUP_WAS_RESTORED","Backup _FILE_NAME_ wurde erfolgreich wiederhergestellt.");
INSERT INTO phpn_vocabulary VALUES("341","en","_BACKUP_WAS_RESTORED","Backup _FILE_NAME_ has been successfully restored.");
INSERT INTO phpn_vocabulary VALUES("342","es","_BACKUP_WAS_RESTORED","_FILE_NAME_ De copia de seguridad fue restaurado con éxito.");
INSERT INTO phpn_vocabulary VALUES("343","de","_BACKUP_YOUR_INSTALLATION","Sichern Sie Ihre aktuelle Installation");
INSERT INTO phpn_vocabulary VALUES("344","en","_BACKUP_YOUR_INSTALLATION","Backup your current Installation");
INSERT INTO phpn_vocabulary VALUES("345","es","_BACKUP_YOUR_INSTALLATION","Copia de seguridad de la instalación actual");
INSERT INTO phpn_vocabulary VALUES("346","de","_BACK_TO_ADMIN_PANEL","Zurück zu Admin Panel");
INSERT INTO phpn_vocabulary VALUES("347","en","_BACK_TO_ADMIN_PANEL","Back to Admin Panel");
INSERT INTO phpn_vocabulary VALUES("348","es","_BACK_TO_ADMIN_PANEL","Volver al Panel de Administración");
INSERT INTO phpn_vocabulary VALUES("349","de","_BANK_PAYMENT_INFO","Bank Zahlungsinformationen");
INSERT INTO phpn_vocabulary VALUES("350","en","_BANK_PAYMENT_INFO","Bank Payment Information");
INSERT INTO phpn_vocabulary VALUES("351","es","_BANK_PAYMENT_INFO","Banco de Información sobre el pago");
INSERT INTO phpn_vocabulary VALUES("352","de","_BANK_TRANSFER","Banküberweisung");
INSERT INTO phpn_vocabulary VALUES("353","en","_BANK_TRANSFER","Bank Transfer");
INSERT INTO phpn_vocabulary VALUES("354","es","_BANK_TRANSFER","Transferencia bancaria");
INSERT INTO phpn_vocabulary VALUES("355","de","_BANNERS","Banner");
INSERT INTO phpn_vocabulary VALUES("356","en","_BANNERS","Banners");
INSERT INTO phpn_vocabulary VALUES("357","es","_BANNERS","Banderas");
INSERT INTO phpn_vocabulary VALUES("358","de","_BANNERS_MANAGEMENT","Banner-Management");
INSERT INTO phpn_vocabulary VALUES("359","en","_BANNERS_MANAGEMENT","Banners Management");
INSERT INTO phpn_vocabulary VALUES("360","es","_BANNERS_MANAGEMENT","Banners de Gestión");
INSERT INTO phpn_vocabulary VALUES("361","de","_BANNERS_SETTINGS","Banner-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("362","en","_BANNERS_SETTINGS","Banners Settings");
INSERT INTO phpn_vocabulary VALUES("363","es","_BANNERS_SETTINGS","Banners Configuración");
INSERT INTO phpn_vocabulary VALUES("364","de","_BANNER_IMAGE","Banner Image");
INSERT INTO phpn_vocabulary VALUES("365","en","_BANNER_IMAGE","Banner Image");
INSERT INTO phpn_vocabulary VALUES("366","es","_BANNER_IMAGE","Banner Image");
INSERT INTO phpn_vocabulary VALUES("367","de","_BAN_ITEM","Ban Posten");
INSERT INTO phpn_vocabulary VALUES("368","en","_BAN_ITEM","Ban Item");
INSERT INTO phpn_vocabulary VALUES("369","es","_BAN_ITEM","Venta de artículo");
INSERT INTO phpn_vocabulary VALUES("370","de","_BAN_LIST","Ban Liste");
INSERT INTO phpn_vocabulary VALUES("371","en","_BAN_LIST","Ban List");
INSERT INTO phpn_vocabulary VALUES("372","es","_BAN_LIST","Lista de prohibición");
INSERT INTO phpn_vocabulary VALUES("373","de","_BATHROOMS","Badezimmer");
INSERT INTO phpn_vocabulary VALUES("374","en","_BATHROOMS","Bathrooms");
INSERT INTO phpn_vocabulary VALUES("375","es","_BATHROOMS","Baños");
INSERT INTO phpn_vocabulary VALUES("376","de","_BEDS","Betten");
INSERT INTO phpn_vocabulary VALUES("377","en","_BEDS","Beds");
INSERT INTO phpn_vocabulary VALUES("378","es","_BEDS","Camas");
INSERT INTO phpn_vocabulary VALUES("379","de","_BEFORE","Vorher");
INSERT INTO phpn_vocabulary VALUES("380","en","_BEFORE","Before");
INSERT INTO phpn_vocabulary VALUES("381","es","_BEFORE","Antes");
INSERT INTO phpn_vocabulary VALUES("382","de","_BILLING_ADDRESS","Rechnungsadresse");
INSERT INTO phpn_vocabulary VALUES("383","en","_BILLING_ADDRESS","Billing Address");
INSERT INTO phpn_vocabulary VALUES("384","es","_BILLING_ADDRESS","Dirección de facturación");
INSERT INTO phpn_vocabulary VALUES("385","de","_BILLING_DETAILS","Rechnungsdetails");
INSERT INTO phpn_vocabulary VALUES("386","en","_BILLING_DETAILS","Billing Details");
INSERT INTO phpn_vocabulary VALUES("387","es","_BILLING_DETAILS","Información sobre facturación");
INSERT INTO phpn_vocabulary VALUES("388","en","_BILLING_DETAILS_UPDATED","Your Billing Details has been updated.");
INSERT INTO phpn_vocabulary VALUES("389","de","_BIRTH_DATE","Geburtsdatum");
INSERT INTO phpn_vocabulary VALUES("390","en","_BIRTH_DATE","Birth Date");
INSERT INTO phpn_vocabulary VALUES("391","es","_BIRTH_DATE","Fecha de nacimiento");
INSERT INTO phpn_vocabulary VALUES("392","de","_BIRTH_DATE_VALID_ALERT","Geburtsdatum war im falschen Format eingegeben! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("393","en","_BIRTH_DATE_VALID_ALERT","Birth date has been entered in wrong format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("394","es","_BIRTH_DATE_VALID_ALERT","Fecha de nacimiento fue inscrita en un formato incorrecto! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("395","de","_BOOK","Buch");
INSERT INTO phpn_vocabulary VALUES("396","en","_BOOK","Book");
INSERT INTO phpn_vocabulary VALUES("397","es","_BOOK","Reservar");
INSERT INTO phpn_vocabulary VALUES("398","de","_BOOKING","Buchung");
INSERT INTO phpn_vocabulary VALUES("399","en","_BOOKING","Booking");
INSERT INTO phpn_vocabulary VALUES("400","es","_BOOKING","Reserva");
INSERT INTO phpn_vocabulary VALUES("401","de","_BOOKINGS","Buchungen");
INSERT INTO phpn_vocabulary VALUES("402","en","_BOOKINGS","Bookings");
INSERT INTO phpn_vocabulary VALUES("403","es","_BOOKINGS","Reservas");
INSERT INTO phpn_vocabulary VALUES("404","de","_BOOKINGS_MANAGEMENT","Reservierungen Management");
INSERT INTO phpn_vocabulary VALUES("405","en","_BOOKINGS_MANAGEMENT","Bookings Management");
INSERT INTO phpn_vocabulary VALUES("406","es","_BOOKINGS_MANAGEMENT","Gestión de Reservas");
INSERT INTO phpn_vocabulary VALUES("407","de","_BOOKINGS_SETTINGS","Buchung Einstellungen");
INSERT INTO phpn_vocabulary VALUES("408","en","_BOOKINGS_SETTINGS","Booking Settings");
INSERT INTO phpn_vocabulary VALUES("409","es","_BOOKINGS_SETTINGS","Reservas Configuración");
INSERT INTO phpn_vocabulary VALUES("410","de","_BOOKING_CANCELED","Buchung storniert");
INSERT INTO phpn_vocabulary VALUES("411","en","_BOOKING_CANCELED","Booking Canceled");
INSERT INTO phpn_vocabulary VALUES("412","es","_BOOKING_CANCELED","Reserva cancelada");
INSERT INTO phpn_vocabulary VALUES("413","de","_BOOKING_CANCELED_SUCCESS","Die Buchung _BOOKING_ wurde erfolgreich aus dem System gelöscht!");
INSERT INTO phpn_vocabulary VALUES("414","en","_BOOKING_CANCELED_SUCCESS","The booking _BOOKING_ has been successfully canceled from the system!");
INSERT INTO phpn_vocabulary VALUES("415","es","_BOOKING_CANCELED_SUCCESS","El _BOOKING_ reserva ha sido cancelado correctamente en el sistema!");
INSERT INTO phpn_vocabulary VALUES("416","de","_BOOKING_COMPLETED","Buchung abgeschlossen");
INSERT INTO phpn_vocabulary VALUES("417","en","_BOOKING_COMPLETED","Booking Completed");
INSERT INTO phpn_vocabulary VALUES("418","es","_BOOKING_COMPLETED","Reserva Completo");
INSERT INTO phpn_vocabulary VALUES("419","de","_BOOKING_DATE","Reservierungsdatum");
INSERT INTO phpn_vocabulary VALUES("420","en","_BOOKING_DATE","Booking Date");
INSERT INTO phpn_vocabulary VALUES("421","es","_BOOKING_DATE","Reservas Fecha");
INSERT INTO phpn_vocabulary VALUES("422","de","_BOOKING_DESCRIPTION","Reservieren Beschreibung");
INSERT INTO phpn_vocabulary VALUES("423","en","_BOOKING_DESCRIPTION","Booking Description");
INSERT INTO phpn_vocabulary VALUES("424","es","_BOOKING_DESCRIPTION","Reservas Descripción");
INSERT INTO phpn_vocabulary VALUES("425","de","_BOOKING_DETAILS","Buchung Details");
INSERT INTO phpn_vocabulary VALUES("426","en","_BOOKING_DETAILS","Booking Details");
INSERT INTO phpn_vocabulary VALUES("427","es","_BOOKING_DETAILS","Datos de la Reserva");
INSERT INTO phpn_vocabulary VALUES("428","de","_BOOKING_NUMBER","Buchungsnummer");
INSERT INTO phpn_vocabulary VALUES("429","en","_BOOKING_NUMBER","Booking Number");
INSERT INTO phpn_vocabulary VALUES("430","es","_BOOKING_NUMBER","Número de reserva");
INSERT INTO phpn_vocabulary VALUES("431","de","_BOOKING_PRICE","Buchung Preis");
INSERT INTO phpn_vocabulary VALUES("432","en","_BOOKING_PRICE","Booking Price");
INSERT INTO phpn_vocabulary VALUES("433","es","_BOOKING_PRICE","Precio de reserva");
INSERT INTO phpn_vocabulary VALUES("434","de","_BOOKING_SETTINGS","Buchung Einstellungen");
INSERT INTO phpn_vocabulary VALUES("435","en","_BOOKING_SETTINGS","Booking Settings");
INSERT INTO phpn_vocabulary VALUES("436","es","_BOOKING_SETTINGS","Ajustes de reservas");
INSERT INTO phpn_vocabulary VALUES("437","de","_BOOKING_STATUS","Buchungsstand");
INSERT INTO phpn_vocabulary VALUES("438","en","_BOOKING_STATUS","Booking Status");
INSERT INTO phpn_vocabulary VALUES("439","es","_BOOKING_STATUS","Estado de la reservación");
INSERT INTO phpn_vocabulary VALUES("440","de","_BOOKING_SUBTOTAL","Buchung Zwischensumme");
INSERT INTO phpn_vocabulary VALUES("441","en","_BOOKING_SUBTOTAL","Booking Subtotal");
INSERT INTO phpn_vocabulary VALUES("442","es","_BOOKING_SUBTOTAL","Reservas Subtotal");
INSERT INTO phpn_vocabulary VALUES("443","de","_BOOKING_WAS_CANCELED_MSG","Ihre Buchung wurde storniert.");
INSERT INTO phpn_vocabulary VALUES("444","en","_BOOKING_WAS_CANCELED_MSG","Your booking has been canceled.");
INSERT INTO phpn_vocabulary VALUES("445","es","_BOOKING_WAS_CANCELED_MSG","Su reserva ha sido cancelada.");
INSERT INTO phpn_vocabulary VALUES("446","de","_BOOKING_WAS_COMPLETED_MSG","Vielen Dank für die Reservierung Zimmer in unserem Hotel! Ihre Buchung ist abgeschlossen.");
INSERT INTO phpn_vocabulary VALUES("447","en","_BOOKING_WAS_COMPLETED_MSG","Thank you for reservation rooms in our hotel! Your booking has been completed.");
INSERT INTO phpn_vocabulary VALUES("448","es","_BOOKING_WAS_COMPLETED_MSG","Gracias por habitaciones reserva en nuestro hotel! Su reserva se ha completado.");
INSERT INTO phpn_vocabulary VALUES("449","de","_BOOK_NOW","Buchen Sie jetzt");
INSERT INTO phpn_vocabulary VALUES("450","en","_BOOK_NOW","Book Now");
INSERT INTO phpn_vocabulary VALUES("451","es","_BOOK_NOW","Reserve ahora");
INSERT INTO phpn_vocabulary VALUES("452","de","_BOOK_ONE_NIGHT_ALERT","Sorry, aber Sie werden muss mindestens eine Nacht.");
INSERT INTO phpn_vocabulary VALUES("453","en","_BOOK_ONE_NIGHT_ALERT","Sorry, but you must book at least one night.");
INSERT INTO phpn_vocabulary VALUES("454","es","_BOOK_ONE_NIGHT_ALERT","Lo sentimos, pero hay que reservar al menos una noche.");
INSERT INTO phpn_vocabulary VALUES("455","de","_BOTTOM","Boden");
INSERT INTO phpn_vocabulary VALUES("456","en","_BOTTOM","Bottom");
INSERT INTO phpn_vocabulary VALUES("457","es","_BOTTOM","Inferior");
INSERT INTO phpn_vocabulary VALUES("458","de","_BUTTON_BACK","Zurück");
INSERT INTO phpn_vocabulary VALUES("459","en","_BUTTON_BACK","Back");
INSERT INTO phpn_vocabulary VALUES("460","es","_BUTTON_BACK","Espalda");
INSERT INTO phpn_vocabulary VALUES("461","de","_BUTTON_CANCEL","Abbrechen");
INSERT INTO phpn_vocabulary VALUES("462","en","_BUTTON_CANCEL","Cancel");
INSERT INTO phpn_vocabulary VALUES("463","es","_BUTTON_CANCEL","Cancelar");
INSERT INTO phpn_vocabulary VALUES("464","de","_BUTTON_CHANGE","Ändern");
INSERT INTO phpn_vocabulary VALUES("465","en","_BUTTON_CHANGE","Change");
INSERT INTO phpn_vocabulary VALUES("466","es","_BUTTON_CHANGE","Cambiar");
INSERT INTO phpn_vocabulary VALUES("467","de","_BUTTON_CHANGE_PASSWORD","Passwort ändern");
INSERT INTO phpn_vocabulary VALUES("468","en","_BUTTON_CHANGE_PASSWORD","Change Password");
INSERT INTO phpn_vocabulary VALUES("469","es","_BUTTON_CHANGE_PASSWORD","Cambiar Contraseña");
INSERT INTO phpn_vocabulary VALUES("470","de","_BUTTON_CREATE","Erstellen");
INSERT INTO phpn_vocabulary VALUES("471","en","_BUTTON_CREATE","Create");
INSERT INTO phpn_vocabulary VALUES("472","es","_BUTTON_CREATE","Crear");
INSERT INTO phpn_vocabulary VALUES("473","de","_BUTTON_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("474","en","_BUTTON_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("475","es","_BUTTON_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("476","de","_BUTTON_LOGOUT","Abmelden");
INSERT INTO phpn_vocabulary VALUES("477","en","_BUTTON_LOGOUT","Logout");
INSERT INTO phpn_vocabulary VALUES("478","es","_BUTTON_LOGOUT","Cerrar sesión");
INSERT INTO phpn_vocabulary VALUES("479","de","_BUTTON_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("480","en","_BUTTON_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("481","es","_BUTTON_RESET","Perdí");
INSERT INTO phpn_vocabulary VALUES("482","de","_BUTTON_REWRITE","Rewrite Wortschatz");
INSERT INTO phpn_vocabulary VALUES("483","en","_BUTTON_REWRITE","Rewrite Vocabulary");
INSERT INTO phpn_vocabulary VALUES("484","es","_BUTTON_REWRITE","Vuelva a escribir Vocabulario");
INSERT INTO phpn_vocabulary VALUES("485","de","_BUTTON_SAVE_CHANGES","Änderungen speichern");
INSERT INTO phpn_vocabulary VALUES("486","en","_BUTTON_SAVE_CHANGES","Save Changes");
INSERT INTO phpn_vocabulary VALUES("487","es","_BUTTON_SAVE_CHANGES","Guardar cambios");
INSERT INTO phpn_vocabulary VALUES("488","de","_BUTTON_UPDATE","Update");
INSERT INTO phpn_vocabulary VALUES("489","en","_BUTTON_UPDATE","Update");
INSERT INTO phpn_vocabulary VALUES("490","es","_BUTTON_UPDATE","Actualizar");
INSERT INTO phpn_vocabulary VALUES("491","de","_CACHE_LIFETIME","Cache Lifetime");
INSERT INTO phpn_vocabulary VALUES("492","en","_CACHE_LIFETIME","Cache Lifetime");
INSERT INTO phpn_vocabulary VALUES("493","es","_CACHE_LIFETIME","Cache de por vida");
INSERT INTO phpn_vocabulary VALUES("494","de","_CACHING","Caching");
INSERT INTO phpn_vocabulary VALUES("495","en","_CACHING","Caching");
INSERT INTO phpn_vocabulary VALUES("496","es","_CACHING","Caché");
INSERT INTO phpn_vocabulary VALUES("497","de","_CAMPAIGNS","Kampagnen");
INSERT INTO phpn_vocabulary VALUES("498","en","_CAMPAIGNS","Campaigns");
INSERT INTO phpn_vocabulary VALUES("499","es","_CAMPAIGNS","Campañas");
INSERT INTO phpn_vocabulary VALUES("500","de","_CAMPAIGNS_MANAGEMENT","Kampagnen-Management");
INSERT INTO phpn_vocabulary VALUES("501","en","_CAMPAIGNS_MANAGEMENT","Campaigns Management");
INSERT INTO phpn_vocabulary VALUES("502","es","_CAMPAIGNS_MANAGEMENT","Campañas Gestión");
INSERT INTO phpn_vocabulary VALUES("503","de","_CAMPAIGNS_TOOLTIP","Global - ermöglicht die Buchung für ein beliebiges Datum und läuft (sichtbar) innerhalb eines definierten Zeitraums nur \n\n Gezielt - ermöglicht den Kauf von Produkten / Dienstleistungen in bestimmten Zeitraum nur und läuft (sichtbar), bis dieser Termin beginnt");
INSERT INTO phpn_vocabulary VALUES("504","en","_CAMPAIGNS_TOOLTIP","Global - allows booking for any date and runs (visible) within a defined period of time only\n\nTargeted - allows booking in a specified period of time only and runs (visible) till the first date is beginning");
INSERT INTO phpn_vocabulary VALUES("505","es","_CAMPAIGNS_TOOLTIP","Global - permite reservar para cualquier fecha y corre (visible) en un plazo de tiempo definido, sólo se \n\n Dirigido - permite la compra de productos y servicios en el período de tiempo determinado y sólo se ejecuta (visible), hasta esta fecha se inicia");
INSERT INTO phpn_vocabulary VALUES("506","de","_CANCELED","Storniert");
INSERT INTO phpn_vocabulary VALUES("507","en","_CANCELED","Canceled");
INSERT INTO phpn_vocabulary VALUES("508","es","_CANCELED","Cancelado");
INSERT INTO phpn_vocabulary VALUES("509","de","_CANCELED_BY_ADMIN","Diese Buchung wurde durch den Administrator erfolgen.");
INSERT INTO phpn_vocabulary VALUES("510","en","_CANCELED_BY_ADMIN","This booking is canceled by administrator.");
INSERT INTO phpn_vocabulary VALUES("511","es","_CANCELED_BY_ADMIN","Esta reserva fue cancelada por el administrador.");
INSERT INTO phpn_vocabulary VALUES("512","de","_CANCELED_BY_CUSTOMER","Diese Buchung wurde vom Kunden abgebrochen.");
INSERT INTO phpn_vocabulary VALUES("513","en","_CANCELED_BY_CUSTOMER","This booking has been canceled by customer.");
INSERT INTO phpn_vocabulary VALUES("514","es","_CANCELED_BY_CUSTOMER","Esta reserva fue cancelada por el cliente.");
INSERT INTO phpn_vocabulary VALUES("515","de","_CAN_USE_TAGS_MSG","Sie können einige HTML-Tags wie");
INSERT INTO phpn_vocabulary VALUES("516","en","_CAN_USE_TAGS_MSG","You can use some HTML tags, such as");
INSERT INTO phpn_vocabulary VALUES("517","es","_CAN_USE_TAGS_MSG","Puede utilizar las etiquetas HTML, como");
INSERT INTO phpn_vocabulary VALUES("518","de","_CAPACITY","Kapazität");
INSERT INTO phpn_vocabulary VALUES("519","en","_CAPACITY","Capacity");
INSERT INTO phpn_vocabulary VALUES("520","es","_CAPACITY","Capacidad");
INSERT INTO phpn_vocabulary VALUES("521","de","_CART_WAS_UPDATED","Reservierung Warenkorb wurde erfolgreich aktualisiert!");
INSERT INTO phpn_vocabulary VALUES("522","en","_CART_WAS_UPDATED","Reservation cart has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("523","es","_CART_WAS_UPDATED","Carro de Reserva se ha actualizado con éxito!");
INSERT INTO phpn_vocabulary VALUES("524","de","_CATEGORIES","Kategorien");
INSERT INTO phpn_vocabulary VALUES("525","en","_CATEGORIES","Categories");
INSERT INTO phpn_vocabulary VALUES("526","es","_CATEGORIES","Categorías");
INSERT INTO phpn_vocabulary VALUES("527","de","_CATEGORIES_MANAGEMENT","Kategorien Management");
INSERT INTO phpn_vocabulary VALUES("528","en","_CATEGORIES_MANAGEMENT","Categories Management");
INSERT INTO phpn_vocabulary VALUES("529","es","_CATEGORIES_MANAGEMENT","Categorías de Manejo");
INSERT INTO phpn_vocabulary VALUES("530","de","_CATEGORY","Kategorie");
INSERT INTO phpn_vocabulary VALUES("531","en","_CATEGORY","Category");
INSERT INTO phpn_vocabulary VALUES("532","es","_CATEGORY","Categoría");
INSERT INTO phpn_vocabulary VALUES("533","de","_CATEGORY_DESCRIPTION","Kategorie Beschreibung");
INSERT INTO phpn_vocabulary VALUES("534","en","_CATEGORY_DESCRIPTION","Category Description");
INSERT INTO phpn_vocabulary VALUES("535","es","_CATEGORY_DESCRIPTION","Categoría Descripción");
INSERT INTO phpn_vocabulary VALUES("536","de","_CC_CARD_HOLDER_NAME_EMPTY","No Name des Karteninhabers versehen! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("537","en","_CC_CARD_HOLDER_NAME_EMPTY","No card holder\'s name provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("538","es","_CC_CARD_HOLDER_NAME_EMPTY","No Nombre del titular de la tarjeta siempre! Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("539","de","_CC_CARD_INVALID_FORMAT","Kreditkarten-Nummer hat ungültiges Format! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("540","en","_CC_CARD_INVALID_FORMAT","Credit card number has invalid format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("541","es","_CC_CARD_INVALID_FORMAT","Número de tarjeta de crédito tiene un formato válido! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("542","de","_CC_CARD_INVALID_NUMBER","Kreditkarten-Nummer ist ungültig! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("543","en","_CC_CARD_INVALID_NUMBER","Credit card number is invalid! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("544","es","_CC_CARD_INVALID_NUMBER","Número de tarjeta de crédito no es válido! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("545","de","_CC_CARD_NO_CVV_NUMBER","Keine CVV Code zur Verfügung gestellt! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("546","en","_CC_CARD_NO_CVV_NUMBER","No CVV Code provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("547","es","_CC_CARD_NO_CVV_NUMBER","No Código CVV siempre! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("548","de","_CC_CARD_WRONG_EXPIRE_DATE","Kreditkarten Ablaufdatum ist falsch! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("549","en","_CC_CARD_WRONG_EXPIRE_DATE","Credit card expiry date is wrong! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("550","es","_CC_CARD_WRONG_EXPIRE_DATE","Tarjeta de crédito fecha de caducidad está mal! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("551","de","_CC_CARD_WRONG_LENGTH","Kreditkarten-Nummer falsch ist lang! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("552","en","_CC_CARD_WRONG_LENGTH","Credit card number has a wrong length! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("553","es","_CC_CARD_WRONG_LENGTH","Número de tarjeta de crédito es la longitud del mal! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("554","de","_CC_NO_CARD_NUMBER_PROVIDED","Keine Karte Nummer versehen! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("555","en","_CC_NO_CARD_NUMBER_PROVIDED","No card number provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("556","es","_CC_NO_CARD_NUMBER_PROVIDED","No hay número de tarjeta de siempre! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("557","de","_CC_NUMBER_INVALID","Kreditkarten-Nummer ist ungültig! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("558","en","_CC_NUMBER_INVALID","Credit card number is invalid! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("559","es","_CC_NUMBER_INVALID","Número de tarjeta de crédito no es válido! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("560","de","_CC_UNKNOWN_CARD_TYPE","Unbekannte Karte Typ! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("561","en","_CC_UNKNOWN_CARD_TYPE","Unknown card type! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("562","es","_CC_UNKNOWN_CARD_TYPE","Tipo de tarjeta desconocido! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("563","de","_CHANGES_SAVED","Änderungen wurden gespeichert.");
INSERT INTO phpn_vocabulary VALUES("564","en","_CHANGES_SAVED","Changes were saved.");
INSERT INTO phpn_vocabulary VALUES("565","es","_CHANGES_SAVED","Los cambios se salvaron.");
INSERT INTO phpn_vocabulary VALUES("566","de","_CHANGES_WERE_SAVED","Änderungen wurden erfolgreich gespeichert!");
INSERT INTO phpn_vocabulary VALUES("567","en","_CHANGES_WERE_SAVED","Changes were successfully saved! Please refresh the <a href=index.php>Home Page</a> to see the results.");
INSERT INTO phpn_vocabulary VALUES("568","es","_CHANGES_WERE_SAVED","Los cambios se han guardado correctamente! Por favor, actualice la <a href=index.php>Home Page</a> para ver los resultados.");
INSERT INTO phpn_vocabulary VALUES("569","de","_CHANGE_CUSTOMER","ändern Sie Kunde");
INSERT INTO phpn_vocabulary VALUES("570","en","_CHANGE_CUSTOMER","Change Customer");
INSERT INTO phpn_vocabulary VALUES("571","es","_CHANGE_CUSTOMER","Cambiar al Cliente");
INSERT INTO phpn_vocabulary VALUES("572","de","_CHANGE_ORDER","Auftrag ändern");
INSERT INTO phpn_vocabulary VALUES("573","en","_CHANGE_ORDER","Change Order");
INSERT INTO phpn_vocabulary VALUES("574","es","_CHANGE_ORDER","Cambiar el orden de");
INSERT INTO phpn_vocabulary VALUES("575","de","_CHANGE_YOUR_PASSWORD","Ändern Sie Ihr Passwort");
INSERT INTO phpn_vocabulary VALUES("576","en","_CHANGE_YOUR_PASSWORD","Change your password");
INSERT INTO phpn_vocabulary VALUES("577","es","_CHANGE_YOUR_PASSWORD","Cambie su contraseña");
INSERT INTO phpn_vocabulary VALUES("578","de","_CHARGE_TYPE","Ladungstyp");
INSERT INTO phpn_vocabulary VALUES("579","en","_CHARGE_TYPE","Charge Type");
INSERT INTO phpn_vocabulary VALUES("580","es","_CHARGE_TYPE","Tipo Carga");
INSERT INTO phpn_vocabulary VALUES("581","de","_CHECKOUT","Kasse");
INSERT INTO phpn_vocabulary VALUES("582","en","_CHECKOUT","Checkout");
INSERT INTO phpn_vocabulary VALUES("583","es","_CHECKOUT","Caja");
INSERT INTO phpn_vocabulary VALUES("584","de","_CHECK_AVAILABILITY","Überprüfen Zimmer");
INSERT INTO phpn_vocabulary VALUES("585","en","_CHECK_AVAILABILITY","Check Availability");
INSERT INTO phpn_vocabulary VALUES("586","es","_CHECK_AVAILABILITY","Obtener la disponibilidad");
INSERT INTO phpn_vocabulary VALUES("587","de","_CHECK_IN","Check In");
INSERT INTO phpn_vocabulary VALUES("588","en","_CHECK_IN","Check In");
INSERT INTO phpn_vocabulary VALUES("589","es","_CHECK_IN","Registrarse");
INSERT INTO phpn_vocabulary VALUES("590","de","_CHECK_NOW","Jetzt prüfen");
INSERT INTO phpn_vocabulary VALUES("591","en","_CHECK_NOW","Check Now");
INSERT INTO phpn_vocabulary VALUES("592","es","_CHECK_NOW","Comprobar ahora");
INSERT INTO phpn_vocabulary VALUES("593","de","_CHECK_OUT","Check Out");
INSERT INTO phpn_vocabulary VALUES("594","en","_CHECK_OUT","Check Out");
INSERT INTO phpn_vocabulary VALUES("595","es","_CHECK_OUT","Hora de salida");
INSERT INTO phpn_vocabulary VALUES("596","de","_CHECK_STATUS","Status prüfen");
INSERT INTO phpn_vocabulary VALUES("597","en","_CHECK_STATUS","Check Status");
INSERT INTO phpn_vocabulary VALUES("598","es","_CHECK_STATUS","Compruebe el estado de");
INSERT INTO phpn_vocabulary VALUES("599","de","_CHILD","Kind");
INSERT INTO phpn_vocabulary VALUES("600","en","_CHILD","Child");
INSERT INTO phpn_vocabulary VALUES("601","es","_CHILD","Niño");
INSERT INTO phpn_vocabulary VALUES("602","de","_CHILDREN","Kinder");
INSERT INTO phpn_vocabulary VALUES("603","en","_CHILDREN","Children");
INSERT INTO phpn_vocabulary VALUES("604","es","_CHILDREN","Niños");
INSERT INTO phpn_vocabulary VALUES("605","de","_CITY","Stadt");
INSERT INTO phpn_vocabulary VALUES("606","en","_CITY","City");
INSERT INTO phpn_vocabulary VALUES("607","es","_CITY","Ciudad");
INSERT INTO phpn_vocabulary VALUES("608","de","_CITY_EMPTY_ALERT","Stadt Darf Nicht Sein leer! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("609","en","_CITY_EMPTY_ALERT","City cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("610","es","_CITY_EMPTY_ALERT","Ciudad no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("611","de","_CLEANED","gereinigt");
INSERT INTO phpn_vocabulary VALUES("612","en","_CLEANED","Cleaned");
INSERT INTO phpn_vocabulary VALUES("613","es","_CLEANED","Limpiar");
INSERT INTO phpn_vocabulary VALUES("614","de","_CLEANUP","Aufräumarbeiten");
INSERT INTO phpn_vocabulary VALUES("615","en","_CLEANUP","Cleanup");
INSERT INTO phpn_vocabulary VALUES("616","es","_CLEANUP","limpieza");
INSERT INTO phpn_vocabulary VALUES("617","de","_CLEANUP_TOOLTIP","Die Bereinigung Funktion wird verwendet, um ausstehende (temporären) Vorbehalte von Ihrer Website zu entfernen. Eine ausstehende Reservierung ist eine, wo das System wartet auf das Payment-Gateway mit der Transaktion wird auf Status-Callback.");
INSERT INTO phpn_vocabulary VALUES("618","en","_CLEANUP_TOOLTIP","The cleanup feature is used to remove pending (temporary) reservations from your web site. A pending reservation is one where the system is waiting for the payment gateway to callback with the transaction status.");
INSERT INTO phpn_vocabulary VALUES("619","es","_CLEANUP_TOOLTIP","La característica de limpieza se utiliza para eliminar pendientes (temporal) de las reservas de su sitio web. Una reserva en espera es aquel en el que el sistema está a la espera de la pasarela de pago de devolución de llamada con el estado de la transacción.");
INSERT INTO phpn_vocabulary VALUES("620","de","_CLEAN_CACHE","Cache löschen");
INSERT INTO phpn_vocabulary VALUES("621","en","_CLEAN_CACHE","Clean Cache");
INSERT INTO phpn_vocabulary VALUES("622","es","_CLEAN_CACHE","Borrar caché");
INSERT INTO phpn_vocabulary VALUES("623","de","_CLICK_FOR_MORE_INFO","Klicken Sie für weitere Informationen");
INSERT INTO phpn_vocabulary VALUES("624","en","_CLICK_FOR_MORE_INFO","Click for more information");
INSERT INTO phpn_vocabulary VALUES("625","es","_CLICK_FOR_MORE_INFO","Haga clic para más información");
INSERT INTO phpn_vocabulary VALUES("626","de","_CLICK_TO_COPY","Klicken Sie zum Kopieren");
INSERT INTO phpn_vocabulary VALUES("627","en","_CLICK_TO_COPY","Click to copy");
INSERT INTO phpn_vocabulary VALUES("628","es","_CLICK_TO_COPY","Haga clic para copiar");
INSERT INTO phpn_vocabulary VALUES("629","de","_CLICK_TO_EDIT","Klicken Sie zum Bearbeiten");
INSERT INTO phpn_vocabulary VALUES("630","en","_CLICK_TO_EDIT","Click to edit");
INSERT INTO phpn_vocabulary VALUES("631","es","_CLICK_TO_EDIT","Haga clic para editar");
INSERT INTO phpn_vocabulary VALUES("632","de","_CLICK_TO_INCREASE","Klicken zum Vergrößern");
INSERT INTO phpn_vocabulary VALUES("633","en","_CLICK_TO_INCREASE","Click to enlarge");
INSERT INTO phpn_vocabulary VALUES("634","es","_CLICK_TO_INCREASE","Haga clic para aumentar");
INSERT INTO phpn_vocabulary VALUES("635","de","_CLICK_TO_MANAGE","Klicken Sie auf die Verwaltung");
INSERT INTO phpn_vocabulary VALUES("636","en","_CLICK_TO_MANAGE","Click to manage");
INSERT INTO phpn_vocabulary VALUES("637","es","_CLICK_TO_MANAGE","Haga clic para gestionar");
INSERT INTO phpn_vocabulary VALUES("638","de","_CLICK_TO_SEE_DESCR","Hier klicken Beschreibung siehe");
INSERT INTO phpn_vocabulary VALUES("639","en","_CLICK_TO_SEE_DESCR","Click to see description");
INSERT INTO phpn_vocabulary VALUES("640","es","_CLICK_TO_SEE_DESCR","Haz clic aquí para ver la descripción");
INSERT INTO phpn_vocabulary VALUES("641","de","_CLICK_TO_SEE_PRICES","Hier klicken für Preisanzeige");
INSERT INTO phpn_vocabulary VALUES("642","en","_CLICK_TO_SEE_PRICES","Click to see prices");
INSERT INTO phpn_vocabulary VALUES("643","es","_CLICK_TO_SEE_PRICES","Haz clic aquí para ver los precios");
INSERT INTO phpn_vocabulary VALUES("644","de","_CLICK_TO_VIEW","Klicken Sie zur Ansicht");
INSERT INTO phpn_vocabulary VALUES("645","en","_CLICK_TO_VIEW","Click to view");
INSERT INTO phpn_vocabulary VALUES("646","es","_CLICK_TO_VIEW","Haga clic para ver");
INSERT INTO phpn_vocabulary VALUES("647","de","_CLOSE","Schließen");
INSERT INTO phpn_vocabulary VALUES("648","en","_CLOSE","Close");
INSERT INTO phpn_vocabulary VALUES("649","es","_CLOSE","Cerrar");
INSERT INTO phpn_vocabulary VALUES("650","de","_CLOSE_META_TAGS","Schließen Sie META-Tags");
INSERT INTO phpn_vocabulary VALUES("651","en","_CLOSE_META_TAGS","Close META tags");
INSERT INTO phpn_vocabulary VALUES("652","es","_CLOSE_META_TAGS","Cerrar etiquetas META");
INSERT INTO phpn_vocabulary VALUES("653","de","_CODE","Code");
INSERT INTO phpn_vocabulary VALUES("654","en","_CODE","Code");
INSERT INTO phpn_vocabulary VALUES("655","es","_CODE","Código");
INSERT INTO phpn_vocabulary VALUES("656","de","_COLLAPSE_PANEL","Collapse Navigationsleiste");
INSERT INTO phpn_vocabulary VALUES("657","en","_COLLAPSE_PANEL","Collapse navigation panel");
INSERT INTO phpn_vocabulary VALUES("658","es","_COLLAPSE_PANEL","Colapso del panel de navegación");
INSERT INTO phpn_vocabulary VALUES("659","de","_COMMENTS","Kommentare");
INSERT INTO phpn_vocabulary VALUES("660","en","_COMMENTS","Comments");
INSERT INTO phpn_vocabulary VALUES("661","es","_COMMENTS","Comentarios");
INSERT INTO phpn_vocabulary VALUES("662","de","_COMMENTS_AWAITING_MODERATION_ALERT","Es gibt _COUNT_ Kommentar/s warten auf Ihren Maßen. Klicken Sie <a href=\'index.php?admin=mod_comments_management\'>hier</a> für eine Bewertung.");
INSERT INTO phpn_vocabulary VALUES("663","en","_COMMENTS_AWAITING_MODERATION_ALERT","There are _COUNT_ comment/s awaiting your moderation. Click <a href=\'index.php?admin=mod_comments_management\'>here</a> for review.");
INSERT INTO phpn_vocabulary VALUES("664","es","_COMMENTS_AWAITING_MODERATION_ALERT","Hay comentarios _COUNT_ en espera de su moderación. Haga clic <a href=\'index.php?admin=mod_comments_management\'>aquí</a> para su revisión.");
INSERT INTO phpn_vocabulary VALUES("665","de","_COMMENTS_LINK","Kommentare (_COUNT_)");
INSERT INTO phpn_vocabulary VALUES("666","en","_COMMENTS_LINK","Comments (_COUNT_)");
INSERT INTO phpn_vocabulary VALUES("667","es","_COMMENTS_LINK","Comentarios (_COUNT_)");
INSERT INTO phpn_vocabulary VALUES("668","de","_COMMENTS_MANAGEMENT","Kommentare Management");
INSERT INTO phpn_vocabulary VALUES("669","en","_COMMENTS_MANAGEMENT","Comments Management");
INSERT INTO phpn_vocabulary VALUES("670","es","_COMMENTS_MANAGEMENT","Observaciones de la Administración");
INSERT INTO phpn_vocabulary VALUES("671","de","_COMMENTS_SETTINGS","Kommentare Einstellungen");
INSERT INTO phpn_vocabulary VALUES("672","en","_COMMENTS_SETTINGS","Comments Settings");
INSERT INTO phpn_vocabulary VALUES("673","es","_COMMENTS_SETTINGS","Comentarios Configuración");
INSERT INTO phpn_vocabulary VALUES("674","de","_COMMENT_DELETED_SUCCESS","Dein Kommentar wurde erfolgreich gelöscht.");
INSERT INTO phpn_vocabulary VALUES("675","en","_COMMENT_DELETED_SUCCESS","Your comment has been successfully deleted.");
INSERT INTO phpn_vocabulary VALUES("676","es","_COMMENT_DELETED_SUCCESS","Su comentario ha sido borrado.");
INSERT INTO phpn_vocabulary VALUES("677","de","_COMMENT_LENGTH_ALERT","Die Länge der Kommentar muss kleiner sein als _LENGTH_ Zeichen!");
INSERT INTO phpn_vocabulary VALUES("678","en","_COMMENT_LENGTH_ALERT","The length of comment must be less than _LENGTH_ characters!");
INSERT INTO phpn_vocabulary VALUES("679","es","_COMMENT_LENGTH_ALERT","La longitud de los comentarios debe ser inferior a _LENGTH_ personajes!");
INSERT INTO phpn_vocabulary VALUES("680","de","_COMMENT_POSTED_SUCCESS","Dein Kommentar wurde erfolgreich auf dem Laufenden!");
INSERT INTO phpn_vocabulary VALUES("681","en","_COMMENT_POSTED_SUCCESS","Your comment has been successfully posted!");
INSERT INTO phpn_vocabulary VALUES("682","es","_COMMENT_POSTED_SUCCESS","Tu comentario ha sido enviado con éxito!");
INSERT INTO phpn_vocabulary VALUES("683","de","_COMMENT_SUBMITTED_SUCCESS","Dein Kommentar wurde Erfolgreich übermittelt und WIRD nach Administrator-Beitrag gepostet Werden!");
INSERT INTO phpn_vocabulary VALUES("684","en","_COMMENT_SUBMITTED_SUCCESS","Your comment has been successfully submitted and will be posted after administrator\'s review!");
INSERT INTO phpn_vocabulary VALUES("685","es","_COMMENT_SUBMITTED_SUCCESS","Tu comentario ha sido enviado correctamente y será publicado después de la revisión del administrador!");
INSERT INTO phpn_vocabulary VALUES("686","de","_COMMENT_TEXT","Kommentieren Text");
INSERT INTO phpn_vocabulary VALUES("687","en","_COMMENT_TEXT","Comment text");
INSERT INTO phpn_vocabulary VALUES("688","es","_COMMENT_TEXT","Comentario de texto");
INSERT INTO phpn_vocabulary VALUES("689","de","_COMPANY","Gesellschaft");
INSERT INTO phpn_vocabulary VALUES("690","en","_COMPANY","Company");
INSERT INTO phpn_vocabulary VALUES("691","es","_COMPANY","Compañía");
INSERT INTO phpn_vocabulary VALUES("692","de","_COMPLETED","Fertiggestellt (gegen Entgelt)");
INSERT INTO phpn_vocabulary VALUES("693","en","_COMPLETED","Completed (Paid)");
INSERT INTO phpn_vocabulary VALUES("694","es","_COMPLETED","Terminado (pagado)");
INSERT INTO phpn_vocabulary VALUES("695","de","_CONFIRMATION","Bestätigung");
INSERT INTO phpn_vocabulary VALUES("696","en","_CONFIRMATION","Confirmation");
INSERT INTO phpn_vocabulary VALUES("697","es","_CONFIRMATION","Confirmación");
INSERT INTO phpn_vocabulary VALUES("698","de","_CONFIRMATION_CODE","Bestätigungs-Code");
INSERT INTO phpn_vocabulary VALUES("699","en","_CONFIRMATION_CODE","Confirmation Code");
INSERT INTO phpn_vocabulary VALUES("700","es","_CONFIRMATION_CODE","Código de confirmación");
INSERT INTO phpn_vocabulary VALUES("701","de","_CONFIRMED_ALREADY_MSG","Ihr Konto wurde bereits bestätigt! <br /><br />Klicken Sie <a href=\'index.php?customer=login\'>hier</a>, um fortzufahren.");
INSERT INTO phpn_vocabulary VALUES("702","en","_CONFIRMED_ALREADY_MSG","Your account has already been confirmed! <br /><br />Click <a href=\'index.php?customer=login\'>here</a> to continue.");
INSERT INTO phpn_vocabulary VALUES("703","es","_CONFIRMED_ALREADY_MSG","Su cuenta ha sido ya confirmado!<br /><br />Haga clic <a href=\'index.php?customer=login\'>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("704","de","_CONFIRMED_SUCCESS_MSG","Vielen Dank für Ihre Anmeldung bestätigt! <br /><br /> Sie können jetzt in Ihr Konto einloggen. Klicken Sie <a href=\'index.php?customer=login\'>hier</a>, um fortzufahren.");
INSERT INTO phpn_vocabulary VALUES("705","en","_CONFIRMED_SUCCESS_MSG","Thank you for confirming your registration! <br /><br />You may now log into your account. Click <a href=\'index.php?customer=login\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("706","es","_CONFIRMED_SUCCESS_MSG","Gracias por confirmar su inscripción! <br /><br />Ahora puede acceder a su cuenta. Haga clic <a href=\'index.php?customer=login\'>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("707","de","_CONFIRM_PASSWORD","Kennwort bestätigen");
INSERT INTO phpn_vocabulary VALUES("708","en","_CONFIRM_PASSWORD","Confirm Password");
INSERT INTO phpn_vocabulary VALUES("709","es","_CONFIRM_PASSWORD","confirmar contraseña");
INSERT INTO phpn_vocabulary VALUES("710","de","_CONFIRM_TERMS_CONDITIONS","Sie müssen bestätigen, stimmen Sie unseren AGB!");
INSERT INTO phpn_vocabulary VALUES("711","en","_CONFIRM_TERMS_CONDITIONS","You must confirm you agree to our Terms & Conditions!");
INSERT INTO phpn_vocabulary VALUES("712","es","_CONFIRM_TERMS_CONDITIONS","Usted debe confirmar que está de acuerdo con nuestros términos y condiciones!");
INSERT INTO phpn_vocabulary VALUES("713","de","_CONF_PASSWORD_IS_EMPTY","Kennwort bestätigen können nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("714","en","_CONF_PASSWORD_IS_EMPTY","Confirm Password cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("715","es","_CONF_PASSWORD_IS_EMPTY","Confirmar contraseña no puede estar vacío!");
INSERT INTO phpn_vocabulary VALUES("716","de","_CONF_PASSWORD_MATCH","Das Passwort muss übereinstimmen mit Kennwort bestätigen werden");
INSERT INTO phpn_vocabulary VALUES("717","en","_CONF_PASSWORD_MATCH","Password must be match with Confirm Password");
INSERT INTO phpn_vocabulary VALUES("718","es","_CONF_PASSWORD_MATCH","La contraseña debe tener partido con Confirmar contraseña");
INSERT INTO phpn_vocabulary VALUES("719","de","_CONTACTUS_DEFAULT_EMAIL_ALERT","Sie haben zum ursprünglichen E-Mail-Adresse für Kontakt-Modul ändern. Klicken Sie <a href=\'index.php?admin=mod_contact_us_settings\'>hier</a>, um fortzufahren.");
INSERT INTO phpn_vocabulary VALUES("720","en","_CONTACTUS_DEFAULT_EMAIL_ALERT","You have to change default email address for Contact Us module. Click <a href=\'index.php?admin=mod_contact_us_settings\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("721","es","_CONTACTUS_DEFAULT_EMAIL_ALERT","Tienes que cambiar la dirección de correo electrónico predeterminado para Contáctenos módulo. Haga clic <a href=\'index.php?admin=mod_contact_us_settings\'>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("722","de","_CONTACT_INFORMATION","Kontakt Information");
INSERT INTO phpn_vocabulary VALUES("723","en","_CONTACT_INFORMATION","Contact Information");
INSERT INTO phpn_vocabulary VALUES("724","es","_CONTACT_INFORMATION","Información de contacto");
INSERT INTO phpn_vocabulary VALUES("725","de","_CONTACT_US","Kontaktieren Sie uns");
INSERT INTO phpn_vocabulary VALUES("726","en","_CONTACT_US","Contact us");
INSERT INTO phpn_vocabulary VALUES("727","es","_CONTACT_US","Contáctenos");
INSERT INTO phpn_vocabulary VALUES("728","de","_CONTACT_US_ALREADY_SENT","Ihre Nachricht wurde bereits gesendet. Bitte versuchen Sie es später noch einmal oder warten _WAIT_ Sekunden.");
INSERT INTO phpn_vocabulary VALUES("729","en","_CONTACT_US_ALREADY_SENT","Your message has been already sent. Please try again later or wait _WAIT_ seconds.");
INSERT INTO phpn_vocabulary VALUES("730","es","_CONTACT_US_ALREADY_SENT","Tu mensaje ha sido enviado ya. Por favor, inténtelo de nuevo más tarde o esperar _WAIT_ segundos.");
INSERT INTO phpn_vocabulary VALUES("731","de","_CONTACT_US_EMAIL_SENT","Vielen Dank für Ihre Kontaktaufnahme! Ihre Nachricht wurde erfolgreich versendet.");
INSERT INTO phpn_vocabulary VALUES("732","en","_CONTACT_US_EMAIL_SENT","Thank you for contacting us! Your message has been successfully sent.");
INSERT INTO phpn_vocabulary VALUES("733","es","_CONTACT_US_EMAIL_SENT","Gracias por contactar con nosotros! Su mensaje ha sido enviado con éxito.");
INSERT INTO phpn_vocabulary VALUES("734","de","_CONTACT_US_SETTINGS","Kontakt Einstellungen");
INSERT INTO phpn_vocabulary VALUES("735","en","_CONTACT_US_SETTINGS","Contact Us Settings");
INSERT INTO phpn_vocabulary VALUES("736","es","_CONTACT_US_SETTINGS","Contacte con nosotros ajustes");
INSERT INTO phpn_vocabulary VALUES("737","de","_CONTENT_TYPE","Content-Typ");
INSERT INTO phpn_vocabulary VALUES("738","en","_CONTENT_TYPE","Content Type");
INSERT INTO phpn_vocabulary VALUES("739","es","_CONTENT_TYPE","Tipo de contenido");
INSERT INTO phpn_vocabulary VALUES("740","de","_CONTINUE_RESERVATION","Weiter Reservierung");
INSERT INTO phpn_vocabulary VALUES("741","en","_CONTINUE_RESERVATION","Continue Reservation");
INSERT INTO phpn_vocabulary VALUES("742","es","_CONTINUE_RESERVATION","Continuar reservación");
INSERT INTO phpn_vocabulary VALUES("743","de","_COPY_TO_OTHERS","Copy to others");
INSERT INTO phpn_vocabulary VALUES("744","en","_COPY_TO_OTHERS","Copy to others");
INSERT INTO phpn_vocabulary VALUES("745","es","_COPY_TO_OTHERS","Copy to others");
INSERT INTO phpn_vocabulary VALUES("746","de","_COPY_TO_OTHER_LANGS","Kopieren in andere Sprachen");
INSERT INTO phpn_vocabulary VALUES("747","en","_COPY_TO_OTHER_LANGS","Copy to other languages");
INSERT INTO phpn_vocabulary VALUES("748","es","_COPY_TO_OTHER_LANGS","Copiar a otros idiomas");
INSERT INTO phpn_vocabulary VALUES("749","de","_COUNT","ZÄHLEN");
INSERT INTO phpn_vocabulary VALUES("750","en","_COUNT","Count");
INSERT INTO phpn_vocabulary VALUES("751","es","_COUNT","Contar");
INSERT INTO phpn_vocabulary VALUES("752","de","_COUNTRIES","Länder");
INSERT INTO phpn_vocabulary VALUES("753","en","_COUNTRIES","Countries");
INSERT INTO phpn_vocabulary VALUES("754","es","_COUNTRIES","Países");
INSERT INTO phpn_vocabulary VALUES("755","de","_COUNTRIES_MANAGEMENT","Länder Management");
INSERT INTO phpn_vocabulary VALUES("756","en","_COUNTRIES_MANAGEMENT","Countries Management");
INSERT INTO phpn_vocabulary VALUES("757","es","_COUNTRIES_MANAGEMENT","Los países de Gestión");
INSERT INTO phpn_vocabulary VALUES("758","de","_COUNTRY","Land");
INSERT INTO phpn_vocabulary VALUES("759","en","_COUNTRY","Country");
INSERT INTO phpn_vocabulary VALUES("760","es","_COUNTRY","País");
INSERT INTO phpn_vocabulary VALUES("761","de","_COUNTRY_EMPTY_ALERT","Land darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("762","en","_COUNTRY_EMPTY_ALERT","Country cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("763","es","_COUNTRY_EMPTY_ALERT","País no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("764","de","_COUPONS","Coupons");
INSERT INTO phpn_vocabulary VALUES("765","en","_COUPONS","Coupons");
INSERT INTO phpn_vocabulary VALUES("766","es","_COUPONS","Cupones");
INSERT INTO phpn_vocabulary VALUES("767","de","_COUPONS_MANAGEMENT","Coupons-Management");
INSERT INTO phpn_vocabulary VALUES("768","en","_COUPONS_MANAGEMENT","Coupons Management");
INSERT INTO phpn_vocabulary VALUES("769","es","_COUPONS_MANAGEMENT","Gestión de cupones");
INSERT INTO phpn_vocabulary VALUES("770","de","_COUPON_CODE","Code coupon");
INSERT INTO phpn_vocabulary VALUES("771","en","_COUPON_CODE","Coupon Code");
INSERT INTO phpn_vocabulary VALUES("772","es","_COUPON_CODE","Código de cupón");
INSERT INTO phpn_vocabulary VALUES("773","de","_COUPON_WAS_APPLIED","Der Coupon _COUPON_CODE_ wurde erfolgreich angewendet!");
INSERT INTO phpn_vocabulary VALUES("774","en","_COUPON_WAS_APPLIED","The coupon _COUPON_CODE_ has been successfully applied!");
INSERT INTO phpn_vocabulary VALUES("775","es","_COUPON_WAS_APPLIED","El _COUPON_CODE_ cupón ha sido aplicado con éxito!");
INSERT INTO phpn_vocabulary VALUES("776","de","_COUPON_WAS_REMOVED","Der Gutschein wurde erfolgreich entfernt!");
INSERT INTO phpn_vocabulary VALUES("777","en","_COUPON_WAS_REMOVED","The coupon has been successfully removed!");
INSERT INTO phpn_vocabulary VALUES("778","es","_COUPON_WAS_REMOVED","El cupón ha sido eliminado con éxito!");
INSERT INTO phpn_vocabulary VALUES("779","de","_CREATED_DATE","Erstellungsdatum");
INSERT INTO phpn_vocabulary VALUES("780","en","_CREATED_DATE","Date Created");
INSERT INTO phpn_vocabulary VALUES("781","es","_CREATED_DATE","Fecha de creación");
INSERT INTO phpn_vocabulary VALUES("782","de","_CREATE_ACCOUNT","Konto erstellen");
INSERT INTO phpn_vocabulary VALUES("783","en","_CREATE_ACCOUNT","Create account");
INSERT INTO phpn_vocabulary VALUES("784","es","_CREATE_ACCOUNT","Crear cuenta");
INSERT INTO phpn_vocabulary VALUES("785","de","_CREATE_ACCOUNT_NOTE","Hinweis: <br>Wir empfehlen");
INSERT INTO phpn_vocabulary VALUES("786","en","_CREATE_ACCOUNT_NOTE","NOTE: <br>We recommend that your password should be at least 6 characters long and should be different from your username.<br><br>Your e-mail address must be valid. We use e-mail for communication purposes (order notifications, etc). Therefore, it is essential to provide a valid e-mail address to be able to use our services correctly.<br><br>All your private data is confidential. We will never sell, exchange or market it in any way. For further information on the responsibilities of both parts, you may refer to us.");
INSERT INTO phpn_vocabulary VALUES("787","es","_CREATE_ACCOUNT_NOTE","NOTA: MedlinePlus Le recomendamos que su contraseña debe tener al menos 6 caracteres de longitud y debe ser diferente de su nombre de usuario. <br>Su dirección de correo electrónico debe ser válida. Usamos el correo electrónico con fines de comunicación (notificaciones de pedido, etc.) Por lo tanto, es esencial para proporcionar una dirección válida de correo electrónico para poder utilizar nuestros servicios correctamente. <br>Todos sus datos personales son confidenciales. Nunca vamos a vender, intercambiar o comercializar de ninguna manera. Para más información sobre las responsabilidades de ambas partes, usted puede referirse a nosotros.");
INSERT INTO phpn_vocabulary VALUES("788","de","_CREATING_ACCOUNT_ERROR","Fehler beim Erstellen Ihres Kontos! Bitte versuchen Sie es später noch einmal oder senden Sie Informationen zu diesem Fehler zur Verwaltung der Website.");
INSERT INTO phpn_vocabulary VALUES("789","en","_CREATING_ACCOUNT_ERROR","An error occurred while creating your account! Please try again later or send information about this error to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("790","es","_CREATING_ACCOUNT_ERROR","Se produjo un error al crear su cuenta! Por favor, inténtelo de nuevo más tarde o enviar información sobre este error a la administración del sitio.");
INSERT INTO phpn_vocabulary VALUES("791","de","_CREATING_NEW_ACCOUNT","Erstellen neues Konto");
INSERT INTO phpn_vocabulary VALUES("792","en","_CREATING_NEW_ACCOUNT","Creating new account");
INSERT INTO phpn_vocabulary VALUES("793","es","_CREATING_NEW_ACCOUNT","Crear cuenta nueva");
INSERT INTO phpn_vocabulary VALUES("794","de","_CREDIT_CARD","Kreditkarte");
INSERT INTO phpn_vocabulary VALUES("795","en","_CREDIT_CARD","Credit Card");
INSERT INTO phpn_vocabulary VALUES("796","es","_CREDIT_CARD","Tarjeta de Crédito");
INSERT INTO phpn_vocabulary VALUES("797","de","_CREDIT_CARD_EXPIRES","Ablauf");
INSERT INTO phpn_vocabulary VALUES("798","en","_CREDIT_CARD_EXPIRES","Expires");
INSERT INTO phpn_vocabulary VALUES("799","es","_CREDIT_CARD_EXPIRES","Expira");
INSERT INTO phpn_vocabulary VALUES("800","de","_CREDIT_CARD_HOLDER_NAME","Name des Karteninhabers");
INSERT INTO phpn_vocabulary VALUES("801","en","_CREDIT_CARD_HOLDER_NAME","Card Holder\'s Name");
INSERT INTO phpn_vocabulary VALUES("802","es","_CREDIT_CARD_HOLDER_NAME","Nombre del titular");
INSERT INTO phpn_vocabulary VALUES("803","de","_CREDIT_CARD_NUMBER","Nummer der Kreditkarte");
INSERT INTO phpn_vocabulary VALUES("804","en","_CREDIT_CARD_NUMBER","Credit Card Number");
INSERT INTO phpn_vocabulary VALUES("805","es","_CREDIT_CARD_NUMBER","Número de tarjeta");
INSERT INTO phpn_vocabulary VALUES("806","de","_CREDIT_CARD_TYPE","Art der Kreditkarte");
INSERT INTO phpn_vocabulary VALUES("807","en","_CREDIT_CARD_TYPE","Credit Card Type");
INSERT INTO phpn_vocabulary VALUES("808","es","_CREDIT_CARD_TYPE","Tarjeta de Crédito");
INSERT INTO phpn_vocabulary VALUES("809","de","_CRONJOB_HTACCESS_BLOCK","So blockieren Sie den Remotezugriff auf cron.php");
INSERT INTO phpn_vocabulary VALUES("810","en","_CRONJOB_HTACCESS_BLOCK","To block remote access to cron.php, in the server&#039;s .htaccess file or vhost configuration file add this section:");
INSERT INTO phpn_vocabulary VALUES("811","es","_CRONJOB_HTACCESS_BLOCK","Para bloquear el acceso remoto a cron.php, en el archivo del servidor htaccess o archivo de configuración de host virtual agregar esta sección.:");
INSERT INTO phpn_vocabulary VALUES("812","de","_CRONJOB_NOTICE","Cron-Jobs können Sie bestimmte Befehle oder Skripte auf Ihrer Website zu automatisieren.<br /><br />uHotelBooking müssen regelmäßig ausgeführt cron.php einige wichtige Operationen durchführen. Die empfohlene Methode");
INSERT INTO phpn_vocabulary VALUES("813","en","_CRONJOB_NOTICE","Cron jobs allow you to automate certain commands or scripts on your site.<br /><br />uHotelBooking needs to periodically run cron.php to close expired discount campaigns or perform another important operations. The recommended way to run cron.php is to set up a cronjob if you run a Unix/Linux server. If for any reason you can&#039;t run a cronjob on your server, you can choose the Non-batch option below to have cron.php run by uHotelBooking itself: in this case cron.php will be run each time someone access your home page. <br /><br />Example of Batch Cron job command: <b>php &#36;HOME/public_html/cron.php >/dev/null 2>&1</b>");
INSERT INTO phpn_vocabulary VALUES("814","es","_CRONJOB_NOTICE","Puestos de trabajo de cron le permiten automatizar ciertos comandos o scripts en su sitio.<br /><br />uHotelBooking debe ejecutar periódicamente cron.php para cerrar expirado campañas de descuento o realizar otra las operaciones de importantes eventos. La manera recomendada de ejecutar cron.php es la creación de un cronjob si ejecuta un servidor Unix/Linux. Si por alguna razón usted no puede ejecutar una tarea programada en el servidor, usted puede elegir la opción para no tener lotes a continuación ejecutar cron.php por uHotelBooking en sí: en este caso cron.php se ejecuta cada vez que alguien de su acceso página de inicio. <br /><br />Ejemplo de comando de proceso por lotes Cron: <b>php &#36;HOME/public_html/cron.php >/dev/null 2>&1</b>");
INSERT INTO phpn_vocabulary VALUES("815","de","_CRON_JOBS","Cron Jobs");
INSERT INTO phpn_vocabulary VALUES("816","en","_CRON_JOBS","Cron Jobs");
INSERT INTO phpn_vocabulary VALUES("817","es","_CRON_JOBS","Cron Jobs");
INSERT INTO phpn_vocabulary VALUES("818","de","_CURRENCIES","Währungen");
INSERT INTO phpn_vocabulary VALUES("819","en","_CURRENCIES","Currencies");
INSERT INTO phpn_vocabulary VALUES("820","es","_CURRENCIES","Monedas");
INSERT INTO phpn_vocabulary VALUES("821","de","_CURRENCIES_DEFAULT_ALERT","Denken Sie daran! Nachdem Sie die Standardwährung: <br> - Edit Wechselkurs jeder Währung manuell (relativ zum neuen Standard-Währung) <br> - Definieren Sie die Preise für alle Zimmer in der neuen Währung.");
INSERT INTO phpn_vocabulary VALUES("822","en","_CURRENCIES_DEFAULT_ALERT","Remember! After you change the default currency:<br>- Edit exchange rate to each currency manually (relatively to the new default currency)<br>- Redefine prices for all rooms in the new currency.");
INSERT INTO phpn_vocabulary VALUES("823","es","_CURRENCIES_DEFAULT_ALERT","¡Recuerde! Después de cambiar la moneda por defecto: <br> - Editar tipo de cambio de cada moneda de forma manual (relativamente a la moneda por defecto nuevo) <br> - Redefinir los precios de todas las habitaciones en la nueva moneda.");
INSERT INTO phpn_vocabulary VALUES("824","de","_CURRENCIES_MANAGEMENT","Währungen Management");
INSERT INTO phpn_vocabulary VALUES("825","en","_CURRENCIES_MANAGEMENT","Currencies Management");
INSERT INTO phpn_vocabulary VALUES("826","es","_CURRENCIES_MANAGEMENT","Monedas de Gestión");
INSERT INTO phpn_vocabulary VALUES("827","de","_CURRENCY","Währung");
INSERT INTO phpn_vocabulary VALUES("828","en","_CURRENCY","Currency");
INSERT INTO phpn_vocabulary VALUES("829","es","_CURRENCY","Moneda");
INSERT INTO phpn_vocabulary VALUES("830","de","_CURRENT_NEXT_YEARS","zum aktuellen/nächsten Jahr");
INSERT INTO phpn_vocabulary VALUES("831","en","_CURRENT_NEXT_YEARS","for current/next years");
INSERT INTO phpn_vocabulary VALUES("832","es","_CURRENT_NEXT_YEARS","para los años corrientes/siguiente");
INSERT INTO phpn_vocabulary VALUES("833","de","_CUSTOMER","Kunde");
INSERT INTO phpn_vocabulary VALUES("834","en","_CUSTOMER","Customer");
INSERT INTO phpn_vocabulary VALUES("835","es","_CUSTOMER","Cliente");
INSERT INTO phpn_vocabulary VALUES("836","de","_CUSTOMERS","Kunden");
INSERT INTO phpn_vocabulary VALUES("837","en","_CUSTOMERS","Customers");
INSERT INTO phpn_vocabulary VALUES("838","es","_CUSTOMERS","Clientes");
INSERT INTO phpn_vocabulary VALUES("839","de","_CUSTOMERS_AWAITING_MODERATION_ALERT","Es gibt _COUNT_ Kunde/n Erwartung Ihrer Zustimmung. Klicken Sie <a href=\'index.php?admin=mod_customers_management\'>hier</a> für die Überprüfung.");
INSERT INTO phpn_vocabulary VALUES("840","en","_CUSTOMERS_AWAITING_MODERATION_ALERT","There are _COUNT_ customer/s awaiting your approval. Click <a href=\'index.php?admin=mod_customers_management\'>here</a> for review.");
INSERT INTO phpn_vocabulary VALUES("841","es","_CUSTOMERS_AWAITING_MODERATION_ALERT","Hay _COUNT_ cliente/s en espera de su aprobación. Haga clic <a href=\'index.php?admin=mod_customers_management\'>aquí</a> para su revisión.");
INSERT INTO phpn_vocabulary VALUES("842","de","_CUSTOMERS_MANAGEMENT","Kunden-Management");
INSERT INTO phpn_vocabulary VALUES("843","en","_CUSTOMERS_MANAGEMENT","Customers Management");
INSERT INTO phpn_vocabulary VALUES("844","es","_CUSTOMERS_MANAGEMENT","Gestión de Clientes");
INSERT INTO phpn_vocabulary VALUES("845","de","_CUSTOMERS_SETTINGS","Kunden, die Einstellungen");
INSERT INTO phpn_vocabulary VALUES("846","en","_CUSTOMERS_SETTINGS","Customers Settings");
INSERT INTO phpn_vocabulary VALUES("847","es","_CUSTOMERS_SETTINGS","Configuración de los clientes de");
INSERT INTO phpn_vocabulary VALUES("848","de","_CUSTOMER_DETAILS","Angaben zum Kunden");
INSERT INTO phpn_vocabulary VALUES("849","en","_CUSTOMER_DETAILS","Customer Details");
INSERT INTO phpn_vocabulary VALUES("850","es","_CUSTOMER_DETAILS","Detalles del cliente");
INSERT INTO phpn_vocabulary VALUES("851","de","_CUSTOMER_GROUP","Kundengruppe");
INSERT INTO phpn_vocabulary VALUES("852","en","_CUSTOMER_GROUP","Customer Group");
INSERT INTO phpn_vocabulary VALUES("853","es","_CUSTOMER_GROUP","Grupo de Clientes");
INSERT INTO phpn_vocabulary VALUES("854","de","_CUSTOMER_GROUPS","Kundengruppen");
INSERT INTO phpn_vocabulary VALUES("855","en","_CUSTOMER_GROUPS","Customer Groups");
INSERT INTO phpn_vocabulary VALUES("856","es","_CUSTOMER_GROUPS","Grupos de clientes");
INSERT INTO phpn_vocabulary VALUES("857","de","_CUSTOMER_LOGIN","Kunden-Login");
INSERT INTO phpn_vocabulary VALUES("858","en","_CUSTOMER_LOGIN","Customer Login");
INSERT INTO phpn_vocabulary VALUES("859","es","_CUSTOMER_LOGIN","Login Cliente");
INSERT INTO phpn_vocabulary VALUES("860","de","_CUSTOMER_NAME","Name des Kunden");
INSERT INTO phpn_vocabulary VALUES("861","en","_CUSTOMER_NAME","Customer Name");
INSERT INTO phpn_vocabulary VALUES("862","es","_CUSTOMER_NAME","Nombre del cliente");
INSERT INTO phpn_vocabulary VALUES("863","de","_CUSTOMER_PANEL","Kunden-Panel");
INSERT INTO phpn_vocabulary VALUES("864","en","_CUSTOMER_PANEL","Customer Panel");
INSERT INTO phpn_vocabulary VALUES("865","es","_CUSTOMER_PANEL","Panel del cliente");
INSERT INTO phpn_vocabulary VALUES("866","de","_CUSTOMER_PAYMENT_MODULES","Kunde & Payment Module");
INSERT INTO phpn_vocabulary VALUES("867","en","_CUSTOMER_PAYMENT_MODULES","Customer & Payment Modules");
INSERT INTO phpn_vocabulary VALUES("868","es","_CUSTOMER_PAYMENT_MODULES","Clientes y Pago Módulos");
INSERT INTO phpn_vocabulary VALUES("869","de","_CVV_CODE","CVV Code");
INSERT INTO phpn_vocabulary VALUES("870","en","_CVV_CODE","CVV Code");
INSERT INTO phpn_vocabulary VALUES("871","es","_CVV_CODE","CVV Código");
INSERT INTO phpn_vocabulary VALUES("872","de","_DASHBOARD","Armaturenbrett");
INSERT INTO phpn_vocabulary VALUES("873","en","_DASHBOARD","Dashboard");
INSERT INTO phpn_vocabulary VALUES("874","es","_DASHBOARD","Salpicadero");
INSERT INTO phpn_vocabulary VALUES("875","de","_DATE","Datum");
INSERT INTO phpn_vocabulary VALUES("876","en","_DATE","Date");
INSERT INTO phpn_vocabulary VALUES("877","es","_DATE","Fecha");
INSERT INTO phpn_vocabulary VALUES("878","de","_DATETIME_PRICE_FORMAT","Datetime & Preis-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("879","en","_DATETIME_PRICE_FORMAT","Datetime & Price Settings");
INSERT INTO phpn_vocabulary VALUES("880","es","_DATETIME_PRICE_FORMAT","Fecha y hora y Configuración de Precio");
INSERT INTO phpn_vocabulary VALUES("881","de","_DATE_AND_TIME_SETTINGS","Datum & Zeit-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("882","en","_DATE_AND_TIME_SETTINGS","Date & Time Settings");
INSERT INTO phpn_vocabulary VALUES("883","es","_DATE_AND_TIME_SETTINGS","De fecha y hora");
INSERT INTO phpn_vocabulary VALUES("884","de","_DATE_CREATED","Erstellungsdatum");
INSERT INTO phpn_vocabulary VALUES("885","en","_DATE_CREATED","Date Created");
INSERT INTO phpn_vocabulary VALUES("886","es","_DATE_CREATED","Fecha de creación");
INSERT INTO phpn_vocabulary VALUES("887","de","_DATE_EMPTY_ALERT","Date fields cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("888","en","_DATE_EMPTY_ALERT","Date fields cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("889","es","_DATE_EMPTY_ALERT","Los campos de fecha no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("890","de","_DATE_FORMAT","Datumsformat");
INSERT INTO phpn_vocabulary VALUES("891","en","_DATE_FORMAT","Date Format");
INSERT INTO phpn_vocabulary VALUES("892","es","_DATE_FORMAT","Formato de fecha");
INSERT INTO phpn_vocabulary VALUES("893","de","_DATE_MODIFIED","Änderungsdatum");
INSERT INTO phpn_vocabulary VALUES("894","en","_DATE_MODIFIED","Date Modified");
INSERT INTO phpn_vocabulary VALUES("895","es","_DATE_MODIFIED","Fecha de modificación");
INSERT INTO phpn_vocabulary VALUES("896","de","_DATE_PAYMENT","Datum der Zahlung");
INSERT INTO phpn_vocabulary VALUES("897","en","_DATE_PAYMENT","Date of Payment");
INSERT INTO phpn_vocabulary VALUES("898","es","_DATE_PAYMENT","Fecha de Pago");
INSERT INTO phpn_vocabulary VALUES("899","de","_DATE_PUBLISHED","Veröffentlichungsdatum");
INSERT INTO phpn_vocabulary VALUES("900","en","_DATE_PUBLISHED","Date Published");
INSERT INTO phpn_vocabulary VALUES("901","es","_DATE_PUBLISHED","Fecha de publicación");
INSERT INTO phpn_vocabulary VALUES("902","de","_DATE_SUBSCRIBED","Datum Gezeichnetes");
INSERT INTO phpn_vocabulary VALUES("903","en","_DATE_SUBSCRIBED","Date Subscribed");
INSERT INTO phpn_vocabulary VALUES("904","es","_DATE_SUBSCRIBED","Fecha de suscripción");
INSERT INTO phpn_vocabulary VALUES("905","de","_DAY","Tag");
INSERT INTO phpn_vocabulary VALUES("906","en","_DAY","Day");
INSERT INTO phpn_vocabulary VALUES("907","es","_DAY","Día");
INSERT INTO phpn_vocabulary VALUES("908","de","_DECEMBER","Dezember");
INSERT INTO phpn_vocabulary VALUES("909","en","_DECEMBER","December");
INSERT INTO phpn_vocabulary VALUES("910","es","_DECEMBER","Diciembre");
INSERT INTO phpn_vocabulary VALUES("911","de","_DECIMALS","Dezimalstellen");
INSERT INTO phpn_vocabulary VALUES("912","en","_DECIMALS","Decimals");
INSERT INTO phpn_vocabulary VALUES("913","es","_DECIMALS","Decimales");
INSERT INTO phpn_vocabulary VALUES("914","de","_DEFAULT","Standardmäßig");
INSERT INTO phpn_vocabulary VALUES("915","en","_DEFAULT","Default");
INSERT INTO phpn_vocabulary VALUES("916","es","_DEFAULT","Predeterminado");
INSERT INTO phpn_vocabulary VALUES("917","de","_DEFAULT_AVAILABILITY","Default Verfügbarkeit");
INSERT INTO phpn_vocabulary VALUES("918","en","_DEFAULT_AVAILABILITY","Default Availability");
INSERT INTO phpn_vocabulary VALUES("919","es","_DEFAULT_AVAILABILITY","Default Availability");
INSERT INTO phpn_vocabulary VALUES("920","de","_DEFAULT_CURRENCY_DELETE_ALERT","Sie können nicht löschen Standardwährung!");
INSERT INTO phpn_vocabulary VALUES("921","en","_DEFAULT_CURRENCY_DELETE_ALERT","You cannot delete default currency!");
INSERT INTO phpn_vocabulary VALUES("922","es","_DEFAULT_CURRENCY_DELETE_ALERT","No se puede eliminar la moneda por defecto!");
INSERT INTO phpn_vocabulary VALUES("923","de","_DEFAULT_EMAIL_ALERT","Sie haben zum ursprünglichen E-Mail-Adresse für Site-Administrator geändert werden. Klicken Sie <a href=\'index.php?admin=settings&tabid=1_4\'>hier</a>, um fortzufahren.");
INSERT INTO phpn_vocabulary VALUES("924","en","_DEFAULT_EMAIL_ALERT","You have to change default email address for site administrator. Click <a href=\'index.php?admin=settings&tabid=1_4\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("925","es","_DEFAULT_EMAIL_ALERT","Tienes que cambiar la dirección de correo electrónico predeterminado para el administrador. Haga clic <a href=\'index.php?admin=settings&tabid=1_4\'>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("926","de","_DEFAULT_HOTEL_DELETE_ALERT","Können nicht gelöscht werden standardmäßig hotel!");
INSERT INTO phpn_vocabulary VALUES("927","en","_DEFAULT_HOTEL_DELETE_ALERT","You cannot delete default hotel!");
INSERT INTO phpn_vocabulary VALUES("928","es","_DEFAULT_HOTEL_DELETE_ALERT","No se pueden eliminar del hotel defecto!");
INSERT INTO phpn_vocabulary VALUES("929","de","_DEFAULT_OWN_EMAIL_ALERT","Sie müssen Ihre eigene E-Mail-Adresse ändern. Klicken Sie <a href=\'index.php?admin=my_account\'>hier</a>, um fortzufahren.");
INSERT INTO phpn_vocabulary VALUES("930","en","_DEFAULT_OWN_EMAIL_ALERT","You have to change your own email address. Click <a href=\'index.php?admin=my_account\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("931","es","_DEFAULT_OWN_EMAIL_ALERT","Usted tiene que cambiar su dirección de correo electrónico propia. Haga clic <a href=\'index.php?admin=my_account\'>aquí</a> para continuar.");
INSERT INTO phpn_vocabulary VALUES("932","de","_DEFAULT_PERIODS_ALERT","Standard Perioden verwendet werden, um Zeit, die durch leicht mit Standard-Preise für jedes Zimmer auf der Zimmerpreise Seite (mit nur einem Klick) konnte erfüllt angeben.");
INSERT INTO phpn_vocabulary VALUES("933","en","_DEFAULT_PERIODS_ALERT","Default Periods are used to specify periods of time that could by easily fulfilled with default prices for each room on the Room Prices page (with just a single click).");
INSERT INTO phpn_vocabulary VALUES("934","es","_DEFAULT_PERIODS_ALERT","Períodos predeterminados se utilizan para especificar los periodos de tiempo que podría fácilmente cumplidas por los precios predeterminados para cada habitación en la página de precios de habitaciones (con un solo clic).");
INSERT INTO phpn_vocabulary VALUES("935","de","_DEFAULT_PERIODS_WERE_ADDED","Standard Perioden wurden erfolgreich hinzugefügt!");
INSERT INTO phpn_vocabulary VALUES("936","en","_DEFAULT_PERIODS_WERE_ADDED","Default periods have been successfully added!");
INSERT INTO phpn_vocabulary VALUES("937","es","_DEFAULT_PERIODS_WERE_ADDED","Períodos predeterminados se han añadido correctamente!");
INSERT INTO phpn_vocabulary VALUES("938","de","_DEFAULT_PRICE","Standard Preis");
INSERT INTO phpn_vocabulary VALUES("939","en","_DEFAULT_PRICE","Default Price");
INSERT INTO phpn_vocabulary VALUES("940","es","_DEFAULT_PRICE","Precio por defecto");
INSERT INTO phpn_vocabulary VALUES("941","de","_DEFAULT_TEMPLATE","Standardvorlage");
INSERT INTO phpn_vocabulary VALUES("942","en","_DEFAULT_TEMPLATE","Default Template");
INSERT INTO phpn_vocabulary VALUES("943","es","_DEFAULT_TEMPLATE","Plantilla predeterminada");
INSERT INTO phpn_vocabulary VALUES("944","de","_DEFINE","Definieren");
INSERT INTO phpn_vocabulary VALUES("945","en","_DEFINE","Define");
INSERT INTO phpn_vocabulary VALUES("946","es","_DEFINE","Definir");
INSERT INTO phpn_vocabulary VALUES("947","de","_DELETE_WARNING","Sie sind sicher, dass Sie diesen Datensatz wirklich löschen?");
INSERT INTO phpn_vocabulary VALUES("948","en","_DELETE_WARNING","Are you sure you want to delete this record?");
INSERT INTO phpn_vocabulary VALUES("949","es","_DELETE_WARNING","¿Estás seguro de que desea eliminar este registro?");
INSERT INTO phpn_vocabulary VALUES("950","de","_DELETE_WARNING_COMMON","Sie sind sicher");
INSERT INTO phpn_vocabulary VALUES("951","en","_DELETE_WARNING_COMMON","Are you sure you want to delete this record?");
INSERT INTO phpn_vocabulary VALUES("952","es","_DELETE_WARNING_COMMON","¿Estás seguro de que desea eliminar este registro?");
INSERT INTO phpn_vocabulary VALUES("953","de","_DELETE_WORD","Löschen");
INSERT INTO phpn_vocabulary VALUES("954","en","_DELETE_WORD","Delete");
INSERT INTO phpn_vocabulary VALUES("955","es","_DELETE_WORD","Borrar");
INSERT INTO phpn_vocabulary VALUES("956","de","_DELETING_ACCOUNT_ERROR","Ein Fehler beim Löschen Sie verändern! Bitte versuchen Sie es später noch einmal oder schicken Sie zu diesem Thema zur Verwaltung der Website.");
INSERT INTO phpn_vocabulary VALUES("957","en","_DELETING_ACCOUNT_ERROR","An error occurred while deleting your account! Please try again later or send email about this issue to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("958","es","_DELETING_ACCOUNT_ERROR","Se produjo un error al eliminar su cuenta! Por favor, inténtelo de nuevo más tarde o envíe un correo electrónico acerca de este problema a la administración del sitio.");
INSERT INTO phpn_vocabulary VALUES("959","de","_DELETING_OPERATION_COMPLETED","Löschen ist erfolgreich abgeschlossen!");
INSERT INTO phpn_vocabulary VALUES("960","en","_DELETING_OPERATION_COMPLETED","Deleting operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("961","es","_DELETING_OPERATION_COMPLETED","Eliminación de la operación se completó con éxito!");
INSERT INTO phpn_vocabulary VALUES("962","de","_DESCRIPTION","Beschreibung");
INSERT INTO phpn_vocabulary VALUES("963","en","_DESCRIPTION","Description");
INSERT INTO phpn_vocabulary VALUES("964","es","_DESCRIPTION","Descripción");
INSERT INTO phpn_vocabulary VALUES("965","de","_DISABLED","behindert");
INSERT INTO phpn_vocabulary VALUES("966","en","_DISABLED","disabled");
INSERT INTO phpn_vocabulary VALUES("967","es","_DISABLED","discapacitado");
INSERT INTO phpn_vocabulary VALUES("968","de","_DISCOUNT","Rabatt");
INSERT INTO phpn_vocabulary VALUES("969","en","_DISCOUNT","Discount");
INSERT INTO phpn_vocabulary VALUES("970","es","_DISCOUNT","Descuento");
INSERT INTO phpn_vocabulary VALUES("971","de","_DISCOUNT_BY_ADMIN","Rabatt von Administrator");
INSERT INTO phpn_vocabulary VALUES("972","en","_DISCOUNT_BY_ADMIN","Discount By Administrator");
INSERT INTO phpn_vocabulary VALUES("973","es","_DISCOUNT_BY_ADMIN","Descuento por Administrator");
INSERT INTO phpn_vocabulary VALUES("974","de","_DISCOUNT_CAMPAIGN","Discount-Kampagne");
INSERT INTO phpn_vocabulary VALUES("975","en","_DISCOUNT_CAMPAIGN","Discount Campaign");
INSERT INTO phpn_vocabulary VALUES("976","es","_DISCOUNT_CAMPAIGN","Descuento Campaña");
INSERT INTO phpn_vocabulary VALUES("977","de","_DISCOUNT_CAMPAIGNS","Rabattaktionen");
INSERT INTO phpn_vocabulary VALUES("978","en","_DISCOUNT_CAMPAIGNS","Discount Campaigns");
INSERT INTO phpn_vocabulary VALUES("979","es","_DISCOUNT_CAMPAIGNS","Descuento Campañas");
INSERT INTO phpn_vocabulary VALUES("980","de","_DISCOUNT_CAMPAIGN_TEXT","<span class=campaign_header>Super Rabattaktion!</span> <br /><br />Genießen Sie besondere Preissenkungen jetzt <br />_FROM_ _TO_: <br /> <b>_PERCENT_</b> Auf jedem Zimmer Reservierung in unserem Hotel!");
INSERT INTO phpn_vocabulary VALUES("981","en","_DISCOUNT_CAMPAIGN_TEXT","<span class=campaign_header>Super discount campaign!</span><br /><br />\nEnjoy special price cuts right now<br />_FROM_ _TO_:<br /> \n<b>_PERCENT_</b> on every room reservation in our Hotel!");
INSERT INTO phpn_vocabulary VALUES("982","es","_DISCOUNT_CAMPAIGN_TEXT","<span class=campaign_header>Super campaña de descuento!</span><br /><br />\nDisfrute de los recortes de precios especiales en este momento <br />_FROM_ _TO_:<br /><b>_PERCENT_</b> en cada reserva de habitación en nuestro hotel!");
INSERT INTO phpn_vocabulary VALUES("983","de","_DISCOUNT_STD_CAMPAIGN_TEXT","<span class=campaign_header>Super-Rabatt-Aktion!</span><br><br>Genießen Sie besondere Preissenkungen in unserem Hotel in den angegebenen Zeiträumen unten!");
INSERT INTO phpn_vocabulary VALUES("984","en","_DISCOUNT_STD_CAMPAIGN_TEXT","<span class=campaign_header>Super discount campaign!</span><br><br>Enjoy special price cuts in our Hotel for the specified periods of time below!");
INSERT INTO phpn_vocabulary VALUES("985","es","_DISCOUNT_STD_CAMPAIGN_TEXT","<span class=campaign_header>La campaña de descuento Super!</span><br><br>Disfrute de recortes de precios especiales en nuestro hotel en los períodos de tiempo especificados a continuación!");
INSERT INTO phpn_vocabulary VALUES("986","de","_DISPLAY_ON","Anzeige");
INSERT INTO phpn_vocabulary VALUES("987","en","_DISPLAY_ON","Display on");
INSERT INTO phpn_vocabulary VALUES("988","es","_DISPLAY_ON","Pantalla en la");
INSERT INTO phpn_vocabulary VALUES("989","de","_DOWN","Nach unten");
INSERT INTO phpn_vocabulary VALUES("990","en","_DOWN","Down");
INSERT INTO phpn_vocabulary VALUES("991","es","_DOWN","Abajo");
INSERT INTO phpn_vocabulary VALUES("992","de","_DOWNLOAD","Herunterladen");
INSERT INTO phpn_vocabulary VALUES("993","en","_DOWNLOAD","Download");
INSERT INTO phpn_vocabulary VALUES("994","es","_DOWNLOAD","Descargar");
INSERT INTO phpn_vocabulary VALUES("995","de","_DOWNLOAD_INVOICE","Download Rechnung");
INSERT INTO phpn_vocabulary VALUES("996","en","_DOWNLOAD_INVOICE","Download Invoice");
INSERT INTO phpn_vocabulary VALUES("997","es","_DOWNLOAD_INVOICE","Descarga de facturas");
INSERT INTO phpn_vocabulary VALUES("998","de","_ECHECK","E-Check");
INSERT INTO phpn_vocabulary VALUES("999","en","_ECHECK","E-Check");
INSERT INTO phpn_vocabulary VALUES("1000","es","_ECHECK","E-Check");
INSERT INTO phpn_vocabulary VALUES("1001","de","_EDIT_MENUS","Bearbeiten-Menüs");
INSERT INTO phpn_vocabulary VALUES("1002","en","_EDIT_MENUS","Edit Menus");
INSERT INTO phpn_vocabulary VALUES("1003","es","_EDIT_MENUS","Editar los menús");
INSERT INTO phpn_vocabulary VALUES("1004","de","_EDIT_MY_ACCOUNT","Mein Konto bearbeiten");
INSERT INTO phpn_vocabulary VALUES("1005","en","_EDIT_MY_ACCOUNT","Edit My Account");
INSERT INTO phpn_vocabulary VALUES("1006","es","_EDIT_MY_ACCOUNT","Editar mi cuenta");
INSERT INTO phpn_vocabulary VALUES("1007","de","_EDIT_PAGE","Seite bearbeiten");
INSERT INTO phpn_vocabulary VALUES("1008","en","_EDIT_PAGE","Edit Page");
INSERT INTO phpn_vocabulary VALUES("1009","es","_EDIT_PAGE","Editar página");
INSERT INTO phpn_vocabulary VALUES("1010","de","_EDIT_WORD","Bearbeiten");
INSERT INTO phpn_vocabulary VALUES("1011","en","_EDIT_WORD","Edit");
INSERT INTO phpn_vocabulary VALUES("1012","es","_EDIT_WORD","Editar");
INSERT INTO phpn_vocabulary VALUES("1013","de","_EMAIL","E-Mail");
INSERT INTO phpn_vocabulary VALUES("1014","en","_EMAIL","Email");
INSERT INTO phpn_vocabulary VALUES("1015","es","_EMAIL","E-mail");
INSERT INTO phpn_vocabulary VALUES("1016","de","_EMAILS_SENT_ERROR","Fehler beim Versenden von Emails oder es sind keine E-Mails gesendet werden! Bitte versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("1017","en","_EMAILS_SENT_ERROR","An error occurred while sending emails or there are no emails to be sent! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("1018","es","_EMAILS_SENT_ERROR","Se produjo un error al enviar correos electrónicos o no hay mensajes de correo electrónico para ser enviados! Por favor, inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("1019","de","_EMAILS_SUCCESSFULLY_SENT","Status: _SENT_ E-Mails von _TOTAL_ waren erfolgreich verschickt!");
INSERT INTO phpn_vocabulary VALUES("1020","en","_EMAILS_SUCCESSFULLY_SENT","Status: _SENT_ emails from _TOTAL_ were successfully sent!");
INSERT INTO phpn_vocabulary VALUES("1021","es","_EMAILS_SUCCESSFULLY_SENT","Estado: mensajes de correo electrónico _SENT_ de _TOTAL_ fueron enviados con éxito!");
INSERT INTO phpn_vocabulary VALUES("1022","de","_EMAIL_ADDRESS","E-Mail-Adresse");
INSERT INTO phpn_vocabulary VALUES("1023","en","_EMAIL_ADDRESS","E-mail address");
INSERT INTO phpn_vocabulary VALUES("1024","es","_EMAIL_ADDRESS","E-mail address");
INSERT INTO phpn_vocabulary VALUES("1025","de","_EMAIL_BLOCKED","Ihre E-Mail wurde gesperrt! um dieses Problem zu beheben");
INSERT INTO phpn_vocabulary VALUES("1026","en","_EMAIL_BLOCKED","Your email is blocked! To resolve this problem, please contact the site administrator.");
INSERT INTO phpn_vocabulary VALUES("1027","es","_EMAIL_BLOCKED","Tu email fue bloqueado! Para resolver este problema, póngase en contacto con el administrador del sitio.");
INSERT INTO phpn_vocabulary VALUES("1028","de","_EMAIL_EMPTY_ALERT","E-Mail darf nicht leer sein! Bitte neu eingeben.");
INSERT INTO phpn_vocabulary VALUES("1029","en","_EMAIL_EMPTY_ALERT","Email cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1030","es","_EMAIL_EMPTY_ALERT","El correo electrónico no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1031","de","_EMAIL_FROM","E-Mail Adresse (Von)");
INSERT INTO phpn_vocabulary VALUES("1032","en","_EMAIL_FROM","Email Address (From)");
INSERT INTO phpn_vocabulary VALUES("1033","es","_EMAIL_FROM","Dirección de correo electrónico (De)");
INSERT INTO phpn_vocabulary VALUES("1034","de","_EMAIL_IS_EMPTY","E-Mail darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1035","en","_EMAIL_IS_EMPTY","Email must not be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1036","es","_EMAIL_IS_EMPTY","El correo electrónico no debe estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1037","de","_EMAIL_IS_WRONG","Bitte geben Sie eine gültige Email-Adresse.");
INSERT INTO phpn_vocabulary VALUES("1038","en","_EMAIL_IS_WRONG","Please enter a valid email address.");
INSERT INTO phpn_vocabulary VALUES("1039","es","_EMAIL_IS_WRONG","Por favor, introduzca una dirección de correo electrónico válida.");
INSERT INTO phpn_vocabulary VALUES("1040","de","_EMAIL_NOTIFICATIONS","Senden Sie E-Mail Benachrichtigungen");
INSERT INTO phpn_vocabulary VALUES("1041","en","_EMAIL_NOTIFICATIONS","Send email notifications");
INSERT INTO phpn_vocabulary VALUES("1042","es","_EMAIL_NOTIFICATIONS","Enviar notificaciones por correo electrónico");
INSERT INTO phpn_vocabulary VALUES("1043","de","_EMAIL_NOT_EXISTS","Diese E-Mail-Konto nicht im System vorhanden! Bitte neu eingeben.");
INSERT INTO phpn_vocabulary VALUES("1044","en","_EMAIL_NOT_EXISTS","This e-mail account does not exist in the system! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1045","es","_EMAIL_NOT_EXISTS","Esta cuenta de correo electrónico no existe en el sistema! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1046","de","_EMAIL_SEND_ERROR","Fehler beim Versenden per E-Mail. Bitte überprüfen Sie Ihre E-Mail-Einstellungen und die Empfänger der Nachricht, dann versuchen Sie es erneut.");
INSERT INTO phpn_vocabulary VALUES("1047","en","_EMAIL_SEND_ERROR","An error occurred while sending email. Please check your email settings and message recipients, then try again.");
INSERT INTO phpn_vocabulary VALUES("1048","es","_EMAIL_SEND_ERROR","Se produjo un error al enviar correo electrónico. Verifica la configuración de correo electrónico y los destinatarios del mensaje y vuelva a intentarlo.");
INSERT INTO phpn_vocabulary VALUES("1049","de","_EMAIL_SETTINGS","E-Mail-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("1050","en","_EMAIL_SETTINGS","Email Settings");
INSERT INTO phpn_vocabulary VALUES("1051","es","_EMAIL_SETTINGS","Configuración del correo electrónico");
INSERT INTO phpn_vocabulary VALUES("1052","de","_EMAIL_SUCCESSFULLY_SENT","E-Mail wurde erfolgreich gesendet!");
INSERT INTO phpn_vocabulary VALUES("1053","en","_EMAIL_SUCCESSFULLY_SENT","Email has been successfully sent!");
INSERT INTO phpn_vocabulary VALUES("1054","es","_EMAIL_SUCCESSFULLY_SENT","El correo electrónico fue enviado con éxito!");
INSERT INTO phpn_vocabulary VALUES("1055","de","_EMAIL_TEMPLATES","E-Mail-Vorlagen");
INSERT INTO phpn_vocabulary VALUES("1056","en","_EMAIL_TEMPLATES","Email Templates");
INSERT INTO phpn_vocabulary VALUES("1057","es","_EMAIL_TEMPLATES","Plantillas de correo");
INSERT INTO phpn_vocabulary VALUES("1058","de","_EMAIL_TEMPLATES_EDITOR","E-Mail-Editor");
INSERT INTO phpn_vocabulary VALUES("1059","en","_EMAIL_TEMPLATES_EDITOR","Email Templates Editor");
INSERT INTO phpn_vocabulary VALUES("1060","es","_EMAIL_TEMPLATES_EDITOR","Plantillas de correo electrónico del editor");
INSERT INTO phpn_vocabulary VALUES("1061","de","_EMAIL_TO","E-Mail Adresse (To)");
INSERT INTO phpn_vocabulary VALUES("1062","en","_EMAIL_TO","Email Address (To)");
INSERT INTO phpn_vocabulary VALUES("1063","es","_EMAIL_TO","Correo electrónico (Para)");
INSERT INTO phpn_vocabulary VALUES("1064","de","_EMAIL_VALID_ALERT","Bitte geben Sie eine gültige E-Mail Adresse!");
INSERT INTO phpn_vocabulary VALUES("1065","en","_EMAIL_VALID_ALERT","Please enter a valid email address!");
INSERT INTO phpn_vocabulary VALUES("1066","es","_EMAIL_VALID_ALERT","Por favor, introduzca una dirección válida de correo electrónico!");
INSERT INTO phpn_vocabulary VALUES("1067","de","_EMPTY","Leere");
INSERT INTO phpn_vocabulary VALUES("1068","en","_EMPTY","Empty");
INSERT INTO phpn_vocabulary VALUES("1069","es","_EMPTY","Vaciar");
INSERT INTO phpn_vocabulary VALUES("1070","de","_ENTER_BOOKING_NUMBER","Geben Sie Ihre Buchungsnummer");
INSERT INTO phpn_vocabulary VALUES("1071","en","_ENTER_BOOKING_NUMBER","Enter your booking number");
INSERT INTO phpn_vocabulary VALUES("1072","es","_ENTER_BOOKING_NUMBER","Ingrese su número de reserva");
INSERT INTO phpn_vocabulary VALUES("1073","de","_ENTER_CONFIRMATION_CODE","Geben Sie Bestätigungs-Code");
INSERT INTO phpn_vocabulary VALUES("1074","en","_ENTER_CONFIRMATION_CODE","Enter Confirmation Code");
INSERT INTO phpn_vocabulary VALUES("1075","es","_ENTER_CONFIRMATION_CODE","Ingrese el código de confirmación");
INSERT INTO phpn_vocabulary VALUES("1076","de","_ENTER_EMAIL_ADDRESS","(Bitte geben Sie <i>nur echte</i> E-Mail-Adresse)");
INSERT INTO phpn_vocabulary VALUES("1077","en","_ENTER_EMAIL_ADDRESS","(Please enter ONLY real email address)");
INSERT INTO phpn_vocabulary VALUES("1078","es","_ENTER_EMAIL_ADDRESS","(Por favor, introduzca sólo real dirección de correo electrónico)");
INSERT INTO phpn_vocabulary VALUES("1079","de","_ENTIRE_SITE","Die ganze Site");
INSERT INTO phpn_vocabulary VALUES("1080","en","_ENTIRE_SITE","Entire Site");
INSERT INTO phpn_vocabulary VALUES("1081","es","_ENTIRE_SITE","Todo el sitio web");
INSERT INTO phpn_vocabulary VALUES("1082","de","_EVENTS","Veranstaltungen");
INSERT INTO phpn_vocabulary VALUES("1083","en","_EVENTS","Events");
INSERT INTO phpn_vocabulary VALUES("1084","es","_EVENTS","Eventos");
INSERT INTO phpn_vocabulary VALUES("1085","de","_EVENT_REGISTRATION_COMPLETED","Vielen Dank für Ihr Interesse! Sie haben gerade erfolgreich auf dieses Ereignis registriert.");
INSERT INTO phpn_vocabulary VALUES("1086","en","_EVENT_REGISTRATION_COMPLETED","Thank you for your interest! You have just successfully registered to this event.");
INSERT INTO phpn_vocabulary VALUES("1087","es","_EVENT_REGISTRATION_COMPLETED","Gracias por tu interés! Usted acaba de éxito registrado para este evento.");
INSERT INTO phpn_vocabulary VALUES("1088","de","_EVENT_USER_ALREADY_REGISTERED","Mitglied mit solchen E-Mail wurde bereits auf dieses Ereignis registriert! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1089","en","_EVENT_USER_ALREADY_REGISTERED","Member with such email is already registered to this event! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1090","es","_EVENT_USER_ALREADY_REGISTERED","Miembro con el correo electrónico como ya estaba registrado a este evento! Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("1091","de","_EXPAND_PANEL","Erweitern Navigationsleiste");
INSERT INTO phpn_vocabulary VALUES("1092","en","_EXPAND_PANEL","Expand navigation panel");
INSERT INTO phpn_vocabulary VALUES("1093","es","_EXPAND_PANEL","Ampliar el panel de navegación");
INSERT INTO phpn_vocabulary VALUES("1094","de","_EXPIRED","Abgelaufen");
INSERT INTO phpn_vocabulary VALUES("1095","en","_EXPIRED","Expired");
INSERT INTO phpn_vocabulary VALUES("1096","es","_EXPIRED","Caducado");
INSERT INTO phpn_vocabulary VALUES("1097","de","_EXPORT","Export");
INSERT INTO phpn_vocabulary VALUES("1098","en","_EXPORT","Export");
INSERT INTO phpn_vocabulary VALUES("1099","es","_EXPORT","Exportación");
INSERT INTO phpn_vocabulary VALUES("1100","de","_EXTRAS","Extras");
INSERT INTO phpn_vocabulary VALUES("1101","en","_EXTRAS","Extras");
INSERT INTO phpn_vocabulary VALUES("1102","es","_EXTRAS","Extras");
INSERT INTO phpn_vocabulary VALUES("1103","de","_EXTRAS_MANAGEMENT","Extras Management");
INSERT INTO phpn_vocabulary VALUES("1104","en","_EXTRAS_MANAGEMENT","Extras Management");
INSERT INTO phpn_vocabulary VALUES("1105","es","_EXTRAS_MANAGEMENT","Extras de Gestión");
INSERT INTO phpn_vocabulary VALUES("1106","de","_EXTRAS_SUBTOTAL","Extras Teilsumme");
INSERT INTO phpn_vocabulary VALUES("1107","en","_EXTRAS_SUBTOTAL","Extras Subtotal");
INSERT INTO phpn_vocabulary VALUES("1108","es","_EXTRAS_SUBTOTAL","Extras Subtotal");
INSERT INTO phpn_vocabulary VALUES("1109","de","_EXTRA_BED","Zustellbett");
INSERT INTO phpn_vocabulary VALUES("1110","en","_EXTRA_BED","Extra Bed");
INSERT INTO phpn_vocabulary VALUES("1111","es","_EXTRA_BED","Cama adicional");
INSERT INTO phpn_vocabulary VALUES("1112","de","_EXTRA_BEDS","Zustellbetten");
INSERT INTO phpn_vocabulary VALUES("1113","en","_EXTRA_BEDS","Extra Beds");
INSERT INTO phpn_vocabulary VALUES("1114","es","_EXTRA_BEDS","Las camas supletorias");
INSERT INTO phpn_vocabulary VALUES("1115","de","_EXTRA_BED_CHARGE","Extrabettgebühr");
INSERT INTO phpn_vocabulary VALUES("1116","en","_EXTRA_BED_CHARGE","Extra Bed Charge");
INSERT INTO phpn_vocabulary VALUES("1117","es","_EXTRA_BED_CHARGE","Cama supletoria");
INSERT INTO phpn_vocabulary VALUES("1118","de","_FACILITIES","Anlagen");
INSERT INTO phpn_vocabulary VALUES("1119","en","_FACILITIES","Facilities");
INSERT INTO phpn_vocabulary VALUES("1120","es","_FACILITIES","Instalaciones");
INSERT INTO phpn_vocabulary VALUES("1121","de","_FAQ","Häufig gestellte Fragen");
INSERT INTO phpn_vocabulary VALUES("1122","en","_FAQ","FAQ");
INSERT INTO phpn_vocabulary VALUES("1123","es","_FAQ","Preguntas frecuentes");
INSERT INTO phpn_vocabulary VALUES("1124","de","_FAQ_MANAGEMENT","FAQ-Management");
INSERT INTO phpn_vocabulary VALUES("1125","en","_FAQ_MANAGEMENT","FAQ Management");
INSERT INTO phpn_vocabulary VALUES("1126","es","_FAQ_MANAGEMENT","Preguntas de Gestión");
INSERT INTO phpn_vocabulary VALUES("1127","de","_FAQ_SETTINGS","FAQ Einstellungen");
INSERT INTO phpn_vocabulary VALUES("1128","en","_FAQ_SETTINGS","FAQ Settings");
INSERT INTO phpn_vocabulary VALUES("1129","es","_FAQ_SETTINGS","Configuración del FAQ");
INSERT INTO phpn_vocabulary VALUES("1130","de","_FAX","Fax");
INSERT INTO phpn_vocabulary VALUES("1131","en","_FAX","Fax");
INSERT INTO phpn_vocabulary VALUES("1132","es","_FAX","Fax");
INSERT INTO phpn_vocabulary VALUES("1133","de","_FEBRUARY","Februar");
INSERT INTO phpn_vocabulary VALUES("1134","en","_FEBRUARY","February");
INSERT INTO phpn_vocabulary VALUES("1135","es","_FEBRUARY","Febrero");
INSERT INTO phpn_vocabulary VALUES("1136","de","_FIELD_CANNOT_BE_EMPTY","Feld _FIELD_ darf nicht leer sein! Bitte neu eingeben.");
INSERT INTO phpn_vocabulary VALUES("1137","en","_FIELD_CANNOT_BE_EMPTY","Field _FIELD_ cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1138","es","_FIELD_CANNOT_BE_EMPTY","Campo _FIELD_ no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1139","de","_FIELD_LENGTH_ALERT","Die länge des feldes _FIELD_ muss kleiner sein als _LENGTH_ zeichen! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1140","en","_FIELD_LENGTH_ALERT","The length of the field _FIELD_ must be less than _LENGTH_ characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1141","es","_FIELD_LENGTH_ALERT","La longitud de la _FIELD_ campo debe ser inferior a _LENGTH_ personajes! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1142","de","_FIELD_LENGTH_EXCEEDED","_SIZE_ Zeichen: _LENGTH_ hat die maximal zulässige Größe überschritten! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1143","en","_FIELD_LENGTH_EXCEEDED","_FIELD_ has exceeded the maximum allowed size: _LENGTH_ characters! Please re-enter. ");
INSERT INTO phpn_vocabulary VALUES("1144","es","_FIELD_LENGTH_EXCEEDED","_FIELD_ ha superado el tamaño máximo permitido: _LENGTH_ personajes! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1145","de","_FIELD_MIN_LENGTH_ALERT","Die Länge des Feldes _FIELD_ kann nicht kleiner sein als _LENGTH_ Zeichen! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1146","en","_FIELD_MIN_LENGTH_ALERT","The length of the field _FIELD_ cannot  be less than _LENGTH_ characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1147","es","_FIELD_MIN_LENGTH_ALERT","La longitud de la _FIELD_ campo no puede ser inferior a _LENGTH_ personajes! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1148","de","_FIELD_MUST_BE_ALPHA","_FIELD_ muss alphabetische wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1149","en","_FIELD_MUST_BE_ALPHA","_FIELD_ must be an alphabetic value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1150","es","_FIELD_MUST_BE_ALPHA","Valor _FIELD_ debe ser alfabético! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1151","de","_FIELD_MUST_BE_ALPHA_NUMERIC","_FIELD_ muss alphanumerischen wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1152","en","_FIELD_MUST_BE_ALPHA_NUMERIC","_FIELD_ must be an alphanumeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1153","es","_FIELD_MUST_BE_ALPHA_NUMERIC","Valor _FIELD_ debe ser alfanumérico! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1154","de","_FIELD_MUST_BE_BOOLEAN","Field _FIELD_ Wert muss Ja oder Nein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1155","en","_FIELD_MUST_BE_BOOLEAN","Field _FIELD_ value must be \'yes\' or \'no\'! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1156","es","_FIELD_MUST_BE_BOOLEAN","Campo de valor _FIELD_ debe ser \'yes\' o \'no\'! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1157","de","_FIELD_MUST_BE_EMAIL","_FIELD_ müssen gültige E-Mail-Format sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1158","en","_FIELD_MUST_BE_EMAIL","_FIELD_ must be in valid email format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1159","es","_FIELD_MUST_BE_EMAIL","_FIELD_ debe estar en formato de correo electrónico válida! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1160","de","_FIELD_MUST_BE_FLOAT","Field _FIELD_ muss floatzahl wert! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1161","en","_FIELD_MUST_BE_FLOAT","Field _FIELD_ must be a float number value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1162","es","_FIELD_MUST_BE_FLOAT","Campo _FIELD_ debe ser el valor flotante número! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1163","de","_FIELD_MUST_BE_FLOAT_POSITIVE","Field _FIELD_ müssen positive fließkommazahl wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1164","en","_FIELD_MUST_BE_FLOAT_POSITIVE","Field _FIELD_ must be a positive float number value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1165","es","_FIELD_MUST_BE_FLOAT_POSITIVE","Campo _FIELD_ debe ser un valor positivo número flotan! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1166","es","_FIELD_MUST_BE_INTEGER","Campo _FIELD_ debe ser número entero positivo!");
INSERT INTO phpn_vocabulary VALUES("1167","de","_FIELD_MUST_BE_IP_ADDRESS","_FIELD_ muss eine gültige IP-Adresse! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1168","en","_FIELD_MUST_BE_IP_ADDRESS","_FIELD_ must be a valid IP Address! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1169","es","_FIELD_MUST_BE_IP_ADDRESS","_FIELD_ Debe ser una dirección IP válida! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1170","de","_FIELD_MUST_BE_NUMERIC","Field _FIELD_ muss numerischer Wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1171","en","_FIELD_MUST_BE_NUMERIC","Field _FIELD_ must be a numeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1172","es","_FIELD_MUST_BE_NUMERIC","Campo _FIELD_ valor debe ser numérico! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1173","de","_FIELD_MUST_BE_NUMERIC_POSITIVE","Field _FIELD_ müssen positive numerischer wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1174","en","_FIELD_MUST_BE_NUMERIC_POSITIVE","Field _FIELD_ must be a positive numeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1175","es","_FIELD_MUST_BE_NUMERIC_POSITIVE","Campo _FIELD_ debe ser el valor numérico positivo! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1176","de","_FIELD_MUST_BE_PASSWORD","_FIELD_ muss 6 zeichen lang sein und mindestens bestehen aus buchstaben und ziffern! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1177","en","_FIELD_MUST_BE_PASSWORD","_FIELD_ must be 6 characters at least and consist of letters and digits! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1178","es","_FIELD_MUST_BE_PASSWORD","_FIELD_ Debe ser de 6 caracteres como mínimo y se componen de letras y dígitos! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1179","de","_FIELD_MUST_BE_POSITIVE_INT","Field _FIELD_ müssen positive ganze zahl sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1180","en","_FIELD_MUST_BE_POSITIVE_INT","Field _FIELD_ must be a positive integer value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1181","es","_FIELD_MUST_BE_POSITIVE_INT","Campo _FIELD_ debe ser un valor entero positivo! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1182","de","_FIELD_MUST_BE_POSITIVE_INTEGER","Field _FIELD_ müssen positive ganze Zahl sein!");
INSERT INTO phpn_vocabulary VALUES("1183","en","_FIELD_MUST_BE_POSITIVE_INTEGER","Field _FIELD_ must be a positive integer number!");
INSERT INTO phpn_vocabulary VALUES("1184","de","_FIELD_MUST_BE_SIZE_VALUE","Feld _FIELD_ muss ein gültiger HTML size-Eigenschaft in \'px\', \'pt\', \'em\' oder \'%\' -Einheiten werden! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1185","en","_FIELD_MUST_BE_SIZE_VALUE","Field _FIELD_ must be a valid HTML size property in \'px\', \'pt\', \'em\' or \'%\' units! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1186","es","_FIELD_MUST_BE_SIZE_VALUE","_FIELD_ Campo debe ser una propiedad válida tamaño HTML en \'px\', \'pt\', \'em\' o \'%\' unidades! Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("1187","de","_FIELD_MUST_BE_TEXT","_FIELD_ Warst Muss Ein Text seins! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1188","en","_FIELD_MUST_BE_TEXT","_FIELD_ value must be a text! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1189","es","_FIELD_MUST_BE_TEXT","_FIELD_ Valor debe ser un texto! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1190","de","_FIELD_MUST_BE_UNSIGNED_FLOAT","Field _FIELD_ muss unsigned Float-Wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1191","en","_FIELD_MUST_BE_UNSIGNED_FLOAT","Field _FIELD_ must be an unsigned float value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1192","es","_FIELD_MUST_BE_UNSIGNED_FLOAT","Campo _FIELD_ debe ser valor flotante sin firmar! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1193","de","_FIELD_MUST_BE_UNSIGNED_INT","Feld _FIELD_ muss unsigned Integer-Wert sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1194","en","_FIELD_MUST_BE_UNSIGNED_INT","Field _FIELD_ must be an unsigned integer value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1195","es","_FIELD_MUST_BE_UNSIGNED_INT","_FIELD_ Campo debe ser un valor entero sin signo! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1196","de","_FIELD_VALUE_EXCEEDED","_FIELD_ Hat die maximal zulässige Wert überschritten _MAX_! Bitte geben Sie erneut oder ändern Zimmer gesamt Anzahl <a href=\'index.php?admin=mod_rooms_management\'>hier</a>.");
INSERT INTO phpn_vocabulary VALUES("1197","en","_FIELD_VALUE_EXCEEDED","_FIELD_ has exceeded the maximum allowed value _MAX_! Please re-enter or change total rooms number <a href=\'index.php?admin=mod_rooms_management\'>here</a>.");
INSERT INTO phpn_vocabulary VALUES("1198","es","_FIELD_VALUE_EXCEEDED","_FIELD_ ha superado el valor máximo permitido _MAX_! Por favor vuelva a introducir o modificar el número total de habitaciones <a href=\'index.php?admin=mod_rooms_management\'>aquí</a>.");
INSERT INTO phpn_vocabulary VALUES("1199","de","_FIELD_VALUE_MINIMUM","_FIELD_ Wert sollte nicht weniger als _MIN_! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1200","en","_FIELD_VALUE_MINIMUM","_FIELD_ value should not be less then _MIN_! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1201","es","_FIELD_VALUE_MINIMUM","_FIELD_ valor no debe ser inferior a _MIN_! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1202","de","_FILED_UNIQUE_VALUE_ALERT","Das feld _FIELD_ akzeptiert nur eindeutige werte - bitte erneut eingeben!");
INSERT INTO phpn_vocabulary VALUES("1203","en","_FILED_UNIQUE_VALUE_ALERT","The field _FIELD_ accepts only unique values - please re-enter!");
INSERT INTO phpn_vocabulary VALUES("1204","es","_FILED_UNIQUE_VALUE_ALERT","El _FIELD_ campo sólo acepta valores únicos - por favor vuelva a entrar!");
INSERT INTO phpn_vocabulary VALUES("1205","de","_FILE_DELETING_ERROR","Fehler beim löschen der datei! Bitte versuchen sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("1206","en","_FILE_DELETING_ERROR","An error occurred while deleting file! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("1207","es","_FILE_DELETING_ERROR","Se produjo un error al eliminar el archivo! Por favor, inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("1208","de","_FILTER_BY","Filtern nach");
INSERT INTO phpn_vocabulary VALUES("1209","en","_FILTER_BY","Filter by");
INSERT INTO phpn_vocabulary VALUES("1210","es","_FILTER_BY","Filtrar por");
INSERT INTO phpn_vocabulary VALUES("1211","de","_FINISH_DATE","Endtermin");
INSERT INTO phpn_vocabulary VALUES("1212","en","_FINISH_DATE","Finish Date");
INSERT INTO phpn_vocabulary VALUES("1213","es","_FINISH_DATE","Fecha de entrega");
INSERT INTO phpn_vocabulary VALUES("1214","de","_FINISH_PUBLISHING","Finish Publishing");
INSERT INTO phpn_vocabulary VALUES("1215","en","_FINISH_PUBLISHING","Finish Publishing");
INSERT INTO phpn_vocabulary VALUES("1216","es","_FINISH_PUBLISHING","Finalizar publicación");
INSERT INTO phpn_vocabulary VALUES("1217","de","_FIRST_NAME","Vorname");
INSERT INTO phpn_vocabulary VALUES("1218","en","_FIRST_NAME","First Name");
INSERT INTO phpn_vocabulary VALUES("1219","es","_FIRST_NAME","Nombre");
INSERT INTO phpn_vocabulary VALUES("1220","de","_FIRST_NAME_EMPTY_ALERT","Vorname darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1221","en","_FIRST_NAME_EMPTY_ALERT","First Name cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1222","es","_FIRST_NAME_EMPTY_ALERT","Nombre no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1223","de","_FIRST_NIGHT","First Night");
INSERT INTO phpn_vocabulary VALUES("1224","en","_FIRST_NIGHT","First Night");
INSERT INTO phpn_vocabulary VALUES("1225","es","_FIRST_NIGHT","Primera Noche");
INSERT INTO phpn_vocabulary VALUES("1226","de","_FIXED_SUM","Festbetrag");
INSERT INTO phpn_vocabulary VALUES("1227","en","_FIXED_SUM","Fixed Sum");
INSERT INTO phpn_vocabulary VALUES("1228","es","_FIXED_SUM","Suma fija");
INSERT INTO phpn_vocabulary VALUES("1229","de","_FOLLOW_US","Folgen Sie uns");
INSERT INTO phpn_vocabulary VALUES("1230","en","_FOLLOW_US","Follow Us");
INSERT INTO phpn_vocabulary VALUES("1231","es","_FOLLOW_US","Siga con nosotros");
INSERT INTO phpn_vocabulary VALUES("1232","de","_FOOTER_IS_EMPTY","Footer darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1233","en","_FOOTER_IS_EMPTY","Footer cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1234","es","_FOOTER_IS_EMPTY","Pie de página no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1235","de","_FORCE_SSL","Force-SSL");
INSERT INTO phpn_vocabulary VALUES("1236","en","_FORCE_SSL","Force SSL");
INSERT INTO phpn_vocabulary VALUES("1237","es","_FORCE_SSL","Fuerza SSL");
INSERT INTO phpn_vocabulary VALUES("1238","de","_FORCE_SSL_ALERT","Force-Website Zugriff auf immer unter SSL (https) treten für ausgewählte Bereiche. Sie oder Website-Besucher nicht in der Lage zu ausgewählten Bereichen unter Nicht-SSL-Zugang. Beachten Sie, müssen Sie SSL-fähigen auf Ihrem Server haben, damit diese Option funktioniert.");
INSERT INTO phpn_vocabulary VALUES("1239","en","_FORCE_SSL_ALERT","Force site access to always occur under SSL (https) for selected areas. You or site visitors will not be able to access selected areas under non-ssl. Note, you must have SSL enabled on your server to make this option works.");
INSERT INTO phpn_vocabulary VALUES("1240","es","_FORCE_SSL_ALERT","Acceso Fuerza del sitio que se produzca siempre bajo SSL (https) para las áreas seleccionadas. Usted o visitantes del sitio no será capaz de acceder a las áreas seleccionadas en virtud de que no sea SSL. Tenga en cuenta, debe tener habilitado SSL en el servidor para que esta opción funcione.");
INSERT INTO phpn_vocabulary VALUES("1241","de","_FORGOT_PASSWORD","Passwort vergessen?");
INSERT INTO phpn_vocabulary VALUES("1242","en","_FORGOT_PASSWORD","Forgot your password?");
INSERT INTO phpn_vocabulary VALUES("1243","es","_FORGOT_PASSWORD","¿Olvidaste tu contraseña?");
INSERT INTO phpn_vocabulary VALUES("1244","de","_FORM","Formular");
INSERT INTO phpn_vocabulary VALUES("1245","en","_FORM","Form");
INSERT INTO phpn_vocabulary VALUES("1246","es","_FORM","Forma");
INSERT INTO phpn_vocabulary VALUES("1247","de","_FOR_BOOKING","Buchung für #");
INSERT INTO phpn_vocabulary VALUES("1248","en","_FOR_BOOKING","for booking #");
INSERT INTO phpn_vocabulary VALUES("1249","es","_FOR_BOOKING","de reserva #");
INSERT INTO phpn_vocabulary VALUES("1250","de","_FOUND_HOTELS","Gefunden Hotels");
INSERT INTO phpn_vocabulary VALUES("1251","en","_FOUND_HOTELS","Found Hotels");
INSERT INTO phpn_vocabulary VALUES("1252","es","_FOUND_HOTELS","Hoteles encontrados");
INSERT INTO phpn_vocabulary VALUES("1253","de","_FOUND_ROOMS","Gefunden Zimmer");
INSERT INTO phpn_vocabulary VALUES("1254","en","_FOUND_ROOMS","Found Rooms");
INSERT INTO phpn_vocabulary VALUES("1255","es","_FOUND_ROOMS","Habitaciones disponibles");
INSERT INTO phpn_vocabulary VALUES("1256","de","_FR","Fr");
INSERT INTO phpn_vocabulary VALUES("1257","en","_FR","Fr");
INSERT INTO phpn_vocabulary VALUES("1258","es","_FR","Vi");
INSERT INTO phpn_vocabulary VALUES("1259","de","_FRI","Fri");
INSERT INTO phpn_vocabulary VALUES("1260","en","_FRI","Fri");
INSERT INTO phpn_vocabulary VALUES("1261","es","_FRI","Vie");
INSERT INTO phpn_vocabulary VALUES("1262","de","_FRIDAY","Freitag");
INSERT INTO phpn_vocabulary VALUES("1263","en","_FRIDAY","Friday");
INSERT INTO phpn_vocabulary VALUES("1264","es","_FRIDAY","viernes");
INSERT INTO phpn_vocabulary VALUES("1265","de","_FROM","von");
INSERT INTO phpn_vocabulary VALUES("1266","en","_FROM","From");
INSERT INTO phpn_vocabulary VALUES("1267","es","_FROM","De");
INSERT INTO phpn_vocabulary VALUES("1268","de","_FROM_TO_DATE_ALERT","Datum An muss gleich oder höher als Datum Von! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1269","en","_FROM_TO_DATE_ALERT","Date \'To\' must be the same or later than date \'From\'! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1270","es","_FROM_TO_DATE_ALERT","Fecha \'A\' debe ser igual o posterior a la fecha \"De: \"! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1271","de","_FULLY_BOOKED","ausgebucht/unavailable");
INSERT INTO phpn_vocabulary VALUES("1272","en","_FULLY_BOOKED","fully booked/unavailable");
INSERT INTO phpn_vocabulary VALUES("1273","es","_FULLY_BOOKED","totalmente reservado/no disponible");
INSERT INTO phpn_vocabulary VALUES("1274","de","_FULL_PRICE","Voller Preis");
INSERT INTO phpn_vocabulary VALUES("1275","en","_FULL_PRICE","Full Price");
INSERT INTO phpn_vocabulary VALUES("1276","es","_FULL_PRICE","De precios completa");
INSERT INTO phpn_vocabulary VALUES("1277","de","_GALLERY","Galerie");
INSERT INTO phpn_vocabulary VALUES("1278","en","_GALLERY","Gallery");
INSERT INTO phpn_vocabulary VALUES("1279","es","_GALLERY","Galería");
INSERT INTO phpn_vocabulary VALUES("1280","de","_GALLERY_MANAGEMENT","Galerie Verwaltung");
INSERT INTO phpn_vocabulary VALUES("1281","en","_GALLERY_MANAGEMENT","Gallery Management");
INSERT INTO phpn_vocabulary VALUES("1282","es","_GALLERY_MANAGEMENT","Galería de Gestión");
INSERT INTO phpn_vocabulary VALUES("1283","de","_GALLERY_SETTINGS","Galerie-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("1284","en","_GALLERY_SETTINGS","Gallery Settings");
INSERT INTO phpn_vocabulary VALUES("1285","es","_GALLERY_SETTINGS","Configuración de la Galería");
INSERT INTO phpn_vocabulary VALUES("1286","de","_GENERAL","Generell");
INSERT INTO phpn_vocabulary VALUES("1287","en","_GENERAL","General");
INSERT INTO phpn_vocabulary VALUES("1288","es","_GENERAL","General");
INSERT INTO phpn_vocabulary VALUES("1289","de","_GENERAL_INFO","Allgemeine Infos");
INSERT INTO phpn_vocabulary VALUES("1290","en","_GENERAL_INFO","General Info");
INSERT INTO phpn_vocabulary VALUES("1291","es","_GENERAL_INFO","Información General");
INSERT INTO phpn_vocabulary VALUES("1292","de","_GENERAL_SETTINGS","Allgemeine Einstellungen");
INSERT INTO phpn_vocabulary VALUES("1293","en","_GENERAL_SETTINGS","General Settings");
INSERT INTO phpn_vocabulary VALUES("1294","es","_GENERAL_SETTINGS","Configuración general");
INSERT INTO phpn_vocabulary VALUES("1295","de","_GENERATE","Erzeugen");
INSERT INTO phpn_vocabulary VALUES("1296","en","_GENERATE","Generate");
INSERT INTO phpn_vocabulary VALUES("1297","es","_GENERATE","Generar");
INSERT INTO phpn_vocabulary VALUES("1298","de","_GLOBAL","Globalen");
INSERT INTO phpn_vocabulary VALUES("1299","en","_GLOBAL","Global");
INSERT INTO phpn_vocabulary VALUES("1300","es","_GLOBAL","Mundial");
INSERT INTO phpn_vocabulary VALUES("1301","de","_GROUP","Gruppe");
INSERT INTO phpn_vocabulary VALUES("1302","en","_GROUP","Group");
INSERT INTO phpn_vocabulary VALUES("1303","es","_GROUP","Grupo");
INSERT INTO phpn_vocabulary VALUES("1304","de","_GROUP_NAME","Name der Gruppe");
INSERT INTO phpn_vocabulary VALUES("1305","en","_GROUP_NAME","Group Name");
INSERT INTO phpn_vocabulary VALUES("1306","es","_GROUP_NAME","Nombre del grupo");
INSERT INTO phpn_vocabulary VALUES("1307","de","_GROUP_TIME_OVERLAPPING_ALERT","Dieser Zeitraum (vollständig oder teilweise) wurde bereits für ausgewählte Gruppe gewählt! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1308","en","_GROUP_TIME_OVERLAPPING_ALERT","This period of time (fully or partially) is already chosen for selected group! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1309","es","_GROUP_TIME_OVERLAPPING_ALERT","Este período de tiempo (total o parcialmente) fue elegido ya para el grupo seleccionado! Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("1310","de","_HDR_FOOTER_TEXT","Fußzeilentext");
INSERT INTO phpn_vocabulary VALUES("1311","en","_HDR_FOOTER_TEXT","Footer Text");
INSERT INTO phpn_vocabulary VALUES("1312","es","_HDR_FOOTER_TEXT","Pie de página de texto");
INSERT INTO phpn_vocabulary VALUES("1313","de","_HDR_HEADER_TEXT","Kopftext");
INSERT INTO phpn_vocabulary VALUES("1314","en","_HDR_HEADER_TEXT","Header Text");
INSERT INTO phpn_vocabulary VALUES("1315","es","_HDR_HEADER_TEXT","Texto de la cabecera");
INSERT INTO phpn_vocabulary VALUES("1316","de","_HDR_SLOGAN_TEXT","Slogan");
INSERT INTO phpn_vocabulary VALUES("1317","en","_HDR_SLOGAN_TEXT","Slogan");
INSERT INTO phpn_vocabulary VALUES("1318","es","_HDR_SLOGAN_TEXT","Eslogan");
INSERT INTO phpn_vocabulary VALUES("1319","de","_HDR_TEMPLATE","Vorlage");
INSERT INTO phpn_vocabulary VALUES("1320","en","_HDR_TEMPLATE","Template");
INSERT INTO phpn_vocabulary VALUES("1321","es","_HDR_TEMPLATE","Plantilla");
INSERT INTO phpn_vocabulary VALUES("1322","de","_HDR_TEXT_DIRECTION","Textrichtung");
INSERT INTO phpn_vocabulary VALUES("1323","en","_HDR_TEXT_DIRECTION","Text Direction");
INSERT INTO phpn_vocabulary VALUES("1324","es","_HDR_TEXT_DIRECTION","Dirección del texto");
INSERT INTO phpn_vocabulary VALUES("1325","de","_HEADER","Kopfzeile");
INSERT INTO phpn_vocabulary VALUES("1326","en","_HEADER","Header");
INSERT INTO phpn_vocabulary VALUES("1327","es","_HEADER","Encabezado");
INSERT INTO phpn_vocabulary VALUES("1328","de","_HEADERS_AND_FOOTERS","Kopf-und Fußzeilen");
INSERT INTO phpn_vocabulary VALUES("1329","en","_HEADERS_AND_FOOTERS","Headers & Footers");
INSERT INTO phpn_vocabulary VALUES("1330","es","_HEADERS_AND_FOOTERS","Encabezados y pies de página");
INSERT INTO phpn_vocabulary VALUES("1331","de","_HEADER_IS_EMPTY","Header können nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1332","en","_HEADER_IS_EMPTY","Header cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1333","es","_HEADER_IS_EMPTY","Encabezado no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1334","de","_HELP","Hilfe");
INSERT INTO phpn_vocabulary VALUES("1335","en","_HELP","Help");
INSERT INTO phpn_vocabulary VALUES("1336","es","_HELP","Ayudar");
INSERT INTO phpn_vocabulary VALUES("1337","de","_HIDDEN","Versteckt");
INSERT INTO phpn_vocabulary VALUES("1338","en","_HIDDEN","Hidden");
INSERT INTO phpn_vocabulary VALUES("1339","es","_HIDDEN","Oculto");
INSERT INTO phpn_vocabulary VALUES("1340","de","_HIDE","Verbergen");
INSERT INTO phpn_vocabulary VALUES("1341","en","_HIDE","Hide");
INSERT INTO phpn_vocabulary VALUES("1342","es","_HIDE","Ocultar");
INSERT INTO phpn_vocabulary VALUES("1343","de","_HOME","Startseite");
INSERT INTO phpn_vocabulary VALUES("1344","en","_HOME","Home");
INSERT INTO phpn_vocabulary VALUES("1345","es","_HOME","Inicio");
INSERT INTO phpn_vocabulary VALUES("1346","de","_HOTEL","Hotel");
INSERT INTO phpn_vocabulary VALUES("1347","en","_HOTEL","Hotel");
INSERT INTO phpn_vocabulary VALUES("1348","es","_HOTEL","Hotel");
INSERT INTO phpn_vocabulary VALUES("1349","de","_HOTELOWNER_WELCOME_TEXT","Willkommen im Hotel Besitzer Control Panel! Mit diesem Control Panel können Sie ganz einfach Ihre Hotels, Kunden, Reservierungen und führen Sie eine vollständige Hotel-Website-Management.");
INSERT INTO phpn_vocabulary VALUES("1350","en","_HOTELOWNER_WELCOME_TEXT","Welcome to Hotel Owner Control Panel! With this Control Panel you can easily manage your hotels, customers, reservations and perform a full hotel site management.");
INSERT INTO phpn_vocabulary VALUES("1351","es","_HOTELOWNER_WELCOME_TEXT","Bienvenido al panel de control del propietario de Hotel! Con este panel de control se pueden manejar fácilmente sus hoteles, los clientes, reservas y realizar una gestión hotelera completa del sitio.");
INSERT INTO phpn_vocabulary VALUES("1352","de","_HOTELS","Hotels");
INSERT INTO phpn_vocabulary VALUES("1353","en","_HOTELS","Hotels");
INSERT INTO phpn_vocabulary VALUES("1354","es","_HOTELS","Hoteles");
INSERT INTO phpn_vocabulary VALUES("1355","de","_HOTELS_AND_ROMS","Hotels und Zimmer");
INSERT INTO phpn_vocabulary VALUES("1356","en","_HOTELS_AND_ROMS","Hotels and Rooms");
INSERT INTO phpn_vocabulary VALUES("1357","es","_HOTELS_AND_ROMS","Hoteles y Habitaciones");
INSERT INTO phpn_vocabulary VALUES("1358","de","_HOTELS_INFO","Infos über Hotels");
INSERT INTO phpn_vocabulary VALUES("1359","en","_HOTELS_INFO","Hotels Info");
INSERT INTO phpn_vocabulary VALUES("1360","es","_HOTELS_INFO","Hoteles Información");
INSERT INTO phpn_vocabulary VALUES("1361","de","_HOTELS_MANAGEMENT","Hotels-Management");
INSERT INTO phpn_vocabulary VALUES("1362","en","_HOTELS_MANAGEMENT","Hotels Management");
INSERT INTO phpn_vocabulary VALUES("1363","es","_HOTELS_MANAGEMENT","Hoteles de gestión");
INSERT INTO phpn_vocabulary VALUES("1364","de","_HOTEL_DELETE_ALERT","Sind Sie sicher, dass Sie dieses Hotel wirklich löschen? Denken Sie daran: nach Abschluss dieser Aktion alle Daten in diesem Hotel konnte nicht wiederhergestellt werden!");
INSERT INTO phpn_vocabulary VALUES("1365","en","_HOTEL_DELETE_ALERT","Are you sure you want to delete this hotel? Remember: after completing this action all related data to this hotel could not be restored!");
INSERT INTO phpn_vocabulary VALUES("1366","es","_HOTEL_DELETE_ALERT","¿Está seguro que desea eliminar este hotel? Recuerde: después de completar esta acción no se podrían restaurar todos los datos relacionados con este hotel!");
INSERT INTO phpn_vocabulary VALUES("1367","de","_HOTEL_DESCRIPTION","Beschreibung des Hotels");
INSERT INTO phpn_vocabulary VALUES("1368","en","_HOTEL_DESCRIPTION","Hotel Description");
INSERT INTO phpn_vocabulary VALUES("1369","es","_HOTEL_DESCRIPTION","Descripción del hotel");
INSERT INTO phpn_vocabulary VALUES("1370","de","_HOTEL_INFO","Hotel-Infos");
INSERT INTO phpn_vocabulary VALUES("1371","en","_HOTEL_INFO","Hotel Info");
INSERT INTO phpn_vocabulary VALUES("1372","es","_HOTEL_INFO","Información del Hotel");
INSERT INTO phpn_vocabulary VALUES("1373","de","_HOTEL_MANAGEMENT","Hotel Management");
INSERT INTO phpn_vocabulary VALUES("1374","en","_HOTEL_MANAGEMENT","Hotel Management");
INSERT INTO phpn_vocabulary VALUES("1375","es","_HOTEL_MANAGEMENT","Dirección Hotelera");
INSERT INTO phpn_vocabulary VALUES("1376","de","_HOTEL_OWNER","Kostenlos anmelden");
INSERT INTO phpn_vocabulary VALUES("1377","en","_HOTEL_OWNER","Hotel Owner");
INSERT INTO phpn_vocabulary VALUES("1378","es","_HOTEL_OWNER","Propietario de Hotel");
INSERT INTO phpn_vocabulary VALUES("1379","de","_HOTEL_OWNERS","Hotelinhaber");
INSERT INTO phpn_vocabulary VALUES("1380","en","_HOTEL_OWNERS","Hotel Owners");
INSERT INTO phpn_vocabulary VALUES("1381","es","_HOTEL_OWNERS","Los propietarios del hotel");
INSERT INTO phpn_vocabulary VALUES("1382","de","_HOTEL_RESERVATION_ID","Hotelreservierung ID");
INSERT INTO phpn_vocabulary VALUES("1383","en","_HOTEL_RESERVATION_ID","Hotel Reservation ID");
INSERT INTO phpn_vocabulary VALUES("1384","es","_HOTEL_RESERVATION_ID","Hotel ID reserva");
INSERT INTO phpn_vocabulary VALUES("1385","de","_HOUR","Stunde");
INSERT INTO phpn_vocabulary VALUES("1386","en","_HOUR","Hour");
INSERT INTO phpn_vocabulary VALUES("1387","es","_HOUR","Horas");
INSERT INTO phpn_vocabulary VALUES("1388","de","_HOURS","Stunden");
INSERT INTO phpn_vocabulary VALUES("1389","en","_HOURS","hours");
INSERT INTO phpn_vocabulary VALUES("1390","es","_HOURS","horas");
INSERT INTO phpn_vocabulary VALUES("1391","de","_ICON_IMAGE","Icon Bild");
INSERT INTO phpn_vocabulary VALUES("1392","en","_ICON_IMAGE","Icon image");
INSERT INTO phpn_vocabulary VALUES("1393","es","_ICON_IMAGE","Imagen de icono");
INSERT INTO phpn_vocabulary VALUES("1394","de","_IMAGE","Bild");
INSERT INTO phpn_vocabulary VALUES("1395","en","_IMAGE","Image");
INSERT INTO phpn_vocabulary VALUES("1396","es","_IMAGE","Imagen");
INSERT INTO phpn_vocabulary VALUES("1397","de","_IMAGES","Bilder");
INSERT INTO phpn_vocabulary VALUES("1398","en","_IMAGES","Images");
INSERT INTO phpn_vocabulary VALUES("1399","es","_IMAGES","Imágenes");
INSERT INTO phpn_vocabulary VALUES("1400","de","_IMAGE_VERIFICATION","Bildüberprüfung");
INSERT INTO phpn_vocabulary VALUES("1401","en","_IMAGE_VERIFICATION","Image verification");
INSERT INTO phpn_vocabulary VALUES("1402","es","_IMAGE_VERIFICATION","Imagen de verificación");
INSERT INTO phpn_vocabulary VALUES("1403","de","_IMAGE_VERIFY_EMPTY","Sie müssen bild verifizierungscode!");
INSERT INTO phpn_vocabulary VALUES("1404","en","_IMAGE_VERIFY_EMPTY","You must enter image verification code!");
INSERT INTO phpn_vocabulary VALUES("1405","es","_IMAGE_VERIFY_EMPTY","Debe introducir el código de verificación de la imagen!");
INSERT INTO phpn_vocabulary VALUES("1406","de","_INCOME","einkommen");
INSERT INTO phpn_vocabulary VALUES("1407","en","_INCOME","Income");
INSERT INTO phpn_vocabulary VALUES("1408","es","_INCOME","Ingresos");
INSERT INTO phpn_vocabulary VALUES("1409","de","_INFO_AND_STATISTICS","Informationen und Statistiken");
INSERT INTO phpn_vocabulary VALUES("1410","en","_INFO_AND_STATISTICS","Information and Statistics");
INSERT INTO phpn_vocabulary VALUES("1411","es","_INFO_AND_STATISTICS","Información y Estadística");
INSERT INTO phpn_vocabulary VALUES("1412","de","_INITIAL_FEE","Ausgabeaufschlag");
INSERT INTO phpn_vocabulary VALUES("1413","en","_INITIAL_FEE","Initial Fee");
INSERT INTO phpn_vocabulary VALUES("1414","es","_INITIAL_FEE","Cuota inicial");
INSERT INTO phpn_vocabulary VALUES("1415","de","_INSTALL","Installieren");
INSERT INTO phpn_vocabulary VALUES("1416","en","_INSTALL","Install");
INSERT INTO phpn_vocabulary VALUES("1417","es","_INSTALL","Instale");
INSERT INTO phpn_vocabulary VALUES("1418","de","_INSTALLED","Installierte");
INSERT INTO phpn_vocabulary VALUES("1419","en","_INSTALLED","Installed");
INSERT INTO phpn_vocabulary VALUES("1420","es","_INSTALLED","Instalado");
INSERT INTO phpn_vocabulary VALUES("1421","de","_INSTALL_PHP_EXISTS","Datei <b>install.php</b> und/oder direkt <b>installieren</b> noch existieren. Aus Sicherheitsgründen ziehen Sie bitte sofort!");
INSERT INTO phpn_vocabulary VALUES("1422","en","_INSTALL_PHP_EXISTS","File <b>install.php</b> and/or directory <b>install/</b> still exists. For security reasons please remove them immediately!");
INSERT INTO phpn_vocabulary VALUES("1423","es","_INSTALL_PHP_EXISTS","Archivo <b>install.php</b> y/o directorio <b>instalar/</b> siguen existiendo. Por razones de seguridad por favor, elimine de inmediato!");
INSERT INTO phpn_vocabulary VALUES("1424","de","_INTEGRATION","Integration");
INSERT INTO phpn_vocabulary VALUES("1425","en","_INTEGRATION","Integration");
INSERT INTO phpn_vocabulary VALUES("1426","es","_INTEGRATION","Integración");
INSERT INTO phpn_vocabulary VALUES("1427","de","_INTEGRATION_MESSAGE","Kopieren Sie den Code unten ein und legte es an der entsprechenden Stelle Ihrer Website, um eine <b>Suche Verfügbarkeit</b>-Block zu bekommen.");
INSERT INTO phpn_vocabulary VALUES("1428","en","_INTEGRATION_MESSAGE","Copy the code below and put it in the appropriate place of your web site to get a <b>Search Availability</b> block.");
INSERT INTO phpn_vocabulary VALUES("1429","es","_INTEGRATION_MESSAGE","Copie el código abajo y lo puso en el lugar correspondiente de su sitio web para obtener una disponibilidad <b>Buscar</b> bloque.");
INSERT INTO phpn_vocabulary VALUES("1430","de","_INTERNAL_USE_TOOLTIP","Nur für internen Gebrauch");
INSERT INTO phpn_vocabulary VALUES("1431","en","_INTERNAL_USE_TOOLTIP","For internal use only");
INSERT INTO phpn_vocabulary VALUES("1432","es","_INTERNAL_USE_TOOLTIP","Sólo para uso interno");
INSERT INTO phpn_vocabulary VALUES("1433","de","_INVALID_FILE_SIZE","Ungültige Dateigröße: _FILE_SIZE_ (max. erlaubt: _MAX_ALLOWED_)");
INSERT INTO phpn_vocabulary VALUES("1434","en","_INVALID_FILE_SIZE","Invalid file size: _FILE_SIZE_ (max. allowed: _MAX_ALLOWED_)");
INSERT INTO phpn_vocabulary VALUES("1435","es","_INVALID_FILE_SIZE","Tamaño de archivo no válido: _FILE_SIZE_ (máximo permitido: _MAX_ALLOWED_)");
INSERT INTO phpn_vocabulary VALUES("1436","de","_INVALID_IMAGE_FILE_TYPE","Hochgeladene Datei ist kein gültiges Bild! Bitte geben Sie erneut.");
INSERT INTO phpn_vocabulary VALUES("1437","en","_INVALID_IMAGE_FILE_TYPE","Uploaded file is not a valid image! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1438","es","_INVALID_IMAGE_FILE_TYPE","El archivo cargado no es una imagen válida! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1439","de","_INVOICE","Rechnung");
INSERT INTO phpn_vocabulary VALUES("1440","en","_INVOICE","Invoice");
INSERT INTO phpn_vocabulary VALUES("1441","es","_INVOICE","Factura");
INSERT INTO phpn_vocabulary VALUES("1442","de","_INVOICE_SENT_SUCCESS","Die Rechnung wurde erfolgreich an den Kunden versendet!");
INSERT INTO phpn_vocabulary VALUES("1443","en","_INVOICE_SENT_SUCCESS","The invoice has been successfully sent to the customer!");
INSERT INTO phpn_vocabulary VALUES("1444","es","_INVOICE_SENT_SUCCESS","La factura se envió con éxito al cliente!");
INSERT INTO phpn_vocabulary VALUES("1445","de","_IN_PRODUCTS","In Produkte");
INSERT INTO phpn_vocabulary VALUES("1446","en","_IN_PRODUCTS","In Products");
INSERT INTO phpn_vocabulary VALUES("1447","es","_IN_PRODUCTS","En Productos");
INSERT INTO phpn_vocabulary VALUES("1448","de","_IP_ADDRESS","IP-Adresse");
INSERT INTO phpn_vocabulary VALUES("1449","en","_IP_ADDRESS","IP Address");
INSERT INTO phpn_vocabulary VALUES("1450","es","_IP_ADDRESS","Dirección IP");
INSERT INTO phpn_vocabulary VALUES("1451","de","_IP_ADDRESS_BLOCKED","Diese IP-Adresse wurde gesperrt! Um dieses Problem zu beheben");
INSERT INTO phpn_vocabulary VALUES("1452","en","_IP_ADDRESS_BLOCKED","Your IP Address is blocked! To resolve this problem, please contact the site administrator.");
INSERT INTO phpn_vocabulary VALUES("1453","es","_IP_ADDRESS_BLOCKED","Su dirección IP está bloqueada! Para resolver este problema, póngase en contacto con el administrador del sitio.");
INSERT INTO phpn_vocabulary VALUES("1454","de","_IS_DEFAULT","Ist standardmäßig");
INSERT INTO phpn_vocabulary VALUES("1455","en","_IS_DEFAULT","Is default");
INSERT INTO phpn_vocabulary VALUES("1456","es","_IS_DEFAULT","Es la opción predeterminada");
INSERT INTO phpn_vocabulary VALUES("1457","de","_ITEMS","Artikel");
INSERT INTO phpn_vocabulary VALUES("1458","en","_ITEMS","Items");
INSERT INTO phpn_vocabulary VALUES("1459","es","_ITEMS","Artículos");
INSERT INTO phpn_vocabulary VALUES("1460","de","_ITEMS_LC","begriffe");
INSERT INTO phpn_vocabulary VALUES("1461","en","_ITEMS_LC","items");
INSERT INTO phpn_vocabulary VALUES("1462","es","_ITEMS_LC","artículos");
INSERT INTO phpn_vocabulary VALUES("1463","de","_ITEM_NAME","Einzelteil-Name");
INSERT INTO phpn_vocabulary VALUES("1464","en","_ITEM_NAME","Item Name");
INSERT INTO phpn_vocabulary VALUES("1465","es","_ITEM_NAME","Nombre del artículo");
INSERT INTO phpn_vocabulary VALUES("1466","de","_JANUARY","Januar");
INSERT INTO phpn_vocabulary VALUES("1467","en","_JANUARY","January");
INSERT INTO phpn_vocabulary VALUES("1468","es","_JANUARY","Enero");
INSERT INTO phpn_vocabulary VALUES("1469","de","_JULY","Juli");
INSERT INTO phpn_vocabulary VALUES("1470","en","_JULY","July");
INSERT INTO phpn_vocabulary VALUES("1471","es","_JULY","Julio");
INSERT INTO phpn_vocabulary VALUES("1472","de","_JUNE","Juni");
INSERT INTO phpn_vocabulary VALUES("1473","en","_JUNE","June");
INSERT INTO phpn_vocabulary VALUES("1474","es","_JUNE","Junio");
INSERT INTO phpn_vocabulary VALUES("1475","de","_KEY","Schlüssel");
INSERT INTO phpn_vocabulary VALUES("1476","en","_KEY","Key");
INSERT INTO phpn_vocabulary VALUES("1477","es","_KEY","Llave");
INSERT INTO phpn_vocabulary VALUES("1478","de","_KEYWORDS","Stichwort");
INSERT INTO phpn_vocabulary VALUES("1479","en","_KEYWORDS","Keywords");
INSERT INTO phpn_vocabulary VALUES("1480","es","_KEYWORDS","Palabras clave");
INSERT INTO phpn_vocabulary VALUES("1481","de","_KEY_DISPLAY_TYPE","Key Display-Typ");
INSERT INTO phpn_vocabulary VALUES("1482","en","_KEY_DISPLAY_TYPE","Key display type");
INSERT INTO phpn_vocabulary VALUES("1483","es","_KEY_DISPLAY_TYPE","Tipo de pantalla Key");
INSERT INTO phpn_vocabulary VALUES("1484","de","_LANGUAGE","Sprache");
INSERT INTO phpn_vocabulary VALUES("1485","en","_LANGUAGE","Language");
INSERT INTO phpn_vocabulary VALUES("1486","es","_LANGUAGE","Habla");
INSERT INTO phpn_vocabulary VALUES("1487","de","_LANGUAGES","Sprachen");
INSERT INTO phpn_vocabulary VALUES("1488","en","_LANGUAGES","Languages");
INSERT INTO phpn_vocabulary VALUES("1489","es","_LANGUAGES","Idiomas");
INSERT INTO phpn_vocabulary VALUES("1490","de","_LANGUAGES_SETTINGS","Sprachen-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("1491","en","_LANGUAGES_SETTINGS","Languages Settings");
INSERT INTO phpn_vocabulary VALUES("1492","es","_LANGUAGES_SETTINGS","Configuración de Idiomas");
INSERT INTO phpn_vocabulary VALUES("1493","de","_LANGUAGE_ADDED","Neue Sprache wurde hinzugefügt!");
INSERT INTO phpn_vocabulary VALUES("1494","en","_LANGUAGE_ADDED","New language has been successfully added!");
INSERT INTO phpn_vocabulary VALUES("1495","es","_LANGUAGE_ADDED","Un nuevo lenguaje se ha añadido!");
INSERT INTO phpn_vocabulary VALUES("1496","de","_LANGUAGE_ADD_NEW","Neue Sprache hinzufügen");
INSERT INTO phpn_vocabulary VALUES("1497","en","_LANGUAGE_ADD_NEW","Add New Language");
INSERT INTO phpn_vocabulary VALUES("1498","es","_LANGUAGE_ADD_NEW","Añadir un nuevo idioma");
INSERT INTO phpn_vocabulary VALUES("1499","de","_LANGUAGE_EDIT","Bearbeiten Sprache");
INSERT INTO phpn_vocabulary VALUES("1500","en","_LANGUAGE_EDIT","Edit Language");
INSERT INTO phpn_vocabulary VALUES("1501","es","_LANGUAGE_EDIT","Editar Idioma");
INSERT INTO phpn_vocabulary VALUES("1502","de","_LANGUAGE_EDITED","Language Daten wurden erfolgreich aktualisiert!");
INSERT INTO phpn_vocabulary VALUES("1503","en","_LANGUAGE_EDITED","Language data has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("1504","es","_LANGUAGE_EDITED","Idioma de datos se ha actualizado correctamente!");
INSERT INTO phpn_vocabulary VALUES("1505","de","_LANGUAGE_NAME","Sprache Name");
INSERT INTO phpn_vocabulary VALUES("1506","en","_LANGUAGE_NAME","Language Name");
INSERT INTO phpn_vocabulary VALUES("1507","es","_LANGUAGE_NAME","Idioma Nombre");
INSERT INTO phpn_vocabulary VALUES("1508","de","_LANG_ABBREV_EMPTY","Sprachkürzel darf nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("1509","en","_LANG_ABBREV_EMPTY","Language abbreviation cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("1510","es","_LANG_ABBREV_EMPTY","Abreviatura del idioma no puede estar vacía!");
INSERT INTO phpn_vocabulary VALUES("1511","de","_LANG_DELETED","Sprache wurde erfolgreich gelöscht!");
INSERT INTO phpn_vocabulary VALUES("1512","en","_LANG_DELETED","Language has been successfully deleted!");
INSERT INTO phpn_vocabulary VALUES("1513","es","_LANG_DELETED","Idioma ha sido borrado!");
INSERT INTO phpn_vocabulary VALUES("1514","de","_LANG_DELETE_LAST_ERROR","Sie können nicht löschen letzten Sprache!");
INSERT INTO phpn_vocabulary VALUES("1515","en","_LANG_DELETE_LAST_ERROR","You cannot delete last language!");
INSERT INTO phpn_vocabulary VALUES("1516","es","_LANG_DELETE_LAST_ERROR","No se puede eliminar Lengua!");
INSERT INTO phpn_vocabulary VALUES("1517","de","_LANG_DELETE_WARNING","Sind Sie sicher");
INSERT INTO phpn_vocabulary VALUES("1518","en","_LANG_DELETE_WARNING","Are you sure you want to remove this language? This operation will delete all language vocabulary!");
INSERT INTO phpn_vocabulary VALUES("1519","es","_LANG_DELETE_WARNING","¿Estás seguro de que deseas eliminar este idioma? Esta operación eliminará todos vocabulario del idioma!");
INSERT INTO phpn_vocabulary VALUES("1520","de","_LANG_MISSED","Verpasste Sprache zu aktualisieren! Bitte versuchen Sie es erneut.");
INSERT INTO phpn_vocabulary VALUES("1521","en","_LANG_MISSED","Missed language to update! Please, try again.");
INSERT INTO phpn_vocabulary VALUES("1522","es","_LANG_MISSED","El lenguaje perdido de actualizar! Por favor, inténtelo de nuevo.");
INSERT INTO phpn_vocabulary VALUES("1523","de","_LANG_NAME_EMPTY","Name der Sprache darf nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("1524","en","_LANG_NAME_EMPTY","Language name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("1525","es","_LANG_NAME_EMPTY","Nombre de idioma no puede estar vacía!");
INSERT INTO phpn_vocabulary VALUES("1526","de","_LANG_NAME_EXISTS","Sprache mit solchen Namen existiert bereits! Bitte wählen Sie einen anderen.");
INSERT INTO phpn_vocabulary VALUES("1527","en","_LANG_NAME_EXISTS","Language with such name already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("1528","es","_LANG_NAME_EXISTS","De idiomas con ese nombre ya existe! Por favor, elija otro.");
INSERT INTO phpn_vocabulary VALUES("1529","de","_LANG_NOT_DELETED","Sprache wurde nicht gelöscht!");
INSERT INTO phpn_vocabulary VALUES("1530","en","_LANG_NOT_DELETED","Language has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("1531","es","_LANG_NOT_DELETED","El idioma no fue eliminado!");
INSERT INTO phpn_vocabulary VALUES("1532","de","_LANG_ORDER_CHANGED","Sprache Bestellung wurde erfolgreich geändert!");
INSERT INTO phpn_vocabulary VALUES("1533","en","_LANG_ORDER_CHANGED","Language order has been successfully changed!");
INSERT INTO phpn_vocabulary VALUES("1534","es","_LANG_ORDER_CHANGED","Orden de los idiomas se ha cambiado!");
INSERT INTO phpn_vocabulary VALUES("1535","de","_LAST_CURRENCY_ALERT","Sie können nicht löschen letzten aktiven Währung!");
INSERT INTO phpn_vocabulary VALUES("1536","en","_LAST_CURRENCY_ALERT","You cannot delete last active currency!");
INSERT INTO phpn_vocabulary VALUES("1537","es","_LAST_CURRENCY_ALERT","No se puede eliminar la moneda activo de último!");
INSERT INTO phpn_vocabulary VALUES("1538","de","_LAST_HOTEL_ALERT","Sie können nicht löschen letzten aktiven hotel Rekord!");
INSERT INTO phpn_vocabulary VALUES("1539","en","_LAST_HOTEL_ALERT","You cannot delete last active hotel record!");
INSERT INTO phpn_vocabulary VALUES("1540","es","_LAST_HOTEL_ALERT","No se puede eliminar el último registro activo de hoteles!");
INSERT INTO phpn_vocabulary VALUES("1541","de","_LAST_LOGGED_IP","Zuletzt eingeloggt IP");
INSERT INTO phpn_vocabulary VALUES("1542","en","_LAST_LOGGED_IP","Last logged IP");
INSERT INTO phpn_vocabulary VALUES("1543","es","_LAST_LOGGED_IP","Última sesión de IP");
INSERT INTO phpn_vocabulary VALUES("1544","de","_LAST_LOGIN","Letzter Login");
INSERT INTO phpn_vocabulary VALUES("1545","en","_LAST_LOGIN","Last Login");
INSERT INTO phpn_vocabulary VALUES("1546","es","_LAST_LOGIN","Último inicio de sesión");
INSERT INTO phpn_vocabulary VALUES("1547","de","_LAST_NAME","Nachname");
INSERT INTO phpn_vocabulary VALUES("1548","en","_LAST_NAME","Last Name");
INSERT INTO phpn_vocabulary VALUES("1549","es","_LAST_NAME","Apellido");
INSERT INTO phpn_vocabulary VALUES("1550","de","_LAST_NAME_EMPTY_ALERT","Nachname darf nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("1551","en","_LAST_NAME_EMPTY_ALERT","Last Name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("1552","es","_LAST_NAME_EMPTY_ALERT","Apellido no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1553","de","_LAST_RUN","Letzter Lauf");
INSERT INTO phpn_vocabulary VALUES("1554","en","_LAST_RUN","Last run");
INSERT INTO phpn_vocabulary VALUES("1555","es","_LAST_RUN","La última ejecución");
INSERT INTO phpn_vocabulary VALUES("1556","de","_LAYOUT","Layout");
INSERT INTO phpn_vocabulary VALUES("1557","en","_LAYOUT","Layout");
INSERT INTO phpn_vocabulary VALUES("1558","es","_LAYOUT","Diseño");
INSERT INTO phpn_vocabulary VALUES("1559","de","_LEAVE_YOUR_COMMENT","Lassen Sie Ihren Kommentar");
INSERT INTO phpn_vocabulary VALUES("1560","en","_LEAVE_YOUR_COMMENT","Leave your comment");
INSERT INTO phpn_vocabulary VALUES("1561","es","_LEAVE_YOUR_COMMENT","Deja tu comentario");
INSERT INTO phpn_vocabulary VALUES("1562","de","_LEFT","Links");
INSERT INTO phpn_vocabulary VALUES("1563","en","_LEFT","Left");
INSERT INTO phpn_vocabulary VALUES("1564","es","_LEFT","Izquierdo");
INSERT INTO phpn_vocabulary VALUES("1565","de","_LEFT_TO_RIGHT","LTR (von links nach rechts)");
INSERT INTO phpn_vocabulary VALUES("1566","en","_LEFT_TO_RIGHT","LTR (left-to-right)");
INSERT INTO phpn_vocabulary VALUES("1567","es","_LEFT_TO_RIGHT","LTR (de izquierda a derecha)");
INSERT INTO phpn_vocabulary VALUES("1568","de","_LEGEND","Legende");
INSERT INTO phpn_vocabulary VALUES("1569","en","_LEGEND","Legend");
INSERT INTO phpn_vocabulary VALUES("1570","es","_LEGEND","Leyenda");
INSERT INTO phpn_vocabulary VALUES("1571","de","_LEGEND_CANCELED","Auftrag wurde von admin abgesagt und den Raum wieder verfügbar ist auf der Suche");
INSERT INTO phpn_vocabulary VALUES("1572","en","_LEGEND_CANCELED","Order is canceled by admin and the room is available again in search");
INSERT INTO phpn_vocabulary VALUES("1573","es","_LEGEND_CANCELED","Orden fue cancelada por correo-e y la habitación está disponible de nuevo en busca");
INSERT INTO phpn_vocabulary VALUES("1574","de","_LEGEND_COMPLETED","Geld bezahlt wurde (ganz oder teilweise) und damit abgeschlossen");
INSERT INTO phpn_vocabulary VALUES("1575","en","_LEGEND_COMPLETED","Money is paid (fully or partially) and order completed");
INSERT INTO phpn_vocabulary VALUES("1576","es","_LEGEND_COMPLETED","El dinero fue pagado (total o parcialmente) y para completar");
INSERT INTO phpn_vocabulary VALUES("1577","de","_LEGEND_PAYMENT_ERROR","Ein Fehler beim Verarbeiten Kundenzahlungen");
INSERT INTO phpn_vocabulary VALUES("1578","en","_LEGEND_PAYMENT_ERROR","An error occurred while processing customer payments");
INSERT INTO phpn_vocabulary VALUES("1579","es","_LEGEND_PAYMENT_ERROR","Se produjo un error durante el procesamiento de pagos de los clientes");
INSERT INTO phpn_vocabulary VALUES("1580","de","_LEGEND_PENDING","Die Buchung erstellt wurde noch nicht bestätigt und reserviert");
INSERT INTO phpn_vocabulary VALUES("1581","en","_LEGEND_PENDING","The booking has been created, but not yet been confirmed and reserved");
INSERT INTO phpn_vocabulary VALUES("1582","es","_LEGEND_PENDING","La reserva ha sido creada todavía no ha sido confirmado y reservado");
INSERT INTO phpn_vocabulary VALUES("1583","de","_LEGEND_PREBOOKING","Dies ist ein System-Status für eine Buchung, die im Gange ist, oder wurde nicht abgeschlossen (Raum wurde in der Reservation Warenkorb hinzugefügt worden, aber noch nicht reserviert)");
INSERT INTO phpn_vocabulary VALUES("1584","en","_LEGEND_PREBOOKING","This is a system status for a booking that is in progress, or was not completed (room has been added to Reservation Cart, but still not reserved)");
INSERT INTO phpn_vocabulary VALUES("1585","es","_LEGEND_PREBOOKING","Este es un estado del sistema de reserva que está en curso, o no se completó (habitación ha sido añadido a la cesta de reserva, pero todavía no reservados)");
INSERT INTO phpn_vocabulary VALUES("1586","de","_LEGEND_REFUNDED","Bestellen Sie erstattet wurde und das Zimmer wieder verfügbar ist auf der Suche");
INSERT INTO phpn_vocabulary VALUES("1587","en","_LEGEND_REFUNDED","Order has been refunded and the room is available again in search");
INSERT INTO phpn_vocabulary VALUES("1588","es","_LEGEND_REFUNDED","Orden fue devuelto y la habitación está disponible de nuevo en la búsqueda");
INSERT INTO phpn_vocabulary VALUES("1589","de","_LEGEND_RESERVED","Das Zimmer ist reserviert, aber um war noch nicht bezahlt (in Vorbereitung)");
INSERT INTO phpn_vocabulary VALUES("1590","en","_LEGEND_RESERVED","Room is reserved, but order is not paid yet (pending)");
INSERT INTO phpn_vocabulary VALUES("1591","es","_LEGEND_RESERVED","Habitación está reservada, pero para que no se pagó aún (pendiente)");
INSERT INTO phpn_vocabulary VALUES("1592","de","_LICENSE","Lizenz");
INSERT INTO phpn_vocabulary VALUES("1593","en","_LICENSE","License");
INSERT INTO phpn_vocabulary VALUES("1594","es","_LICENSE","Licencia");
INSERT INTO phpn_vocabulary VALUES("1595","de","_LINK","Link");
INSERT INTO phpn_vocabulary VALUES("1596","en","_LINK","Link");
INSERT INTO phpn_vocabulary VALUES("1597","es","_LINK","Link");
INSERT INTO phpn_vocabulary VALUES("1598","de","_LINK_PARAMETER","Link-Parameter");
INSERT INTO phpn_vocabulary VALUES("1599","en","_LINK_PARAMETER","Link Parameter");
INSERT INTO phpn_vocabulary VALUES("1600","es","_LINK_PARAMETER","Enlace de parámetros");
INSERT INTO phpn_vocabulary VALUES("1601","de","_LOADING","laden");
INSERT INTO phpn_vocabulary VALUES("1602","en","_LOADING","loading");
INSERT INTO phpn_vocabulary VALUES("1603","es","_LOADING","de carga");
INSERT INTO phpn_vocabulary VALUES("1604","de","_LOCAL_TIME","Ortszeit");
INSERT INTO phpn_vocabulary VALUES("1605","en","_LOCAL_TIME","Local Time");
INSERT INTO phpn_vocabulary VALUES("1606","es","_LOCAL_TIME","Hora Local");
INSERT INTO phpn_vocabulary VALUES("1607","de","_LOCATION","Lage");
INSERT INTO phpn_vocabulary VALUES("1608","en","_LOCATION","Location");
INSERT INTO phpn_vocabulary VALUES("1609","es","_LOCATION","Ubicación");
INSERT INTO phpn_vocabulary VALUES("1610","de","_LOCATIONS","Standorte");
INSERT INTO phpn_vocabulary VALUES("1611","en","_LOCATIONS","Locations");
INSERT INTO phpn_vocabulary VALUES("1612","es","_LOCATIONS","Ubicaciones");
INSERT INTO phpn_vocabulary VALUES("1613","de","_LOCATION_NAME","Ort Name");
INSERT INTO phpn_vocabulary VALUES("1614","en","_LOCATION_NAME","Location Name");
INSERT INTO phpn_vocabulary VALUES("1615","es","_LOCATION_NAME","Nombre Ubicación");
INSERT INTO phpn_vocabulary VALUES("1616","de","_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("1617","en","_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("1618","es","_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("1619","de","_LOGINS","Login");
INSERT INTO phpn_vocabulary VALUES("1620","en","_LOGINS","Logins");
INSERT INTO phpn_vocabulary VALUES("1621","es","_LOGINS","Iniciar sesión");
INSERT INTO phpn_vocabulary VALUES("1622","de","_LOGIN_PAGE_MSG","Verwenden Sie einen gültigen Administrator-Benutzernamen und das Passwort für den Zugriff auf das Administrator-Back-End zu bekommen. <br><br>Zurück zur Seite <a href=\'index.php\'>Home Page</a> <br><br><img align=\'center\' src=\'images/lock.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("1623","en","_LOGIN_PAGE_MSG","Use a valid administrator username and password to get access to the Administrator Back-End.<br><br>Return to site <a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/lock.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("1624","es","_LOGIN_PAGE_MSG","Utilice un nombre de usuario administrador y contraseña válidos para acceder al Administrador de servicios de fondo.<br><br>Volver a la página <a href=\'index.php\'>Home Page</a> <br><br><img align=\'center\' src=\'images/lock.png\' alt =\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("1625","de","_LONG_DESCRIPTION","Lange Beschreibung");
INSERT INTO phpn_vocabulary VALUES("1626","en","_LONG_DESCRIPTION","Long Description");
INSERT INTO phpn_vocabulary VALUES("1627","es","_LONG_DESCRIPTION","Descripción completa");
INSERT INTO phpn_vocabulary VALUES("1628","de","_LOOK_IN","Suchen in");
INSERT INTO phpn_vocabulary VALUES("1629","en","_LOOK_IN","Look in");
INSERT INTO phpn_vocabulary VALUES("1630","es","_LOOK_IN","Buscar en");
INSERT INTO phpn_vocabulary VALUES("1631","de","_MAILER","Mailer");
INSERT INTO phpn_vocabulary VALUES("1632","en","_MAILER","Mailer");
INSERT INTO phpn_vocabulary VALUES("1633","es","_MAILER","Mailer");
INSERT INTO phpn_vocabulary VALUES("1634","de","_MAIN","Haupt");
INSERT INTO phpn_vocabulary VALUES("1635","en","_MAIN","Main");
INSERT INTO phpn_vocabulary VALUES("1636","es","_MAIN","Principal");
INSERT INTO phpn_vocabulary VALUES("1637","de","_MAIN_ADMIN","Haupt-Admin");
INSERT INTO phpn_vocabulary VALUES("1638","en","_MAIN_ADMIN","Main Admin");
INSERT INTO phpn_vocabulary VALUES("1639","es","_MAIN_ADMIN","Administrador principal");
INSERT INTO phpn_vocabulary VALUES("1640","de","_MAKE_RESERVATION","Machen а Reservierung");
INSERT INTO phpn_vocabulary VALUES("1641","en","_MAKE_RESERVATION","Make а Reservation");
INSERT INTO phpn_vocabulary VALUES("1642","es","_MAKE_RESERVATION","Haga а Reserva");
INSERT INTO phpn_vocabulary VALUES("1643","de","_MANAGE_TEMPLATES","Vorlagen verwalten");
INSERT INTO phpn_vocabulary VALUES("1644","en","_MANAGE_TEMPLATES","Manage Templates");
INSERT INTO phpn_vocabulary VALUES("1645","es","_MANAGE_TEMPLATES","Administrar plantillas");
INSERT INTO phpn_vocabulary VALUES("1646","de","_MAP_CODE","Karte Code");
INSERT INTO phpn_vocabulary VALUES("1647","en","_MAP_CODE","Map Code");
INSERT INTO phpn_vocabulary VALUES("1648","es","_MAP_CODE","Map Code");
INSERT INTO phpn_vocabulary VALUES("1649","de","_MAP_OVERLAY","Landkarten-Overlay");
INSERT INTO phpn_vocabulary VALUES("1650","en","_MAP_OVERLAY","Map Overlay");
INSERT INTO phpn_vocabulary VALUES("1651","es","_MAP_OVERLAY","Map Overlay");
INSERT INTO phpn_vocabulary VALUES("1652","de","_MARCH","März");
INSERT INTO phpn_vocabulary VALUES("1653","en","_MARCH","March");
INSERT INTO phpn_vocabulary VALUES("1654","es","_MARCH","Marzo");
INSERT INTO phpn_vocabulary VALUES("1655","de","_MASS_MAIL","Masse Mail");
INSERT INTO phpn_vocabulary VALUES("1656","en","_MASS_MAIL","Mass Mail");
INSERT INTO phpn_vocabulary VALUES("1657","es","_MASS_MAIL","Misa Mail");
INSERT INTO phpn_vocabulary VALUES("1658","de","_MASS_MAIL_ALERT","Achtung: gewöhnlich öffentlichen Hostings haben ein Limit von 200 Mails pro Stunde");
INSERT INTO phpn_vocabulary VALUES("1659","en","_MASS_MAIL_ALERT","Attention: shared hosting services usually have a limit of 200 emails per hour");
INSERT INTO phpn_vocabulary VALUES("1660","es","_MASS_MAIL_ALERT","Atención: servicios de alojamiento compartido por lo general tienen un límite de 200 correos electrónicos por hora");
INSERT INTO phpn_vocabulary VALUES("1661","de","_MASS_MAIL_AND_TEMPLATES","Masse Mail & Vorlagen");
INSERT INTO phpn_vocabulary VALUES("1662","en","_MASS_MAIL_AND_TEMPLATES","Mass Mail & Templates");
INSERT INTO phpn_vocabulary VALUES("1663","es","_MASS_MAIL_AND_TEMPLATES","Misa de correo y plantillas");
INSERT INTO phpn_vocabulary VALUES("1664","de","_MAXIMUM_NIGHTS","Maximale Nights");
INSERT INTO phpn_vocabulary VALUES("1665","en","_MAXIMUM_NIGHTS","Maximum Nights");
INSERT INTO phpn_vocabulary VALUES("1666","es","_MAXIMUM_NIGHTS","Noches Máximo");
INSERT INTO phpn_vocabulary VALUES("1667","de","_MAXIMUM_NIGHTS_ALERT","_IN_HOTEL_Die maximal zulässige Aufenthalt für diesen Zeitraum von _FROM_ um _TO_ ist _NIGHTS_ Übernachtungen pro Buchung. Bitte geben Sie erneut.");
INSERT INTO phpn_vocabulary VALUES("1668","en","_MAXIMUM_NIGHTS_ALERT","_IN_HOTEL_The maximum allowed stay for this period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1669","es","_MAXIMUM_NIGHTS_ALERT","_IN_HOTEL_La estancia máxima permitida para este período de tiempo desde _FROM_ a _TO_ es _NIGHTS_ noches por reserva. Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1670","de","_MAX_ADULTS","Max Erwachsene");
INSERT INTO phpn_vocabulary VALUES("1671","en","_MAX_ADULTS","Max Adults");
INSERT INTO phpn_vocabulary VALUES("1672","es","_MAX_ADULTS","Número máximo de Adultos");
INSERT INTO phpn_vocabulary VALUES("1673","de","_MAX_ADULTS_ACCOMMODATE","Maximalanzahl Erwachsene Dieses Zimmer bietet Platz");
INSERT INTO phpn_vocabulary VALUES("1674","en","_MAX_ADULTS_ACCOMMODATE","Maximum number of adults this room can accommodate");
INSERT INTO phpn_vocabulary VALUES("1675","es","_MAX_ADULTS_ACCOMMODATE","Número máximo de adultos Esta habitación tiene capacidad");
INSERT INTO phpn_vocabulary VALUES("1676","de","_MAX_CHARS","(max: _MAX_CHARS_ Zeichen)");
INSERT INTO phpn_vocabulary VALUES("1677","en","_MAX_CHARS","(max: _MAX_CHARS_ chars)");
INSERT INTO phpn_vocabulary VALUES("1678","es","_MAX_CHARS","(max: _MAX_CHARS_ caracteres)");
INSERT INTO phpn_vocabulary VALUES("1679","de","_MAX_CHILDREN","Max Kinder");
INSERT INTO phpn_vocabulary VALUES("1680","en","_MAX_CHILDREN","Max Children");
INSERT INTO phpn_vocabulary VALUES("1681","es","_MAX_CHILDREN","Número máximo de niños");
INSERT INTO phpn_vocabulary VALUES("1682","de","_MAX_CHILDREN_ACCOMMODATE","Maximale Anzahl der Kinder in diesem Zimmer bietet Platz für");
INSERT INTO phpn_vocabulary VALUES("1683","en","_MAX_CHILDREN_ACCOMMODATE","Maximum number of children this room can accommodate");
INSERT INTO phpn_vocabulary VALUES("1684","es","_MAX_CHILDREN_ACCOMMODATE","El número máximo de niños de esta habitación se pueden alojar");
INSERT INTO phpn_vocabulary VALUES("1685","de","_MAX_EXTRA_BEDS","Maximale Zustellbetten");
INSERT INTO phpn_vocabulary VALUES("1686","en","_MAX_EXTRA_BEDS","Maximum Extra Beds");
INSERT INTO phpn_vocabulary VALUES("1687","es","_MAX_EXTRA_BEDS","Máximo camas supletorias");
INSERT INTO phpn_vocabulary VALUES("1688","de","_MAX_OCCUPANCY","Max. Belegung");
INSERT INTO phpn_vocabulary VALUES("1689","en","_MAX_OCCUPANCY","Max. Occupancy");
INSERT INTO phpn_vocabulary VALUES("1690","es","_MAX_OCCUPANCY","Max. ocupación");
INSERT INTO phpn_vocabulary VALUES("1691","de","_MAX_RESERVATIONS_ERROR","Sie haben die maximale Anzahl von erlaubten Zimmerreservierungen erreicht, dass Sie noch nicht fertig! Bitte füllen Sie mindestens eine von ihnen auf die Reservierung von neuen Räumen fort.");
INSERT INTO phpn_vocabulary VALUES("1692","en","_MAX_RESERVATIONS_ERROR","You have reached the maximum number of permitted room reservations, that you have not yet finished! Please complete at least one of them to proceed reservation of new rooms.");
INSERT INTO phpn_vocabulary VALUES("1693","es","_MAX_RESERVATIONS_ERROR","Usted ha alcanzado el número máximo de reservas de habitaciones permitidas, que aún no ha terminado! Por favor, complete al menos uno de ellos para continuar la reserva de habitaciones nuevas.");
INSERT INTO phpn_vocabulary VALUES("1694","de","_MAY","Mai");
INSERT INTO phpn_vocabulary VALUES("1695","en","_MAY","May");
INSERT INTO phpn_vocabulary VALUES("1696","es","_MAY","Mayo");
INSERT INTO phpn_vocabulary VALUES("1697","de","_MD_BACKUP_AND_RESTORE","Mit Backup and Restore-Modul können Sie alle Ihre Datenbanktabellen, eine Datei herunterzuladen Dump speichern oder in eine Datei auf dem Server, und von einer hochgeladenen oder zuvor gespeicherte Datenbank-Dump wiederherstellen.");
INSERT INTO phpn_vocabulary VALUES("1698","en","_MD_BACKUP_AND_RESTORE","With Backup and Restore module you can dump all of your database tables to a file download or save to a file on the server, and to restore from an uploaded or previously saved database dump.");
INSERT INTO phpn_vocabulary VALUES("1699","es","_MD_BACKUP_AND_RESTORE","Con el módulo de seguridad y restauración para volcar todas las tablas de su base de datos de descarga de un archivo o guardarlo en un archivo en el servidor, y restaurar a partir de un depósito de base de datos de subida o guardado previamente.");
INSERT INTO phpn_vocabulary VALUES("1700","de","_MD_BANNERS","Die Banner-Modul erlaubt dem Administrator, Bilder auf der Site in zufälliger oder Rotation Stil angezeigt.");
INSERT INTO phpn_vocabulary VALUES("1701","en","_MD_BANNERS","The Banners module allows administrator to display images on the site in random or rotation style.");
INSERT INTO phpn_vocabulary VALUES("1702","es","_MD_BANNERS","El módulo de Banners que permite al administrador visualizar las imágenes en el sitio en el estilo de azar o de la rotación.");
INSERT INTO phpn_vocabulary VALUES("1703","de","_MD_BOOKINGS","Das Modul ermöglicht die Buchung der Eigentümer der Website, um Buchungen für alle Räume definieren, dann Preis sie auf einer individuellen Basis von Unterkunft und Datum. Es ermöglicht auch Buchungen von Kunden aufgenommen und verwaltet werden, über Administrator-Panel.");
INSERT INTO phpn_vocabulary VALUES("1704","en","_MD_BOOKINGS","The Bookings module allows the site owner to define bookings for all rooms, then price them on an individual basis by accommodation and date. It also permits bookings to be taken from customers and managed via administrator panel.");
INSERT INTO phpn_vocabulary VALUES("1705","es","_MD_BOOKINGS","El módulo de reservas le permite al propietario del sitio para definir las reservas para todas las habitaciones, y luego los precios sobre una base individual por el alojamiento y la fecha. También permite las reservas que se tomarán de los clientes y gestionados a través del panel de administrador.");
INSERT INTO phpn_vocabulary VALUES("1706","de","_MD_COMMENTS","Die Kommentare Modul ermöglicht es den Besuchern, um Kommentare zu Artikeln und Administrator der Website zu verlassen, um sie zu mäßigen.");
INSERT INTO phpn_vocabulary VALUES("1707","en","_MD_COMMENTS","The Comments module allows visitors to leave comments on articles and administrator of the site to moderate them.");
INSERT INTO phpn_vocabulary VALUES("1708","es","_MD_COMMENTS","El módulo de Comentarios permite a los visitantes dejar comentarios en los artículos y administrador del sitio a moderada.");
INSERT INTO phpn_vocabulary VALUES("1709","de","_MD_CONTACT_US","Kontakt Modul ermöglicht die einfache Erstellung und am Online-Kontaktformular auf Seiten der Website, unter Verwendung vordefinierter Code, wie: {module:contact_us}.");
INSERT INTO phpn_vocabulary VALUES("1710","en","_MD_CONTACT_US","Contact Us module allows easy create and place on-line contact form on site pages, using predefined code, like: {module:contact_us}.");
INSERT INTO phpn_vocabulary VALUES("1711","es","_MD_CONTACT_US","Contáctenos módulo permite una fácil crear y colocar en línea el formulario de contacto en las páginas del sitio, utilizando el código predefinidas, como: {module: contactenos}.");
INSERT INTO phpn_vocabulary VALUES("1712","de","_MD_CUSTOMERS","Die Kunden-Modul ermöglicht die einfache Kunden-Management auf Ihrer Website. Administrator könnte erstellen, bearbeiten oder löschen Kundenkonten. Die Kunden könnten auf der Website und loggen Sie sich in ihr Konto zu registrieren.");
INSERT INTO phpn_vocabulary VALUES("1713","en","_MD_CUSTOMERS","The Customers module allows easy customers management on your site. Administrator could create, edit or delete customer accounts. Customers could register on the site and log into their accounts.");
INSERT INTO phpn_vocabulary VALUES("1714","es","_MD_CUSTOMERS","El módulo permite a los clientes la gestión de clientes, desde tu sitio. Administrador puede crear, editar o eliminar las cuentas de clientes. Los clientes pueden registrarse en el sitio y acceder a sus cuentas.");
INSERT INTO phpn_vocabulary VALUES("1715","de","_MD_FAQ","Die Frequently Asked Questions (FAQ)-Modul ermöglicht admin Benutzer Frage und Antwort-Paare, die sie auf der &#034;FAQ&#034;-Seite angezeigt werden sollen erstellen.");
INSERT INTO phpn_vocabulary VALUES("1716","en","_MD_FAQ","The Frequently Asked Questions (faq) module allows admin users to create question and answer pairs which they want displayed on the \'faq\' page.");
INSERT INTO phpn_vocabulary VALUES("1717","es","_MD_FAQ","Las Preguntas Frecuentes (FAQ) del módulo permite a los usuarios de administración para crear pares de preguntas y respuestas que desea mostrar en la página \'preguntas frecuentes\'.");
INSERT INTO phpn_vocabulary VALUES("1718","de","_MD_GALLERY","Die Galerie-Modul erlaubt dem Administrator, Bild oder Video Alben zu erstellen, hochzuladen Album Inhalte und display diese Inhalte durch Besucher der Website eingesehen werden.");
INSERT INTO phpn_vocabulary VALUES("1719","en","_MD_GALLERY","The Gallery module allows administrator to create image or video albums, upload album content and display this content to be viewed by visitor of the site.");
INSERT INTO phpn_vocabulary VALUES("1720","es","_MD_GALLERY","El módulo permite al administrador de la Galería para crear álbumes de imágenes o de vídeo, subir contenido del álbum y display este contenido para ser visto por los visitantes del sitio.");
INSERT INTO phpn_vocabulary VALUES("1721","de","_MD_NEWS","Das News und Events-Modul erlaubt dem Administrator, Neuigkeiten und Veranstaltungen auf der Website zu veröffentlichen, zeigen neueste von ihnen an der Seite zu blockieren.");
INSERT INTO phpn_vocabulary VALUES("1722","en","_MD_NEWS","The News and Events module allows administrator to post news and events on the site, display latest of them at the side block.");
INSERT INTO phpn_vocabulary VALUES("1723","es","_MD_NEWS","El módulo de Noticias y Eventos permite al administrador para publicar noticias y eventos en el sitio, muestra más reciente de ellas en el bloque lateral.");
INSERT INTO phpn_vocabulary VALUES("1724","de","_MD_PAGES","Seiten-Modul ermöglicht Administratoren die einfache Erstellung und Pflege Inhalt der Seite.");
INSERT INTO phpn_vocabulary VALUES("1725","en","_MD_PAGES","Pages module allows administrator to easily create and maintain page content.");
INSERT INTO phpn_vocabulary VALUES("1726","es","_MD_PAGES","Módulo de páginas permite al administrador crear y mantener fácilmente contenido de la página.");
INSERT INTO phpn_vocabulary VALUES("1727","de","_MD_RATINGS","Die Ratings Modul ermöglicht Benutzern, um die Hotels zu bewerten. Die Zahl der Stimmen und der durchschnittlichen Bewertung werden auf der geeigneten Hotel vorgelegt werden.");
INSERT INTO phpn_vocabulary VALUES("1728","en","_MD_RATINGS","The Ratings module allows your users to rate the hotels. The number of votes and average rating will be shown at the appropriate hotel.");
INSERT INTO phpn_vocabulary VALUES("1729","es","_MD_RATINGS","El módulo Valoraciones permite a sus usuarios valorar los hoteles. El número de votos y la calificación media se mostrará en el hotel adecuado.");
INSERT INTO phpn_vocabulary VALUES("1730","de","_MD_ROOMS","Die Zimmer-Modul ermöglicht den Webseiten-Inhaber einfach verwalten Zimmer in Ihrem Hotel: Erstellen, bearbeiten oder entfernen Sie sie, geben Zimmerausstattung, definieren Preise und Verfügbarkeit für bestimmte Zeit, etc.");
INSERT INTO phpn_vocabulary VALUES("1731","en","_MD_ROOMS","The Rooms module allows the site owner easily manage rooms in your hotel: create, edit or remove them, specify room facilities, define prices and availability for certain period of time, etc.");
INSERT INTO phpn_vocabulary VALUES("1732","es","_MD_ROOMS","El módulo de habitaciones permite que el propietario del sitio gestionar fácilmente habitaciones de su hotel: crear, editar o eliminarlos, especifique sala de instalaciones, definir los precios y la disponibilidad de cierto período de tiempo, etc");
INSERT INTO phpn_vocabulary VALUES("1736","de","_MEAL_PLANS","Mahlzeit Pläne");
INSERT INTO phpn_vocabulary VALUES("1737","en","_MEAL_PLANS","Meal Plans");
INSERT INTO phpn_vocabulary VALUES("1738","es","_MEAL_PLANS","Planes de alimentación");
INSERT INTO phpn_vocabulary VALUES("1739","de","_MEAL_PLANS_MANAGEMENT","Essen plant das Management");
INSERT INTO phpn_vocabulary VALUES("1740","en","_MEAL_PLANS_MANAGEMENT","Meal Plans Management");
INSERT INTO phpn_vocabulary VALUES("1741","es","_MEAL_PLANS_MANAGEMENT","Harina de Planes de Gestión");
INSERT INTO phpn_vocabulary VALUES("1742","de","_MENUS","Menüs");
INSERT INTO phpn_vocabulary VALUES("1743","en","_MENUS","Menus");
INSERT INTO phpn_vocabulary VALUES("1744","es","_MENUS","Menús");
INSERT INTO phpn_vocabulary VALUES("1745","de","_MENUS_AND_PAGES","Menüs und Seiten");
INSERT INTO phpn_vocabulary VALUES("1746","en","_MENUS_AND_PAGES","Menus and Pages");
INSERT INTO phpn_vocabulary VALUES("1747","es","_MENUS_AND_PAGES","Los menús y páginas");
INSERT INTO phpn_vocabulary VALUES("1748","de","_MENUS_DISABLED_ALERT","Nehmen Sie in Rechnung, dass einige Menüs für diese Vorlage kann deaktiviert werden.");
INSERT INTO phpn_vocabulary VALUES("1749","en","_MENUS_DISABLED_ALERT","Take in account that some menus may be disabled for this template.");
INSERT INTO phpn_vocabulary VALUES("1750","es","_MENUS_DISABLED_ALERT","Tome en cuenta que algunos menús pueden ser deshabilitados para esta plantilla.");
INSERT INTO phpn_vocabulary VALUES("1751","de","_MENU_ADD","Menü hinzufügen");
INSERT INTO phpn_vocabulary VALUES("1752","en","_MENU_ADD","Add Menu");
INSERT INTO phpn_vocabulary VALUES("1753","es","_MENU_ADD","Añadir Menú");
INSERT INTO phpn_vocabulary VALUES("1754","de","_MENU_CREATED","Menu wurde erfolgreich erstellt");
INSERT INTO phpn_vocabulary VALUES("1755","en","_MENU_CREATED","Menu has been successfully created");
INSERT INTO phpn_vocabulary VALUES("1756","es","_MENU_CREATED","Menu fue creado con éxito");
INSERT INTO phpn_vocabulary VALUES("1757","de","_MENU_DELETED","Menu wurde erfolgreich gelöscht");
INSERT INTO phpn_vocabulary VALUES("1758","en","_MENU_DELETED","Menu has been successfully deleted");
INSERT INTO phpn_vocabulary VALUES("1759","es","_MENU_DELETED","Menú ha sido borrado");
INSERT INTO phpn_vocabulary VALUES("1760","de","_MENU_DELETE_WARNING","Sie sind sicher");
INSERT INTO phpn_vocabulary VALUES("1761","en","_MENU_DELETE_WARNING","Are you sure you want to delete this menu? Note: this will make all its menu links invisible to your site visitors!");
INSERT INTO phpn_vocabulary VALUES("1762","es","_MENU_DELETE_WARNING","¿Estás seguro de que deseas eliminar este menú? Nota: esto hará que todo su menú de enlaces invisibles a sus visitantes!");
INSERT INTO phpn_vocabulary VALUES("1763","de","_MENU_EDIT","Menü Bearbeiten");
INSERT INTO phpn_vocabulary VALUES("1764","en","_MENU_EDIT","Edit Menu");
INSERT INTO phpn_vocabulary VALUES("1765","es","_MENU_EDIT","Menú Edición");
INSERT INTO phpn_vocabulary VALUES("1766","de","_MENU_LINK","Menü-Link");
INSERT INTO phpn_vocabulary VALUES("1767","en","_MENU_LINK","Menu Link");
INSERT INTO phpn_vocabulary VALUES("1768","es","_MENU_LINK","Enlace del menú");
INSERT INTO phpn_vocabulary VALUES("1769","de","_MENU_LINK_TEXT","Menü-Link (max. 40 Zeichen)");
INSERT INTO phpn_vocabulary VALUES("1770","en","_MENU_LINK_TEXT","Menu Link (max. 40 chars)");
INSERT INTO phpn_vocabulary VALUES("1771","es","_MENU_LINK_TEXT","Enlace del menú (máx. 40 caracteres)");
INSERT INTO phpn_vocabulary VALUES("1772","de","_MENU_MANAGEMENT","Menüs Management");
INSERT INTO phpn_vocabulary VALUES("1773","en","_MENU_MANAGEMENT","Menus Management");
INSERT INTO phpn_vocabulary VALUES("1774","es","_MENU_MANAGEMENT","Menús de Gestión");
INSERT INTO phpn_vocabulary VALUES("1775","de","_MENU_MISSED","Verpasste Menü zu aktualisieren! Bitte versuchen Sie es erneut.");
INSERT INTO phpn_vocabulary VALUES("1776","en","_MENU_MISSED","Missed menu to update! Please, try again.");
INSERT INTO phpn_vocabulary VALUES("1777","es","_MENU_MISSED","Menú perdidas para actualizar! Por favor, inténtelo de nuevo.");
INSERT INTO phpn_vocabulary VALUES("1778","de","_MENU_NAME","Menüname");
INSERT INTO phpn_vocabulary VALUES("1779","en","_MENU_NAME","Menu Name");
INSERT INTO phpn_vocabulary VALUES("1780","es","_MENU_NAME","Nombre de menú");
INSERT INTO phpn_vocabulary VALUES("1781","de","_MENU_NAME_EMPTY","Menü-Name darf nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("1782","en","_MENU_NAME_EMPTY","Menu name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("1783","es","_MENU_NAME_EMPTY","Nombre del menú no puede estar vacía!");
INSERT INTO phpn_vocabulary VALUES("1784","de","_MENU_NOT_CREATED","Menü wurde nicht erstellt!");
INSERT INTO phpn_vocabulary VALUES("1785","en","_MENU_NOT_CREATED","Menu has not been created!");
INSERT INTO phpn_vocabulary VALUES("1786","es","_MENU_NOT_CREATED","Menú no se ha creado!");
INSERT INTO phpn_vocabulary VALUES("1787","de","_MENU_NOT_DELETED","Menü wurde nicht gelöscht!");
INSERT INTO phpn_vocabulary VALUES("1788","en","_MENU_NOT_DELETED","Menu has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("1789","es","_MENU_NOT_DELETED","Menú no se ha eliminado!");
INSERT INTO phpn_vocabulary VALUES("1790","de","_MENU_NOT_FOUND","Keine Menüs gefunden");
INSERT INTO phpn_vocabulary VALUES("1791","en","_MENU_NOT_FOUND","No Menus Found");
INSERT INTO phpn_vocabulary VALUES("1792","es","_MENU_NOT_FOUND","No se han encontrado los menús");
INSERT INTO phpn_vocabulary VALUES("1793","de","_MENU_NOT_SAVED","Menü wurde nicht gespart!");
INSERT INTO phpn_vocabulary VALUES("1794","en","_MENU_NOT_SAVED","Menu has not been saved!");
INSERT INTO phpn_vocabulary VALUES("1795","es","_MENU_NOT_SAVED","Menú no se salvó!");
INSERT INTO phpn_vocabulary VALUES("1796","de","_MENU_ORDER","Menü Auftrag");
INSERT INTO phpn_vocabulary VALUES("1797","en","_MENU_ORDER","Menu Order");
INSERT INTO phpn_vocabulary VALUES("1798","es","_MENU_ORDER","Orden del menú");
INSERT INTO phpn_vocabulary VALUES("1799","de","_MENU_ORDER_CHANGED","Menü Auftrag wurde erfolgreich geändert");
INSERT INTO phpn_vocabulary VALUES("1800","en","_MENU_ORDER_CHANGED","Menu order has been successfully changed");
INSERT INTO phpn_vocabulary VALUES("1801","es","_MENU_ORDER_CHANGED","Orden del menú se ha cambiado correctamente");
INSERT INTO phpn_vocabulary VALUES("1802","de","_MENU_SAVED","Menu wurde erfolgreich gespeichert");
INSERT INTO phpn_vocabulary VALUES("1803","en","_MENU_SAVED","Menu has been successfully saved");
INSERT INTO phpn_vocabulary VALUES("1804","es","_MENU_SAVED","Menú se ha guardado correctamente");
INSERT INTO phpn_vocabulary VALUES("1805","de","_MENU_TITLE","Menü Titel");
INSERT INTO phpn_vocabulary VALUES("1806","en","_MENU_TITLE","Menu Title");
INSERT INTO phpn_vocabulary VALUES("1807","es","_MENU_TITLE","Menu TitleTítulo del menú");
INSERT INTO phpn_vocabulary VALUES("1808","de","_MENU_WORD","Menü");
INSERT INTO phpn_vocabulary VALUES("1809","en","_MENU_WORD","Menu");
INSERT INTO phpn_vocabulary VALUES("1810","es","_MENU_WORD","Menú");
INSERT INTO phpn_vocabulary VALUES("1811","de","_MESSAGE","Nachricht");
INSERT INTO phpn_vocabulary VALUES("1812","en","_MESSAGE","Message");
INSERT INTO phpn_vocabulary VALUES("1813","es","_MESSAGE","Mensaje");
INSERT INTO phpn_vocabulary VALUES("1814","de","_MESSAGE_EMPTY_ALERT","Nachricht kann nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("1815","en","_MESSAGE_EMPTY_ALERT","Message cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1816","es","_MESSAGE_EMPTY_ALERT","El mensaje no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1817","de","_META_TAG","Meta-Tag");
INSERT INTO phpn_vocabulary VALUES("1818","en","_META_TAG","Meta Tag");
INSERT INTO phpn_vocabulary VALUES("1819","es","_META_TAG","Meta Tag");
INSERT INTO phpn_vocabulary VALUES("1820","de","_META_TAGS","META-Tags");
INSERT INTO phpn_vocabulary VALUES("1821","en","_META_TAGS","META Tags");
INSERT INTO phpn_vocabulary VALUES("1822","es","_META_TAGS","Etiquetas META");
INSERT INTO phpn_vocabulary VALUES("1823","de","_METHOD","Methode");
INSERT INTO phpn_vocabulary VALUES("1824","en","_METHOD","Method");
INSERT INTO phpn_vocabulary VALUES("1825","es","_METHOD","Método");
INSERT INTO phpn_vocabulary VALUES("1826","de","_MIN","min");
INSERT INTO phpn_vocabulary VALUES("1827","en","_MIN","Min");
INSERT INTO phpn_vocabulary VALUES("1828","es","_MIN","Min");
INSERT INTO phpn_vocabulary VALUES("1829","de","_MINIMUM_NIGHTS","Minimum Nächte");
INSERT INTO phpn_vocabulary VALUES("1830","en","_MINIMUM_NIGHTS","Minimum Nights");
INSERT INTO phpn_vocabulary VALUES("1831","es","_MINIMUM_NIGHTS","Noches mínimo");
INSERT INTO phpn_vocabulary VALUES("1832","de","_MINIMUM_NIGHTS_ALERT","_IN_HOTEL_Der kleinste zulässige Aufenthalt für den Zeitraum von _FROM_ bis _TO_ ist _NIGHTS_ Übernachtungen pro Buchung (Paket _PACKAGE_NAME_). Bitte geben Sie erneut.");
INSERT INTO phpn_vocabulary VALUES("1833","en","_MINIMUM_NIGHTS_ALERT","_IN_HOTEL_The minimum allowed stay for the period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking (package _PACKAGE_NAME_). Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1834","es","_MINIMUM_NIGHTS_ALERT","_IN_HOTEL_La estancia mínima permitida para el período de tiempo a partir de _FROM_ a _TO_ es _NIGHTS_ noches por reserva (paquete _PACKAGE_NAME_). Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("1835","de","_MINUTES","Minuten");
INSERT INTO phpn_vocabulary VALUES("1836","en","_MINUTES","minutes");
INSERT INTO phpn_vocabulary VALUES("1837","es","_MINUTES","acta");
INSERT INTO phpn_vocabulary VALUES("1838","de","_MO","Mo");
INSERT INTO phpn_vocabulary VALUES("1839","en","_MO","Mo");
INSERT INTO phpn_vocabulary VALUES("1840","es","_MO","Lu");
INSERT INTO phpn_vocabulary VALUES("1841","de","_MODULES","Modul");
INSERT INTO phpn_vocabulary VALUES("1842","en","_MODULES","Modules");
INSERT INTO phpn_vocabulary VALUES("1843","es","_MODULES","Módulos");
INSERT INTO phpn_vocabulary VALUES("1844","de","_MODULES_MANAGEMENT","Modul-Management");
INSERT INTO phpn_vocabulary VALUES("1845","en","_MODULES_MANAGEMENT","Modules Management");
INSERT INTO phpn_vocabulary VALUES("1846","es","_MODULES_MANAGEMENT","Módulos de Gestión");
INSERT INTO phpn_vocabulary VALUES("1847","de","_MODULES_NOT_FOUND","Keine Module gefunden!");
INSERT INTO phpn_vocabulary VALUES("1848","en","_MODULES_NOT_FOUND","No modules found!");
INSERT INTO phpn_vocabulary VALUES("1849","es","_MODULES_NOT_FOUND","N módulos encontrado!");
INSERT INTO phpn_vocabulary VALUES("1850","de","_MODULE_INSTALLED","Modul wurde erfolgreich installiert!");
INSERT INTO phpn_vocabulary VALUES("1851","en","_MODULE_INSTALLED","Module has been successfully installed!");
INSERT INTO phpn_vocabulary VALUES("1852","es","_MODULE_INSTALLED","Módulo fue instalado con éxito!");
INSERT INTO phpn_vocabulary VALUES("1853","de","_MODULE_INSTALL_ALERT","Sie sind sicher");
INSERT INTO phpn_vocabulary VALUES("1854","en","_MODULE_INSTALL_ALERT","Are you sure you want to install this module?");
INSERT INTO phpn_vocabulary VALUES("1855","es","_MODULE_INSTALL_ALERT","¿Estás seguro de que desea instalar este módulo?");
INSERT INTO phpn_vocabulary VALUES("1856","de","_MODULE_UNINSTALLED","Modul wurde erfolgreich in nicht eingebautem Zustand!");
INSERT INTO phpn_vocabulary VALUES("1857","en","_MODULE_UNINSTALLED","Module has been successfully un-installed!");
INSERT INTO phpn_vocabulary VALUES("1858","es","_MODULE_UNINSTALLED","Módulo éxito sin instalar!");
INSERT INTO phpn_vocabulary VALUES("1859","de","_MODULE_UNINSTALL_ALERT","Sie sind sicher");
INSERT INTO phpn_vocabulary VALUES("1860","en","_MODULE_UNINSTALL_ALERT","Are you sure you want to un-install this module? All data, related to this module will be permanently deleted form the system!");
INSERT INTO phpn_vocabulary VALUES("1861","es","_MODULE_UNINSTALL_ALERT","¿Estás seguro de que desea desinstalar este módulo? Todos los datos, relacionados con este módulo se borran definitivamente forman el sistema!");
INSERT INTO phpn_vocabulary VALUES("1862","de","_MON","Mon");
INSERT INTO phpn_vocabulary VALUES("1863","en","_MON","Mon");
INSERT INTO phpn_vocabulary VALUES("1864","es","_MON","Lun");
INSERT INTO phpn_vocabulary VALUES("1865","de","_MONDAY","Montag");
INSERT INTO phpn_vocabulary VALUES("1866","en","_MONDAY","Monday");
INSERT INTO phpn_vocabulary VALUES("1867","es","_MONDAY","dlunes");
INSERT INTO phpn_vocabulary VALUES("1868","de","_MONTH","Monat");
INSERT INTO phpn_vocabulary VALUES("1869","en","_MONTH","Month");
INSERT INTO phpn_vocabulary VALUES("1870","es","_MONTH","Mes");
INSERT INTO phpn_vocabulary VALUES("1871","de","_MONTHS","Monate");
INSERT INTO phpn_vocabulary VALUES("1872","en","_MONTHS","Months");
INSERT INTO phpn_vocabulary VALUES("1873","es","_MONTHS","Meses");
INSERT INTO phpn_vocabulary VALUES("1874","de","_MS_ACTIVATE_BOOKINGS","Legt fest, ob Buchung Modul aktiv ist auf einem Ganze Web-Site, Front-End/Back-End-Lösung oder nur inaktiv");
INSERT INTO phpn_vocabulary VALUES("1875","en","_MS_ACTIVATE_BOOKINGS","Specifies whether booking module is active on a Whole Site, Front-End/Back-End only or inactive");
INSERT INTO phpn_vocabulary VALUES("1876","es","_MS_ACTIVATE_BOOKINGS","Define si el módulo de reserva está activa en todo el sitio, Front-End/Back-End solo o inactivo");
INSERT INTO phpn_vocabulary VALUES("1877","de","_MS_ADD_WATERMARK","Gibt an, ob Wasserzeichen zur Zimmer Bilder oder nicht hinzufügen");
INSERT INTO phpn_vocabulary VALUES("1878","en","_MS_ADD_WATERMARK","Specifies whether to add watermark to rooms images or not");
INSERT INTO phpn_vocabulary VALUES("1879","es","_MS_ADD_WATERMARK","Especifica si se debe añadir marcas de agua a las imágenes o no habitaciones");
INSERT INTO phpn_vocabulary VALUES("1880","de","_MS_ADMIN_BOOKING_IN_PAST","Gibt an, ob der Buchung in der Vergangenheit für Administratoren und Besitzer des Hotels (ab Beginn des laufenden Monats)");
INSERT INTO phpn_vocabulary VALUES("1881","en","_MS_ADMIN_BOOKING_IN_PAST","Specifies whether to allow booking in the past for admins and hotel owners (from the beginning of current month)");
INSERT INTO phpn_vocabulary VALUES("1882","es","_MS_ADMIN_BOOKING_IN_PAST","Especifica si se permite la reserva en el pasado para los administradores y propietarios de hoteles (desde el principio del mes actual)");
INSERT INTO phpn_vocabulary VALUES("1883","de","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","Gibt an, ob ändernden Kunden-Passwort von Admin");
INSERT INTO phpn_vocabulary VALUES("1884","en","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","Specifies whether to allow changing customer password by Admin");
INSERT INTO phpn_vocabulary VALUES("1885","es","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","Especifica si se permite cambiar la contraseña de cliente de administración");
INSERT INTO phpn_vocabulary VALUES("1886","de","_MS_ADMIN_CHANGE_USER_PASSWORD","Gibt an, ob sich ändernde Benutzer-Passwort vom Admin erlaubt");
INSERT INTO phpn_vocabulary VALUES("1887","en","_MS_ADMIN_CHANGE_USER_PASSWORD","Specifies whether to allow changing user password by Admin");
INSERT INTO phpn_vocabulary VALUES("1888","es","_MS_ADMIN_CHANGE_USER_PASSWORD","Especifica si se permite el cambio de contraseñas de usuario de Admin");
INSERT INTO phpn_vocabulary VALUES("1889","de","_MS_ALBUMS_PER_LINE","Anzahl der Album Symbole pro Zeile");
INSERT INTO phpn_vocabulary VALUES("1890","en","_MS_ALBUMS_PER_LINE","Number of album icons per line");
INSERT INTO phpn_vocabulary VALUES("1891","es","_MS_ALBUMS_PER_LINE","Número de iconos de disco por línea");
INSERT INTO phpn_vocabulary VALUES("1892","de","_MS_ALBUM_ICON_HEIGHT","Album-Symbol Höhe");
INSERT INTO phpn_vocabulary VALUES("1893","en","_MS_ALBUM_ICON_HEIGHT","Album icon height");
INSERT INTO phpn_vocabulary VALUES("1894","es","_MS_ALBUM_ICON_HEIGHT","Album altura icono");
INSERT INTO phpn_vocabulary VALUES("1895","de","_MS_ALBUM_ICON_WIDTH","Album-Symbol Breite");
INSERT INTO phpn_vocabulary VALUES("1896","en","_MS_ALBUM_ICON_WIDTH","Album icon width");
INSERT INTO phpn_vocabulary VALUES("1897","es","_MS_ALBUM_ICON_WIDTH","Album ancho icono");
INSERT INTO phpn_vocabulary VALUES("1898","de","_MS_ALBUM_ITEMS_NUMERATION","Gibt an, ob Elemente Nummerierung in Alben zeigen");
INSERT INTO phpn_vocabulary VALUES("1899","en","_MS_ALBUM_ITEMS_NUMERATION","Specifies whether to show items numeration in albums");
INSERT INTO phpn_vocabulary VALUES("1900","es","_MS_ALBUM_ITEMS_NUMERATION","Especifica si se deben mostrar los elementos de numeración en álbumes");
INSERT INTO phpn_vocabulary VALUES("1901","de","_MS_ALBUM_KEY","Das Schlüsselwort, das mit einem bestimmten Album Bilder ersetzt werden (Kopieren und fügen Sie ihn auf der Seite)");
INSERT INTO phpn_vocabulary VALUES("1902","en","_MS_ALBUM_KEY","The keyword that will be replaced with a certain album images (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("1903","es","_MS_ALBUM_KEY","La palabra clave que será reemplazado con un álbum de imágenes de algunos (copiar y pegarlo en la página)");
INSERT INTO phpn_vocabulary VALUES("1904","de","_MS_ALERT_ADMIN_NEW_REGISTRATION","Gibt an, ob alarmieren auf neue Kunden-Registrierung Admin");
INSERT INTO phpn_vocabulary VALUES("1905","en","_MS_ALERT_ADMIN_NEW_REGISTRATION","Specifies whether to alert admin on new customer registration");
INSERT INTO phpn_vocabulary VALUES("1906","es","_MS_ALERT_ADMIN_NEW_REGISTRATION","Especifica si se debe alertar a los admin el registro de nuevo cliente");
INSERT INTO phpn_vocabulary VALUES("1907","de","_MS_ALLOW_ADDING_BY_ADMIN","Gibt an, ob das Hinzufügen neuer Kunden durch Admin");
INSERT INTO phpn_vocabulary VALUES("1908","en","_MS_ALLOW_ADDING_BY_ADMIN","Specifies whether to allow adding new customers by Admin");
INSERT INTO phpn_vocabulary VALUES("1909","es","_MS_ALLOW_ADDING_BY_ADMIN","Especifica si se permite la adición de nuevos clientes por Admin");
INSERT INTO phpn_vocabulary VALUES("1910","de","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","Gibt an, ob die Buchung für den Kunden, ohne Berücksichtigung erlauben");
INSERT INTO phpn_vocabulary VALUES("1911","en","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","Specifies whether to allow booking for customer without creating account");
INSERT INTO phpn_vocabulary VALUES("1912","es","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","Especifica si se permite reservar para el cliente sin necesidad de crear la cuenta");
INSERT INTO phpn_vocabulary VALUES("1913","de","_MS_ALLOW_CHILDREN_IN_ROOM","Gibt an, ob Kinder im Zimmer erlaubt");
INSERT INTO phpn_vocabulary VALUES("1914","en","_MS_ALLOW_CHILDREN_IN_ROOM","Specifies whether to allow children in the room");
INSERT INTO phpn_vocabulary VALUES("1915","es","_MS_ALLOW_CHILDREN_IN_ROOM","Especifica si se debe permitir a los niños en la sala de");
INSERT INTO phpn_vocabulary VALUES("1916","de","_MS_ALLOW_CUSTOMERS_LOGIN","Gibt an, ob bestehende Kunden Login");
INSERT INTO phpn_vocabulary VALUES("1917","en","_MS_ALLOW_CUSTOMERS_LOGIN","Specifies whether to allow existing customers to login");
INSERT INTO phpn_vocabulary VALUES("1918","es","_MS_ALLOW_CUSTOMERS_LOGIN","Especifica si se debe permitir a los clientes existentes para acceder");
INSERT INTO phpn_vocabulary VALUES("1919","de","_MS_ALLOW_CUSTOMERS_REGISTRATION","Gibt an, ob die Registrierung neuer Kunden ermöglichen");
INSERT INTO phpn_vocabulary VALUES("1920","en","_MS_ALLOW_CUSTOMERS_REGISTRATION","Specifies whether to allow registration of new customers");
INSERT INTO phpn_vocabulary VALUES("1921","es","_MS_ALLOW_CUSTOMERS_REGISTRATION","Especifica si se permite el registro de nuevos clientes");
INSERT INTO phpn_vocabulary VALUES("1922","de","_MS_ALLOW_CUST_RESET_PASSWORDS","Gibt an, ob Kunden wieder ihre Passwörter");
INSERT INTO phpn_vocabulary VALUES("1923","en","_MS_ALLOW_CUST_RESET_PASSWORDS","Specifies whether to allow customers to restore their passwords");
INSERT INTO phpn_vocabulary VALUES("1924","es","_MS_ALLOW_CUST_RESET_PASSWORDS","Especifica si se permite a los clientes para restablecer sus contraseñas");
INSERT INTO phpn_vocabulary VALUES("1925","de","_MS_ALLOW_DEFAULT_PERIODS","Gibt an, ob das Hinzufügen und das Management von Standard-Zeiträume für Räume");
INSERT INTO phpn_vocabulary VALUES("1926","en","_MS_ALLOW_DEFAULT_PERIODS","Specifies whether to allow adding and management of default periods for rooms ");
INSERT INTO phpn_vocabulary VALUES("1927","es","_MS_ALLOW_DEFAULT_PERIODS","Especifica si se permite la adición y la gestión de los períodos por defecto para los cuartos");
INSERT INTO phpn_vocabulary VALUES("1928","de","_MS_ALLOW_EXTRA_BEDS","Gibt an, ob zusätzliche Betten in den Zimmern ermöglichen");
INSERT INTO phpn_vocabulary VALUES("1929","en","_MS_ALLOW_EXTRA_BEDS","Specifies whether to allow extra beds in the rooms");
INSERT INTO phpn_vocabulary VALUES("1930","es","_MS_ALLOW_EXTRA_BEDS","Especifica si se pueden añadir camas supletorias en las habitaciones");
INSERT INTO phpn_vocabulary VALUES("1931","de","_MS_ALLOW_SYSTEM_SUGGESTION","Gibt an, ob System-Funktion auf Vorschlag leer Suchergebnisse zeigen");
INSERT INTO phpn_vocabulary VALUES("1932","en","_MS_ALLOW_SYSTEM_SUGGESTION","Specifies whether to show system suggestion feature on empty search results");
INSERT INTO phpn_vocabulary VALUES("1933","es","_MS_ALLOW_SYSTEM_SUGGESTION","Especifica si se muestra característica del sistema de sugerencia sobre los resultados de búsqueda vacías");
INSERT INTO phpn_vocabulary VALUES("1934","de","_MS_AUTHORIZE_LOGIN_ID","Gibt Authorize.Net API Login ID");
INSERT INTO phpn_vocabulary VALUES("1935","en","_MS_AUTHORIZE_LOGIN_ID","Specifies Authorize.Net API Login ID");
INSERT INTO phpn_vocabulary VALUES("1936","es","_MS_AUTHORIZE_LOGIN_ID","Especifica Authorize.Net API Login ID");
INSERT INTO phpn_vocabulary VALUES("1937","de","_MS_AUTHORIZE_TRANSACTION_KEY","Authorize.Net Transaction Key gibt");
INSERT INTO phpn_vocabulary VALUES("1938","en","_MS_AUTHORIZE_TRANSACTION_KEY","Specifies Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("1939","es","_MS_AUTHORIZE_TRANSACTION_KEY","Especificador Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("1940","de","_MS_AVAILABLE_UNTIL_APPROVAL","Gibt an, ob \"reservierten\" Zimmer in den Suchergebnissen angezeigt, bis Buchung abgeschlossen");
INSERT INTO phpn_vocabulary VALUES("1941","en","_MS_AVAILABLE_UNTIL_APPROVAL","Specifies whether to show \'reserved\' rooms in search results until booking is complete");
INSERT INTO phpn_vocabulary VALUES("1942","es","_MS_AVAILABLE_UNTIL_APPROVAL","Especifica si se mostrarán los reservadas las habitaciones de los resultados de búsqueda hasta la reserva se ha completado");
INSERT INTO phpn_vocabulary VALUES("1943","de","_MS_BANK_TRANSFER_INFO","Gibt einen erforderlichen Bankdaten: Name der Bank, Filiale, Kontonummer usw.");
INSERT INTO phpn_vocabulary VALUES("1944","en","_MS_BANK_TRANSFER_INFO","Specifies a required banking information: name of the bank, branch, account number etc.");
INSERT INTO phpn_vocabulary VALUES("1945","es","_MS_BANK_TRANSFER_INFO","Especifica una información bancaria requerida: nombre del banco, sucursal, número de cuenta, etc");
INSERT INTO phpn_vocabulary VALUES("1946","de","_MS_BANNERS_CAPTION_HTML","Gibt an, ob mit Hilfe von HTML-Diashow in Bildunterschriften oder nicht");
INSERT INTO phpn_vocabulary VALUES("1947","en","_MS_BANNERS_CAPTION_HTML","Specifies whether to allow using of HTML in slideshow captions or not");
INSERT INTO phpn_vocabulary VALUES("1948","es","_MS_BANNERS_CAPTION_HTML","Especifica si se debe permitir el uso de HTML en los textos de presentación de diapositivas o no");
INSERT INTO phpn_vocabulary VALUES("1949","de","_MS_BANNERS_IS_ACTIVE","Legt fest, ob Banner-Modul aktiv ist oder nicht");
INSERT INTO phpn_vocabulary VALUES("1950","en","_MS_BANNERS_IS_ACTIVE","Defines whether banners module is active or not");
INSERT INTO phpn_vocabulary VALUES("1951","es","_MS_BANNERS_IS_ACTIVE","Define si el módulo banners está activo o no");
INSERT INTO phpn_vocabulary VALUES("1952","de","_MS_BOOKING_MODE","Gibt an, welcher Modus aktiviert ist für die Buchung gedreht");
INSERT INTO phpn_vocabulary VALUES("1953","en","_MS_BOOKING_MODE","Specifies which mode is turned ON for booking");
INSERT INTO phpn_vocabulary VALUES("1954","es","_MS_BOOKING_MODE","Especifica en qué modo se enciende la reserva");
INSERT INTO phpn_vocabulary VALUES("1955","de","_MS_BOOKING_NUMBER_TYPE","Gibt die Art der Buchung Zahlen");
INSERT INTO phpn_vocabulary VALUES("1956","en","_MS_BOOKING_NUMBER_TYPE","Specifies the type of booking numbers");
INSERT INTO phpn_vocabulary VALUES("1957","es","_MS_BOOKING_NUMBER_TYPE","Especifica el tipo de números de reserva");
INSERT INTO phpn_vocabulary VALUES("1958","de","_MS_CHECK_PARTIALLY_OVERLAPPING","Gibt an, ob für sich teilweise überlappenden Daten für Pakete überprüfen");
INSERT INTO phpn_vocabulary VALUES("1959","en","_MS_CHECK_PARTIALLY_OVERLAPPING","Specifies whether to allow check for partially overlapping dates for packages");
INSERT INTO phpn_vocabulary VALUES("1960","es","_MS_CHECK_PARTIALLY_OVERLAPPING","Especifica si se debe permitir comprobar si las fechas se superponen parcialmente para los paquetes");
INSERT INTO phpn_vocabulary VALUES("1961","de","_MS_COMMENTS_ALLOW","Gibt an, ob Kommentare zu Artikeln ermöglichen");
INSERT INTO phpn_vocabulary VALUES("1962","en","_MS_COMMENTS_ALLOW","Specifies whether to allow comments to articles");
INSERT INTO phpn_vocabulary VALUES("1963","es","_MS_COMMENTS_ALLOW","Especifica si se permiten comentarios a los artículos");
INSERT INTO phpn_vocabulary VALUES("1964","de","_MS_COMMENTS_LENGTH","Die maximale Länge eines Kommentars");
INSERT INTO phpn_vocabulary VALUES("1965","en","_MS_COMMENTS_LENGTH","The maximum length of a comment");
INSERT INTO phpn_vocabulary VALUES("1966","es","_MS_COMMENTS_LENGTH","La longitud máxima de un comentario");
INSERT INTO phpn_vocabulary VALUES("1967","de","_MS_COMMENTS_PAGE_SIZE","Legt fest, wie viel Kommentare werden auf einer Seite angezeigt werden");
INSERT INTO phpn_vocabulary VALUES("1968","en","_MS_COMMENTS_PAGE_SIZE","Defines how much comments will be shown on one page");
INSERT INTO phpn_vocabulary VALUES("1969","es","_MS_COMMENTS_PAGE_SIZE","Define la cantidad de comentarios tanto se muestra en una página");
INSERT INTO phpn_vocabulary VALUES("1970","de","_MS_CONTACT_US_KEY","Das Schlüsselwort, das mit ersetzt werden Kontakt-Formular (Kopie und fügen Sie ihn in die Seite)");
INSERT INTO phpn_vocabulary VALUES("1971","en","_MS_CONTACT_US_KEY","The keyword that will be replaced with Contact Us form (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("1972","es","_MS_CONTACT_US_KEY","La palabra clave que serán reemplazadas con nosotros Formulario de contacto (copiar y pegarlo en la página)");
INSERT INTO phpn_vocabulary VALUES("1973","de","_MS_CUSTOMERS_CANCEL_RESERVATION","Geeft het aantal dagen voordat klanten kunnen annuleren van een reservering");
INSERT INTO phpn_vocabulary VALUES("1974","en","_MS_CUSTOMERS_CANCEL_RESERVATION","Specifies the number of days before customers may cancel a reservation");
INSERT INTO phpn_vocabulary VALUES("1975","es","_MS_CUSTOMERS_CANCEL_RESERVATION","Especifica el número de días para que los clientes pueden cancelar una reserva");
INSERT INTO phpn_vocabulary VALUES("1976","de","_MS_CUSTOMERS_IMAGE_VERIFICATION","Geeft aan of afbeelding verificatie (captcha) kan de klant registratiepagina");
INSERT INTO phpn_vocabulary VALUES("1977","en","_MS_CUSTOMERS_IMAGE_VERIFICATION","Specifies whether to allow image verification (captcha) on customer registration page");
INSERT INTO phpn_vocabulary VALUES("1978","es","_MS_CUSTOMERS_IMAGE_VERIFICATION","Especifica si se debe permitir la verificación de la imagen (CAPTCHA) en la página de registro de clientes");
INSERT INTO phpn_vocabulary VALUES("1979","de","_MS_DEFAULT_PAYMENT_SYSTEM","Gibt Zahlungsverzug Verarbeitungssystem");
INSERT INTO phpn_vocabulary VALUES("1980","en","_MS_DEFAULT_PAYMENT_SYSTEM","Specifies default payment processing system");
INSERT INTO phpn_vocabulary VALUES("1981","es","_MS_DEFAULT_PAYMENT_SYSTEM","Especifica el pago por defecto del sistema de procesamiento");
INSERT INTO phpn_vocabulary VALUES("1982","de","_MS_DELAY_LENGTH","Definiert eine Länge von Verzögerung zwischen dem Senden von E-Mails (in Sekunden)");
INSERT INTO phpn_vocabulary VALUES("1983","en","_MS_DELAY_LENGTH","Defines a length of delay between sending emails (in seconds)");
INSERT INTO phpn_vocabulary VALUES("1984","es","_MS_DELAY_LENGTH","Define un tiempo de retraso entre el envío de correos electrónicos (en segundos)");
INSERT INTO phpn_vocabulary VALUES("1985","de","_MS_DELETE_PENDING_TIME","Die maximale angemeldete Zeit für das Löschen von Kommentar in Minuten");
INSERT INTO phpn_vocabulary VALUES("1986","en","_MS_DELETE_PENDING_TIME","The maximum pending time for deleting of comment in minutes");
INSERT INTO phpn_vocabulary VALUES("1987","es","_MS_DELETE_PENDING_TIME","El tiempo máximo de espera para borrar el comentario en cuestión de minutos");
INSERT INTO phpn_vocabulary VALUES("1988","de","_MS_EMAIL","Die E-Mail-Adresse, die verwendet werden, um an Informationen zu bekommen sein wird");
INSERT INTO phpn_vocabulary VALUES("1989","en","_MS_EMAIL","The email address, that will be used to get sent information");
INSERT INTO phpn_vocabulary VALUES("1990","es","_MS_EMAIL","La dirección de correo electrónico, que se utiliza para obtener la información enviada");
INSERT INTO phpn_vocabulary VALUES("1991","de","_MS_FAQ_IS_ACTIVE","Legt fest, ob FAQ-Modul aktiv ist oder nicht");
INSERT INTO phpn_vocabulary VALUES("1992","en","_MS_FAQ_IS_ACTIVE","Defines whether FAQ module is active or not");
INSERT INTO phpn_vocabulary VALUES("1993","es","_MS_FAQ_IS_ACTIVE","Define si FAQ módulo está activo o no");
INSERT INTO phpn_vocabulary VALUES("1994","de","_MS_FIRST_NIGHT_CALCULATING_TYPE","Gibt einen Typ des \"ersten Nacht\" Wert-Berechnung: real oder durchschnittliche");
INSERT INTO phpn_vocabulary VALUES("1995","en","_MS_FIRST_NIGHT_CALCULATING_TYPE","Specifies a type of the \'first night\' value calculating: real or average");
INSERT INTO phpn_vocabulary VALUES("1996","es","_MS_FIRST_NIGHT_CALCULATING_TYPE","Especifica un tipo de cálculo del valor de la \'primera noche\': real o promedio");
INSERT INTO phpn_vocabulary VALUES("1997","de","_MS_GALLERY_KEY","Das Schlüsselwort, das mit Galerie (Kopie ersetzt wird, und fügen Sie ihn in die Seite)");
INSERT INTO phpn_vocabulary VALUES("1998","en","_MS_GALLERY_KEY","The keyword that will be replaced with gallery (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("1999","es","_MS_GALLERY_KEY","La palabra clave que serán reemplazadas con la galería (copiar y pegarlo en la página)");
INSERT INTO phpn_vocabulary VALUES("2000","de","_MS_GALLERY_WRAPPER","Definiert eine Wrapper-Typ für die Galerie");
INSERT INTO phpn_vocabulary VALUES("2001","en","_MS_GALLERY_WRAPPER","Defines a wrapper type for gallery");
INSERT INTO phpn_vocabulary VALUES("2002","es","_MS_GALLERY_WRAPPER","Define un tipo de contenedor de la galería");
INSERT INTO phpn_vocabulary VALUES("2003","de","_MS_IMAGE_GALLERY_TYPE","Erlaubt Arten von Image Gallery");
INSERT INTO phpn_vocabulary VALUES("2004","en","_MS_IMAGE_GALLERY_TYPE","Allowed types of Image Gallery");
INSERT INTO phpn_vocabulary VALUES("2005","es","_MS_IMAGE_GALLERY_TYPE","Tipos de animales Galería de imágenes");
INSERT INTO phpn_vocabulary VALUES("2006","de","_MS_IMAGE_VERIFICATION_ALLOW","Gibt an, ob Bild überprüft werden kann (captcha)");
INSERT INTO phpn_vocabulary VALUES("2007","en","_MS_IMAGE_VERIFICATION_ALLOW","Specifies whether to allow image verification (captcha)");
INSERT INTO phpn_vocabulary VALUES("2008","es","_MS_IMAGE_VERIFICATION_ALLOW","Especifica si se debe permitir la verificación de la imagen (captcha)");
INSERT INTO phpn_vocabulary VALUES("2009","de","_MS_IS_SEND_DELAY","Gibt an, ob Zeitverzögerung zwischen E-Mails ermöglichen.");
INSERT INTO phpn_vocabulary VALUES("2010","en","_MS_IS_SEND_DELAY","Specifies whether to allow time delay between sending emails.");
INSERT INTO phpn_vocabulary VALUES("2011","es","_MS_IS_SEND_DELAY","Especifica si se permite tiempo de retardo entre el envío de mensajes de correo electrónico.");
INSERT INTO phpn_vocabulary VALUES("2012","de","_MS_ITEMS_COUNT_IN_ALBUM","Gibt an, ob Anzahl der Bilder / Videos unter Albumname zeigen");
INSERT INTO phpn_vocabulary VALUES("2013","en","_MS_ITEMS_COUNT_IN_ALBUM","Specifies whether to show count of images/video under album name");
INSERT INTO phpn_vocabulary VALUES("2014","es","_MS_ITEMS_COUNT_IN_ALBUM","Especifica si se muestra la cuenta de las imágenes / video bajo el nombre del álbum");
INSERT INTO phpn_vocabulary VALUES("2015","de","_MS_MAXIMUM_ALLOWED_RESERVATIONS","Gibt die maximal zulässige Zimmerreservierungen (nicht abgeschlossen) pro Kunde");
INSERT INTO phpn_vocabulary VALUES("2016","en","_MS_MAXIMUM_ALLOWED_RESERVATIONS","Specifies the maximum number of allowed room reservations (not completed) per customer");
INSERT INTO phpn_vocabulary VALUES("2017","es","_MS_MAXIMUM_ALLOWED_RESERVATIONS","Especifica el máximo permitido las reservas de habitaciones (no terminado) por cliente");
INSERT INTO phpn_vocabulary VALUES("2018","de","_MS_MAXIMUM_NIGHTS","Definiert eine maximale Anzahl der Nächte pro Buchung [<a href=index.php?admin=mod_booking_packages>eingrenzen Paket</a>]");
INSERT INTO phpn_vocabulary VALUES("2019","en","_MS_MAXIMUM_NIGHTS","Defines a maximum number of nights per booking [<a href=index.php?admin=mod_booking_packages>Define by Package</a>]");
INSERT INTO phpn_vocabulary VALUES("2020","es","_MS_MAXIMUM_NIGHTS","Define un número máximo de noches por reserva [<a href=index.php?admin=mod_booking_packages>Definir por Paquete</a>]");
INSERT INTO phpn_vocabulary VALUES("2021","de","_MS_MAX_ADULTS_IN_SEARCH","Gibt die maximale Anzahl der Erwachsenen in der Dropdown-Liste der Suche Verfügbarkeit Form");
INSERT INTO phpn_vocabulary VALUES("2022","en","_MS_MAX_ADULTS_IN_SEARCH","Specifies the maximum number of adults in the dropdown list of the Search Availability form");
INSERT INTO phpn_vocabulary VALUES("2023","es","_MS_MAX_ADULTS_IN_SEARCH","Especifica el número máximo de adultos en la lista desplegable del formulario disponibilidad Buscar");
INSERT INTO phpn_vocabulary VALUES("2024","de","_MS_MAX_CHILDREN_IN_SEARCH","Gibt die maximale Anzahl von Kindern in der Dropdown-Liste der Suche Verfügbarkeit Form");
INSERT INTO phpn_vocabulary VALUES("2025","en","_MS_MAX_CHILDREN_IN_SEARCH","Specifies the maximum number of children in the dropdown list of the Search Availability form");
INSERT INTO phpn_vocabulary VALUES("2026","es","_MS_MAX_CHILDREN_IN_SEARCH","Especifica el número máximo de niños en la lista desplegable del formulario disponibilidad Buscar");
INSERT INTO phpn_vocabulary VALUES("2027","de","_MS_MINIMUM_NIGHTS","Definiert eine minimale Anzahl der Nächte pro Buchung [<a href=index.php?admin=mod_booking_packages>Eingrenzen Package</a>]");
INSERT INTO phpn_vocabulary VALUES("2028","en","_MS_MINIMUM_NIGHTS","Defines a minimum number of nights per booking [<a href=index.php?admin=mod_booking_packages>Define by Package</a>]");
INSERT INTO phpn_vocabulary VALUES("2029","es","_MS_MINIMUM_NIGHTS","Define un número mínimo de noches por reserva [<a href=index.php?admin=mod_booking_packages>Definir por paquete</a>]");
INSERT INTO phpn_vocabulary VALUES("2030","de","_MS_MULTIPLE_ITEMS_PER_DAY","Gibt an, ob Benutzer sich mehrere Artikel pro Tag oder nicht zulassen");
INSERT INTO phpn_vocabulary VALUES("2031","en","_MS_MULTIPLE_ITEMS_PER_DAY","Specifies whether to allow users to rate multiple items per day or not");
INSERT INTO phpn_vocabulary VALUES("2032","es","_MS_MULTIPLE_ITEMS_PER_DAY","Especifica si se permite a los usuarios artículos tasas múltiples por día o no");
INSERT INTO phpn_vocabulary VALUES("2033","de","_MS_NEWS_COUNT","Definiert, wie viele Nachrichten in Nachrichten-Block gezeigt werden");
INSERT INTO phpn_vocabulary VALUES("2034","en","_MS_NEWS_COUNT","Defines how many news will be shown in news block");
INSERT INTO phpn_vocabulary VALUES("2035","es","_MS_NEWS_COUNT","Define el número de noticias se muestra en el bloque de noticias");
INSERT INTO phpn_vocabulary VALUES("2036","de","_MS_NEWS_HEADER_LENGTH","Definiert eine Länge von Nachrichten-Header in Block");
INSERT INTO phpn_vocabulary VALUES("2037","en","_MS_NEWS_HEADER_LENGTH","Defines a length of news header in block");
INSERT INTO phpn_vocabulary VALUES("2038","es","_MS_NEWS_HEADER_LENGTH","Define una longitud de la cabecera de noticias en el bloque");
INSERT INTO phpn_vocabulary VALUES("2039","de","_MS_NEWS_RSS","Definiert mit der RSS für Nachrichten");
INSERT INTO phpn_vocabulary VALUES("2040","en","_MS_NEWS_RSS","Defines using of RSS for news");
INSERT INTO phpn_vocabulary VALUES("2041","es","_MS_NEWS_RSS","Define el uso de los RSS de noticias");
INSERT INTO phpn_vocabulary VALUES("2042","de","_MS_ONLINE_CREDIT_CARD_REQUIRED","Gibt an, ob das Sammeln von Kreditkartendaten ist für \'On-line Bestellungen\' benötigt");
INSERT INTO phpn_vocabulary VALUES("2043","en","_MS_ONLINE_CREDIT_CARD_REQUIRED","Specifies whether collecting of credit card info is required for \'On-line Orders\'");
INSERT INTO phpn_vocabulary VALUES("2044","es","_MS_ONLINE_CREDIT_CARD_REQUIRED","Especifica si la recogida de información de tarjetas de crédito es necesaria para \'Pedidos on-line\'");
INSERT INTO phpn_vocabulary VALUES("2045","de","_MS_PAYMENT_TYPE_2CO","Gibt an, ob \'2 CO \'Art der Bezahlung zu ermöglichen und zu beantragen, falls es");
INSERT INTO phpn_vocabulary VALUES("2046","en","_MS_PAYMENT_TYPE_2CO","Specifies whether to allow \'2CO\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2047","es","_MS_PAYMENT_TYPE_2CO","Especifica si se permite a \'2 CO \'tipo de pago y donde aplicarlo");
INSERT INTO phpn_vocabulary VALUES("2048","de","_MS_PAYMENT_TYPE_AUTHORIZE","Gibt an, ob \'Authorize.Net\' Art der Bezahlung und wo anwenden");
INSERT INTO phpn_vocabulary VALUES("2049","en","_MS_PAYMENT_TYPE_AUTHORIZE","Specifies whether to allow \'Authorize.Net\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2050","es","_MS_PAYMENT_TYPE_AUTHORIZE","Especifica si se debe permitir \'Authorize.Net\' tipo de pago y donde la aplican");
INSERT INTO phpn_vocabulary VALUES("2051","de","_MS_PAYMENT_TYPE_BANK_TRANSFER","Gibt an, ob \'Banküberweisung\' Art der Bezahlung zu ermöglichen und zu beantragen, falls es");
INSERT INTO phpn_vocabulary VALUES("2052","en","_MS_PAYMENT_TYPE_BANK_TRANSFER","Specifies whether to allow \'Bank Transfer\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2053","es","_MS_PAYMENT_TYPE_BANK_TRANSFER","Especifica si se debe permitir que \"Transferencia bancaria\" tipo de pago y donde aplicarlo");
INSERT INTO phpn_vocabulary VALUES("2054","de","_MS_PAYMENT_TYPE_ONLINE","Gibt an, ob \'On-line Bestellung\' Zahlungsart ermöglichen und wo anwenden");
INSERT INTO phpn_vocabulary VALUES("2055","en","_MS_PAYMENT_TYPE_ONLINE","Specifies whether to allow \'On-line Order\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2056","es","_MS_PAYMENT_TYPE_ONLINE","Especifica si se permite el tipo \'on-line Orden\' de pago y donde aplicarlo");
INSERT INTO phpn_vocabulary VALUES("2057","de","_MS_PAYMENT_TYPE_PAYPAL","Gibt an, ob \'PayPal\' Art der Bezahlung zu ermöglichen und zu beantragen, falls es");
INSERT INTO phpn_vocabulary VALUES("2058","en","_MS_PAYMENT_TYPE_PAYPAL","Specifies whether to allow \'PayPal\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2059","es","_MS_PAYMENT_TYPE_PAYPAL","Especifica si se debe permitir que el tipo de pago \'PayPal\' y donde aplicarlo");
INSERT INTO phpn_vocabulary VALUES("2060","de","_MS_PAYMENT_TYPE_POA","Gibt an, ob (POA) Art der Bezahlung \'bei der Ankunft zu bezahlen\' und wo anwenden");
INSERT INTO phpn_vocabulary VALUES("2061","en","_MS_PAYMENT_TYPE_POA","Specifies whether to allow \'Pay on Arrival\' (POA) payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2062","es","_MS_PAYMENT_TYPE_POA","Especifica si se permite \'Pagar a la llegada\' (POA) el tipo de pago y donde aplicarlo");
INSERT INTO phpn_vocabulary VALUES("2063","de","_MS_PAYPAL_EMAIL","Gibt PayPal (Geschäft) E-Mail");
INSERT INTO phpn_vocabulary VALUES("2064","en","_MS_PAYPAL_EMAIL","Specifies PayPal (business) email ");
INSERT INTO phpn_vocabulary VALUES("2065","es","_MS_PAYPAL_EMAIL","Especifica PayPal (de negocios) de correo electrónico");
INSERT INTO phpn_vocabulary VALUES("2066","de","_MS_PREBOOKING_ORDERS_TIMEOUT","Definiert ein Timeout für \'Vorausbuchung\' Bestellungen vor dem automatischen Löschen (in Stunden)");
INSERT INTO phpn_vocabulary VALUES("2067","en","_MS_PREBOOKING_ORDERS_TIMEOUT","Defines a timeout for \'prebooking\' orders before automatic deleting (in hours)");
INSERT INTO phpn_vocabulary VALUES("2068","es","_MS_PREBOOKING_ORDERS_TIMEOUT","Define un tiempo de espera para \"pre-reservas\" las órdenes antes de eliminar automática (en horas)");
INSERT INTO phpn_vocabulary VALUES("2069","de","_MS_PRE_MODERATION_ALLOW","Gibt an, ob Pre-Moderation für Kommentare zulassen");
INSERT INTO phpn_vocabulary VALUES("2070","en","_MS_PRE_MODERATION_ALLOW","Specifies whether to allow pre-moderation for comments");
INSERT INTO phpn_vocabulary VALUES("2071","es","_MS_PRE_MODERATION_ALLOW","Especifica si se permite antes de la moderación de los comentarios");
INSERT INTO phpn_vocabulary VALUES("2072","de","_MS_PRE_PAYMENT_TYPE","Definiert eine Vorauszahlung Typ (den vollen Preis, ersten Nacht nur, festen Betrag oder Prozentsatz)");
INSERT INTO phpn_vocabulary VALUES("2073","en","_MS_PRE_PAYMENT_TYPE","Defines a pre-payment type (full price, first night only, fixed sum or percentage)");
INSERT INTO phpn_vocabulary VALUES("2074","es","_MS_PRE_PAYMENT_TYPE","Define un tipo de pre-pago (precio total, la primera noche solamente, suma fija o porcentaje)");
INSERT INTO phpn_vocabulary VALUES("2075","de","_MS_PRE_PAYMENT_VALUE","Definiert eine Vorauszahlung Wert für \'fixed sum\' oder \'Prozent\'-Typen");
INSERT INTO phpn_vocabulary VALUES("2076","en","_MS_PRE_PAYMENT_VALUE","Defines a pre-payment value for \'fixed sum\' or \'percentage\' types");
INSERT INTO phpn_vocabulary VALUES("2077","es","_MS_PRE_PAYMENT_VALUE","Define un valor de prepago para \'suma fija\' o tipos de \'porcentaje\'");
INSERT INTO phpn_vocabulary VALUES("2078","de","_MS_RATINGS_USER_TYPE","Typ von Usern, die Hotels bewertet werden können");
INSERT INTO phpn_vocabulary VALUES("2079","en","_MS_RATINGS_USER_TYPE","Type of users, who can rate hotels");
INSERT INTO phpn_vocabulary VALUES("2080","es","_MS_RATINGS_USER_TYPE","Tipo de usuarios, que pueden votar hoteles");
INSERT INTO phpn_vocabulary VALUES("2081","de","_MS_REG_CONFIRMATION","Legt fest, ob die Bestätigung (welche Art) für die Anmeldung ist erforderlich");
INSERT INTO phpn_vocabulary VALUES("2082","en","_MS_REG_CONFIRMATION","Defines whether confirmation (which type of) is required for registration");
INSERT INTO phpn_vocabulary VALUES("2083","es","_MS_REG_CONFIRMATION","Define si la confirmación (el tipo) es necesaria para la inscripción");
INSERT INTO phpn_vocabulary VALUES("2084","de","_MS_REMEMBER_ME","Gibt an, ob Remember Me Funktion");
INSERT INTO phpn_vocabulary VALUES("2085","en","_MS_REMEMBER_ME","Specifies whether to allow Remember Me feature");
INSERT INTO phpn_vocabulary VALUES("2086","es","_MS_REMEMBER_ME","Especifica si se permite Recordarme función");
INSERT INTO phpn_vocabulary VALUES("2087","de","_MS_RESERVATION EXPIRED_ALERT","Gibt an, ob E-Mail-Benachrichtigung an Kunden zu senden, wenn Reservierung abgelaufen");
INSERT INTO phpn_vocabulary VALUES("2088","en","_MS_RESERVATION EXPIRED_ALERT","Specifies whether to send email alert to customer when reservation has expired");
INSERT INTO phpn_vocabulary VALUES("2089","es","_MS_RESERVATION EXPIRED_ALERT","Especifica si se debe enviar alertas de correo electrónico a los clientes cuando la reserva ha caducado");
INSERT INTO phpn_vocabulary VALUES("2090","de","_MS_RESERVATION_INITIAL_FEE","Start (Anfangs-) Gebühr - die Summe, die zu jeder Buchung hinzugefügt werden (Festbetrag in Standard-Währung)");
INSERT INTO phpn_vocabulary VALUES("2091","en","_MS_RESERVATION_INITIAL_FEE","Start (initial) fee - the sum that will be added to each booking (fixed value in default currency)");
INSERT INTO phpn_vocabulary VALUES("2092","es","_MS_RESERVATION_INITIAL_FEE","Inicio (inicial) tarifa - la suma que se añadirá a cada reserva (suma fija en moneda por defecto)");
INSERT INTO phpn_vocabulary VALUES("2093","de","_MS_ROOMS_IN_SEARCH","Gibt an, welche Arten von Zimmern in Suchergebnis zeigen: alle oder nur zur Verfügung stehenden Zimmer (ohne ausgebucht / nicht verfügbar)");
INSERT INTO phpn_vocabulary VALUES("2094","en","_MS_ROOMS_IN_SEARCH","Specifies what types of rooms to show in search result: all or available rooms only (without fully booked / unavailable)");
INSERT INTO phpn_vocabulary VALUES("2095","es","_MS_ROOMS_IN_SEARCH","Especifica los tipos de salas para mostrar en el resultado de la búsqueda: todas las habitaciones o disponible solamente (sin completamente lleno / no disponible)");
INSERT INTO phpn_vocabulary VALUES("2096","de","_MS_ROTATE_DELAY","Definiert Banner Rotation Verzögerung in Sekunden");
INSERT INTO phpn_vocabulary VALUES("2097","en","_MS_ROTATE_DELAY","Defines banners rotation delay in seconds");
INSERT INTO phpn_vocabulary VALUES("2098","es","_MS_ROTATE_DELAY","Define retraso banners de rotación en cuestión de segundos");
INSERT INTO phpn_vocabulary VALUES("2099","de","_MS_ROTATION_TYPE","Verschiedene Arten von Banner-Rotation");
INSERT INTO phpn_vocabulary VALUES("2100","en","_MS_ROTATION_TYPE","Different type of banner rotation");
INSERT INTO phpn_vocabulary VALUES("2101","es","_MS_ROTATION_TYPE","Tipo de rotación de banners");
INSERT INTO phpn_vocabulary VALUES("2102","de","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","Gibt die Anzahl der Zimmer/Hotels, die auf der einen Seite bei der Suche Verfügbarkeit Ergebnisse angezeigt werden");
INSERT INTO phpn_vocabulary VALUES("2103","en","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","Specifies the number of rooms/hotels that will be displayed on one page in the search availability results");
INSERT INTO phpn_vocabulary VALUES("2104","es","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","Especifica el número de habitaciones/hoteles que se mostrará en una página en los resultados de búsqueda de disponibilidad");
INSERT INTO phpn_vocabulary VALUES("2105","de","_MS_SEND_ORDER_COPY_TO_ADMIN","Gibt an, ob Sie eine Kopie, um Admin-oder Hotelbesitzer");
INSERT INTO phpn_vocabulary VALUES("2106","en","_MS_SEND_ORDER_COPY_TO_ADMIN","Specifies whether to allow sending a copy of order to admin or hotel owner");
INSERT INTO phpn_vocabulary VALUES("2107","es","_MS_SEND_ORDER_COPY_TO_ADMIN","Especifica si se permite el envío de una copia de la orden de administrador o propietario del hotel");
INSERT INTO phpn_vocabulary VALUES("2108","de","_MS_SHOW_BOOKING_STATUS_FORM","Gibt an, ob Booking-Status Formular auf der Homepage zeigen oder nicht");
INSERT INTO phpn_vocabulary VALUES("2109","en","_MS_SHOW_BOOKING_STATUS_FORM","Specifies whether to show Booking Status Form on homepage or not");
INSERT INTO phpn_vocabulary VALUES("2110","es","_MS_SHOW_BOOKING_STATUS_FORM","Especifica si se muestra Formulario de Reserva de estado en la página principal o no");
INSERT INTO phpn_vocabulary VALUES("2111","de","_MS_SHOW_DEFAULT_PRICES","Gibt an, ob standardmäßig die Preise auf dem Front-End-oder nicht");
INSERT INTO phpn_vocabulary VALUES("2112","en","_MS_SHOW_DEFAULT_PRICES","Specifies whether to show default prices  on the Front-End or not");
INSERT INTO phpn_vocabulary VALUES("2113","es","_MS_SHOW_DEFAULT_PRICES","Especifica si se deben mostrar los precios predeterminados en el Front-End o no");
INSERT INTO phpn_vocabulary VALUES("2114","de","_MS_SHOW_FULLY_BOOKED_ROOMS","Gibt an, ob Vorführung ausgebucht / nicht verfügbar Zimmer auf der Suche");
INSERT INTO phpn_vocabulary VALUES("2115","en","_MS_SHOW_FULLY_BOOKED_ROOMS","Specifies whether to allow showing of fully booked/unavailable rooms in search");
INSERT INTO phpn_vocabulary VALUES("2116","es","_MS_SHOW_FULLY_BOOKED_ROOMS","Especifica si se permitirá mostrar de habitaciones completamente lleno / no está disponible en la búsqueda");
INSERT INTO phpn_vocabulary VALUES("2117","de","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Legt fest, ob zur Newsletter-Anmeldung sperren oder nicht zeigen");
INSERT INTO phpn_vocabulary VALUES("2118","en","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Defines whether to show Newsletter Subscription block or not");
INSERT INTO phpn_vocabulary VALUES("2119","es","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Define si se debe mostrar bloque Suscripción al boletín o no");
INSERT INTO phpn_vocabulary VALUES("2120","de","_MS_SHOW_NEWS_BLOCK","Legt fest, ob zu News Seite sperren oder nicht zeigen");
INSERT INTO phpn_vocabulary VALUES("2121","en","_MS_SHOW_NEWS_BLOCK","Defines whether to show News side block or not");
INSERT INTO phpn_vocabulary VALUES("2122","es","_MS_SHOW_NEWS_BLOCK","Define si se debe mostrar bloque Noticias lado o no");
INSERT INTO phpn_vocabulary VALUES("2123","de","_MS_SHOW_RESERVATION_FORM","Gibt an, ob Reservation Form auf der Homepage oder nicht zeigen");
INSERT INTO phpn_vocabulary VALUES("2124","en","_MS_SHOW_RESERVATION_FORM","Specifies whether to show Reservation Form on homepage or not");
INSERT INTO phpn_vocabulary VALUES("2125","es","_MS_SHOW_RESERVATION_FORM","Especifica si se muestra formulario de reserva en la página principal o no");
INSERT INTO phpn_vocabulary VALUES("2129","de","_MS_TWO_CHECKOUT_VENDOR","Gibt 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("2130","en","_MS_TWO_CHECKOUT_VENDOR","Specifies 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("2131","es","_MS_TWO_CHECKOUT_VENDOR","Especifica 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("2132","de","_MS_USER_TYPE","Art der Benutzer kann die post comments");
INSERT INTO phpn_vocabulary VALUES("2133","en","_MS_USER_TYPE","Type of users, who can post comments");
INSERT INTO phpn_vocabulary VALUES("2134","es","_MS_USER_TYPE","Tipo de usuarios, que pueden enviar comentarios");
INSERT INTO phpn_vocabulary VALUES("2135","de","_MS_VAT_INCLUDED_IN_PRICE","Gibt an, ob Mehrwertsteuer Gebühr im Zimmer und Extras Preise oder nicht im Lieferumfang enthalten");
INSERT INTO phpn_vocabulary VALUES("2136","en","_MS_VAT_INCLUDED_IN_PRICE","Specifies whether VAT fee is included in room and extras prices or not");
INSERT INTO phpn_vocabulary VALUES("2137","es","_MS_VAT_INCLUDED_IN_PRICE","Especifica si de IVA incluido en la habitación y los precios de los extras o no");
INSERT INTO phpn_vocabulary VALUES("2138","de","_MS_VAT_VALUE","Gibt standardmäßige MwSt. Preis-Leistungs-Ordnung (in%) [ <a href=index.php?admin=countries_management>eingrenzen Land</a> ]");
INSERT INTO phpn_vocabulary VALUES("2139","en","_MS_VAT_VALUE","Specifies default VAT value for order (in %) &nbsp;[<a href=index.php?admin=countries_management>Define by Country</a>]");
INSERT INTO phpn_vocabulary VALUES("2140","es","_MS_VAT_VALUE","Especifica el valor predeterminado de IVA por el orden (en%) [<a href=index.php?admin=countries_management>Definir por País</a>]");
INSERT INTO phpn_vocabulary VALUES("2141","de","_MS_VIDEO_GALLERY_TYPE","Erlaubt Arten von Video-Galerie");
INSERT INTO phpn_vocabulary VALUES("2142","en","_MS_VIDEO_GALLERY_TYPE","Allowed types of Video Gallery");
INSERT INTO phpn_vocabulary VALUES("2143","es","_MS_VIDEO_GALLERY_TYPE","Tipos de animales Galería de Videos");
INSERT INTO phpn_vocabulary VALUES("2144","de","_MS_WATERMARK_TEXT","Wasserzeichen Text, der die Bilder hinzugefügt werden");
INSERT INTO phpn_vocabulary VALUES("2145","en","_MS_WATERMARK_TEXT","Watermark text that will be added to images");
INSERT INTO phpn_vocabulary VALUES("2146","es","_MS_WATERMARK_TEXT","Texto de la marca que se añadirá a las imágenes");
INSERT INTO phpn_vocabulary VALUES("2147","de","_MUST_BE_LOGGED","Sie müssen eingeloggt sein um diese Seite sehen zu können! <a href=\'index.php?customer=login\'>Anmelden</a> oder <a href=\'index.php?customer=create_account\'>Konto erstellen kostenlos</a>.");
INSERT INTO phpn_vocabulary VALUES("2148","en","_MUST_BE_LOGGED","You must be logged in to view this page! <a href=\'index.php?customer=login\'>Login</a> or <a href=\'index.php?customer=create_account\'>Create Account for free</a>.");
INSERT INTO phpn_vocabulary VALUES("2149","es","_MUST_BE_LOGGED","Debe estar registrado para ver esta página! <a href=\'index.php?customer=login\'>Login</a> o <a href=\'index.php?customer=create_account\'>Crear una cuenta gratis</a>.");
INSERT INTO phpn_vocabulary VALUES("2150","de","_MY_ACCOUNT","Ihr Konto");
INSERT INTO phpn_vocabulary VALUES("2151","en","_MY_ACCOUNT","My Account");
INSERT INTO phpn_vocabulary VALUES("2152","es","_MY_ACCOUNT","Mi Cuenta");
INSERT INTO phpn_vocabulary VALUES("2153","de","_MY_BOOKINGS","Meine Buchungen");
INSERT INTO phpn_vocabulary VALUES("2154","en","_MY_BOOKINGS","My Bookings");
INSERT INTO phpn_vocabulary VALUES("2155","es","_MY_BOOKINGS","Mis Reservas");
INSERT INTO phpn_vocabulary VALUES("2156","de","_MY_ORDERS","Meine Bestellungen");
INSERT INTO phpn_vocabulary VALUES("2157","en","_MY_ORDERS","My Orders");
INSERT INTO phpn_vocabulary VALUES("2158","es","_MY_ORDERS","My Orders");
INSERT INTO phpn_vocabulary VALUES("2159","de","_NAME","Name");
INSERT INTO phpn_vocabulary VALUES("2160","en","_NAME","Name");
INSERT INTO phpn_vocabulary VALUES("2161","es","_NAME","Nombre");
INSERT INTO phpn_vocabulary VALUES("2162","de","_NAME_A_Z","name (a-z)");
INSERT INTO phpn_vocabulary VALUES("2163","en","_NAME_A_Z","name (a-z)");
INSERT INTO phpn_vocabulary VALUES("2164","es","_NAME_A_Z","nombre del (a-z)");
INSERT INTO phpn_vocabulary VALUES("2165","de","_NAME_Z_A","name (z-a)");
INSERT INTO phpn_vocabulary VALUES("2166","en","_NAME_Z_A","name (z-a)");
INSERT INTO phpn_vocabulary VALUES("2167","es","_NAME_Z_A","nombre del (z-a)");
INSERT INTO phpn_vocabulary VALUES("2168","de","_NEVER","nie");
INSERT INTO phpn_vocabulary VALUES("2169","en","_NEVER","never");
INSERT INTO phpn_vocabulary VALUES("2170","es","_NEVER","nunca");
INSERT INTO phpn_vocabulary VALUES("2171","de","_NEWS","Nachrichten");
INSERT INTO phpn_vocabulary VALUES("2172","en","_NEWS","News");
INSERT INTO phpn_vocabulary VALUES("2173","es","_NEWS","Noticias");
INSERT INTO phpn_vocabulary VALUES("2174","de","_NEWSLETTER_PAGE_TEXT","<p>Um Newsletter von unserer Website zu erhalten, geben Sie einfach Ihre Email und klicken Sie auf \"Abonnieren\"-Knopf. </p><p> Wenn Sie sich später entscheiden, um Ihr Abonnement beenden oder ändern Sie den Typ der Nachricht erhalten, folgen Sie einfach dem Link am Ende des aktuellen Newsletter und aktualisieren Sie Ihr Profil oder abbestellen, indem Sie das Kontrollkästchen unten.</p>");
INSERT INTO phpn_vocabulary VALUES("2175","en","_NEWSLETTER_PAGE_TEXT","<p>To receive newsletters from our site, simply enter your email and click on \"Subscribe\" button.</p><p>If you later decide to stop your subscription or change the type of news you receive, simply follow the link at the end of the latest newsletter and update your profile or unsubscribe by ticking the checkbox below.</p>");
INSERT INTO phpn_vocabulary VALUES("2176","es","_NEWSLETTER_PAGE_TEXT","<p>Para recibir boletines de noticias de nuestro sitio, simplemente introduzca su email y haga clic en el botón \"Suscribirse\".</p><p>Si posteriormente decide dejar su suscripción o cambiar el tipo de noticias que recibe, sólo tiene que seguir el enlace al final del último boletín de noticias y actualizar su perfil o darse de baja, marcando la casilla a continuación.</p>");
INSERT INTO phpn_vocabulary VALUES("2177","de","_NEWSLETTER_PRE_SUBSCRIBE_ALERT","Bitte klicken Sie auf den \"Abonnieren\"-Knopf, um den Vorgang abzuschließen.");
INSERT INTO phpn_vocabulary VALUES("2178","en","_NEWSLETTER_PRE_SUBSCRIBE_ALERT","Please click on the \"Subscribe\" button to complete the process.");
INSERT INTO phpn_vocabulary VALUES("2179","es","_NEWSLETTER_PRE_SUBSCRIBE_ALERT","Por favor, haga clic en el botón \"Suscribirse\" para completar el proceso.");
INSERT INTO phpn_vocabulary VALUES("2180","de","_NEWSLETTER_PRE_UNSUBSCRIBE_ALERT","Bitte klicken Sie auf die Schaltfläche \"Abmelden\", um den Vorgang abzuschließen.");
INSERT INTO phpn_vocabulary VALUES("2181","en","_NEWSLETTER_PRE_UNSUBSCRIBE_ALERT","Please click on the \"Unsubscribe\" button to complete the process.");
INSERT INTO phpn_vocabulary VALUES("2182","es","_NEWSLETTER_PRE_UNSUBSCRIBE_ALERT","Por favor, haga clic en \"Anular\" para completar el proceso.");
INSERT INTO phpn_vocabulary VALUES("2183","de","_NEWSLETTER_SUBSCRIBERS","Newsletter-Abonnenten");
INSERT INTO phpn_vocabulary VALUES("2184","en","_NEWSLETTER_SUBSCRIBERS","Newsletter Subscribers");
INSERT INTO phpn_vocabulary VALUES("2185","es","_NEWSLETTER_SUBSCRIBERS","Suscriptores del boletín");
INSERT INTO phpn_vocabulary VALUES("2186","de","_NEWSLETTER_SUBSCRIBE_SUCCESS","Danke für die Anmeldung zu unseren elektronischen Newsletter. Sie erhalten eine E-Mail, um Ihr Abonnement zu bestätigen.");
INSERT INTO phpn_vocabulary VALUES("2187","en","_NEWSLETTER_SUBSCRIBE_SUCCESS","Thank you for subscribing to our electronic newsletter. You will receive an e-mail to confirm your subscription.");
INSERT INTO phpn_vocabulary VALUES("2188","es","_NEWSLETTER_SUBSCRIBE_SUCCESS","Gracias por suscribirse a nuestro boletín electrónico. Usted recibirá un e-mail para confirmar su suscripción.");
INSERT INTO phpn_vocabulary VALUES("2189","de","_NEWSLETTER_SUBSCRIBE_TEXT","<p>Um Newsletter von unserer Website zu erhalten, geben Sie einfach Ihre Email und klicken Sie auf \"Abonnieren\"-Knopf. </p><p> Wenn Sie sich später entscheiden, um Ihr Abonnement beenden oder ändern Sie den Typ der Nachricht erhalten, folgen Sie einfach dem Link am Ende des aktuellen Newsletter und aktualisieren Sie Ihr Profil oder abbestellen, indem Sie das Kontrollkästchen unten.</p>");
INSERT INTO phpn_vocabulary VALUES("2190","en","_NEWSLETTER_SUBSCRIBE_TEXT","<p>To receive newsletters from our site, simply enter your email and click on \"Subscribe\" button.</p><p>If you later decide to stop your subscription or change the type of news you receive, simply follow the link at the end of the latest newsletter and update your profile or unsubscribe by ticking the checkbox below.</p>");
INSERT INTO phpn_vocabulary VALUES("2191","es","_NEWSLETTER_SUBSCRIBE_TEXT","<p>Para recibir boletines de noticias de nuestro sitio, simplemente introduzca su email y haga clic en el botón \"Suscribirse\".</p><p>Si posteriormente decide dejar su suscripción o cambiar el tipo de noticias que recibe, sólo tiene que seguir el enlace al final del último boletín de noticias y actualizar su perfil o darse de baja, marcando la casilla a continuación.</p>");
INSERT INTO phpn_vocabulary VALUES("2192","de","_NEWSLETTER_SUBSCRIPTION_MANAGEMENT","Newsletter-Abonnement-Verwaltung");
INSERT INTO phpn_vocabulary VALUES("2193","en","_NEWSLETTER_SUBSCRIPTION_MANAGEMENT","Newsletter Subscription Management");
INSERT INTO phpn_vocabulary VALUES("2194","es","_NEWSLETTER_SUBSCRIPTION_MANAGEMENT","Boletín de administración de suscripciones");
INSERT INTO phpn_vocabulary VALUES("2195","de","_NEWSLETTER_UNSUBSCRIBE_SUCCESS","Sie haben sich erfolgreich von unserem Newsletter abgemeldet!");
INSERT INTO phpn_vocabulary VALUES("2196","en","_NEWSLETTER_UNSUBSCRIBE_SUCCESS","You have been successfully unsubscribed from our newsletter!");
INSERT INTO phpn_vocabulary VALUES("2197","es","_NEWSLETTER_UNSUBSCRIBE_SUCCESS","Que han sido éxito de baja de nuestro boletín de noticias!");
INSERT INTO phpn_vocabulary VALUES("2198","de","_NEWSLETTER_UNSUBSCRIBE_TEXT","<p>Um von unserem Newsletter abmelden, geben Sie Ihre E-Mail-Adresse ein und klicken Sie auf die Schaltfläche Abbestellen.</p>");
INSERT INTO phpn_vocabulary VALUES("2199","en","_NEWSLETTER_UNSUBSCRIBE_TEXT","<p>To unsubscribe from our newsletters, enter your email address below and click the unsubscribe button.</p>");
INSERT INTO phpn_vocabulary VALUES("2200","es","_NEWSLETTER_UNSUBSCRIBE_TEXT","<p>Para darse de baja de nuestros boletines, introduzca su dirección de correo electrónico y haga clic en el botón de darse de baja.</p>");
INSERT INTO phpn_vocabulary VALUES("2201","de","_NEWS_AND_EVENTS","News &amp; Events");
INSERT INTO phpn_vocabulary VALUES("2202","en","_NEWS_AND_EVENTS","News & Events");
INSERT INTO phpn_vocabulary VALUES("2203","es","_NEWS_AND_EVENTS","Noticias y Eventos");
INSERT INTO phpn_vocabulary VALUES("2204","de","_NEWS_MANAGEMENT","News Management");
INSERT INTO phpn_vocabulary VALUES("2205","en","_NEWS_MANAGEMENT","News Management");
INSERT INTO phpn_vocabulary VALUES("2206","es","_NEWS_MANAGEMENT","Gestión de Noticias");
INSERT INTO phpn_vocabulary VALUES("2207","de","_NEWS_SETTINGS","News Einstellungen");
INSERT INTO phpn_vocabulary VALUES("2208","en","_NEWS_SETTINGS","News Settings");
INSERT INTO phpn_vocabulary VALUES("2209","es","_NEWS_SETTINGS","Noticias Ajustes");
INSERT INTO phpn_vocabulary VALUES("2210","de","_NEXT","nächste");
INSERT INTO phpn_vocabulary VALUES("2211","en","_NEXT","Next");
INSERT INTO phpn_vocabulary VALUES("2212","es","_NEXT","Próximo");
INSERT INTO phpn_vocabulary VALUES("2213","de","_NIGHT","Nacht");
INSERT INTO phpn_vocabulary VALUES("2214","en","_NIGHT","Night");
INSERT INTO phpn_vocabulary VALUES("2215","es","_NIGHT","Noche");
INSERT INTO phpn_vocabulary VALUES("2216","de","_NIGHTS","Nächte");
INSERT INTO phpn_vocabulary VALUES("2217","en","_NIGHTS","Nights");
INSERT INTO phpn_vocabulary VALUES("2218","es","_NIGHTS","Noches");
INSERT INTO phpn_vocabulary VALUES("2219","de","_NO","Keine");
INSERT INTO phpn_vocabulary VALUES("2220","en","_NO","No");
INSERT INTO phpn_vocabulary VALUES("2221","es","_NO","No");
INSERT INTO phpn_vocabulary VALUES("2222","de","_NONE","keiner");
INSERT INTO phpn_vocabulary VALUES("2223","en","_NONE","None");
INSERT INTO phpn_vocabulary VALUES("2224","es","_NONE","Ninguno");
INSERT INTO phpn_vocabulary VALUES("2225","de","_NOTICE_MODULES_CODE","Um die verfügbaren Module zu dieser Seite hinzufügen nur kopieren und einfügen in den Text:");
INSERT INTO phpn_vocabulary VALUES("2226","en","_NOTICE_MODULES_CODE","To add available modules to this page just copy and paste into the text:");
INSERT INTO phpn_vocabulary VALUES("2227","es","_NOTICE_MODULES_CODE","Para agregar módulos a disposición de esta página sólo tienes que copiar y pegar en el texto:");
INSERT INTO phpn_vocabulary VALUES("2228","de","_NOTIFICATION_MSG","Bitte senden Sie mir Informationen über Updates");
INSERT INTO phpn_vocabulary VALUES("2229","en","_NOTIFICATION_MSG","Please send me information about specials and discounts!");
INSERT INTO phpn_vocabulary VALUES("2230","es","_NOTIFICATION_MSG","Por favor, envíenme información acerca de ofertas especiales y descuentos!");
INSERT INTO phpn_vocabulary VALUES("2231","de","_NOTIFICATION_STATUS_CHANGED","Notification Status geändert");
INSERT INTO phpn_vocabulary VALUES("2232","en","_NOTIFICATION_STATUS_CHANGED","Notification status changed");
INSERT INTO phpn_vocabulary VALUES("2233","es","_NOTIFICATION_STATUS_CHANGED","Estatuto de notificación cambiado");
INSERT INTO phpn_vocabulary VALUES("2234","de","_NOT_ALLOWED","Nicht Erlaubt");
INSERT INTO phpn_vocabulary VALUES("2235","en","_NOT_ALLOWED","Not Allowed");
INSERT INTO phpn_vocabulary VALUES("2236","es","_NOT_ALLOWED","No se admiten");
INSERT INTO phpn_vocabulary VALUES("2237","de","_NOT_AUTHORIZED","Sie sind nicht berechtigt");
INSERT INTO phpn_vocabulary VALUES("2238","en","_NOT_AUTHORIZED","You are not authorized to view this page.");
INSERT INTO phpn_vocabulary VALUES("2239","es","_NOT_AUTHORIZED","Usted no está autorizado a ver esta página.");
INSERT INTO phpn_vocabulary VALUES("2240","de","_NOT_AVAILABLE","n/a");
INSERT INTO phpn_vocabulary VALUES("2241","en","_NOT_AVAILABLE","N/A");
INSERT INTO phpn_vocabulary VALUES("2242","es","_NOT_AVAILABLE","N/A");
INSERT INTO phpn_vocabulary VALUES("2243","de","_NOT_PAID_YET","Noch nicht bezahlt");
INSERT INTO phpn_vocabulary VALUES("2244","en","_NOT_PAID_YET","Not paid yet");
INSERT INTO phpn_vocabulary VALUES("2245","es","_NOT_PAID_YET","Aún no pagados");
INSERT INTO phpn_vocabulary VALUES("2246","de","_NOVEMBER","November");
INSERT INTO phpn_vocabulary VALUES("2247","en","_NOVEMBER","November");
INSERT INTO phpn_vocabulary VALUES("2248","es","_NOVEMBER","Noviembre");
INSERT INTO phpn_vocabulary VALUES("2249","de","_NO_AVAILABLE","Nicht verfügbar");
INSERT INTO phpn_vocabulary VALUES("2250","en","_NO_AVAILABLE","Not Available");
INSERT INTO phpn_vocabulary VALUES("2251","es","_NO_AVAILABLE","No está disponible");
INSERT INTO phpn_vocabulary VALUES("2252","de","_NO_BOOKING_FOUND","Die Zahl der Buchung die Sie eingegeben haben war nicht in unserem System gefunden! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("2253","en","_NO_BOOKING_FOUND","The number of booking you\'ve entered has not been found in our system! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2254","es","_NO_BOOKING_FOUND","El número de reserva que has introducido no se encuentra en nuestro sistema! Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("2255","de","_NO_COMMENTS_YET","Noch keine Kommentare.");
INSERT INTO phpn_vocabulary VALUES("2256","en","_NO_COMMENTS_YET","No comments yet.");
INSERT INTO phpn_vocabulary VALUES("2257","es","_NO_COMMENTS_YET","Todavía no hay comentarios.");
INSERT INTO phpn_vocabulary VALUES("2258","de","_NO_CUSTOMER_FOUND","Kein Kunde gefunden!");
INSERT INTO phpn_vocabulary VALUES("2259","en","_NO_CUSTOMER_FOUND","No customer found!");
INSERT INTO phpn_vocabulary VALUES("2260","es","_NO_CUSTOMER_FOUND","Ningún cliente ha encontrado!");
INSERT INTO phpn_vocabulary VALUES("2261","de","_NO_DEFAULT_PERIODS","Standard Zeiträume noch nicht für dieses Hotel definiert. Klicken Sie <a href=_HREF_>hier</a>, um sie hinzuzufügen.");
INSERT INTO phpn_vocabulary VALUES("2262","en","_NO_DEFAULT_PERIODS","Default periods not yet defined for this hotel. Click <a href=_HREF_>here</a> to add them.");
INSERT INTO phpn_vocabulary VALUES("2263","es","_NO_DEFAULT_PERIODS","Períodos por defecto no definido para este hotel. Haga clic en <a href=_HREF_>aquí</a> para agregarlos.");
INSERT INTO phpn_vocabulary VALUES("2264","de","_NO_NEWS","Keine Nachrichten");
INSERT INTO phpn_vocabulary VALUES("2265","en","_NO_NEWS","No news");
INSERT INTO phpn_vocabulary VALUES("2266","es","_NO_NEWS","No hay noticias");
INSERT INTO phpn_vocabulary VALUES("2267","de","_NO_PAYMENT_METHODS_ALERT","Keine Zahlungsmethoden zur Verfügung! Bitte kontaktieren Sie unseren technischen Support.");
INSERT INTO phpn_vocabulary VALUES("2268","en","_NO_PAYMENT_METHODS_ALERT","No payment options are available! Please try later or contact our technical support.");
INSERT INTO phpn_vocabulary VALUES("2269","es","_NO_PAYMENT_METHODS_ALERT","No hay métodos de pago disponibles! Por favor, póngase en contacto con nuestro soporte técnico.");
INSERT INTO phpn_vocabulary VALUES("2270","de","_NO_RECORDS_FOUND","Keine Datensätze gefunden");
INSERT INTO phpn_vocabulary VALUES("2271","en","_NO_RECORDS_FOUND","No records found");
INSERT INTO phpn_vocabulary VALUES("2272","es","_NO_RECORDS_FOUND","No se encontraron registros");
INSERT INTO phpn_vocabulary VALUES("2273","de","_NO_RECORDS_PROCESSED","Keine Datensätze zur Verarbeitung gefunden!");
INSERT INTO phpn_vocabulary VALUES("2274","en","_NO_RECORDS_PROCESSED","No records found for processing!");
INSERT INTO phpn_vocabulary VALUES("2275","es","_NO_RECORDS_PROCESSED","No se encontraron registros para el tratamiento!");
INSERT INTO phpn_vocabulary VALUES("2276","de","_NO_RECORDS_UPDATED","Keine Datensätze wurden aktualisiert!");
INSERT INTO phpn_vocabulary VALUES("2277","en","_NO_RECORDS_UPDATED","No records were updated!");
INSERT INTO phpn_vocabulary VALUES("2278","es","_NO_RECORDS_UPDATED","No hay registros fueron actualizados!");
INSERT INTO phpn_vocabulary VALUES("2279","de","_NO_ROOMS_FOUND","Sorry, es gibt keine Räume, die den Suchkriterien entsprechen. Bitte ändern Sie Ihre Suchkriterien ein, um mehr Räume zu sehen.");
INSERT INTO phpn_vocabulary VALUES("2280","en","_NO_ROOMS_FOUND","Sorry, there are no rooms that match your search. Please change your search criteria to see more rooms.");
INSERT INTO phpn_vocabulary VALUES("2281","es","_NO_ROOMS_FOUND","Lo sentimos, no hay habitaciones que coinciden con su búsqueda. Por favor cambie sus criterios de búsqueda para ver más habitaciones.");
INSERT INTO phpn_vocabulary VALUES("2282","de","_NO_TEMPLATE","keine vorlage");
INSERT INTO phpn_vocabulary VALUES("2283","en","_NO_TEMPLATE","no template");
INSERT INTO phpn_vocabulary VALUES("2284","es","_NO_TEMPLATE","sin plantilla");
INSERT INTO phpn_vocabulary VALUES("2285","de","_NO_USER_EMAIL_EXISTS_ALERT","Es scheint, dass Sie bereits gebuchten Zimmer mit uns! <br>Bitte klicken Sie <a href=index.php?customer=reset_account>hier</a>, um Ihren Benutzernamen zurückgesetzt und bekommen ein temporäres Passwort.");
INSERT INTO phpn_vocabulary VALUES("2286","en","_NO_USER_EMAIL_EXISTS_ALERT","It seems that you already booked rooms with us! <br>Please click <a href=index.php?customer=reset_account>here</a> to reset your username and get a temporary password. ");
INSERT INTO phpn_vocabulary VALUES("2287","es","_NO_USER_EMAIL_EXISTS_ALERT","Parece que ya reservaron habitaciones con nosotros! <br>Por favor, haga clic <a href=index.php?customer=reset_account>aquí</a> para restablecer su nombre de usuario y obtener una contraseña temporal.");
INSERT INTO phpn_vocabulary VALUES("2288","de","_NO_WRITE_ACCESS_ALERT","Bitte überprüfen Sie Schreibzugriff auf folgenden Verzeichnissen:");
INSERT INTO phpn_vocabulary VALUES("2289","en","_NO_WRITE_ACCESS_ALERT","Please check you have write access to following directories:");
INSERT INTO phpn_vocabulary VALUES("2290","es","_NO_WRITE_ACCESS_ALERT","Por favor, compruebe que tiene acceso de escritura a los directorios siguientes:");
INSERT INTO phpn_vocabulary VALUES("2291","de","_OCCUPANCY","Belegung");
INSERT INTO phpn_vocabulary VALUES("2292","en","_OCCUPANCY","Occupancy");
INSERT INTO phpn_vocabulary VALUES("2293","es","_OCCUPANCY","Ocupación");
INSERT INTO phpn_vocabulary VALUES("2294","de","_OCTOBER","Oktober");
INSERT INTO phpn_vocabulary VALUES("2295","en","_OCTOBER","October");
INSERT INTO phpn_vocabulary VALUES("2296","es","_OCTOBER","Octubre");
INSERT INTO phpn_vocabulary VALUES("2297","de","_OFF","aus");
INSERT INTO phpn_vocabulary VALUES("2298","en","_OFF","Off");
INSERT INTO phpn_vocabulary VALUES("2299","es","_OFF","Desc.");
INSERT INTO phpn_vocabulary VALUES("2300","de","_OFFLINE_LOGIN_ALERT","So melden Sie sich beim Admin Panel Website offline ist");
INSERT INTO phpn_vocabulary VALUES("2301","en","_OFFLINE_LOGIN_ALERT","To log into Admin Panel when site is offline, type in your browser: http://{your_site_address}/index.php?admin=login");
INSERT INTO phpn_vocabulary VALUES("2302","es","_OFFLINE_LOGIN_ALERT","Para entrar en el panel de administración cuando el sitio está en línea, escriba en su navegador: http:// {your_site_address}/index.php?admin=login");
INSERT INTO phpn_vocabulary VALUES("2303","de","_OFFLINE_MESSAGE","Offline-Nachricht");
INSERT INTO phpn_vocabulary VALUES("2304","en","_OFFLINE_MESSAGE","Offline Message");
INSERT INTO phpn_vocabulary VALUES("2305","es","_OFFLINE_MESSAGE","Mensaje Desconectado");
INSERT INTO phpn_vocabulary VALUES("2306","de","_ON","an");
INSERT INTO phpn_vocabulary VALUES("2307","en","_ON","On");
INSERT INTO phpn_vocabulary VALUES("2308","es","_ON","Activ.");
INSERT INTO phpn_vocabulary VALUES("2309","de","_ONLINE","Online");
INSERT INTO phpn_vocabulary VALUES("2310","en","_ONLINE","Online");
INSERT INTO phpn_vocabulary VALUES("2311","es","_ONLINE","Online");
INSERT INTO phpn_vocabulary VALUES("2312","de","_ONLINE_ORDER","On-line Bestellen");
INSERT INTO phpn_vocabulary VALUES("2313","en","_ONLINE_ORDER","On-line Order");
INSERT INTO phpn_vocabulary VALUES("2314","es","_ONLINE_ORDER","Orden en línea");
INSERT INTO phpn_vocabulary VALUES("2315","de","_ONLY","nur");
INSERT INTO phpn_vocabulary VALUES("2316","en","_ONLY","Only");
INSERT INTO phpn_vocabulary VALUES("2317","es","_ONLY","Sólo");
INSERT INTO phpn_vocabulary VALUES("2318","de","_OPEN","offen");
INSERT INTO phpn_vocabulary VALUES("2319","en","_OPEN","Open");
INSERT INTO phpn_vocabulary VALUES("2320","es","_OPEN","Abrir");
INSERT INTO phpn_vocabulary VALUES("2321","de","_OPEN_ALERT_WINDOW","Open Alert Window");
INSERT INTO phpn_vocabulary VALUES("2322","en","_OPEN_ALERT_WINDOW","Open Alert Window");
INSERT INTO phpn_vocabulary VALUES("2323","es","_OPEN_ALERT_WINDOW","Abra la ventana de alerta");
INSERT INTO phpn_vocabulary VALUES("2324","de","_OPERATION_BLOCKED","Dieser Vorgang ist in der Demoversion gesperrt!");
INSERT INTO phpn_vocabulary VALUES("2325","en","_OPERATION_BLOCKED","This operation is blocked in Demo Version!");
INSERT INTO phpn_vocabulary VALUES("2326","es","_OPERATION_BLOCKED","Esta operación está bloqueada en Versión Demo!");
INSERT INTO phpn_vocabulary VALUES("2327","de","_OPERATION_COMMON_COMPLETED","Die Operation wurde erfolgreich abgeschlossen!");
INSERT INTO phpn_vocabulary VALUES("2328","en","_OPERATION_COMMON_COMPLETED","The operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("2329","es","_OPERATION_COMMON_COMPLETED","La operación se completó con éxito!");
INSERT INTO phpn_vocabulary VALUES("2330","de","_OPERATION_WAS_ALREADY_COMPLETED","Dieser Vorgang wurde bereits abgeschlossen!");
INSERT INTO phpn_vocabulary VALUES("2331","en","_OPERATION_WAS_ALREADY_COMPLETED","This operation has been already completed!");
INSERT INTO phpn_vocabulary VALUES("2332","es","_OPERATION_WAS_ALREADY_COMPLETED","Esta operación ya se había terminado!");
INSERT INTO phpn_vocabulary VALUES("2333","de","_OR","oder");
INSERT INTO phpn_vocabulary VALUES("2334","en","_OR","or");
INSERT INTO phpn_vocabulary VALUES("2335","es","_OR","o");
INSERT INTO phpn_vocabulary VALUES("2336","de","_ORDER","Bestellen");
INSERT INTO phpn_vocabulary VALUES("2337","en","_ORDER","Order");
INSERT INTO phpn_vocabulary VALUES("2338","es","_ORDER","Orden");
INSERT INTO phpn_vocabulary VALUES("2339","de","_ORDERS","Bestellungen");
INSERT INTO phpn_vocabulary VALUES("2340","en","_ORDERS","Orders");
INSERT INTO phpn_vocabulary VALUES("2341","es","_ORDERS","Órdenes");
INSERT INTO phpn_vocabulary VALUES("2342","de","_ORDERS_COUNT","Bestellungen rechnen");
INSERT INTO phpn_vocabulary VALUES("2343","en","_ORDERS_COUNT","Orders count");
INSERT INTO phpn_vocabulary VALUES("2344","es","_ORDERS_COUNT","Órdenes de contar");
INSERT INTO phpn_vocabulary VALUES("2345","de","_ORDER_DATE","Sortierung: Datum");
INSERT INTO phpn_vocabulary VALUES("2346","en","_ORDER_DATE","Order Date");
INSERT INTO phpn_vocabulary VALUES("2347","es","_ORDER_DATE","Fecha del pedido");
INSERT INTO phpn_vocabulary VALUES("2348","de","_ORDER_ERROR","Kann nicht abgeschlossen werden Ihre Bestellung! Bitte versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("2349","en","_ORDER_ERROR","Cannot complete your order! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("2350","es","_ORDER_ERROR","No se puede completar su solicitud! Por favor, inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("2351","de","_ORDER_NOW","Jetzt bestellen");
INSERT INTO phpn_vocabulary VALUES("2352","en","_ORDER_NOW","Order Now");
INSERT INTO phpn_vocabulary VALUES("2353","es","_ORDER_NOW","Ordene ahora");
INSERT INTO phpn_vocabulary VALUES("2354","de","_ORDER_PLACED_MSG","Danke! Der Auftrag ist in unserem System platziert und wird in Kürze bearbeitet werden. Ihre Buchungsnummer ist: _BOOKING_NUMBER_. ");
INSERT INTO phpn_vocabulary VALUES("2355","en","_ORDER_PLACED_MSG","Thank you! The order has been placed in our system and will be processed shortly. Your booking number is: _BOOKING_NUMBER_.");
INSERT INTO phpn_vocabulary VALUES("2356","es","_ORDER_PLACED_MSG","¡Gracias! El pedido ha sido realizado en nuestro sistema y se procesará en breve. Su número de reserva es la siguiente: _BOOKING_NUMBER_.");
INSERT INTO phpn_vocabulary VALUES("2357","de","_ORDER_PRICE","Bestellen Preis");
INSERT INTO phpn_vocabulary VALUES("2358","en","_ORDER_PRICE","Order Price");
INSERT INTO phpn_vocabulary VALUES("2359","es","_ORDER_PRICE","Orden de precios");
INSERT INTO phpn_vocabulary VALUES("2360","de","_OTHER","Anderen");
INSERT INTO phpn_vocabulary VALUES("2361","en","_OTHER","Other");
INSERT INTO phpn_vocabulary VALUES("2362","es","_OTHER","Otro");
INSERT INTO phpn_vocabulary VALUES("2363","de","_OUR_LOCATION","Unser Standort");
INSERT INTO phpn_vocabulary VALUES("2364","en","_OUR_LOCATION","Our location");
INSERT INTO phpn_vocabulary VALUES("2365","es","_OUR_LOCATION","Nuestra ubicación");
INSERT INTO phpn_vocabulary VALUES("2366","de","_OWNER","Eigentümer");
INSERT INTO phpn_vocabulary VALUES("2367","en","_OWNER","Owner");
INSERT INTO phpn_vocabulary VALUES("2368","es","_OWNER","Propietario");
INSERT INTO phpn_vocabulary VALUES("2369","de","_OWNER_NOT_ASSIGNED","Sie hat immer noch nicht zu jedem Hotel zugewiesen wurde, um die Berichte zu sehen.");
INSERT INTO phpn_vocabulary VALUES("2370","en","_OWNER_NOT_ASSIGNED","You still has not been assigned to any hotel to see the reports.");
INSERT INTO phpn_vocabulary VALUES("2371","es","_OWNER_NOT_ASSIGNED","Todavía no se ha asignado a ningún hotel para ver los informes.");
INSERT INTO phpn_vocabulary VALUES("2372","de","_PACKAGES","Pakete");
INSERT INTO phpn_vocabulary VALUES("2373","en","_PACKAGES","Packages");
INSERT INTO phpn_vocabulary VALUES("2374","es","_PACKAGES","Paquetes");
INSERT INTO phpn_vocabulary VALUES("2375","de","_PACKAGES_MANAGEMENT","Pakete-Management");
INSERT INTO phpn_vocabulary VALUES("2376","en","_PACKAGES_MANAGEMENT","Packages Management");
INSERT INTO phpn_vocabulary VALUES("2377","es","_PACKAGES_MANAGEMENT","Gestión de paquetes");
INSERT INTO phpn_vocabulary VALUES("2378","de","_PAGE","Seite");
INSERT INTO phpn_vocabulary VALUES("2379","en","_PAGE","Page");
INSERT INTO phpn_vocabulary VALUES("2380","es","_PAGE","Página");
INSERT INTO phpn_vocabulary VALUES("2381","de","_PAGES","Seiten");
INSERT INTO phpn_vocabulary VALUES("2382","en","_PAGES","Pages");
INSERT INTO phpn_vocabulary VALUES("2383","es","_PAGES","Páginas");
INSERT INTO phpn_vocabulary VALUES("2384","de","_PAGE_ADD_NEW","Neue Seite hinzufügen");
INSERT INTO phpn_vocabulary VALUES("2385","en","_PAGE_ADD_NEW","Add New Page");
INSERT INTO phpn_vocabulary VALUES("2386","es","_PAGE_ADD_NEW","Add New Page");
INSERT INTO phpn_vocabulary VALUES("2387","de","_PAGE_CREATED","Seite wurde erfolgreich erstellt");
INSERT INTO phpn_vocabulary VALUES("2388","en","_PAGE_CREATED","Page has been successfully created");
INSERT INTO phpn_vocabulary VALUES("2389","es","_PAGE_CREATED","La página se ha creado correctamente");
INSERT INTO phpn_vocabulary VALUES("2390","de","_PAGE_DELETED","Seite wurde erfolgreich gelöscht");
INSERT INTO phpn_vocabulary VALUES("2391","en","_PAGE_DELETED","Page has been successfully deleted");
INSERT INTO phpn_vocabulary VALUES("2392","es","_PAGE_DELETED","Page ha sido borrado");
INSERT INTO phpn_vocabulary VALUES("2393","de","_PAGE_DELETE_WARNING","Sind Sie sicher");
INSERT INTO phpn_vocabulary VALUES("2394","en","_PAGE_DELETE_WARNING","Are you sure you want to delete this page?");
INSERT INTO phpn_vocabulary VALUES("2395","es","_PAGE_DELETE_WARNING","¿Estás seguro de que deseas eliminar esta página?");
INSERT INTO phpn_vocabulary VALUES("2396","de","_PAGE_EDIT_HOME","Bearbeiten Home Page");
INSERT INTO phpn_vocabulary VALUES("2397","en","_PAGE_EDIT_HOME","Edit Home Page");
INSERT INTO phpn_vocabulary VALUES("2398","es","_PAGE_EDIT_HOME","Editar Home Page");
INSERT INTO phpn_vocabulary VALUES("2399","de","_PAGE_EDIT_PAGES","Bearbeiten Seiten");
INSERT INTO phpn_vocabulary VALUES("2400","en","_PAGE_EDIT_PAGES","Edit Pages");
INSERT INTO phpn_vocabulary VALUES("2401","es","_PAGE_EDIT_PAGES","Editar páginas");
INSERT INTO phpn_vocabulary VALUES("2402","de","_PAGE_EDIT_SYS_PAGES","Edit System Seiten");
INSERT INTO phpn_vocabulary VALUES("2403","en","_PAGE_EDIT_SYS_PAGES","Edit System Pages");
INSERT INTO phpn_vocabulary VALUES("2404","es","_PAGE_EDIT_SYS_PAGES","Editar páginas del sistema");
INSERT INTO phpn_vocabulary VALUES("2405","de","_PAGE_EXPIRED","Der von Ihnen angeforderte Seite ist abgelaufen!");
INSERT INTO phpn_vocabulary VALUES("2406","en","_PAGE_EXPIRED","The page you requested has expired!");
INSERT INTO phpn_vocabulary VALUES("2407","es","_PAGE_EXPIRED","Página solicitada ha caducado!");
INSERT INTO phpn_vocabulary VALUES("2408","de","_PAGE_HEADER","Seitenkopf");
INSERT INTO phpn_vocabulary VALUES("2409","en","_PAGE_HEADER","Page Header");
INSERT INTO phpn_vocabulary VALUES("2410","es","_PAGE_HEADER","Encabezado de página");
INSERT INTO phpn_vocabulary VALUES("2411","de","_PAGE_HEADER_EMPTY","Page Header kann nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("2412","en","_PAGE_HEADER_EMPTY","Page header cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("2413","es","_PAGE_HEADER_EMPTY","Cabecera de la página no puede estar vacía!");
INSERT INTO phpn_vocabulary VALUES("2414","de","_PAGE_KEY_EMPTY","Page-Taste kann nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("2415","en","_PAGE_KEY_EMPTY","Page key cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("2416","es","_PAGE_KEY_EMPTY","Page clave no puede estar vacía!");
INSERT INTO phpn_vocabulary VALUES("2417","de","_PAGE_LINK_TOO_LONG","Link Menu zu lang!");
INSERT INTO phpn_vocabulary VALUES("2418","en","_PAGE_LINK_TOO_LONG","Menu link too long!");
INSERT INTO phpn_vocabulary VALUES("2419","es","_PAGE_LINK_TOO_LONG","Enlace del menú demasiado tiempo!");
INSERT INTO phpn_vocabulary VALUES("2420","de","_PAGE_MANAGEMENT","Seiten-Management");
INSERT INTO phpn_vocabulary VALUES("2421","en","_PAGE_MANAGEMENT","Pages Management");
INSERT INTO phpn_vocabulary VALUES("2422","es","_PAGE_MANAGEMENT","Páginas de Gestión de");
INSERT INTO phpn_vocabulary VALUES("2423","de","_PAGE_NOT_CREATED","Seite wurde nicht erstellt!");
INSERT INTO phpn_vocabulary VALUES("2424","en","_PAGE_NOT_CREATED","Page has not been created!");
INSERT INTO phpn_vocabulary VALUES("2425","es","_PAGE_NOT_CREATED","La página no ha sido creado!");
INSERT INTO phpn_vocabulary VALUES("2426","de","_PAGE_NOT_DELETED","Seite wurde nicht gelöscht!");
INSERT INTO phpn_vocabulary VALUES("2427","en","_PAGE_NOT_DELETED","Page has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("2428","es","_PAGE_NOT_DELETED","La página no se ha eliminado!");
INSERT INTO phpn_vocabulary VALUES("2429","de","_PAGE_NOT_EXISTS","Die Seite");
INSERT INTO phpn_vocabulary VALUES("2430","en","_PAGE_NOT_EXISTS","The page you attempted to access does not exist");
INSERT INTO phpn_vocabulary VALUES("2431","es","_PAGE_NOT_EXISTS","La página que intenta acceder no existe");
INSERT INTO phpn_vocabulary VALUES("2432","de","_PAGE_NOT_FOUND","Keine Seiten gefunden");
INSERT INTO phpn_vocabulary VALUES("2433","en","_PAGE_NOT_FOUND","No Pages Found");
INSERT INTO phpn_vocabulary VALUES("2434","es","_PAGE_NOT_FOUND","No se han encontrado páginas");
INSERT INTO phpn_vocabulary VALUES("2435","de","_PAGE_NOT_SAVED","Diese Seite wurde nicht gespart!");
INSERT INTO phpn_vocabulary VALUES("2436","en","_PAGE_NOT_SAVED","Page has not been saved!");
INSERT INTO phpn_vocabulary VALUES("2437","es","_PAGE_NOT_SAVED","La página no se salvó!");
INSERT INTO phpn_vocabulary VALUES("2438","de","_PAGE_ORDER_CHANGED","Page Bestellung wurde erfolgreich geändert!");
INSERT INTO phpn_vocabulary VALUES("2439","en","_PAGE_ORDER_CHANGED","Page order has been successfully changed!");
INSERT INTO phpn_vocabulary VALUES("2440","es","_PAGE_ORDER_CHANGED","Orden de las páginas se ha cambiado correctamente!");
INSERT INTO phpn_vocabulary VALUES("2441","de","_PAGE_REMOVED","Diese Seite wurde erfolgreich entfernt!");
INSERT INTO phpn_vocabulary VALUES("2442","en","_PAGE_REMOVED","Page has been successfully removed!");
INSERT INTO phpn_vocabulary VALUES("2443","es","_PAGE_REMOVED","Page se ha eliminado correctamente!");
INSERT INTO phpn_vocabulary VALUES("2444","de","_PAGE_REMOVE_WARNING","Sie sind sicher");
INSERT INTO phpn_vocabulary VALUES("2445","en","_PAGE_REMOVE_WARNING","Are you sure you want to move this page to the Trash?");
INSERT INTO phpn_vocabulary VALUES("2446","es","_PAGE_REMOVE_WARNING","¿Está seguro que desea mover esta pagina a la papelera?");
INSERT INTO phpn_vocabulary VALUES("2447","de","_PAGE_RESTORED","Diese Seite wurde erfolgreich wiederhergestellt!");
INSERT INTO phpn_vocabulary VALUES("2448","en","_PAGE_RESTORED","Page has been successfully restored!");
INSERT INTO phpn_vocabulary VALUES("2449","es","_PAGE_RESTORED","Page fue restaurado con éxito!");
INSERT INTO phpn_vocabulary VALUES("2450","de","_PAGE_RESTORE_WARNING","Sind Sie sicher");
INSERT INTO phpn_vocabulary VALUES("2451","en","_PAGE_RESTORE_WARNING","Are you sure you want to restore this page?");
INSERT INTO phpn_vocabulary VALUES("2452","es","_PAGE_RESTORE_WARNING","¿Estás seguro de que desea restaurar esta página?");
INSERT INTO phpn_vocabulary VALUES("2453","de","_PAGE_SAVED","Diese Seite wurde erfolgreich gespeichert");
INSERT INTO phpn_vocabulary VALUES("2454","en","_PAGE_SAVED","Page has been successfully saved!");
INSERT INTO phpn_vocabulary VALUES("2455","es","_PAGE_SAVED","La página se ha guardado correctamente");
INSERT INTO phpn_vocabulary VALUES("2456","de","_PAGE_TEXT","Seite Text");
INSERT INTO phpn_vocabulary VALUES("2457","en","_PAGE_TEXT","Page text");
INSERT INTO phpn_vocabulary VALUES("2458","es","_PAGE_TEXT","Las páginas de texto");
INSERT INTO phpn_vocabulary VALUES("2459","de","_PAGE_TITLE","Seitentitel");
INSERT INTO phpn_vocabulary VALUES("2460","en","_PAGE_TITLE","Page Title");
INSERT INTO phpn_vocabulary VALUES("2461","es","_PAGE_TITLE","Título de la página");
INSERT INTO phpn_vocabulary VALUES("2462","de","_PAGE_UNKNOWN","Unbekannte Seite!");
INSERT INTO phpn_vocabulary VALUES("2463","en","_PAGE_UNKNOWN","Unknown page!");
INSERT INTO phpn_vocabulary VALUES("2464","es","_PAGE_UNKNOWN","Página desconocido!");
INSERT INTO phpn_vocabulary VALUES("2465","de","_PARAMETER","Parameter");
INSERT INTO phpn_vocabulary VALUES("2466","en","_PARAMETER","Parameter");
INSERT INTO phpn_vocabulary VALUES("2467","es","_PARAMETER","Parámetro");
INSERT INTO phpn_vocabulary VALUES("2468","de","_PARTIALLY_AVAILABLE","Teilweise verfügbar");
INSERT INTO phpn_vocabulary VALUES("2469","en","_PARTIALLY_AVAILABLE","Partially Available");
INSERT INTO phpn_vocabulary VALUES("2470","es","_PARTIALLY_AVAILABLE","Parcialmente disponible");
INSERT INTO phpn_vocabulary VALUES("2471","de","_PARTIAL_PRICE","Teilweise Preis");
INSERT INTO phpn_vocabulary VALUES("2472","en","_PARTIAL_PRICE","Partial Price");
INSERT INTO phpn_vocabulary VALUES("2473","es","_PARTIAL_PRICE","Precio parcial");
INSERT INTO phpn_vocabulary VALUES("2474","de","_PASSWORD","Kennwort");
INSERT INTO phpn_vocabulary VALUES("2475","en","_PASSWORD","Password");
INSERT INTO phpn_vocabulary VALUES("2476","es","_PASSWORD","Contraseña");
INSERT INTO phpn_vocabulary VALUES("2477","de","_PASSWORD_ALREADY_SENT","Passwort wurde bereits auf Ihre E-Mail geschickt. Bitte versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("2478","en","_PASSWORD_ALREADY_SENT","Password has been already sent to your email. Please try again later.");
INSERT INTO phpn_vocabulary VALUES("2479","es","_PASSWORD_ALREADY_SENT","Contraseña era ya ha sido enviada a su correo electrónico. Por favor, inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("2480","de","_PASSWORD_CHANGED","Passwort wurde geändert.");
INSERT INTO phpn_vocabulary VALUES("2481","en","_PASSWORD_CHANGED","Password has been changed.");
INSERT INTO phpn_vocabulary VALUES("2482","es","_PASSWORD_CHANGED","Contraseña ha cambiado.");
INSERT INTO phpn_vocabulary VALUES("2483","de","_PASSWORD_DO_NOT_MATCH","Kennwort und Bestätigung stimmen nicht überein!");
INSERT INTO phpn_vocabulary VALUES("2484","en","_PASSWORD_DO_NOT_MATCH","Password and confirmation do not match!");
INSERT INTO phpn_vocabulary VALUES("2485","es","_PASSWORD_DO_NOT_MATCH","Contraseña y la confirmación no coinciden!");
INSERT INTO phpn_vocabulary VALUES("2486","de","_PASSWORD_FORGOTTEN","Passwort vergessen?");
INSERT INTO phpn_vocabulary VALUES("2487","en","_PASSWORD_FORGOTTEN","Forgotten Password");
INSERT INTO phpn_vocabulary VALUES("2488","es","_PASSWORD_FORGOTTEN","He olvidado la contraseña");
INSERT INTO phpn_vocabulary VALUES("2489","de","_PASSWORD_FORGOTTEN_PAGE_MSG","Verwenden Sie eine gültige E-Mail-Administrator, Ihr Passwort für den Administrator Back-End-Wiederherstellung. <br><br> Zurück zur Seite <a href=\'index.php\'>Home Page</a> <br><br><img align=\'center\' src=\'images/password.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("2490","en","_PASSWORD_FORGOTTEN_PAGE_MSG","Use a valid administrator e-mail to restore your password to the Administrator Back-End.<br><br>Return to site <a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/password.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("2491","es","_PASSWORD_FORGOTTEN_PAGE_MSG","Utilice un administrador de correo electrónico válida para restablecer su contraseña en el Administrador de servicios de fondo. <br> Volver a la página<a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/password.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("2492","de","_PASSWORD_IS_EMPTY","Passwörter dürfen nicht leer sein und mindestens sechs (6) Zeichen lang sein!");
INSERT INTO phpn_vocabulary VALUES("2493","en","_PASSWORD_IS_EMPTY","Passwords must not be empty and at least 6 characters!");
INSERT INTO phpn_vocabulary VALUES("2494","es","_PASSWORD_IS_EMPTY","Las contraseñas no deben estar vacías y al menos 6 caracteres!");
INSERT INTO phpn_vocabulary VALUES("2495","de","_PASSWORD_NOT_CHANGED","Passwort wurde nicht geändert. Bitte versuchen Sie es erneut!");
INSERT INTO phpn_vocabulary VALUES("2496","en","_PASSWORD_NOT_CHANGED","Password has not been changed. Please try again!");
INSERT INTO phpn_vocabulary VALUES("2497","es","_PASSWORD_NOT_CHANGED","La contraseña no ha cambiado. Por favor, inténtelo de nuevo!");
INSERT INTO phpn_vocabulary VALUES("2498","de","_PASSWORD_RECOVERY_MSG","Um Ihr Kennwort ein");
INSERT INTO phpn_vocabulary VALUES("2499","en","_PASSWORD_RECOVERY_MSG","To recover your password, please enter your e-mail address and a link will be emailed to you.");
INSERT INTO phpn_vocabulary VALUES("2500","es","_PASSWORD_RECOVERY_MSG","Para recuperar su contraseña, por favor, introduzca su dirección de correo electrónico y un enlace será enviada a usted.");
INSERT INTO phpn_vocabulary VALUES("2501","de","_PASSWORD_SUCCESSFULLY_SENT","Ihr Passwort wurde erfolgreich an die Email-Adresse gesendet.");
INSERT INTO phpn_vocabulary VALUES("2502","en","_PASSWORD_SUCCESSFULLY_SENT","Your password has been successfully sent to the email address.");
INSERT INTO phpn_vocabulary VALUES("2503","es","_PASSWORD_SUCCESSFULLY_SENT","Su contraseña ha sido enviado con éxito a la dirección de correo electrónico.");
INSERT INTO phpn_vocabulary VALUES("2504","de","_PAST_TIME_ALERT","Sie können keine Reservierung in der Vergangenheit! Bitte erneut eingeben Termine.");
INSERT INTO phpn_vocabulary VALUES("2505","en","_PAST_TIME_ALERT","You cannot perform reservation in the past! Please re-enter dates.");
INSERT INTO phpn_vocabulary VALUES("2506","es","_PAST_TIME_ALERT","No es posible realizar reservas en el pasado! Por favor, vuelva a entrar en fechas.");
INSERT INTO phpn_vocabulary VALUES("2507","de","_PAYED_BY","Bezahlt durch");
INSERT INTO phpn_vocabulary VALUES("2508","en","_PAYED_BY","Payed by");
INSERT INTO phpn_vocabulary VALUES("2509","es","_PAYED_BY","Pagado por");
INSERT INTO phpn_vocabulary VALUES("2510","de","_PAYMENT","Zahlung");
INSERT INTO phpn_vocabulary VALUES("2511","en","_PAYMENT","Payment");
INSERT INTO phpn_vocabulary VALUES("2512","es","_PAYMENT","Pago");
INSERT INTO phpn_vocabulary VALUES("2513","de","_PAYMENTS","Zahlungen");
INSERT INTO phpn_vocabulary VALUES("2514","en","_PAYMENTS","Payments");
INSERT INTO phpn_vocabulary VALUES("2515","es","_PAYMENTS","Pagos");
INSERT INTO phpn_vocabulary VALUES("2516","de","_PAYMENT_COMPANY_ACCOUNT","Zahlung Firmenkonto");
INSERT INTO phpn_vocabulary VALUES("2517","en","_PAYMENT_COMPANY_ACCOUNT","Payment Company Account");
INSERT INTO phpn_vocabulary VALUES("2518","es","_PAYMENT_COMPANY_ACCOUNT","Pago de cuenta de la compañía");
INSERT INTO phpn_vocabulary VALUES("2519","de","_PAYMENT_DATE","Zahltag");
INSERT INTO phpn_vocabulary VALUES("2520","en","_PAYMENT_DATE","Payment Date");
INSERT INTO phpn_vocabulary VALUES("2521","es","_PAYMENT_DATE","Fecha de Pago");
INSERT INTO phpn_vocabulary VALUES("2522","de","_PAYMENT_DETAILS","Einzelheiten zur Bezahlung");
INSERT INTO phpn_vocabulary VALUES("2523","en","_PAYMENT_DETAILS","Payment Details");
INSERT INTO phpn_vocabulary VALUES("2524","es","_PAYMENT_DETAILS","Detalles de pago");
INSERT INTO phpn_vocabulary VALUES("2525","de","_PAYMENT_ERROR","Zahlung Fehler");
INSERT INTO phpn_vocabulary VALUES("2526","en","_PAYMENT_ERROR","Payment error");
INSERT INTO phpn_vocabulary VALUES("2527","es","_PAYMENT_ERROR","El pago de error");
INSERT INTO phpn_vocabulary VALUES("2528","de","_PAYMENT_METHOD","Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("2529","en","_PAYMENT_METHOD","Payment Method");
INSERT INTO phpn_vocabulary VALUES("2530","es","_PAYMENT_METHOD","Forma de pago");
INSERT INTO phpn_vocabulary VALUES("2531","de","_PAYMENT_METHODS","Zahlungsmethoden");
INSERT INTO phpn_vocabulary VALUES("2532","en","_PAYMENT_METHODS","Payment Methods");
INSERT INTO phpn_vocabulary VALUES("2533","es","_PAYMENT_METHODS","Formas de pago");
INSERT INTO phpn_vocabulary VALUES("2534","de","_PAYMENT_REQUIRED","Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("2535","en","_PAYMENT_REQUIRED","Payment Required");
INSERT INTO phpn_vocabulary VALUES("2536","es","_PAYMENT_REQUIRED","Se requiere el pago");
INSERT INTO phpn_vocabulary VALUES("2537","de","_PAYMENT_SUM","Die Zahlung Sum");
INSERT INTO phpn_vocabulary VALUES("2538","en","_PAYMENT_SUM","Payment Sum");
INSERT INTO phpn_vocabulary VALUES("2539","es","_PAYMENT_SUM","Pago de Suma");
INSERT INTO phpn_vocabulary VALUES("2540","de","_PAYMENT_TYPE","Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("2541","en","_PAYMENT_TYPE","Payment Type");
INSERT INTO phpn_vocabulary VALUES("2542","es","_PAYMENT_TYPE","Tipo de Pago");
INSERT INTO phpn_vocabulary VALUES("2543","de","_PAYPAL","PayPal");
INSERT INTO phpn_vocabulary VALUES("2544","en","_PAYPAL","PayPal");
INSERT INTO phpn_vocabulary VALUES("2545","es","_PAYPAL","PayPal");
INSERT INTO phpn_vocabulary VALUES("2546","de","_PAYPAL_NOTICE","Sparen Sie Zeit. Sicher bezahlen mit Ihrer gespeicherten Zahlungsinformationen. <br />Bezahlen mit <b>Kreditkarte, Bankkonto</b> oder <b>PayPal-Guthaben.</b>");
INSERT INTO phpn_vocabulary VALUES("2547","en","_PAYPAL_NOTICE","Save time. Pay securely using your stored payment information.<br />Pay with <b>credit card</b>, <b>bank account</b> or <b>PayPal</b> account balance.");
INSERT INTO phpn_vocabulary VALUES("2548","es","_PAYPAL_NOTICE","Ahorre tiempo. Pague con seguridad con su información almacenada pago.<br /> Pago con <b>tarjeta de crédito</b>, <b> cuenta bancaria</b> o <b>PayPal</b> saldo de la cuenta.");
INSERT INTO phpn_vocabulary VALUES("2549","de","_PAYPAL_ORDER","PayPal Auftrag");
INSERT INTO phpn_vocabulary VALUES("2550","en","_PAYPAL_ORDER","PayPal Order");
INSERT INTO phpn_vocabulary VALUES("2551","es","_PAYPAL_ORDER","PayPal Orden");
INSERT INTO phpn_vocabulary VALUES("2552","de","_PAY_ON_ARRIVAL","Zahlung bei Ankunft");
INSERT INTO phpn_vocabulary VALUES("2553","en","_PAY_ON_ARRIVAL","Pay on Arrival");
INSERT INTO phpn_vocabulary VALUES("2554","es","_PAY_ON_ARRIVAL","Pagar a la llegada");
INSERT INTO phpn_vocabulary VALUES("2555","de","_PC_BILLING_INFORMATION_TEXT","Zahlungsinformationen: Adresse, Stadt, Land usw.");
INSERT INTO phpn_vocabulary VALUES("2556","en","_PC_BILLING_INFORMATION_TEXT","billing information: address, city, country etc.");
INSERT INTO phpn_vocabulary VALUES("2557","es","_PC_BILLING_INFORMATION_TEXT","información de facturación: dirección, ciudad, país, etc");
INSERT INTO phpn_vocabulary VALUES("2558","de","_PC_BOOKING_DETAILS_TEXT","Um Details, Liste der gekauften Produkte usw.");
INSERT INTO phpn_vocabulary VALUES("2559","en","_PC_BOOKING_DETAILS_TEXT","order details, list of purchased products etc.");
INSERT INTO phpn_vocabulary VALUES("2560","es","_PC_BOOKING_DETAILS_TEXT","detalles de la orden, la lista de los productos adquiridos, etc");
INSERT INTO phpn_vocabulary VALUES("2561","de","_PC_BOOKING_NUMBER_TEXT","die Zahl der Bestellung");
INSERT INTO phpn_vocabulary VALUES("2562","en","_PC_BOOKING_NUMBER_TEXT","the number of order");
INSERT INTO phpn_vocabulary VALUES("2563","es","_PC_BOOKING_NUMBER_TEXT","el número de orden");
INSERT INTO phpn_vocabulary VALUES("2564","de","_PC_EVENT_TEXT","der Titel der Veranstaltung");
INSERT INTO phpn_vocabulary VALUES("2565","en","_PC_EVENT_TEXT","the title of event");
INSERT INTO phpn_vocabulary VALUES("2566","es","_PC_EVENT_TEXT","el título del evento");
INSERT INTO phpn_vocabulary VALUES("2567","de","_PC_FIRST_NAME_TEXT","Der erste Name, der Kunde oder Admin");
INSERT INTO phpn_vocabulary VALUES("2568","en","_PC_FIRST_NAME_TEXT","the first name of customer or admin");
INSERT INTO phpn_vocabulary VALUES("2569","es","_PC_FIRST_NAME_TEXT","el nombre del cliente o administrador");
INSERT INTO phpn_vocabulary VALUES("2570","de","_PC_HOTEL_INFO_TEXT","Informationen über das Hotel: Name, Adresse, Telefon, Fax usw.");
INSERT INTO phpn_vocabulary VALUES("2571","en","_PC_HOTEL_INFO_TEXT","information about hotel: name, address, telephone, fax etc.");
INSERT INTO phpn_vocabulary VALUES("2572","es","_PC_HOTEL_INFO_TEXT","Información sobre el hotel: nombre, dirección, teléfono, fax, etc");
INSERT INTO phpn_vocabulary VALUES("2573","de","_PC_LAST_NAME_TEXT","der Nachname des Kunden oder Admin");
INSERT INTO phpn_vocabulary VALUES("2574","en","_PC_LAST_NAME_TEXT","the last name of customer or admin");
INSERT INTO phpn_vocabulary VALUES("2575","es","_PC_LAST_NAME_TEXT","el apellido del cliente o administrador");
INSERT INTO phpn_vocabulary VALUES("2576","de","_PC_PERSONAL_INFORMATION_TEXT","persönliche Daten des Kunden: Vorname, Nachname usw.");
INSERT INTO phpn_vocabulary VALUES("2577","en","_PC_PERSONAL_INFORMATION_TEXT","personal information of customer: first name, last name etc.");
INSERT INTO phpn_vocabulary VALUES("2578","es","_PC_PERSONAL_INFORMATION_TEXT","información personal del cliente: nombre, apellidos, etc");
INSERT INTO phpn_vocabulary VALUES("2579","de","_PC_REGISTRATION_CODE_TEXT","Bestätigungs-Code für neues Konto");
INSERT INTO phpn_vocabulary VALUES("2580","en","_PC_REGISTRATION_CODE_TEXT","confirmation code for new account");
INSERT INTO phpn_vocabulary VALUES("2581","es","_PC_REGISTRATION_CODE_TEXT","código de confirmación para la nueva cuenta");
INSERT INTO phpn_vocabulary VALUES("2582","de","_PC_STATUS_DESCRIPTION_TEXT","beschreibung der Zahlungsstatus");
INSERT INTO phpn_vocabulary VALUES("2583","en","_PC_STATUS_DESCRIPTION_TEXT","description of payment status");
INSERT INTO phpn_vocabulary VALUES("2584","es","_PC_STATUS_DESCRIPTION_TEXT","descripción del estado de pago");
INSERT INTO phpn_vocabulary VALUES("2585","de","_PC_USER_EMAIL_TEXT","E-Mail an Benutzer");
INSERT INTO phpn_vocabulary VALUES("2586","en","_PC_USER_EMAIL_TEXT","email of user");
INSERT INTO phpn_vocabulary VALUES("2587","es","_PC_USER_EMAIL_TEXT","correo electrónico del usuario");
INSERT INTO phpn_vocabulary VALUES("2588","de","_PC_USER_NAME_TEXT","Benutzername (Login) des Benutzers");
INSERT INTO phpn_vocabulary VALUES("2589","en","_PC_USER_NAME_TEXT","username (login) of user");
INSERT INTO phpn_vocabulary VALUES("2590","es","_PC_USER_NAME_TEXT","nombre de usuario (login) del usuario");
INSERT INTO phpn_vocabulary VALUES("2591","de","_PC_USER_PASSWORD_TEXT","Passwort für den Kunden oder Admin");
INSERT INTO phpn_vocabulary VALUES("2592","en","_PC_USER_PASSWORD_TEXT","password for customer or admin");
INSERT INTO phpn_vocabulary VALUES("2593","es","_PC_USER_PASSWORD_TEXT","contraseña para el cliente o administrador");
INSERT INTO phpn_vocabulary VALUES("2594","de","_PC_WEB_SITE_BASED_URL_TEXT","Website Basis-URL");
INSERT INTO phpn_vocabulary VALUES("2595","en","_PC_WEB_SITE_BASED_URL_TEXT","web site base url");
INSERT INTO phpn_vocabulary VALUES("2596","es","_PC_WEB_SITE_BASED_URL_TEXT","sitio web, base de url");
INSERT INTO phpn_vocabulary VALUES("2597","de","_PC_WEB_SITE_URL_TEXT","Website eintragen");
INSERT INTO phpn_vocabulary VALUES("2598","en","_PC_WEB_SITE_URL_TEXT","web site url");
INSERT INTO phpn_vocabulary VALUES("2599","es","_PC_WEB_SITE_URL_TEXT","URL del sitio web");
INSERT INTO phpn_vocabulary VALUES("2600","de","_PC_YEAR_TEXT","laufenden Jahr im Format YYYY");
INSERT INTO phpn_vocabulary VALUES("2601","en","_PC_YEAR_TEXT","current year in YYYY format");
INSERT INTO phpn_vocabulary VALUES("2602","es","_PC_YEAR_TEXT","año en curso en formato AAAA");
INSERT INTO phpn_vocabulary VALUES("2603","de","_PENDING","anhängig");
INSERT INTO phpn_vocabulary VALUES("2604","en","_PENDING","Pending");
INSERT INTO phpn_vocabulary VALUES("2605","es","_PENDING","Pendiente");
INSERT INTO phpn_vocabulary VALUES("2606","de","_PEOPLE_ARRIVING","Menschen angekommen");
INSERT INTO phpn_vocabulary VALUES("2607","en","_PEOPLE_ARRIVING","People Arriving");
INSERT INTO phpn_vocabulary VALUES("2608","es","_PEOPLE_ARRIVING","Las personas que llegan");
INSERT INTO phpn_vocabulary VALUES("2609","de","_PEOPLE_DEPARTING","Menschen Abfahrt");
INSERT INTO phpn_vocabulary VALUES("2610","en","_PEOPLE_DEPARTING","People Departing");
INSERT INTO phpn_vocabulary VALUES("2611","es","_PEOPLE_DEPARTING","La gente de Salida");
INSERT INTO phpn_vocabulary VALUES("2612","de","_PEOPLE_STAYING","Menschen bleiben");
INSERT INTO phpn_vocabulary VALUES("2613","en","_PEOPLE_STAYING","People Staying");
INSERT INTO phpn_vocabulary VALUES("2614","es","_PEOPLE_STAYING","Personas que se quedan");
INSERT INTO phpn_vocabulary VALUES("2615","de","_PERCENTAGE_MAX_ALOWED_VALUE","Die maximal zulässige Wert für Anteil beträgt 99%! Bitte geben Sie erneut.");
INSERT INTO phpn_vocabulary VALUES("2616","en","_PERCENTAGE_MAX_ALOWED_VALUE","The maximum allowed value for percentage is 99%! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2617","es","_PERCENTAGE_MAX_ALOWED_VALUE","El valor máximo permitido para el porcentaje es del 99%! Vuelva a inscribir.");
INSERT INTO phpn_vocabulary VALUES("2618","de","_PERFORM_OPERATION_COMMON_ALERT","Sie sind Sicher");
INSERT INTO phpn_vocabulary VALUES("2619","en","_PERFORM_OPERATION_COMMON_ALERT","Are you sure you want to perform this operation?");
INSERT INTO phpn_vocabulary VALUES("2620","es","_PERFORM_OPERATION_COMMON_ALERT","¿Está seguro que desea llevar a cabo esta operación?");
INSERT INTO phpn_vocabulary VALUES("2621","de","_PERIODS","Spielzeiten");
INSERT INTO phpn_vocabulary VALUES("2622","en","_PERIODS","Periods");
INSERT INTO phpn_vocabulary VALUES("2623","es","_PERIODS","Períodos");
INSERT INTO phpn_vocabulary VALUES("2624","de","_PERSONAL_DATA_SAVED","Ihre Zahlungsinformationen aktualisiert wurde.");
INSERT INTO phpn_vocabulary VALUES("2625","en","_PERSONAL_DATA_SAVED","Your billing information has been updated.");
INSERT INTO phpn_vocabulary VALUES("2626","es","_PERSONAL_DATA_SAVED","Sus datos de facturación se ha actualizado.");
INSERT INTO phpn_vocabulary VALUES("2627","de","_PERSONAL_DETAILS","Persönliche Daten");
INSERT INTO phpn_vocabulary VALUES("2628","en","_PERSONAL_DETAILS","Personal Details");
INSERT INTO phpn_vocabulary VALUES("2629","es","_PERSONAL_DETAILS","Datos personales");
INSERT INTO phpn_vocabulary VALUES("2630","de","_PERSONAL_INFORMATION","Persönliche Informationen");
INSERT INTO phpn_vocabulary VALUES("2631","en","_PERSONAL_INFORMATION","Personal Information");
INSERT INTO phpn_vocabulary VALUES("2632","es","_PERSONAL_INFORMATION","Información Personal");
INSERT INTO phpn_vocabulary VALUES("2633","de","_PERSON_PER_NIGHT","Person/pro Nacht");
INSERT INTO phpn_vocabulary VALUES("2634","en","_PERSON_PER_NIGHT","Person/Per Night");
INSERT INTO phpn_vocabulary VALUES("2635","es","_PERSON_PER_NIGHT","Persona/Por noche");
INSERT INTO phpn_vocabulary VALUES("2636","de","_PER_NIGHT","Pro Nacht");
INSERT INTO phpn_vocabulary VALUES("2637","en","_PER_NIGHT","Per Night");
INSERT INTO phpn_vocabulary VALUES("2638","es","_PER_NIGHT","Por Noche");
INSERT INTO phpn_vocabulary VALUES("2639","de","_PHONE","Telefon");
INSERT INTO phpn_vocabulary VALUES("2640","en","_PHONE","Phone");
INSERT INTO phpn_vocabulary VALUES("2641","es","_PHONE","Teléfono");
INSERT INTO phpn_vocabulary VALUES("2642","de","_PHONE_EMPTY_ALERT","Telefon Feld darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("2643","en","_PHONE_EMPTY_ALERT","Phone field cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2644","es","_PHONE_EMPTY_ALERT","Campo de teléfono no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("2645","de","_PICK_DATE","Kalender öffnen und ein Datum wählen");
INSERT INTO phpn_vocabulary VALUES("2646","en","_PICK_DATE","Open calendar and pick a date");
INSERT INTO phpn_vocabulary VALUES("2647","es","_PICK_DATE","Abrir el calendario y elegir una fecha");
INSERT INTO phpn_vocabulary VALUES("2648","de","_PLACEMENT","Platzierung");
INSERT INTO phpn_vocabulary VALUES("2649","en","_PLACEMENT","Placement");
INSERT INTO phpn_vocabulary VALUES("2650","es","_PLACEMENT","Colocación");
INSERT INTO phpn_vocabulary VALUES("2651","de","_PLACE_ORDER","Bestellung");
INSERT INTO phpn_vocabulary VALUES("2652","en","_PLACE_ORDER","Place Order");
INSERT INTO phpn_vocabulary VALUES("2653","es","_PLACE_ORDER","Realizar pedido");
INSERT INTO phpn_vocabulary VALUES("2654","de","_PLAY","Spielen");
INSERT INTO phpn_vocabulary VALUES("2655","en","_PLAY","Play");
INSERT INTO phpn_vocabulary VALUES("2656","es","_PLAY","Jugar");
INSERT INTO phpn_vocabulary VALUES("2657","de","_POPULARITY","Beliebtheit");
INSERT INTO phpn_vocabulary VALUES("2658","en","_POPULARITY","Popularity");
INSERT INTO phpn_vocabulary VALUES("2659","es","_POPULARITY","Popularidad");
INSERT INTO phpn_vocabulary VALUES("2660","de","_POPULAR_SEARCH","Beliebte Suchen");
INSERT INTO phpn_vocabulary VALUES("2661","en","_POPULAR_SEARCH","Popular Search");
INSERT INTO phpn_vocabulary VALUES("2662","es","_POPULAR_SEARCH","Popular Buscar");
INSERT INTO phpn_vocabulary VALUES("2663","de","_POSTED_ON","Gesendet am");
INSERT INTO phpn_vocabulary VALUES("2664","en","_POSTED_ON","Posted on");
INSERT INTO phpn_vocabulary VALUES("2665","es","_POSTED_ON","Publicado en");
INSERT INTO phpn_vocabulary VALUES("2666","de","_POST_COM_REGISTERED_ALERT","Ihr müssen registriert sein, um Kommentare verfassen zu können.");
INSERT INTO phpn_vocabulary VALUES("2667","en","_POST_COM_REGISTERED_ALERT","Your need to be registered to post comments.");
INSERT INTO phpn_vocabulary VALUES("2668","es","_POST_COM_REGISTERED_ALERT","Su necesidad de estar registrado para enviar comentarios.");
INSERT INTO phpn_vocabulary VALUES("2669","de","_PREBOOKING","Voranmeldung");
INSERT INTO phpn_vocabulary VALUES("2670","en","_PREBOOKING","Pre-Booking");
INSERT INTO phpn_vocabulary VALUES("2671","es","_PREBOOKING","Pre-Reserva");
INSERT INTO phpn_vocabulary VALUES("2672","de","_PREDEFINED_CONSTANTS","Vordefinierte Konstanten");
INSERT INTO phpn_vocabulary VALUES("2673","en","_PREDEFINED_CONSTANTS","Predefined Constants");
INSERT INTO phpn_vocabulary VALUES("2674","es","_PREDEFINED_CONSTANTS","Constantes predefinidas");
INSERT INTO phpn_vocabulary VALUES("2675","de","_PREFERRED_LANGUAGE","Bevorzugte Sprache");
INSERT INTO phpn_vocabulary VALUES("2676","en","_PREFERRED_LANGUAGE","Preferred Language");
INSERT INTO phpn_vocabulary VALUES("2677","es","_PREFERRED_LANGUAGE","Idioma de preferencia");
INSERT INTO phpn_vocabulary VALUES("2678","de","_PREVIEW","Vorschau");
INSERT INTO phpn_vocabulary VALUES("2679","en","_PREVIEW","Preview");
INSERT INTO phpn_vocabulary VALUES("2680","es","_PREVIEW","Prevista");
INSERT INTO phpn_vocabulary VALUES("2681","de","_PREVIOUS","vorhergehenden");
INSERT INTO phpn_vocabulary VALUES("2682","en","_PREVIOUS","Previous");
INSERT INTO phpn_vocabulary VALUES("2683","es","_PREVIOUS","Anterior");
INSERT INTO phpn_vocabulary VALUES("2684","de","_PRE_PAYMENT","Vorauszahlung");
INSERT INTO phpn_vocabulary VALUES("2685","en","_PRE_PAYMENT","Pre-Payment");
INSERT INTO phpn_vocabulary VALUES("2686","es","_PRE_PAYMENT","Pre-Pago");
INSERT INTO phpn_vocabulary VALUES("2687","de","_PRICE","Preis");
INSERT INTO phpn_vocabulary VALUES("2688","en","_PRICE","Price");
INSERT INTO phpn_vocabulary VALUES("2689","es","_PRICE","Precio");
INSERT INTO phpn_vocabulary VALUES("2690","de","_PRICES","Preise");
INSERT INTO phpn_vocabulary VALUES("2691","en","_PRICES","Prices");
INSERT INTO phpn_vocabulary VALUES("2692","es","_PRICES","Precios");
INSERT INTO phpn_vocabulary VALUES("2693","de","_PRICE_EMPTY_ALERT","Field Preis darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("2694","en","_PRICE_EMPTY_ALERT","Field price cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2695","es","_PRICE_EMPTY_ALERT","Precio de campo no puede estar vacío! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("2696","de","_PRICE_FORMAT","Preis Format");
INSERT INTO phpn_vocabulary VALUES("2697","en","_PRICE_FORMAT","Price Format");
INSERT INTO phpn_vocabulary VALUES("2698","es","_PRICE_FORMAT","Formato Precio");
INSERT INTO phpn_vocabulary VALUES("2699","de","_PRICE_FORMAT_ALERT","Ermöglicht die Preise für Besucher in geeigneter Form angezeigt werden");
INSERT INTO phpn_vocabulary VALUES("2700","en","_PRICE_FORMAT_ALERT","Allows to display prices for visitor in appropriate format");
INSERT INTO phpn_vocabulary VALUES("2701","es","_PRICE_FORMAT_ALERT","Permite mostrar los precios de los visitantes en el formato adecuado");
INSERT INTO phpn_vocabulary VALUES("2702","de","_PRICE_H_L","Preis (höchste)");
INSERT INTO phpn_vocabulary VALUES("2703","en","_PRICE_H_L","price (from highest)");
INSERT INTO phpn_vocabulary VALUES("2704","es","_PRICE_H_L","precio (de alto)");
INSERT INTO phpn_vocabulary VALUES("2705","de","_PRICE_L_H","Preis (von unten)");
INSERT INTO phpn_vocabulary VALUES("2706","en","_PRICE_L_H","price (from lowest)");
INSERT INTO phpn_vocabulary VALUES("2707","es","_PRICE_L_H","precio (más bajo)");
INSERT INTO phpn_vocabulary VALUES("2708","de","_PRINT","drucken");
INSERT INTO phpn_vocabulary VALUES("2709","en","_PRINT","Print");
INSERT INTO phpn_vocabulary VALUES("2710","es","_PRINT","Imprimir");
INSERT INTO phpn_vocabulary VALUES("2711","de","_PRIVILEGES","Vorrechte");
INSERT INTO phpn_vocabulary VALUES("2712","en","_PRIVILEGES","Privileges");
INSERT INTO phpn_vocabulary VALUES("2713","es","_PRIVILEGES","Privilegios");
INSERT INTO phpn_vocabulary VALUES("2714","de","_PRIVILEGES_MANAGEMENT","Privileges-Management");
INSERT INTO phpn_vocabulary VALUES("2715","en","_PRIVILEGES_MANAGEMENT","Privileges Management");
INSERT INTO phpn_vocabulary VALUES("2716","es","_PRIVILEGES_MANAGEMENT","Privilegios de administración");
INSERT INTO phpn_vocabulary VALUES("2717","de","_PRODUCT","Produkt");
INSERT INTO phpn_vocabulary VALUES("2718","en","_PRODUCT","Product");
INSERT INTO phpn_vocabulary VALUES("2719","es","_PRODUCT","Producto");
INSERT INTO phpn_vocabulary VALUES("2720","de","_PRODUCTS","Produkte");
INSERT INTO phpn_vocabulary VALUES("2721","en","_PRODUCTS","Products");
INSERT INTO phpn_vocabulary VALUES("2722","es","_PRODUCTS","Productos");
INSERT INTO phpn_vocabulary VALUES("2723","de","_PRODUCTS_COUNT","Produkte zählen");
INSERT INTO phpn_vocabulary VALUES("2724","en","_PRODUCTS_COUNT","Products count");
INSERT INTO phpn_vocabulary VALUES("2725","es","_PRODUCTS_COUNT","Productos cuentan");
INSERT INTO phpn_vocabulary VALUES("2726","de","_PRODUCTS_MANAGEMENT","Produkte Management");
INSERT INTO phpn_vocabulary VALUES("2727","en","_PRODUCTS_MANAGEMENT","Products Management");
INSERT INTO phpn_vocabulary VALUES("2728","es","_PRODUCTS_MANAGEMENT","Productos para la administración");
INSERT INTO phpn_vocabulary VALUES("2729","de","_PRODUCT_DESCRIPTION","Produkt-Beschreibung");
INSERT INTO phpn_vocabulary VALUES("2730","en","_PRODUCT_DESCRIPTION","Product Description");
INSERT INTO phpn_vocabulary VALUES("2731","es","_PRODUCT_DESCRIPTION","Descripción del producto");
INSERT INTO phpn_vocabulary VALUES("2732","de","_PROMO_AND_DISCOUNTS","Promo und Rabatte");
INSERT INTO phpn_vocabulary VALUES("2733","en","_PROMO_AND_DISCOUNTS","Promo and Discounts");
INSERT INTO phpn_vocabulary VALUES("2734","es","_PROMO_AND_DISCOUNTS","Promociones y Descuentos");
INSERT INTO phpn_vocabulary VALUES("2735","de","_PROMO_CODE_OR_COUPON","Promo Code oder Rabatt-Coupon");
INSERT INTO phpn_vocabulary VALUES("2736","en","_PROMO_CODE_OR_COUPON","Promo Code or Discount Coupon");
INSERT INTO phpn_vocabulary VALUES("2737","es","_PROMO_CODE_OR_COUPON","Código de promoción o cupón de descuento");
INSERT INTO phpn_vocabulary VALUES("2738","de","_PROMO_COUPON_NOTICE","Wenn Sie einen Promo-Code oder Rabatt-Coupon haben, bitte geben Sie ihn hier");
INSERT INTO phpn_vocabulary VALUES("2739","en","_PROMO_COUPON_NOTICE","If you have a promo code or discount coupon please enter it here");
INSERT INTO phpn_vocabulary VALUES("2740","es","_PROMO_COUPON_NOTICE","Si usted tiene un código promocional o cupón de descuento por favor ingrese aquí");
INSERT INTO phpn_vocabulary VALUES("2741","de","_PUBLIC","Öffentliche");
INSERT INTO phpn_vocabulary VALUES("2742","en","_PUBLIC","Public");
INSERT INTO phpn_vocabulary VALUES("2743","es","_PUBLIC","Pública");
INSERT INTO phpn_vocabulary VALUES("2744","de","_PUBLISHED","Veröffentlicht");
INSERT INTO phpn_vocabulary VALUES("2745","en","_PUBLISHED","Published");
INSERT INTO phpn_vocabulary VALUES("2746","es","_PUBLISHED","Publicado");
INSERT INTO phpn_vocabulary VALUES("2747","de","_PUBLISH_YOUR_COMMENT","Veröffentlichen she Ihren Kommentar");
INSERT INTO phpn_vocabulary VALUES("2748","en","_PUBLISH_YOUR_COMMENT","Publish your comment");
INSERT INTO phpn_vocabulary VALUES("2749","es","_PUBLISH_YOUR_COMMENT","Publicar tu comentario");
INSERT INTO phpn_vocabulary VALUES("2750","de","_QTY","Anz");
INSERT INTO phpn_vocabulary VALUES("2751","en","_QTY","Qty");
INSERT INTO phpn_vocabulary VALUES("2752","es","_QTY","Cantidad");
INSERT INTO phpn_vocabulary VALUES("2753","de","_QUANTITY","Menge");
INSERT INTO phpn_vocabulary VALUES("2754","en","_QUANTITY","Quantity");
INSERT INTO phpn_vocabulary VALUES("2755","es","_QUANTITY","Cantidad");
INSERT INTO phpn_vocabulary VALUES("2756","de","_QUESTION","Frage");
INSERT INTO phpn_vocabulary VALUES("2757","en","_QUESTION","Question");
INSERT INTO phpn_vocabulary VALUES("2758","es","_QUESTION","Pregunta");
INSERT INTO phpn_vocabulary VALUES("2759","de","_QUESTIONS","Fragen");
INSERT INTO phpn_vocabulary VALUES("2760","en","_QUESTIONS","Questions");
INSERT INTO phpn_vocabulary VALUES("2761","es","_QUESTIONS","Preguntas");
INSERT INTO phpn_vocabulary VALUES("2762","de","_RATE","Rate");
INSERT INTO phpn_vocabulary VALUES("2763","en","_RATE","Rate");
INSERT INTO phpn_vocabulary VALUES("2764","es","_RATE","Tipo");
INSERT INTO phpn_vocabulary VALUES("2765","de","_RATE_PER_NIGHT","Preis pro Nacht");
INSERT INTO phpn_vocabulary VALUES("2766","en","_RATE_PER_NIGHT","Rate per night");
INSERT INTO phpn_vocabulary VALUES("2767","es","_RATE_PER_NIGHT","Precio por noche");
INSERT INTO phpn_vocabulary VALUES("2768","de","_RATE_PER_NIGHT_AVG","Durchschnittspreis pro Nacht");
INSERT INTO phpn_vocabulary VALUES("2769","en","_RATE_PER_NIGHT_AVG","Average rate per night");
INSERT INTO phpn_vocabulary VALUES("2770","es","_RATE_PER_NIGHT_AVG","Tarifa media por noche");
INSERT INTO phpn_vocabulary VALUES("2771","de","_RATINGS","Bewertungen");
INSERT INTO phpn_vocabulary VALUES("2772","en","_RATINGS","Ratings");
INSERT INTO phpn_vocabulary VALUES("2773","es","_RATINGS","Valoraciones");
INSERT INTO phpn_vocabulary VALUES("2774","de","_RATINGS_SETTINGS","Bewertungen Einstellungen");
INSERT INTO phpn_vocabulary VALUES("2775","en","_RATINGS_SETTINGS","Ratings Settings");
INSERT INTO phpn_vocabulary VALUES("2776","es","_RATINGS_SETTINGS","Valoraciones Configuración");
INSERT INTO phpn_vocabulary VALUES("2777","de","_REACTIVATION_EMAIL","Aktivierungsmail erneut zuschicken");
INSERT INTO phpn_vocabulary VALUES("2778","en","_REACTIVATION_EMAIL","Resend Activation Email");
INSERT INTO phpn_vocabulary VALUES("2779","es","_REACTIVATION_EMAIL","Reenviar email de activación");
INSERT INTO phpn_vocabulary VALUES("2780","de","_READY","bereit");
INSERT INTO phpn_vocabulary VALUES("2781","en","_READY","Ready");
INSERT INTO phpn_vocabulary VALUES("2782","es","_READY","Listo");
INSERT INTO phpn_vocabulary VALUES("2783","de","_READ_MORE","Lesen Sie mehr");
INSERT INTO phpn_vocabulary VALUES("2784","en","_READ_MORE","Read more");
INSERT INTO phpn_vocabulary VALUES("2785","es","_READ_MORE","Leer más");
INSERT INTO phpn_vocabulary VALUES("2786","de","_REAL_TIME_CAMPAIGN","Echtzeit-Kampagne");
INSERT INTO phpn_vocabulary VALUES("2787","en","_REAL_TIME_CAMPAIGN","Real Time Campaign");
INSERT INTO phpn_vocabulary VALUES("2788","es","_REAL_TIME_CAMPAIGN","Campaña Tiempo real");
INSERT INTO phpn_vocabulary VALUES("2789","de","_REASON","Grund");
INSERT INTO phpn_vocabulary VALUES("2790","en","_REASON","Reason");
INSERT INTO phpn_vocabulary VALUES("2791","es","_REASON","Razón");
INSERT INTO phpn_vocabulary VALUES("2792","de","_RECORD_WAS_DELETED_COMMON","Der Datensatz wurde erfolgreich gelöscht!");
INSERT INTO phpn_vocabulary VALUES("2793","en","_RECORD_WAS_DELETED_COMMON","The record has been successfully deleted!");
INSERT INTO phpn_vocabulary VALUES("2794","es","_RECORD_WAS_DELETED_COMMON","El registro ha sido borrado!");
INSERT INTO phpn_vocabulary VALUES("2795","de","_REFRESH","Erfrischen");
INSERT INTO phpn_vocabulary VALUES("2796","en","_REFRESH","Refresh");
INSERT INTO phpn_vocabulary VALUES("2797","es","_REFRESH","Refrescar");
INSERT INTO phpn_vocabulary VALUES("2798","de","_REFUNDED","erstattet");
INSERT INTO phpn_vocabulary VALUES("2799","en","_REFUNDED","Refunded");
INSERT INTO phpn_vocabulary VALUES("2800","es","_REFUNDED","Devuelto");
INSERT INTO phpn_vocabulary VALUES("2801","de","_REGISTERED","registriert");
INSERT INTO phpn_vocabulary VALUES("2802","en","_REGISTERED","Registered");
INSERT INTO phpn_vocabulary VALUES("2803","es","_REGISTERED","Registrado");
INSERT INTO phpn_vocabulary VALUES("2804","de","_REGISTERED_FROM_IP","Einschreiben aus IP");
INSERT INTO phpn_vocabulary VALUES("2805","en","_REGISTERED_FROM_IP","Registered from IP");
INSERT INTO phpn_vocabulary VALUES("2806","es","_REGISTERED_FROM_IP","Registradas a partir de la IP");
INSERT INTO phpn_vocabulary VALUES("2807","de","_REGISTRATIONS","anmeldungen");
INSERT INTO phpn_vocabulary VALUES("2808","en","_REGISTRATIONS","Registrations");
INSERT INTO phpn_vocabulary VALUES("2809","es","_REGISTRATIONS","Inscripciones");
INSERT INTO phpn_vocabulary VALUES("2810","de","_REGISTRATION_CODE","Registrierungs-Code");
INSERT INTO phpn_vocabulary VALUES("2811","en","_REGISTRATION_CODE","Registration code");
INSERT INTO phpn_vocabulary VALUES("2812","es","_REGISTRATION_CODE","Código de registro");
INSERT INTO phpn_vocabulary VALUES("2813","de","_REGISTRATION_CONFIRMATION","Anmeldebestätigung");
INSERT INTO phpn_vocabulary VALUES("2814","en","_REGISTRATION_CONFIRMATION","Registration Confirmation");
INSERT INTO phpn_vocabulary VALUES("2815","es","_REGISTRATION_CONFIRMATION","Confirmación de registro");
INSERT INTO phpn_vocabulary VALUES("2816","de","_REGISTRATION_FORM","Anmeldeformular");
INSERT INTO phpn_vocabulary VALUES("2817","en","_REGISTRATION_FORM","Registration Form");
INSERT INTO phpn_vocabulary VALUES("2818","es","_REGISTRATION_FORM","Formulario de Inscripción");
INSERT INTO phpn_vocabulary VALUES("2819","de","_REGISTRATION_NOT_COMPLETED","Ihre Registrierung ist noch nicht abgeschlossen! Bitte überprüfen Sie nochmals Ihre E-Mail für weitere Anweisungen oder klicken Sie <a href=index.php?customer=resend_activation>hier</a>, um erneut wieder.");
INSERT INTO phpn_vocabulary VALUES("2820","en","_REGISTRATION_NOT_COMPLETED","Your registration process is not yet complete! Please check again your email for further instructions or click <a href=index.php?customer=resend_activation>here</a> to resend them again.");
INSERT INTO phpn_vocabulary VALUES("2821","es","_REGISTRATION_NOT_COMPLETED","El proceso de registro aún no ha terminado! Por favor, compruebe de nuevo su correo electrónico para recibir instrucciones o haga clic en <a href=index.php?customer=resend_activation>aquí</a> para volver a enviar de nuevo.");
INSERT INTO phpn_vocabulary VALUES("2822","de","_REMEMBER_ME","Erinnere dich an mich");
INSERT INTO phpn_vocabulary VALUES("2823","en","_REMEMBER_ME","Remember Me");
INSERT INTO phpn_vocabulary VALUES("2824","es","_REMEMBER_ME","Acuérdate de mí");
INSERT INTO phpn_vocabulary VALUES("2825","de","_REMOVE","Entfernen");
INSERT INTO phpn_vocabulary VALUES("2826","en","_REMOVE","Remove");
INSERT INTO phpn_vocabulary VALUES("2827","es","_REMOVE","Eliminar");
INSERT INTO phpn_vocabulary VALUES("2828","de","_REMOVED","Entfernt");
INSERT INTO phpn_vocabulary VALUES("2829","en","_REMOVED","Removed");
INSERT INTO phpn_vocabulary VALUES("2830","es","_REMOVED","Eliminado");
INSERT INTO phpn_vocabulary VALUES("2831","de","_REMOVE_ACCOUNT","Konto entfernen");
INSERT INTO phpn_vocabulary VALUES("2832","en","_REMOVE_ACCOUNT","Remove Account");
INSERT INTO phpn_vocabulary VALUES("2833","es","_REMOVE_ACCOUNT","Quite la cuenta");
INSERT INTO phpn_vocabulary VALUES("2834","de","_REMOVE_ACCOUNT_ALERT","Sie sind sicher");
INSERT INTO phpn_vocabulary VALUES("2835","en","_REMOVE_ACCOUNT_ALERT","Are you sure you want to remove your account?");
INSERT INTO phpn_vocabulary VALUES("2836","es","_REMOVE_ACCOUNT_ALERT","¿Está seguro que desea eliminar su cuenta?");
INSERT INTO phpn_vocabulary VALUES("2837","de","_REMOVE_ACCOUNT_WARNING","Wenn Sie nicht glauben wollen");
INSERT INTO phpn_vocabulary VALUES("2838","en","_REMOVE_ACCOUNT_WARNING","If you don\'t think you will use this site again and would like your account deleted, we can take care of this for you. Keep in mind, that you will not be able to reactivate your account or retrieve any of the content or information that was added. If you would like your account deleted, then click Remove button");
INSERT INTO phpn_vocabulary VALUES("2839","es","_REMOVE_ACCOUNT_WARNING","Si usted no piensa que va a utilizar este sitio de nuevo y le gustaría que su cuenta eliminada, nos podemos encargar de esto por usted. Tenga en cuenta, que no podrá reactivar su cuenta o recuperar cualquier contenido o información que se ha añadido. Si desea que su cuenta eliminada, a continuación, haga clic en el botón Quitar");
INSERT INTO phpn_vocabulary VALUES("2840","de","_REMOVE_LAST_COUNTRY_ALERT","Das Land ausgewählt wurde nicht gelöscht, weil Sie mindestens ein aktives Land, um korrekte Arbeitsweise der Seite haben muss!");
INSERT INTO phpn_vocabulary VALUES("2841","en","_REMOVE_LAST_COUNTRY_ALERT","The country selected has not been deleted, because you must have at least one active country for correct work of the site!");
INSERT INTO phpn_vocabulary VALUES("2842","es","_REMOVE_LAST_COUNTRY_ALERT","El país seleccionado no se ha eliminado, ya que debe tener al menos un país activo para un funcionamiento correcto del sitio!");
INSERT INTO phpn_vocabulary VALUES("2843","de","_REMOVE_ROOM_FROM_CART","Nehmen Sie Platz aus dem Warenkorb");
INSERT INTO phpn_vocabulary VALUES("2844","en","_REMOVE_ROOM_FROM_CART","Remove room from the cart");
INSERT INTO phpn_vocabulary VALUES("2845","es","_REMOVE_ROOM_FROM_CART","Retire de la sala de la cesta");
INSERT INTO phpn_vocabulary VALUES("2846","de","_REPORTS","Berichte");
INSERT INTO phpn_vocabulary VALUES("2847","en","_REPORTS","Reports");
INSERT INTO phpn_vocabulary VALUES("2848","es","_REPORTS","Informes");
INSERT INTO phpn_vocabulary VALUES("2849","de","_RESEND_ACTIVATION_EMAIL","Aktivierungsmail erneut zuschicken");
INSERT INTO phpn_vocabulary VALUES("2850","en","_RESEND_ACTIVATION_EMAIL","Resend Activation Email");
INSERT INTO phpn_vocabulary VALUES("2851","es","_RESEND_ACTIVATION_EMAIL","Reenviar email de activación");
INSERT INTO phpn_vocabulary VALUES("2852","de","_RESEND_ACTIVATION_EMAIL_MSG","Bitte geben Sie Ihre E-Mail-Adresse ein und klicken Sie auf Senden. Sie wird die Aktivierung per E-Mail zugesandt.");
INSERT INTO phpn_vocabulary VALUES("2853","en","_RESEND_ACTIVATION_EMAIL_MSG","Please enter your email address then click on Send button. You will receive the activation email shortly.");
INSERT INTO phpn_vocabulary VALUES("2854","es","_RESEND_ACTIVATION_EMAIL_MSG","Por favor, introduzca su dirección de correo electrónico continuación, haga clic en el botón Enviar. Usted recibirá el mensaje de activación en breve.");
INSERT INTO phpn_vocabulary VALUES("2855","de","_RESERVATION","Reservierung");
INSERT INTO phpn_vocabulary VALUES("2856","en","_RESERVATION","Reservation");
INSERT INTO phpn_vocabulary VALUES("2857","es","_RESERVATION","Reservation");
INSERT INTO phpn_vocabulary VALUES("2858","de","_RESERVATIONS","Reservierungen");
INSERT INTO phpn_vocabulary VALUES("2859","en","_RESERVATIONS","Reservations");
INSERT INTO phpn_vocabulary VALUES("2860","es","_RESERVATIONS","Reservas");
INSERT INTO phpn_vocabulary VALUES("2861","de","_RESERVATION_CART","Reservierung Warenkorb");
INSERT INTO phpn_vocabulary VALUES("2862","en","_RESERVATION_CART","Reservation Cart");
INSERT INTO phpn_vocabulary VALUES("2863","es","_RESERVATION_CART","Reserva de compra");
INSERT INTO phpn_vocabulary VALUES("2864","de","_RESERVATION_CART_IS_EMPTY_ALERT","Ihre Reservierung Warenkorb ist leer!");
INSERT INTO phpn_vocabulary VALUES("2865","en","_RESERVATION_CART_IS_EMPTY_ALERT","Your reservation cart is empty!");
INSERT INTO phpn_vocabulary VALUES("2866","es","_RESERVATION_CART_IS_EMPTY_ALERT","Su cesta de la reserva está vacía!");
INSERT INTO phpn_vocabulary VALUES("2867","de","_RESERVATION_DETAILS","Reservierung Details");
INSERT INTO phpn_vocabulary VALUES("2868","en","_RESERVATION_DETAILS","Reservation Details");
INSERT INTO phpn_vocabulary VALUES("2869","es","_RESERVATION_DETAILS","Datos de la Reserva");
INSERT INTO phpn_vocabulary VALUES("2870","de","_RESERVED","vorbehalten");
INSERT INTO phpn_vocabulary VALUES("2871","en","_RESERVED","Reserved");
INSERT INTO phpn_vocabulary VALUES("2872","es","_RESERVED","Reservados");
INSERT INTO phpn_vocabulary VALUES("2873","de","_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("2874","en","_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("2875","es","_RESET","Restablecer");
INSERT INTO phpn_vocabulary VALUES("2876","de","_RESET_ACCOUNT","Konto zurücksetzen");
INSERT INTO phpn_vocabulary VALUES("2877","en","_RESET_ACCOUNT","Reset Account");
INSERT INTO phpn_vocabulary VALUES("2878","es","_RESET_ACCOUNT","Restablecer la cuenta");
INSERT INTO phpn_vocabulary VALUES("2879","de","_RESTAURANT","Restaurant");
INSERT INTO phpn_vocabulary VALUES("2880","en","_RESTAURANT","Restaurant");
INSERT INTO phpn_vocabulary VALUES("2881","es","_RESTAURANT","Restaurant");
INSERT INTO phpn_vocabulary VALUES("2882","de","_RESTORE","Wiederherstellen");
INSERT INTO phpn_vocabulary VALUES("2883","en","_RESTORE","Restore");
INSERT INTO phpn_vocabulary VALUES("2884","es","_RESTORE","Restaurar");
INSERT INTO phpn_vocabulary VALUES("2885","de","_RETYPE_PASSWORD","Passwort wiederholen");
INSERT INTO phpn_vocabulary VALUES("2886","en","_RETYPE_PASSWORD","Retype Password");
INSERT INTO phpn_vocabulary VALUES("2887","es","_RETYPE_PASSWORD","Volver a escribir contraseña");
INSERT INTO phpn_vocabulary VALUES("2888","de","_RIGHT","Recht");
INSERT INTO phpn_vocabulary VALUES("2889","en","_RIGHT","Right");
INSERT INTO phpn_vocabulary VALUES("2890","es","_RIGHT","Derecho");
INSERT INTO phpn_vocabulary VALUES("2891","de","_RIGHT_TO_LEFT","RTL (von rechts nach links)");
INSERT INTO phpn_vocabulary VALUES("2892","en","_RIGHT_TO_LEFT","RTL (right-to-left)");
INSERT INTO phpn_vocabulary VALUES("2893","es","_RIGHT_TO_LEFT","RTL (de derecha a izquierda)");
INSERT INTO phpn_vocabulary VALUES("2894","de","_ROLES_AND_PRIVILEGES","Rollen und Zugriffsrechte");
INSERT INTO phpn_vocabulary VALUES("2895","en","_ROLES_AND_PRIVILEGES","Roles & Privileges");
INSERT INTO phpn_vocabulary VALUES("2896","es","_ROLES_AND_PRIVILEGES","Los roles y privilegios");
INSERT INTO phpn_vocabulary VALUES("2897","de","_ROLES_MANAGEMENT","Rollen-Management");
INSERT INTO phpn_vocabulary VALUES("2898","en","_ROLES_MANAGEMENT","Roles Management");
INSERT INTO phpn_vocabulary VALUES("2899","es","_ROLES_MANAGEMENT","Funciones de administración");
INSERT INTO phpn_vocabulary VALUES("2900","de","_ROOMS","Zimmer");
INSERT INTO phpn_vocabulary VALUES("2901","en","_ROOMS","Rooms");
INSERT INTO phpn_vocabulary VALUES("2902","es","_ROOMS","Habitaciones");
INSERT INTO phpn_vocabulary VALUES("2903","de","_ROOMS_AVAILABILITY","Zimmer Verfügbarkeit");
INSERT INTO phpn_vocabulary VALUES("2904","en","_ROOMS_AVAILABILITY","Rooms Availability");
INSERT INTO phpn_vocabulary VALUES("2905","es","_ROOMS_AVAILABILITY","Disponibilidad");
INSERT INTO phpn_vocabulary VALUES("2906","de","_ROOMS_COUNT","Anzahl der Zimmer (im Hotel)");
INSERT INTO phpn_vocabulary VALUES("2907","en","_ROOMS_COUNT","Number of Rooms (in the Hotel)");
INSERT INTO phpn_vocabulary VALUES("2908","es","_ROOMS_COUNT","Número de habitaciones (en el hotel)");
INSERT INTO phpn_vocabulary VALUES("2909","de","_ROOMS_FACILITIES","Zimmer Einrichtungen");
INSERT INTO phpn_vocabulary VALUES("2910","en","_ROOMS_FACILITIES","Rooms Facilities");
INSERT INTO phpn_vocabulary VALUES("2911","es","_ROOMS_FACILITIES","Habitaciones instalaciones");
INSERT INTO phpn_vocabulary VALUES("2912","de","_ROOMS_LAST","la última habitación");
INSERT INTO phpn_vocabulary VALUES("2913","en","_ROOMS_LAST","last room");
INSERT INTO phpn_vocabulary VALUES("2914","es","_ROOMS_LAST","habitación");
INSERT INTO phpn_vocabulary VALUES("2915","de","_ROOMS_LEFT","Zimmer links");
INSERT INTO phpn_vocabulary VALUES("2916","en","_ROOMS_LEFT","rooms left");
INSERT INTO phpn_vocabulary VALUES("2917","es","_ROOMS_LEFT","salas de la izquierda");
INSERT INTO phpn_vocabulary VALUES("2918","de","_ROOMS_MANAGEMENT","Zimmer Management");
INSERT INTO phpn_vocabulary VALUES("2919","en","_ROOMS_MANAGEMENT","Rooms Management");
INSERT INTO phpn_vocabulary VALUES("2920","es","_ROOMS_MANAGEMENT","Administración Salas de");
INSERT INTO phpn_vocabulary VALUES("2921","de","_ROOMS_OCCUPANCY","Zimmer Belegung");
INSERT INTO phpn_vocabulary VALUES("2922","en","_ROOMS_OCCUPANCY","Rooms Occupancy");
INSERT INTO phpn_vocabulary VALUES("2923","es","_ROOMS_OCCUPANCY","Las habitaciones de ocupación");
INSERT INTO phpn_vocabulary VALUES("2924","de","_ROOMS_RESERVATION","Zimmer Reservierung");
INSERT INTO phpn_vocabulary VALUES("2925","en","_ROOMS_RESERVATION","Rooms Reservation");
INSERT INTO phpn_vocabulary VALUES("2926","es","_ROOMS_RESERVATION","Reservación de Habitaciones");
INSERT INTO phpn_vocabulary VALUES("2927","de","_ROOMS_SETTINGS","Zimmer-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("2928","en","_ROOMS_SETTINGS","Rooms Settings");
INSERT INTO phpn_vocabulary VALUES("2929","es","_ROOMS_SETTINGS","Configuración de habitaciones");
INSERT INTO phpn_vocabulary VALUES("2930","de","_ROOM_AREA","Raum Fläche");
INSERT INTO phpn_vocabulary VALUES("2931","en","_ROOM_AREA","Room Area");
INSERT INTO phpn_vocabulary VALUES("2932","es","_ROOM_AREA","Sala Espacio");
INSERT INTO phpn_vocabulary VALUES("2933","de","_ROOM_DESCRIPTION","Zimmer Beschreibung");
INSERT INTO phpn_vocabulary VALUES("2934","en","_ROOM_DESCRIPTION","Room Description");
INSERT INTO phpn_vocabulary VALUES("2935","es","_ROOM_DESCRIPTION","Descripción de las habitaciones");
INSERT INTO phpn_vocabulary VALUES("2936","de","_ROOM_DETAILS","Zimmerbeschreibung");
INSERT INTO phpn_vocabulary VALUES("2937","en","_ROOM_DETAILS","Room Details");
INSERT INTO phpn_vocabulary VALUES("2938","es","_ROOM_DETAILS","Información de las habitaciones");
INSERT INTO phpn_vocabulary VALUES("2939","de","_ROOM_FACILITIES","Zimmerausstattung");
INSERT INTO phpn_vocabulary VALUES("2940","en","_ROOM_FACILITIES","Room Facilities");
INSERT INTO phpn_vocabulary VALUES("2941","es","_ROOM_FACILITIES","Facilidades en la habitación");
INSERT INTO phpn_vocabulary VALUES("2942","de","_ROOM_FACILITIES_MANAGEMENT","Zimmerausstattung Management");
INSERT INTO phpn_vocabulary VALUES("2943","en","_ROOM_FACILITIES_MANAGEMENT","Room Facilities Management");
INSERT INTO phpn_vocabulary VALUES("2944","es","_ROOM_FACILITIES_MANAGEMENT","Servicio de habitaciones Administración");
INSERT INTO phpn_vocabulary VALUES("2945","de","_ROOM_NOT_FOUND","Zimmer wurde nicht gefunden!");
INSERT INTO phpn_vocabulary VALUES("2946","en","_ROOM_NOT_FOUND","Room has not been found!");
INSERT INTO phpn_vocabulary VALUES("2947","es","_ROOM_NOT_FOUND","Habitaciones no se ha encontrado!");
INSERT INTO phpn_vocabulary VALUES("2948","de","_ROOM_NUMBERS","Zimmernummern");
INSERT INTO phpn_vocabulary VALUES("2949","en","_ROOM_NUMBERS","Room Numbers");
INSERT INTO phpn_vocabulary VALUES("2950","es","_ROOM_NUMBERS","Números de las habitaciones");
INSERT INTO phpn_vocabulary VALUES("2951","de","_ROOM_PRICE","Zimmerpreise");
INSERT INTO phpn_vocabulary VALUES("2952","en","_ROOM_PRICE","Room Price");
INSERT INTO phpn_vocabulary VALUES("2953","es","_ROOM_PRICE","Precio de la habitación");
INSERT INTO phpn_vocabulary VALUES("2954","de","_ROOM_PRICES_WERE_ADDED","Zimmerpreise für neue Periode wurden erfolgreich hinzugefügt!");
INSERT INTO phpn_vocabulary VALUES("2955","en","_ROOM_PRICES_WERE_ADDED","Room prices for new period were successfully added!");
INSERT INTO phpn_vocabulary VALUES("2956","es","_ROOM_PRICES_WERE_ADDED","los precios de habitaciones nuevo período se ha añadido correctamente!");
INSERT INTO phpn_vocabulary VALUES("2957","de","_ROOM_TYPE","Zimmer Typ");
INSERT INTO phpn_vocabulary VALUES("2958","en","_ROOM_TYPE","Room Type");
INSERT INTO phpn_vocabulary VALUES("2959","es","_ROOM_TYPE","Tipo de Habitación");
INSERT INTO phpn_vocabulary VALUES("2960","de","_ROOM_WAS_ADDED","Zimmer war erfolgreich, um Ihre Reservierung hinzugefügt!");
INSERT INTO phpn_vocabulary VALUES("2961","en","_ROOM_WAS_ADDED","Room has been successfully added to your reservation!");
INSERT INTO phpn_vocabulary VALUES("2962","es","_ROOM_WAS_ADDED","Sala se ha añadido a su reserva!");
INSERT INTO phpn_vocabulary VALUES("2963","de","_ROOM_WAS_REMOVED","Ausgewählte Zimmer wurde erfolgreich von Ihrem Reservierung Warenkorb entfernt!");
INSERT INTO phpn_vocabulary VALUES("2964","en","_ROOM_WAS_REMOVED","Selected room has been successfully removed from your Reservation Cart!");
INSERT INTO phpn_vocabulary VALUES("2965","es","_ROOM_WAS_REMOVED","Habitación seleccionada se ha eliminado de su Reserva de compra!");
INSERT INTO phpn_vocabulary VALUES("2966","de","_ROWS","Reihen");
INSERT INTO phpn_vocabulary VALUES("2967","en","_ROWS","Rows");
INSERT INTO phpn_vocabulary VALUES("2968","es","_ROWS","Filas");
INSERT INTO phpn_vocabulary VALUES("2969","de","_RSS_FEED_TYPE","RSS Feed-Typ");
INSERT INTO phpn_vocabulary VALUES("2970","en","_RSS_FEED_TYPE","RSS Feed Type");
INSERT INTO phpn_vocabulary VALUES("2971","es","_RSS_FEED_TYPE","RSS Feed Tipo");
INSERT INTO phpn_vocabulary VALUES("2972","de","_RSS_FILE_ERROR","Kann nicht geöffnet werden RSS-Datei auf neues Element hinzufügen! Bitte überprüfen Sie Ihre Zugriffsrechte auf <b>feeds/</b> Verzeichnis oder versuchen Sie es später erneut.");
INSERT INTO phpn_vocabulary VALUES("2973","en","_RSS_FILE_ERROR","Cannot open RSS file to add new item! Please check your access rights to <b>feeds/</b> directory or try again later.");
INSERT INTO phpn_vocabulary VALUES("2974","es","_RSS_FILE_ERROR","No se puede abrir el archivo RSS para añadir el artículo nuevo! Por favor, compruebe sus derechos de acceso a <b>feeds/</b> del directorio o inténtelo de nuevo más tarde.");
INSERT INTO phpn_vocabulary VALUES("2975","de","_RUN_CRON","Laufen Cron");
INSERT INTO phpn_vocabulary VALUES("2976","en","_RUN_CRON","Run cron");
INSERT INTO phpn_vocabulary VALUES("2977","es","_RUN_CRON","Ejecutar cron");
INSERT INTO phpn_vocabulary VALUES("2978","de","_RUN_EVERY","Führen Sie alle");
INSERT INTO phpn_vocabulary VALUES("2979","en","_RUN_EVERY","Run every");
INSERT INTO phpn_vocabulary VALUES("2980","es","_RUN_EVERY","Ejecutar cada");
INSERT INTO phpn_vocabulary VALUES("2981","de","_SA","Sa");
INSERT INTO phpn_vocabulary VALUES("2982","en","_SA","Sa");
INSERT INTO phpn_vocabulary VALUES("2983","es","_SA","Sa");
INSERT INTO phpn_vocabulary VALUES("2984","de","_SAID","sagte");
INSERT INTO phpn_vocabulary VALUES("2985","en","_SAID","said");
INSERT INTO phpn_vocabulary VALUES("2986","es","_SAID","dijo");
INSERT INTO phpn_vocabulary VALUES("2987","de","_SAT","Sat");
INSERT INTO phpn_vocabulary VALUES("2988","en","_SAT","Sat");
INSERT INTO phpn_vocabulary VALUES("2989","es","_SAT","Sáb");
INSERT INTO phpn_vocabulary VALUES("2990","de","_SATURDAY","Samstag");
INSERT INTO phpn_vocabulary VALUES("2991","en","_SATURDAY","Saturday");
INSERT INTO phpn_vocabulary VALUES("2992","es","_SATURDAY","sábado");
INSERT INTO phpn_vocabulary VALUES("2993","de","_SCHEDULED_CAMPAIGN","Geplante Kampagne");
INSERT INTO phpn_vocabulary VALUES("2994","en","_SCHEDULED_CAMPAIGN","Scheduled Campaign");
INSERT INTO phpn_vocabulary VALUES("2995","es","_SCHEDULED_CAMPAIGN","Campaña Programado");
INSERT INTO phpn_vocabulary VALUES("2996","de","_SEARCH","Suche");
INSERT INTO phpn_vocabulary VALUES("2997","en","_SEARCH","Search");
INSERT INTO phpn_vocabulary VALUES("2998","es","_SEARCH","Búsqueda");
INSERT INTO phpn_vocabulary VALUES("2999","de","_SEARCH_KEYWORDS","Suchbegriffe");
INSERT INTO phpn_vocabulary VALUES("3000","en","_SEARCH_KEYWORDS","search keywords");
INSERT INTO phpn_vocabulary VALUES("3001","es","_SEARCH_KEYWORDS","búsqueda");
INSERT INTO phpn_vocabulary VALUES("3002","de","_SEARCH_RESULT_FOR","Suchergebnisse für");
INSERT INTO phpn_vocabulary VALUES("3003","en","_SEARCH_RESULT_FOR","Search Results for");
INSERT INTO phpn_vocabulary VALUES("3004","es","_SEARCH_RESULT_FOR","Resultados de la búsqueda para");
INSERT INTO phpn_vocabulary VALUES("3005","de","_SEARCH_ROOM_TIPS","<b> Weitere Räume durch die Erweiterung Ihrer Suchoptionen</b>: <br> - Reduzieren Sie die Anzahl der Erwachsenen im Zimmer, um mehr Ergebnisse zu erhalten\n<br> - Reduzieren Sie die Anzahl der Kinder im Zimmer um Ihr Ergebnis zu\n<br> - Ändern Sie Ihr Check-in/Check-out-Datum<br>");
INSERT INTO phpn_vocabulary VALUES("3006","en","_SEARCH_ROOM_TIPS","<b>Find more rooms by expanding your search options</b>:<br>- Reduce the number of adults in room to get more results<br>- Reduce the number of children in room to get more results<br>- Change your Check-in/Check-out dates<br>");
INSERT INTO phpn_vocabulary VALUES("3007","es","_SEARCH_ROOM_TIPS","<b>Buscar más habitaciones, ampliando sus opciones de búsqueda</b>:<br> - Reducir el número de adultos en la habitación para conseguir más resultados <br> - Reducir el número de niños en la habitación para conseguir más resultados <br> - Cambio de fechas de llegada y salida<br>");
INSERT INTO phpn_vocabulary VALUES("3008","de","_SEC","sec");
INSERT INTO phpn_vocabulary VALUES("3009","en","_SEC","Sec");
INSERT INTO phpn_vocabulary VALUES("3010","es","_SEC","Seg");
INSERT INTO phpn_vocabulary VALUES("3011","de","_SELECT","wählen");
INSERT INTO phpn_vocabulary VALUES("3012","en","_SELECT","select");
INSERT INTO phpn_vocabulary VALUES("3013","es","_SELECT","Seleccione");
INSERT INTO phpn_vocabulary VALUES("3014","de","_SELECTED_ROOMS","Ausgewählte Zimmer");
INSERT INTO phpn_vocabulary VALUES("3015","en","_SELECTED_ROOMS","Selected Rooms");
INSERT INTO phpn_vocabulary VALUES("3016","es","_SELECTED_ROOMS","En algunas de ellas");
INSERT INTO phpn_vocabulary VALUES("3017","de","_SELECT_FILE_TO_UPLOAD","Wählen Sie eine Datei zum Hochladen");
INSERT INTO phpn_vocabulary VALUES("3018","en","_SELECT_FILE_TO_UPLOAD","Select a file to upload");
INSERT INTO phpn_vocabulary VALUES("3019","es","_SELECT_FILE_TO_UPLOAD","Seleccione un archivo para cargar");
INSERT INTO phpn_vocabulary VALUES("3020","de","_SELECT_HOTEL","Hotel wählen");
INSERT INTO phpn_vocabulary VALUES("3021","en","_SELECT_HOTEL","Select Hotel");
INSERT INTO phpn_vocabulary VALUES("3022","es","_SELECT_HOTEL","Select Hotel");
INSERT INTO phpn_vocabulary VALUES("3023","de","_SELECT_LANG_TO_UPDATE","Wählen Sie eine Sprache zu aktualisieren");
INSERT INTO phpn_vocabulary VALUES("3024","en","_SELECT_LANG_TO_UPDATE","Select a language to update");
INSERT INTO phpn_vocabulary VALUES("3025","es","_SELECT_LANG_TO_UPDATE","Seleccione un idioma para actualizar");
INSERT INTO phpn_vocabulary VALUES("3026","de","_SELECT_LOCATION","Standort auswählen");
INSERT INTO phpn_vocabulary VALUES("3027","en","_SELECT_LOCATION","Select Location");
INSERT INTO phpn_vocabulary VALUES("3028","es","_SELECT_LOCATION","Seleccionar ubicación");
INSERT INTO phpn_vocabulary VALUES("3029","de","_SELECT_REPORT_ALERT","Bitte wählen Sie einen Bericht geben!");
INSERT INTO phpn_vocabulary VALUES("3030","en","_SELECT_REPORT_ALERT","Please select a report type!");
INSERT INTO phpn_vocabulary VALUES("3031","es","_SELECT_REPORT_ALERT","Por favor, seleccione un tipo de informe!");
INSERT INTO phpn_vocabulary VALUES("3032","de","_SEND","Senden");
INSERT INTO phpn_vocabulary VALUES("3033","en","_SEND","Send");
INSERT INTO phpn_vocabulary VALUES("3034","es","_SEND","Enviar");
INSERT INTO phpn_vocabulary VALUES("3035","de","_SENDING","Senden");
INSERT INTO phpn_vocabulary VALUES("3036","en","_SENDING","Sending");
INSERT INTO phpn_vocabulary VALUES("3037","es","_SENDING","Envío");
INSERT INTO phpn_vocabulary VALUES("3038","de","_SEND_COPY_TO_ADMIN","Senden Einer kopie ein Admin");
INSERT INTO phpn_vocabulary VALUES("3039","en","_SEND_COPY_TO_ADMIN","Send a copy to admin");
INSERT INTO phpn_vocabulary VALUES("3040","es","_SEND_COPY_TO_ADMIN","Enviar una copia al administrador");
INSERT INTO phpn_vocabulary VALUES("3041","de","_SEND_INVOICE","Senden Rechnung");
INSERT INTO phpn_vocabulary VALUES("3042","en","_SEND_INVOICE","Send Invoice");
INSERT INTO phpn_vocabulary VALUES("3043","es","_SEND_INVOICE","Enviar factura");
INSERT INTO phpn_vocabulary VALUES("3044","de","_SEO_LINKS_ALERT","Wenn Sie diese Option");
INSERT INTO phpn_vocabulary VALUES("3045","en","_SEO_LINKS_ALERT","If you select this option, make sure SEO Links Section uncommented in .htaccess file");
INSERT INTO phpn_vocabulary VALUES("3046","es","_SEO_LINKS_ALERT","Si selecciona esta opción, asegúrese de SEO Enlaces sin comentarios en la sección .htaccess");
INSERT INTO phpn_vocabulary VALUES("3047","de","_SEO_URLS","SEO URLs");
INSERT INTO phpn_vocabulary VALUES("3048","en","_SEO_URLS","SEO URLs");
INSERT INTO phpn_vocabulary VALUES("3049","es","_SEO_URLS","SEO URLs");
INSERT INTO phpn_vocabulary VALUES("3050","de","_SEPTEMBER","September");
INSERT INTO phpn_vocabulary VALUES("3051","en","_SEPTEMBER","September");
INSERT INTO phpn_vocabulary VALUES("3052","es","_SEPTEMBER","Septiembre");
INSERT INTO phpn_vocabulary VALUES("3053","de","_SERVER_INFO","Server Info");
INSERT INTO phpn_vocabulary VALUES("3054","en","_SERVER_INFO","Server Info");
INSERT INTO phpn_vocabulary VALUES("3055","es","_SERVER_INFO","Servidor de Información");
INSERT INTO phpn_vocabulary VALUES("3056","de","_SERVER_LOCALE","Server locale");
INSERT INTO phpn_vocabulary VALUES("3057","en","_SERVER_LOCALE","Server Locale");
INSERT INTO phpn_vocabulary VALUES("3058","es","_SERVER_LOCALE","servidor local");
INSERT INTO phpn_vocabulary VALUES("3059","de","_SERVICE","Service");
INSERT INTO phpn_vocabulary VALUES("3060","en","_SERVICE","Service");
INSERT INTO phpn_vocabulary VALUES("3061","es","_SERVICE","Servicio");
INSERT INTO phpn_vocabulary VALUES("3062","de","_SERVICES","Dienstleistungen");
INSERT INTO phpn_vocabulary VALUES("3063","en","_SERVICES","Services");
INSERT INTO phpn_vocabulary VALUES("3064","es","_SERVICES","Servicios");
INSERT INTO phpn_vocabulary VALUES("3065","de","_SETTINGS","Einstellungen");
INSERT INTO phpn_vocabulary VALUES("3066","en","_SETTINGS","Settings");
INSERT INTO phpn_vocabulary VALUES("3067","es","_SETTINGS","Configuración");
INSERT INTO phpn_vocabulary VALUES("3068","de","_SETTINGS_SAVED","Änderungen wurden gespeichert! Bitte aktualisieren Sie die <a href=index.php>Home Page</a> zu den Suchergebnissen finden Sie im.");
INSERT INTO phpn_vocabulary VALUES("3069","en","_SETTINGS_SAVED","Changes were saved! Please refresh the <a href=index.php>Home Page</a> to see the results.");
INSERT INTO phpn_vocabulary VALUES("3070","es","_SETTINGS_SAVED","Los cambios fueron salvados! Por favor, actualice la <a href=index.php>Home Page</a> para ver los resultados.");
INSERT INTO phpn_vocabulary VALUES("3071","de","_SET_ADMIN","Set Admin");
INSERT INTO phpn_vocabulary VALUES("3072","en","_SET_ADMIN","Set Admin");
INSERT INTO phpn_vocabulary VALUES("3073","es","_SET_ADMIN","Set de admin");
INSERT INTO phpn_vocabulary VALUES("3074","de","_SET_DATE","Stellen Sie das Datum");
INSERT INTO phpn_vocabulary VALUES("3075","en","_SET_DATE","Set date");
INSERT INTO phpn_vocabulary VALUES("3076","es","_SET_DATE","Establecer fecha");
INSERT INTO phpn_vocabulary VALUES("3077","de","_SET_PERIODS","gesetzt Perioden");
INSERT INTO phpn_vocabulary VALUES("3078","en","_SET_PERIODS","Set Periods");
INSERT INTO phpn_vocabulary VALUES("3079","es","_SET_PERIODS","Establecer períodos");
INSERT INTO phpn_vocabulary VALUES("3080","de","_SET_TIME","Eingestellte Zeit");
INSERT INTO phpn_vocabulary VALUES("3081","en","_SET_TIME","Set Time");
INSERT INTO phpn_vocabulary VALUES("3082","es","_SET_TIME","Ajuste de la hora");
INSERT INTO phpn_vocabulary VALUES("3083","de","_SHORT_DESCRIPTION","Kurzbeschreibung");
INSERT INTO phpn_vocabulary VALUES("3084","en","_SHORT_DESCRIPTION","Short Description");
INSERT INTO phpn_vocabulary VALUES("3085","es","_SHORT_DESCRIPTION","Descripción corta");
INSERT INTO phpn_vocabulary VALUES("3086","de","_SHOW","Zeigen");
INSERT INTO phpn_vocabulary VALUES("3087","en","_SHOW","Show");
INSERT INTO phpn_vocabulary VALUES("3088","es","_SHOW","Mostrar");
INSERT INTO phpn_vocabulary VALUES("3089","de","_SHOW_IN_SEARCH","Show auf der Suche");
INSERT INTO phpn_vocabulary VALUES("3090","en","_SHOW_IN_SEARCH","Show in Search");
INSERT INTO phpn_vocabulary VALUES("3091","es","_SHOW_IN_SEARCH","Ver en la búsqueda");
INSERT INTO phpn_vocabulary VALUES("3092","de","_SHOW_META_TAGS","META-Tags anzeigen");
INSERT INTO phpn_vocabulary VALUES("3093","en","_SHOW_META_TAGS","Show META tags");
INSERT INTO phpn_vocabulary VALUES("3094","es","_SHOW_META_TAGS","Mostrar Las etiquetas META");
INSERT INTO phpn_vocabulary VALUES("3095","de","_SHOW_ON_DASHBOARD","Anzeigen auf Armaturenbrett");
INSERT INTO phpn_vocabulary VALUES("3096","en","_SHOW_ON_DASHBOARD","Show on Dashboard");
INSERT INTO phpn_vocabulary VALUES("3097","es","_SHOW_ON_DASHBOARD","Mostrar en el Dashboard");
INSERT INTO phpn_vocabulary VALUES("3098","de","_SIDE_PANEL","Seitenteil");
INSERT INTO phpn_vocabulary VALUES("3099","en","_SIDE_PANEL","Side Panel");
INSERT INTO phpn_vocabulary VALUES("3100","es","_SIDE_PANEL","El panel lateral");
INSERT INTO phpn_vocabulary VALUES("3101","de","_SIMPLE","Einfache");
INSERT INTO phpn_vocabulary VALUES("3102","en","_SIMPLE","Simple");
INSERT INTO phpn_vocabulary VALUES("3103","es","_SIMPLE","Simple");
INSERT INTO phpn_vocabulary VALUES("3104","de","_SITE_DEVELOPMENT_MODE_ALERT","Die Seite ist in Entwicklung Modus läuft! Um ihn auszuschalten ändern <b>SITE_MODE</b>-Wert in <b>inc/settings.inc.php</b>");
INSERT INTO phpn_vocabulary VALUES("3105","en","_SITE_DEVELOPMENT_MODE_ALERT","The site is running in Development Mode! To turn it off change <b>SITE_MODE</b> value in <b>inc/settings.inc.php</b>");
INSERT INTO phpn_vocabulary VALUES("3106","es","_SITE_DEVELOPMENT_MODE_ALERT","El sitio se está ejecutando en el modo de desarrollo! Para desactivarlo cambio <b>SITE_MODE</b> valor en <b>inc/settings.inc.php</b>");
INSERT INTO phpn_vocabulary VALUES("3107","de","_SITE_INFO","Homepage Info");
INSERT INTO phpn_vocabulary VALUES("3108","en","_SITE_INFO","Site Info");
INSERT INTO phpn_vocabulary VALUES("3109","es","_SITE_INFO","Información del Sitio");
INSERT INTO phpn_vocabulary VALUES("3110","de","_SITE_OFFLINE","Website offline");
INSERT INTO phpn_vocabulary VALUES("3111","en","_SITE_OFFLINE","Site Offline");
INSERT INTO phpn_vocabulary VALUES("3112","es","_SITE_OFFLINE","Sitio fuera de línea");
INSERT INTO phpn_vocabulary VALUES("3113","de","_SITE_OFFLINE_ALERT","Wählen Sie");
INSERT INTO phpn_vocabulary VALUES("3114","en","_SITE_OFFLINE_ALERT","Select whether access to the Site Front-end is available. If Yes, the Front-End will display the message below");
INSERT INTO phpn_vocabulary VALUES("3115","es","_SITE_OFFLINE_ALERT","Seleccione si el acceso al Sitio Front-end está disponible. En caso afirmativo, el Front-End mostrará el mensaje siguiente:");
INSERT INTO phpn_vocabulary VALUES("3116","de","_SITE_OFFLINE_MESSAGE_ALERT","Eine Nachricht");
INSERT INTO phpn_vocabulary VALUES("3117","en","_SITE_OFFLINE_MESSAGE_ALERT","A message that displays in the Front-end if your site is offline");
INSERT INTO phpn_vocabulary VALUES("3118","es","_SITE_OFFLINE_MESSAGE_ALERT","Un mensaje que aparece en el front-end si su sitio está en línea");
INSERT INTO phpn_vocabulary VALUES("3119","de","_SITE_PREVIEW","Website-Vorschau");
INSERT INTO phpn_vocabulary VALUES("3120","en","_SITE_PREVIEW","Site Preview");
INSERT INTO phpn_vocabulary VALUES("3121","es","_SITE_PREVIEW","Sitio de vista preliminar");
INSERT INTO phpn_vocabulary VALUES("3122","de","_SITE_RANKS","Website Reihen");
INSERT INTO phpn_vocabulary VALUES("3123","en","_SITE_RANKS","Site Ranks");
INSERT INTO phpn_vocabulary VALUES("3124","es","_SITE_RANKS","Sitio Ranks");
INSERT INTO phpn_vocabulary VALUES("3125","de","_SITE_RSS","Website RSS");
INSERT INTO phpn_vocabulary VALUES("3126","en","_SITE_RSS","Site RSS");
INSERT INTO phpn_vocabulary VALUES("3127","es","_SITE_RSS","RSS del sitio");
INSERT INTO phpn_vocabulary VALUES("3128","de","_SITE_SETTINGS","Site-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("3129","en","_SITE_SETTINGS","Site Settings");
INSERT INTO phpn_vocabulary VALUES("3130","es","_SITE_SETTINGS","Configuración del sitio");
INSERT INTO phpn_vocabulary VALUES("3131","de","_SMTP_HOST","SMTP-Host");
INSERT INTO phpn_vocabulary VALUES("3132","en","_SMTP_HOST","SMTP Host");
INSERT INTO phpn_vocabulary VALUES("3133","es","_SMTP_HOST","SMTP host");
INSERT INTO phpn_vocabulary VALUES("3134","de","_SMTP_PORT","SMTP-Port");
INSERT INTO phpn_vocabulary VALUES("3135","en","_SMTP_PORT","SMTP Port");
INSERT INTO phpn_vocabulary VALUES("3136","es","_SMTP_PORT","Puerto SMTP");
INSERT INTO phpn_vocabulary VALUES("3137","de","_SMTP_SECURE","SMTP Sichere");
INSERT INTO phpn_vocabulary VALUES("3138","en","_SMTP_SECURE","SMTP Secure");
INSERT INTO phpn_vocabulary VALUES("3139","es","_SMTP_SECURE","Secure SMTP");
INSERT INTO phpn_vocabulary VALUES("3140","de","_SORT_BY","Sortieren");
INSERT INTO phpn_vocabulary VALUES("3141","en","_SORT_BY","Sort by");
INSERT INTO phpn_vocabulary VALUES("3142","es","_SORT_BY","Ordenar");
INSERT INTO phpn_vocabulary VALUES("3143","de","_STANDARD","Standards");
INSERT INTO phpn_vocabulary VALUES("3144","en","_STANDARD","Standard");
INSERT INTO phpn_vocabulary VALUES("3145","es","_STANDARD","Estándar");
INSERT INTO phpn_vocabulary VALUES("3146","de","_STANDARD_PRICE","Standard Preis");
INSERT INTO phpn_vocabulary VALUES("3147","en","_STANDARD_PRICE","Standard Price");
INSERT INTO phpn_vocabulary VALUES("3148","es","_STANDARD_PRICE","Precio estándar");
INSERT INTO phpn_vocabulary VALUES("3149","de","_STARS","Sterne");
INSERT INTO phpn_vocabulary VALUES("3150","en","_STARS","Stars");
INSERT INTO phpn_vocabulary VALUES("3151","es","_STARS","Estrellas");
INSERT INTO phpn_vocabulary VALUES("3152","de","_STARS_1_5","1 Stern bis 5 Sterne");
INSERT INTO phpn_vocabulary VALUES("3153","en","_STARS_1_5","1 star to 5 stars");
INSERT INTO phpn_vocabulary VALUES("3154","es","_STARS_1_5","1 a 5 estrellas");
INSERT INTO phpn_vocabulary VALUES("3155","de","_STARS_5_1","5 Sterne für 1 Stern");
INSERT INTO phpn_vocabulary VALUES("3156","en","_STARS_5_1","5 stars to 1 star");
INSERT INTO phpn_vocabulary VALUES("3157","es","_STARS_5_1","5 estrellas a 1 estrella");
INSERT INTO phpn_vocabulary VALUES("3158","de","_START_DATE","Startdatum");
INSERT INTO phpn_vocabulary VALUES("3159","en","_START_DATE","Start Date");
INSERT INTO phpn_vocabulary VALUES("3160","es","_START_DATE","Fecha de inicio");
INSERT INTO phpn_vocabulary VALUES("3161","de","_START_FINISH_DATE_ERROR","Endtermin muss spätestens Startdatum werden! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3162","en","_START_FINISH_DATE_ERROR","Finish date must be later than start date! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3163","es","_START_FINISH_DATE_ERROR","Fecha de finalización debe ser posterior a la fecha de inicio! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3164","de","_START_OVER","Beginnen");
INSERT INTO phpn_vocabulary VALUES("3165","en","_START_OVER","Start Over");
INSERT INTO phpn_vocabulary VALUES("3166","es","_START_OVER","Comenzar de Nuevo");
INSERT INTO phpn_vocabulary VALUES("3167","de","_STATE","Zustand");
INSERT INTO phpn_vocabulary VALUES("3168","en","_STATE","State");
INSERT INTO phpn_vocabulary VALUES("3169","es","_STATE","Estado");
INSERT INTO phpn_vocabulary VALUES("3170","de","_STATES","Staaten");
INSERT INTO phpn_vocabulary VALUES("3171","en","_STATES","States");
INSERT INTO phpn_vocabulary VALUES("3172","es","_STATES","Unidos");
INSERT INTO phpn_vocabulary VALUES("3173","de","_STATE_PROVINCE","Staat/Provinz");
INSERT INTO phpn_vocabulary VALUES("3174","en","_STATE_PROVINCE","State/Province");
INSERT INTO phpn_vocabulary VALUES("3175","es","_STATE_PROVINCE","Estado/Provincia");
INSERT INTO phpn_vocabulary VALUES("3176","de","_STATISTICS","Statistik");
INSERT INTO phpn_vocabulary VALUES("3177","en","_STATISTICS","Statistics");
INSERT INTO phpn_vocabulary VALUES("3178","es","_STATISTICS","Estadística");
INSERT INTO phpn_vocabulary VALUES("3179","de","_STATUS","Status");
INSERT INTO phpn_vocabulary VALUES("3180","en","_STATUS","Status");
INSERT INTO phpn_vocabulary VALUES("3181","es","_STATUS","Estado");
INSERT INTO phpn_vocabulary VALUES("3182","de","_STOP","Anschlag");
INSERT INTO phpn_vocabulary VALUES("3183","en","_STOP","Stop");
INSERT INTO phpn_vocabulary VALUES("3184","es","_STOP","Detener");
INSERT INTO phpn_vocabulary VALUES("3185","de","_SU","So");
INSERT INTO phpn_vocabulary VALUES("3186","en","_SU","Su");
INSERT INTO phpn_vocabulary VALUES("3187","es","_SU","Do");
INSERT INTO phpn_vocabulary VALUES("3188","de","_SUBJECT","Gegenstand");
INSERT INTO phpn_vocabulary VALUES("3189","en","_SUBJECT","Subject");
INSERT INTO phpn_vocabulary VALUES("3190","es","_SUBJECT","Tema");
INSERT INTO phpn_vocabulary VALUES("3191","de","_SUBJECT_EMPTY_ALERT","Betreff darf nicht leer sein!");
INSERT INTO phpn_vocabulary VALUES("3192","en","_SUBJECT_EMPTY_ALERT","Subject cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("3193","es","_SUBJECT_EMPTY_ALERT","Sin perjuicio no puede estar vacío!");
INSERT INTO phpn_vocabulary VALUES("3194","de","_SUBMIT","Eintragen");
INSERT INTO phpn_vocabulary VALUES("3195","en","_SUBMIT","Submit");
INSERT INTO phpn_vocabulary VALUES("3196","es","_SUBMIT","Submit");
INSERT INTO phpn_vocabulary VALUES("3197","de","_SUBMIT_BOOKING","Senden Buchung");
INSERT INTO phpn_vocabulary VALUES("3198","en","_SUBMIT_BOOKING","Submit Booking");
INSERT INTO phpn_vocabulary VALUES("3199","es","_SUBMIT_BOOKING","Enviar Reservas");
INSERT INTO phpn_vocabulary VALUES("3200","de","_SUBMIT_PAYMENT","Submit Zahlung");
INSERT INTO phpn_vocabulary VALUES("3201","en","_SUBMIT_PAYMENT","Submit Payment");
INSERT INTO phpn_vocabulary VALUES("3202","es","_SUBMIT_PAYMENT","Presentar el pago");
INSERT INTO phpn_vocabulary VALUES("3203","de","_SUBSCRIBE","Zeichnen");
INSERT INTO phpn_vocabulary VALUES("3204","en","_SUBSCRIBE","Subscribe");
INSERT INTO phpn_vocabulary VALUES("3205","es","_SUBSCRIBE","Suscribir");
INSERT INTO phpn_vocabulary VALUES("3206","de","_SUBSCRIBE_EMAIL_EXISTS_ALERT","Jemand mit einer solchen E-Mail hat schon unseren Newsletter abonniert worden. Bitte wählen Sie ein anderes E-Mail-Adresse zum Bezug anzubieten.");
INSERT INTO phpn_vocabulary VALUES("3207","en","_SUBSCRIBE_EMAIL_EXISTS_ALERT","Someone with such email has already been subscribed to our newsletter. Please choose another email address for subscription.");
INSERT INTO phpn_vocabulary VALUES("3208","es","_SUBSCRIBE_EMAIL_EXISTS_ALERT","Alguien con el correo electrónico como ya se ha suscrito a nuestro boletín de noticias. Por favor, elija otra dirección de correo electrónico para la suscripción.");
INSERT INTO phpn_vocabulary VALUES("3209","de","_SUBSCRIBE_TO_NEWSLETTER","Newsletter abonnieren");
INSERT INTO phpn_vocabulary VALUES("3210","en","_SUBSCRIBE_TO_NEWSLETTER","Newsletter Subscription");
INSERT INTO phpn_vocabulary VALUES("3211","es","_SUBSCRIBE_TO_NEWSLETTER","Suscríbete al boletín de noticias");
INSERT INTO phpn_vocabulary VALUES("3212","de","_SUBSCRIPTION_ALREADY_SENT","Sie haben bereits unseren Newsletter abonniert. Bitte versuchen Sie es später noch einmal oder warten _WAIT_ Sekunden.");
INSERT INTO phpn_vocabulary VALUES("3213","en","_SUBSCRIPTION_ALREADY_SENT","You have already subscribed to our newsletter. Please try again later or wait _WAIT_ seconds.");
INSERT INTO phpn_vocabulary VALUES("3214","es","_SUBSCRIPTION_ALREADY_SENT","Ya se ha suscrito a nuestro boletín. Por favor, inténtelo de nuevo más tarde o esperar unos segundos _WAIT_.");
INSERT INTO phpn_vocabulary VALUES("3215","de","_SUBSCRIPTION_MANAGEMENT","Abo-Verwaltung");
INSERT INTO phpn_vocabulary VALUES("3216","en","_SUBSCRIPTION_MANAGEMENT","Subscription Management");
INSERT INTO phpn_vocabulary VALUES("3217","es","_SUBSCRIPTION_MANAGEMENT","De administración de suscripciones");
INSERT INTO phpn_vocabulary VALUES("3218","de","_SUBTOTAL","Zwischensumme");
INSERT INTO phpn_vocabulary VALUES("3219","en","_SUBTOTAL","Subtotal");
INSERT INTO phpn_vocabulary VALUES("3220","es","_SUBTOTAL","Total parcial");
INSERT INTO phpn_vocabulary VALUES("3221","de","_SUN","Son");
INSERT INTO phpn_vocabulary VALUES("3222","en","_SUN","Sun");
INSERT INTO phpn_vocabulary VALUES("3223","es","_SUN","Dom");
INSERT INTO phpn_vocabulary VALUES("3224","de","_SUNDAY","Sonntag");
INSERT INTO phpn_vocabulary VALUES("3225","en","_SUNDAY","Sunday");
INSERT INTO phpn_vocabulary VALUES("3226","es","_SUNDAY","domingo");
INSERT INTO phpn_vocabulary VALUES("3227","de","_SWITCH_TO_EXPORT","Schalter für den Export");
INSERT INTO phpn_vocabulary VALUES("3228","en","_SWITCH_TO_EXPORT","Switch to Export");
INSERT INTO phpn_vocabulary VALUES("3229","es","_SWITCH_TO_EXPORT","Cambiar a la exportación");
INSERT INTO phpn_vocabulary VALUES("3230","de","_SWITCH_TO_NORMAL","Wechseln Sie zur Normalansicht");
INSERT INTO phpn_vocabulary VALUES("3231","en","_SWITCH_TO_NORMAL","Switch to Normal");
INSERT INTO phpn_vocabulary VALUES("3232","es","_SWITCH_TO_NORMAL","Cambiar a la normal");
INSERT INTO phpn_vocabulary VALUES("3233","de","_SYMBOL","Symbol");
INSERT INTO phpn_vocabulary VALUES("3234","en","_SYMBOL","Symbol");
INSERT INTO phpn_vocabulary VALUES("3235","es","_SYMBOL","Símbolo");
INSERT INTO phpn_vocabulary VALUES("3236","de","_SYMBOL_PLACEMENT","Symbolplatzierung");
INSERT INTO phpn_vocabulary VALUES("3237","en","_SYMBOL_PLACEMENT","Symbol Placement");
INSERT INTO phpn_vocabulary VALUES("3238","es","_SYMBOL_PLACEMENT","Ubicación de Símbolo");
INSERT INTO phpn_vocabulary VALUES("3239","de","_SYSTEM","System");
INSERT INTO phpn_vocabulary VALUES("3240","en","_SYSTEM","System");
INSERT INTO phpn_vocabulary VALUES("3241","es","_SYSTEM","Sistema");
INSERT INTO phpn_vocabulary VALUES("3242","de","_SYSTEM_EMAIL_DELETE_ALERT","Diese E-Mail-Vorlage wird durch das System verwendet und kann nicht gelöscht werden!");
INSERT INTO phpn_vocabulary VALUES("3243","en","_SYSTEM_EMAIL_DELETE_ALERT","This email template is used by the system and cannot be deleted!");
INSERT INTO phpn_vocabulary VALUES("3244","es","_SYSTEM_EMAIL_DELETE_ALERT","Esta plantilla de correo electrónico es utilizado por el sistema y no se puede eliminar!");
INSERT INTO phpn_vocabulary VALUES("3245","de","_SYSTEM_MODULE","System-Modul");
INSERT INTO phpn_vocabulary VALUES("3246","en","_SYSTEM_MODULE","System Module");
INSERT INTO phpn_vocabulary VALUES("3247","es","_SYSTEM_MODULE","Módulo del Sistema");
INSERT INTO phpn_vocabulary VALUES("3248","de","_SYSTEM_MODULES","System-Module");
INSERT INTO phpn_vocabulary VALUES("3249","en","_SYSTEM_MODULES","System Modules");
INSERT INTO phpn_vocabulary VALUES("3250","es","_SYSTEM_MODULES","Sistema de Módulos");
INSERT INTO phpn_vocabulary VALUES("3251","de","_SYSTEM_MODULE_ACTIONS_BLOCKED","Alle Operationen mit System-Modul sind gesperrt!");
INSERT INTO phpn_vocabulary VALUES("3252","en","_SYSTEM_MODULE_ACTIONS_BLOCKED","All operations with system module are blocked!");
INSERT INTO phpn_vocabulary VALUES("3253","es","_SYSTEM_MODULE_ACTIONS_BLOCKED","Todas las operaciones con el módulo del sistema están bloqueados!");
INSERT INTO phpn_vocabulary VALUES("3254","de","_SYSTEM_TEMPLATE","System Vorlage");
INSERT INTO phpn_vocabulary VALUES("3255","en","_SYSTEM_TEMPLATE","System Template");
INSERT INTO phpn_vocabulary VALUES("3256","es","_SYSTEM_TEMPLATE","Sistema de plantillas");
INSERT INTO phpn_vocabulary VALUES("3257","de","_TAG","Begriffe");
INSERT INTO phpn_vocabulary VALUES("3258","en","_TAG","Tag");
INSERT INTO phpn_vocabulary VALUES("3259","es","_TAG","Tag");
INSERT INTO phpn_vocabulary VALUES("3260","de","_TAG_TITLE_IS_EMPTY","&lt;TITLE&gt; Tag nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3261","en","_TAG_TITLE_IS_EMPTY","Tag &lt;TITLE&gt; cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3262","es","_TAG_TITLE_IS_EMPTY","&lt;Title&gt; no puede estar vacío! Por favor, introduzca");
INSERT INTO phpn_vocabulary VALUES("3263","de","_TARGET","Ziel");
INSERT INTO phpn_vocabulary VALUES("3264","en","_TARGET","Target");
INSERT INTO phpn_vocabulary VALUES("3265","es","_TARGET","Meta");
INSERT INTO phpn_vocabulary VALUES("3266","de","_TARGET_GROUP","Zielgruppe");
INSERT INTO phpn_vocabulary VALUES("3267","en","_TARGET_GROUP","Target Group");
INSERT INTO phpn_vocabulary VALUES("3268","es","_TARGET_GROUP","Grupo Objetivo");
INSERT INTO phpn_vocabulary VALUES("3269","de","_TAXES","Steuern");
INSERT INTO phpn_vocabulary VALUES("3270","en","_TAXES","Taxes");
INSERT INTO phpn_vocabulary VALUES("3271","es","_TAXES","Impuestos");
INSERT INTO phpn_vocabulary VALUES("3272","de","_TEMPLATES_STYLES","Templates &amp; Styles");
INSERT INTO phpn_vocabulary VALUES("3273","en","_TEMPLATES_STYLES","Templates & Styles");
INSERT INTO phpn_vocabulary VALUES("3274","es","_TEMPLATES_STYLES","Plantillas y Estilos");
INSERT INTO phpn_vocabulary VALUES("3275","de","_TEMPLATE_CODE","Template-Code");
INSERT INTO phpn_vocabulary VALUES("3276","en","_TEMPLATE_CODE","Template Code");
INSERT INTO phpn_vocabulary VALUES("3277","es","_TEMPLATE_CODE","Plantilla Código");
INSERT INTO phpn_vocabulary VALUES("3278","de","_TEMPLATE_IS_EMPTY","Vorlage kann nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3279","en","_TEMPLATE_IS_EMPTY","Template cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3280","es","_TEMPLATE_IS_EMPTY","Plantilla no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3281","de","_TERMS","Allgemeine Geschäftsbedingungen");
INSERT INTO phpn_vocabulary VALUES("3282","en","_TERMS","Terms & Conditions");
INSERT INTO phpn_vocabulary VALUES("3283","es","_TERMS","Términos y Condiciones");
INSERT INTO phpn_vocabulary VALUES("3284","de","_REVIEWS","Bewertungen");
INSERT INTO phpn_vocabulary VALUES("3285","en","_REVIEWS","Reviews");
INSERT INTO phpn_vocabulary VALUES("3286","es","_REVIEWS","Opiniones");
INSERT INTO phpn_vocabulary VALUES("3287","de","_REVIEWS_MANAGEMENT","Bewertungen Verwaltung");
INSERT INTO phpn_vocabulary VALUES("3288","en","_REVIEWS_MANAGEMENT","Reviews Management");
INSERT INTO phpn_vocabulary VALUES("3289","es","_REVIEWS_MANAGEMENT","Los comentarios de Gestión");
INSERT INTO phpn_vocabulary VALUES("3293","de","_TEST_EMAIL","Test-E-Mail");
INSERT INTO phpn_vocabulary VALUES("3294","en","_TEST_EMAIL","Test Email");
INSERT INTO phpn_vocabulary VALUES("3295","es","_TEST_EMAIL","Prueba de correo electrónico");
INSERT INTO phpn_vocabulary VALUES("3296","de","_TEST_MODE_ALERT","Testmodus Reservierung Warenkorb ist eingeschaltet! So ändern Sie aktuelle Modus klicken <a href=index.php?admin=mod_booking_settings>Sie hier</a>.");
INSERT INTO phpn_vocabulary VALUES("3297","en","_TEST_MODE_ALERT","Test Mode in Reservation Cart is turned ON! To change current mode click <a href=index.php?admin=mod_booking_settings>here</a>.");
INSERT INTO phpn_vocabulary VALUES("3298","es","_TEST_MODE_ALERT","Modo de prueba en la cesta de reserva se enciende! Para cambiar <a actual, haga clic en el modo de href=index.php?admin=mod_booking_settings>aquí</a>.");
INSERT INTO phpn_vocabulary VALUES("3299","de","_TEST_MODE_ALERT_SHORT","Achtung: Reservierung Warenkorb ist im Test-Modus laufen!");
INSERT INTO phpn_vocabulary VALUES("3300","en","_TEST_MODE_ALERT_SHORT","Attention: Reservation Cart is running in Test Mode!");
INSERT INTO phpn_vocabulary VALUES("3301","es","_TEST_MODE_ALERT_SHORT","Atención: La reserva de compra se está ejecutando en modo de prueba!");
INSERT INTO phpn_vocabulary VALUES("3302","de","_TEXT","Text");
INSERT INTO phpn_vocabulary VALUES("3303","en","_TEXT","Text");
INSERT INTO phpn_vocabulary VALUES("3304","es","_TEXT","Texto");
INSERT INTO phpn_vocabulary VALUES("3305","de","_TH","Do");
INSERT INTO phpn_vocabulary VALUES("3306","en","_TH","Th");
INSERT INTO phpn_vocabulary VALUES("3307","es","_TH","Ju");
INSERT INTO phpn_vocabulary VALUES("3308","de","_THU","Don");
INSERT INTO phpn_vocabulary VALUES("3309","en","_THU","Thu");
INSERT INTO phpn_vocabulary VALUES("3310","es","_THU","Jue");
INSERT INTO phpn_vocabulary VALUES("3311","de","_THUMBNAIL","Vorschaubild");
INSERT INTO phpn_vocabulary VALUES("3312","en","_THUMBNAIL","Thumbnail");
INSERT INTO phpn_vocabulary VALUES("3313","es","_THUMBNAIL","Miniatura");
INSERT INTO phpn_vocabulary VALUES("3314","de","_THURSDAY","Donnerstag");
INSERT INTO phpn_vocabulary VALUES("3315","en","_THURSDAY","Thursday");
INSERT INTO phpn_vocabulary VALUES("3316","es","_THURSDAY","jueves");
INSERT INTO phpn_vocabulary VALUES("3317","de","_TIME_PERIOD_OVERLAPPING_ALERT","Dieser Zeitraum (vollständig oder teilweise) wurde bereits ausgewählt! Bitte wählen Sie einen anderen.");
INSERT INTO phpn_vocabulary VALUES("3318","en","_TIME_PERIOD_OVERLAPPING_ALERT","This period of time (fully or partially) is already selected! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("3319","es","_TIME_PERIOD_OVERLAPPING_ALERT","Este período de tiempo (total o parcialmente) fue seleccionado ya! Por favor elija otro.");
INSERT INTO phpn_vocabulary VALUES("3320","de","_TIME_ZONE","Time Zone");
INSERT INTO phpn_vocabulary VALUES("3321","en","_TIME_ZONE","Time Zone");
INSERT INTO phpn_vocabulary VALUES("3322","es","_TIME_ZONE","Zona horaria");
INSERT INTO phpn_vocabulary VALUES("3323","de","_TO","auf");
INSERT INTO phpn_vocabulary VALUES("3324","en","_TO","To");
INSERT INTO phpn_vocabulary VALUES("3325","es","_TO","A");
INSERT INTO phpn_vocabulary VALUES("3326","de","_TODAY","Heute");
INSERT INTO phpn_vocabulary VALUES("3327","en","_TODAY","Today");
INSERT INTO phpn_vocabulary VALUES("3328","es","_TODAY","Hoy");
INSERT INTO phpn_vocabulary VALUES("3329","de","_TOP","Oben");
INSERT INTO phpn_vocabulary VALUES("3330","en","_TOP","Top");
INSERT INTO phpn_vocabulary VALUES("3331","es","_TOP","Superior");
INSERT INTO phpn_vocabulary VALUES("3332","de","_TOP_PANEL","Oberseite");
INSERT INTO phpn_vocabulary VALUES("3333","en","_TOP_PANEL","Top Panel");
INSERT INTO phpn_vocabulary VALUES("3334","es","_TOP_PANEL","Panel superior");
INSERT INTO phpn_vocabulary VALUES("3335","de","_TOTAL","Insgesamt");
INSERT INTO phpn_vocabulary VALUES("3336","en","_TOTAL","Total");
INSERT INTO phpn_vocabulary VALUES("3337","es","_TOTAL","Total");
INSERT INTO phpn_vocabulary VALUES("3338","de","_TOTAL_PRICE","Gesamtpreis");
INSERT INTO phpn_vocabulary VALUES("3339","en","_TOTAL_PRICE","Total Price");
INSERT INTO phpn_vocabulary VALUES("3340","es","_TOTAL_PRICE","Precio Total");
INSERT INTO phpn_vocabulary VALUES("3341","de","_TOTAL_ROOMS","Anzahl der Zimmer");
INSERT INTO phpn_vocabulary VALUES("3342","en","_TOTAL_ROOMS","Total Rooms");
INSERT INTO phpn_vocabulary VALUES("3343","es","_TOTAL_ROOMS","Habitaciones del hotel");
INSERT INTO phpn_vocabulary VALUES("3344","de","_TRANSACTION","Transaktion");
INSERT INTO phpn_vocabulary VALUES("3345","en","_TRANSACTION","Transaction");
INSERT INTO phpn_vocabulary VALUES("3346","es","_TRANSACTION","Transacción");
INSERT INTO phpn_vocabulary VALUES("3347","de","_TRANSLATE_VIA_GOOGLE","Übersetzen mit Google");
INSERT INTO phpn_vocabulary VALUES("3348","en","_TRANSLATE_VIA_GOOGLE","Translate via Google");
INSERT INTO phpn_vocabulary VALUES("3349","es","_TRANSLATE_VIA_GOOGLE","Traducir a través de Google");
INSERT INTO phpn_vocabulary VALUES("3350","de","_TRASH","Müll");
INSERT INTO phpn_vocabulary VALUES("3351","en","_TRASH","Trash");
INSERT INTO phpn_vocabulary VALUES("3352","es","_TRASH","Basura");
INSERT INTO phpn_vocabulary VALUES("3353","de","_TRASH_PAGES","Trash Seiten");
INSERT INTO phpn_vocabulary VALUES("3354","en","_TRASH_PAGES","Trash Pages");
INSERT INTO phpn_vocabulary VALUES("3355","es","_TRASH_PAGES","Papelera Páginas");
INSERT INTO phpn_vocabulary VALUES("3356","de","_TRUNCATE_RELATED_TABLES","Truncate verknüpften Tabellen?");
INSERT INTO phpn_vocabulary VALUES("3357","en","_TRUNCATE_RELATED_TABLES","Truncate related tables?");
INSERT INTO phpn_vocabulary VALUES("3358","es","_TRUNCATE_RELATED_TABLES","Truncar tablas relacionadas?");
INSERT INTO phpn_vocabulary VALUES("3359","de","_TRY_LATER","Ein Fehler Trat während der ausführung der Operation. Bitte versuchen Sie Es Noch Einmal später!");
INSERT INTO phpn_vocabulary VALUES("3360","en","_TRY_LATER","An error occurred while executing. Please try again later!");
INSERT INTO phpn_vocabulary VALUES("3361","es","_TRY_LATER","Se produjo un error mientras que la ejecución. Por favor, inténtelo de nuevo más tarde!");
INSERT INTO phpn_vocabulary VALUES("3362","de","_TRY_SYSTEM_SUGGESTION","Testen Sie Systems Vorschlag");
INSERT INTO phpn_vocabulary VALUES("3363","en","_TRY_SYSTEM_SUGGESTION","Try out system suggestion");
INSERT INTO phpn_vocabulary VALUES("3364","es","_TRY_SYSTEM_SUGGESTION","Pruebe la sugerencia del sistema");
INSERT INTO phpn_vocabulary VALUES("3365","de","_TU","Di");
INSERT INTO phpn_vocabulary VALUES("3366","en","_TU","Tu");
INSERT INTO phpn_vocabulary VALUES("3367","es","_TU","Ma");
INSERT INTO phpn_vocabulary VALUES("3368","de","_TUE","Die");
INSERT INTO phpn_vocabulary VALUES("3369","en","_TUE","Tue");
INSERT INTO phpn_vocabulary VALUES("3370","es","_TUE","Mar");
INSERT INTO phpn_vocabulary VALUES("3371","de","_TUESDAY","Dienstag");
INSERT INTO phpn_vocabulary VALUES("3372","en","_TUESDAY","Tuesday");
INSERT INTO phpn_vocabulary VALUES("3373","es","_TUESDAY","martes");
INSERT INTO phpn_vocabulary VALUES("3374","de","_TYPE","Typ");
INSERT INTO phpn_vocabulary VALUES("3375","en","_TYPE","Type");
INSERT INTO phpn_vocabulary VALUES("3376","es","_TYPE","Tipo");
INSERT INTO phpn_vocabulary VALUES("3377","de","_TYPE_CHARS","Geben Sie die Zeichen im Bild sehen");
INSERT INTO phpn_vocabulary VALUES("3378","en","_TYPE_CHARS","Type the characters you see in the picture");
INSERT INTO phpn_vocabulary VALUES("3379","es","_TYPE_CHARS","Escriba los caracteres que ve en la imagen");
INSERT INTO phpn_vocabulary VALUES("3380","de","_UNCATEGORIZED","Uncategorized");
INSERT INTO phpn_vocabulary VALUES("3381","en","_UNCATEGORIZED","Uncategorized");
INSERT INTO phpn_vocabulary VALUES("3382","es","_UNCATEGORIZED","Hasta");
INSERT INTO phpn_vocabulary VALUES("3383","de","_UNDEFINED","undefiniert");
INSERT INTO phpn_vocabulary VALUES("3384","en","_UNDEFINED","undefined");
INSERT INTO phpn_vocabulary VALUES("3385","es","_UNDEFINED","indefinido");
INSERT INTO phpn_vocabulary VALUES("3386","de","_UNINSTALL","Deinstallieren");
INSERT INTO phpn_vocabulary VALUES("3387","en","_UNINSTALL","Uninstall");
INSERT INTO phpn_vocabulary VALUES("3388","es","_UNINSTALL","Desinstalar");
INSERT INTO phpn_vocabulary VALUES("3389","de","_UNITS","Einheiten");
INSERT INTO phpn_vocabulary VALUES("3390","en","_UNITS","Units");
INSERT INTO phpn_vocabulary VALUES("3391","es","_UNITS","Unidades");
INSERT INTO phpn_vocabulary VALUES("3392","de","_UNIT_PRICE","Einheitspreis");
INSERT INTO phpn_vocabulary VALUES("3393","en","_UNIT_PRICE","Unit Price");
INSERT INTO phpn_vocabulary VALUES("3394","es","_UNIT_PRICE","Precio unitario");
INSERT INTO phpn_vocabulary VALUES("3395","de","_UNKNOWN","Unbekannt");
INSERT INTO phpn_vocabulary VALUES("3396","en","_UNKNOWN","Unknown");
INSERT INTO phpn_vocabulary VALUES("3397","es","_UNKNOWN","Desconocido");
INSERT INTO phpn_vocabulary VALUES("3398","de","_UNSUBSCRIBE","Unsubscribe");
INSERT INTO phpn_vocabulary VALUES("3399","en","_UNSUBSCRIBE","Unsubscribe");
INSERT INTO phpn_vocabulary VALUES("3400","es","_UNSUBSCRIBE","Darse de baja");
INSERT INTO phpn_vocabulary VALUES("3401","de","_UP","Bis");
INSERT INTO phpn_vocabulary VALUES("3402","en","_UP","Up");
INSERT INTO phpn_vocabulary VALUES("3403","es","_UP","Arriba");
INSERT INTO phpn_vocabulary VALUES("3404","de","_UPDATING_ACCOUNT","Aktualisierung von Kontoinformationen");
INSERT INTO phpn_vocabulary VALUES("3405","en","_UPDATING_ACCOUNT","Updating Account");
INSERT INTO phpn_vocabulary VALUES("3406","es","_UPDATING_ACCOUNT","Actualización de la cuenta");
INSERT INTO phpn_vocabulary VALUES("3407","de","_UPDATING_ACCOUNT_ERROR","Fehler beim Aktualisieren Sie Ihr Konto! Bitte versuchen Sie es später noch einmal oder senden Sie Informationen zu diesem Fehler zur Verwaltung der Website.");
INSERT INTO phpn_vocabulary VALUES("3408","en","_UPDATING_ACCOUNT_ERROR","An error occurred while updating your account! Please try again later or send information about this error to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("3409","es","_UPDATING_ACCOUNT_ERROR","Se produjo un error al actualizar su cuenta! Por favor, inténtelo de nuevo más tarde o enviar información sobre este error a la administración del sitio.");
INSERT INTO phpn_vocabulary VALUES("3410","de","_UPDATING_OPERATION_COMPLETED","Aktualisierung ist erfolgreich abgeschlossen!");
INSERT INTO phpn_vocabulary VALUES("3411","en","_UPDATING_OPERATION_COMPLETED","Updating operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("3412","es","_UPDATING_OPERATION_COMPLETED","Actualización de la operación se completó con éxito!");
INSERT INTO phpn_vocabulary VALUES("3413","de","_UPLOAD","Upload");
INSERT INTO phpn_vocabulary VALUES("3414","en","_UPLOAD","Upload");
INSERT INTO phpn_vocabulary VALUES("3415","es","_UPLOAD","Cargar");
INSERT INTO phpn_vocabulary VALUES("3416","de","_UPLOAD_AND_PROCCESS","Upload-und Prozessmanagement");
INSERT INTO phpn_vocabulary VALUES("3417","en","_UPLOAD_AND_PROCCESS","Upload and Process");
INSERT INTO phpn_vocabulary VALUES("3418","es","_UPLOAD_AND_PROCCESS","Subir y Proceso");
INSERT INTO phpn_vocabulary VALUES("3419","de","_UPLOAD_FROM_FILE","Upload von Datei");
INSERT INTO phpn_vocabulary VALUES("3420","en","_UPLOAD_FROM_FILE","Upload from File");
INSERT INTO phpn_vocabulary VALUES("3421","es","_UPLOAD_FROM_FILE","Cargar desde archivo");
INSERT INTO phpn_vocabulary VALUES("3422","de","_URL","URL");
INSERT INTO phpn_vocabulary VALUES("3423","en","_URL","URL");
INSERT INTO phpn_vocabulary VALUES("3424","es","_URL","URL");
INSERT INTO phpn_vocabulary VALUES("3425","de","_USED_ON","Verwendet auf");
INSERT INTO phpn_vocabulary VALUES("3426","en","_USED_ON","Used On");
INSERT INTO phpn_vocabulary VALUES("3427","es","_USED_ON","Se utiliza en");
INSERT INTO phpn_vocabulary VALUES("3428","de","_USERNAME","Benutzername");
INSERT INTO phpn_vocabulary VALUES("3429","en","_USERNAME","Username");
INSERT INTO phpn_vocabulary VALUES("3430","es","_USERNAME","Nombre de usuario");
INSERT INTO phpn_vocabulary VALUES("3431","de","_USERNAME_AND_PASSWORD","Benutzername und Passwort");
INSERT INTO phpn_vocabulary VALUES("3432","en","_USERNAME_AND_PASSWORD","Username & Password");
INSERT INTO phpn_vocabulary VALUES("3433","es","_USERNAME_AND_PASSWORD","Nombre de usuario y contraseña");
INSERT INTO phpn_vocabulary VALUES("3434","de","_USERNAME_EMPTY_ALERT","Benutzername darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3435","en","_USERNAME_EMPTY_ALERT","Username cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3436","es","_USERNAME_EMPTY_ALERT","Nombre de usuario no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3437","de","_USERNAME_LENGTH_ALERT","Die Länge der Benutzername darf nicht weniger als 6 Zeichen! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3438","en","_USERNAME_LENGTH_ALERT","The length of username cannot be less than 4 characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3439","es","_USERNAME_LENGTH_ALERT","La longitud de nombre de usuario no puede ser menos de 4 caracteres! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3440","de","_USERS","Benutzer");
INSERT INTO phpn_vocabulary VALUES("3441","en","_USERS","Users");
INSERT INTO phpn_vocabulary VALUES("3442","es","_USERS","Usuarios");
INSERT INTO phpn_vocabulary VALUES("3443","de","_USER_EMAIL_EXISTS_ALERT","User mit solchen E-Mail ist bereits vorhanden! Bitte wählen Sie einen anderen.");
INSERT INTO phpn_vocabulary VALUES("3444","en","_USER_EMAIL_EXISTS_ALERT","User with such email already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("3445","es","_USER_EMAIL_EXISTS_ALERT","Usuario con el correo electrónico ya existe! Por favor seleccione otro.");
INSERT INTO phpn_vocabulary VALUES("3446","de","_USER_EXISTS_ALERT","User mit solchen Benutzername existiert bereits! Bitte wählen Sie einen anderen.");
INSERT INTO phpn_vocabulary VALUES("3447","en","_USER_EXISTS_ALERT","User with such username already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("3448","es","_USER_EXISTS_ALERT","Usuario con nombre de usuario ya existe! Por favor seleccione otro.");
INSERT INTO phpn_vocabulary VALUES("3449","de","_USER_NAME","Benutzername");
INSERT INTO phpn_vocabulary VALUES("3450","en","_USER_NAME","User name");
INSERT INTO phpn_vocabulary VALUES("3451","es","_USER_NAME","Nombre de usuario");
INSERT INTO phpn_vocabulary VALUES("3452","de","_USE_THIS_PASSWORD","Verwenden Sie dieses Kennwort");
INSERT INTO phpn_vocabulary VALUES("3453","en","_USE_THIS_PASSWORD","Use this password");
INSERT INTO phpn_vocabulary VALUES("3454","es","_USE_THIS_PASSWORD","Utilice esta contraseña");
INSERT INTO phpn_vocabulary VALUES("3455","de","_VALUE","Wert");
INSERT INTO phpn_vocabulary VALUES("3456","en","_VALUE","Value");
INSERT INTO phpn_vocabulary VALUES("3457","es","_VALUE","Valor");
INSERT INTO phpn_vocabulary VALUES("3458","de","_VAT","MwSt.");
INSERT INTO phpn_vocabulary VALUES("3459","en","_VAT","VAT");
INSERT INTO phpn_vocabulary VALUES("3460","es","_VAT","IVA");
INSERT INTO phpn_vocabulary VALUES("3461","de","_VAT_PERCENT","MwSt. Prozent");
INSERT INTO phpn_vocabulary VALUES("3462","en","_VAT_PERCENT","VAT Percent");
INSERT INTO phpn_vocabulary VALUES("3463","es","_VAT_PERCENT","Porcentaje del IVA");
INSERT INTO phpn_vocabulary VALUES("3464","de","_VERSION","Version");
INSERT INTO phpn_vocabulary VALUES("3465","en","_VERSION","Version");
INSERT INTO phpn_vocabulary VALUES("3466","es","_VERSION","Versión");
INSERT INTO phpn_vocabulary VALUES("3467","de","_VIDEO","Video");
INSERT INTO phpn_vocabulary VALUES("3468","en","_VIDEO","Video");
INSERT INTO phpn_vocabulary VALUES("3469","es","_VIDEO","Video");
INSERT INTO phpn_vocabulary VALUES("3470","de","_VIEW_ALL","Alle anzeigen");
INSERT INTO phpn_vocabulary VALUES("3471","en","_VIEW_ALL","View All");
INSERT INTO phpn_vocabulary VALUES("3472","es","_VIEW_ALL","Ver todos");
INSERT INTO phpn_vocabulary VALUES("3473","de","_VIEW_WORD","Blick");
INSERT INTO phpn_vocabulary VALUES("3474","en","_VIEW_WORD","View");
INSERT INTO phpn_vocabulary VALUES("3475","es","_VIEW_WORD","Ver");
INSERT INTO phpn_vocabulary VALUES("3476","de","_VISITOR","Besucher");
INSERT INTO phpn_vocabulary VALUES("3477","en","_VISITOR","Visitor");
INSERT INTO phpn_vocabulary VALUES("3478","es","_VISITOR","Visitante");
INSERT INTO phpn_vocabulary VALUES("3479","de","_VISITORS_RATING","Besucher Bewertung");
INSERT INTO phpn_vocabulary VALUES("3480","en","_VISITORS_RATING","Visitors Rating");
INSERT INTO phpn_vocabulary VALUES("3481","es","_VISITORS_RATING","Ustedes evaluación");
INSERT INTO phpn_vocabulary VALUES("3482","de","_VISUAL_SETTINGS","Visuelle Einstellungen");
INSERT INTO phpn_vocabulary VALUES("3483","en","_VISUAL_SETTINGS","Visual Settings");
INSERT INTO phpn_vocabulary VALUES("3484","es","_VISUAL_SETTINGS","Ajustes Visual");
INSERT INTO phpn_vocabulary VALUES("3485","de","_VOCABULARY","wortschatz");
INSERT INTO phpn_vocabulary VALUES("3486","en","_VOCABULARY","Vocabulary");
INSERT INTO phpn_vocabulary VALUES("3487","es","_VOCABULARY","Vocabulario");
INSERT INTO phpn_vocabulary VALUES("3488","de","_VOC_KEYS_UPDATED","Operation wurde erfolgreich abgeschlossen. Aktualisiert: _KEYS_ Tasten. Klicken Sie <a href=\'index.php?admin=vocabulary&filter_by=A\'>hier</a>, um Website aktualisieren.");
INSERT INTO phpn_vocabulary VALUES("3489","en","_VOC_KEYS_UPDATED","Operation has been successfully completed. Updated: _KEYS_ keys. Click <a href=\'index.php?admin=vocabulary&filter_by=A\'>here</a> to refresh the site.");
INSERT INTO phpn_vocabulary VALUES("3490","es","_VOC_KEYS_UPDATED","La operación se completó con éxito. Actualización: claves _KEYS_. Haga clic en href=\'index.php?admin=vocabulary&filter_by=A\'>aquí</a> para volver a cargar el sitio.");
INSERT INTO phpn_vocabulary VALUES("3491","de","_VOC_KEY_UPDATED","Wortschatz Schlüssel wurde erfolgreich aktualisiert.");
INSERT INTO phpn_vocabulary VALUES("3492","en","_VOC_KEY_UPDATED","Vocabulary key has been successfully updated.");
INSERT INTO phpn_vocabulary VALUES("3493","es","_VOC_KEY_UPDATED","Vocabulario clave se ha actualizado correctamente.");
INSERT INTO phpn_vocabulary VALUES("3494","de","_VOC_KEY_VALUE_EMPTY","Key-Wert darf nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3495","en","_VOC_KEY_VALUE_EMPTY","Key value cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3496","es","_VOC_KEY_VALUE_EMPTY","Valor de la clave no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3497","de","_VOC_NOT_FOUND","Kein Schlüssel gefunden");
INSERT INTO phpn_vocabulary VALUES("3498","en","_VOC_NOT_FOUND","No keys found");
INSERT INTO phpn_vocabulary VALUES("3499","es","_VOC_NOT_FOUND","No hay teclas que se encuentran");
INSERT INTO phpn_vocabulary VALUES("3500","de","_VOC_UPDATED","Wortschatz wurde erfolgreich aktualisiert. Klicken Sie <a href=index.php>hier</a>");
INSERT INTO phpn_vocabulary VALUES("3501","en","_VOC_UPDATED","Vocabulary has been successfully updated. Click <a href=index.php>here</a> to refresh the site.");
INSERT INTO phpn_vocabulary VALUES("3502","es","_VOC_UPDATED","Vocabulario se ha actualizado correctamente. Haga clic <a href=index.php>aquí</a> para actualizar el sitio.");
INSERT INTO phpn_vocabulary VALUES("3503","de","_VOTE_NOT_REGISTERED","Ihre Stimme wurde nicht registriert! Sie müssen, bevor Sie abstimmen können protokolliert werden.");
INSERT INTO phpn_vocabulary VALUES("3504","en","_VOTE_NOT_REGISTERED","Your vote has not been registered! You must be logged in before you can vote.");
INSERT INTO phpn_vocabulary VALUES("3505","es","_VOTE_NOT_REGISTERED","Su voto no se ha registrado? Debe estar registrado para poder votar.");
INSERT INTO phpn_vocabulary VALUES("3506","de","_WE","Mi");
INSERT INTO phpn_vocabulary VALUES("3507","en","_WE","We");
INSERT INTO phpn_vocabulary VALUES("3508","es","_WE","Mi");
INSERT INTO phpn_vocabulary VALUES("3509","de","_WEB_SITE","Web-Site");
INSERT INTO phpn_vocabulary VALUES("3510","en","_WEB_SITE","Web Site");
INSERT INTO phpn_vocabulary VALUES("3511","es","_WEB_SITE","Sitio Web");
INSERT INTO phpn_vocabulary VALUES("3512","de","_WED","Mit");
INSERT INTO phpn_vocabulary VALUES("3513","en","_WED","Wed");
INSERT INTO phpn_vocabulary VALUES("3514","es","_WED","Mié");
INSERT INTO phpn_vocabulary VALUES("3515","de","_WEDNESDAY","Mittwoch");
INSERT INTO phpn_vocabulary VALUES("3516","en","_WEDNESDAY","Wednesday");
INSERT INTO phpn_vocabulary VALUES("3517","es","_WEDNESDAY","miércoles");
INSERT INTO phpn_vocabulary VALUES("3518","de","_WEEK_START_DAY","Woche Starttag");
INSERT INTO phpn_vocabulary VALUES("3519","en","_WEEK_START_DAY","Week Start Day");
INSERT INTO phpn_vocabulary VALUES("3520","es","_WEEK_START_DAY","Semana Día de inicio");
INSERT INTO phpn_vocabulary VALUES("3521","de","_WELCOME_CUSTOMER_TEXT","<p>Hallo <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>\n<p>Willkommen auf Kundenkonto-Panel, die Sie verändern den Status anzeigen können, verwalten Sie Ihre Account-Einstellungen und Buchungen.</p>\n<p>\n    _TODAY_ <br />\n    _LAST_LOGIN_\n</p>\n<p><b>&#8226;</b> Um dieses Konto Zusammenfassung nur auf einem klicken <a href=\'index.php?customer=home\'>Armaturenbrett</a> link.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Mein Konto bearbeiten</a> Menü können Sie Ihre persönlichen Daten und Kontodaten zu ändern.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>Meine Buchungen</a> enthält Informationen über Ihre Bestellungen.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("3522","en","_WELCOME_CUSTOMER_TEXT","<p>Hello <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>        \n<p>Welcome to Customer Account Panel, that allows you to view account status, manage your account settings and bookings.</p>\n<p>\n   _TODAY_<br />\n   _LAST_LOGIN_\n</p>				\n<p> <b>&#8226;</b> To view this account summary just click on a <a href=\'index.php?customer=home\'>Dashboard</a> link.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Edit My Account</a> menu allows you to change your personal info and account data.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>My Bookings</a> contains information about your orders.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("3523","es","_WELCOME_CUSTOMER_TEXT","<p>Hola <b>_FIRST_NAME_ _LAST_NAME_</b></p>\n<p>Bienvenido a la cuenta del cliente, de que le permite ver el estado de cuenta de gestión de la configuración de su cuenta y reservas.</p>\n<p>\n   _TODAY_ <br />\n   _LAST_LOGIN_\n</P>\n<p><b>&#8226;</b> Para ver este resumen de la cuenta basta con hacer clic en una <a href=\'index.php?customer=home\'>Salpicadero</a> enlace.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Editar Mi Cuenta</a> menú le permite cambiar su información personal y datos de la cuenta.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>Mi Reserva</a> contiene información acerca de sus pedidos.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("3524","de","_WHAT_IS_CVV","Was ist CVV");
INSERT INTO phpn_vocabulary VALUES("3525","en","_WHAT_IS_CVV","What is CVV");
INSERT INTO phpn_vocabulary VALUES("3526","es","_WHAT_IS_CVV","¿Qué es el CVV");
INSERT INTO phpn_vocabulary VALUES("3527","de","_WHOLE_SITE","Ganze Web-Site");
INSERT INTO phpn_vocabulary VALUES("3528","en","_WHOLE_SITE","Whole site");
INSERT INTO phpn_vocabulary VALUES("3529","es","_WHOLE_SITE","Todo el sitio web");
INSERT INTO phpn_vocabulary VALUES("3530","de","_WIDGET_INTEGRATION_MESSAGE","Sie können Hotel-Site Suchmaschine mit einem anderen bestehenden Website integrieren.");
INSERT INTO phpn_vocabulary VALUES("3531","en","_WIDGET_INTEGRATION_MESSAGE","You may integrate Hotel Site search engine with another existing web site.");
INSERT INTO phpn_vocabulary VALUES("3532","es","_WIDGET_INTEGRATION_MESSAGE","Puede integrar hotel buscador del sitio con otro sitio web existente.");
INSERT INTO phpn_vocabulary VALUES("3533","de","_WIDGET_INTEGRATION_MESSAGE_HINT","<b>Hinweis</b>: Um alle verfügbaren Hotels auflisten, lassen hsJsKey Wert leer oder geben hotel IDs durch Komma getrennt.");
INSERT INTO phpn_vocabulary VALUES("3534","en","_WIDGET_INTEGRATION_MESSAGE_HINT","<b>Hint</b>: To list all available hotels, leave hsJsKey value empty or enter hotel IDs separated by commas, for ex.: var hsHotelIDs = \"2,9,12\";");
INSERT INTO phpn_vocabulary VALUES("3535","es","_WIDGET_INTEGRATION_MESSAGE_HINT","<b>Hint</b>: Para una lista de todos los disponibles, deje el valor hsJsKey vacío o introducir ID de hoteles, separados por comas.");
INSERT INTO phpn_vocabulary VALUES("3536","de","_WITHOUT_ACCOUNT","ohne Konto");
INSERT INTO phpn_vocabulary VALUES("3537","en","_WITHOUT_ACCOUNT","without account");
INSERT INTO phpn_vocabulary VALUES("3538","es","_WITHOUT_ACCOUNT","sin tener en cuenta");
INSERT INTO phpn_vocabulary VALUES("3539","de","_WRONG_BOOKING_NUMBER","Die Buchungsnummer die Sie eingegeben haben wurde nicht gefunden! Bitte geben Sie eine gültige Buchungsnummer.");
INSERT INTO phpn_vocabulary VALUES("3540","en","_WRONG_BOOKING_NUMBER","The booking number you\'ve entered was not found! Please enter a valid booking number.");
INSERT INTO phpn_vocabulary VALUES("3541","es","_WRONG_BOOKING_NUMBER","El número de reserva que has introducido no se encuentra! Por favor, introduzca un número de reserva válida.");
INSERT INTO phpn_vocabulary VALUES("3542","de","_WRONG_CHECKOUT_DATE_ALERT","Falsches Datum ausgewählt! Bitte wählen Sie eine gültige Abreisedatum.");
INSERT INTO phpn_vocabulary VALUES("3543","en","_WRONG_CHECKOUT_DATE_ALERT","Wrong date selected! Please choose a valid check-out date.");
INSERT INTO phpn_vocabulary VALUES("3544","es","_WRONG_CHECKOUT_DATE_ALERT","Ha seleccionado la fecha! Por favor, elija una fecha válida de salida.");
INSERT INTO phpn_vocabulary VALUES("3545","de","_WRONG_CODE_ALERT","Leider war der Code");
INSERT INTO phpn_vocabulary VALUES("3546","en","_WRONG_CODE_ALERT","Sorry, the code you have entered is invalid! Please try again.");
INSERT INTO phpn_vocabulary VALUES("3547","es","_WRONG_CODE_ALERT","Lo sentimos, el código que ha introducido no es válido! Por favor, inténtelo de nuevo.");
INSERT INTO phpn_vocabulary VALUES("3548","de","_WRONG_CONFIRMATION_CODE","Falscher Bestätigungscode oder Ihre Anmeldung wurde bereits bestätigt!");
INSERT INTO phpn_vocabulary VALUES("3549","en","_WRONG_CONFIRMATION_CODE","Wrong confirmation code or your registration is already confirmed!");
INSERT INTO phpn_vocabulary VALUES("3550","es","_WRONG_CONFIRMATION_CODE","Código de confirmación incorrecto o su registro se confirmó ya!");
INSERT INTO phpn_vocabulary VALUES("3551","de","_WRONG_COUPON_CODE","Dieser Gutscheincode ist ungültig oder abgelaufen ist!");
INSERT INTO phpn_vocabulary VALUES("3552","en","_WRONG_COUPON_CODE","This coupon code is invalid or has expired!");
INSERT INTO phpn_vocabulary VALUES("3553","es","_WRONG_COUPON_CODE","Este código de cupón es válido o ha caducado!");
INSERT INTO phpn_vocabulary VALUES("3554","de","_WRONG_FILE_TYPE","Datei hochgeladen ist keine gültige PHP-Datei Wortschatz! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3555","en","_WRONG_FILE_TYPE","Uploaded file is not a valid PHP vocabulary file! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3556","es","_WRONG_FILE_TYPE","El archivo subido no es un archivo válido PHP vocabulario! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3557","de","_WRONG_LOGIN","Falscher Benutzername oder Passwort!");
INSERT INTO phpn_vocabulary VALUES("3558","en","_WRONG_LOGIN","Wrong username or password!");
INSERT INTO phpn_vocabulary VALUES("3559","es","_WRONG_LOGIN","Nombre de usuario o contraseña incorrecta!");
INSERT INTO phpn_vocabulary VALUES("3560","de","_WRONG_PARAMETER_PASSED","Falscher Parameter übergeben - kann nicht die gesamte Bedienung!");
INSERT INTO phpn_vocabulary VALUES("3561","en","_WRONG_PARAMETER_PASSED","Wrong parameters passed - cannot complete operation!");
INSERT INTO phpn_vocabulary VALUES("3562","es","_WRONG_PARAMETER_PASSED","Parámetros erróneos pasado - no puede completar la operación!");
INSERT INTO phpn_vocabulary VALUES("3563","de","_WYSIWYG_EDITOR","WYSIWYG-Editor");
INSERT INTO phpn_vocabulary VALUES("3564","en","_WYSIWYG_EDITOR","WYSIWYG Editor");
INSERT INTO phpn_vocabulary VALUES("3565","es","_WYSIWYG_EDITOR","Editor WYSIWYG");
INSERT INTO phpn_vocabulary VALUES("3566","de","_YEAR","Jahr");
INSERT INTO phpn_vocabulary VALUES("3567","en","_YEAR","Year");
INSERT INTO phpn_vocabulary VALUES("3568","es","_YEAR","Año");
INSERT INTO phpn_vocabulary VALUES("3569","de","_YES","Ja");
INSERT INTO phpn_vocabulary VALUES("3570","en","_YES","Yes");
INSERT INTO phpn_vocabulary VALUES("3571","es","_YES","Sí");
INSERT INTO phpn_vocabulary VALUES("3572","de","_YOUR_EMAIL","Ihre E-Mail");
INSERT INTO phpn_vocabulary VALUES("3573","en","_YOUR_EMAIL","Your Email");
INSERT INTO phpn_vocabulary VALUES("3574","es","_YOUR_EMAIL","Su correo electrónico");
INSERT INTO phpn_vocabulary VALUES("3575","de","_YOUR_NAME","Ihr Name");
INSERT INTO phpn_vocabulary VALUES("3576","en","_YOUR_NAME","Your Name");
INSERT INTO phpn_vocabulary VALUES("3577","es","_YOUR_NAME","Su nombre");
INSERT INTO phpn_vocabulary VALUES("3578","de","_YOU_ARE_LOGGED_AS","Sie sind angemeldet als");
INSERT INTO phpn_vocabulary VALUES("3579","en","_YOU_ARE_LOGGED_AS","You are logged in as");
INSERT INTO phpn_vocabulary VALUES("3580","es","_YOU_ARE_LOGGED_AS","Estás conectado como");
INSERT INTO phpn_vocabulary VALUES("3581","de","_ZIPCODE_EMPTY_ALERT","PLZ kann nicht leer sein! Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("3582","en","_ZIPCODE_EMPTY_ALERT","Zip/Postal code cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3583","es","_ZIPCODE_EMPTY_ALERT","Código postal no puede estar vacía! Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("3584","de","_ZIP_CODE","Postleitzahl");
INSERT INTO phpn_vocabulary VALUES("3585","en","_ZIP_CODE","Zip/Postal code");
INSERT INTO phpn_vocabulary VALUES("3586","es","_ZIP_CODE","Código Postal");
INSERT INTO phpn_vocabulary VALUES("3587","en","_PROPERTY_TYPE","Property Type");
INSERT INTO phpn_vocabulary VALUES("3588","es","_PROPERTY_TYPE","Tipo de Inmueble");
INSERT INTO phpn_vocabulary VALUES("3589","de","_PROPERTY_TYPE","Art der Immobilie");
INSERT INTO phpn_vocabulary VALUES("3590","en","_PROPERTY_TYPES","Property Types");
INSERT INTO phpn_vocabulary VALUES("3591","es","_PROPERTY_TYPES","Tipos de la característica");
INSERT INTO phpn_vocabulary VALUES("3592","de","_PROPERTY_TYPES","Objekt-Typen");
INSERT INTO phpn_vocabulary VALUES("3593","en","_VILLA","Villa");
INSERT INTO phpn_vocabulary VALUES("3594","es","_VILLA","Villa");
INSERT INTO phpn_vocabulary VALUES("3595","de","_VILLA","Villa");
INSERT INTO phpn_vocabulary VALUES("3596","en","_VILLAS","Villas");
INSERT INTO phpn_vocabulary VALUES("3597","es","_VILLAS","Villas");
INSERT INTO phpn_vocabulary VALUES("3598","de","_VILLAS","Villen");
INSERT INTO phpn_vocabulary VALUES("3599","en","_DETAILS","Details");
INSERT INTO phpn_vocabulary VALUES("3600","es","_DETAILS","Detalles");
INSERT INTO phpn_vocabulary VALUES("3601","de","_DETAILS","Details");
INSERT INTO phpn_vocabulary VALUES("3602","en","_FACILITIES_MANAGEMENT","Facilities Management");
INSERT INTO phpn_vocabulary VALUES("3603","es","_FACILITIES_MANAGEMENT","Gestión de Instalaciones");
INSERT INTO phpn_vocabulary VALUES("3604","de","_FACILITIES_MANAGEMENT","Anlagen-Management");
INSERT INTO phpn_vocabulary VALUES("3605","en","_HOTEL_FACILITIES","Hotel Facilities");
INSERT INTO phpn_vocabulary VALUES("3606","es","_HOTEL_FACILITIES","Instalaciones del hotel");
INSERT INTO phpn_vocabulary VALUES("3607","de","_HOTEL_FACILITIES","Hotelausstattung");
INSERT INTO phpn_vocabulary VALUES("3608","en","_FEATURED_OFFERS","Featured Offers");
INSERT INTO phpn_vocabulary VALUES("3609","es","_FEATURED_OFFERS","Ofertas destacadas");
INSERT INTO phpn_vocabulary VALUES("3610","de","_FEATURED_OFFERS","Besondere Angebote");
INSERT INTO phpn_vocabulary VALUES("3611","en","_FEATURED_OFFERS_WITH_BR","Featured<br>Offers");
INSERT INTO phpn_vocabulary VALUES("3612","es","_FEATURED_OFFERS_WITH_BR","Ofertas<br>destacadas");
INSERT INTO phpn_vocabulary VALUES("3613","de","_FEATURED_OFFERS_WITH_BR","Besondere<br>Angebote");
INSERT INTO phpn_vocabulary VALUES("3614","en","_FEATURED_OFFERS_TEXT","Start your search with a look at the best rates on our site.");
INSERT INTO phpn_vocabulary VALUES("3615","es","_FEATURED_OFFERS_TEXT","Comience su búsqueda con una mirada a las mejores tarifas en nuestro sitio.");
INSERT INTO phpn_vocabulary VALUES("3616","de","_FEATURED_OFFERS_TEXT","Beginnen Sie Ihre Suche mit einem Blick auf die besten Preise auf unserer Website.");
INSERT INTO phpn_vocabulary VALUES("3617","en","_TODAY_TOP_DEALS","Today\'s Top Deals");
INSERT INTO phpn_vocabulary VALUES("3618","es","_TODAY_TOP_DEALS","Top de Hoy Ofertas");
INSERT INTO phpn_vocabulary VALUES("3619","de","_TODAY_TOP_DEALS","Die heutigen Top Angebote");
INSERT INTO phpn_vocabulary VALUES("3620","en","_TODAY_TOP_DEALS_WITH_BR","Today\'s Top<br/>Deals");
INSERT INTO phpn_vocabulary VALUES("3621","es","_TODAY_TOP_DEALS_WITH_BR","Top de Hoy<br/>Ofertas");
INSERT INTO phpn_vocabulary VALUES("3622","de","_TODAY_TOP_DEALS_WITH_BR","Die heutigen Top<br/>Angebote");
INSERT INTO phpn_vocabulary VALUES("3623","en","_TODAY_TOP_DEALS_TEXT","Start your search with a look at the best rates on our site.");
INSERT INTO phpn_vocabulary VALUES("3624","es","_TODAY_TOP_DEALS_TEXT","Comience su búsqueda con una mirada a las mejores tarifas en nuestro sitio.");
INSERT INTO phpn_vocabulary VALUES("3625","de","_TODAY_TOP_DEALS_TEXT","Beginnen Sie Ihre Suche mit einem Blick auf die besten Preise auf unserer Website.");
INSERT INTO phpn_vocabulary VALUES("3626","en","_EARLY_BOOKING","Early Booking");
INSERT INTO phpn_vocabulary VALUES("3627","es","_EARLY_BOOKING","Reserva Anticipada");
INSERT INTO phpn_vocabulary VALUES("3628","de","_EARLY_BOOKING","Frühbucher");
INSERT INTO phpn_vocabulary VALUES("3629","en","_HOT_DEALS","Hot Deals");
INSERT INTO phpn_vocabulary VALUES("3630","es","_HOT_DEALS","Ofertas Especiales");
INSERT INTO phpn_vocabulary VALUES("3631","de","_HOT_DEALS","Heiße Angebote");
INSERT INTO phpn_vocabulary VALUES("3632","en","_COPYRIGHT","Copyright");
INSERT INTO phpn_vocabulary VALUES("3633","es","_COPYRIGHT","Derechos de autor");
INSERT INTO phpn_vocabulary VALUES("3634","de","_COPYRIGHT","Urheberrecht");
INSERT INTO phpn_vocabulary VALUES("3635","en","_ALL_RIGHTS_RESERVED","All rights reserved");
INSERT INTO phpn_vocabulary VALUES("3636","es","_ALL_RIGHTS_RESERVED","Reservados todos los derechos");
INSERT INTO phpn_vocabulary VALUES("3637","de","_ALL_RIGHTS_RESERVED","Alle Rechte vorbehalten");
INSERT INTO phpn_vocabulary VALUES("3638","en","_LETS_SOCIALIZE","Let\'s socialize");
INSERT INTO phpn_vocabulary VALUES("3639","es","_LETS_SOCIALIZE","Vamos a Socializar");
INSERT INTO phpn_vocabulary VALUES("3640","de","_LETS_SOCIALIZE","Lassen Sie uns zu knüpfen");
INSERT INTO phpn_vocabulary VALUES("3641","en","_TARGET_HOTEL","Target Hotel");
INSERT INTO phpn_vocabulary VALUES("3642","es","_TARGET_HOTEL","Objetivo del hotel");
INSERT INTO phpn_vocabulary VALUES("3643","de","_TARGET_HOTEL","Ziel Hotels");
INSERT INTO phpn_vocabulary VALUES("3644","en","_WONDERFUL","Wonderful!");
INSERT INTO phpn_vocabulary VALUES("3645","es","_WONDERFUL","¡Maravilloso!");
INSERT INTO phpn_vocabulary VALUES("3646","de","_WONDERFUL","Wunderbar!");
INSERT INTO phpn_vocabulary VALUES("3647","en","_VERY_GOOD","Very Good!");
INSERT INTO phpn_vocabulary VALUES("3648","es","_VERY_GOOD","Muy Buena!");
INSERT INTO phpn_vocabulary VALUES("3649","de","_VERY_GOOD","Sehr Gut!");
INSERT INTO phpn_vocabulary VALUES("3650","en","_NOT_GOOD","Not Good");
INSERT INTO phpn_vocabulary VALUES("3651","es","_NOT_GOOD","No Es Bueno");
INSERT INTO phpn_vocabulary VALUES("3652","de","_NOT_GOOD","Nicht Gut!");
INSERT INTO phpn_vocabulary VALUES("3653","en","_SUMMARY","Summary");
INSERT INTO phpn_vocabulary VALUES("3654","es","_SUMMARY","resumen");
INSERT INTO phpn_vocabulary VALUES("3655","de","_SUMMARY","Zusammenfassung");
INSERT INTO phpn_vocabulary VALUES("3656","en","_MAPS","Maps");
INSERT INTO phpn_vocabulary VALUES("3657","es","_MAPS","Mapas");
INSERT INTO phpn_vocabulary VALUES("3658","de","_MAPS","Karten");
INSERT INTO phpn_vocabulary VALUES("3659","en","_PREFERENCES","Preferences");
INSERT INTO phpn_vocabulary VALUES("3660","es","_PREFERENCES","Preferencias");
INSERT INTO phpn_vocabulary VALUES("3661","de","_PREFERENCES","Einstellungen");
INSERT INTO phpn_vocabulary VALUES("3662","en","_CUSTOMER_SUPPORT","Customer support");
INSERT INTO phpn_vocabulary VALUES("3663","es","_CUSTOMER_SUPPORT","Atención al cliente");
INSERT INTO phpn_vocabulary VALUES("3664","de","_CUSTOMER_SUPPORT","Kundendienst");
INSERT INTO phpn_vocabulary VALUES("3665","en","_NEED_ASSISTANCE","Need Assistance?");
INSERT INTO phpn_vocabulary VALUES("3666","es","_NEED_ASSISTANCE","¿Necesita ayuda?");
INSERT INTO phpn_vocabulary VALUES("3667","de","_NEED_ASSISTANCE","Brauchen Sie Hilfe?");
INSERT INTO phpn_vocabulary VALUES("3668","en","_NEED_ASSISTANCE_TEXT","Our team is 24/7 at your service to help you with your booking issues or answer any related questions");
INSERT INTO phpn_vocabulary VALUES("3669","es","_NEED_ASSISTANCE_TEXT","Nuestro equipo está a su servicio 24/7 para ayudarle con sus problemas de reserva o responder a cualquier pregunta relacionada");
INSERT INTO phpn_vocabulary VALUES("3670","de","_NEED_ASSISTANCE_TEXT","Unser Team ist 24/7 für Sie da, um Sie bei Ihrer Buchung Problemen zu helfen oder alle damit verbundenen Fragen zu beantworten");
INSERT INTO phpn_vocabulary VALUES("3671","en","_LAST_MINUTE","Last Minute");
INSERT INTO phpn_vocabulary VALUES("3672","es","_LAST_MINUTE","Última Hora");
INSERT INTO phpn_vocabulary VALUES("3673","de","_LAST_MINUTE","Last-Minute");
INSERT INTO phpn_vocabulary VALUES("3674","en","_ALERT_SAME_HOTEL_ROOMS","You may add to this reservation cart only rooms from the same hotel!");
INSERT INTO phpn_vocabulary VALUES("3675","es","_ALERT_SAME_HOTEL_ROOMS","Usted puede añadir a este carro de reservas únicas habitaciones desde el mismo hotel!");
INSERT INTO phpn_vocabulary VALUES("3676","de","_ALERT_SAME_HOTEL_ROOMS","Sie können zu dieser Reservierungs Warenkorb legen aus dem gleichen Hotel hinzufügen nur Zimmer!");
INSERT INTO phpn_vocabulary VALUES("3677","en","_INFO","Info");
INSERT INTO phpn_vocabulary VALUES("3678","es","_INFO","Info");
INSERT INTO phpn_vocabulary VALUES("3679","de","_INFO","Infos");
INSERT INTO phpn_vocabulary VALUES("3680","en","_TODAY_CHECKIN","Today\'s Check-in");
INSERT INTO phpn_vocabulary VALUES("3681","es","_TODAY_CHECKIN","De hoy check-in");
INSERT INTO phpn_vocabulary VALUES("3682","de","_TODAY_CHECKIN","Heutige Check-in");
INSERT INTO phpn_vocabulary VALUES("3683","en","_TODAY_CHECKOUT","Today\'s Check-out");
INSERT INTO phpn_vocabulary VALUES("3684","es","_TODAY_CHECKOUT","De hoy check-out");
INSERT INTO phpn_vocabulary VALUES("3685","de","_TODAY_CHECKOUT","Heutige Check-out");
INSERT INTO phpn_vocabulary VALUES("3686","en","_FROM_PER_NIGHT","from per night");
INSERT INTO phpn_vocabulary VALUES("3687","es","_FROM_PER_NIGHT","from per night");
INSERT INTO phpn_vocabulary VALUES("3688","de","_FROM_PER_NIGHT","from per night");
INSERT INTO phpn_vocabulary VALUES("3689","en","_ROOM_LEFT","room left");
INSERT INTO phpn_vocabulary VALUES("3690","es","_ROOM_LEFT","room left");
INSERT INTO phpn_vocabulary VALUES("3691","de","_ROOM_LEFT","room left");
INSERT INTO phpn_vocabulary VALUES("3692","en","_CHECK_HOTELS","Check Hotels");
INSERT INTO phpn_vocabulary VALUES("3693","es","_CHECK_HOTELS","Verifique Hoteles");
INSERT INTO phpn_vocabulary VALUES("3694","de","_CHECK_HOTELS","Überprüfen Hotels");
INSERT INTO phpn_vocabulary VALUES("3695","en","_CHECK_VILLAS","Check Villas");
INSERT INTO phpn_vocabulary VALUES("3696","es","_CHECK_VILLAS","Verifique Villas");
INSERT INTO phpn_vocabulary VALUES("3697","de","_CHECK_VILLAS","Überprüfen Villas");
INSERT INTO phpn_vocabulary VALUES("3698","en","_AGENCIES","Agencies");
INSERT INTO phpn_vocabulary VALUES("3699","es","_AGENCIES","Agencias");
INSERT INTO phpn_vocabulary VALUES("3700","de","_AGENCIES","Agenturen");
INSERT INTO phpn_vocabulary VALUES("3701","en","_MS_ALLOW_AGENCIES","Allow special type of customers - agencies");
INSERT INTO phpn_vocabulary VALUES("3702","es","_MS_ALLOW_AGENCIES","Permitir tipo especial de los clientes - agencias");
INSERT INTO phpn_vocabulary VALUES("3703","de","_MS_ALLOW_AGENCIES","Lassen Sie besondere Art von Kunden - Agenturen");
INSERT INTO phpn_vocabulary VALUES("3704","en","_AGENCY_DETAILS","Agency Details");
INSERT INTO phpn_vocabulary VALUES("3705","es","_AGENCY_DETAILS","Agencia Detalles");
INSERT INTO phpn_vocabulary VALUES("3706","de","_AGENCY_DETAILS","Anbieter-Detail");
INSERT INTO phpn_vocabulary VALUES("3707","en","_BALANCE","Balance");
INSERT INTO phpn_vocabulary VALUES("3708","es","_BALANCE","Equilibrar");
INSERT INTO phpn_vocabulary VALUES("3709","de","_BALANCE","Balance");
INSERT INTO phpn_vocabulary VALUES("3710","en","_ACCOUNT_BALANCE","Account Balance");
INSERT INTO phpn_vocabulary VALUES("3711","es","_ACCOUNT_BALANCE","Saldo de la cuenta");
INSERT INTO phpn_vocabulary VALUES("3712","de","_ACCOUNT_BALANCE","Kontostand");
INSERT INTO phpn_vocabulary VALUES("3713","en","_PAY_WITH_BALANCE","Pay with Balance");
INSERT INTO phpn_vocabulary VALUES("3714","es","_PAY_WITH_BALANCE","Pague con Equilibrio");
INSERT INTO phpn_vocabulary VALUES("3715","de","_PAY_WITH_BALANCE","Bezahlen Sie mit Abgleich");
INSERT INTO phpn_vocabulary VALUES("3716","en","_LOGO","Logo");
INSERT INTO phpn_vocabulary VALUES("3717","es","_LOGO","Logo");
INSERT INTO phpn_vocabulary VALUES("3718","de","_LOGO","Logo");
INSERT INTO phpn_vocabulary VALUES("3719","en","_AGENCY_LOGIN","Agency Login");
INSERT INTO phpn_vocabulary VALUES("3720","es","_AGENCY_LOGIN","Agencia Entrar");
INSERT INTO phpn_vocabulary VALUES("3721","de","_AGENCY_LOGIN","Agentur-Login");
INSERT INTO phpn_vocabulary VALUES("3722","de","_WELCOME_AGENCY_TEXT","<p>Hallo <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>\n<p>Willkommen in Agentur Konto Steuerung, die Sie verändern den Status anzeigen können, verwalten Sie Ihre Account-Einstellungen und Buchungen.</p>\n<p>\n    _TODAY_ <br />\n    _LAST_LOGIN_\n</p>\n<p><b>&#8226;</b> Um dieses Konto Zusammenfassung nur auf einem klicken <a href=\'index.php?customer=home\'>Armaturenbrett</a> link.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Mein Konto bearbeiten</a> Menü können Sie Ihre persönlichen Daten und Kontodaten zu ändern.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>Meine Buchungen</a> enthält Informationen über Ihre Bestellungen.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("3723","en","_WELCOME_AGENCY_TEXT","<p>Hello <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>\n<p>Welcome to Agency Account Panel, that allows you to view account status, manage your account settings and bookings.</p>\n<p>\n   _TODAY_<br />\n   _LAST_LOGIN_\n</p>				\n<p> <b>&#8226;</b> To view this account summary just click on a <a href=\'index.php?customer=home\'>Dashboard</a> link.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Edit My Account</a> menu allows you to change your personal info and account data.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>My Bookings</a> contains information about your orders.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("3724","es","_WELCOME_AGENCY_TEXT","<p>Hola <b>_FIRST_NAME_ _LAST_NAME_</b></p>\n<p>Bienvenido a Grupo Cuenta Agencia, de que le permite ver el estado de cuenta de gestión de la configuración de su cuenta y reservas.</p>\n<p>\n   _TODAY_ <br />\n   _LAST_LOGIN_\n</P>\n<p><b>&#8226;</b> Para ver este resumen de la cuenta basta con hacer clic en una <a href=\'index.php?customer=home\'>Salpicadero</a> enlace.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Editar Mi Cuenta</a> menú le permite cambiar su información personal y datos de la cuenta.</p>\n<p><b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>Mi Reserva</a> contiene información acerca de sus pedidos.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("3725","en","_INFORMATION","Information");
INSERT INTO phpn_vocabulary VALUES("3726","es","_INFORMATION","Información");
INSERT INTO phpn_vocabulary VALUES("3727","de","_INFORMATION","Informationen");
INSERT INTO phpn_vocabulary VALUES("3728","en","_STARTING_FROM","Starting from");
INSERT INTO phpn_vocabulary VALUES("3729","es","_STARTING_FROM","Empezando desde");
INSERT INTO phpn_vocabulary VALUES("3730","de","_STARTING_FROM","Ab");
INSERT INTO phpn_vocabulary VALUES("3731","en","_AGENCY","Agency");
INSERT INTO phpn_vocabulary VALUES("3732","es","_AGENCY","Agencia");
INSERT INTO phpn_vocabulary VALUES("3733","de","_AGENCY","Agentur");
INSERT INTO phpn_vocabulary VALUES("3734","en","_AGENCY_PANEL","Agency Panel");
INSERT INTO phpn_vocabulary VALUES("3735","es","_AGENCY_PANEL","Panel Agencia");
INSERT INTO phpn_vocabulary VALUES("3736","de","_AGENCY_PANEL","Agentur-Panel");
INSERT INTO phpn_vocabulary VALUES("3737","en","_COUPON_FOR_SINGLE_HOTEL_ALERT","This discount coupon can be applied only for single hotel!");
INSERT INTO phpn_vocabulary VALUES("3738","es","_COUPON_FOR_SINGLE_HOTEL_ALERT","Este cupón de descuento se puede aplicar solamente para solo hotel!");
INSERT INTO phpn_vocabulary VALUES("3739","de","_COUPON_FOR_SINGLE_HOTEL_ALERT","Dieser Rabatt Gutschein kann nur für einzelne Hotel angewendet werden!");
INSERT INTO phpn_vocabulary VALUES("3740","en","_NOT_ENOUGH_MONEY_ALERT","Not enough money in your account balance: ");
INSERT INTO phpn_vocabulary VALUES("3741","es","_NOT_ENOUGH_MONEY_ALERT","No hay suficiente dinero en su saldo de la cuenta: ");
INSERT INTO phpn_vocabulary VALUES("3742","de","_NOT_ENOUGH_MONEY_ALERT","Nicht genug Geld in Ihr Kontostand : ");
INSERT INTO phpn_vocabulary VALUES("3743","en","_CLICK_TO_PAY","Click To Pay");
INSERT INTO phpn_vocabulary VALUES("3744","es","_CLICK_TO_PAY","Haga clic para pagar");
INSERT INTO phpn_vocabulary VALUES("3745","de","_CLICK_TO_PAY","Klick zu zahlen");
INSERT INTO phpn_vocabulary VALUES("3746","en","_YOU_MAY_ALSO_LIKE","You May Also Like");
INSERT INTO phpn_vocabulary VALUES("3747","es","_YOU_MAY_ALSO_LIKE","También te podría gustar");
INSERT INTO phpn_vocabulary VALUES("3748","de","_YOU_MAY_ALSO_LIKE","Sie können auch mögen");
INSERT INTO phpn_vocabulary VALUES("3749","en","_MS_ALLOW_PAYMENT_WITH_BALANCE","Specifies whether to allow agencies to pay for bookings with their balance");
INSERT INTO phpn_vocabulary VALUES("3750","es","_MS_ALLOW_PAYMENT_WITH_BALANCE","Permitir que los organismos que pagan para reservas con su equilibrio");
INSERT INTO phpn_vocabulary VALUES("3751","de","_MS_ALLOW_PAYMENT_WITH_BALANCE","Lassen Agenturen für Buchungen mit dem Gleichgewicht zu zahlen");
INSERT INTO phpn_vocabulary VALUES("3752","en","_ALL_NEWS","All News");
INSERT INTO phpn_vocabulary VALUES("3753","es","_ALL_NEWS","Todas las noticias");
INSERT INTO phpn_vocabulary VALUES("3754","de","_ALL_NEWS","Alle Nachrichten");
INSERT INTO phpn_vocabulary VALUES("3755","en","_LOW_BALANCE","low balance");
INSERT INTO phpn_vocabulary VALUES("3756","es","_LOW_BALANCE","saldo bajo");
INSERT INTO phpn_vocabulary VALUES("3757","de","_LOW_BALANCE","niedrige Balance");
INSERT INTO phpn_vocabulary VALUES("3758","en","_ADD_FUNDS","Add Funds");
INSERT INTO phpn_vocabulary VALUES("3759","es","_ADD_FUNDS","Añadir fondos");
INSERT INTO phpn_vocabulary VALUES("3760","de","_ADD_FUNDS","hinzufügen Fonds");
INSERT INTO phpn_vocabulary VALUES("3761","en","_FUNDS","Funds");
INSERT INTO phpn_vocabulary VALUES("3762","es","_FUNDS","Fondos");
INSERT INTO phpn_vocabulary VALUES("3763","de","_FUNDS","Mittel");
INSERT INTO phpn_vocabulary VALUES("3764","en","_ADDED_BY","Added By");
INSERT INTO phpn_vocabulary VALUES("3765","es","_ADDED_BY","Añadido por");
INSERT INTO phpn_vocabulary VALUES("3766","de","_ADDED_BY","Hinzugefügt von");
INSERT INTO phpn_vocabulary VALUES("3767","en","_DATE_ADDED","Date Added");
INSERT INTO phpn_vocabulary VALUES("3768","es","_DATE_ADDED","Fecha agregada");
INSERT INTO phpn_vocabulary VALUES("3769","de","_DATE_ADDED","Datum hinzugefügt");
INSERT INTO phpn_vocabulary VALUES("3770","en","_MS_SPECIAL_TAX","Special Tax per Guest");
INSERT INTO phpn_vocabulary VALUES("3771","es","_MS_SPECIAL_TAX","Impuesto Especial por huésped");
INSERT INTO phpn_vocabulary VALUES("3772","de","_MS_SPECIAL_TAX","Sonderabgaben pro Gast");
INSERT INTO phpn_vocabulary VALUES("3773","en","_GUEST_TAX","Guest Tax");
INSERT INTO phpn_vocabulary VALUES("3774","es","_GUEST_TAX","Guest Tax");
INSERT INTO phpn_vocabulary VALUES("3775","de","_GUEST_TAX","Guest Tax");
INSERT INTO phpn_vocabulary VALUES("3776","en","_CANNOT_REMOVE_FUNDS_ALERT","You cannot remove these funds because it may lead to negative balance for this customer.");
INSERT INTO phpn_vocabulary VALUES("3777","es","_CANNOT_REMOVE_FUNDS_ALERT","No se puede quitar estos fondos, ya que puede conducir a saldo negativo para este cliente.");
INSERT INTO phpn_vocabulary VALUES("3778","de","_CANNOT_REMOVE_FUNDS_ALERT","Sie können diese Mittel nicht entfernt werden, da es für diesen Kunden zu negativen Bilanz führen kann.");
INSERT INTO phpn_vocabulary VALUES("3779","en","_FUNDS_INFORMATION","Funds Information");
INSERT INTO phpn_vocabulary VALUES("3780","es","_FUNDS_INFORMATION","Fondos de Información");
INSERT INTO phpn_vocabulary VALUES("3781","de","_FUNDS_INFORMATION","Fonds Informationen");
INSERT INTO phpn_vocabulary VALUES("3782","en","_MSN_BANNERS_IS_ACTIVE","Activate Banners");
INSERT INTO phpn_vocabulary VALUES("3783","es","_MSN_BANNERS_IS_ACTIVE","Activar Banners");
INSERT INTO phpn_vocabulary VALUES("3784","de","_MSN_BANNERS_IS_ACTIVE","Banner zu aktivieren");
INSERT INTO phpn_vocabulary VALUES("3785","en","_MSN_ROTATION_TYPE","Rotation Type");
INSERT INTO phpn_vocabulary VALUES("3786","es","_MSN_ROTATION_TYPE","Tipo de rotación");
INSERT INTO phpn_vocabulary VALUES("3787","de","_MSN_ROTATION_TYPE","Rotationstyp");
INSERT INTO phpn_vocabulary VALUES("3788","en","_MSN_ROTATE_DELAY","Rotation Delay");
INSERT INTO phpn_vocabulary VALUES("3789","es","_MSN_ROTATE_DELAY","Rotación de retardo");
INSERT INTO phpn_vocabulary VALUES("3790","de","_MSN_ROTATE_DELAY","Rotation Verzögerung");
INSERT INTO phpn_vocabulary VALUES("3791","en","_MSN_BANNERS_CAPTION_HTML","HTML in Slideshow Caption");
INSERT INTO phpn_vocabulary VALUES("3792","es","_MSN_BANNERS_CAPTION_HTML","HTML caption in slideshow");
INSERT INTO phpn_vocabulary VALUES("3793","de","_MSN_BANNERS_CAPTION_HTML","HTML-Beschriftungs im Diashow");
INSERT INTO phpn_vocabulary VALUES("3794","en","_MSN_ACTIVATE_BOOKINGS","Activate Bookings");
INSERT INTO phpn_vocabulary VALUES("3795","es","_MSN_ACTIVATE_BOOKINGS","Activar reservas");
INSERT INTO phpn_vocabulary VALUES("3796","de","_MSN_ACTIVATE_BOOKINGS","Aktivieren Buchungen");
INSERT INTO phpn_vocabulary VALUES("3797","en","_MSN_PAYMENT_TYPE_POA","&#8226; \'POA\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("3798","es","_MSN_PAYMENT_TYPE_POA","&#8226; Tipo \'POA\' Pago");
INSERT INTO phpn_vocabulary VALUES("3799","de","_MSN_PAYMENT_TYPE_POA","&#8226; \'POA\' Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("3800","en","_MSN_PAYMENT_TYPE_ONLINE","&#8226; \'On-line Order\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("3801","es","_MSN_PAYMENT_TYPE_ONLINE","&#8226; Tipo \'On-line Orden\' Pago");
INSERT INTO phpn_vocabulary VALUES("3802","de","_MSN_PAYMENT_TYPE_ONLINE","&#8226; \'On-line Bestell\' Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("3803","en","_MSN_ONLINE_CREDIT_CARD_REQUIRED","&#8226; Credit Cards for \'On-line Orders\'");
INSERT INTO phpn_vocabulary VALUES("3804","es","_MSN_ONLINE_CREDIT_CARD_REQUIRED","&#8226; Tarjetas de crédito para \'Pedidos on-line\'");
INSERT INTO phpn_vocabulary VALUES("3805","de","_MSN_ONLINE_CREDIT_CARD_REQUIRED","&#8226; Kreditkarten für \'Online-Bestellungen\'");
INSERT INTO phpn_vocabulary VALUES("3806","en","_MSN_PAYMENT_TYPE_BANK_TRANSFER","&#8226; \'Bank Transfer\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("3807","es","_MSN_PAYMENT_TYPE_BANK_TRANSFER","&#8226; \'Transferencia bancaria\' Tipo de Pago");
INSERT INTO phpn_vocabulary VALUES("3808","de","_MSN_PAYMENT_TYPE_BANK_TRANSFER","&#8226; \'Banküberweisung\' Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("3809","en","_MSN_BANK_TRANSFER_INFO","&#8226; \'Bank Transfer\' Info");
INSERT INTO phpn_vocabulary VALUES("3810","es","_MSN_BANK_TRANSFER_INFO","&#8226; \'Transferencia bancaria\' info");
INSERT INTO phpn_vocabulary VALUES("3811","de","_MSN_BANK_TRANSFER_INFO","&#8226; \'Banküberweisung\' Info");
INSERT INTO phpn_vocabulary VALUES("3812","en","_MSN_PAYMENT_TYPE_PAYPAL","&#8226; PayPal Payment Type");
INSERT INTO phpn_vocabulary VALUES("3813","es","_MSN_PAYMENT_TYPE_PAYPAL","&#8226; PayPal Tipo de Pago");
INSERT INTO phpn_vocabulary VALUES("3814","de","_MSN_PAYMENT_TYPE_PAYPAL","&#8226; PayPal-Zahlungstyp");
INSERT INTO phpn_vocabulary VALUES("3815","en","_MSN_PAYPAL_EMAIL","&nbsp; PayPal Email");
INSERT INTO phpn_vocabulary VALUES("3816","es","_MSN_PAYPAL_EMAIL","&nbsp; PayPal Email");
INSERT INTO phpn_vocabulary VALUES("3817","de","_MSN_PAYPAL_EMAIL","&nbsp; PayPal Email");
INSERT INTO phpn_vocabulary VALUES("3818","en","_MSN_PAYMENT_TYPE_2CO","&#8226; 2CO Payment Type	");
INSERT INTO phpn_vocabulary VALUES("3819","es","_MSN_PAYMENT_TYPE_2CO","&#8226; 2CO Tipo de Pago");
INSERT INTO phpn_vocabulary VALUES("3820","de","_MSN_PAYMENT_TYPE_2CO","&#8226; 2CO Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("3821","en","_MSN_TWO_CHECKOUT_VENDOR","&#8226; 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("3822","es","_MSN_TWO_CHECKOUT_VENDOR","&#8226; 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("3823","de","_MSN_TWO_CHECKOUT_VENDOR","&#8226; 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("3824","en","_MSN_PAYMENT_TYPE_AUTHORIZE","&#8226; Authorize.Net Payment Type");
INSERT INTO phpn_vocabulary VALUES("3825","es","_MSN_PAYMENT_TYPE_AUTHORIZE","&#8226; Authorize,Net Tipo de Pago");
INSERT INTO phpn_vocabulary VALUES("3826","de","_MSN_PAYMENT_TYPE_AUTHORIZE","&#8226; Authorize.Net Zahlungsart");
INSERT INTO phpn_vocabulary VALUES("3827","en","_MSN_AUTHORIZE_LOGIN_ID","&nbsp; Authorize.Net Login ID");
INSERT INTO phpn_vocabulary VALUES("3828","es","_MSN_AUTHORIZE_LOGIN_ID","&nbsp; Authorize.Net usuario ID");
INSERT INTO phpn_vocabulary VALUES("3829","de","_MSN_AUTHORIZE_LOGIN_ID","&nbsp; Authorize.Net Login-ID");
INSERT INTO phpn_vocabulary VALUES("3830","en","_MSN_AUTHORIZE_TRANSACTION_KEY","&nbsp; Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("3831","es","_MSN_AUTHORIZE_TRANSACTION_KEY","&nbsp; Authorize.Net Transacción Clave");
INSERT INTO phpn_vocabulary VALUES("3832","de","_MSN_AUTHORIZE_TRANSACTION_KEY","&nbsp; Authorize.Net Transaktionsschlüssel");
INSERT INTO phpn_vocabulary VALUES("3833","en","_MSN_DEFAULT_PAYMENT_SYSTEM","&nbsp; Default Payment System");
INSERT INTO phpn_vocabulary VALUES("3834","es","_MSN_DEFAULT_PAYMENT_SYSTEM","&nbsp; Por defecto del sistema de pago");
INSERT INTO phpn_vocabulary VALUES("3835","de","_MSN_DEFAULT_PAYMENT_SYSTEM","&nbsp; Standard-Zahlungssystem");
INSERT INTO phpn_vocabulary VALUES("3836","en","_MSN_SEPARATE_GATEWAYS","Hotel Owners Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("3837","es","_MSN_SEPARATE_GATEWAYS","Propietarios de hotel Gateways separados");
INSERT INTO phpn_vocabulary VALUES("3838","de","_MSN_SEPARATE_GATEWAYS","Hotelbesitzern separate Gateways");
INSERT INTO phpn_vocabulary VALUES("3839","en","_MSN_SEND_ORDER_COPY_TO_ADMIN","Send Order Copy To Admin");
INSERT INTO phpn_vocabulary VALUES("3840","es","_MSN_SEND_ORDER_COPY_TO_ADMIN","Enviar copia a fin de administrador");
INSERT INTO phpn_vocabulary VALUES("3841","de","_MSN_SEND_ORDER_COPY_TO_ADMIN","Bestellung abschicken Kopie an Admin");
INSERT INTO phpn_vocabulary VALUES("3842","en","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","Allow Booking Without Account");
INSERT INTO phpn_vocabulary VALUES("3843","es","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","Permitir Reserva Sin Cuenta");
INSERT INTO phpn_vocabulary VALUES("3844","de","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","Erlauben Buchen ohne Konto");
INSERT INTO phpn_vocabulary VALUES("3845","en","_MSN_PRE_PAYMENT_TYPE","Pre-Payment Type");
INSERT INTO phpn_vocabulary VALUES("3846","es","_MSN_PRE_PAYMENT_TYPE","Tipo Prepago");
INSERT INTO phpn_vocabulary VALUES("3847","de","_MSN_PRE_PAYMENT_TYPE","Vorauszahlung Typ");
INSERT INTO phpn_vocabulary VALUES("3848","en","_MSN_PRE_PAYMENT_VALUE","Pre-Payment Value");
INSERT INTO phpn_vocabulary VALUES("3849","es","_MSN_PRE_PAYMENT_VALUE","Valor de prepago");
INSERT INTO phpn_vocabulary VALUES("3850","de","_MSN_PRE_PAYMENT_VALUE","Vorauszahlung Wert");
INSERT INTO phpn_vocabulary VALUES("3851","en","_MSN_VAT_VALUE","VAT Default Value");
INSERT INTO phpn_vocabulary VALUES("3852","es","_MSN_VAT_VALUE","IVA Valor predeterminado");
INSERT INTO phpn_vocabulary VALUES("3853","de","_MSN_VAT_VALUE","MwSt Standardwert");
INSERT INTO phpn_vocabulary VALUES("3854","en","_MSN_MINIMUM_NIGHTS","Minimum Nights Stay");
INSERT INTO phpn_vocabulary VALUES("3855","es","_MSN_MINIMUM_NIGHTS","Estancia Mínima");
INSERT INTO phpn_vocabulary VALUES("3856","de","_MSN_MINIMUM_NIGHTS","Minimale Nächte Aufenthalt");
INSERT INTO phpn_vocabulary VALUES("3857","en","_MSN_MAXIMUM_NIGHTS","Maximum Nights Stay");
INSERT INTO phpn_vocabulary VALUES("3858","es","_MSN_MAXIMUM_NIGHTS","Máximo Noches de Alojamiento");
INSERT INTO phpn_vocabulary VALUES("3859","de","_MSN_MAXIMUM_NIGHTS","Maximale Nächte Aufenthalt");
INSERT INTO phpn_vocabulary VALUES("3860","en","_MSN_BOOKING_MODE","Payment Mode");
INSERT INTO phpn_vocabulary VALUES("3861","es","_MSN_BOOKING_MODE","Modo de Pago");
INSERT INTO phpn_vocabulary VALUES("3862","de","_MSN_BOOKING_MODE","Zahlungsmodus");
INSERT INTO phpn_vocabulary VALUES("3863","en","_MSN_SHOW_FULLY_BOOKED_ROOMS","Show Fully Booked Rooms");
INSERT INTO phpn_vocabulary VALUES("3864","es","_MSN_SHOW_FULLY_BOOKED_ROOMS","Mostrar Habitaciones totalmente Reservados");
INSERT INTO phpn_vocabulary VALUES("3865","de","_MSN_SHOW_FULLY_BOOKED_ROOMS","Zeigen Vollbelegung Zimmer");
INSERT INTO phpn_vocabulary VALUES("3866","en","_MSN_PREBOOKING_ORDERS_TIMEOUT","\'Prebooking\' Orders Timeout");
INSERT INTO phpn_vocabulary VALUES("3867","es","_MSN_PREBOOKING_ORDERS_TIMEOUT","\'Órdenes Pre Reserva\' Tiempo de espera");
INSERT INTO phpn_vocabulary VALUES("3868","de","_MSN_PREBOOKING_ORDERS_TIMEOUT","\'Vorläufige Buchung\' Orders Timeout");
INSERT INTO phpn_vocabulary VALUES("3869","en","_MSN_CUSTOMERS_CANCEL_RESERVATION","Customers May Cancel Reservation");
INSERT INTO phpn_vocabulary VALUES("3870","es","_MSN_CUSTOMERS_CANCEL_RESERVATION","Los clientes pueden cancelar reservación");
INSERT INTO phpn_vocabulary VALUES("3871","de","_MSN_CUSTOMERS_CANCEL_RESERVATION","Kunden können Reservierung stornieren");
INSERT INTO phpn_vocabulary VALUES("3872","en","_MSN_SHOW_RESERVATION_FORM","Show Reservation Form");
INSERT INTO phpn_vocabulary VALUES("3873","es","_MSN_SHOW_RESERVATION_FORM","Mostrar Formulario de Reserva");
INSERT INTO phpn_vocabulary VALUES("3874","de","_MSN_SHOW_RESERVATION_FORM","Buchungsformular anzeigen");
INSERT INTO phpn_vocabulary VALUES("3875","en","_MSN_RESERVATION_INITIAL_FEE","Booking Initial Fee");
INSERT INTO phpn_vocabulary VALUES("3876","es","_MSN_RESERVATION_INITIAL_FEE","Cargo inicial de Reservas");
INSERT INTO phpn_vocabulary VALUES("3877","de","_MSN_RESERVATION_INITIAL_FEE","Booking Ausgangsgebühr");
INSERT INTO phpn_vocabulary VALUES("3878","en","_MSN_BOOKING_NUMBER_TYPE","Type of Booking Numbers");
INSERT INTO phpn_vocabulary VALUES("3879","es","_MSN_BOOKING_NUMBER_TYPE","Tipo de Números de reservación");
INSERT INTO phpn_vocabulary VALUES("3880","de","_MSN_BOOKING_NUMBER_TYPE","Art der Buchungsnummern");
INSERT INTO phpn_vocabulary VALUES("3881","en","_MSN_VAT_INCLUDED_IN_PRICE","Include VAT in Price");
INSERT INTO phpn_vocabulary VALUES("3882","es","_MSN_VAT_INCLUDED_IN_PRICE","IVA Precio Incluye");
INSERT INTO phpn_vocabulary VALUES("3883","de","_MSN_VAT_INCLUDED_IN_PRICE","Enthalten die gesetzliche Mehrwertsteuer in Preis");
INSERT INTO phpn_vocabulary VALUES("3884","en","_MSN_SHOW_BOOKING_STATUS_FORM","Show Booking Status Form");
INSERT INTO phpn_vocabulary VALUES("3885","es","_MSN_SHOW_BOOKING_STATUS_FORM","Mostrar Formulario de Estado de la reservación");
INSERT INTO phpn_vocabulary VALUES("3886","de","_MSN_SHOW_BOOKING_STATUS_FORM","Zeige Buchungsstand Formular");
INSERT INTO phpn_vocabulary VALUES("3887","en","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","Maximum Allowed Reservations");
INSERT INTO phpn_vocabulary VALUES("3888","es","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","Máximo Reservas mascotas");
INSERT INTO phpn_vocabulary VALUES("3889","de","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","Maximal erlaubt Reservierung");
INSERT INTO phpn_vocabulary VALUES("3890","en","_MSN_FIRST_NIGHT_CALCULATING_TYPE","First Night Calculating Type");
INSERT INTO phpn_vocabulary VALUES("3891","es","_MSN_FIRST_NIGHT_CALCULATING_TYPE","Primera Noche Tipo de Cálculo");
INSERT INTO phpn_vocabulary VALUES("3892","de","_MSN_FIRST_NIGHT_CALCULATING_TYPE","First Night Rechnen Typ");
INSERT INTO phpn_vocabulary VALUES("3893","en","_MSN_AVAILABLE_UNTIL_APPROVAL","Available Until Approval");
INSERT INTO phpn_vocabulary VALUES("3894","es","_MSN_AVAILABLE_UNTIL_APPROVAL","Disponible hasta Aprobación");
INSERT INTO phpn_vocabulary VALUES("3895","de","_MSN_AVAILABLE_UNTIL_APPROVAL","Verfügbar bis zur Zulassung");
INSERT INTO phpn_vocabulary VALUES("3896","en","_MSN_RESERVATION_EXPIRED_ALERT","Reservation Expired Alert");
INSERT INTO phpn_vocabulary VALUES("3897","es","_MSN_RESERVATION_EXPIRED_ALERT","Reserva Expirado Alerta");
INSERT INTO phpn_vocabulary VALUES("3898","de","_MSN_RESERVATION_EXPIRED_ALERT","Reservierung Abgelaufen Benachrichtigung");
INSERT INTO phpn_vocabulary VALUES("3899","en","_MSN_ADMIN_BOOKING_IN_PAST","Allow Booking in the Past");
INSERT INTO phpn_vocabulary VALUES("3900","es","_MSN_ADMIN_BOOKING_IN_PAST","Permitir reserva en el Pasado");
INSERT INTO phpn_vocabulary VALUES("3901","de","_MSN_ADMIN_BOOKING_IN_PAST","Lassen Sie Buchung in der Vergangenheit");
INSERT INTO phpn_vocabulary VALUES("3902","en","_MSN_ALLOW_PAYMENT_WITH_BALANCE","Allow Agencies to pay with their balance");
INSERT INTO phpn_vocabulary VALUES("3903","es","_MSN_ALLOW_PAYMENT_WITH_BALANCE","Permitir a las agencias que pagan con su equilibrio");
INSERT INTO phpn_vocabulary VALUES("3904","de","_MSN_ALLOW_PAYMENT_WITH_BALANCE","Lassen Agenturen mit ihr Gleichgewicht zu zahlen");
INSERT INTO phpn_vocabulary VALUES("3905","en","_MSN_SPECIAL_TAX","Special Tax per Guest");
INSERT INTO phpn_vocabulary VALUES("3906","es","_MSN_SPECIAL_TAX","Impuesto Especial por huésped");
INSERT INTO phpn_vocabulary VALUES("3907","de","_MSN_SPECIAL_TAX","Sonderabgaben pro Gast");
INSERT INTO phpn_vocabulary VALUES("3908","en","_MSN_COMMENTS_ALLOW","Allow Comments");
INSERT INTO phpn_vocabulary VALUES("3909","es","_MSN_COMMENTS_ALLOW","Permitir comentarios");
INSERT INTO phpn_vocabulary VALUES("3910","de","_MSN_COMMENTS_ALLOW","Kommentare zulassen");
INSERT INTO phpn_vocabulary VALUES("3911","en","_MSN_USER_TYPE","User Type");
INSERT INTO phpn_vocabulary VALUES("3912","es","_MSN_USER_TYPE","Tipo de usuario");
INSERT INTO phpn_vocabulary VALUES("3913","de","_MSN_USER_TYPE","Benutzertyp");
INSERT INTO phpn_vocabulary VALUES("3914","en","_MSN_COMMENTS_LENGTH","Comment Length");
INSERT INTO phpn_vocabulary VALUES("3915","es","_MSN_COMMENTS_LENGTH","Comentario Largo");
INSERT INTO phpn_vocabulary VALUES("3916","de","_MSN_COMMENTS_LENGTH","Kommentar Länge");
INSERT INTO phpn_vocabulary VALUES("3917","en","_MSN_IMAGE_VERIFICATION_ALLOW","Image Verification");
INSERT INTO phpn_vocabulary VALUES("3918","es","_MSN_IMAGE_VERIFICATION_ALLOW","Verificación de la imagen");
INSERT INTO phpn_vocabulary VALUES("3919","de","_MSN_IMAGE_VERIFICATION_ALLOW","Bild Verifikation");
INSERT INTO phpn_vocabulary VALUES("3920","en","_MSN_COMMENTS_PAGE_SIZE","Comments Per Page");
INSERT INTO phpn_vocabulary VALUES("3921","es","_MSN_COMMENTS_PAGE_SIZE","Comentarios Por Pagina");
INSERT INTO phpn_vocabulary VALUES("3922","de","_MSN_COMMENTS_PAGE_SIZE","Kommentare pro Seite");
INSERT INTO phpn_vocabulary VALUES("3923","en","_MSN_PRE_MODERATION_ALLOW","Comments Pre-Moderation");
INSERT INTO phpn_vocabulary VALUES("3924","es","_MSN_PRE_MODERATION_ALLOW","Comentarios Moderación Pre");
INSERT INTO phpn_vocabulary VALUES("3925","de","_MSN_PRE_MODERATION_ALLOW","Kommentare Pre-Moderation");
INSERT INTO phpn_vocabulary VALUES("3926","en","_MSN_DELETE_PENDING_TIME","Pending Time");
INSERT INTO phpn_vocabulary VALUES("3927","es","_MSN_DELETE_PENDING_TIME","Tiempo de espera");
INSERT INTO phpn_vocabulary VALUES("3928","de","_MSN_DELETE_PENDING_TIME","Bis Zeit");
INSERT INTO phpn_vocabulary VALUES("3929","en","_MSN_CONTACT_US_KEY","Contact Key");
INSERT INTO phpn_vocabulary VALUES("3930","es","_MSN_CONTACT_US_KEY","Contacto Llave");
INSERT INTO phpn_vocabulary VALUES("3931","de","_MSN_CONTACT_US_KEY","Kontakt Schlüssel");
INSERT INTO phpn_vocabulary VALUES("3932","en","_MSN_EMAIL","Contact Email");
INSERT INTO phpn_vocabulary VALUES("3933","es","_MSN_EMAIL","Correo electrónico de contacto");
INSERT INTO phpn_vocabulary VALUES("3934","de","_MSN_EMAIL","Kontakt E-Mail");
INSERT INTO phpn_vocabulary VALUES("3935","en","_MSN_IS_SEND_DELAY","Sending Delay");
INSERT INTO phpn_vocabulary VALUES("3936","es","_MSN_IS_SEND_DELAY","El envío de Delay");
INSERT INTO phpn_vocabulary VALUES("3937","de","_MSN_IS_SEND_DELAY","Senden Verzögerung");
INSERT INTO phpn_vocabulary VALUES("3938","en","_MSN_DELAY_LENGTH","Delay Length");
INSERT INTO phpn_vocabulary VALUES("3939","es","_MSN_DELAY_LENGTH","Retardo Largo");
INSERT INTO phpn_vocabulary VALUES("3940","de","_MSN_DELAY_LENGTH","Delay-Länge");
INSERT INTO phpn_vocabulary VALUES("3941","en","_MSN_ALLOW_ADDING_BY_ADMIN","Admin Creates Patients");
INSERT INTO phpn_vocabulary VALUES("3942","es","_MSN_ALLOW_ADDING_BY_ADMIN","Administración Crea pacientes");
INSERT INTO phpn_vocabulary VALUES("3943","de","_MSN_ALLOW_ADDING_BY_ADMIN","Admin Erstellt Patienten");
INSERT INTO phpn_vocabulary VALUES("3944","en","_MSN_REG_CONFIRMATION","Confirmation Type");
INSERT INTO phpn_vocabulary VALUES("3945","es","_MSN_REG_CONFIRMATION","Tipo de confirmación");
INSERT INTO phpn_vocabulary VALUES("3946","de","_MSN_REG_CONFIRMATION","Bestätigung Typ");
INSERT INTO phpn_vocabulary VALUES("3947","en","_MSN_CUSTOMERS_IMAGE_VERIFICATION","Image Verification");
INSERT INTO phpn_vocabulary VALUES("3948","es","_MSN_CUSTOMERS_IMAGE_VERIFICATION","Verificación de la imagen");
INSERT INTO phpn_vocabulary VALUES("3949","de","_MSN_CUSTOMERS_IMAGE_VERIFICATION","Bild Verifikation");
INSERT INTO phpn_vocabulary VALUES("3950","en","_MSN_ALLOW_CUSTOMERS_LOGIN","Allow Customers to Login");
INSERT INTO phpn_vocabulary VALUES("3951","es","_MSN_ALLOW_CUSTOMERS_LOGIN","Permitir que los customers Login");
INSERT INTO phpn_vocabulary VALUES("3952","de","_MSN_ALLOW_CUSTOMERS_LOGIN","Customers ermöglichen, sich in");
INSERT INTO phpn_vocabulary VALUES("3953","en","_MSN_ALLOW_CUSTOMERS_REGISTRATION","Allow Customers to Register");
INSERT INTO phpn_vocabulary VALUES("3954","es","_MSN_ALLOW_CUSTOMERS_REGISTRATION","Permiten a los customers Registro");
INSERT INTO phpn_vocabulary VALUES("3955","de","_MSN_ALLOW_CUSTOMERS_REGISTRATION","Allow customers to Login");
INSERT INTO phpn_vocabulary VALUES("3956","en","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","Admin Changes Customers Password");
INSERT INTO phpn_vocabulary VALUES("3957","es","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","Administrador cambia la contraseña del Customers");
INSERT INTO phpn_vocabulary VALUES("3958","de","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","Admin-Passwort ändert Customers");
INSERT INTO phpn_vocabulary VALUES("3959","en","_MSN_ALLOW_CUST_RESET_PASSWORDS","Allow Reset Passwords");
INSERT INTO phpn_vocabulary VALUES("3960","es","_MSN_ALLOW_CUST_RESET_PASSWORDS","Permitir restablecer las contraseñas");
INSERT INTO phpn_vocabulary VALUES("3961","de","_MSN_ALLOW_CUST_RESET_PASSWORDS","Lassen Passwörter zurücksetzen");
INSERT INTO phpn_vocabulary VALUES("3962","en","_MSN_ALERT_ADMIN_NEW_REGISTRATION","Alert Admin On New Registration");
INSERT INTO phpn_vocabulary VALUES("3963","es","_MSN_ALERT_ADMIN_NEW_REGISTRATION","Administrador Alerta en Nuevo Registro");
INSERT INTO phpn_vocabulary VALUES("3964","de","_MSN_ALERT_ADMIN_NEW_REGISTRATION","Alert-Admin über Neu Registrierung");
INSERT INTO phpn_vocabulary VALUES("3965","en","_MSN_REMEMBER_ME","Remember Me");
INSERT INTO phpn_vocabulary VALUES("3966","es","_MSN_REMEMBER_ME","Recordarme");
INSERT INTO phpn_vocabulary VALUES("3967","de","_MSN_REMEMBER_ME","Angemeldet bleiben");
INSERT INTO phpn_vocabulary VALUES("3968","en","_MSN_ALLOW_AGENCIES","Allow Agencies");
INSERT INTO phpn_vocabulary VALUES("3969","es","_MSN_ALLOW_AGENCIES","Allow Agencies");
INSERT INTO phpn_vocabulary VALUES("3970","de","_MSN_ALLOW_AGENCIES","Allow Agencies");
INSERT INTO phpn_vocabulary VALUES("3971","en","_MSN_FAQ_IS_ACTIVE","Activate FAQ");
INSERT INTO phpn_vocabulary VALUES("3972","es","_MSN_FAQ_IS_ACTIVE","Activar FAQ");
INSERT INTO phpn_vocabulary VALUES("3973","de","_MSN_FAQ_IS_ACTIVE","Aktivieren FAQ");
INSERT INTO phpn_vocabulary VALUES("3974","en","_MSN_ACTIVATE_CAR_RENTAL","Activate Car Rental");
INSERT INTO phpn_vocabulary VALUES("3975","es","_MSN_ACTIVATE_CAR_RENTAL","Alquiler de coches Activar");
INSERT INTO phpn_vocabulary VALUES("3976","de","_MSN_ACTIVATE_CAR_RENTAL","Aktivieren Autovermietung");
INSERT INTO phpn_vocabulary VALUES("3977","en","_MS_ACTIVATE_CAR_RENTAL","Specifies whether car rental module is active or inactive");
INSERT INTO phpn_vocabulary VALUES("3978","es","_MS_ACTIVATE_CAR_RENTAL","Especifica si el módulo de alquiler de coches está activo o inactivo");
INSERT INTO phpn_vocabulary VALUES("3979","de","_MS_ACTIVATE_CAR_RENTAL","Gibt an, ob Autovermietung Modul aktiv oder inaktiv ist");
INSERT INTO phpn_vocabulary VALUES("3980","en","_MSN_TC_SEPARATE_GATEWAYS","Car Agencies Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("3981","es","_MSN_TC_SEPARATE_GATEWAYS","Agencias de autos separados Gateways");
INSERT INTO phpn_vocabulary VALUES("3982","de","_MSN_TC_SEPARATE_GATEWAYS","Auto Agenturen Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("3983","en","_MS_TC_SEPARATE_GATEWAYS","Specifies whether to allow using separate payment gateways for car rental agency");
INSERT INTO phpn_vocabulary VALUES("3984","es","_MS_TC_SEPARATE_GATEWAYS","Especifica si se permite el uso de pasarelas de pago separadas para la agencia de alquiler de coches");
INSERT INTO phpn_vocabulary VALUES("3985","de","_MS_TC_SEPARATE_GATEWAYS","Gibt an, ob für die Autovermietung mit separaten Zahlungs-Gateways zu ermöglichen");
INSERT INTO phpn_vocabulary VALUES("3986","en","_MSN_GALLERY_KEY","Gallery Key");
INSERT INTO phpn_vocabulary VALUES("3987","es","_MSN_GALLERY_KEY","Galería clave");
INSERT INTO phpn_vocabulary VALUES("3988","de","_MSN_GALLERY_KEY","Galerie Key");
INSERT INTO phpn_vocabulary VALUES("3989","en","_MSN_ALBUM_KEY","Album Key");
INSERT INTO phpn_vocabulary VALUES("3990","es","_MSN_ALBUM_KEY","Clave álbum");
INSERT INTO phpn_vocabulary VALUES("3991","de","_MSN_ALBUM_KEY","Album Schlüssel");
INSERT INTO phpn_vocabulary VALUES("3992","en","_MSN_IMAGE_GALLERY_TYPE","Image Gallery Type");
INSERT INTO phpn_vocabulary VALUES("3993","es","_MSN_IMAGE_GALLERY_TYPE","Galería de imágenes Tipo");
INSERT INTO phpn_vocabulary VALUES("3994","de","_MSN_IMAGE_GALLERY_TYPE","Bildergalerie Art");
INSERT INTO phpn_vocabulary VALUES("3995","en","_MSN_ALBUM_ICON_WIDTH","Album Icon Width");
INSERT INTO phpn_vocabulary VALUES("3996","es","_MSN_ALBUM_ICON_WIDTH","Álbum Icono Ancho");
INSERT INTO phpn_vocabulary VALUES("3997","de","_MSN_ALBUM_ICON_WIDTH","Album Ikone Breite");
INSERT INTO phpn_vocabulary VALUES("3998","en","_MSN_ALBUM_ICON_HEIGHT","Album Icon Height");
INSERT INTO phpn_vocabulary VALUES("3999","es","_MSN_ALBUM_ICON_HEIGHT","Icono Álbum Altura");
INSERT INTO phpn_vocabulary VALUES("4000","de","_MSN_ALBUM_ICON_HEIGHT","Album Icon Höhe");
INSERT INTO phpn_vocabulary VALUES("4001","en","_MSN_ALBUMS_PER_LINE","Albums Per Line");
INSERT INTO phpn_vocabulary VALUES("4002","es","_MSN_ALBUMS_PER_LINE","Álbumes por línea");
INSERT INTO phpn_vocabulary VALUES("4003","de","_MSN_ALBUMS_PER_LINE","Alben pro Zeile");
INSERT INTO phpn_vocabulary VALUES("4004","en","_MSN_VIDEO_GALLERY_TYPE","Video Gallery Type");
INSERT INTO phpn_vocabulary VALUES("4005","es","_MSN_VIDEO_GALLERY_TYPE","La galería de video Tipo");
INSERT INTO phpn_vocabulary VALUES("4006","de","_MSN_VIDEO_GALLERY_TYPE","Videogalerie Art");
INSERT INTO phpn_vocabulary VALUES("4007","en","_MSN_GALLERY_WRAPPER","HTML Wrapping Tag");
INSERT INTO phpn_vocabulary VALUES("4008","es","_MSN_GALLERY_WRAPPER","Envolver HTML Tag");
INSERT INTO phpn_vocabulary VALUES("4009","de","_MSN_GALLERY_WRAPPER","HTML-Wrapping-Tag");
INSERT INTO phpn_vocabulary VALUES("4010","en","_MSN_ITEMS_COUNT_IN_ALBUM","Show Items Count in Album");
INSERT INTO phpn_vocabulary VALUES("4011","es","_MSN_ITEMS_COUNT_IN_ALBUM","Mostrar elementos Count en álbum");
INSERT INTO phpn_vocabulary VALUES("4012","de","_MSN_ITEMS_COUNT_IN_ALBUM","Alle Artikel anzeigen Graf in Album");
INSERT INTO phpn_vocabulary VALUES("4013","en","_MSN_ALBUM_ITEMS_NUMERATION","Show Items Numeration in Album");
INSERT INTO phpn_vocabulary VALUES("4014","es","_MSN_ALBUM_ITEMS_NUMERATION","Mostrar elementos Numeración en álbum");
INSERT INTO phpn_vocabulary VALUES("4015","de","_MSN_ALBUM_ITEMS_NUMERATION","Artikel anzeigen Nummerierung in Album");
INSERT INTO phpn_vocabulary VALUES("4016","en","_MSN_NEWS_COUNT","News Count");
INSERT INTO phpn_vocabulary VALUES("4017","es","_MSN_NEWS_COUNT","Conde Noticias");
INSERT INTO phpn_vocabulary VALUES("4018","de","_MSN_NEWS_COUNT","Nachrichten Graf");
INSERT INTO phpn_vocabulary VALUES("4019","en","_MSN_NEWS_HEADER_LENGTH","News Header Length");
INSERT INTO phpn_vocabulary VALUES("4020","es","_MSN_NEWS_HEADER_LENGTH","Longitud de la cabecera Noticias");
INSERT INTO phpn_vocabulary VALUES("4021","de","_MSN_NEWS_HEADER_LENGTH","Nachrichten-Header-Länge");
INSERT INTO phpn_vocabulary VALUES("4022","en","_MSN_NEWS_RSS","News RSS");
INSERT INTO phpn_vocabulary VALUES("4023","es","_MSN_NEWS_RSS","RSS de las Noticias");
INSERT INTO phpn_vocabulary VALUES("4024","de","_MSN_NEWS_RSS","RSS-News");
INSERT INTO phpn_vocabulary VALUES("4025","en","_MSN_SHOW_NEWS_BLOCK","News Block");
INSERT INTO phpn_vocabulary VALUES("4026","es","_MSN_SHOW_NEWS_BLOCK","Bloque de noticias");
INSERT INTO phpn_vocabulary VALUES("4027","de","_MSN_SHOW_NEWS_BLOCK","Nachrichten blockieren");
INSERT INTO phpn_vocabulary VALUES("4028","en","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Newsletter Subscription");
INSERT INTO phpn_vocabulary VALUES("4029","es","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Suscripción boletín");
INSERT INTO phpn_vocabulary VALUES("4030","de","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Newsletter Abonnement");
INSERT INTO phpn_vocabulary VALUES("4031","en","_MSN_RATINGS_USER_TYPE","User Type");
INSERT INTO phpn_vocabulary VALUES("4032","es","_MSN_RATINGS_USER_TYPE","Tipo de usuario");
INSERT INTO phpn_vocabulary VALUES("4033","de","_MSN_RATINGS_USER_TYPE","Benutzertyp");
INSERT INTO phpn_vocabulary VALUES("4034","en","_MSN_MULTIPLE_ITEMS_PER_DAY","Multiple Items per Day");
INSERT INTO phpn_vocabulary VALUES("4035","es","_MSN_MULTIPLE_ITEMS_PER_DAY","Varios elementos por Día");
INSERT INTO phpn_vocabulary VALUES("4036","de","_MSN_MULTIPLE_ITEMS_PER_DAY","Mehrere Artikel pro Tag");
INSERT INTO phpn_vocabulary VALUES("4037","en","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","Search Availability Page Size");
INSERT INTO phpn_vocabulary VALUES("4038","es","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","Buscar disponibilidad Tamaño de página");
INSERT INTO phpn_vocabulary VALUES("4039","de","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","Suche Verfügbarkeit Seitenformat");
INSERT INTO phpn_vocabulary VALUES("4040","en","_MSN_ROOMS_IN_SEARCH","Show Rooms In Search");
INSERT INTO phpn_vocabulary VALUES("4041","es","_MSN_ROOMS_IN_SEARCH","Mostrar la habitación In Search");
INSERT INTO phpn_vocabulary VALUES("4042","de","_MSN_ROOMS_IN_SEARCH","Details Zimmer auf der Suche");
INSERT INTO phpn_vocabulary VALUES("4043","en","_MSN_ALLOW_CHILDREN_IN_ROOM","Allow Children in Room");
INSERT INTO phpn_vocabulary VALUES("4044","es","_MSN_ALLOW_CHILDREN_IN_ROOM","Mostrar la habitación In Search");
INSERT INTO phpn_vocabulary VALUES("4045","de","_MSN_ALLOW_CHILDREN_IN_ROOM","Details Zimmer auf der Suche");
INSERT INTO phpn_vocabulary VALUES("4046","en","_MSN_ALLOW_SYSTEM_SUGGESTION","Allow System Suggestion");
INSERT INTO phpn_vocabulary VALUES("4047","es","_MSN_ALLOW_SYSTEM_SUGGESTION","Permitir Sistema de Sugerencias");
INSERT INTO phpn_vocabulary VALUES("4048","de","_MSN_ALLOW_SYSTEM_SUGGESTION","Lassen Sie System-Vorschlag");
INSERT INTO phpn_vocabulary VALUES("4049","en","_MSN_ALLOW_EXTRA_BEDS","Allow Extra Beds in Rooms");
INSERT INTO phpn_vocabulary VALUES("4050","es","_MSN_ALLOW_EXTRA_BEDS","Permitir camas supletorias en las habitaciones");
INSERT INTO phpn_vocabulary VALUES("4051","de","_MSN_ALLOW_EXTRA_BEDS","Lassen Sie Zustellbetten im Zimmer");
INSERT INTO phpn_vocabulary VALUES("4052","en","_MSN_SHOW_DEFAULT_PRICES","Show Default Prices");
INSERT INTO phpn_vocabulary VALUES("4053","es","_MSN_SHOW_DEFAULT_PRICES","Mostrar Defecto precios");
INSERT INTO phpn_vocabulary VALUES("4054","de","_MSN_SHOW_DEFAULT_PRICES","Preise anzeigen Standard");
INSERT INTO phpn_vocabulary VALUES("4055","en","_MSN_ALLOW_DEFAULT_PERIODS","Allow Default Periods");
INSERT INTO phpn_vocabulary VALUES("4056","es","_MSN_ALLOW_DEFAULT_PERIODS","Permitir períodos predeterminados");
INSERT INTO phpn_vocabulary VALUES("4057","de","_MSN_ALLOW_DEFAULT_PERIODS","Lassen Sie Standardzeiten");
INSERT INTO phpn_vocabulary VALUES("4058","en","_MSN_ADD_WATERMARK","Add Watermark to Images");
INSERT INTO phpn_vocabulary VALUES("4059","es","_MSN_ADD_WATERMARK","Añadir marca de agua a las imágenes");
INSERT INTO phpn_vocabulary VALUES("4060","de","_MSN_ADD_WATERMARK","Wasserzeichen hinzufügen, um Bilder");
INSERT INTO phpn_vocabulary VALUES("4061","en","_MSN_WATERMARK_TEXT","Watermark Text");
INSERT INTO phpn_vocabulary VALUES("4062","es","_MSN_WATERMARK_TEXT","Estampilla de texto");
INSERT INTO phpn_vocabulary VALUES("4063","de","_MSN_WATERMARK_TEXT","Wasserzeichentext");
INSERT INTO phpn_vocabulary VALUES("4064","en","_MSN_MAX_ADULTS_IN_SEARCH","Maximum Adults in Search Box");
INSERT INTO phpn_vocabulary VALUES("4065","es","_MSN_MAX_ADULTS_IN_SEARCH","Máximo Adultos en el cuadro de búsqueda");
INSERT INTO phpn_vocabulary VALUES("4066","de","_MSN_MAX_ADULTS_IN_SEARCH","Maximale Erwachsene im Suchfeld");
INSERT INTO phpn_vocabulary VALUES("4067","en","_MSN_MAX_CHILDREN_IN_SEARCH","Maximum Adults in Search Box");
INSERT INTO phpn_vocabulary VALUES("4068","es","_MSN_MAX_CHILDREN_IN_SEARCH","Máximo Adultos en el cuadro de búsqueda");
INSERT INTO phpn_vocabulary VALUES("4069","de","_MSN_MAX_CHILDREN_IN_SEARCH","Maximale Erwachsene im Suchfeld");
INSERT INTO phpn_vocabulary VALUES("4070","en","_MSN_CHECK_PARTIALLY_OVERLAPPING","Check Partially Overlapping");
INSERT INTO phpn_vocabulary VALUES("4071","es","_MSN_CHECK_PARTIALLY_OVERLAPPING","Compruebe que se superponen parcialmente");
INSERT INTO phpn_vocabulary VALUES("4072","de","_MSN_CHECK_PARTIALLY_OVERLAPPING","Überprüfen Sie teilweise überlappend");
INSERT INTO phpn_vocabulary VALUES("4073","en","_MSN_SHOW_ROOMS_OCCUPANCY","Show Rooms Occupancy");
INSERT INTO phpn_vocabulary VALUES("4074","es","_MSN_SHOW_ROOMS_OCCUPANCY","Showrooms Ocupación");
INSERT INTO phpn_vocabulary VALUES("4075","de","_MSN_SHOW_ROOMS_OCCUPANCY","Belegung Zimmer einblenden");
INSERT INTO phpn_vocabulary VALUES("4076","en","_MS_SHOW_ROOMS_OCCUPANCY","Specifies whether to show occupancy on the room\'s description page");
INSERT INTO phpn_vocabulary VALUES("4077","es","_MS_SHOW_ROOMS_OCCUPANCY","Especifica si se debe mostrar la ocupación en la página Descripción de la habitación");
INSERT INTO phpn_vocabulary VALUES("4078","de","_MS_SHOW_ROOMS_OCCUPANCY","Gibt an, ob Belegung Beschreibung Seite auf dem Zimmer zu zeigen");
INSERT INTO phpn_vocabulary VALUES("4079","en","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","Rooms Occupancy Months");
INSERT INTO phpn_vocabulary VALUES("4080","es","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","Meses habitaciones de ocupación");
INSERT INTO phpn_vocabulary VALUES("4081","de","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","Zimmer Belegung Monate");
INSERT INTO phpn_vocabulary VALUES("4082","en","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","Specifies the number of months that will be shown on room occupancy tab");
INSERT INTO phpn_vocabulary VALUES("4083","es","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","Especifica el número de meses que se mostrará en la sala de pestaña de ocupación");
INSERT INTO phpn_vocabulary VALUES("4084","de","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","Gibt die Anzahl der Monate, die auf Raumbelegung Tab angezeigt werden");
INSERT INTO phpn_vocabulary VALUES("4085","en","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Allow Reservation Without Account");
INSERT INTO phpn_vocabulary VALUES("4086","es","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Permitir reserva sin cuenta");
INSERT INTO phpn_vocabulary VALUES("4087","de","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Lassen Buchung ohne Konto");
INSERT INTO phpn_vocabulary VALUES("4088","en","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Specifies whether to allow reservation for customer without creating account");
INSERT INTO phpn_vocabulary VALUES("4089","es","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Especifica si se permite la reserva para el cliente sin crear cuenta");
INSERT INTO phpn_vocabulary VALUES("4090","de","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Gibt an, ob für den Kunden zu ermöglichen, Buchung ohne Konto einrichten");
INSERT INTO phpn_vocabulary VALUES("4091","en","_MSN_RESERVATION_NUMBER_TYPE","Type of Reservation Numbers");
INSERT INTO phpn_vocabulary VALUES("4092","es","_MSN_RESERVATION_NUMBER_TYPE","Tipo de números de reservas");
INSERT INTO phpn_vocabulary VALUES("4093","de","_MSN_RESERVATION_NUMBER_TYPE","Art der Reservierungsnummern ");
INSERT INTO phpn_vocabulary VALUES("4094","en","_MS_RESERVATION_NUMBER_TYPE","Specifies the type of reservation numbers");
INSERT INTO phpn_vocabulary VALUES("4095","es","_MS_RESERVATION_NUMBER_TYPE","Especifica el tipo de números de reservas");
INSERT INTO phpn_vocabulary VALUES("4096","de","_MS_RESERVATION_NUMBER_TYPE","Gibt die Art der Reservierungsnummern");
INSERT INTO phpn_vocabulary VALUES("4097","en","_MSN_REVIEWS_KEY","Reviews Key");
INSERT INTO phpn_vocabulary VALUES("4098","es","_MSN_REVIEWS_KEY","Opiniones clave");
INSERT INTO phpn_vocabulary VALUES("4099","de","_MSN_REVIEWS_KEY","Reviews Key");
INSERT INTO phpn_vocabulary VALUES("4100","en","_MS_AGENCY_IN_SEARCH_RESULT","Specifies whether to show agency name in search results");
INSERT INTO phpn_vocabulary VALUES("4101","es","_MS_AGENCY_IN_SEARCH_RESULT","Especifica si se debe mostrar el nombre del organismo en los resultados de búsqueda");
INSERT INTO phpn_vocabulary VALUES("4102","de","_MS_AGENCY_IN_SEARCH_RESULT","Gibt an, ob Agenturname in den Suchergebnissen angezeigt");
INSERT INTO phpn_vocabulary VALUES("4103","en","_MSN_AGENCY_IN_SEARCH_RESULT","Agency Name in Search Result");
INSERT INTO phpn_vocabulary VALUES("4104","es","_MSN_AGENCY_IN_SEARCH_RESULT","Agencia de resultados de la búsqueda");
INSERT INTO phpn_vocabulary VALUES("4105","de","_MSN_AGENCY_IN_SEARCH_RESULT","Agenturname in Suchergebnis");
INSERT INTO phpn_vocabulary VALUES("4106","en","_MSN_TC_DEFAULT_CONTACT_PHONE","Default Contact Phone");
INSERT INTO phpn_vocabulary VALUES("4107","es","_MSN_TC_DEFAULT_CONTACT_PHONE","Teléfono de contacto predeterminada");
INSERT INTO phpn_vocabulary VALUES("4108","de","_MSN_TC_DEFAULT_CONTACT_PHONE","Standard-Kontakt Telefon");
INSERT INTO phpn_vocabulary VALUES("4109","en","_MSN_TC_DEFAULT_CONTACT_EMAIL","Default Contact Email");
INSERT INTO phpn_vocabulary VALUES("4110","es","_MSN_TC_DEFAULT_CONTACT_EMAIL","Correo electrónico de contacto predeterminado");
INSERT INTO phpn_vocabulary VALUES("4111","de","_MSN_TC_DEFAULT_CONTACT_EMAIL","Standard Kontakt E-Mail");
INSERT INTO phpn_vocabulary VALUES("4112","en","_MS_TC_DEFAULT_CONTACT_PHONE","Specifies a default contact phone number when Car Agency info is not shown");
INSERT INTO phpn_vocabulary VALUES("4113","es","_MS_TC_DEFAULT_CONTACT_PHONE","Especifica un número de teléfono de contacto por defecto cuando información Agencia de coches no se muestra");
INSERT INTO phpn_vocabulary VALUES("4114","de","_MS_TC_DEFAULT_CONTACT_PHONE","Gibt einen Standardkontakttelefonnummer, wenn Auto-Agentur Info nicht angezeigt wird");
INSERT INTO phpn_vocabulary VALUES("4115","en","_MS_TC_DEFAULT_CONTACT_EMAIL","Specifies a default contact email when Car Agency info is not shown");
INSERT INTO phpn_vocabulary VALUES("4116","es","_MS_TC_DEFAULT_CONTACT_EMAIL","Especifica un e-mail de contacto por defecto cuando información Agencia de coches no se muestra");
INSERT INTO phpn_vocabulary VALUES("4117","de","_MS_TC_DEFAULT_CONTACT_EMAIL","Gibt einen Standard Kontakt E-Mail, wenn Auto-Agentur Info nicht angezeigt wird");
INSERT INTO phpn_vocabulary VALUES("4118","en","_MD_REVIEWS","The Reviews Module allows the administrator of the site to add/edit visitor reviews, manage them and show on the Hotel Site frontend.");
INSERT INTO phpn_vocabulary VALUES("4119","es","_MD_REVIEWS","El módulo de comentarios permite que el administrador del sitio para añadir / editar los comentarios de los visitantes, así como gestionarlos y mostrar en la interfaz de comentarios de los clientes.");
INSERT INTO phpn_vocabulary VALUES("4120","de","_MD_REVIEWS","Die Bewertungen Modul ermöglicht es dem Administrator der Website bearbeiten / Besucher-Rezensionen zu schreiben, verwalten sie und zeigen auf der Hotel-Site-Frontend.");
INSERT INTO phpn_vocabulary VALUES("4121","en","_MS_REVIEWS_KEY","The keyword that will be replaced with a list of customer reviews (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("4122","es","_MS_REVIEWS_KEY","La palabra clave que será reemplazado con una lista de comentarios de los clientes (copiar y pegar en la página)");
INSERT INTO phpn_vocabulary VALUES("4123","de","_MS_REVIEWS_KEY","Das Schlüsselwort, das mit einer Liste von Kundenrezensionen ersetzt werden (kopieren und in die Seite einfügen)");
INSERT INTO phpn_vocabulary VALUES("4124","en","_MS_SEPARATE_GATEWAYS","Specifies whether to allow using separate payment gateways for each hotel");
INSERT INTO phpn_vocabulary VALUES("4125","es","_MS_SEPARATE_GATEWAYS","Especifica si se permite Usind pasarelas de pago por separado para cada hotel");
INSERT INTO phpn_vocabulary VALUES("4126","de","_MS_SEPARATE_GATEWAYS","Ob damit usind gibt gesonderte Zahlungs-Gateways für jedes Hotel");
INSERT INTO phpn_vocabulary VALUES("4127","en","_CAR_RENTAL","Car Rental");
INSERT INTO phpn_vocabulary VALUES("4128","es","_CAR_RENTAL","Alquiler de coches");
INSERT INTO phpn_vocabulary VALUES("4129","de","_CAR_RENTAL","Autovermietung");
INSERT INTO phpn_vocabulary VALUES("4130","en","_CAR_RENTAL_SETTINGS","Car Rental Settings");
INSERT INTO phpn_vocabulary VALUES("4131","es","_CAR_RENTAL_SETTINGS","Configuración de alquiler de coches");
INSERT INTO phpn_vocabulary VALUES("4132","de","_CAR_RENTAL_SETTINGS","Mietwagen-Einstellungen");
INSERT INTO phpn_vocabulary VALUES("4133","en","_TRAVEL_AGENCIES","Travel Agencies");
INSERT INTO phpn_vocabulary VALUES("4134","es","_TRAVEL_AGENCIES","Agencias de viajes");
INSERT INTO phpn_vocabulary VALUES("4135","de","_TRAVEL_AGENCIES","Reiseagenturen");
INSERT INTO phpn_vocabulary VALUES("4136","en","_TYPES_AND_RATES","Types and Rates");
INSERT INTO phpn_vocabulary VALUES("4137","es","_TYPES_AND_RATES","Tipos y tarifas");
INSERT INTO phpn_vocabulary VALUES("4138","de","_TYPES_AND_RATES","Typen und Preise");
INSERT INTO phpn_vocabulary VALUES("4139","en","_CAR_INVENTORY","Car Inventory");
INSERT INTO phpn_vocabulary VALUES("4140","es","_CAR_INVENTORY","Inventario de automóviles");
INSERT INTO phpn_vocabulary VALUES("4141","de","_CAR_INVENTORY","Autobestands");
INSERT INTO phpn_vocabulary VALUES("4142","en","_CAR_RESERVATIONS","Car Reservations");
INSERT INTO phpn_vocabulary VALUES("4143","es","_CAR_RESERVATIONS","Reservas de coches");
INSERT INTO phpn_vocabulary VALUES("4144","de","_CAR_RESERVATIONS","Auto Reservierungen");
INSERT INTO phpn_vocabulary VALUES("4145","en","_HOTEL_PAYMENT_GATEWAYS","Hotel Payment Gateways");
INSERT INTO phpn_vocabulary VALUES("4146","es","_HOTEL_PAYMENT_GATEWAYS","Pasarelas de pago del hotel");
INSERT INTO phpn_vocabulary VALUES("4147","de","_HOTEL_PAYMENT_GATEWAYS","Hotel Zahlungs-Gateways");
INSERT INTO phpn_vocabulary VALUES("4148","en","_MD_CAR_RENTAL","The Car Rental module allows customers to search for cars and vehicles in required location, rent them and pay online.");
INSERT INTO phpn_vocabulary VALUES("4149","es","_MD_CAR_RENTAL","El módulo de alquiler de coches permite a los clientes buscar para coches y vehículos en el lugar requerido, las alquilan y pagar online.");
INSERT INTO phpn_vocabulary VALUES("4150","de","_MD_CAR_RENTAL","Die Autovermietung Modul ermöglicht es Kunden, für Autos und Fahrzeuge in gewünschten Stelle zu suchen, mieten sie und online bezahlen.");
INSERT INTO phpn_vocabulary VALUES("4151","en","_AGENCY_INFO","Agency Info");
INSERT INTO phpn_vocabulary VALUES("4152","es","_AGENCY_INFO","Información de la agencia");
INSERT INTO phpn_vocabulary VALUES("4153","de","_AGENCY_INFO","Agentur Info");
INSERT INTO phpn_vocabulary VALUES("4154","en","_AGENCIES_INFO","Agencies Info");
INSERT INTO phpn_vocabulary VALUES("4155","es","_AGENCIES_INFO","Agencias de Información");
INSERT INTO phpn_vocabulary VALUES("4156","de","_AGENCIES_INFO","Agenturen Info");
INSERT INTO phpn_vocabulary VALUES("4157","en","_TRANSPORTATION_AGENCIES","Transportation Agencies");
INSERT INTO phpn_vocabulary VALUES("4158","es","_TRANSPORTATION_AGENCIES","Las agencias de transporte");
INSERT INTO phpn_vocabulary VALUES("4159","de","_TRANSPORTATION_AGENCIES","Transport-Agenturen");
INSERT INTO phpn_vocabulary VALUES("4160","en","_TRANSPORTATION_AGENCIES_MANAGEMENT","Transportation Agencies Management");
INSERT INTO phpn_vocabulary VALUES("4161","es","_TRANSPORTATION_AGENCIES_MANAGEMENT","Gestión de Agencias de Transporte");
INSERT INTO phpn_vocabulary VALUES("4162","de","_TRANSPORTATION_AGENCIES_MANAGEMENT","Verkehrsbüros Verwaltungs");
INSERT INTO phpn_vocabulary VALUES("4163","en","_GOOD","Good");
INSERT INTO phpn_vocabulary VALUES("4164","es","_GOOD","Bueno");
INSERT INTO phpn_vocabulary VALUES("4165","de","_GOOD","Gut");
INSERT INTO phpn_vocabulary VALUES("4166","en","_AGENCY_ADMINS","Agency Admins");
INSERT INTO phpn_vocabulary VALUES("4167","es","_AGENCY_ADMINS","Los administradores de las agencias");
INSERT INTO phpn_vocabulary VALUES("4168","de","_AGENCY_ADMINS","Agentur Admins");
INSERT INTO phpn_vocabulary VALUES("4169","en","_CARS","Cars");
INSERT INTO phpn_vocabulary VALUES("4170","es","_CARS","Carros");
INSERT INTO phpn_vocabulary VALUES("4171","de","_CARS","Autos");
INSERT INTO phpn_vocabulary VALUES("4172","en","_ADMIN_COPY","admin copy");
INSERT INTO phpn_vocabulary VALUES("4173","es","_ADMIN_COPY","copia administrador");
INSERT INTO phpn_vocabulary VALUES("4174","de","_ADMIN_COPY","Admin Kopie");
INSERT INTO phpn_vocabulary VALUES("4175","en","_EVENTS_NEW_USER_REGISTERED","Events - new user has been registered");
INSERT INTO phpn_vocabulary VALUES("4176","es","_EVENTS_NEW_USER_REGISTERED","Eventos - nuevo usuario se ha registrado");
INSERT INTO phpn_vocabulary VALUES("4177","de","_EVENTS_NEW_USER_REGISTERED","Events - neuen Benutzer registriert wurde");
INSERT INTO phpn_vocabulary VALUES("4178","en","_CAR_AGENCY_OWNER","Car Agency Owner");
INSERT INTO phpn_vocabulary VALUES("4179","es","_CAR_AGENCY_OWNER","Agencia coche del dueño");
INSERT INTO phpn_vocabulary VALUES("4180","de","_CAR_AGENCY_OWNER","Auto Agentur Eigentümer");
INSERT INTO phpn_vocabulary VALUES("4181","en","_CAR_AGENCY_OWNERS","Car Agency Owners");
INSERT INTO phpn_vocabulary VALUES("4182","es","_CAR_AGENCY_OWNERS","Los propietarios de coches Agencia");
INSERT INTO phpn_vocabulary VALUES("4183","de","_CAR_AGENCY_OWNERS","Auto Agentur Eigentümer");
INSERT INTO phpn_vocabulary VALUES("4184","en","_CAR_AGENCIES","Car Agencies");
INSERT INTO phpn_vocabulary VALUES("4185","es","_CAR_AGENCIES","Agencias de autos");
INSERT INTO phpn_vocabulary VALUES("4186","de","_CAR_AGENCIES","Auto Agenturen");
INSERT INTO phpn_vocabulary VALUES("4187","en","_PASSENGERS","Passengers");
INSERT INTO phpn_vocabulary VALUES("4188","es","_PASSENGERS","Los pasajeros");
INSERT INTO phpn_vocabulary VALUES("4189","de","_PASSENGERS","Passagiere");
INSERT INTO phpn_vocabulary VALUES("4190","en","_LUGGAGES","Luggages");
INSERT INTO phpn_vocabulary VALUES("4191","es","_LUGGAGES","Equipaje");
INSERT INTO phpn_vocabulary VALUES("4192","de","_LUGGAGES","Gepäcks");
INSERT INTO phpn_vocabulary VALUES("4193","en","_DOORS","Doors");
INSERT INTO phpn_vocabulary VALUES("4194","es","_DOORS","Puertas");
INSERT INTO phpn_vocabulary VALUES("4195","de","_DOORS","Türen");
INSERT INTO phpn_vocabulary VALUES("4196","en","_TRANSMISSION","Transmission");
INSERT INTO phpn_vocabulary VALUES("4197","es","_TRANSMISSION","Transmisión");
INSERT INTO phpn_vocabulary VALUES("4198","de","_TRANSMISSION","Übertragung");
INSERT INTO phpn_vocabulary VALUES("4199","en","_PRICE_PER_DAY","Price per Day");
INSERT INTO phpn_vocabulary VALUES("4200","es","_PRICE_PER_DAY","Precio por día");
INSERT INTO phpn_vocabulary VALUES("4201","de","_PRICE_PER_DAY","Preis pro Tag");
INSERT INTO phpn_vocabulary VALUES("4202","en","_PRICE_PER_HOUR","Price per Hour");
INSERT INTO phpn_vocabulary VALUES("4203","es","_PRICE_PER_HOUR","Precio por hora");
INSERT INTO phpn_vocabulary VALUES("4204","de","_PRICE_PER_HOUR","Preis pro Stunde");
INSERT INTO phpn_vocabulary VALUES("4205","en","_MAKE","Make");
INSERT INTO phpn_vocabulary VALUES("4206","es","_MAKE","Hacer");
INSERT INTO phpn_vocabulary VALUES("4207","de","_MAKE","Machen");
INSERT INTO phpn_vocabulary VALUES("4208","en","_MODEL","Model");
INSERT INTO phpn_vocabulary VALUES("4209","es","_MODEL","Modelo");
INSERT INTO phpn_vocabulary VALUES("4210","de","_MODEL","Modell");
INSERT INTO phpn_vocabulary VALUES("4211","en","_API_LOGIN","API Login");
INSERT INTO phpn_vocabulary VALUES("4212","es","_API_LOGIN","API Login");
INSERT INTO phpn_vocabulary VALUES("4213","de","_API_LOGIN","API Anmeldung");
INSERT INTO phpn_vocabulary VALUES("4214","en","_API_KEY","API Key");
INSERT INTO phpn_vocabulary VALUES("4215","es","_API_KEY","API Key");
INSERT INTO phpn_vocabulary VALUES("4216","de","_API_KEY","API-Schlüssel");
INSERT INTO phpn_vocabulary VALUES("4217","en","_PAYMENT_INFO","Payment Info");
INSERT INTO phpn_vocabulary VALUES("4218","es","_PAYMENT_INFO","Información de pago");
INSERT INTO phpn_vocabulary VALUES("4219","de","_PAYMENT_INFO","Zahlungsinformationen");
INSERT INTO phpn_vocabulary VALUES("4220","en","_LATITUDE","Latitude");
INSERT INTO phpn_vocabulary VALUES("4221","es","_LATITUDE","Latitud");
INSERT INTO phpn_vocabulary VALUES("4222","de","_LATITUDE","Breite");
INSERT INTO phpn_vocabulary VALUES("4223","en","_LONGITUDE","Longitude");
INSERT INTO phpn_vocabulary VALUES("4224","es","_LONGITUDE","Longitud");
INSERT INTO phpn_vocabulary VALUES("4225","de","_LONGITUDE","Länge");
INSERT INTO phpn_vocabulary VALUES("4226","en","_MAP_CODE_TOOLTIP","Used only if longitude and latitude are not defined");
INSERT INTO phpn_vocabulary VALUES("4227","es","_MAP_CODE_TOOLTIP","Utilizar sólo si longitud y latitud no se definen");
INSERT INTO phpn_vocabulary VALUES("4228","de","_MAP_CODE_TOOLTIP","Wird nur verwendet, wenn Länge und Breite nicht definiert sind");
INSERT INTO phpn_vocabulary VALUES("4229","en","_DROP_OFF_DATE","Drop off date");
INSERT INTO phpn_vocabulary VALUES("4230","es","_DROP_OFF_DATE","Fecha de dejada");
INSERT INTO phpn_vocabulary VALUES("4231","de","_DROP_OFF_DATE","Rückgabedatum ");
INSERT INTO phpn_vocabulary VALUES("4232","en","_PICK_UP_DATE","Pick up date");
INSERT INTO phpn_vocabulary VALUES("4233","es","_PICK_UP_DATE","Fecha de recogida");
INSERT INTO phpn_vocabulary VALUES("4234","de","_PICK_UP_DATE","Abholdatum");
INSERT INTO phpn_vocabulary VALUES("4235","en","_DROPPING_OFF","Dropping off");
INSERT INTO phpn_vocabulary VALUES("4236","es","_DROPPING_OFF","Dejar a");
INSERT INTO phpn_vocabulary VALUES("4237","de","_DROPPING_OFF","Absetzen");
INSERT INTO phpn_vocabulary VALUES("4238","en","_PICKING_UP","Picking up");
INSERT INTO phpn_vocabulary VALUES("4239","es","_PICKING_UP","Recogiendo");
INSERT INTO phpn_vocabulary VALUES("4240","de","_PICKING_UP","Aufheben");
INSERT INTO phpn_vocabulary VALUES("4241","en","_RENT_A_CAR","Rent a Car");
INSERT INTO phpn_vocabulary VALUES("4242","es","_RENT_A_CAR","Alquilar un coche");
INSERT INTO phpn_vocabulary VALUES("4243","de","_RENT_A_CAR","Ein Auto mieten");
INSERT INTO phpn_vocabulary VALUES("4244","en","_AVAILABLE_CARS","Available Cars");
INSERT INTO phpn_vocabulary VALUES("4245","es","_AVAILABLE_CARS","Autos disponibles");
INSERT INTO phpn_vocabulary VALUES("4246","de","_AVAILABLE_CARS","Verfügbare Autos");
INSERT INTO phpn_vocabulary VALUES("4247","en","_CAR_AGENCY","Car Agency");
INSERT INTO phpn_vocabulary VALUES("4248","es","_CAR_AGENCY","Agencia de autos");
INSERT INTO phpn_vocabulary VALUES("4249","de","_CAR_AGENCY","Auto-Agentur");
INSERT INTO phpn_vocabulary VALUES("4250","en","_VEHICLE_TYPE","Vehicle Type");
INSERT INTO phpn_vocabulary VALUES("4251","es","_VEHICLE_TYPE","Tipo de vehiculo");
INSERT INTO phpn_vocabulary VALUES("4252","de","_VEHICLE_TYPE","Fahrzeugtyp");
INSERT INTO phpn_vocabulary VALUES("4253","en","_VEHICLE_TYPES","Vehicle Types");
INSERT INTO phpn_vocabulary VALUES("4254","es","_VEHICLE_TYPES","Tipos de vehículos");
INSERT INTO phpn_vocabulary VALUES("4255","de","_VEHICLE_TYPES","Fahrzeugtypen");
INSERT INTO phpn_vocabulary VALUES("4256","en","_VEHICLES","Vehicles");
INSERT INTO phpn_vocabulary VALUES("4257","es","_VEHICLES","Vehículos");
INSERT INTO phpn_vocabulary VALUES("4258","de","_VEHICLES","Fahrzeuge");
INSERT INTO phpn_vocabulary VALUES("4259","en","_REGISTRATION_NUMBER","Registration Number");
INSERT INTO phpn_vocabulary VALUES("4260","es","_REGISTRATION_NUMBER","Número de registro");
INSERT INTO phpn_vocabulary VALUES("4261","de","_REGISTRATION_NUMBER","Registrationsnummer");
INSERT INTO phpn_vocabulary VALUES("4262","en","_MILEAGE","Mileage");
INSERT INTO phpn_vocabulary VALUES("4263","es","_MILEAGE","Kilometraje");
INSERT INTO phpn_vocabulary VALUES("4264","de","_MILEAGE","Kilometerstand");
INSERT INTO phpn_vocabulary VALUES("4265","en","_DEFAULT_DISTANCE","Default Distance");
INSERT INTO phpn_vocabulary VALUES("4266","es","_DEFAULT_DISTANCE","De distancia por omisión");
INSERT INTO phpn_vocabulary VALUES("4267","de","_DEFAULT_DISTANCE","Standard Entfernung");
INSERT INTO phpn_vocabulary VALUES("4268","en","_DISTANCE_EXTRA_PRICE","Distance Extra Price");
INSERT INTO phpn_vocabulary VALUES("4269","es","_DISTANCE_EXTRA_PRICE","Distancia Precio extra");
INSERT INTO phpn_vocabulary VALUES("4270","de","_DISTANCE_EXTRA_PRICE","Die Entfernung Extra-Preis");
INSERT INTO phpn_vocabulary VALUES("4271","en","_CAR_HALF_HOUR_ALERT","Sorry, but you need to reserve a car for at least half an hour.");
INSERT INTO phpn_vocabulary VALUES("4272","es","_CAR_HALF_HOUR_ALERT","Lo sentimos, pero es necesario reservar un coche durante al menos media hora.");
INSERT INTO phpn_vocabulary VALUES("4273","de","_CAR_HALF_HOUR_ALERT","Sorry, aber Sie müssen für mindestens eine halbe Stunde, um ein Auto zu reservieren.");
INSERT INTO phpn_vocabulary VALUES("4274","en","_GUESTS","Guests");
INSERT INTO phpn_vocabulary VALUES("4275","es","_GUESTS","Los huéspedes");
INSERT INTO phpn_vocabulary VALUES("4276","de","_GUESTS","Gäste");
INSERT INTO phpn_vocabulary VALUES("4277","en","_GEAR","Gear");
INSERT INTO phpn_vocabulary VALUES("4278","es","_GEAR","Engranaje");
INSERT INTO phpn_vocabulary VALUES("4279","de","_GEAR","Gang");
INSERT INTO phpn_vocabulary VALUES("4280","en","_BAGGAGE","Baggage");
INSERT INTO phpn_vocabulary VALUES("4281","es","_BAGGAGE","Equipaje");
INSERT INTO phpn_vocabulary VALUES("4282","de","_BAGGAGE","Gepäck");
INSERT INTO phpn_vocabulary VALUES("4283","en","_PER_DAY","per day");
INSERT INTO phpn_vocabulary VALUES("4284","es","_PER_DAY","por día");
INSERT INTO phpn_vocabulary VALUES("4285","de","_PER_DAY","pro Tag");
INSERT INTO phpn_vocabulary VALUES("4286","en","_PER_HOUR","per hour");
INSERT INTO phpn_vocabulary VALUES("4287","es","_PER_HOUR","por hora");
INSERT INTO phpn_vocabulary VALUES("4288","de","_PER_HOUR","pro Stunde");
INSERT INTO phpn_vocabulary VALUES("4289","en","_AUTOMATIC","Automatic");
INSERT INTO phpn_vocabulary VALUES("4290","es","_AUTOMATIC","Automático");
INSERT INTO phpn_vocabulary VALUES("4291","de","_AUTOMATIC","Automatisch");
INSERT INTO phpn_vocabulary VALUES("4292","en","_MANUAL","Manual");
INSERT INTO phpn_vocabulary VALUES("4293","es","_MANUAL","Manual");
INSERT INTO phpn_vocabulary VALUES("4294","de","_MANUAL","Handbuch");
INSERT INTO phpn_vocabulary VALUES("4295","en","_TIPTRONIC","Tiptronic");
INSERT INTO phpn_vocabulary VALUES("4296","es","_TIPTRONIC","Tiptronic");
INSERT INTO phpn_vocabulary VALUES("4297","de","_TIPTRONIC","Tiptronic");
INSERT INTO phpn_vocabulary VALUES("4298","en","_DISTANCE_UNITS","km");
INSERT INTO phpn_vocabulary VALUES("4299","es","_DISTANCE_UNITS","Km");
INSERT INTO phpn_vocabulary VALUES("4300","de","_DISTANCE_UNITS","Km");
INSERT INTO phpn_vocabulary VALUES("4301","en","_VEHICLE_DETAILS","Vehicle Details");
INSERT INTO phpn_vocabulary VALUES("4302","es","_VEHICLE_DETAILS","Detalles del vehículo");
INSERT INTO phpn_vocabulary VALUES("4303","de","_VEHICLE_DETAILS","Fahrzeugdetails ");
INSERT INTO phpn_vocabulary VALUES("4304","en","_AGES","Ages");
INSERT INTO phpn_vocabulary VALUES("4305","es","_AGES","Siglos");
INSERT INTO phpn_vocabulary VALUES("4306","de","_AGES","Ewigkeit");
INSERT INTO phpn_vocabulary VALUES("4307","en","_RESERVATION_NUMBER","Reservation Number");
INSERT INTO phpn_vocabulary VALUES("4308","es","_RESERVATION_NUMBER","Número de reserva");
INSERT INTO phpn_vocabulary VALUES("4309","de","_RESERVATION_NUMBER","Reservierungsnummer");
INSERT INTO phpn_vocabulary VALUES("4310","en","_EX_OTELS_VILLAS_OR_APARTMENTS","Ex.: hotels, villas or apartments");
INSERT INTO phpn_vocabulary VALUES("4311","es","_EX_OTELS_VILLAS_OR_APARTMENTS","Ej .: hoteles, villas o apartamentos");
INSERT INTO phpn_vocabulary VALUES("4312","de","_EX_OTELS_VILLAS_OR_APARTMENTS","Bsp .: Hotels, Villen oder Wohnungen");
INSERT INTO phpn_vocabulary VALUES("4313","en","_VEHICLE","Vehicle");
INSERT INTO phpn_vocabulary VALUES("4314","es","_VEHICLE","Vehículo");
INSERT INTO phpn_vocabulary VALUES("4315","de","_VEHICLE","Fahrzeug");
INSERT INTO phpn_vocabulary VALUES("4316","en","_RENTAL_FROM","Rental From");
INSERT INTO phpn_vocabulary VALUES("4317","es","_RENTAL_FROM","De Alquiler");
INSERT INTO phpn_vocabulary VALUES("4318","de","_RENTAL_FROM","Vermietung von");
INSERT INTO phpn_vocabulary VALUES("4319","en","_RENTAL_TO","Rental To");
INSERT INTO phpn_vocabulary VALUES("4320","es","_RENTAL_TO","Alquiler de");
INSERT INTO phpn_vocabulary VALUES("4321","de","_RENTAL_TO","Vermietung an");
INSERT INTO phpn_vocabulary VALUES("4322","en","_CAR_OWNER_NOT_ASSIGNED","You still has not been assigned to any car agency to see the reports.");
INSERT INTO phpn_vocabulary VALUES("4323","es","_CAR_OWNER_NOT_ASSIGNED","Todavía no se ha asignado a ningún organismo de coche para ver los informes.");
INSERT INTO phpn_vocabulary VALUES("4324","de","_CAR_OWNER_NOT_ASSIGNED","Sie hat immer noch nicht jedes Auto Agentur zugewiesen worden, um die Berichte zu sehen");
INSERT INTO phpn_vocabulary VALUES("4325","en","_LAST_HOTEL_PROPERTY_ALERT","This property type cannot be deleted, because it is participating in one property at least.");
INSERT INTO phpn_vocabulary VALUES("4326","es","_LAST_HOTEL_PROPERTY_ALERT","Este tipo de propiedad no puede ser eliminado, ya que está participando en un establecimiento al menos.");
INSERT INTO phpn_vocabulary VALUES("4327","de","_LAST_HOTEL_PROPERTY_ALERT","Diese Eigenschaft Typ kann nicht gelöscht werden, weil sie in einer Eigenschaft zumindest beteiligt.");
INSERT INTO phpn_vocabulary VALUES("4328","en","_CAR_BOOKING_COMPLETED","Car booking was completed");
INSERT INTO phpn_vocabulary VALUES("4329","es","_CAR_BOOKING_COMPLETED","Completado de reserva de coches");
INSERT INTO phpn_vocabulary VALUES("4330","de","_CAR_BOOKING_COMPLETED","Buchung Auto abgeschlossen");
INSERT INTO phpn_vocabulary VALUES("4331","en","_CAR_WAS_COMPLETED_MSG","Thank you for reservation car! Your booking has been completed.");
INSERT INTO phpn_vocabulary VALUES("4332","es","_CAR_WAS_COMPLETED_MSG","Gracias un coche de reserva! Su reserva se ha completado.");
INSERT INTO phpn_vocabulary VALUES("4333","de","_CAR_WAS_COMPLETED_MSG","Vielen Dank für die Reservierung Auto! Ihre Buchung ist abgeschlossen.");
INSERT INTO phpn_vocabulary VALUES("4334","en","_CAR_TYPE","Car Type");
INSERT INTO phpn_vocabulary VALUES("4335","es","_CAR_TYPE","Tipo de carro");
INSERT INTO phpn_vocabulary VALUES("4336","de","_CAR_TYPE","Auto Typ");
INSERT INTO phpn_vocabulary VALUES("4337","en","_MY_CAR_RENTALS","My Car Rentals");
INSERT INTO phpn_vocabulary VALUES("4338","es","_MY_CAR_RENTALS","Mis Alquiler de coches");
INSERT INTO phpn_vocabulary VALUES("4339","de","_MY_CAR_RENTALS","Mein Mietwagen");
INSERT INTO phpn_vocabulary VALUES("4340","en","_PAID_SUM","Paid Sum");
INSERT INTO phpn_vocabulary VALUES("4341","es","_PAID_SUM","suma pagada");
INSERT INTO phpn_vocabulary VALUES("4342","de","_PAID_SUM","gezahlten Betrag");
INSERT INTO phpn_vocabulary VALUES("4343","en","_CAR_PICKING_UP","Car Picking Up");
INSERT INTO phpn_vocabulary VALUES("4344","es","_CAR_PICKING_UP","Recogiendo del Auto");
INSERT INTO phpn_vocabulary VALUES("4345","de","_CAR_PICKING_UP","Auto Heben");
INSERT INTO phpn_vocabulary VALUES("4346","en","_CAR_RETURNING","Car Returning");
INSERT INTO phpn_vocabulary VALUES("4347","es","_CAR_RETURNING","Volviendo coche");
INSERT INTO phpn_vocabulary VALUES("4348","de","_CAR_RETURNING","Auto-Rückkehr");
INSERT INTO phpn_vocabulary VALUES("4349","en","_CARS_IN_RENTAL","Cars In Rental");
INSERT INTO phpn_vocabulary VALUES("4350","es","_CARS_IN_RENTAL","Coches de alquiler");
INSERT INTO phpn_vocabulary VALUES("4351","de","_CARS_IN_RENTAL","Autos In Vermietung");
INSERT INTO phpn_vocabulary VALUES("4352","en","_CAR_WAS_ADDED","Car has been successfully added to your reservation!");
INSERT INTO phpn_vocabulary VALUES("4353","es","_CAR_WAS_ADDED","Coche se ha añadido con éxito a su reserva!");
INSERT INTO phpn_vocabulary VALUES("4354","de","_CAR_WAS_ADDED","Car wurde auf Ihre Reservierung erfolgreich hinzugefügt!");
INSERT INTO phpn_vocabulary VALUES("4355","en","_SELECTED_CARS","Selected Cars");
INSERT INTO phpn_vocabulary VALUES("4356","es","_SELECTED_CARS","Coches seleccionados");
INSERT INTO phpn_vocabulary VALUES("4357","de","_SELECTED_CARS","Ausgewählte Autos");
INSERT INTO phpn_vocabulary VALUES("4358","en","_REMOVE_CAR_FROM_CART","Remove car from the cart");
INSERT INTO phpn_vocabulary VALUES("4359","es","_REMOVE_CAR_FROM_CART","Retire del coche del carro");
INSERT INTO phpn_vocabulary VALUES("4360","de","_REMOVE_CAR_FROM_CART","Entfernen Sie Auto aus dem Warenkorb");
INSERT INTO phpn_vocabulary VALUES("4361","en","_CAR_WAS_REMOVED","Selected car has been successfully removed from your Reservation Cart!");
INSERT INTO phpn_vocabulary VALUES("4362","es","_CAR_WAS_REMOVED","Vehículo seleccionado se ha eliminado correctamente de su reserva de la compra!");
INSERT INTO phpn_vocabulary VALUES("4363","de","_CAR_WAS_REMOVED","Ausgewählte Auto wurde aus Ihrer Reservierung Wagen erfolgreich entfernt!");
INSERT INTO phpn_vocabulary VALUES("4364","en","_CAR_NOT_FOUND","Car has not been found!");
INSERT INTO phpn_vocabulary VALUES("4365","es","_CAR_NOT_FOUND","Coche no ha sido encontrado!");
INSERT INTO phpn_vocabulary VALUES("4366","de","_CAR_NOT_FOUND","Auto ist nicht gefunden worden!");
INSERT INTO phpn_vocabulary VALUES("4367","en","_CARS_RESERVATION","Cars Reservation");
INSERT INTO phpn_vocabulary VALUES("4368","es","_CARS_RESERVATION","Coches de reserva");
INSERT INTO phpn_vocabulary VALUES("4369","de","_CARS_RESERVATION","Autos Reservierung");
INSERT INTO phpn_vocabulary VALUES("4370","en","_TIME","Time");
INSERT INTO phpn_vocabulary VALUES("4371","es","_TIME","Hora");
INSERT INTO phpn_vocabulary VALUES("4372","de","_TIME","Zeit");
INSERT INTO phpn_vocabulary VALUES("4373","en","_FUEL_TYPE","Fuel Type");
INSERT INTO phpn_vocabulary VALUES("4374","es","_FUEL_TYPE","Tipo de combustible");
INSERT INTO phpn_vocabulary VALUES("4375","de","_FUEL_TYPE","Treibstoffart");
INSERT INTO phpn_vocabulary VALUES("4376","en","_BIODIESEL","Biodiesel");
INSERT INTO phpn_vocabulary VALUES("4377","es","_BIODIESEL","El biodiesel");
INSERT INTO phpn_vocabulary VALUES("4378","de","_BIODIESEL","Biodiesel");
INSERT INTO phpn_vocabulary VALUES("4379","en","_CNG","CNG");
INSERT INTO phpn_vocabulary VALUES("4380","es","_CNG","GNC");
INSERT INTO phpn_vocabulary VALUES("4381","de","_CNG","CNG");
INSERT INTO phpn_vocabulary VALUES("4382","en","_DIESEL","Diesel");
INSERT INTO phpn_vocabulary VALUES("4383","es","_DIESEL","Diesel");
INSERT INTO phpn_vocabulary VALUES("4384","de","_DIESEL","Diesel");
INSERT INTO phpn_vocabulary VALUES("4385","en","_ELECTRIC","Electric");
INSERT INTO phpn_vocabulary VALUES("4386","es","_ELECTRIC","Eléctrico");
INSERT INTO phpn_vocabulary VALUES("4387","de","_ELECTRIC","elektrisch");
INSERT INTO phpn_vocabulary VALUES("4388","en","_ETHANOL_FFV","Ethanol FFV");
INSERT INTO phpn_vocabulary VALUES("4389","es","_ETHANOL_FFV","El etanol FFV");
INSERT INTO phpn_vocabulary VALUES("4390","de","_ETHANOL_FFV","Ethanol FFV");
INSERT INTO phpn_vocabulary VALUES("4391","en","_GASOLINE","Gasoline");
INSERT INTO phpn_vocabulary VALUES("4392","es","_GASOLINE","Gasolina");
INSERT INTO phpn_vocabulary VALUES("4393","de","_GASOLINE","Benzin");
INSERT INTO phpn_vocabulary VALUES("4394","en","_MAKES","Makes");
INSERT INTO phpn_vocabulary VALUES("4395","es","_MAKES","Hace");
INSERT INTO phpn_vocabulary VALUES("4396","de","_MAKES","Macht");
INSERT INTO phpn_vocabulary VALUES("4397","en","_HYBRID_ELECTRIC","Hybrid Electric");
INSERT INTO phpn_vocabulary VALUES("4398","es","_HYBRID_ELECTRIC","Híbrido eléctrico");
INSERT INTO phpn_vocabulary VALUES("4399","de","_HYBRID_ELECTRIC","Hybrid-Elektro");
INSERT INTO phpn_vocabulary VALUES("4400","en","_PETROL","Petrol");
INSERT INTO phpn_vocabulary VALUES("4401","es","_PETROL","Gasolina");
INSERT INTO phpn_vocabulary VALUES("4402","de","_PETROL","Benzin");
INSERT INTO phpn_vocabulary VALUES("4403","en","_STEAM","Steam");
INSERT INTO phpn_vocabulary VALUES("4404","es","_STEAM","Vapor");
INSERT INTO phpn_vocabulary VALUES("4405","de","_STEAM","Dampf");
INSERT INTO phpn_vocabulary VALUES("4406","en","_TIME_INTERVAL","Time Interval");
INSERT INTO phpn_vocabulary VALUES("4407","es","_TIME_INTERVAL","Intervalo de tiempo");
INSERT INTO phpn_vocabulary VALUES("4408","de","_TIME_INTERVAL","Zeitintervall");
INSERT INTO phpn_vocabulary VALUES("4409","en","_AGENCY_NAME","Agency Name");
INSERT INTO phpn_vocabulary VALUES("4410","es","_AGENCY_NAME","Nombre de agencia");
INSERT INTO phpn_vocabulary VALUES("4411","de","_AGENCY_NAME","Agenturname");
INSERT INTO phpn_vocabulary VALUES("4412","en","_AGENCYOWNER_WELCOME_TEXT","<p>Welcome to Car Agency Control Panel that allows you to manage information related to your transportation agency. With this Control Panel you can easy manage agency info, vehicles, vehicle types and perform a full car reservations management.</p>");
INSERT INTO phpn_vocabulary VALUES("4413","es","_AGENCYOWNER_WELCOME_TEXT","<p>Bienvenido al panel de control de la Agencia de coche que le permite administrar información relacionada con su agencia de transporte. Con este panel de control puede gestionar fácilmente información sobre la agencia, vehículos, tipos de vehículos y realizar una gestión completa de reservas de coches.</p>");
INSERT INTO phpn_vocabulary VALUES("4414","de","_AGENCYOWNER_WELCOME_TEXT","<p>Willkommen bei Auto Agentur Control Panel, die Sie Informationen im Zusammenhang mit Ihrer Transport-Agentur zu verwalten. Mit diesem Control Panel können Sie einfach Agentur Info, Fahrzeuge, Fahrzeugtypen und führen Sie eine vollständige Auto Reservierungen Management verwalten.</p>");
INSERT INTO phpn_vocabulary VALUES("4415","en","_AGENCY_CAR_TYPES","Agency Car Types");
INSERT INTO phpn_vocabulary VALUES("4416","es","_AGENCY_CAR_TYPES","Tipos Agencia de coches");
INSERT INTO phpn_vocabulary VALUES("4417","de","_AGENCY_CAR_TYPES","Agentur Autotypen");
INSERT INTO phpn_vocabulary VALUES("4418","en","_VEHICLE_CATEGORIES","Vehicle Categories");
INSERT INTO phpn_vocabulary VALUES("4419","es","_VEHICLE_CATEGORIES","Las categorías de vehículos");
INSERT INTO phpn_vocabulary VALUES("4420","de","_VEHICLE_CATEGORIES","Fahrzeug-Kategorien");
INSERT INTO phpn_vocabulary VALUES("4421","en","_VEHICLE_CATEGORY","Vehicle Category");
INSERT INTO phpn_vocabulary VALUES("4422","es","_VEHICLE_CATEGORY","Categoría de vehículo");
INSERT INTO phpn_vocabulary VALUES("4423","de","_VEHICLE_CATEGORY","Fahrzeugkategorie");
INSERT INTO phpn_vocabulary VALUES("4424","en","_PREORDERING","Pre-Ordering");
INSERT INTO phpn_vocabulary VALUES("4425","es","_PREORDERING","Pre-Orden");
INSERT INTO phpn_vocabulary VALUES("4426","de","_PREORDERING","Vorbestellung");
INSERT INTO phpn_vocabulary VALUES("4427","en","_LEGEND_PREORDERING","This is a system status for a reservation that is in progress, or was not completed (car has been added to Reservation Cart, but still not reserved)");
INSERT INTO phpn_vocabulary VALUES("4428","es","_LEGEND_PREORDERING","Este es un estado del sistema de una reserva que está en curso, o no se completó (coche ha sido añadido a la cesta de reserva, pero aún no reservado)");
INSERT INTO phpn_vocabulary VALUES("4429","de","_LEGEND_PREORDERING","Dies ist ein Systemstatus für eine Reservierung, die im Gange ist, oder nicht abgeschlossen wurde (Auto Reservierung Warenkorb hinzugefügt wurde, aber reserviert noch nicht)");
INSERT INTO phpn_vocabulary VALUES("4430","en","_NO_CARS_FOUND","No results found for your search conditions");
INSERT INTO phpn_vocabulary VALUES("4431","es","_NO_CARS_FOUND","No se encontraron resultados para sus condiciones de búsqueda");
INSERT INTO phpn_vocabulary VALUES("4432","de","_NO_CARS_FOUND","Keine Ergebnisse für Ihre Suchbedingungen gefunden");
INSERT INTO phpn_vocabulary VALUES("4433","en","_PRICE_RANGE","Price range/per day");
INSERT INTO phpn_vocabulary VALUES("4434","es","_PRICE_RANGE","Gama de precios / por día");
INSERT INTO phpn_vocabulary VALUES("4435","de","_PRICE_RANGE","Preisbereich / pro Tag");
INSERT INTO phpn_vocabulary VALUES("4436","en","_MANUFACTURER","Manufacturer");
INSERT INTO phpn_vocabulary VALUES("4437","es","_MANUFACTURER","Fabricante");
INSERT INTO phpn_vocabulary VALUES("4438","de","_MANUFACTURER","Hersteller");
INSERT INTO phpn_vocabulary VALUES("4439","en","_ASCENDING","Ascending");
INSERT INTO phpn_vocabulary VALUES("4440","es","_ASCENDING","ascendente");
INSERT INTO phpn_vocabulary VALUES("4441","de","_ASCENDING","Aufsteigend");
INSERT INTO phpn_vocabulary VALUES("4442","en","_DESCENDING","Descending");
INSERT INTO phpn_vocabulary VALUES("4443","es","_DESCENDING","descendente");
INSERT INTO phpn_vocabulary VALUES("4444","de","_DESCENDING","Absteigend");
INSERT INTO phpn_vocabulary VALUES("4445","en","_NO_MATCHES_FOUND","No matches found");
INSERT INTO phpn_vocabulary VALUES("4446","es","_NO_MATCHES_FOUND","No se encontraron coincidencias");
INSERT INTO phpn_vocabulary VALUES("4447","de","_NO_MATCHES_FOUND","Keine Treffer gefunden");
INSERT INTO phpn_vocabulary VALUES("4448","en","_LEGEND_CAR_PENDING","The car reservation has been created, but not yet been confirmed and reserved");
INSERT INTO phpn_vocabulary VALUES("4449","es","_LEGEND_CAR_PENDING","La reserva alquiler ha sido creado, pero todavía no se ha confirmado y reservado");
INSERT INTO phpn_vocabulary VALUES("4450","de","_LEGEND_CAR_PENDING","Das Auto Reservierung erstellt wurde, aber noch nicht bestätigt worden und reserviert");
INSERT INTO phpn_vocabulary VALUES("4451","en","_LEGEND_CAR_RESERVED","Car is reserved, but order is not paid yet (pending)");
INSERT INTO phpn_vocabulary VALUES("4452","es","_LEGEND_CAR_RESERVED","Está reservado coche, pero el fin no es pagado todavía (en espera)");
INSERT INTO phpn_vocabulary VALUES("4453","de","_LEGEND_CAR_RESERVED","Das Auto ist reserviert, aber Bestellung bezahlt noch nicht (in Vorbereitung)");
INSERT INTO phpn_vocabulary VALUES("4454","en","_TYPE_YOUR_PICKUP_LOCATION","type your picking up location");
INSERT INTO phpn_vocabulary VALUES("4455","es","_TYPE_YOUR_PICKUP_LOCATION","escriba su lugar de recogida");
INSERT INTO phpn_vocabulary VALUES("4456","de","_TYPE_YOUR_PICKUP_LOCATION","Geben Sie Ihren Abholort");
INSERT INTO phpn_vocabulary VALUES("4457","en","_TYPE_YOUR_DROPPING_OFF_LOCATION","type your dropping off location");
INSERT INTO phpn_vocabulary VALUES("4458","es","_TYPE_YOUR_DROPPING_OFF_LOCATION","escriba su caída Lugar");
INSERT INTO phpn_vocabulary VALUES("4459","de","_TYPE_YOUR_DROPPING_OFF_LOCATION","Geben Sie Ihre Abfallen Lage");
INSERT INTO phpn_vocabulary VALUES("4460","en","_PLEASE_SELECT_LOCATION","Please select both locations within a same country");
INSERT INTO phpn_vocabulary VALUES("4461","es","_PLEASE_SELECT_LOCATION","Por favor, seleccione una ubicación en un país son");
INSERT INTO phpn_vocabulary VALUES("4462","de","_PLEASE_SELECT_LOCATION","Bitte wählen Sie einen Ort in einem Land sind");
INSERT INTO phpn_vocabulary VALUES("4463","en","_LOWEST_HIGHEST","Lowest to Highest");
INSERT INTO phpn_vocabulary VALUES("4464","es","_LOWEST_HIGHEST","Los más bajos");
INSERT INTO phpn_vocabulary VALUES("4465","de","_LOWEST_HIGHEST","Die niedrigste");
INSERT INTO phpn_vocabulary VALUES("4466","en","_HIGHEST_LOWEST","Highest to Lowest");
INSERT INTO phpn_vocabulary VALUES("4467","es","_HIGHEST_LOWEST","De mayor a menor");
INSERT INTO phpn_vocabulary VALUES("4468","de","_HIGHEST_LOWEST","Niedrigste Preise");
INSERT INTO phpn_vocabulary VALUES("4469","en","_FOUND_AGENCIES","Found Agencies");
INSERT INTO phpn_vocabulary VALUES("4470","es","_FOUND_AGENCIES","Las agencias que se encuentran");
INSERT INTO phpn_vocabulary VALUES("4471","de","_FOUND_AGENCIES","gefunden Agenturen");
INSERT INTO phpn_vocabulary VALUES("4472","en","_TOTAL_CARS","Total Cars");
INSERT INTO phpn_vocabulary VALUES("4473","es","_TOTAL_CARS","Coches totales");
INSERT INTO phpn_vocabulary VALUES("4474","de","_TOTAL_CARS","insgesamt Autos");
INSERT INTO phpn_vocabulary VALUES("4475","en","_DAYS","Days");
INSERT INTO phpn_vocabulary VALUES("4476","es","_DAYS","Días");
INSERT INTO phpn_vocabulary VALUES("4477","de","_DAYS","Tage");
INSERT INTO phpn_vocabulary VALUES("4478","en","_PERIOD","Period");
INSERT INTO phpn_vocabulary VALUES("4479","es","_PERIOD","Período");
INSERT INTO phpn_vocabulary VALUES("4480","de","_PERIOD","Periode");
INSERT INTO phpn_vocabulary VALUES("4481","en","_VOTE","vote");
INSERT INTO phpn_vocabulary VALUES("4482","es","_VOTE","votar");
INSERT INTO phpn_vocabulary VALUES("4483","de","_VOTE","abstimmung");
INSERT INTO phpn_vocabulary VALUES("4484","en","_VOTES","votes");
INSERT INTO phpn_vocabulary VALUES("4485","es","_VOTES","votos");
INSERT INTO phpn_vocabulary VALUES("4486","de","_VOTES","stimmen");
INSERT INTO phpn_vocabulary VALUES("4487","en","_MOST_POPULAR_HOTELS","Most Popular Hotels");
INSERT INTO phpn_vocabulary VALUES("4488","es","_MOST_POPULAR_HOTELS","Hoteles más populares");
INSERT INTO phpn_vocabulary VALUES("4489","de","_MOST_POPULAR_HOTELS","Die beliebtesten Hotels");
INSERT INTO phpn_vocabulary VALUES("4490","en","_LAST_BOOKINGS","Last Bookings");
INSERT INTO phpn_vocabulary VALUES("4491","es","_LAST_BOOKINGS","últimos Reservas");
INSERT INTO phpn_vocabulary VALUES("4492","de","_LAST_BOOKINGS","Letzte Buchungen");
INSERT INTO phpn_vocabulary VALUES("4493","en","_LAST_BOOKINGS_MESSAGE","A visitor from {user_location} {type_booking} {count_rooms} at {hotel_name} in {hotel_location}");
INSERT INTO phpn_vocabulary VALUES("4494","es","_LAST_BOOKINGS_MESSAGE","Un visitante de la {user_location} {count_rooms} {type_booking} en el {hotel_name} en {hotel_location}");
INSERT INTO phpn_vocabulary VALUES("4495","de","_LAST_BOOKINGS_MESSAGE","Ein Besucher aus {user_location} {type_booking} {count_rooms} auf {hotel_name} in {hotel_location}");
INSERT INTO phpn_vocabulary VALUES("4496","en","_JUST_BOOKED","just booked");
INSERT INTO phpn_vocabulary VALUES("4497","es","_JUST_BOOKED","hecho una reserva");
INSERT INTO phpn_vocabulary VALUES("4498","de","_JUST_BOOKED","nur gebucht");
INSERT INTO phpn_vocabulary VALUES("4499","en","_AGO","ago");
INSERT INTO phpn_vocabulary VALUES("4500","es","_AGO","hace");
INSERT INTO phpn_vocabulary VALUES("4501","de","_AGO","vor");
INSERT INTO phpn_vocabulary VALUES("4502","en","_ROOM","room");
INSERT INTO phpn_vocabulary VALUES("4503","es","_ROOM","habitación");
INSERT INTO phpn_vocabulary VALUES("4504","de","_ROOM","Zimmer");
INSERT INTO phpn_vocabulary VALUES("4505","en","_BOOKED","booked");
INSERT INTO phpn_vocabulary VALUES("4506","es","_BOOKED","reservado");
INSERT INTO phpn_vocabulary VALUES("4507","de","_BOOKED","gebucht");
INSERT INTO phpn_vocabulary VALUES("4508","en","_VEHICLE_CATEGORY_DELETE_ALERT","You cannot delete this category, because it still includes some vehicles! Please remove all vehicles from this category before deleting operation.");
INSERT INTO phpn_vocabulary VALUES("4509","es","_VEHICLE_CATEGORY_DELETE_ALERT","No se puede eliminar esta categoría, ya que todavía incluye algunos vehículos! Por favor, elimine todos los vehículos de esta categoría antes de la operación de borrado.");
INSERT INTO phpn_vocabulary VALUES("4510","de","_VEHICLE_CATEGORY_DELETE_ALERT","Sie können diese Kategorie nicht löschen, weil es noch einige Fahrzeuge beinhaltet! Bitte entfernen Sie alle Fahrzeuge dieser Kategorie vor dem Betrieb zu löschen.");
INSERT INTO phpn_vocabulary VALUES("4511","en","_LEGEND_CAR_REFUNDED","Order has been refunded and vehicle is available again in search");
INSERT INTO phpn_vocabulary VALUES("4512","es","_LEGEND_CAR_REFUNDED","Pedido ha sido devuelto y el vehículo está disponible de nuevo en busca");
INSERT INTO phpn_vocabulary VALUES("4513","de","_LEGEND_CAR_REFUNDED","Auftrag wurde erstattet und Fahrzeug wieder verfügbar ist auf der Suche");
INSERT INTO phpn_vocabulary VALUES("4514","en","_LEGEND_CAR_CANCELED","Order is canceled by admin and vehicle is available again in search");
INSERT INTO phpn_vocabulary VALUES("4515","es","_LEGEND_CAR_CANCELED","Orden es cancelada por el administrador y el vehículo está disponible de nuevo en busca");
INSERT INTO phpn_vocabulary VALUES("4516","de","_LEGEND_CAR_CANCELED","Bestellung wird von admin aufgehoben und das Fahrzeug wieder verfügbar auf der Suche");
INSERT INTO phpn_vocabulary VALUES("4517","en","_POSITIVE_COMMENTS","Positive Comments");
INSERT INTO phpn_vocabulary VALUES("4518","es","_POSITIVE_COMMENTS","Comentarios positivos");
INSERT INTO phpn_vocabulary VALUES("4519","de","_POSITIVE_COMMENTS","Positive Kommentare");
INSERT INTO phpn_vocabulary VALUES("4520","en","_NEGATIVE_COMMENTS","Negative Comments");
INSERT INTO phpn_vocabulary VALUES("4521","es","_NEGATIVE_COMMENTS","Comentarios negativos");
INSERT INTO phpn_vocabulary VALUES("4522","de","_NEGATIVE_COMMENTS","Negative Kommentare");
INSERT INTO phpn_vocabulary VALUES("4523","en","_WRONG_EMPTY_FIELD","Fields picking up or/and dropping off are empty. Please select them.");
INSERT INTO phpn_vocabulary VALUES("4524","es","_WRONG_EMPTY_FIELD","Campo vacío incorrecto recoger y/o dejar a");
INSERT INTO phpn_vocabulary VALUES("4525","de","_WRONG_EMPTY_FIELD","Falsche leeres Feld Aufnehmen oder/und Abwurf");
INSERT INTO phpn_vocabulary VALUES("4526","en","_REVIEW","review");
INSERT INTO phpn_vocabulary VALUES("4527","es","_REVIEW","revisión");
INSERT INTO phpn_vocabulary VALUES("4528","de","_REVIEW","Überprüfung");
INSERT INTO phpn_vocabulary VALUES("4529","en","_CLEANLINESS","Cleanliness");
INSERT INTO phpn_vocabulary VALUES("4530","es","_CLEANLINESS","Limpieza");
INSERT INTO phpn_vocabulary VALUES("4531","de","_CLEANLINESS","Sauberkeit");
INSERT INTO phpn_vocabulary VALUES("4532","en","_ROOM_COMFORT","Room Comfort");
INSERT INTO phpn_vocabulary VALUES("4533","es","_ROOM_COMFORT","Confort de la habitación");
INSERT INTO phpn_vocabulary VALUES("4534","de","_ROOM_COMFORT","Zimmerkomfort");
INSERT INTO phpn_vocabulary VALUES("4535","en","_SERVICE_AND_STAFF","Service & Staff");
INSERT INTO phpn_vocabulary VALUES("4536","es","_SERVICE_AND_STAFF","Personal de servicio");
INSERT INTO phpn_vocabulary VALUES("4537","de","_SERVICE_AND_STAFF","Servicepersonal");
INSERT INTO phpn_vocabulary VALUES("4538","en","_SLEEP_QUALITY","Sleep Quality");
INSERT INTO phpn_vocabulary VALUES("4539","es","_SLEEP_QUALITY","Calidad del sueño");
INSERT INTO phpn_vocabulary VALUES("4540","de","_SLEEP_QUALITY","Schlafqualität ");
INSERT INTO phpn_vocabulary VALUES("4541","en","_VALUE_FOR_PRICE","Value for Price");
INSERT INTO phpn_vocabulary VALUES("4542","es","_VALUE_FOR_PRICE","Relación calidad-precio");
INSERT INTO phpn_vocabulary VALUES("4543","de","_VALUE_FOR_PRICE","Preis-Leistungs-Preis");
INSERT INTO phpn_vocabulary VALUES("4544","en","_WRONG_EMPTY_FIELDS_PICK_UP_AND_DROP_OFF","Fields picking up and dropping off may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4545","es","_WRONG_EMPTY_FIELDS_PICK_UP_AND_DROP_OFF","Campo vacío incorrecto recoger y dejar a");
INSERT INTO phpn_vocabulary VALUES("4546","de","_WRONG_EMPTY_FIELDS_PICK_UP_AND_DROP_OFF","Falsche leeres Feld Aufnehmen und Abwurf");
INSERT INTO phpn_vocabulary VALUES("4547","en","_WRONG_EMPTY_FIELD_PICK_UP","Fields picking up may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4548","es","_WRONG_EMPTY_FIELD_PICK_UP","Campo vacío incorrecto recogiendo");
INSERT INTO phpn_vocabulary VALUES("4549","de","_WRONG_EMPTY_FIELD_PICK_UP","Falsche leeres Feld Abholung");
INSERT INTO phpn_vocabulary VALUES("4550","en","_WRONG_EMPTY_FIELD_DROP_OFF","Fields dropping off may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4551","es","_WRONG_EMPTY_FIELD_DROP_OFF","Campo vacío incorrecto dejar a");
INSERT INTO phpn_vocabulary VALUES("4552","de","_WRONG_EMPTY_FIELD_DROP_OFF","Falsche leeres Feld Abwurf");
INSERT INTO phpn_vocabulary VALUES("4553","en","_CAR_AGENCY_PAYMENT_GATEWAYS","Agency Payment Gateways");
INSERT INTO phpn_vocabulary VALUES("4554","es","_CAR_AGENCY_PAYMENT_GATEWAYS","Agencia de alquiler de pasarelas de pago");
INSERT INTO phpn_vocabulary VALUES("4555","de","_CAR_AGENCY_PAYMENT_GATEWAYS","Auto-Agentur Zahlungs-Gateways");
INSERT INTO phpn_vocabulary VALUES("4556","en","_TITLE","Title");
INSERT INTO phpn_vocabulary VALUES("4557","es","_TITLE","Título");
INSERT INTO phpn_vocabulary VALUES("4558","de","_TITLE","Titel");
INSERT INTO phpn_vocabulary VALUES("4559","en","_EVALUATION","Evaluation");
INSERT INTO phpn_vocabulary VALUES("4560","es","_EVALUATION","Evaluación");
INSERT INTO phpn_vocabulary VALUES("4561","de","_EVALUATION","Auswertung");
INSERT INTO phpn_vocabulary VALUES("4562","en","_NOT_RECOMMENDED","Not Recommended");
INSERT INTO phpn_vocabulary VALUES("4563","es","_NOT_RECOMMENDED","No recomendado");
INSERT INTO phpn_vocabulary VALUES("4564","de","_NOT_RECOMMENDED","Nicht empfohlen");
INSERT INTO phpn_vocabulary VALUES("4565","en","_RECOMMENDED_TO_EVERYONE","Recommended to Everyone");
INSERT INTO phpn_vocabulary VALUES("4566","es","_RECOMMENDED_TO_EVERYONE","Recomendó a todos");
INSERT INTO phpn_vocabulary VALUES("4567","de","_RECOMMENDED_TO_EVERYONE","Empfohlen Jeder");
INSERT INTO phpn_vocabulary VALUES("4568","en","_PERCENT_OF_GUESTS_RECOMMEND","_PERCENT_ of guests recommend");
INSERT INTO phpn_vocabulary VALUES("4569","es","_PERCENT_OF_GUESTS_RECOMMEND","_PERCENT_ de los huéspedes recomienda");
INSERT INTO phpn_vocabulary VALUES("4570","de","_PERCENT_OF_GUESTS_RECOMMEND","_PERCENT_ der Gäste empfehlen");
INSERT INTO phpn_vocabulary VALUES("4571","en","_NEUTRAL","Neutral");
INSERT INTO phpn_vocabulary VALUES("4572","es","_NEUTRAL","Neutral");
INSERT INTO phpn_vocabulary VALUES("4573","de","_NEUTRAL","Neutral");
INSERT INTO phpn_vocabulary VALUES("4574","en","_RATINGS_BASED_ON_REVIEWS","Ratings based on _REVIEWS_ Verified Reviews");
INSERT INTO phpn_vocabulary VALUES("4575","es","_RATINGS_BASED_ON_REVIEWS","Las capacidades están basadas en _REVIEWS_ Comentarios verificados");
INSERT INTO phpn_vocabulary VALUES("4576","de","_RATINGS_BASED_ON_REVIEWS","Bewertungen basierend auf _REVIEWS_ Prüfte Bewertungen");
INSERT INTO phpn_vocabulary VALUES("4577","en","_X_OUT_OF_Y","_X_ out of _Y_");
INSERT INTO phpn_vocabulary VALUES("4578","es","_X_OUT_OF_Y","_X_ De _Y_");
INSERT INTO phpn_vocabulary VALUES("4579","de","_X_OUT_OF_Y","_X_ Aus _Y_");
INSERT INTO phpn_vocabulary VALUES("4580","en","_AVERAGE_RATINGS","Average Ratings");
INSERT INTO phpn_vocabulary VALUES("4581","es","_AVERAGE_RATINGS","Promedio de Calificaciones");
INSERT INTO phpn_vocabulary VALUES("4582","de","_AVERAGE_RATINGS","Durchschnittliche Bewertungen");
INSERT INTO phpn_vocabulary VALUES("4583","en","_BY","by");
INSERT INTO phpn_vocabulary VALUES("4584","es","_BY","por");
INSERT INTO phpn_vocabulary VALUES("4585","de","_BY","durch");
INSERT INTO phpn_vocabulary VALUES("4586","en","_ADD_REVIEW","Add Review");
INSERT INTO phpn_vocabulary VALUES("4587","es","_ADD_REVIEW","Añadir Comentario");
INSERT INTO phpn_vocabulary VALUES("4588","de","_ADD_REVIEW","Review schreiben");
INSERT INTO phpn_vocabulary VALUES("4589","en","_PROPERTY_NOT_REVIEWED_YET","Not yet reviewed by any member... You can be the FIRST one to write a review for _PROPERTY_NAME_.");
INSERT INTO phpn_vocabulary VALUES("4590","es","_PROPERTY_NOT_REVIEWED_YET","aún no revisado por cualquier miembro ... Usted puede ser el primero en escribir un comentario para _PROPERTY_NAME_.");
INSERT INTO phpn_vocabulary VALUES("4591","de","_PROPERTY_NOT_REVIEWED_YET","Noch nicht von jedem Mitglied prüft ... Sie können der erste sein, der eine Bewertung für _PROPERTY_NAME_ zu schreiben.");
INSERT INTO phpn_vocabulary VALUES("4592","en","_GUEST_RATINGS","guest ratings");
INSERT INTO phpn_vocabulary VALUES("4593","es","_GUEST_RATINGS","clasificaciones de los huéspedes");
INSERT INTO phpn_vocabulary VALUES("4594","de","_GUEST_RATINGS","Bewertungen");
INSERT INTO phpn_vocabulary VALUES("4595","en","_REVIEWS_SETTINGS","Reviews Settings");
INSERT INTO phpn_vocabulary VALUES("4596","es","_REVIEWS_SETTINGS","Ajustes críticas");
INSERT INTO phpn_vocabulary VALUES("4597","de","_REVIEWS_SETTINGS","Bewertungen Einstellungen");
INSERT INTO phpn_vocabulary VALUES("4598","en","_MD_TESTIMONIALS","The Testimonials Module allows the administrator of the site to add/edit customer testimonials, manage them and show on the Hotel Site frontend.");
INSERT INTO phpn_vocabulary VALUES("4599","es","_MD_TESTIMONIALS","El módulo de Testimonios permite al administrador del sitio para añadir/editar testimonios de clientes, gestión de ellos y mostrar en la interfaz web del hotel.");
INSERT INTO phpn_vocabulary VALUES("4600","de","_MD_TESTIMONIALS","Die Testimonials Modul erlaubt dem Administrator der Seite, auf Hinzufügen / Bearbeiten Kundenaussagen, verwalten Sie sie und zeigen auf der Website des Hotels Frontend.");
INSERT INTO phpn_vocabulary VALUES("4601","en","_MS_TESTIMONIALS_KEY","The keyword that will be replaced with a list of customer testimonials (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("4602","es","_MS_TESTIMONIALS_KEY","La palabra clave que serán reemplazados por una lista de testimonios de clientes (copiar y pegarlo en la página)");
INSERT INTO phpn_vocabulary VALUES("4603","de","_MS_TESTIMONIALS_KEY","Das Schlüsselwort, das mit einer Liste von Kunden Statements ersetzt werden (Kopieren und fügen Sie ihn auf der Seite)");
INSERT INTO phpn_vocabulary VALUES("4604","en","_TESTIMONIALS","Testimonials");
INSERT INTO phpn_vocabulary VALUES("4605","es","_TESTIMONIALS","Testimonios");
INSERT INTO phpn_vocabulary VALUES("4606","de","_TESTIMONIALS","Zeugnisse");
INSERT INTO phpn_vocabulary VALUES("4607","en","_TESTIMONIALS_MANAGEMENT","Testimonials Management");
INSERT INTO phpn_vocabulary VALUES("4608","es","_TESTIMONIALS_MANAGEMENT","Gestión de los testimonios");
INSERT INTO phpn_vocabulary VALUES("4609","de","_TESTIMONIALS_MANAGEMENT","Management von Testimonials");
INSERT INTO phpn_vocabulary VALUES("4610","en","_TESTIMONIALS_SETTINGS","Testimonials Settings");
INSERT INTO phpn_vocabulary VALUES("4611","es","_TESTIMONIALS_SETTINGS","Configuración de testimonios");
INSERT INTO phpn_vocabulary VALUES("4612","de","_TESTIMONIALS_SETTINGS","Zeugnisse Einstellungen");
INSERT INTO phpn_vocabulary VALUES("4613","en","_MSN_TESTIMONIALS_KEY","Testimonials Key");
INSERT INTO phpn_vocabulary VALUES("4614","es","_MSN_TESTIMONIALS_KEY","Testimonios Key");
INSERT INTO phpn_vocabulary VALUES("4615","de","_MSN_TESTIMONIALS_KEY","Testimonials Key");
INSERT INTO phpn_vocabulary VALUES("4616","en","_REVIEW_YOU_HAVE_REGISTERED","You have to be registered in to post your review.");
INSERT INTO phpn_vocabulary VALUES("4617","es","_REVIEW_YOU_HAVE_REGISTERED","Tiene que estar registrado para enviar su opinión.");
INSERT INTO phpn_vocabulary VALUES("4618","de","_REVIEW_YOU_HAVE_REGISTERED","Sie müssen registriert in einen Beitrag zu schreiben.");
INSERT INTO phpn_vocabulary VALUES("4619","en","_REVIEW_LINK_LOGIN","<a href=index.php?customer=login>Link</a> to login page.");
INSERT INTO phpn_vocabulary VALUES("4620","es","_REVIEW_LINK_LOGIN","<a href=index.php?customer=login>Enlazar</a> a la página de acceso.");
INSERT INTO phpn_vocabulary VALUES("4621","de","_REVIEW_LINK_LOGIN","<a href=index.php?customer=login>Link-Seite</a> anmelden.");
INSERT INTO phpn_vocabulary VALUES("4622","en","_MIN_MAX_NIGHTS_ALERT","The minimum and maximum allowed stay for the period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking (package _PACKAGE_NAME_). Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4623","es","_MIN_MAX_NIGHTS_ALERT","_IN_HOTEL_La estancia mínima permitida para el período de tiempo a partir de _FROM_ a _TO_ es _NIGHTS_ noches por reserva (paquete _PACKAGE_NAME_). Por favor, vuelva a entrar.");
INSERT INTO phpn_vocabulary VALUES("4624","de","_MIN_MAX_NIGHTS_ALERT","_IN_HOTEL_Der kleinste zulässige Aufenthalt für den Zeitraum von _FROM_ bis _TO_ ist _NIGHTS_ Übernachtungen pro Buchung (Paket _PACKAGE_NAME_). Bitte geben Sie erneut.");
INSERT INTO phpn_vocabulary VALUES("4625","en","_MY_REVIEWS","My Reviews");
INSERT INTO phpn_vocabulary VALUES("4626","es","_MY_REVIEWS","Mis críticas");
INSERT INTO phpn_vocabulary VALUES("4627","de","_MY_REVIEWS","Meine Bewertungen");
INSERT INTO phpn_vocabulary VALUES("4628","en","_PHOTO","Photo");
INSERT INTO phpn_vocabulary VALUES("4629","es","_PHOTO","Foto");
INSERT INTO phpn_vocabulary VALUES("4630","de","_PHOTO","Foto");
INSERT INTO phpn_vocabulary VALUES("4631","en","_MSN_SEARCH_AVAILABILITY_PERIOD","Maximum Allowed Search Period");
INSERT INTO phpn_vocabulary VALUES("4632","es","_MSN_SEARCH_AVAILABILITY_PERIOD","Máximo periodo de Búsqueda");
INSERT INTO phpn_vocabulary VALUES("4633","de","_MSN_SEARCH_AVAILABILITY_PERIOD","Maximal zulässige Suchperiode ");
INSERT INTO phpn_vocabulary VALUES("4634","en","_MS_SEARCH_AVAILABILITY_PERIOD","Specifies the maximum allowed period of time for search in years");
INSERT INTO phpn_vocabulary VALUES("4635","es","_MS_SEARCH_AVAILABILITY_PERIOD","Especifica el periodo máximo de tiempo permitido para la búsqueda en años");
INSERT INTO phpn_vocabulary VALUES("4636","de","_MS_SEARCH_AVAILABILITY_PERIOD","Gibt die maximale im Jahre Zeit für die Suche erlaubt");
INSERT INTO phpn_vocabulary VALUES("4637","en","_MAXIMUM_PERIOD_ALERT","The maximum allowed period for search is _DAYS_ days. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4638","es","_MAXIMUM_PERIOD_ALERT","El plazo máximo para la búsqueda es _DAYS_ días. Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("4639","de","_MAXIMUM_PERIOD_ALERT","Die maximal erlaubte Zeit für die Suche ist _DAYS_ Tage. Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("4640","en","_WISHLIST","Wishlist");
INSERT INTO phpn_vocabulary VALUES("4641","es","_WISHLIST","Lista");
INSERT INTO phpn_vocabulary VALUES("4642","de","_WISHLIST","Wunschliste");
INSERT INTO phpn_vocabulary VALUES("4643","en","_ADD_TO_WISHLIST","Add to wishlist");
INSERT INTO phpn_vocabulary VALUES("4644","es","_ADD_TO_WISHLIST","Añadir a la lista de deseos");
INSERT INTO phpn_vocabulary VALUES("4645","de","_ADD_TO_WISHLIST","Zur Wunschliste hinzufügen");
INSERT INTO phpn_vocabulary VALUES("4646","en","_REMOVE_FROM_WISHLIST","Remove from wishlist");
INSERT INTO phpn_vocabulary VALUES("4647","es","_REMOVE_FROM_WISHLIST","Eliminar de la lista de deseos");
INSERT INTO phpn_vocabulary VALUES("4648","de","_REMOVE_FROM_WISHLIST","Von Wunschliste entfernen");
INSERT INTO phpn_vocabulary VALUES("4649","en","_DISCOUNTS","Discounts");
INSERT INTO phpn_vocabulary VALUES("4650","es","_DISCOUNTS","Descuentos");
INSERT INTO phpn_vocabulary VALUES("4651","de","_DISCOUNTS","Rabatte");
INSERT INTO phpn_vocabulary VALUES("4652","en","_DISCOUNT_TYPE","Discount Type");
INSERT INTO phpn_vocabulary VALUES("4653","es","_DISCOUNT_TYPE","Tipo de descuento");
INSERT INTO phpn_vocabulary VALUES("4654","de","_DISCOUNT_TYPE","Rabatt-Art");
INSERT INTO phpn_vocabulary VALUES("4655","en","_NIGHT_3","3rd night");
INSERT INTO phpn_vocabulary VALUES("4656","es","_NIGHT_3","3ª noche");
INSERT INTO phpn_vocabulary VALUES("4657","de","_NIGHT_3","3. Nacht");
INSERT INTO phpn_vocabulary VALUES("4658","en","_NIGHT_4","4th night");
INSERT INTO phpn_vocabulary VALUES("4659","es","_NIGHT_4","4ª noche");
INSERT INTO phpn_vocabulary VALUES("4660","de","_NIGHT_4","4. Nacht");
INSERT INTO phpn_vocabulary VALUES("4661","en","_NIGHT_5","5th night");
INSERT INTO phpn_vocabulary VALUES("4662","es","_NIGHT_5","5ª noche");
INSERT INTO phpn_vocabulary VALUES("4663","de","_NIGHT_5","5. Nacht");
INSERT INTO phpn_vocabulary VALUES("4664","en","_FIXED_PRICE","Fixed Price");
INSERT INTO phpn_vocabulary VALUES("4665","es","_FIXED_PRICE","Precio fijo");
INSERT INTO phpn_vocabulary VALUES("4666","de","_FIXED_PRICE","Festpreis");
INSERT INTO phpn_vocabulary VALUES("4667","en","_PERCENTAGE","Percentage");
INSERT INTO phpn_vocabulary VALUES("4668","es","_PERCENTAGE","Porcentaje");
INSERT INTO phpn_vocabulary VALUES("4669","de","_PERCENTAGE","Prozentsatz");
INSERT INTO phpn_vocabulary VALUES("4670","en","_NOT_LOGGED_ALERT","You must be logged in to perform this operation!");
INSERT INTO phpn_vocabulary VALUES("4671","es","_NOT_LOGGED_ALERT","Usted debe estar conectado para realizar esta operación!");
INSERT INTO phpn_vocabulary VALUES("4672","de","_NOT_LOGGED_ALERT","Sie müssen angemeldet sein, um diese Operation auszuführen!");
INSERT INTO phpn_vocabulary VALUES("4673","en","_ADDED_TO_WISHLIST","Item successfully added to wishlist!");
INSERT INTO phpn_vocabulary VALUES("4674","es","_ADDED_TO_WISHLIST","Éxito añadido a la lista de deseos!");
INSERT INTO phpn_vocabulary VALUES("4675","de","_ADDED_TO_WISHLIST","Erfolgreich in den Wunschliste!");
INSERT INTO phpn_vocabulary VALUES("4676","en","_REMOVED_FROM_WISHLIST","Item removed from wishlist!");
INSERT INTO phpn_vocabulary VALUES("4677","es","_REMOVED_FROM_WISHLIST","Se eliminó el elemento de lista de deseos!");
INSERT INTO phpn_vocabulary VALUES("4678","de","_REMOVED_FROM_WISHLIST","Artikel von der Wunschliste entfernt!");
INSERT INTO phpn_vocabulary VALUES("4679","en","_EX_HOTEL_OR_LOCATION","e.g. hotel, landmark or location");
INSERT INTO phpn_vocabulary VALUES("4680","es","_EX_HOTEL_OR_LOCATION","por ejemplo hotel, sitio destacado o localización");
INSERT INTO phpn_vocabulary VALUES("4681","de","_EX_HOTEL_OR_LOCATION","z.B. Hotel, Sehenswürdigkeit oder Lage");
INSERT INTO phpn_vocabulary VALUES("4682","en","_MASS_MAIL_LOG","Mass Mail Log");
INSERT INTO phpn_vocabulary VALUES("4683","es","_MASS_MAIL_LOG","Registro Electrónico Mas");
INSERT INTO phpn_vocabulary VALUES("4684","de","_MASS_MAIL_LOG","Mas Mail-Protokoll");
INSERT INTO phpn_vocabulary VALUES("4685","en","_ALL_REVIEWS","All Reviews");
INSERT INTO phpn_vocabulary VALUES("4686","es","_ALL_REVIEWS","Todos Los Comentarios");
INSERT INTO phpn_vocabulary VALUES("4687","de","_ALL_REVIEWS","Alle Bewertungen");
INSERT INTO phpn_vocabulary VALUES("4688","en","_MINIMUM_PERIOD_ALERT","The minimum allowed period for search is _DAYS_ days. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4689","es","_MINIMUM_PERIOD_ALERT","El periodo mínimo permitido para la búsqueda es. Por favor, vuelva a introducir.");
INSERT INTO phpn_vocabulary VALUES("4690","de","_MINIMUM_PERIOD_ALERT","Der zulässige Mindestzeitraum für die Suche ist _DAYS_ Tage. Bitte erneut eingeben.");
INSERT INTO phpn_vocabulary VALUES("4691","en","_SELECT_DESTINATION_OR_HOTEL","Destination / Hotel Name");
INSERT INTO phpn_vocabulary VALUES("4692","es","_SELECT_DESTINATION_OR_HOTEL","Destino / Nombre del hotel");
INSERT INTO phpn_vocabulary VALUES("4693","de","_SELECT_DESTINATION_OR_HOTEL","Reiseziel / Hotelname");
INSERT INTO phpn_vocabulary VALUES("4694","en","_YOU_NOT_REVIEW_THIS_HOTEL","Sorry, but you can not leave a review for this hotel, you\'ve never been there");
INSERT INTO phpn_vocabulary VALUES("4695","es","_YOU_NOT_REVIEW_THIS_HOTEL","Lo sentimos, pero no se puede dejar una reseña para este hotel, que nunca ha estado allí");
INSERT INTO phpn_vocabulary VALUES("4696","de","_YOU_NOT_REVIEW_THIS_HOTEL","Sorry, aber Sie können nicht eine Bewertung für dieses Hotel verlassen, die Sie noch nie dort gewesen");
INSERT INTO phpn_vocabulary VALUES("4697","en","_LONG_TERM_STAY_DISCOUNT","Long-Term Stay Discount");
INSERT INTO phpn_vocabulary VALUES("4698","es","_LONG_TERM_STAY_DISCOUNT","Largo tiempo estancia Descuento");
INSERT INTO phpn_vocabulary VALUES("4699","de","_LONG_TERM_STAY_DISCOUNT","Langzeitaufenthalt -Rabatt");
INSERT INTO phpn_vocabulary VALUES("4700","en","_YOU_NOT_DOUBLE_REVIEW","You cannot double-post a review for a hotel");
INSERT INTO phpn_vocabulary VALUES("4701","es","_YOU_NOT_DOUBLE_REVIEW","Puede no doble publicar una reseña de un hotel");
INSERT INTO phpn_vocabulary VALUES("4702","de","_YOU_NOT_DOUBLE_REVIEW","Sie können eine Bewertung für ein Hotel nicht doppelt posten");
INSERT INTO phpn_vocabulary VALUES("4703","en","_LONG_TERM_IF_YOU_BOOK_ROOM","If you book this room for long stay you get");
INSERT INTO phpn_vocabulary VALUES("4704","es","_LONG_TERM_IF_YOU_BOOK_ROOM","If you book this room for long stay you get");
INSERT INTO phpn_vocabulary VALUES("4705","de","_LONG_TERM_IF_YOU_BOOK_ROOM","Wenn Sie das Zimmer für längere Aufenthalte buchen Sie bekommen");
INSERT INTO phpn_vocabulary VALUES("4706","en","_DISCOUNT_FOR_3RD_NIGHT","Discount for 3rd night");
INSERT INTO phpn_vocabulary VALUES("4707","es","_DISCOUNT_FOR_3RD_NIGHT","Descuento 3ª noche");
INSERT INTO phpn_vocabulary VALUES("4708","de","_DISCOUNT_FOR_3RD_NIGHT","Rabatt für 3. Nacht");
INSERT INTO phpn_vocabulary VALUES("4709","en","_DISCOUNT_FOR_4TH_NIGHT","Discount for 4th night");
INSERT INTO phpn_vocabulary VALUES("4710","es","_DISCOUNT_FOR_4TH_NIGHT","Descuento 4ª noche");
INSERT INTO phpn_vocabulary VALUES("4711","de","_DISCOUNT_FOR_4TH_NIGHT","Rabatt für 4. Nacht");
INSERT INTO phpn_vocabulary VALUES("4712","en","_DISCOUNT_FOR_5TH_OR_MORE_NIGHTS","Discount for 5th+ nights");
INSERT INTO phpn_vocabulary VALUES("4713","es","_DISCOUNT_FOR_5TH_OR_MORE_NIGHTS","De descuento para el 5 + noches");
INSERT INTO phpn_vocabulary VALUES("4714","de","_DISCOUNT_FOR_5TH_OR_MORE_NIGHTS","Rabatt für 5. + Nächte");
INSERT INTO phpn_vocabulary VALUES("4715","en","_COORDINATES_CENTER_POINT","Coordinates of Center Point");
INSERT INTO phpn_vocabulary VALUES("4716","es","_COORDINATES_CENTER_POINT","Coordenadas del centro de Punto");
INSERT INTO phpn_vocabulary VALUES("4717","de","_COORDINATES_CENTER_POINT","Koordinaten des Mittelpunktes");
INSERT INTO phpn_vocabulary VALUES("4718","en","_NAME_CENTER_POINT","Name of Center Point");
INSERT INTO phpn_vocabulary VALUES("4719","es","_NAME_CENTER_POINT","Nombrar a un punto central");
INSERT INTO phpn_vocabulary VALUES("4720","de","_NAME_CENTER_POINT","Nennen Center Point");
INSERT INTO phpn_vocabulary VALUES("4721","en","_DISTANCE_TO_CENTER_POINT","Distance to Center Point");
INSERT INTO phpn_vocabulary VALUES("4722","es","_DISTANCE_TO_CENTER_POINT","Distancia de Center Point");
INSERT INTO phpn_vocabulary VALUES("4723","de","_DISTANCE_TO_CENTER_POINT","Abstand zum Mittelpunkt");
INSERT INTO phpn_vocabulary VALUES("4724","en","_GET_DISTANCE","Get Distance");
INSERT INTO phpn_vocabulary VALUES("4725","es","_GET_DISTANCE","Obtener Distancia");
INSERT INTO phpn_vocabulary VALUES("4726","de","_GET_DISTANCE","Holen Entfernung");
INSERT INTO phpn_vocabulary VALUES("4727","en","_TOTAL_FUNDS","Total Funds");
INSERT INTO phpn_vocabulary VALUES("4728","es","_TOTAL_FUNDS","Total de fondos");
INSERT INTO phpn_vocabulary VALUES("4729","de","_TOTAL_FUNDS","Gesamtmittel");
INSERT INTO phpn_vocabulary VALUES("4730","en","_TOTAL_PAYMENTS","Total Payments");
INSERT INTO phpn_vocabulary VALUES("4731","es","_TOTAL_PAYMENTS","Pagos totales");
INSERT INTO phpn_vocabulary VALUES("4732","de","_TOTAL_PAYMENTS","Insgesamt Zahlungen");
INSERT INTO phpn_vocabulary VALUES("4733","en","_STARTING","Starting");
INSERT INTO phpn_vocabulary VALUES("4734","es","_STARTING","Empezando");
INSERT INTO phpn_vocabulary VALUES("4735","de","_STARTING","Ab");
INSERT INTO phpn_vocabulary VALUES("4736","en","_FOUND_VILLAS","Found Villas");
INSERT INTO phpn_vocabulary VALUES("4737","es","_FOUND_VILLAS","Villas encontrados");
INSERT INTO phpn_vocabulary VALUES("4738","de","_FOUND_VILLAS","Gefunden Villas");
INSERT INTO phpn_vocabulary VALUES("4739","en","_FOUND_PROPERTIES","Found Properties");
INSERT INTO phpn_vocabulary VALUES("4740","es","_FOUND_PROPERTIES","Propiedades encontradas");
INSERT INTO phpn_vocabulary VALUES("4741","de","_FOUND_PROPERTIES","Immobilien gefunden");
INSERT INTO phpn_vocabulary VALUES("4742","en","_METERS_SHORTENED","m.");
INSERT INTO phpn_vocabulary VALUES("4743","es","_METERS_SHORTENED","m.");
INSERT INTO phpn_vocabulary VALUES("4744","de","_METERS_SHORTENED","m.");
INSERT INTO phpn_vocabulary VALUES("4745","en","_KILOMETERS_SHORTENED","km.");
INSERT INTO phpn_vocabulary VALUES("4746","es","_KILOMETERS_SHORTENED","km.");
INSERT INTO phpn_vocabulary VALUES("4747","de","_KILOMETERS_SHORTENED","km.");
INSERT INTO phpn_vocabulary VALUES("4748","en","_DISTANCE_OF_HOTEL_FROM_CENTER_POINT","Distance of the hotel form the {name_center_point} is {distance_center_point}");
INSERT INTO phpn_vocabulary VALUES("4749","es","_DISTANCE_OF_HOTEL_FROM_CENTER_POINT","Distancia del hotel de la {name_center_point} es {distance_center_point}");
INSERT INTO phpn_vocabulary VALUES("4750","de","_DISTANCE_OF_HOTEL_FROM_CENTER_POINT","Die Entfernung des Hotels vom {name_center_point} ist {distance_center_point}");



DROP TABLE IF EXISTS phpn_wishlist;

CREATE TABLE `phpn_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - hotel, 2 - room, 3 - car',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `item_id` (`item_id`),
  KEY `item_type` (`item_type`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_wishlist VALUES("1","1","1","1","2016-04-25 00:00:00");



