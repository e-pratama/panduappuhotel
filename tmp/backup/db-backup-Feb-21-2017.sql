DROP TABLE IF EXISTS phpn_accounts;

CREATE TABLE `phpn_accounts` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(70) CHARACTER SET latin1 NOT NULL,
  `account_type` enum('owner','mainadmin','admin','hotelowner','agencyowner') CHARACTER SET latin1 NOT NULL DEFAULT 'mainadmin',
  `companies` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `preferred_language` varchar(2) CHARACTER SET latin1 NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_accounts VALUES("1","Dustin","D","Pandu2016","êL£Â Dv•>¿û´æs‚´h30jýQ^GÏÐK;%","harfinovian@yahoo.com","owner","","en","0000-00-00 00:00:00","2017-02-21 08:06:51","1");
INSERT INTO phpn_accounts VALUES("2","Pandu","KetuaPH","ketuaph",")Iå®m…œáWcZj0","ketuaph@pandu-hotel.com","hotelowner","","en","2017-02-11 10:19:53","0000-00-00 00:00:00","1");



DROP TABLE IF EXISTS phpn_banlist;

CREATE TABLE `phpn_banlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ban_item` varchar(70) CHARACTER SET latin1 NOT NULL,
  `ban_item_type` enum('IP','Email') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IP',
  `ban_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ban_ip` (`ban_item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_banners;

CREATE TABLE `phpn_banners` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `image_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image_file_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` tinyint(1) NOT NULL DEFAULT '0',
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `priority_order` (`priority_order`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_banners VALUES("14","","zottkddshtzs9hl7osca.jpg","zottkddshtzs9hl7osca_thumb.jpg","9","","1");
INSERT INTO phpn_banners VALUES("15","","mark91uue8aqq2n72uo1.jpg","mark91uue8aqq2n72uo1_thumb.jpg","10","","1");
INSERT INTO phpn_banners VALUES("4","check_hotels","qs80zz4tc7eu0v9p688u.jpg","qs80zz4tc7eu0v9p688u_thumb.jpg","4","","0");
INSERT INTO phpn_banners VALUES("5","check_hotels","egkfkiris0uw320kfuam.jpg","egkfkiris0uw320kfuam_thumb.jpg","5","","0");
INSERT INTO phpn_banners VALUES("6","check_hotels","ejqc949g9ei0mmlf6rib.jpg","ejqc949g9ei0mmlf6rib_thumb.jpg","6","","0");
INSERT INTO phpn_banners VALUES("7","check_hotels","xc6rigv51e0tkz96we95.jpg","xc6rigv51e0tkz96we95_thumb.jpg","7","","0");
INSERT INTO phpn_banners VALUES("8","check_hotels","eb7i97qa7dkzjv2v3hfd.jpg","eb7i97qa7dkzjv2v3hfd_thumb.jpg","8","","0");



DROP TABLE IF EXISTS phpn_banners_description;

CREATE TABLE `phpn_banners_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `banner_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `image_text` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_banners_description VALUES("10","4","en","<span class=\"lato size28 slim caps white\">Italy</span><br><br><br><span class=\"lato size100 slim caps white\">Rome</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("14","5","en","<span class=\"lato size28 slim caps white\">France</span><br><br><br><span class=\"lato size100 slim caps white\">Paris</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("17","6","en","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Zakynthos</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("48","8","ID","<span class=\"lato size18 slim caps white\">Islands</span><br><br><br><span class=\"lato size65 slim caps white\">Bahamas</span><br><span class=\"lato size13 normal caps white\">from</span><br><br><span class=\"lato size40 slim caps yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("20","7","en","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("47","7","ID","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Santorini</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1450</span>");
INSERT INTO phpn_banners_description VALUES("23","8","en","<span class=\"lato size18 slim caps white\">Islands</span><br><br><br><span class=\"lato size65 slim caps white\">Bahamas</span><br><span class=\"lato size13 normal caps white\">from</span><br><br><span class=\"lato size40 slim caps yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("46","6","ID","<span class=\"lato size28 slim caps white\">Greece</span><br><br><br><span class=\"lato size100 slim caps white\">Zakynthos</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("45","5","ID","<span class=\"lato size28 slim caps white\">France</span><br><br><br><span class=\"lato size100 slim caps white\">Paris</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1300</span>");
INSERT INTO phpn_banners_description VALUES("44","4","ID","<span class=\"lato size28 slim caps white\">Italy</span><br><br><br><span class=\"lato size100 slim caps white\">Rome</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">$1500</span>");
INSERT INTO phpn_banners_description VALUES("54","14","en","<span class=\"lato size28 slim caps white\">Parapat</span><br><br><br><span class=\"lato size100 slim caps white\">Sumut</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">RP320K</span>");
INSERT INTO phpn_banners_description VALUES("55","14","ID","<span class=\"lato size28 slim caps white\">Parapat</span><br><br><br><span class=\"lato size100 slim caps white\">Sumut</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">RP320K</span>");
INSERT INTO phpn_banners_description VALUES("56","15","en","<span class=\"lato size28 slim caps white\">Tuktuk</span><br><br><br><span class=\"lato size100 slim caps white\">Samosir</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">RP350K</span>");
INSERT INTO phpn_banners_description VALUES("57","15","ID","<span class=\"lato size28 slim caps white\">Tuktuk</span><br><br><br><span class=\"lato size100 slim caps white\">Samosir</span><br><span class=\"lato size20 normal caps white\">from</span><br><br><span class=\"lato size48 slim uppercase yellow\">RP350K</span>");



DROP TABLE IF EXISTS phpn_bookings;

CREATE TABLE `phpn_bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_number` varchar(20) CHARACTER SET latin1 NOT NULL,
  `hotel_reservation_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `booking_description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `discount_percent` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `discount_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pre_payment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pre_payment_value` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_percent` decimal(5,3) unsigned NOT NULL DEFAULT '0.000',
  `initial_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `guest_tax` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `payment_sum` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `additional_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'USD',
  `rooms_amount` tinyint(4) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `is_admin_reservation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `transaction_number` varchar(30) CHARACTER SET latin1 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - POA, 1 - Online Order, 2 - PayPal, 3 - 2CO, 4 - Authorize.Net, 5 - Bank Transfer, 6 - Account Balance',
  `payment_method` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - Payment Company Account, 1 - Credit Card, 2 - E-Check',
  `coupon_code` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `discount_campaign_id` int(10) DEFAULT '0',
  `additional_info` text COLLATE utf8_unicode_ci NOT NULL,
  `extras` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extras_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `cc_type` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_holder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cc_number` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cc_expires_month` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_expires_year` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_cvv_code` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - prebooking, 1 - pending, 2 - reserved, 3 - completed, 4 - refunded, 5 - payment error, 6 - canceled',
  `status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email_sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `payment_type` (`payment_type`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_bookings VALUES("1","SL2G8HU8CL","","Rooms Reservation","0.00","0.00","500000.00","full price","0.00","0.00","0.000","0.00","0.00","500000.00","0.00","IDR","1","1","1","","2017-02-11 11:09:00","0000-00-00 00:00:00","0","0","","0","","a:0:{}","0.00","","","´h30jýQ^GÏÐK;%","","","´h30jýQ^GÏÐK;%","2","2017-02-11 11:09:04","","0");
INSERT INTO phpn_bookings VALUES("2","VA768Z5JTN","","Rooms Reservation","50.00","297500.00","595000.00","full price","0.00","0.00","0.000","0.00","0.00","297500.00","0.00","IDR","1","5","0","","2017-02-17 10:46:00","2017-02-17 13:12:49","0","0","RP7A-VJZC-T0BH-U778","0","","a:0:{}","0.00","","","´h30jýQ^GÏÐK;%","","","´h30jýQ^GÏÐK;%","3","2017-02-17 10:46:05","","1");



DROP TABLE IF EXISTS phpn_bookings_rooms;

CREATE TABLE `phpn_bookings_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `room_id` int(11) NOT NULL DEFAULT '0',
  `room_numbers` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `checkin` date DEFAULT NULL,
  `checkout` date DEFAULT NULL,
  `adults` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `children` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rooms` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `extra_beds` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extra_beds_charge` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `meal_plan_id` int(11) unsigned NOT NULL DEFAULT '0',
  `meal_plan_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `booking_number` (`booking_number`),
  KEY `room_id` (`room_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_bookings_rooms VALUES("1","SL2G8HU8CL","5","10","","2017-02-11","2017-02-12","1","0","1","350000.00","0","0.00","8","150000.00");
INSERT INTO phpn_bookings_rooms VALUES("2","VA768Z5JTN","5","11","","2017-02-17","2017-02-18","1","0","1","320000.00","1","125000.00","8","150000.00");



DROP TABLE IF EXISTS phpn_campaigns;

CREATE TABLE `phpn_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `campaign_type` enum('global','standard') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `campaign_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `discount_percent` decimal(5,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `target_group_id` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_campaigns VALUES("1","1","0","global","Campaign #4 Sep 2015","2015-09-07","2015-10-07","10.00","1");
INSERT INTO phpn_campaigns VALUES("2","5","1","standard","Campaign #_ Feb 2017","2017-01-01","2017-01-04","40.00","1");



DROP TABLE IF EXISTS phpn_car_agencies;

CREATE TABLE `phpn_car_agencies` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logo_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `map_code` text COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies VALUES("1","1-800-123-4567","1-800-123-6789","info@car_agency1.com","company_logo_1.png","<iframe width=\"100%\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Hotel+Circle+South,+San+Diego,+California,+United+States&aq=1&sll=33.981232,-84.173813&sspn=0.12868,0.307274&ie=UTF8&hq=&hnear=Hotel+Cir+S,+San+Diego,+California&ll=32.759,-117.177036&spn=0.017215,0.038409&z=14&output=embed\"></iframe>","0","1");
INSERT INTO phpn_car_agencies VALUES("2","1-800-123-2222","1-800-123-2223","info@car_agency2.com","company_logo_2.png","<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2948.0965968856144!2d-71.0666208!3d42.361780599999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e3709a3c7713c5%3A0x922cf2c02c6d4e5a!2s5+Blossom+St%2C+Charles+River+Plaza+Shopping+Center%2C+Boston%2C+MA+02114!5e0!3m2!1sen!2s!4v1405938488556\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\"></iframe>","0","1");



DROP TABLE IF EXISTS phpn_car_agencies_description;

CREATE TABLE `phpn_car_agencies_description` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `terms_and_conditions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies_description VALUES("1","1","en","Transportation Agency 1","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("4","2","en","Transportation Agency 2","address here...","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","<h3>Excess Protection - Excellent Value, Best Cover, Peace of Mind</h3><p>When you pick up your car a deposit is usually held on your card for your excess which may be charged if the vehicle is damaged. Take our Damage Excess Refund and if you have any bumps or scrapes we\'ll refund any deductions from your excess.<br /><br /></p><div class=\"hpadding20\"><ul class=\"checklist\"><li><span class=\"bold green\">You\'re protected</span> - Damage Excess Refund protects you when things go wrong and you are required to pay out from your excess</li><li><span class=\"bold green\">Accidents covered</span> - Your excess will be protected if your vehicle is damaged in an accident</li><li><span class=\"bold green\">Damage cover</span> - Damage to bodywork, vehicle parts and accessories incurred during most driving incidents is covered.</li></ul></div>");
INSERT INTO phpn_car_agencies_description VALUES("7","1","ID","Transportation Agency 1","","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","");
INSERT INTO phpn_car_agencies_description VALUES("8","2","ID","Transportation Agency 2","","<h3>We give you the following for free</h3><div class=\"col-md-6\"><ul class=\"checklist\"><li>Amendments</li><li>Theft Protection</li><li>Direct-dial phone</li><li>Collision Damage Waiver</li></ul></div><div class=\"col-md-6\"><ul class=\"checklist\"><li>Unlimited Kilometres for ALL car groups</li><li>Local Taxes</li><li>PRoad Fee</li><li>First Additional Driver</li></ul></div>","");



DROP TABLE IF EXISTS phpn_car_agencies_locations;

CREATE TABLE `phpn_car_agencies_locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies_locations VALUES("1","US","2","1");
INSERT INTO phpn_car_agencies_locations VALUES("2","US","3","1");



DROP TABLE IF EXISTS phpn_car_agencies_locations_description;

CREATE TABLE `phpn_car_agencies_locations_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_location_id` (`agency_location_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agencies_locations_description VALUES("1","1","en","Boston");
INSERT INTO phpn_car_agencies_locations_description VALUES("8","2","ID","New York City");
INSERT INTO phpn_car_agencies_locations_description VALUES("4","2","en","New York City");
INSERT INTO phpn_car_agencies_locations_description VALUES("7","1","ID","Boston");



DROP TABLE IF EXISTS phpn_car_agency_locations;

CREATE TABLE `phpn_car_agency_locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `agency_location_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_locations VALUES("1","1","1","0","1");
INSERT INTO phpn_car_agency_locations VALUES("2","1","2","1","1");
INSERT INTO phpn_car_agency_locations VALUES("3","2","1","1","1");



DROP TABLE IF EXISTS phpn_car_agency_makes;

CREATE TABLE `phpn_car_agency_makes` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_makes VALUES("1","1","0");
INSERT INTO phpn_car_agency_makes VALUES("2","1","1");
INSERT INTO phpn_car_agency_makes VALUES("3","1","2");
INSERT INTO phpn_car_agency_makes VALUES("4","1","3");
INSERT INTO phpn_car_agency_makes VALUES("5","1","4");
INSERT INTO phpn_car_agency_makes VALUES("6","1","5");
INSERT INTO phpn_car_agency_makes VALUES("7","1","6");
INSERT INTO phpn_car_agency_makes VALUES("8","1","7");
INSERT INTO phpn_car_agency_makes VALUES("9","1","8");
INSERT INTO phpn_car_agency_makes VALUES("10","1","9");
INSERT INTO phpn_car_agency_makes VALUES("11","1","10");
INSERT INTO phpn_car_agency_makes VALUES("12","1","11");
INSERT INTO phpn_car_agency_makes VALUES("13","1","12");
INSERT INTO phpn_car_agency_makes VALUES("14","2","0");
INSERT INTO phpn_car_agency_makes VALUES("15","2","1");
INSERT INTO phpn_car_agency_makes VALUES("16","2","2");
INSERT INTO phpn_car_agency_makes VALUES("17","2","3");
INSERT INTO phpn_car_agency_makes VALUES("18","2","4");
INSERT INTO phpn_car_agency_makes VALUES("19","2","5");
INSERT INTO phpn_car_agency_makes VALUES("20","2","6");
INSERT INTO phpn_car_agency_makes VALUES("21","2","7");
INSERT INTO phpn_car_agency_makes VALUES("22","2","8");
INSERT INTO phpn_car_agency_makes VALUES("23","2","9");
INSERT INTO phpn_car_agency_makes VALUES("24","2","10");
INSERT INTO phpn_car_agency_makes VALUES("25","2","11");
INSERT INTO phpn_car_agency_makes VALUES("26","2","12");
INSERT INTO phpn_car_agency_makes VALUES("27","3","0");
INSERT INTO phpn_car_agency_makes VALUES("28","3","1");
INSERT INTO phpn_car_agency_makes VALUES("29","3","2");
INSERT INTO phpn_car_agency_makes VALUES("30","3","3");
INSERT INTO phpn_car_agency_makes VALUES("31","3","4");
INSERT INTO phpn_car_agency_makes VALUES("32","3","5");
INSERT INTO phpn_car_agency_makes VALUES("33","3","6");
INSERT INTO phpn_car_agency_makes VALUES("34","3","7");
INSERT INTO phpn_car_agency_makes VALUES("35","3","8");
INSERT INTO phpn_car_agency_makes VALUES("36","3","9");
INSERT INTO phpn_car_agency_makes VALUES("37","3","10");
INSERT INTO phpn_car_agency_makes VALUES("38","3","11");
INSERT INTO phpn_car_agency_makes VALUES("39","3","12");



DROP TABLE IF EXISTS phpn_car_agency_makes_description;

CREATE TABLE `phpn_car_agency_makes_description` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `make_id` smallint(6) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `make_id` (`make_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_makes_description VALUES("1","1","en","Audi");
INSERT INTO phpn_car_agency_makes_description VALUES("2","2","en","BMW");
INSERT INTO phpn_car_agency_makes_description VALUES("3","3","en","Dodge");
INSERT INTO phpn_car_agency_makes_description VALUES("4","4","en","GMC");
INSERT INTO phpn_car_agency_makes_description VALUES("5","5","en","Jeep");
INSERT INTO phpn_car_agency_makes_description VALUES("6","6","en","Ford");
INSERT INTO phpn_car_agency_makes_description VALUES("7","7","en","Opel");
INSERT INTO phpn_car_agency_makes_description VALUES("8","8","en","Mazda");
INSERT INTO phpn_car_agency_makes_description VALUES("9","9","en","Mercedes-Benz");
INSERT INTO phpn_car_agency_makes_description VALUES("10","10","en","Mitsubishi");
INSERT INTO phpn_car_agency_makes_description VALUES("11","11","en","Renault");
INSERT INTO phpn_car_agency_makes_description VALUES("12","12","en","Subaru");
INSERT INTO phpn_car_agency_makes_description VALUES("13","13","en","Toyota");
INSERT INTO phpn_car_agency_makes_description VALUES("14","1","es","Audi");
INSERT INTO phpn_car_agency_makes_description VALUES("15","2","es","BMW");
INSERT INTO phpn_car_agency_makes_description VALUES("16","3","es","Dodge");
INSERT INTO phpn_car_agency_makes_description VALUES("17","4","es","GMC");
INSERT INTO phpn_car_agency_makes_description VALUES("18","5","es","Jeep");
INSERT INTO phpn_car_agency_makes_description VALUES("19","6","es","Ford");
INSERT INTO phpn_car_agency_makes_description VALUES("20","7","es","Opel");
INSERT INTO phpn_car_agency_makes_description VALUES("21","8","es","Mazda");
INSERT INTO phpn_car_agency_makes_description VALUES("22","9","es","Mercedes-Benz");
INSERT INTO phpn_car_agency_makes_description VALUES("23","10","es","Mitsubishi");
INSERT INTO phpn_car_agency_makes_description VALUES("24","11","es","Renault");
INSERT INTO phpn_car_agency_makes_description VALUES("25","12","es","Subaru");
INSERT INTO phpn_car_agency_makes_description VALUES("26","13","es","Toyota");
INSERT INTO phpn_car_agency_makes_description VALUES("27","1","de","Audi");
INSERT INTO phpn_car_agency_makes_description VALUES("28","2","de","BMW");
INSERT INTO phpn_car_agency_makes_description VALUES("29","3","de","Dodge");
INSERT INTO phpn_car_agency_makes_description VALUES("30","4","de","GMC");
INSERT INTO phpn_car_agency_makes_description VALUES("31","5","de","Jeep");
INSERT INTO phpn_car_agency_makes_description VALUES("32","6","de","Ford");
INSERT INTO phpn_car_agency_makes_description VALUES("33","7","de","Opel");
INSERT INTO phpn_car_agency_makes_description VALUES("34","8","de","Mazda");
INSERT INTO phpn_car_agency_makes_description VALUES("35","9","de","Mercedes-Benz");
INSERT INTO phpn_car_agency_makes_description VALUES("36","10","de","Mitsubishi");
INSERT INTO phpn_car_agency_makes_description VALUES("37","11","de","Renault");
INSERT INTO phpn_car_agency_makes_description VALUES("38","12","de","Subaru");
INSERT INTO phpn_car_agency_makes_description VALUES("39","13","de","Toyota");



DROP TABLE IF EXISTS phpn_car_agency_payment_gateways;

CREATE TABLE `phpn_car_agency_payment_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL DEFAULT '0',
  `payment_type` varchar(20) NOT NULL DEFAULT '',
  `payment_type_name` varchar(50) NOT NULL DEFAULT '',
  `api_login` varchar(40) NOT NULL DEFAULT '',
  `api_key` varchar(40) NOT NULL DEFAULT '',
  `payment_info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO phpn_car_agency_payment_gateways VALUES("1","1","paypal","PayPal","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("2","1","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("3","1","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("4","1","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_car_agency_payment_gateways VALUES("5","2","paypal","PayPal","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("6","2","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("7","2","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_car_agency_payment_gateways VALUES("8","2","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");



DROP TABLE IF EXISTS phpn_car_agency_vehicle_categories;

CREATE TABLE `phpn_car_agency_vehicle_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicle_categories VALUES("1","0","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("2","1","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("3","2","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("4","3","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("5","4","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("6","5","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("7","6","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("8","7","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("9","8","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("10","9","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("11","10","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("12","11","1");
INSERT INTO phpn_car_agency_vehicle_categories VALUES("13","12","1");



DROP TABLE IF EXISTS phpn_car_agency_vehicle_categories_description;

CREATE TABLE `phpn_car_agency_vehicle_categories_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_vehicle_category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_vehicle_category_id` (`agency_vehicle_category_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("1","1","en","Economy","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("45","9","ID","Minivan","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("4","2","en","Compact","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("7","3","en","Midsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("44","8","ID","Convertible","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("10","4","en","Standard","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("43","7","ID","Luxury","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("13","5","en","Fullsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("42","6","ID","Premium","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("16","6","en","Premium","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("19","7","en","Luxury","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("41","5","ID","Fullsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("22","8","en","Convertible","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("25","9","en","Minivan","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("40","4","ID","Standard","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("28","10","en","Minibus","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("48","13","ID","Sport Car","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("39","3","ID","Midsize","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("31","11","en","Sport Utility","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("47","11","ID","Sport Utility","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("38","2","ID","Compact","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("34","13","en","Sport Car","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("46","10","ID","Minibus","");
INSERT INTO phpn_car_agency_vehicle_categories_description VALUES("37","1","ID","Economy","");



DROP TABLE IF EXISTS phpn_car_agency_vehicle_types;

CREATE TABLE `phpn_car_agency_vehicle_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `vehicle_category_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_thumb` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `passengers` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `luggages` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `doors` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicle_types VALUES("1","1","1","vehicle_type_1.jpg","vehicle_type_1_thumb.jpg","5","5","4","0","1");
INSERT INTO phpn_car_agency_vehicle_types VALUES("2","1","2","vehicle_type_2.jpg","vehicle_type_2_thumb.jpg","5","5","4","1","1");
INSERT INTO phpn_car_agency_vehicle_types VALUES("3","2","2","vehicle_type_3.jpg","vehicle_type_3_thumb.jpg","5","4","4","2","1");



DROP TABLE IF EXISTS phpn_car_agency_vehicles;

CREATE TABLE `phpn_car_agency_vehicles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `vehicle_type_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `make_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `registration_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mileage` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fuel_type_id` tinyint(2) NOT NULL,
  `transmission` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_thumb` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price_per_day` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `price_per_hour` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `default_distance` int(10) unsigned NOT NULL DEFAULT '0',
  `distance_extra_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicles VALUES("1","1","1","1","730i","AB 5632","30000","1","automatic","vehicle_1.jpg","vehicle_1_thumb.jpg","312.00","13.00","150","14.00","0","1");
INSERT INTO phpn_car_agency_vehicles VALUES("2","1","2","8","6","12345-AA","20000","1","automatic","vehicle_2.jpg","vehicle_2_thumb.jpg","288.00","12.00","150","15.00","1","1");
INSERT INTO phpn_car_agency_vehicles VALUES("3","1","2","6","Fiesta","AB1 888C","30000","1","automatic","vehicle_3.jpg","vehicle_3_thumb.jpg","336.00","14.00","150","15.00","2","1");
INSERT INTO phpn_car_agency_vehicles VALUES("4","2","1","6","Mondeo","AB123 888","20000","2","manual","vehicle_4.jpg","vehicle_4_thumb.jpg","336.00","14.00","150","15.00","3","1");
INSERT INTO phpn_car_agency_vehicles VALUES("5","2","2","6","Ka","ABV 123","250000","2","automatic","vehicle_5.jpg","vehicle_5_thumb.jpg","336.00","14.00","150","15.00","4","1");



DROP TABLE IF EXISTS phpn_car_agency_vehicles_description;

CREATE TABLE `phpn_car_agency_vehicles_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `agency_vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_vehicle_id` (`agency_vehicle_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_agency_vehicles_description VALUES("1","1","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("2","1","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("3","1","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("4","2","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("5","2","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("6","2","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("7","3","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("8","3","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("9","3","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo. Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("10","4","en","Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("11","4","es","Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("12","4","de","Mauris metus justo, rhoncus non ipsum ut, sollicitudin interdum ligula.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("13","5","en","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("14","5","es","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo.");
INSERT INTO phpn_car_agency_vehicles_description VALUES("15","5","de","Etiam faucibus neque sem. Duis quis orci aliquet, porta sem eu, venenatis leo.");



DROP TABLE IF EXISTS phpn_car_reservations;

CREATE TABLE `phpn_car_reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `car_agency_id` int(11) NOT NULL DEFAULT '0',
  `car_vehicle_type_id` int(11) NOT NULL DEFAULT '0',
  `car_vehicle_id` int(11) NOT NULL DEFAULT '0',
  `location_from_id` int(11) NOT NULL DEFAULT '0',
  `location_to_id` int(11) NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `reservation_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reservation_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pre_payment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'full price',
  `pre_payment_value` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_percent` decimal(5,3) unsigned NOT NULL DEFAULT '0.000',
  `extras` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extras_fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `reservation_total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reservation_paid` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `additional_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `is_admin_reservation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `payment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - POA, 1 - Online Order, 2 - PayPal, 3 - 2CO, 4 - Authorize.Net, 5 - Bank Transfer, 6 - Account Balance',
  `payment_method` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - Payment Company Account, 1 - Credit Card, 2 - E-Check',
  `transaction_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `additional_info` text COLLATE utf8_unicode_ci NOT NULL,
  `cc_type` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_holder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cc_number` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cc_expires_month` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_expires_year` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `cc_cvv_code` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - preordering, 1 - pending, 2 - reserved, 3 - completed, 4 - refunded, 5 - payment error, 6 - canceled',
  `status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email_sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `payment_type` (`payment_type`),
  KEY `car_agency_id` (`car_agency_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_car_reservations VALUES("1","12WS34ED99","1","1","1","0","0","2016-03-11","12:00:00","2016-03-11","15:00:00","Car Rental","42.00","full price","0.00","0.00","0.000","","0.00","42.00","0.00","0.00","USD","1","0","0000-00-00 00:00:00","0","0","","","","","","","","","1","0000-00-00 00:00:00","","0","2016-03-09 00:00:00");
INSERT INTO phpn_car_reservations VALUES("2","RRW5T4ED67","2","2","2","0","0","2016-03-12","11:00:00","2016-03-12","12:00:00","Car Rental","42.00","full price","0.00","0.00","0.000","","0.00","42.00","0.00","0.00","USD","1","0","0000-00-00 00:00:00","0","0","","","","","","","","","1","0000-00-00 00:00:00","","0","2016-03-09 00:00:00");



DROP TABLE IF EXISTS phpn_comments;

CREATE TABLE `phpn_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(70) CHARACTER SET latin1 NOT NULL,
  `comment_text` text COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`,`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_comments VALUES("1","25","1","customer1","","Thanks! Very cool site.","2013-11-12 14:34:40","1","2013-11-12 14:34:47");



DROP TABLE IF EXISTS phpn_countries;

CREATE TABLE `phpn_countries` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `abbrv` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vat_value` decimal(5,3) NOT NULL DEFAULT '0.000',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `abbrv` (`abbrv`)
) ENGINE=MyISAM AUTO_INCREMENT=238 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_countries VALUES("1","AF","Afghanistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("2","AL","Albania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("3","DZ","Algeria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("4","AS","American Samoa","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("5","AD","Andorra","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("6","AO","Angola","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("7","AI","Anguilla","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("8","AQ","Antarctica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("9","AG","Antigua and Barbuda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("10","AR","Argentina","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("11","AM","Armenia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("12","AW","Aruba","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("13","AU","Australia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("14","AT","Austria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("15","AZ","Azerbaijan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("16","BS","Bahamas","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("17","BH","Bahrain","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("18","BD","Bangladesh","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("19","BB","Barbados","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("20","BY","Belarus","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("21","BE","Belgium","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("22","BZ","Belize","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("23","BJ","Benin","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("24","BM","Bermuda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("25","BT","Bhutan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("26","BO","Bolivia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("27","BA","Bosnia and Herzegowina","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("28","BW","Botswana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("29","BV","Bouvet Island","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("30","BR","Brazil","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("31","IO","British Indian Ocean Territory","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("32","VG","British Virgin Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("33","BN","Brunei Darussalam","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("34","BG","Bulgaria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("35","BF","Burkina Faso","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("36","BI","Burundi","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("37","KH","Cambodia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("38","CM","Cameroon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("39","CA","Canada","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("40","CV","Cape Verde","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("41","KY","Cayman Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("42","CF","Central African Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("43","TD","Chad","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("44","CL","Chile","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("45","CN","China","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("46","CX","Christmas Island","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("47","CC","Cocos (Keeling) Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("48","CO","Colombia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("49","KM","Comoros","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("50","CG","Congo","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("51","CK","Cook Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("52","CR","Costa Rica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("53","CI","Cote D\'ivoire","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("54","HR","Croatia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("55","CU","Cuba","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("56","CY","Cyprus","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("57","CZ","Czech Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("58","DK","Denmark","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("59","DJ","Djibouti","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("60","DM","Dominica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("61","DO","Dominican Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("62","TP","East Timor","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("63","EC","Ecuador","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("64","EG","Egypt","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("65","SV","El Salvador","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("66","GQ","Equatorial Guinea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("67","ER","Eritrea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("68","EE","Estonia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("69","ET","Ethiopia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("70","FK","Falkland Islands (Malvinas)","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("71","FO","Faroe Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("72","FJ","Fiji","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("73","FI","Finland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("74","FR","France","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("75","GF","French Guiana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("76","PF","French Polynesia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("77","TF","French Southern Territories","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("78","GA","Gabon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("79","GM","Gambia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("80","GE","Georgia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("81","DE","Germany","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("82","GH","Ghana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("83","GI","Gibraltar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("84","GR","Greece","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("85","GL","Greenland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("86","GD","Grenada","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("87","GP","Guadeloupe","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("88","GU","Guam","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("89","GT","Guatemala","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("90","GN","Guinea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("91","GW","Guinea-Bissau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("92","GY","Guyana","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("93","HT","Haiti","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("94","HM","Heard and McDonald Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("95","HN","Honduras","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("96","HK","Hong Kong","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("97","HU","Hungary","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("98","IS","Iceland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("99","IN","India","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("100","ID","Indonesia","1","1","0.000","0");
INSERT INTO phpn_countries VALUES("101","IQ","Iraq","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("102","IE","Ireland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("103","IR","Islamic Republic of Iran","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("104","IL","Israel","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("105","IT","Italy","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("106","JM","Jamaica","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("107","JP","Japan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("108","JO","Jordan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("109","KZ","Kazakhstan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("110","KE","Kenya","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("111","KI","Kiribati","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("112","KP","Korea, Dem. Peoples Rep of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("113","KR","Korea, Republic of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("114","KW","Kuwait","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("115","KG","Kyrgyzstan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("116","LA","Laos","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("117","LV","Latvia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("118","LB","Lebanon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("119","LS","Lesotho","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("120","LR","Liberia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("121","LY","Libyan Arab Jamahiriya","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("122","LI","Liechtenstein","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("123","LT","Lithuania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("124","LU","Luxembourg","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("125","MO","Macau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("126","MK","Macedonia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("127","MG","Madagascar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("128","MW","Malawi","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("129","MY","Malaysia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("130","MV","Maldives","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("131","ML","Mali","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("132","MT","Malta","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("133","MH","Marshall Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("134","MQ","Martinique","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("135","MR","Mauritania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("136","MU","Mauritius","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("137","YT","Mayotte","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("138","MX","Mexico","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("139","FM","Micronesia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("140","MD","Moldova, Republic of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("141","MC","Monaco","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("142","MN","Mongolia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("143","MS","Montserrat","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("144","MA","Morocco","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("145","MZ","Mozambique","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("146","MM","Myanmar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("147","NA","Namibia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("148","NR","Nauru","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("149","NP","Nepal","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("150","NL","Netherlands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("151","AN","Netherlands Antilles","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("152","NC","New Caledonia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("153","NZ","New Zealand","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("154","NI","Nicaragua","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("155","NE","Niger","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("156","NG","Nigeria","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("157","NU","Niue","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("158","NF","Norfolk Island","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("159","MP","Northern Mariana Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("160","NO","Norway","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("161","OM","Oman","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("162","PK","Pakistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("163","PW","Palau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("164","PA","Panama","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("165","PG","Papua New Guinea","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("166","PY","Paraguay","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("167","PE","Peru","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("168","PH","Philippines","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("169","PN","Pitcairn","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("170","PL","Poland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("171","PT","Portugal","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("172","PR","Puerto Rico","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("173","QA","Qatar","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("174","RE","Reunion","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("175","RO","Romania","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("176","RU","Russian Federation","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("177","RW","Rwanda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("178","LC","Saint Lucia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("179","WS","Samoa","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("180","SM","San Marino","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("181","ST","Sao Tome and Principe","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("182","SA","Saudi Arabia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("183","SN","Senegal","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("184","RS","Republic of Serbia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("185","SC","Seychelles","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("186","SL","Sierra Leone","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("187","SG","Singapore","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("188","SK","Slovakia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("189","SI","Slovenia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("190","SB","Solomon Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("191","SO","Somalia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("192","ZA","South Africa","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("193","ES","Spain","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("194","LK","Sri Lanka","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("195","SH","St. Helena","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("196","KN","St. Kitts and Nevis","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("197","PM","St. Pierre and Miquelon","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("198","VC","St. Vincent and the Grenadines","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("199","SD","Sudan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("200","SR","Suriname","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("201","SJ","Svalbard and Jan Mayen Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("202","SZ","Swaziland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("203","SE","Sweden","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("204","CH","Switzerland","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("205","SY","Syrian Arab Republic","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("206","TW","Taiwan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("207","TJ","Tajikistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("208","TZ","Tanzania, United Republic of","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("209","TH","Thailand","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("210","TG","Togo","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("211","TK","Tokelau","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("212","TO","Tonga","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("213","TT","Trinidad and Tobago","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("214","TN","Tunisia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("215","TR","Turkey","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("216","TM","Turkmenistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("217","TC","Turks and Caicos Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("218","TV","Tuvalu","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("219","UG","Uganda","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("220","UA","Ukraine","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("221","AE","United Arab Emirates","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("222","GB","United Kingdom (GB)","1","0","0.000","999");
INSERT INTO phpn_countries VALUES("224","US","United States","1","0","0.000","1000");
INSERT INTO phpn_countries VALUES("225","VI","United States Virgin Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("226","UY","Uruguay","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("227","UZ","Uzbekistan","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("228","VU","Vanuatu","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("229","VA","Vatican City State","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("230","VE","Venezuela","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("231","VN","Vietnam","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("232","WF","Wallis And Futuna Islands","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("233","EH","Western Sahara","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("234","YE","Yemen","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("235","ZR","Zaire","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("236","ZM","Zambia","1","0","0.000","0");
INSERT INTO phpn_countries VALUES("237","ZW","Zimbabwe","1","0","0.000","0");



DROP TABLE IF EXISTS phpn_coupons;

CREATE TABLE `phpn_coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `coupon_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_started` date NOT NULL,
  `date_finished` date NOT NULL,
  `discount_percent` tinyint(2) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_coupons VALUES("1","0","0","RP7A-VJZC-T0BH-U778","2017-01-01","2017-04-10","50","Special Discount","1");
INSERT INTO phpn_coupons VALUES("2","0","0","NK2H-845G-69ZV-LW6Y","2017-01-13","2017-01-17","10","Diskon Week Days","1");



DROP TABLE IF EXISTS phpn_currencies;

CREATE TABLE `phpn_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(3) CHARACTER SET latin1 NOT NULL,
  `rate` double(10,4) NOT NULL DEFAULT '1.0000',
  `decimals` tinyint(1) NOT NULL DEFAULT '2',
  `symbol_placement` enum('before','after') CHARACTER SET latin1 NOT NULL DEFAULT 'before',
  `primary_order` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_currencies VALUES("1","US Dollar","$","USD","0.0001","2","before","1","0","0");
INSERT INTO phpn_currencies VALUES("2","Euro","€","EUR","0.0001","2","before","2","0","0");
INSERT INTO phpn_currencies VALUES("6","Rupiah","Rp.","IDR","1.0000","0","before","0","1","1");



DROP TABLE IF EXISTS phpn_customer_funds;

CREATE TABLE `phpn_customer_funds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) NOT NULL DEFAULT '0',
  `admin_id` int(10) NOT NULL DEFAULT '0',
  `funds` decimal(10,2) NOT NULL DEFAULT '0.00',
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_customer_groups;

CREATE TABLE `phpn_customer_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_customer_groups VALUES("1","General","General purpose only");
INSERT INTO phpn_customer_groups VALUES("2","No Promotion","Customer who wants their name removed from receiving email promotion");
INSERT INTO phpn_customer_groups VALUES("3","Promotion","Customer who wants to receive promotion");



DROP TABLE IF EXISTS phpn_customers;

CREATE TABLE `phpn_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `customer_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - private person, 1- agency',
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL DEFAULT '0000-00-00',
  `company` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logo` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `b_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_address_2` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_city` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_state` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `b_country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `b_zipcode` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `user_password` varchar(50) CHARACTER SET latin1 NOT NULL,
  `profile_photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `profile_photo_thumb` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `preferred_language` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `date_created` datetime NOT NULL,
  `date_lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `registered_from_ip` varchar(15) CHARACTER SET latin1 NOT NULL,
  `last_logged_ip` varchar(15) CHARACTER SET latin1 NOT NULL DEFAULT '000.000.000.000',
  `email_notifications` tinyint(1) NOT NULL DEFAULT '0',
  `notification_status_changed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `orders_count` smallint(6) NOT NULL DEFAULT '0',
  `rooms_count` smallint(6) NOT NULL DEFAULT '0',
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - registration pending, 1 - active customer',
  `is_removed` tinyint(4) NOT NULL DEFAULT '0',
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `registration_code` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `b_country` (`b_country`),
  KEY `status` (`is_active`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_customers VALUES("1","1","0","John","Smith","0000-00-00","","","","","","","CA","US","","","","john.smith@email.com","customer1","","customer1.png","customer1_thumb.png","en","2016-04-20 11:00:06","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","1","1","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("2","1","0","Stephan","","0000-00-00","","","","","","","","DE","","","","stephan@email.com","customer2","","customer2.png","customer2_thumb.png","en","2016-04-21 12:00:02","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("3","1","0","Robert","","0000-00-00","","","","","","","","GB","","","","robert@email.com","customer3","","","","en","2016-04-22 13:00:03","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("4","1","0","Debora","","0000-00-00","","","","","","","FL","US","","","","debora@email.com","customer4","","customer4.png","customer4_thumb.png","en","2016-04-23 14:00:04","0000-00-00 00:00:00","127.0.0.1","","0","0000-00-00 00:00:00","0","0","0.00","1","0","","");
INSERT INTO phpn_customers VALUES("5","3","0","Test","123","1985-04-02","","","","JL. abcdefgh","","Jakarta","","ID","101010","08123123","","harfinovian@yahoo.com","test123","r2ÚmyƒC/™ŠPürîT","","","en","2017-02-17 10:39:54","2017-02-17 10:41:32","175.103.47.117","175.103.47.117","1","0000-00-00 00:00:00","1","1","0.00","1","0","","");



DROP TABLE IF EXISTS phpn_email_templates;

CREATE TABLE `phpn_email_templates` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `template_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `template_name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `template_subject` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `template_content` text COLLATE utf8_unicode_ci NOT NULL,
  `is_system_template` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_email_templates VALUES("1","en","new_account_created","Email for new customer","Your account has been created","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYou login: {USER NAME}\nYou password: {USER PASSWORD}\n\nYou may follow the link below to log into your account:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("4","en","new_account_created_confirm_by_admin","Email for new user (admin approval required)","Your account has been created (approval required)","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYour login: {USER NAME}\nYour password: {USER PASSWORD}\n\nAfter your registration will be approved by administrator,  you could log into your account with a following link:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("84","ID","test_template","Testing Email","Testing Email","Hello <b>{USER NAME}</b>!\n\nThis a testing email.\n\nBest regards,\n{WEB SITE}","0");
INSERT INTO phpn_email_templates VALUES("7","en","new_account_created_confirm_by_email","Email for new user (email confirmation required)","Your account has been created (confirmation required)","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYour login: {USER NAME}\nYour password: {USER PASSWORD}\n\nIn order to become authorized member, you will need to confirm your registration. You may follow the link below to access the confirmation page:\n<a href=\"{BASE URL}index.php?customer=confirm_registration&c={REGISTRATION CODE}\">Confirm Registration</a>\n\nP.S. Remember, we will never sell your personal information or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("82","ID","unsubscription_from_newsletter","Newsletter - member has unsubscribed (member copy)","You have been unsubscribed from the Newsletter","Hello!\n\nYou are receiving this email because you, or someone using this email address, unsubscribed from the Newsletter of {WEB SITE}\n\nYou can always restore your subscription, using the link below: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_subscribe&email={USER EMAIL}\">Subscribe</a>\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("83","ID","reservation_expired","Reservation has been expired","Your reservation has been expired!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order reservation has been expired.\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("10","en","new_account_created_by_admin","Email for new user (account created by admin)","Your account has been created by admin","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nOur administrator just created a new account for you.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYou login: {USER NAME}\nYou password: {USER PASSWORD}\n\nYou may follow the link below to log into your account:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("81","ID","subscription_to_newsletter","Newsletter - new member has subscribed (member copy)","You have been subscribed to the Newsletter","Hello!\n\nYou are receiving this email because you, or someone using this email address, subscribed to the Newsletter of {WEB SITE}\n\nIf you do not wish to receive such emails in the future, please click this link: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_unsubscribe&email={USER EMAIL}\">Unsubscribe</a>\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("79","ID","order_status_changed","Reservation status has been changed","Your reservation status has been changed.","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThe status of your reservation #{BOOKING NUMBER} has been changed to {STATUS DESCRIPTION}!\n\n-\nSincerely,\nCustomer Support\n","1");
INSERT INTO phpn_email_templates VALUES("80","ID","payment_error","Customer payment has been failed for some reason","Your payment has been failed","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThe payment for your booking {BOOKING NUMBER} has been failed. The reason was: {STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("14","en","new_account_created_notify_admin","New account has been created (notify admin)","New account has been created","Hello Admin!\n\nA new user has been registered at your site.\nThis email contains a user account details:\n\nName: {FIRST NAME} {LAST NAME}\nEmail: {USER EMAIL}\nUsername: {USER NAME}\n\nP.S. Please check if it doesn\'t require your approval for activation","1");
INSERT INTO phpn_email_templates VALUES("16","en","password_forgotten","Email for customer or admin forgotten password","Forgotten Password","Hello <b>{USER NAME}</b>!\n\nYou or someone else asked for your login info on our site:\n{WEB SITE}\n\nYour Login Info:\n\nUsername: {USER NAME}\nPassword: {USER PASSWORD}\n\n\nBest regards,\n{WEB SITE}","1");
INSERT INTO phpn_email_templates VALUES("78","ID","order_refunded","Reservation has been refunded by administrator","Your order  has been refunded","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order {BOOKING NUMBER} has been refunded!\n\n{BOOKING DETAILS}\n\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("19","en","password_changed_by_admin","Password changed by admin","Your password has been changed","Hello <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour password was changed by administrator of the site:\n{WEB SITE}\n\nHere your new login info:\n-\nUsername: {USER NAME} \nPassword: {USER PASSWORD}\n\n-\nBest regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("77","ID","order_canceled","Reservation has been canceled by Customer/Administrator","Your order has been canceled!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order {BOOKING NUMBER} has been canceled.\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("22","en","registration_approved_by_admin","Email for new customer (registration was approved by admin)","Your registration has been approved","Dear <b>{FIRST NAME} {LAST NAME}!</b>\n\nCongratulations! This e-mail is to confirm that your registration at {WEB SITE} has been approved.\n\nYou can now login in to your account now.\n\nThank you for choosing {WEB SITE}.\n-\nSincerely,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("76","ID","events_new_registration","Events - new member has registered (member copy)","You have been successfully registered to the event!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on registering to {EVENT}.\n\nPlease keep this email for your records, as it contains an important information that you may need.\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("25","en","account_deleted_by_user","Account removed email (by customer)","Your account has been removed","Dear {USER NAME}!\n\nYour account was removed.\n\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("74","ID","order_placed_online","Email for online placed orders (not paid yet)","Your order has been placed in our system!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for reservation request!\n\nYour order {BOOKING NUMBER} has been placed in our system and will be processed shortly.\n{STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\nP.S. Please keep this email for your records, as it contains an important information that you may\nneed.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("75","ID","order_paid","Email for orders paid via payment processing systems","Your order {BOOKING NUMBER} has been paid and received by the system!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for reservation!\n\nYour order {BOOKING NUMBER} has been completed!\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\n{BILLING INFORMATION}\n\nP.S. Please keep this email for your records, as it contains an important information that you may need.\nP.P.S You may always check your booking status here:\n<a href=\"{BASE URL}index.php?page=check_status\">Check Status</a>\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("73","ID","new_account_created_without","Email for new/returned customer (without account)","Your contact information has been accepted","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for sending us your contact information. You may now complete your booking - just follow the instructions on the checkout page.\n\nPlease remember that even you don\'t have account on our site, you may always create it with easily. To do it simply follow this link and enter all needed information to create a new account: <a href=\"{BASE URL}index.php?customer=create_account\">Create Account</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("30","en","new_account_created_without","Email for new/returned customer (without account)","Your contact information has been accepted","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for sending us your contact information. You may now complete your booking - just follow the instructions on the checkout page.\n\nPlease remember that even you don\'t have account on our site, you may always create it with easily. To do it simply follow this link and enter all needed information to create a new account: <a href=\"{BASE URL}index.php?customer=create_account\">Create Account</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("31","en","order_placed_online","Email for online placed orders (not paid yet)","Your order has been placed in our system!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for reservation request!\n\nYour order {BOOKING NUMBER} has been placed in our system and will be processed shortly.\n{STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\nP.S. Please keep this email for your records, as it contains an important information that you may\nneed.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("71","ID","registration_approved_by_admin","Email for new customer (registration was approved by admin)","Your registration has been approved","Dear <b>{FIRST NAME} {LAST NAME}!</b>\n\nCongratulations! This e-mail is to confirm that your registration at {WEB SITE} has been approved.\n\nYou can now login in to your account now.\n\nThank you for choosing {WEB SITE}.\n-\nSincerely,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("72","ID","account_deleted_by_user","Account removed email (by customer)","Your account has been removed","Dear {USER NAME}!\n\nYour account was removed.\n\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("34","en","order_paid","Email for orders paid via payment processing systems","Your order {BOOKING NUMBER} has been paid and received by the system!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThank you for reservation!\n\nYour order {BOOKING NUMBER} has been completed!\n\n{BOOKING DETAILS}\n\n{PERSONAL INFORMATION}\n\n{BILLING INFORMATION}\n\nP.S. Please keep this email for your records, as it contains an important information that you may need.\nP.P.S You may always check your booking status here:\n<a href=\"{BASE URL}index.php?page=check_status\">Check Status</a>\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("69","ID","password_forgotten","Email for customer or admin forgotten password","Forgotten Password","Hello <b>{USER NAME}</b>!\n\nYou or someone else asked for your login info on our site:\n{WEB SITE}\n\nYour Login Info:\n\nUsername: {USER NAME}\nPassword: {USER PASSWORD}\n\n\nBest regards,\n{WEB SITE}","1");
INSERT INTO phpn_email_templates VALUES("70","ID","password_changed_by_admin","Password changed by admin","Your password has been changed","Hello <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour password was changed by administrator of the site:\n{WEB SITE}\n\nHere your new login info:\n-\nUsername: {USER NAME} \nPassword: {USER PASSWORD}\n\n-\nBest regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("37","en","events_new_registration","Events - new member has registered (member copy)","You have been successfully registered to the event!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on registering to {EVENT}.\n\nPlease keep this email for your records, as it contains an important information that you may need.\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("68","ID","new_account_created_notify_admin","New account has been created (notify admin)","New account has been created","Hello Admin!\n\nA new user has been registered at your site.\nThis email contains a user account details:\n\nName: {FIRST NAME} {LAST NAME}\nEmail: {USER EMAIL}\nUsername: {USER NAME}\n\nP.S. Please check if it doesn\'t require your approval for activation","1");
INSERT INTO phpn_email_templates VALUES("40","en","order_canceled","Reservation has been canceled by Customer/Administrator","Your order has been canceled!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order {BOOKING NUMBER} has been canceled.\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("67","ID","new_account_created_by_admin","Email for new user (account created by admin)","Your account has been created by admin","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nOur administrator just created a new account for you.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYou login: {USER NAME}\nYou password: {USER PASSWORD}\n\nYou may follow the link below to log into your account:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("44","en","order_refunded","Reservation has been refunded by administrator","Your order  has been refunded","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order {BOOKING NUMBER} has been refunded!\n\n{BOOKING DETAILS}\n\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("47","en","order_status_changed","Reservation status has been changed","Your reservation status has been changed.","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThe status of your reservation #{BOOKING NUMBER} has been changed to {STATUS DESCRIPTION}!\n\n-\nSincerely,\nCustomer Support\n","1");
INSERT INTO phpn_email_templates VALUES("49","en","payment_error","Customer payment has been failed for some reason","Your payment has been failed","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nThe payment for your booking {BOOKING NUMBER} has been failed. The reason was: {STATUS DESCRIPTION}\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("52","en","subscription_to_newsletter","Newsletter - new member has subscribed (member copy)","You have been subscribed to the Newsletter","Hello!\n\nYou are receiving this email because you, or someone using this email address, subscribed to the Newsletter of {WEB SITE}\n\nIf you do not wish to receive such emails in the future, please click this link: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_unsubscribe&email={USER EMAIL}\">Unsubscribe</a>\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("66","ID","new_account_created_confirm_by_email","Email for new user (email confirmation required)","Your account has been created (confirmation required)","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYour login: {USER NAME}\nYour password: {USER PASSWORD}\n\nIn order to become authorized member, you will need to confirm your registration. You may follow the link below to access the confirmation page:\n<a href=\"{BASE URL}index.php?customer=confirm_registration&c={REGISTRATION CODE}\">Confirm Registration</a>\n\nP.S. Remember, we will never sell your personal information or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("55","en","unsubscription_from_newsletter","Newsletter - member has unsubscribed (member copy)","You have been unsubscribed from the Newsletter","Hello!\n\nYou are receiving this email because you, or someone using this email address, unsubscribed from the Newsletter of {WEB SITE}\n\nYou can always restore your subscription, using the link below: <a href=\"{BASE URL}index.php?page=newsletter&task=pre_subscribe&email={USER EMAIL}\">Subscribe</a>\n\n-\nBest Regards,\nAdministration","1");
INSERT INTO phpn_email_templates VALUES("65","ID","new_account_created_confirm_by_admin","Email for new user (admin approval required)","Your account has been created (approval required)","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYour login: {USER NAME}\nYour password: {USER PASSWORD}\n\nAfter your registration will be approved by administrator,  you could log into your account with a following link:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");
INSERT INTO phpn_email_templates VALUES("58","en","reservation_expired","Reservation has been expired","Your reservation has been expired!","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nYour order reservation has been expired.\n\n{BOOKING DETAILS}\n\nP.S. Please feel free to contact us if you have any questions.\n\n-\nSincerely,\nCustomer Support\n\n{HOTEL INFO}","1");
INSERT INTO phpn_email_templates VALUES("61","en","test_template","Testing Email","Testing Email","Hello <b>{USER NAME}</b>!\n\nThis a testing email.\n\nBest regards,\n{WEB SITE}","0");
INSERT INTO phpn_email_templates VALUES("64","ID","new_account_created","Email for new customer","Your account has been created","Dear <b>{FIRST NAME} {LAST NAME}</b>!\n\nCongratulations on creating your new account.\n\nPlease keep this email for your records, as it contains an important information that you may need, should you ever encounter problems or forget your password.\n\nYou login: {USER NAME}\nYou password: {USER PASSWORD}\n\nYou may follow the link below to log into your account:\n<a href=\"{BASE URL}index.php?customer=login\">Login</a>\n\nP.S. Remember, we will never sell your name or email address.\n\nEnjoy!\n-\nSincerely,\nCustomer Support","1");



DROP TABLE IF EXISTS phpn_events_registered;

CREATE TABLE `phpn_events_registered` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL DEFAULT '0',
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_extras;

CREATE TABLE `phpn_extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `maximum_count` smallint(6) unsigned NOT NULL DEFAULT '0',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_extras VALUES("1","100000.00","1","2","0");
INSERT INTO phpn_extras VALUES("2","400000.00","2","1","1");



DROP TABLE IF EXISTS phpn_extras_description;

CREATE TABLE `phpn_extras_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `extra_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_extras_description VALUES("1","1","en","Wireless Internet Access","Wireless Internet Access (24 hour period)	");
INSERT INTO phpn_extras_description VALUES("2","1","es","Acceso inalámbrico a Internet","Acceso inalámbrico a Internet (período de 24 horas)");
INSERT INTO phpn_extras_description VALUES("3","1","de","WLAN","WLAN (24 Stunden)");
INSERT INTO phpn_extras_description VALUES("4","2","en","Airport Pickup","Airport Pickup (1 car with 5 seater)");
INSERT INTO phpn_extras_description VALUES("5","2","es","Recogida en el aeropuerto","Recogida en el aeropuerto (1 coche con 5 plazas)");
INSERT INTO phpn_extras_description VALUES("6","2","de","Abholung vom Flughafen","Abholung vom Flughafen (1 Fahrzeug mit 5-Sitzer)");



DROP TABLE IF EXISTS phpn_faq_categories;

CREATE TABLE `phpn_faq_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_faq_category_items;

CREATE TABLE `phpn_faq_category_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `faq_question` text COLLATE utf8_unicode_ci NOT NULL,
  `faq_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_gallery_album_items;

CREATE TABLE `phpn_gallery_album_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `album_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `item_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_file_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `album_code` (`album_code`),
  KEY `priority_order` (`priority_order`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_album_items VALUES("1","dkw3vvot","http://www.youtube.com/watch?v=5VIV8nt2KkU","","5","1");
INSERT INTO phpn_gallery_album_items VALUES("2","afbirxww","home.jpg","home_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("3","7u9sfhaz","img1_1.jpg","img1_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("4","7u9sfhaz","img1_2.jpg","img1_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("5","7u9sfhaz","img1_3.jpg","img1_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("6","0bxbqgps","img2_1.jpg","img2_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("7","0bxbqgps","img2_2.jpg","img2_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("8","0bxbqgps","img2_3.jpg","img2_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("9","6z5i5ikr","img3_1.jpg","img3_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("10","6z5i5ikr","img3_2.jpg","img3_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("11","6z5i5ikr","img3_3.jpg","img3_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("12","gvgbrtmc","img4_1.jpg","img4_1_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("13","gvgbrtmc","img4_2.jpg","img4_2_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("14","gvgbrtmc","img4_3.jpg","img4_3_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("15","0sbqvgbu","CIMG03771-825x510.jpg","cimg03771-825x510_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("16","0sbqvgbu","CIMG04041-825x510.jpg","cimg04041-825x510_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("17","0sbqvgbu","Convert-of-DSC_46851-825x510.jpg","convert-of-dsc_46851-825x510_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("18","0sbqvgbu","Convert-of-View1-400x300.jpg","convert-of-view1-400x300_thumb.jpg","4","1");
INSERT INTO phpn_gallery_album_items VALUES("19","0sbqvgbu","Convert-of-View3-600x400.jpg","convert-of-view3-600x400_thumb.jpg","5","1");
INSERT INTO phpn_gallery_album_items VALUES("20","0sbqvgbu","front-640x480.png","front-640x480_thumb.jpg","6","1");
INSERT INTO phpn_gallery_album_items VALUES("21","0sbqvgbu","IMG00202-20091026-1640-400x300.jpg","img00202-20091026-1640-400x300_thumb.jpg","7","1");
INSERT INTO phpn_gallery_album_items VALUES("22","0sbqvgbu","IMG00203-20091026-1641-600x400.jpg","img00203-20091026-1641-600x400_thumb.jpg","8","1");
INSERT INTO phpn_gallery_album_items VALUES("23","0sbqvgbu","inet2-825x510.jpg","inet2-825x510_thumb.jpg","9","1");
INSERT INTO phpn_gallery_album_items VALUES("24","0sbqvgbu","slider1-400x300.png","slider1-400x300_thumb.jpg","10","1");
INSERT INTO phpn_gallery_album_items VALUES("25","zqnxqxm2","Convert-of-IMG_4492-640x480.jpg","convert-of-img_4492-640x480_thumb.jpg","1","1");
INSERT INTO phpn_gallery_album_items VALUES("26","zqnxqxm2","Convert-of-IMG_5071-600x400.jpg","convert-of-img_5071-600x400_thumb.jpg","2","1");
INSERT INTO phpn_gallery_album_items VALUES("41","zqnxqxm2","rinjani-400x300.jpg","rinjani-400x300_thumb.jpg","11","1");
INSERT INTO phpn_gallery_album_items VALUES("40","zqnxqxm2","soekarno-600x382.png","soekarno-600x382_thumb.jpg","10","1");
INSERT INTO phpn_gallery_album_items VALUES("39","zqnxqxm2","IMG_1222-640x480.jpg","img_1222-640x480_thumb.jpg","9","1");
INSERT INTO phpn_gallery_album_items VALUES("38","zqnxqxm2","toba-600x400.jpg","toba-600x400_thumb.jpg","8","1");
INSERT INTO phpn_gallery_album_items VALUES("32","zqnxqxm2","hotspring-600x400.jpg","hotspring-600x400_thumb.jpg","3","1");
INSERT INTO phpn_gallery_album_items VALUES("33","zqnxqxm2","samosir-326x300.jpg","samosir-326x300_thumb.jpg","4","1");
INSERT INTO phpn_gallery_album_items VALUES("34","zqnxqxm2","scuba-400x300.jpg","scuba-400x300_thumb.jpg","5","1");
INSERT INTO phpn_gallery_album_items VALUES("35","zqnxqxm2","simanindo-600x400.jpg","simanindo-600x400_thumb.jpg","6","1");
INSERT INTO phpn_gallery_album_items VALUES("37","zqnxqxm2","tomok-640x480.png","tomok-640x480_thumb.jpg","7","1");



DROP TABLE IF EXISTS phpn_gallery_album_items_description;

CREATE TABLE `phpn_gallery_album_items_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `gallery_album_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `album_code` (`gallery_album_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_album_items_description VALUES("2","1","en","My Hotel Video","");
INSERT INTO phpn_gallery_album_items_description VALUES("55","13","ID","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("5","2","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("54","12","ID","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("7","3","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("53","11","ID","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("10","4","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("52","10","ID","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("13","5","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("51","9","ID","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("16","6","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("50","8","ID","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("19","7","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("49","7","ID","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("22","8","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("48","6","ID","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("25","9","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("47","5","ID","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("28","10","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("46","4","ID","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("31","11","en","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("45","3","ID","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("44","2","ID","Picture #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("35","12","en","Picture #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("37","13","en","Picture #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("40","14","en","","");
INSERT INTO phpn_gallery_album_items_description VALUES("56","14","ID","","");
INSERT INTO phpn_gallery_album_items_description VALUES("43","1","ID","My Hotel Video","");
INSERT INTO phpn_gallery_album_items_description VALUES("57","15","en","Hotel View #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("58","15","ID","Hotel View #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("59","16","en","Hotel View #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("60","16","ID","Hotel View #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("61","17","en","Hotel View #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("62","17","ID","Hotel View #3","");
INSERT INTO phpn_gallery_album_items_description VALUES("63","18","en","Hotel View #4","");
INSERT INTO phpn_gallery_album_items_description VALUES("64","18","ID","Hotel View #4","");
INSERT INTO phpn_gallery_album_items_description VALUES("65","19","en","Hotel View #5","");
INSERT INTO phpn_gallery_album_items_description VALUES("66","19","ID","Hotel View #5","");
INSERT INTO phpn_gallery_album_items_description VALUES("67","20","en","Hotel View #6","");
INSERT INTO phpn_gallery_album_items_description VALUES("68","20","ID","Hotel View #6","");
INSERT INTO phpn_gallery_album_items_description VALUES("69","21","en","Hotel View #7","");
INSERT INTO phpn_gallery_album_items_description VALUES("70","21","ID","Hotel View #7","");
INSERT INTO phpn_gallery_album_items_description VALUES("71","22","en","Hotel View #8","");
INSERT INTO phpn_gallery_album_items_description VALUES("72","22","ID","Hotel View #8","");
INSERT INTO phpn_gallery_album_items_description VALUES("73","23","en","Hotel View #9","");
INSERT INTO phpn_gallery_album_items_description VALUES("74","23","ID","Hotel View #9","");
INSERT INTO phpn_gallery_album_items_description VALUES("75","24","en","Hotel View #10","");
INSERT INTO phpn_gallery_album_items_description VALUES("76","24","ID","Hotel View #10","");
INSERT INTO phpn_gallery_album_items_description VALUES("77","25","en","Cruise","You can explore the Samosir Island and Toba Lake in natural  environment  with your family");
INSERT INTO phpn_gallery_album_items_description VALUES("78","25","ID","Tourist Attraction #1","");
INSERT INTO phpn_gallery_album_items_description VALUES("79","26","en","Pool Lake Side","");
INSERT INTO phpn_gallery_album_items_description VALUES("80","26","ID","Tourist Attraction #2","");
INSERT INTO phpn_gallery_album_items_description VALUES("105","41","en","Rinjani Mountain","");
INSERT INTO phpn_gallery_album_items_description VALUES("104","40","en","Soekarno\'s Palace","");
INSERT INTO phpn_gallery_album_items_description VALUES("103","39","en","Monkey House","");
INSERT INTO phpn_gallery_album_items_description VALUES("91","32","en","Panguguran","");
INSERT INTO phpn_gallery_album_items_description VALUES("92","32","ID","Tourist Attraction #8","");
INSERT INTO phpn_gallery_album_items_description VALUES("93","33","en","Samosir Island","");
INSERT INTO phpn_gallery_album_items_description VALUES("94","33","ID","Tourist Attraction #9","");
INSERT INTO phpn_gallery_album_items_description VALUES("95","34","en","Scuba Diving","");
INSERT INTO phpn_gallery_album_items_description VALUES("96","34","ID","Tourist Attraction #10","");
INSERT INTO phpn_gallery_album_items_description VALUES("97","35","en","Simanindo Village","");
INSERT INTO phpn_gallery_album_items_description VALUES("98","35","ID","Tourist Attraction #11","");
INSERT INTO phpn_gallery_album_items_description VALUES("102","38","en","Toba Lake","");
INSERT INTO phpn_gallery_album_items_description VALUES("101","37","en","Tomok","");



DROP TABLE IF EXISTS phpn_gallery_albums;

CREATE TABLE `phpn_gallery_albums` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `album_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `album_type` enum('images','video') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'images',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_albums VALUES("1","afbirxww","images","1","0");
INSERT INTO phpn_gallery_albums VALUES("2","dkw3vvot","video","11","0");
INSERT INTO phpn_gallery_albums VALUES("3","7u9sfhaz","images","3","0");
INSERT INTO phpn_gallery_albums VALUES("4","0bxbqgps","images","5","0");
INSERT INTO phpn_gallery_albums VALUES("5","6z5i5ikr","images","7","0");
INSERT INTO phpn_gallery_albums VALUES("6","gvgbrtmc","images","9","0");
INSERT INTO phpn_gallery_albums VALUES("7","0sbqvgbu","images","12","1");
INSERT INTO phpn_gallery_albums VALUES("8","zqnxqxm2","images","13","1");



DROP TABLE IF EXISTS phpn_gallery_albums_description;

CREATE TABLE `phpn_gallery_albums_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `gallery_album_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_gallery_albums_description VALUES("2","1","en","General Images","General Images");
INSERT INTO phpn_gallery_albums_description VALUES("23","5","ID","Superior Rooms","Superior Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("4","2","en","General Video","General Video");
INSERT INTO phpn_gallery_albums_description VALUES("22","4","ID","Double Rooms","Double Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("7","3","en","Single Rooms","Single Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("21","3","ID","Single Rooms","Single Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("10","4","en","Double Rooms","Double Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("20","2","ID","General Video","General Video");
INSERT INTO phpn_gallery_albums_description VALUES("13","5","en","Superior Rooms","Superior Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("16","6","en","Luxury Rooms","Luxury Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("24","6","ID","Luxury Rooms","Luxury Rooms");
INSERT INTO phpn_gallery_albums_description VALUES("19","1","ID","General Images","General Images");
INSERT INTO phpn_gallery_albums_description VALUES("25","7","en","Pandu","Hotel View");
INSERT INTO phpn_gallery_albums_description VALUES("26","7","ID","Pandu","Hotel View");
INSERT INTO phpn_gallery_albums_description VALUES("27","8","en","Tourist Attraction","Tourist Attraction");
INSERT INTO phpn_gallery_albums_description VALUES("28","8","ID","Tourist Attraction","Tourist Attraction");



DROP TABLE IF EXISTS phpn_hotel_images;

CREATE TABLE `phpn_hotel_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL DEFAULT '0',
  `item_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_file_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `priority_order` (`priority_order`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotel_images VALUES("6","5","jamzak7kclj92oioz06m.jpg","jamzak7kclj92oioz06m_thumb.jpg","","0","1");
INSERT INTO phpn_hotel_images VALUES("9","5","p74naoxb6k9bn20kwtad.jpg","p74naoxb6k9bn20kwtad_thumb.jpg","","1","1");
INSERT INTO phpn_hotel_images VALUES("10","5","b1c7z2cum4g92w8ut72t.jpg","b1c7z2cum4g92w8ut72t_thumb.jpg","","2","1");
INSERT INTO phpn_hotel_images VALUES("11","5","ocf88ofernj2kh4grdi7.jpg","ocf88ofernj2kh4grdi7_thumb.jpg","","3","1");
INSERT INTO phpn_hotel_images VALUES("12","6","srpa4ldhjwmqsghdx1ox.jpg","srpa4ldhjwmqsghdx1ox_thumb.jpg","Lake View","0","1");
INSERT INTO phpn_hotel_images VALUES("13","6","b681b0rizbzlps209lur.jpg","b681b0rizbzlps209lur_thumb.jpg","","1","1");
INSERT INTO phpn_hotel_images VALUES("16","7","j78bw71ciov0k7k5q5rs.jpg","j78bw71ciov0k7k5q5rs_thumb.jpg","","0","1");
INSERT INTO phpn_hotel_images VALUES("15","6","cinda1nyybnw0unexm1a.jpg","cinda1nyybnw0unexm1a_thumb.jpg","","2","1");
INSERT INTO phpn_hotel_images VALUES("17","7","aqhekccy5mza2da51qy1.jpg","aqhekccy5mza2da51qy1_thumb.jpg","","1","1");



DROP TABLE IF EXISTS phpn_hotel_payment_gateways;

CREATE TABLE `phpn_hotel_payment_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL DEFAULT '0',
  `payment_type` varchar(20) NOT NULL DEFAULT '',
  `payment_type_name` varchar(50) NOT NULL DEFAULT '',
  `api_login` varchar(40) NOT NULL DEFAULT '',
  `api_key` varchar(40) NOT NULL DEFAULT '',
  `payment_info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

INSERT INTO phpn_hotel_payment_gateways VALUES("17","5","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("18","5","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("19","5","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("20","5","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_hotel_payment_gateways VALUES("25","7","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("26","7","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("27","7","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("28","7","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");
INSERT INTO phpn_hotel_payment_gateways VALUES("21","6","paypal","PayPal","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("22","6","2co","2CO (2 checkout)","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("23","6","authorize.net","Authorize.Net","","","");
INSERT INTO phpn_hotel_payment_gateways VALUES("24","6","bank.transfer","Bank Transfer","","","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n");



DROP TABLE IF EXISTS phpn_hotel_periods;

CREATE TABLE `phpn_hotel_periods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL DEFAULT '0',
  `period_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `finish_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_hotels;

CREATE TABLE `phpn_hotels` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `hotel_location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `property_type_id` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `hotel_group_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time_zone` varchar(5) CHARACTER SET latin1 NOT NULL,
  `map_code` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longitude` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `latitude_center` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longitude_center` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `distance_center` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hotel_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hotel_image_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `agent_commision` decimal(4,1) unsigned NOT NULL DEFAULT '0.0',
  `stars` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `lowest_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `facilities` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `hotel_location_id` (`hotel_location_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels VALUES("5","5","1","1","+62(625) 41290 || +62(625) 42399","+62(625) 41290","","7","https://www.google.com/maps/place/Pandu+Lakeside+Hotel/@2.6651553,98.9345108,18z/data=!4m5!3m4!1s0x0000000000000000:0xfb4e7666c21dfd30!8m2!3d2.664664!4d98.935276!6m1!1e1","2.664664","98.935276","2.6671683","98.9397776","","hotel_-1_dyjskizd7zvaqi9hllv5.jpg","hotel_-1_dyjskizd7zvaqi9hllv5_thumb.jpg","100.0","0","350000.00","a:13:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";i:4;s:2:\"10\";i:5;s:2:\"13\";i:6;s:2:\"14\";i:7;s:2:\"15\";i:8;s:2:\"17\";i:9;s:2:\"22\";i:10;s:2:\"24\";i:11;s:2:\"25\";i:12;s:2:\"27\";}","0","1","1");
INSERT INTO phpn_hotels VALUES("7","7","1","2","+62(370) 6155 342","+62(56985178","pandulombok@yahoo.com","7","","","","","","","hotel_-1_l196q5jzkp2hygpvrqop.jpg","hotel_-1_l196q5jzkp2hygpvrqop_thumb.jpg","100.0","0","150000.00","a:11:{i:0;s:1:\"1\";i:1;s:1:\"5\";i:2;s:2:\"10\";i:3;s:2:\"12\";i:4;s:2:\"13\";i:5;s:2:\"14\";i:6;s:2:\"17\";i:7;s:2:\"18\";i:8;s:2:\"22\";i:9;s:2:\"25\";i:10;s:2:\"28\";}","2","0","0");
INSERT INTO phpn_hotels VALUES("6","6","1","1","+62(625)451118 || +62(625)451088","+62(625)451281","","7","","","","","","","hotel_6_gu3u8gkq5sqeewwh6udn.jpg","hotel_6_gu3u8gkq5sqeewwh6udn_thumb.jpg","100.0","0","350000.00","a:15:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";i:4;s:2:\"10\";i:5;s:2:\"12\";i:6;s:2:\"13\";i:7;s:2:\"14\";i:8;s:2:\"15\";i:9;s:2:\"16\";i:10;s:2:\"17\";i:11;s:2:\"19\";i:12;s:2:\"22\";i:13;s:2:\"27\";i:14;s:2:\"28\";}","1","0","1");



DROP TABLE IF EXISTS phpn_hotels_description;

CREATE TABLE `phpn_hotels_description` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '1',
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `name_center_point` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `preferences` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_description VALUES("19","6","en","Pandu Tuktuk","Jl. Tuktuk Siadong, Samosir Island\nNorth Sumatra, Indonesia","","<p>Pandu Tuktuk lakeside hotel is located in the eco-tourism zone of Tuktuk, a perfect base to explore and discover the wonder and beauty of toba lake. The location boast toba lake waterfront so you can enjoy swimming on this fresh water lake. Ideal for holiday and relaxation. Lake Toba will make any stay comfortable and memorable.</p>\n<p>Tour around the lake is an option if you would love to explore the elegant and charming history and culture of Toba Lake.</p>\n<p>Many local restaurants and cafe are also within walking distance of this hotel.</p>\n<p>Meeting room for 50 people is also available for events.</p>","");
INSERT INTO phpn_hotels_description VALUES("20","6","ID","Pandu Tuktuk","Jl. Tuktuk Siadong, Pulau Samosir\nSumatra Utara, Indonesia","","<p>Pandu Tuktuk Danau ini terletak di zona eko-wisata dari Tuktuk, basis yang sempurna untuk menjelajahi dan menemukan keajaiban dan keindahan danau toba. Lokasi membanggakan toba danau tepi sehingga Anda dapat menikmati berenang di danau air tawar ini. Ideal untuk liburan dan relaksasi. Danau Toba akan membuat tetap nyaman dan berkesan.</p>\n<p> </p>\n<p>Tur di sekitar danau adalah pilihan jika Anda ingin menjelajahi sejarah elegan dan menawan dan budaya Danau Toba.</p>\n<p> </p>\n<p>Banyak restoran lokal dan kafe juga berada dalam jarak berjalan kaki dari hotel ini.</p>\n<p> </p>\n<p>Ruang pertemuan untuk 50 orang juga tersedia untuk acara.</p>","");
INSERT INTO phpn_hotels_description VALUES("21","7","en","Pandu Homestay Lombok","Jl. Raya Kuta, Kuta Lombok Tengah\nLombok Tengah, Nusa Tenggara Barat, Indonesia 83573","","","");
INSERT INTO phpn_hotels_description VALUES("22","7","ID","Pandu Homestay Lombok","Jl. Raya Kuta, Kuta Lombok Tengah\nLombok Tengah, Nusa Tenggara Barat, Indonesia 83573","","","");
INSERT INTO phpn_hotels_description VALUES("17","5","en","Pandu Parapat","Jl. TPR Sinaga No. 12, Parapat 21174\nNorth Sumatra, Indonesia","Parapat city","<p>Pandu Parapat lakeside hotel is located in the strategic city of Parapat, a perfect base to explore and discover the wonder and beauty of toba lake. The location boast toba lake waterfront so you can enjoy swimming on this fresh water lake. Ideal for holiday and relaxation. Lake Toba will make any stay comfortable and memorable.</p>\n<p>Two major water transportation port is within walking distance. Tour around the lake is an option if you would love to explore the elegant and charming history and culture of Toba Lake. (1-20 minutes walk)</p>\n<p>Many local restaurants and shopping centers are also within walking distance of this hotel. (5-15 minutes walk)</p>","");
INSERT INTO phpn_hotels_description VALUES("18","5","ID","Pandu Parapat","Jl. TPR Sinaga No. 12, Parapat 21174\nSumatra Utara, Indonesia","Kota Parapat","<p>Parapat Pandu Danau ini terletak di kota strategis Parapat, basis yang sempurna untuk menjelajahi dan menemukan keajaiban dan keindahan danau Toba. Lokasi membanggakan Toba Danau tepi sehingga Anda dapat menikmati berenang di danau air tawar ini. Ideal untuk liburan dan relaksasi. Danau Toba akan membuat setiap tetap nyaman dan berkesan.</p>\n<p> </p>\n<p>Dua utama pelabuhan transportasi air berdekatan. Tur di sekitar danau adalah pilihan jika Anda ingin menjelajahi sejarah elegan dan menawan dan budaya Danau Toba. (1-20 menit berjalan kaki)</p>\n<p> </p>\n<p>Banyak restoran lokal dan pusat perbelanjaan juga berdekatan dengan hotel ini. (5-15 menit berjalan kaki)</p>","");



DROP TABLE IF EXISTS phpn_hotels_locations;

CREATE TABLE `phpn_hotels_locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_locations VALUES("6","ID","1","1");
INSERT INTO phpn_hotels_locations VALUES("7","ID","2","0");
INSERT INTO phpn_hotels_locations VALUES("5","ID","0","1");



DROP TABLE IF EXISTS phpn_hotels_locations_description;

CREATE TABLE `phpn_hotels_locations_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_location_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_location_id` (`hotel_location_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_locations_description VALUES("21","7","en","Kuta, Lombok	");
INSERT INTO phpn_hotels_locations_description VALUES("20","6","ID","Tuktuk, Samosir, Sumatra Utara");
INSERT INTO phpn_hotels_locations_description VALUES("19","6","en","Tuktuk, Samosir, North Sumatra");
INSERT INTO phpn_hotels_locations_description VALUES("18","5","ID","Parapat, Sumatra Utara");
INSERT INTO phpn_hotels_locations_description VALUES("17","5","en","Parapat, North Sumatra");
INSERT INTO phpn_hotels_locations_description VALUES("22","7","ID","Kuta, Lombok	");



DROP TABLE IF EXISTS phpn_hotels_property_types;

CREATE TABLE `phpn_hotels_property_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `property_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_property_types VALUES("1","hotels","1","1","1");
INSERT INTO phpn_hotels_property_types VALUES("2","villas","2","0","0");



DROP TABLE IF EXISTS phpn_hotels_property_types_description;

CREATE TABLE `phpn_hotels_property_types_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_property_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_property_id` (`hotel_property_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_hotels_property_types_description VALUES("1","1","en","Hotels");
INSERT INTO phpn_hotels_property_types_description VALUES("8","2","ID","Villas");
INSERT INTO phpn_hotels_property_types_description VALUES("4","2","en","Villas");
INSERT INTO phpn_hotels_property_types_description VALUES("7","1","ID","Hotels");



DROP TABLE IF EXISTS phpn_languages;

CREATE TABLE `phpn_languages` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `lang_name_en` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `lc_time_name` varchar(5) CHARACTER SET latin1 NOT NULL DEFAULT 'en_US',
  `lang_dir` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'ltr',
  `icon_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `used_on` enum('front-end','back-end','global') CHARACTER SET latin1 NOT NULL DEFAULT 'global',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_languages VALUES("1","English","English","en","en_US","ltr","en.gif","1","global","1","1");
INSERT INTO phpn_languages VALUES("4","Indonesia","Indonesian","ID","id_ID","ltr","","2","global","0","0");



DROP TABLE IF EXISTS phpn_mass_mail_log;

CREATE TABLE `phpn_mass_mail_log` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `email_to` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email_template_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_mass_mail_log VALUES("1","1","admins","test_template","2016-02-01 01:12:20");
INSERT INTO phpn_mass_mail_log VALUES("2","1","test","test_template","2016-03-01 01:12:20");
INSERT INTO phpn_mass_mail_log VALUES("3","1","all","test_template","2016-03-02 02:22:40");



DROP TABLE IF EXISTS phpn_meal_plans;

CREATE TABLE `phpn_meal_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `charge_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Per person per night',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_meal_plans VALUES("8","5","0.00","0","1","1","1");
INSERT INTO phpn_meal_plans VALUES("9","5","0.00","0","0","1","0");



DROP TABLE IF EXISTS phpn_meal_plans_description;

CREATE TABLE `phpn_meal_plans_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meal_plan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_meal_plans_description VALUES("25","8","en","Seafood Fried Rice","");
INSERT INTO phpn_meal_plans_description VALUES("26","8","ID","Nasi Goreng Seafood","");
INSERT INTO phpn_meal_plans_description VALUES("27","9","en","Mie Goreng","");
INSERT INTO phpn_meal_plans_description VALUES("28","9","ID","Mie Goreng","");



DROP TABLE IF EXISTS phpn_menus;

CREATE TABLE `phpn_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `menu_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_placement` enum('','left','top','right','bottom','hidden') CHARACTER SET latin1 NOT NULL,
  `menu_order` tinyint(3) DEFAULT '1',
  `access_level` enum('public','registered') CHARACTER SET latin1 NOT NULL DEFAULT 'public',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_menus VALUES("1","AMM8WBAKJ9","en","Information","hidden","1","registered");
INSERT INTO phpn_menus VALUES("4","AMM8WBAKJ9","ID","Information","left","1","public");



DROP TABLE IF EXISTS phpn_modules;

CREATE TABLE `phpn_modules` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name_const` varchar(20) CHARACTER SET latin1 NOT NULL,
  `description_const` varchar(30) CHARACTER SET latin1 NOT NULL,
  `icon_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `module_tables` varchar(255) CHARACTER SET latin1 NOT NULL,
  `dependent_modules` varchar(20) CHARACTER SET latin1 NOT NULL,
  `settings_page` varchar(30) CHARACTER SET latin1 NOT NULL,
  `settings_const` varchar(30) CHARACTER SET latin1 NOT NULL,
  `settings_access_by` varchar(50) CHARACTER SET latin1 NOT NULL,
  `management_page` varchar(125) CHARACTER SET latin1 NOT NULL,
  `management_const` varchar(125) CHARACTER SET latin1 NOT NULL,
  `management_access_by` varchar(50) CHARACTER SET latin1 NOT NULL,
  `is_installed` tinyint(1) NOT NULL DEFAULT '0',
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_on_dashboard` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `priority_order` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_modules VALUES("1","rooms","_ROOMS","_MD_ROOMS","rooms.png","rooms,rooms_availabilities,rooms_description,rooms_prices,room_facilities,room_facilities_description","","mod_rooms_settings","_ROOMS_SETTINGS","owner,mainadmin","mod_rooms_management","_ROOMS_MANAGEMENT","owner,mainadmin","1","1","1","0");
INSERT INTO phpn_modules VALUES("2","pages","_PAGES","_MD_PAGES","pages.png","pages,menus","","","","owner,mainadmin","pages","_PAGE_EDIT_PAGES","owner,mainadmin","1","1","1","1");
INSERT INTO phpn_modules VALUES("3","customers","_CUSTOMERS","_MD_CUSTOMERS","customers.png","customers","","mod_customers_settings","_CUSTOMERS_SETTINGS","owner,mainadmin","","","","1","0","0","0");
INSERT INTO phpn_modules VALUES("4","booking","_BOOKINGS","_MD_BOOKINGS","booking.png","bookings,bookings_rooms,extras","","mod_booking_settings","_BOOKINGS_SETTINGS","owner,mainadmin","","","","1","0","1","1");
INSERT INTO phpn_modules VALUES("5","car_rental","_CAR_RENTAL","_MD_CAR_RENTAL","car_rental.png","car_agencies,car_agencies_description","","","","owner,mainadmin","mod_car_rental_agencies","","owner,mainadmin","1","0","1","2");
INSERT INTO phpn_modules VALUES("6","contact_us","_CONTACT_US","_MD_CONTACT_US","contact_us.png","","","mod_contact_us_settings","_CONTACT_US_SETTINGS","owner,mainadmin","","","","1","0","0","3");
INSERT INTO phpn_modules VALUES("7","comments","_COMMENTS","_MD_COMMENTS","comments.png","comments","","mod_comments_settings","_COMMENTS_SETTINGS","owner,mainadmin","mod_comments_management","_COMMENTS_MANAGEMENT","owner,mainadmin","1","0","0","4");
INSERT INTO phpn_modules VALUES("8","ratings","_RATINGS","_MD_RATINGS","ratings.png","ratings_items,ratings_users","","mod_ratings_settings","_RATINGS_SETTINGS","owner,mainadmin","","","","1","0","0","5");
INSERT INTO phpn_modules VALUES("9","testimonials","_TESTIMONIALS","_MD_TESTIMONIALS","testimonials.png","testimonials","","mod_testimonials_settings","_TESTIMONIALS_SETTINGS","owner,mainadmin","mod_testimonials_management","_TESTIMONIALS_MANAGEMENT","owner,mainadmin","1","0","0","6");
INSERT INTO phpn_modules VALUES("10","reviews","_REVIEWS","_MD_REVIEWS","reviews.png","reviews","","mod_reviews_settings","_REVIEWS_SETTINGS","owner,mainadmin","mod_reviews_management","_REVIEWS_MANAGEMENT","owner,mainadmin,hotelowner","1","0","1","7");
INSERT INTO phpn_modules VALUES("11","news","_NEWS","_MD_NEWS","news.png","news,events_registered,news_subscribed","","mod_news_settings","_NEWS_SETTINGS","owner,mainadmin","mod_news_management,mod_news_subscribed","_NEWS_MANAGEMENT,_SUBSCRIPTION_MANAGEMENT","owner,mainadmin","1","0","0","8");
INSERT INTO phpn_modules VALUES("12","gallery","_GALLERY","_MD_GALLERY","gallery.png","gallery_albums,gallery_images","","mod_gallery_settings","_GALLERY_SETTINGS","owner,mainadmin","mod_gallery_management","_GALLERY_MANAGEMENT","owner,mainadmin","1","0","0","9");
INSERT INTO phpn_modules VALUES("13","banners","_BANNERS","_MD_BANNERS","banners.png","banners","","mod_banners_settings","_BANNERS_SETTINGS","owner,mainadmin","mod_banners_management","_BANNERS_MANAGEMENT","owner,mainadmin","1","0","0","10");
INSERT INTO phpn_modules VALUES("14","faq","_FAQ","_MD_FAQ","faq.png","faq_categories,faq_category_items","","mod_faq_settings","_FAQ_SETTINGS","owner,mainadmin","mod_faq_management","_FAQ_MANAGEMENT","owner,mainadmin","1","0","0","11");
INSERT INTO phpn_modules VALUES("15","backup","_BACKUP_AND_RESTORE","_MD_BACKUP_AND_RESTORE","backup.png","","","mod_backup_installation","_BACKUP_INSTALLATION","owner","mod_backup_restore","_BACKUP_RESTORE","owner,mainadmin","1","0","0","12");



DROP TABLE IF EXISTS phpn_modules_settings;

CREATE TABLE `phpn_modules_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(20) CHARACTER SET latin1 NOT NULL,
  `settings_key` varchar(40) CHARACTER SET latin1 NOT NULL,
  `settings_value` text COLLATE utf8_unicode_ci NOT NULL,
  `settings_name_const` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `settings_description_const` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `key_display_type` enum('string','email','numeric','unsigned float','integer','positive integer','unsigned integer','enum','yes/no','html size','text') CHARACTER SET latin1 NOT NULL,
  `key_is_required` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `key_display_source` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'for ''enum'' field type',
  PRIMARY KEY (`id`),
  KEY `module_name` (`module_name`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_modules_settings VALUES("1","banners","is_active","yes","_MSN_BANNERS_IS_ACTIVE","_MS_BANNERS_IS_ACTIVE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("2","banners","rotation_type","slide show","_MSN_ROTATION_TYPE","_MS_ROTATION_TYPE","enum","1","random image,slide show");
INSERT INTO phpn_modules_settings VALUES("3","banners","rotate_delay","9","_MSN_ROTATE_DELAY","_MS_ROTATE_DELAY","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("4","banners","slideshow_caption_html","no","_MSN_BANNERS_CAPTION_HTML","_MS_BANNERS_CAPTION_HTML","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("5","booking","is_active","global","_MSN_ACTIVATE_BOOKINGS","_MS_ACTIVATE_BOOKINGS","enum","1","front-end,back-end,global,no");
INSERT INTO phpn_modules_settings VALUES("6","booking","payment_type_poa","No","_MSN_PAYMENT_TYPE_POA","_MS_PAYMENT_TYPE_POA","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("7","booking","payment_type_online","Frontend & Backend","_MSN_PAYMENT_TYPE_ONLINE","_MS_PAYMENT_TYPE_ONLINE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("8","booking","online_credit_card_required","no","_MSN_ONLINE_CREDIT_CARD_REQUIRED","_MS_ONLINE_CREDIT_CARD_REQUIRED","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("9","booking","payment_type_bank_transfer","Frontend & Backend","_MSN_PAYMENT_TYPE_BANK_TRANSFER","_MS_PAYMENT_TYPE_BANK_TRANSFER","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("10","booking","bank_transfer_info","Bank name: {Bank Central Asia}\nSwift code: {BCA}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {123456789}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n","_MSN_BANK_TRANSFER_INFO","_MS_BANK_TRANSFER_INFO","text","0","");
INSERT INTO phpn_modules_settings VALUES("11","booking","payment_type_paypal","No","_MSN_PAYMENT_TYPE_PAYPAL","_MS_PAYMENT_TYPE_PAYPAL","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("12","booking","paypal_email","paypal@yourdomain.com","_MSN_PAYPAL_EMAIL","_MS_PAYPAL_EMAIL","email","1","");
INSERT INTO phpn_modules_settings VALUES("13","booking","payment_type_2co","No","_MSN_PAYMENT_TYPE_2CO","_MS_PAYMENT_TYPE_2CO","enum","0","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("14","booking","two_checkout_vendor","Your 2CO Vendor ID here","_MSN_TWO_CHECKOUT_VENDOR","_MS_TWO_CHECKOUT_VENDOR","string","1","");
INSERT INTO phpn_modules_settings VALUES("15","booking","payment_type_authorize","No","_MSN_PAYMENT_TYPE_AUTHORIZE","_MS_PAYMENT_TYPE_AUTHORIZE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("16","booking","authorize_login_id","Your API Login ID here","_MSN_AUTHORIZE_LOGIN_ID","_MS_AUTHORIZE_LOGIN_ID","string","1","");
INSERT INTO phpn_modules_settings VALUES("17","booking","authorize_transaction_key","Your Transaction Key here","_MSN_AUTHORIZE_TRANSACTION_KEY","_MS_AUTHORIZE_TRANSACTION_KEY","string","1","");
INSERT INTO phpn_modules_settings VALUES("18","booking","default_payment_system","bank transfer","_MSN_DEFAULT_PAYMENT_SYSTEM","_MS_DEFAULT_PAYMENT_SYSTEM","enum","1","poa,online,bank transfer,paypal,2co,authorize.net");
INSERT INTO phpn_modules_settings VALUES("19","booking","allow_separate_gateways","no","_MSN_SEPARATE_GATEWAYS","_MS_SEPARATE_GATEWAYS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("20","booking","send_order_copy_to_admin","yes","_MSN_SEND_ORDER_COPY_TO_ADMIN","_MS_SEND_ORDER_COPY_TO_ADMIN","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("21","booking","allow_booking_without_account","yes","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("22","booking","pre_payment_type","percentage","_MSN_PRE_PAYMENT_TYPE","_MS_PRE_PAYMENT_TYPE","enum","1","full price,first night,fixed sum,percentage");
INSERT INTO phpn_modules_settings VALUES("23","booking","pre_payment_value","10","_MSN_PRE_PAYMENT_VALUE","_MS_PRE_PAYMENT_VALUE","enum","0","1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,99,100,125,150,175,200,250,500,750,1000");
INSERT INTO phpn_modules_settings VALUES("24","booking","vat_value","0","_MSN_VAT_VALUE","_MS_VAT_VALUE","unsigned float","0","");
INSERT INTO phpn_modules_settings VALUES("25","booking","minimum_nights","1","_MSN_MINIMUM_NIGHTS","_MS_MINIMUM_NIGHTS","enum","1","1,2,3,4,5,6,7,8,9,10,14,21,28,30,45,60,90");
INSERT INTO phpn_modules_settings VALUES("26","booking","maximum_nights","90","_MSN_MAXIMUM_NIGHTS","_MS_MAXIMUM_NIGHTS","enum","1","1,2,3,4,5,6,7,8,9,10,14,21,28,30,45,60,90,120,150,180,240,360");
INSERT INTO phpn_modules_settings VALUES("27","booking","mode","REAL MODE","_MSN_BOOKING_MODE","_MS_BOOKING_MODE","enum","1","TEST MODE,REAL MODE");
INSERT INTO phpn_modules_settings VALUES("28","booking","show_fully_booked_rooms","no","_MSN_SHOW_FULLY_BOOKED_ROOMS","_MS_SHOW_FULLY_BOOKED_ROOMS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("29","booking","prebooking_orders_timeout","2","_MSN_PREBOOKING_ORDERS_TIMEOUT","_MS_PREBOOKING_ORDERS_TIMEOUT","enum","1","0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,22,24,36,48,72");
INSERT INTO phpn_modules_settings VALUES("30","booking","customers_cancel_reservation","7","_MSN_CUSTOMERS_CANCEL_RESERVATION","_MS_CUSTOMERS_CANCEL_RESERVATION","enum","1","0,1,2,3,4,5,6,7,10,14,21,30,45,60");
INSERT INTO phpn_modules_settings VALUES("31","booking","show_reservation_form","yes","_MSN_SHOW_RESERVATION_FORM","_MS_SHOW_RESERVATION_FORM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("32","booking","booking_initial_fee","0","_MSN_RESERVATION_INITIAL_FEE","_MS_RESERVATION_INITIAL_FEE","unsigned float","1","");
INSERT INTO phpn_modules_settings VALUES("33","booking","booking_number_type","random","_MSN_BOOKING_NUMBER_TYPE","_MS_BOOKING_NUMBER_TYPE","enum","1","random,sequential");
INSERT INTO phpn_modules_settings VALUES("34","booking","vat_included_in_price","no","_MSN_VAT_INCLUDED_IN_PRICE","_MS_VAT_INCLUDED_IN_PRICE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("35","booking","show_booking_status_form","yes","_MSN_SHOW_BOOKING_STATUS_FORM","_MS_SHOW_BOOKING_STATUS_FORM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("36","booking","maximum_allowed_reservations","1","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","_MS_MAXIMUM_ALLOWED_RESERVATIONS","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("37","booking","first_night_calculating_type","real","_MSN_FIRST_NIGHT_CALCULATING_TYPE","_MS_FIRST_NIGHT_CALCULATING_TYPE","enum","1","real,average");
INSERT INTO phpn_modules_settings VALUES("38","booking","available_until_approval","no","_MSN_AVAILABLE_UNTIL_APPROVAL","_MS_AVAILABLE_UNTIL_APPROVAL","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("39","booking","reservation_expired_alert","no","_MSN_RESERVATION_EXPIRED_ALERT","_MS_RESERVATION EXPIRED_ALERT","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("40","booking","allow_booking_in_past","no","_MSN_ADMIN_BOOKING_IN_PAST","_MS_ADMIN_BOOKING_IN_PAST","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("41","booking","allow_payment_with_balance","yes","_MSN_ALLOW_PAYMENT_WITH_BALANCE","_MS_ALLOW_PAYMENT_WITH_BALANCE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("42","booking","special_tax","0","_MSN_SPECIAL_TAX","_MS_SPECIAL_TAX","unsigned float","1","");
INSERT INTO phpn_modules_settings VALUES("43","car_rental","is_active","no","_MSN_ACTIVATE_CAR_RENTAL","_MS_ACTIVATE_CAR_RENTAL","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("44","car_rental","allow_separate_gateways","yes","_MSN_TC_SEPARATE_GATEWAYS","_MS_TC_SEPARATE_GATEWAYS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("45","car_rental","payment_type_poa","Frontend & Backend","_MSN_PAYMENT_TYPE_POA","_MS_PAYMENT_TYPE_POA","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("46","car_rental","payment_type_online","Frontend & Backend","_MSN_PAYMENT_TYPE_ONLINE","_MS_PAYMENT_TYPE_ONLINE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("47","car_rental","online_credit_card_required","yes","_MSN_ONLINE_CREDIT_CARD_REQUIRED","_MS_ONLINE_CREDIT_CARD_REQUIRED","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("48","car_rental","payment_type_bank_transfer","Frontend & Backend","_MSN_PAYMENT_TYPE_BANK_TRANSFER","_MS_PAYMENT_TYPE_BANK_TRANSFER","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("49","car_rental","bank_transfer_info","Bank name: {BANK NAME HERE}\nSwift code: {CODE HERE}\nRouting in Transit# or ABA#: {ROUTING HERE}\nAccount number *: {ACCOUNT NUMBER HERE}\n\n*The account number must be in the IBAN format which may be obtained from the branch handling the customer\'s account or may be seen at the top the customer\'s bank statement\n","_MSN_BANK_TRANSFER_INFO","_MS_BANK_TRANSFER_INFO","text","0","");
INSERT INTO phpn_modules_settings VALUES("50","car_rental","payment_type_paypal","Frontend & Backend","_MSN_PAYMENT_TYPE_PAYPAL","_MS_PAYMENT_TYPE_PAYPAL","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("51","car_rental","paypal_email","paypal@yourdomain.com","_MSN_PAYPAL_EMAIL","_MS_PAYPAL_EMAIL","email","1","");
INSERT INTO phpn_modules_settings VALUES("52","car_rental","payment_type_2co","Frontend & Backend","_MSN_PAYMENT_TYPE_2CO","_MS_PAYMENT_TYPE_2CO","enum","0","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("53","car_rental","two_checkout_vendor","Your 2CO Vendor ID here","_MSN_TWO_CHECKOUT_VENDOR","_MS_TWO_CHECKOUT_VENDOR","string","1","");
INSERT INTO phpn_modules_settings VALUES("54","car_rental","payment_type_authorize","Frontend & Backend","_MSN_PAYMENT_TYPE_AUTHORIZE","_MS_PAYMENT_TYPE_AUTHORIZE","enum","1","No,Frontend Only,Backend Only,Frontend & Backend");
INSERT INTO phpn_modules_settings VALUES("55","car_rental","authorize_login_id","Your API Login ID here","_MSN_AUTHORIZE_LOGIN_ID","_MS_AUTHORIZE_LOGIN_ID","string","1","");
INSERT INTO phpn_modules_settings VALUES("56","car_rental","authorize_transaction_key","Your Transaction Key here","_MSN_AUTHORIZE_TRANSACTION_KEY","_MS_AUTHORIZE_TRANSACTION_KEY","string","1","");
INSERT INTO phpn_modules_settings VALUES("57","car_rental","default_payment_system","paypal","_MSN_DEFAULT_PAYMENT_SYSTEM","_MS_DEFAULT_PAYMENT_SYSTEM","enum","1","poa,online,bank transfer,paypal,2co,authorize.net");
INSERT INTO phpn_modules_settings VALUES("58","car_rental","pre_payment_type","percentage","_MSN_PRE_PAYMENT_TYPE","_MS_PRE_PAYMENT_TYPE","enum","1","full price,first night,fixed sum,percentage");
INSERT INTO phpn_modules_settings VALUES("59","car_rental","pre_payment_value","10","_MSN_PRE_PAYMENT_VALUE","_MS_PRE_PAYMENT_VALUE","enum","0","1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,99,100,125,150,175,200,250,500,750,1000");
INSERT INTO phpn_modules_settings VALUES("60","car_rental","vat_value","0","_MSN_VAT_VALUE","_MS_VAT_VALUE","unsigned float","0","");
INSERT INTO phpn_modules_settings VALUES("61","car_rental","mode","REAL MODE","_MSN_BOOKING_MODE","_MS_BOOKING_MODE","enum","1","TEST MODE,REAL MODE");
INSERT INTO phpn_modules_settings VALUES("62","car_rental","show_agency_in_search_result","yes","_MSN_AGENCY_IN_SEARCH_RESULT","_MS_AGENCY_IN_SEARCH_RESULT","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("63","car_rental","default_contant_email","","_MSN_TC_DEFAULT_CONTACT_EMAIL","_MS_TC_DEFAULT_CONTACT_EMAIL","email","0","");
INSERT INTO phpn_modules_settings VALUES("64","car_rental","default_contant_phone","","_MSN_TC_DEFAULT_CONTACT_PHONE","_MS_TC_DEFAULT_CONTACT_PHONE","string","0","");
INSERT INTO phpn_modules_settings VALUES("65","car_rental","customers_cancel_reservation","7","_MSN_CUSTOMERS_CANCEL_RESERVATION","_MS_CUSTOMERS_CANCEL_RESERVATION","enum","1","0,1,2,3,4,5,6,7,10,14,21,30,45,60");
INSERT INTO phpn_modules_settings VALUES("66","car_rental","vat_included_in_price","no","_MSN_VAT_INCLUDED_IN_PRICE","_MS_VAT_INCLUDED_IN_PRICE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("67","car_rental","allow_reservation_without_account","yes","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("68","car_rental","allow_payment_with_balance","yes","_MSN_ALLOW_PAYMENT_WITH_BALANCE","_MS_ALLOW_PAYMENT_WITH_BALANCE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("69","car_rental","prebooking_orders_timeout","2","_MSN_PREBOOKING_ORDERS_TIMEOUT","_MS_PREBOOKING_ORDERS_TIMEOUT","enum","1","0,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,22,24,36,48,72");
INSERT INTO phpn_modules_settings VALUES("70","car_rental","reservation_expired_alert","no","_MSN_RESERVATION_EXPIRED_ALERT","_MS_RESERVATION EXPIRED_ALERT","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("71","car_rental","send_order_copy_to_admin","yes","_MSN_SEND_ORDER_COPY_TO_ADMIN","_MS_SEND_ORDER_COPY_TO_ADMIN","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("72","car_rental","reservation_number_type","random","_MSN_RESERVATION_NUMBER_TYPE","_MS_RESERVATION_NUMBER_TYPE","enum","1","random,sequential");
INSERT INTO phpn_modules_settings VALUES("73","comments","comments_allow","yes","_MSN_COMMENTS_ALLOW","_MS_COMMENTS_ALLOW","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("74","comments","user_type","all","_MSN_USER_TYPE","_MS_USER_TYPE","enum","1","all,registered");
INSERT INTO phpn_modules_settings VALUES("75","comments","comment_length","500","_MSN_COMMENTS_LENGTH","_MS_COMMENTS_LENGTH","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("76","comments","image_verification_allow","yes","_MSN_IMAGE_VERIFICATION_ALLOW","_MS_IMAGE_VERIFICATION_ALLOW","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("77","comments","page_size","20","_MSN_COMMENTS_PAGE_SIZE","_MS_COMMENTS_PAGE_SIZE","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("78","comments","pre_moderation_allow","yes","_MSN_PRE_MODERATION_ALLOW","_MS_PRE_MODERATION_ALLOW","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("79","comments","delete_pending_time","2","_MSN_DELETE_PENDING_TIME","_MS_DELETE_PENDING_TIME","enum","1","0,1,2,3,4,5,6,7,8,9,10,15,20,30,45,60,120,180");
INSERT INTO phpn_modules_settings VALUES("80","contact_us","key","{module:contact_us}","_MSN_CONTACT_US_KEY","_MS_CONTACT_US_KEY","enum","1","{module:contact_us}");
INSERT INTO phpn_modules_settings VALUES("81","contact_us","email","info@pandu-hotel.com","_MSN_EMAIL","_MS_EMAIL","email","1","");
INSERT INTO phpn_modules_settings VALUES("82","contact_us","is_send_delay","yes","_MSN_IS_SEND_DELAY","_MS_IS_SEND_DELAY","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("83","contact_us","delay_length","20","_MSN_DELAY_LENGTH","_MS_DELAY_LENGTH","positive integer","0","");
INSERT INTO phpn_modules_settings VALUES("84","contact_us","image_verification_allow","yes","_MSN_IMAGE_VERIFICATION_ALLOW","_MS_IMAGE_VERIFICATION_ALLOW","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("85","customers","allow_adding_by_admin","yes","_MSN_ALLOW_ADDING_BY_ADMIN","_MS_ALLOW_ADDING_BY_ADMIN","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("86","customers","reg_confirmation","by admin","_MSN_REG_CONFIRMATION","_MS_REG_CONFIRMATION","enum","0","automatic,by email,by admin");
INSERT INTO phpn_modules_settings VALUES("87","customers","image_verification_allow","yes","_MSN_CUSTOMERS_IMAGE_VERIFICATION","_MS_CUSTOMERS_IMAGE_VERIFICATION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("88","customers","allow_login","yes","_MSN_ALLOW_CUSTOMERS_LOGIN","_MS_ALLOW_CUSTOMERS_LOGIN","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("89","customers","allow_registration","yes","_MSN_ALLOW_CUSTOMERS_REGISTRATION","_MS_ALLOW_CUSTOMERS_REGISTRATION","yes/no","0","");
INSERT INTO phpn_modules_settings VALUES("90","customers","password_changing_by_admin","yes","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("91","customers","allow_reset_passwords","yes","_MSN_ALLOW_CUST_RESET_PASSWORDS","_MS_ALLOW_CUST_RESET_PASSWORDS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("92","customers","admin_alert_new_registration","yes","_MSN_ALERT_ADMIN_NEW_REGISTRATION","_MS_ALERT_ADMIN_NEW_REGISTRATION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("93","customers","remember_me_allow","yes","_MSN_REMEMBER_ME","_MS_REMEMBER_ME","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("94","customers","allow_agencies","yes","_MSN_ALLOW_AGENCIES","_MS_ALLOW_AGENCIES","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("95","faq","is_active","yes","_MSN_FAQ_IS_ACTIVE","_MS_FAQ_IS_ACTIVE","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("96","gallery","key","{module:gallery}","_MSN_GALLERY_KEY","_MS_GALLERY_KEY","enum","1","{module:gallery}");
INSERT INTO phpn_modules_settings VALUES("97","gallery","album_key","{module:album=CODE}","_MSN_ALBUM_KEY","_MS_ALBUM_KEY","enum","1","{module:album=CODE}");
INSERT INTO phpn_modules_settings VALUES("98","gallery","image_gallery_type","lytebox","_MSN_IMAGE_GALLERY_TYPE","_MS_IMAGE_GALLERY_TYPE","enum","1","lytebox,rokbox");
INSERT INTO phpn_modules_settings VALUES("99","gallery","album_icon_width","140px","_MSN_ALBUM_ICON_WIDTH","_MS_ALBUM_ICON_WIDTH","html size","1","");
INSERT INTO phpn_modules_settings VALUES("100","gallery","album_icon_height","105px","_MSN_ALBUM_ICON_HEIGHT","_MS_ALBUM_ICON_HEIGHT","html size","1","");
INSERT INTO phpn_modules_settings VALUES("101","gallery","albums_per_line","4","_MSN_ALBUMS_PER_LINE","_MS_ALBUMS_PER_LINE","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("102","gallery","video_gallery_type","rokbox","_MSN_VIDEO_GALLERY_TYPE","_MS_VIDEO_GALLERY_TYPE","enum","1","rokbox,videobox");
INSERT INTO phpn_modules_settings VALUES("103","gallery","wrapper","table","_MSN_GALLERY_WRAPPER","_MS_GALLERY_WRAPPER","enum","1","table,div");
INSERT INTO phpn_modules_settings VALUES("104","gallery","show_items_count_in_album","yes","_MSN_ITEMS_COUNT_IN_ALBUM","_MS_ITEMS_COUNT_IN_ALBUM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("105","gallery","show_items_numeration_in_album","yes","_MSN_ALBUM_ITEMS_NUMERATION","_MS_ALBUM_ITEMS_NUMERATION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("106","news","news_count","5","_MSN_NEWS_COUNT","_MS_NEWS_COUNT","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("107","news","news_header_length","80","_MSN_NEWS_HEADER_LENGTH","_MS_NEWS_HEADER_LENGTH","positive integer","1","");
INSERT INTO phpn_modules_settings VALUES("108","news","news_rss","no","_MSN_NEWS_RSS","_MS_NEWS_RSS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("109","news","show_news_block","no","_MSN_SHOW_NEWS_BLOCK","_MS_SHOW_NEWS_BLOCK","enum","1","no,left side,right side");
INSERT INTO phpn_modules_settings VALUES("110","news","show_newsletter_subscribe_block","no","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","enum","1","no,left side,right side");
INSERT INTO phpn_modules_settings VALUES("111","ratings","user_type","registered","_MSN_RATINGS_USER_TYPE","_MS_RATINGS_USER_TYPE","enum","1","all,registered");
INSERT INTO phpn_modules_settings VALUES("112","ratings","multiple_items_per_day","no","_MSN_MULTIPLE_ITEMS_PER_DAY","_MS_MULTIPLE_ITEMS_PER_DAY","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("113","reviews","key","{module:reviews}","_MSN_REVIEWS_KEY","_MS_REVIEWS_KEY","enum","1","{module:reviews}");
INSERT INTO phpn_modules_settings VALUES("114","rooms","search_availability_period","1","_MSN_SEARCH_AVAILABILITY_PERIOD","_MS_SEARCH_AVAILABILITY_PERIOD","enum","1","1,2,3");
INSERT INTO phpn_modules_settings VALUES("115","rooms","search_availability_page_size","20","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","enum","1","1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,250,500,1000");
INSERT INTO phpn_modules_settings VALUES("116","rooms","show_room_types_in_search","all","_MSN_ROOMS_IN_SEARCH","_MS_ROOMS_IN_SEARCH","enum","1","all,available only");
INSERT INTO phpn_modules_settings VALUES("117","rooms","allow_children","no","_MSN_ALLOW_CHILDREN_IN_ROOM","_MS_ALLOW_CHILDREN_IN_ROOM","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("118","rooms","allow_system_suggestion","yes","_MSN_ALLOW_SYSTEM_SUGGESTION","_MS_ALLOW_SYSTEM_SUGGESTION","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("119","rooms","allow_extra_beds","yes","_MSN_ALLOW_EXTRA_BEDS","_MS_ALLOW_EXTRA_BEDS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("120","rooms","show_default_prices","yes","_MSN_SHOW_DEFAULT_PRICES","_MS_SHOW_DEFAULT_PRICES","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("121","rooms","allow_default_periods","yes","_MSN_ALLOW_DEFAULT_PERIODS","_MS_ALLOW_DEFAULT_PERIODS","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("122","rooms","watermark","no","_MSN_ADD_WATERMARK","_MS_ADD_WATERMARK","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("123","rooms","watermark_text","","_MSN_WATERMARK_TEXT","_MS_WATERMARK_TEXT","string","0","");
INSERT INTO phpn_modules_settings VALUES("124","rooms","max_adults","4","_MSN_MAX_ADULTS_IN_SEARCH","_MS_MAX_ADULTS_IN_SEARCH","enum","1","1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
INSERT INTO phpn_modules_settings VALUES("125","rooms","max_children","1","_MSN_MAX_CHILDREN_IN_SEARCH","_MS_MAX_CHILDREN_IN_SEARCH","enum","1","1,2,3,4,5");
INSERT INTO phpn_modules_settings VALUES("126","rooms","check_partially_overlapping","yes","_MSN_CHECK_PARTIALLY_OVERLAPPING","_MS_CHECK_PARTIALLY_OVERLAPPING","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("127","rooms","show_rooms_occupancy","no","_MSN_SHOW_ROOMS_OCCUPANCY","_MS_SHOW_ROOMS_OCCUPANCY","yes/no","1","");
INSERT INTO phpn_modules_settings VALUES("128","rooms","show_rooms_occupancy_months","3","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","enum","1","3,6,9,12");
INSERT INTO phpn_modules_settings VALUES("129","testimonials","key","{module:testimonials}","_MSN_TESTIMONIALS_KEY","_MS_TESTIMONIALS_KEY","enum","1","{module:testimonials}");



DROP TABLE IF EXISTS phpn_news;

CREATE TABLE `phpn_news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `type` enum('news','events','last_minute') CHARACTER SET latin1 NOT NULL DEFAULT 'news',
  `header_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_text` text COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_news VALUES("1","txj17hkwau","en","news","New version 2.2.6 of uHotelBooking has been released","<p>New version 2.2.6 of uHotelBooking has been released and available now for downloading. There are many improvements and new features added you will like. This version may be installed as a new version or updated, if you have the previous version already installed. All recent changes can be viewed <a href=\"http://www.hotel-booking-script.com/index.php?page=history_of_changes\">here</a>.</p>","2013-11-21 19:39:45","0");
INSERT INTO phpn_news VALUES("4","txj17hkwau","ID","news","New version 2.2.6 of uHotelBooking has been released","<p>New version 2.2.6 of uHotelBooking has been released and available now for downloading. There are many improvements and new features added you will like. This version may be installed as a new version or updated, if you have the previous version already installed. All recent changes can be viewed <a href=\"http://www.hotel-booking-script.com/index.php?page=history_of_changes\">here</a>.</p>","2013-11-21 19:39:45","1");



DROP TABLE IF EXISTS phpn_news_subscribed;

CREATE TABLE `phpn_news_subscribed` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `date_subscribed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_packages;

CREATE TABLE `phpn_packages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `package_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `finish_date` date NOT NULL DEFAULT '0000-00-00',
  `minimum_nights` tinyint(1) NOT NULL DEFAULT '0',
  `maximum_nights` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_pages;

CREATE TABLE `phpn_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `content_type` enum('article','link','') CHARACTER SET latin1 NOT NULL DEFAULT 'article',
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_target` enum('','_self','_blank') COLLATE utf8_unicode_ci NOT NULL,
  `page_key` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_text` text COLLATE utf8_unicode_ci,
  `menu_id` int(11) DEFAULT '0',
  `menu_link` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `tag_description` text COLLATE utf8_unicode_ci NOT NULL,
  `comments_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_publishing` date NOT NULL DEFAULT '0000-00-00',
  `is_home` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_removed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_system_page` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `system_page` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `show_in_search` tinyint(1) NOT NULL DEFAULT '1',
  `status_changed` datetime NOT NULL,
  `access_level` enum('public','registered') CHARACTER SET latin1 NOT NULL DEFAULT 'public',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `is_published` (`is_published`),
  KEY `is_removed` (`is_removed`),
  KEY `language_id` (`language_id`),
  KEY `comments_allowed` (`comments_allowed`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_pages VALUES("1","rpo5bahloy","en","article","","_self","Welcome-to-Pandu-Hotel","Welcome to Pandu Hotel !","<p><img class=\"img-indent\" src=\"images/upload/img1.png\" border=\"0\" alt=\"\" align=\"left\" /></p>\n<p>Come alone or bring your family with you, stay here for a night or for weeks, stay here while on business trip or at some kind of conference - either way our hotel is the best possible variant.</p>\n<p>Our site makes it simple to book your next hotel stay. Whether you\'re traveling for business or pleasure, we provide you with some of the best hotel deals around. Discover cheap rates on rooms near the places you like, tucked away in the mountains, in the heart of the city, or scattered in the countryside. With our site, your hotel reservation options are endless.</p>","0","","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-11 10:48:36","0000-00-00","1","0","1","0","","1","2010-04-24 16:55:05","public","0");
INSERT INTO phpn_pages VALUES("4","99fnhie8in","en","article","","_self","Installation","Installation","<p>Software requirements: PHP 5.0 or later version.</p>\n<p>A new installation of uHotelBooking is a very straight forward process:</p>\n<p><strong>Step 1</strong>. Uncompressing downloaded file.<br />-<br /> Uncompress the uHotelBooking version 4.x.x script archive. The archive will create<br /> a directory called \"uHotelBooking_2xx\"<br /><br /><br /><strong>Step 2</strong>. Uploading files.<br />-<br /> Upload content of this folder (all files and directories it includes) to your <br /> document root (public_html, www, httpdocs etc.) or your booking script directory using FTP.<br /> Pay attention to DON\'T use the capital letters in the name of the folder (for Linux users).</p>\n<p>public_html/<br /> or<br /> public_html/{hotel-site directory}/<br /> <br /> Rename default.htaccess into .htaccess if you need to add PHP5 handler.</p>\n<p><br /><strong>Step 3</strong>. Creating database.<br />-<br /> Using your hosting Control Panel, phpMyAdmin or another tool, create your database<br /> and user, and assign that user to the database. Write down the name of the<br /> database, username, and password for the site installation procedure.</p>\n<p><br /><strong>Step 4</strong>. Running install.php file.<br />-<br /> Now you can run install.php file. To do this, open a browser and type in Address Bar</p>\n<p>http://{www.mydomain.com}/install.php<br /> or<br /> http://{www.mydomain.com}/{hotel-site directory}/install.php</p>\n<p>Follow instructions on the screen. You will be asked to enter: database host,<br /> database name, username and password. Also you need to enter admin username and<br /> admin password, that will be used to get access to administration area of the<br /> site.</p>\n<p><br /><strong>Step 5</strong>. Setting up access permissions.<br />-<br /> Check access permissions to images/uploads/. You need to have 755 permissions <br /> to this folder.</p>\n<p><br /><strong>Step 6</strong>. Deleting install.php file.<br />-<br /> After successful installation you will get an appropriate message and warning to<br /> remove install.php file. For security reasons, please delete install file<br /> immediately.</p>\n<p><br />Congratulations, you now have uHotelBooking installed!</p>","1","Installation","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-11 10:50:09","0000-00-00","0","0","0","0","","0","2012-04-23 20:08:59","registered","0");
INSERT INTO phpn_pages VALUES("37","s3d4fder56","ID","article","","_self","About-Us","About Us","<p>{module:about_us}</p>","0","About Us","About Us","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:02","0000-00-00","0","0","1","1","about_us","1","0000-00-00 00:00:00","public","4");
INSERT INTO phpn_pages VALUES("38","zxcs3d4fd5","ID","article","","_self","Test-Page","Test Page","<p>Test page with comments</p>","1","Test Page","Test Page","php hotel site, hotel online booking site","uHotelBooking","1","2011-05-01 11:16:22","2013-11-21 19:16:15","0000-00-00","0","0","1","0","","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("39","q8mv7zrzmo","ID","article","","_self","Contact-Us","Contact Us","<p>{module:contact_us}</p>","0","Contact Us","Contact Us","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 17:02:41","0000-00-00","0","0","1","1","contact_us","1","0000-00-00 00:00:00","public","5");
INSERT INTO phpn_pages VALUES("40","90jhtyu78y","ID","article","","_self","Terms-and-Conditions","Terms and Conditions","<h4>Conditions of Purchase and Money Back Guarantee</h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in enim sed arcu congue mollis. Mauris sed elementum nulla. Donec eleifend nunc dapibus turpis euismod at commodo mi pulvinar. Praesent vitae metus ligula. Maecenas commodo massa id arcu luctus posuere. Praesent adipiscing scelerisque nisi id accumsan.</p>\n<ul>\n<li>Sed posuere, sem mollis eleifend placerat, nisl magna dapibus nunc, in mattis augue urna ac dui. Nunc mollis venenatis mi. </li>\n<li>A elementum nulla mollis in. Maecenas et mi augue. Nulla euismod mauris sit amet mauris ullamcorper lobortis. </li>\n<li>Vivamus nec ligula nulla. Curabitur non sapien nec lectus euismod consectetur. Morbi ut vestibulum risus. </li>\n</ul>\n<h4><br />Detailed Conditions</h4>\n<p><br />Cras elit purus, dapibus et cursus vel, eleifend interdum neque. Aenean nec magna sit amet felis pellentesque sollicitudin. Praesent ut enim est, quis ornare massa:</p>\n<ul>\n<li>Sed ultrices turpis at dolor dictum eu sollicitudin leo gravida. Praesent leo leo, malesuada nec facilisis non, lobortis eget lacus. </li>\n<li>Donec at orci odio. Aliquam eu nulla felis, eget volutpat enim. Vivamus ullamcorper ligula eu sapien rutrum et hendrerit neque convallis. Sed fringilla tristique arcu, a interdum erat fringilla non. Nunc sit amet sodales leo. </li>\n<li>Quisque luctus lacus nulla. Duis iaculis porttitor velit et feugiat. Nam sed velit libero. Praesent metus mauris, fermentum nec consequat vel, bibendum vel sem. </li>\n</ul>\n<p><br />Etiam auctor est et leo tristique ut scelerisque sapien bibendum. Suspendisse tellus urna, pellentesque eget pellentesque a, dictum in massa.</p>","0","Terms and Conditions","Terms and Conditions","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-06 19:32:11","0000-00-00","0","0","1","1","terms_and_conditions","1","0000-00-00 00:00:00","public","6");
INSERT INTO phpn_pages VALUES("7","afd4vgf5yt","en","article","","_self","Gallery","Gallery","<p>{module:gallery}</p>","0","Gallery","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2013-11-06 16:59:27","0000-00-00","0","0","1","1","gallery","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("10","op8uy67ydd","en","article","","_self","Testimonials","Testimonials","<p>{module:testimonials}</p>","0","Testimonials","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-11 10:53:12","0000-00-00","0","0","0","1","testimonials","0","0000-00-00 00:00:00","public","3");
INSERT INTO phpn_pages VALUES("13","87ghtyfd5t","en","article","","_self","Our-available-Rooms","Our available Rooms","<p>We offer several kinds of rooms, in various sizes and price ranges:<br />{module:rooms}</p>","0","Rooms","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-17 08:41:57","0000-00-00","0","0","1","1","rooms","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("36","45tfrtbfg8","ID","article","","_self","Today’s-featured-menu-item","Today’s featured menu item","<p><img style=\"margin-right: 7px;\" src=\"images/upload/restaurant_dishes.jpg\" border=\"0\" alt=\"\" vspace=\"5\" align=\"left\" /></p>\n<h3 class=\"extra-wrap\">Foie gras!</h3>\n<div class=\"extra-wrap\">\n<ul class=\"list2\">\n<li>Nice and tasty! </li>\n<li>Made from French ingredients! </li>\n<li>Cooked by Italian chef! </li>\n<li>Awarded by world’s assosiation of chef! </li>\n<li>Proved to be good for your health!</li>\n</ul>\n</div>\n<div><strong class=\"txt2\">AS LOW AS €19!</strong></div>\n<p><br /><br /><br /><br /></p>\n<h3><br />Menu/Specials</h3>\n<div class=\"extra-wrap\">\n<ul>\n<li>LYNAGH’S BEER CHEESE <br />Our own recipe, made with Guinness Stout served w/ carrots, celery &amp; crackers. -- $4.99 </li>\n<li>SALSA <br />TAME OR FLAME Homemade salsa served with tortilla chips. The TAME is HOT!!! -- $2.99 </li>\n<li>SPINACH ARTICHOKE DIP <br />Served with tortilla chips. -- $6.49 </li>\n<li>DOC BILL\'S PUB PRETZELS <br />Two jumbo pretzels deep fried and served with hot homemade beer cheese. -- $5.49 </li>\n<li>ULTIMATE IRISH <br />Take the Irish Nacho, add red onions and our famous chili. -- $9.99 </li>\n<li>SPICY QUESO BEEF DIP <br />Ground beef, queso, Mexican spices, jalapenos, and sour cream. That’s gotta be good! -- $6.49 </li>\n<li>DELUXE NACHOS <br />Tortillas smothered with chili, cheese, lettuce tomatoes, jalapenos &amp; sour cream.</li>\n</ul>\n</div>","0","Restaurant","Restaurant","php hotel site, hotel online booking site","Today’s featured menu item","0","2011-05-01 11:16:22","2013-11-12 14:04:55","0000-00-00","0","0","1","1","restaurant","1","0000-00-00 00:00:00","public","2");
INSERT INTO phpn_pages VALUES("16","45tfrtbfg8","en","article","","_self","Today’s-featured-menu-item","Today’s featured menu item","<p><img style=\"margin-right: 7px;\" src=\"images/upload/restaurant_dishes.jpg\" border=\"0\" alt=\"\" vspace=\"5\" align=\"left\" /></p>\n<h3 class=\"extra-wrap\">Foie gras!</h3>\n<div class=\"extra-wrap\">\n<ul class=\"list2\">\n<li>Nice and tasty! </li>\n<li>Made from French ingredients! </li>\n<li>Cooked by Italian chef! </li>\n<li>Awarded by world’s assosiation of chef! </li>\n<li>Proved to be good for your health!</li>\n</ul>\n</div>\n<div><strong class=\"txt2\">AS LOW AS €19!</strong></div>\n<p><br /><br /><br /><br /></p>\n<h3><br />Menu/Specials</h3>\n<div class=\"extra-wrap\">\n<ul>\n<li>LYNAGH’S BEER CHEESE <br />Our own recipe, made with Guinness Stout served w/ carrots, celery &amp; crackers. -- $4.99 </li>\n<li>SALSA <br />TAME OR FLAME Homemade salsa served with tortilla chips. The TAME is HOT!!! -- $2.99 </li>\n<li>SPINACH ARTICHOKE DIP <br />Served with tortilla chips. -- $6.49 </li>\n<li>DOC BILL\'S PUB PRETZELS <br />Two jumbo pretzels deep fried and served with hot homemade beer cheese. -- $5.49 </li>\n<li>ULTIMATE IRISH <br />Take the Irish Nacho, add red onions and our famous chili. -- $9.99 </li>\n<li>SPICY QUESO BEEF DIP <br />Ground beef, queso, Mexican spices, jalapenos, and sour cream. That’s gotta be good! -- $6.49 </li>\n<li>DELUXE NACHOS <br />Tortillas smothered with chili, cheese, lettuce tomatoes, jalapenos &amp; sour cream.</li>\n</ul>\n</div>","0","Restaurant","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-11 10:53:45","0000-00-00","0","0","0","1","restaurant","0","0000-00-00 00:00:00","public","2");
INSERT INTO phpn_pages VALUES("33","afd4vgf5yt","ID","article","","_self","Gallery","Gallery","<p>{module:gallery}</p>","0","Gallery","Gallery","php hotel site, hotel online booking site","View our video and image galleries","0","2011-05-01 11:16:22","2013-11-06 16:59:27","0000-00-00","0","0","1","1","gallery","1","0000-00-00 00:00:00","public","1");
INSERT INTO phpn_pages VALUES("34","op8uy67ydd","ID","article","","_self","Testimonials","Testimonials","<p>{module:testimonials}</p>","0","Testimonials","Testimonials","php hotel site, hotel online booking site","What our clients are saying","0","2011-05-01 11:16:22","2017-02-17 08:44:09","0000-00-00","0","0","0","1","testimonials","1","0000-00-00 00:00:00","public","3");
INSERT INTO phpn_pages VALUES("35","87ghtyfd5t","ID","article","","_self","We-offer-several-kinds-of-rooms","We offer several kinds of rooms","We offer several kinds of rooms and apartments, in various sizes and  price ranges; single rooms in shared kitchens and studio, couples and  family apartments (1 or 2 bedroom). Many apartments are accessible for  wheelchair users.<br>{module:rooms}","0","Rooms","Rooms","php hotel site, hotel online booking site","Check different rooms in our hotel","0","2011-05-01 11:16:22","2013-11-19 19:06:56","0000-00-00","0","0","1","1","rooms","1","0000-00-00 00:00:00","public","0");
INSERT INTO phpn_pages VALUES("19","s3d4fder56","en","article","","_self","About-Us","About Us","<p>{module:about_us}</p>","0","About Us","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-11 10:54:15","0000-00-00","0","0","1","1","about_us","0","0000-00-00 00:00:00","public","4");
INSERT INTO phpn_pages VALUES("22","90jhtyu78y","en","article","","_self","Terms-and-Conditions","Terms and Conditions","<ul>\n<li>Price include breakfast.</li>\n<li>Check in Time : 02.00 pm, Check out Time : 12.00 am.</li>\n<li>Over Capacity will be charged Rp. 150.000/night/person.</li>\n<li>No Smoking Room.</li>\n<li>No Pets are allowed.</li>\n<li>Your reservation will be release automatically by our system if we have not received the payment.</li>\n<li>Please send us your confirmation payment by fax or email to reservation@pandu-hotel.com 1 working day before the date of arrival.</li>\n<li>You warrant that you are at least 18 years of age.</li>\n<li>All bookings are personal to you and may not be sold, assigned or otherwise transferred.</li>\n<li>The booking confirmation and these terms represent the entire agreement between the Pandu Lakeside Hotel and you.</li>\n<li>Your Personal Details are required for our Check in System.</li>\n</ul>","0","Terms and Conditions","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-21 11:59:14","0000-00-00","0","0","1","1","terms_and_conditions","1","0000-00-00 00:00:00","public","6");
INSERT INTO phpn_pages VALUES("32","99fnhie8in","ID","article","","_self","Installation","Installation","<p>Software requirements: PHP 5.0 or later version.</p>\n<p>A new installation of uHotelBooking is a very straight forward process:</p>\n<p><strong>Step 1</strong>. Uncompressing downloaded file.<br />-<br /> Uncompress the uHotelBooking version 4.x.x script archive. The archive will create<br /> a directory called \"uHotelBooking_2xx\"<br /><br /><br /><strong>Step 2</strong>. Uploading files.<br />-<br /> Upload content of this folder (all files and directories it includes) to your <br /> document root (public_html, www, httpdocs etc.) or your booking script directory using FTP.<br /> Pay attention to DON\'T use the capital letters in the name of the folder (for Linux users).</p>\n<p>public_html/<br /> or<br /> public_html/{hotel-site directory}/<br /> <br /> Rename default.htaccess into .htaccess if you need to add PHP5 handler.</p>\n<p><br /><strong>Step 3</strong>. Creating database.<br />-<br /> Using your hosting Control Panel, phpMyAdmin or another tool, create your database<br /> and user, and assign that user to the database. Write down the name of the<br /> database, username, and password for the site installation procedure.</p>\n<p><br /><strong>Step 4</strong>. Running install.php file.<br />-<br /> Now you can run install.php file. To do this, open a browser and type in Address Bar</p>\n<p>http://{www.mydomain.com}/install.php<br /> or<br /> http://{www.mydomain.com}/{hotel-site directory}/install.php</p>\n<p>Follow instructions on the screen. You will be asked to enter: database host,<br /> database name, username and password. Also you need to enter admin username and<br /> admin password, that will be used to get access to administration area of the<br /> site.</p>\n<p><br /><strong>Step 5</strong>. Setting up access permissions.<br />-<br /> Check access permissions to images/uploads/. You need to have 755 permissions <br /> to this folder.</p>\n<p><br /><strong>Step 6</strong>. Deleting install.php file.<br />-<br /> After successful installation you will get an appropriate message and warning to<br /> remove install.php file. For security reasons, please delete install file<br /> immediately.</p>\n<p><br />Congratulations, you now have uHotelBooking installed!</p>","1","Installation","Installation of uHotelBooking","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2013-11-21 19:18:35","0000-00-00","0","0","1","0","","1","2012-04-23 20:08:59","public","0");
INSERT INTO phpn_pages VALUES("25","zxcs3d4fd5","en","article","","_self","Test-Page","Test Page","<p>Test page with comments</p>","1","Test Page","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-11 10:50:51","0000-00-00","0","0","0","0","","0","0000-00-00 00:00:00","registered","1");
INSERT INTO phpn_pages VALUES("28","q8mv7zrzmo","en","article","","_self","Contact-Us","Contact Us","<p>{module:contact_us}</p>","0","Contact Us","Pandu Hotel","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk","Pandu Hotel Indonesia","0","2011-05-01 11:16:22","2017-02-21 11:52:22","0000-00-00","0","0","0","1","contact_us","0","0000-00-00 00:00:00","public","5");
INSERT INTO phpn_pages VALUES("31","rpo5bahloy","ID","article","","_self","uHotelBooking-is-happy-to-welcome-you","uHotelBooking is happy to welcome you!","<p><img class=\"img-indent\" src=\"images/upload/img1.png\" border=\"0\" alt=\"\" align=\"left\" /></p>\n<p>Come alone or bring your family with you, stay here for a night or for weeks, stay here while on business trip or at some kind of conference - either way our hotel is the best possible variant.</p>\n<p>Our site makes it simple to book your next hotel stay. Whether you\'re traveling for business or pleasure, we provide you with some of the best hotel deals around. Discover cheap rates on rooms near the places you like, tucked away in the mountains, in the heart of the city, or scattered in the countryside. With our site, your hotel reservation options are endless.</p>","0","","uHotelBooking","php hotel site, hotel online booking site","uHotelBooking","0","2011-05-01 11:16:22","2016-02-03 17:11:01","0000-00-00","1","0","1","0","","1","2010-04-24 16:55:05","public","0");



DROP TABLE IF EXISTS phpn_privileges;

CREATE TABLE `phpn_privileges` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_privileges VALUES("1","add_menus","Add Menus","Add Menus on the site");
INSERT INTO phpn_privileges VALUES("2","edit_menus","Edit Menus","Edit Menus on the site");
INSERT INTO phpn_privileges VALUES("3","delete_menus","Delete Menus","Delete Menus from the site");
INSERT INTO phpn_privileges VALUES("4","add_pages","Add Pages","Add Pages on the site");
INSERT INTO phpn_privileges VALUES("5","edit_pages","Edit Pages","Edit Pages on the site");
INSERT INTO phpn_privileges VALUES("6","delete_pages","Delete Pages","Delete Pages from the site");
INSERT INTO phpn_privileges VALUES("7","edit_hotel_info","Manage Hotels","See and modify the hotels info");
INSERT INTO phpn_privileges VALUES("8","edit_hotel_rooms","Manage Hotel Rooms","See and modify the hotel rooms info");
INSERT INTO phpn_privileges VALUES("9","view_hotel_reports","See Hotel Reports","See only reports related to assigned hotel");
INSERT INTO phpn_privileges VALUES("10","edit_bookings","Edit Bookings","Edit bookings on the site");
INSERT INTO phpn_privileges VALUES("11","cancel_bookings","Cancel Bookings","Cancel bookings on the site");
INSERT INTO phpn_privileges VALUES("12","delete_bookings","Delete Bookings","Delete bookings from the site");
INSERT INTO phpn_privileges VALUES("13","edit_car_agency_info","Manage Car Agencies","See and modify the car agency info");
INSERT INTO phpn_privileges VALUES("14","edit_car_agency_vehicles","Manage Car Agencies Vehicles","See and modify the car agency vehicles");
INSERT INTO phpn_privileges VALUES("15","edit_car_reservations","Edit Car Reservations","Edit car reservations on the site");
INSERT INTO phpn_privileges VALUES("16","cancel_car_reservations","Cancel Car Reservations","Cancel car reservations on the site");
INSERT INTO phpn_privileges VALUES("17","delete_car_reservations","Delete Car Reservations","Delete car reservations from the site");



DROP TABLE IF EXISTS phpn_ratings_items;

CREATE TABLE `phpn_ratings_items` (
  `item` varchar(200) NOT NULL DEFAULT '',
  `totalrate` int(10) NOT NULL DEFAULT '0',
  `nrrates` int(9) NOT NULL DEFAULT '1',
  PRIMARY KEY (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS phpn_ratings_users;

CREATE TABLE `phpn_ratings_users` (
  `day` int(2) DEFAULT NULL,
  `rater` varchar(15) DEFAULT NULL,
  `item` varchar(200) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS phpn_reviews;

CREATE TABLE `phpn_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `positive_comments` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `negative_comments` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rating_cleanliness` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_room_comfort` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_location` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_service` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_sleep_quality` float(2,1) NOT NULL DEFAULT '0.0',
  `rating_price` float(2,1) NOT NULL DEFAULT '0.0',
  `evaluation` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0 - Not Recommend, 1 - Not Good, 2 - Neutral, 3 - Good, 4 - Very Good, 5 - Wonderful',
  `image_file_1` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_1_thumb` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_2` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_2_thumb` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_3` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_file_3_thumb` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `priority_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO phpn_reviews VALUES("1","1","1","Great experience","Nice large hotel located on the Red Sea. The breakfast buffet is GREAT! Good location, good beach, good facilities. Staff was more or less friendly, with some exceptions. Nice breakfasts.","Noisy at evenings, especially when lots of people arrive. The hotel facilities are outdated. When the hotel is heavily booked it becomes too crowded everywhere and not nice to be.","2.0","3.0","4.0","4.1","4.2","4.0","4","","","","","","","2013-01-02 12:03:00","1","0");
INSERT INTO phpn_reviews VALUES("2","1","2","I liked it very mutch!","It is close to everything but if you go in the lower season the pool won\'t be ready even though the temperature was quiet high already.","Your front staff was wonderful, i believe her name was Moran, please thank her from us.","4.4","2.3","3.0","5.0","4.0","4.0","5","","","","","","","2013-01-04 13:04:00","1","1");
INSERT INTO phpn_reviews VALUES("3","1","3","Excellent hotel, very good!","The view from our balcony in room # 409, was terrific. It was centrally located to everything on and around the port area. Wonderful service and everything was very clean.","The breakfast was below average, although not bad. If back in this town we would stay there again.","4.0","5.0","4.1","4.3","4.0","3.0","3","","","","","","","2013-01-04 15:04:00","1","2");
INSERT INTO phpn_reviews VALUES("4","1","4","Great experience","Excellent hotel, friendly staff would def go there again. Breakfast is from 7:00 until 12:00, which is Great. Breakfast is really good and worth the cost.","","4.1","3.2","4.0","4.0","5.0","4.0","4","","","","","","","2013-01-05 12:05:00","1","3");
INSERT INTO phpn_reviews VALUES("5","2","1","Super, great experience","Rooms clean tidy and serviced daily. Pool stunning.","Nice location, but the hotel is so old...","3.2","4.2","4.0","2.5","5.0","4.0","3","","","","","","","2013-01-06 16:05:00","1","4");



DROP TABLE IF EXISTS phpn_role_privileges;

CREATE TABLE `phpn_role_privileges` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(5) NOT NULL,
  `privilege_id` int(5) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_role_privileges VALUES("1","1","1","1");
INSERT INTO phpn_role_privileges VALUES("2","1","2","1");
INSERT INTO phpn_role_privileges VALUES("3","1","3","1");
INSERT INTO phpn_role_privileges VALUES("4","1","4","1");
INSERT INTO phpn_role_privileges VALUES("5","1","5","1");
INSERT INTO phpn_role_privileges VALUES("6","1","6","1");
INSERT INTO phpn_role_privileges VALUES("7","1","10","1");
INSERT INTO phpn_role_privileges VALUES("8","1","11","1");
INSERT INTO phpn_role_privileges VALUES("9","1","12","1");
INSERT INTO phpn_role_privileges VALUES("10","1","13","1");
INSERT INTO phpn_role_privileges VALUES("11","1","14","1");
INSERT INTO phpn_role_privileges VALUES("12","1","15","1");
INSERT INTO phpn_role_privileges VALUES("13","1","16","1");
INSERT INTO phpn_role_privileges VALUES("14","1","17","1");
INSERT INTO phpn_role_privileges VALUES("15","2","1","1");
INSERT INTO phpn_role_privileges VALUES("16","2","2","1");
INSERT INTO phpn_role_privileges VALUES("17","2","3","1");
INSERT INTO phpn_role_privileges VALUES("18","2","4","1");
INSERT INTO phpn_role_privileges VALUES("19","2","5","1");
INSERT INTO phpn_role_privileges VALUES("20","2","6","1");
INSERT INTO phpn_role_privileges VALUES("21","2","10","1");
INSERT INTO phpn_role_privileges VALUES("22","2","11","1");
INSERT INTO phpn_role_privileges VALUES("23","2","12","1");
INSERT INTO phpn_role_privileges VALUES("24","2","13","1");
INSERT INTO phpn_role_privileges VALUES("25","2","14","1");
INSERT INTO phpn_role_privileges VALUES("26","2","15","1");
INSERT INTO phpn_role_privileges VALUES("27","2","16","1");
INSERT INTO phpn_role_privileges VALUES("28","2","17","1");
INSERT INTO phpn_role_privileges VALUES("29","3","1","0");
INSERT INTO phpn_role_privileges VALUES("30","3","2","1");
INSERT INTO phpn_role_privileges VALUES("31","3","3","0");
INSERT INTO phpn_role_privileges VALUES("32","3","4","1");
INSERT INTO phpn_role_privileges VALUES("33","3","5","1");
INSERT INTO phpn_role_privileges VALUES("34","3","6","0");
INSERT INTO phpn_role_privileges VALUES("35","3","10","1");
INSERT INTO phpn_role_privileges VALUES("36","3","11","0");
INSERT INTO phpn_role_privileges VALUES("37","3","12","0");
INSERT INTO phpn_role_privileges VALUES("38","3","13","0");
INSERT INTO phpn_role_privileges VALUES("39","3","14","0");
INSERT INTO phpn_role_privileges VALUES("40","3","15","0");
INSERT INTO phpn_role_privileges VALUES("41","3","16","0");
INSERT INTO phpn_role_privileges VALUES("42","3","17","0");
INSERT INTO phpn_role_privileges VALUES("43","4","7","1");
INSERT INTO phpn_role_privileges VALUES("44","4","8","1");
INSERT INTO phpn_role_privileges VALUES("45","4","9","1");
INSERT INTO phpn_role_privileges VALUES("46","4","10","1");
INSERT INTO phpn_role_privileges VALUES("47","4","11","1");
INSERT INTO phpn_role_privileges VALUES("48","4","12","1");
INSERT INTO phpn_role_privileges VALUES("49","5","13","1");
INSERT INTO phpn_role_privileges VALUES("50","5","14","1");
INSERT INTO phpn_role_privileges VALUES("51","5","15","1");
INSERT INTO phpn_role_privileges VALUES("52","5","16","1");
INSERT INTO phpn_role_privileges VALUES("53","5","17","1");



DROP TABLE IF EXISTS phpn_roles;

CREATE TABLE `phpn_roles` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_roles VALUES("1","owner","Site Owner","Site Owner is the owner of the site, has all privileges and could not be removed.");
INSERT INTO phpn_roles VALUES("2","mainadmin","Main Admin","The \"Main Administrator\" user has top privileges like Site Owner and may be removed only by him.");
INSERT INTO phpn_roles VALUES("3","admin","Simple Admin","The \"Simple Admin\" is required to assist the Main Admins, has different privileges and may be created by Site Owner or Main Admins.");
INSERT INTO phpn_roles VALUES("4","hotelowner","Hotel Owner","The \"Hotel Owner\" is the owner of the hotel, has special privileges to the hotels/rooms he/she assigned to.");
INSERT INTO phpn_roles VALUES("5","agencyowner","Car Agency Owner","The \"Car Agency Owner\" is the owner of the car agency, has special privileges to the agency/cars he/she assigned to.");



DROP TABLE IF EXISTS phpn_room_facilities;

CREATE TABLE `phpn_room_facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_room_facilities VALUES("1","1","bar","1");
INSERT INTO phpn_room_facilities VALUES("2","2","microwave","1");
INSERT INTO phpn_room_facilities VALUES("3","3","washing","1");
INSERT INTO phpn_room_facilities VALUES("4","4","pets","1");
INSERT INTO phpn_room_facilities VALUES("5","5","roomservice","1");
INSERT INTO phpn_room_facilities VALUES("6","6","fridge","1");
INSERT INTO phpn_room_facilities VALUES("7","7","tv","1");
INSERT INTO phpn_room_facilities VALUES("8","8","pool","1");
INSERT INTO phpn_room_facilities VALUES("9","9","grill","1");
INSERT INTO phpn_room_facilities VALUES("10","10","garden","1");
INSERT INTO phpn_room_facilities VALUES("11","11","kitchen","1");
INSERT INTO phpn_room_facilities VALUES("12","12","internet","1");
INSERT INTO phpn_room_facilities VALUES("13","13","parking","1");
INSERT INTO phpn_room_facilities VALUES("14","14","childcare","1");
INSERT INTO phpn_room_facilities VALUES("15","15","internet","1");
INSERT INTO phpn_room_facilities VALUES("16","16","hairdryer","1");
INSERT INTO phpn_room_facilities VALUES("17","17","conferenceroom","1");
INSERT INTO phpn_room_facilities VALUES("18","18","air","1");
INSERT INTO phpn_room_facilities VALUES("19","19","playground","1");
INSERT INTO phpn_room_facilities VALUES("21","20","fitness","1");
INSERT INTO phpn_room_facilities VALUES("22","21","breakfast","1");
INSERT INTO phpn_room_facilities VALUES("23","22","spa","1");
INSERT INTO phpn_room_facilities VALUES("24","23","safe","1");
INSERT INTO phpn_room_facilities VALUES("25","24","cafe","1");
INSERT INTO phpn_room_facilities VALUES("27","25","","1");
INSERT INTO phpn_room_facilities VALUES("28","26","","1");



DROP TABLE IF EXISTS phpn_room_facilities_description;

CREATE TABLE `phpn_room_facilities_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_facility_id` int(10) unsigned NOT NULL DEFAULT '0',
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_room_facilities_description VALUES("1","1","en","Bar","");
INSERT INTO phpn_room_facilities_description VALUES("93","18","ID","Air Conditioning","");
INSERT INTO phpn_room_facilities_description VALUES("4","2","en","Microwave","");
INSERT INTO phpn_room_facilities_description VALUES("8","3","en","Washing","");
INSERT INTO phpn_room_facilities_description VALUES("92","17","ID","Suitable for Events","");
INSERT INTO phpn_room_facilities_description VALUES("10","4","en","Pets allowed","");
INSERT INTO phpn_room_facilities_description VALUES("91","16","ID","Washer/Dryer","");
INSERT INTO phpn_room_facilities_description VALUES("13","5","en","Room Service","");
INSERT INTO phpn_room_facilities_description VALUES("16","6","en","Fridge","");
INSERT INTO phpn_room_facilities_description VALUES("90","15","ID","Wireless Internet","");
INSERT INTO phpn_room_facilities_description VALUES("19","7","en","TV","");
INSERT INTO phpn_room_facilities_description VALUES("22","8","en","Pool","");
INSERT INTO phpn_room_facilities_description VALUES("105","28","ID","Ruang Pertemuan","");
INSERT INTO phpn_room_facilities_description VALUES("89","14","ID","Family/Kid Friendly","");
INSERT INTO phpn_room_facilities_description VALUES("25","9","en","Grill","");
INSERT INTO phpn_room_facilities_description VALUES("29","10","en","Garden","");
INSERT INTO phpn_room_facilities_description VALUES("88","13","ID","Parking Included","");
INSERT INTO phpn_room_facilities_description VALUES("31","11","en","Kitchen","");
INSERT INTO phpn_room_facilities_description VALUES("104","28","en","Meeting Room","");
INSERT INTO phpn_room_facilities_description VALUES("87","12","ID","Internet","");
INSERT INTO phpn_room_facilities_description VALUES("34","12","en","Internet","");
INSERT INTO phpn_room_facilities_description VALUES("86","11","ID","Kitchen","");
INSERT INTO phpn_room_facilities_description VALUES("37","13","en","Parking Included","");
INSERT INTO phpn_room_facilities_description VALUES("103","27","ID","Pemandangan Danau","");
INSERT INTO phpn_room_facilities_description VALUES("85","10","ID","Garden","");
INSERT INTO phpn_room_facilities_description VALUES("40","14","en","Family/Kid Friendly","");
INSERT INTO phpn_room_facilities_description VALUES("102","27","en","Lake View","");
INSERT INTO phpn_room_facilities_description VALUES("43","15","en","Wireless Internet","");
INSERT INTO phpn_room_facilities_description VALUES("99","25","ID","Cafe","");
INSERT INTO phpn_room_facilities_description VALUES("84","9","ID","Grill","");
INSERT INTO phpn_room_facilities_description VALUES("46","16","en","Washer/Dryer","");
INSERT INTO phpn_room_facilities_description VALUES("98","24","ID","Safe","");
INSERT INTO phpn_room_facilities_description VALUES("83","8","ID","Pool","");
INSERT INTO phpn_room_facilities_description VALUES("49","17","en","Suitable for Events","");
INSERT INTO phpn_room_facilities_description VALUES("97","23","ID","SPA","");
INSERT INTO phpn_room_facilities_description VALUES("82","7","ID","TV","");
INSERT INTO phpn_room_facilities_description VALUES("52","18","en","Air Conditioning","");
INSERT INTO phpn_room_facilities_description VALUES("96","22","ID","Breakfast","");
INSERT INTO phpn_room_facilities_description VALUES("81","6","ID","Fridge","");
INSERT INTO phpn_room_facilities_description VALUES("55","19","en","Playground","");
INSERT INTO phpn_room_facilities_description VALUES("95","21","ID","Fitness","");
INSERT INTO phpn_room_facilities_description VALUES("80","5","ID","Room Service","");
INSERT INTO phpn_room_facilities_description VALUES("79","4","ID","Pets allowed","");
INSERT INTO phpn_room_facilities_description VALUES("61","21","en","Fitness","");
INSERT INTO phpn_room_facilities_description VALUES("64","22","en","Breakfast","");
INSERT INTO phpn_room_facilities_description VALUES("78","3","ID","Washing","");
INSERT INTO phpn_room_facilities_description VALUES("67","23","en","SPA","");
INSERT INTO phpn_room_facilities_description VALUES("94","19","ID","Playground","");
INSERT INTO phpn_room_facilities_description VALUES("70","24","en","Safe","");
INSERT INTO phpn_room_facilities_description VALUES("77","2","ID","Microwave","");
INSERT INTO phpn_room_facilities_description VALUES("73","25","en","Cafe","");
INSERT INTO phpn_room_facilities_description VALUES("76","1","ID","Bar","");



DROP TABLE IF EXISTS phpn_rooms;

CREATE TABLE `phpn_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `room_type` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `room_short_description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `room_long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `room_count` smallint(6) NOT NULL,
  `max_adults` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `max_extra_beds` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `max_children` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `extra_bed_charge` decimal(10,2) unsigned NOT NULL,
  `discount_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - fixed price, 1 - percentage',
  `discount_night_3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_night_4` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_night_5` decimal(10,2) NOT NULL DEFAULT '0.00',
  `default_availability` tinyint(1) NOT NULL DEFAULT '1',
  `beds` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `bathrooms` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `room_area` decimal(4,1) unsigned NOT NULL DEFAULT '0.0',
  `facilities` varchar(400) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_icon_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_1` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_1_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_2` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_2_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_3` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_3_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_4` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_4_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_5` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `room_picture_5_thumb` varchar(70) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms VALUES("11","5","Standard Room - Two Single Beds","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room with two single sized beds and a TV.</span></p>","","16","2","1","0","320000.00","125000.00","0","0.00","0.00","0.00","1","2","1","0.0","a:15:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";i:4;s:2:\"10\";i:5;s:2:\"12\";i:6;s:2:\"13\";i:7;s:2:\"14\";i:8;s:2:\"15\";i:9;s:2:\"17\";i:10;s:2:\"18\";i:11;s:2:\"22\";i:12;s:2:\"25\";i:13;s:2:\"27\";i:14;s:2:\"28\";}","5_icon_p8uhu8r8y21a5urn0df1.jpg","5_icon_p8uhu8r8y21a5urn0df1_thumb.jpg","5_view1_nx5yiue2iwg54blyjd7h.jpg","5_view1_nx5yiue2iwg54blyjd7h_thumb.jpg","5_view2_k7rj1j7awmbgjfe3at5t.jpg","5_view2_k7rj1j7awmbgjfe3at5t_thumb.jpg","5_view3_skxtwjsfwywb6ov783g5.jpg","5_view3_skxtwjsfwywb6ov783g5_thumb.jpg","","","","","0","1");
INSERT INTO phpn_rooms VALUES("12","5","Deluxe Room - Two Single Beds w/ sofabed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with two single beds, a sofabed, and a TV. These units are loca...</span></p>","","3","2","1","0","400000.00","125000.00","0","0.00","0.00","0.00","1","2","1","25.0","a:12:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";i:4;s:2:\"10\";i:5;s:2:\"12\";i:6;s:2:\"13\";i:7;s:2:\"15\";i:8;s:2:\"17\";i:9;s:2:\"22\";i:10;s:2:\"27\";i:11;s:2:\"28\";}","5_icon_bpr6u7vvfkfzh2sr55h0.jpg","5_icon_bpr6u7vvfkfzh2sr55h0_thumb.jpg","5_view1_xn8hm5wj0wjbmahghcbw.jpg","5_view1_xn8hm5wj0wjbmahghcbw_thumb.jpg","5_view2_xrwfto7xto8pce8yi5ir.jpg","5_view2_xrwfto7xto8pce8yi5ir_thumb.jpg","5_view3_a23mck3txequ7n0zcfw6.jpg","5_view3_a23mck3txequ7n0zcfw6_thumb.jpg","","","","","2","1");
INSERT INTO phpn_rooms VALUES("10","5","Standard Room - King Sized Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with one King sized bed and a TV</span></p>","","7","2","2","0","350000.00","125000.00","0","0.00","0.00","0.00","1","1","1","20.0","a:15:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";i:4;s:2:\"10\";i:5;s:2:\"12\";i:6;s:2:\"13\";i:7;s:2:\"14\";i:8;s:2:\"15\";i:9;s:2:\"17\";i:10;s:2:\"22\";i:11;s:2:\"24\";i:12;s:2:\"25\";i:13;s:2:\"27\";i:14;s:2:\"28\";}","5_icon_ikxy1att5b523uv5lqen.jpg","5_icon_ikxy1att5b523uv5lqen_thumb.jpg","5_view1_izsexjzv6bjhwggwrapw.jpg","5_view1_izsexjzv6bjhwggwrapw_thumb.jpg","5_view2_puyooutak8xw8qa5aa1g.jpg","5_view2_puyooutak8xw8qa5aa1g_thumb.jpg","","","","","","","1","1");
INSERT INTO phpn_rooms VALUES("13","5","Deluxe Room - Four Single Beds","<table class=\"mgrid_table\" style=\"font-size: 12px; color: #222222; width: 382px; font-family: arial, verdana, sans-serif;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\n<tbody>\n<tr class=\"highlight_light\">\n<td align=\"left\"><label class=\"mgrid_label\" title=\"A unit with four single beds and a TV. These units are located on the top floor. A door connects this room to another room.\">A unit with four single beds and a TV. These units are located on the ...</label></td>\n</tr>\n</tbody>\n</table>","","2","4","1","0","400000.00","125000.00","0","0.00","0.00","0.00","1","2","1","25.0","a:14:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:1:\"8\";i:3;s:1:\"9\";i:4;s:2:\"10\";i:5;s:2:\"12\";i:6;s:2:\"13\";i:7;s:2:\"14\";i:8;s:2:\"15\";i:9;s:2:\"17\";i:10;s:2:\"22\";i:11;s:2:\"25\";i:12;s:2:\"27\";i:13;s:2:\"28\";}","5_icon_i4j0wbo5kpdnb0tcgwba.jpg","5_icon_i4j0wbo5kpdnb0tcgwba_thumb.jpg","5_view1_kcctj7lqfdosg82diqi4.jpg","5_view1_kcctj7lqfdosg82diqi4_thumb.jpg","5_view2_lvqr6k5lgfvus9obf03v.jpg","5_view2_lvqr6k5lgfvus9obf03v_thumb.jpg","5_view3_qrn5yphgfzjwvan1vrlc.jpg","5_view3_qrn5yphgfzjwvan1vrlc_thumb.jpg","","","","","3","1");
INSERT INTO phpn_rooms VALUES("14","5","Family Room 1","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with two single sized bed and a sofabed, a living room, a kitch...</span></p>","","1","2","1","0","485000.00","125000.00","0","0.00","0.00","0.00","1","1","1","50.0","a:16:{i:0;s:1:\"2\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"17\";i:13;s:2:\"22\";i:14;s:2:\"25\";i:15;s:2:\"28\";}","5_icon_svslq03aezwz3aezgx6m.jpg","5_icon_svslq03aezwz3aezgx6m_thumb.jpg","5_view1_jkpd6gcxq23gxu2n55yi.jpg","5_view1_jkpd6gcxq23gxu2n55yi_thumb.jpg","5_view2_cui76x7mub99vxm2dyz5.jpg","5_view2_cui76x7mub99vxm2dyz5_thumb.jpg","5_view3_z2lwwnk1rijvd22iz85t.jpg","5_view3_z2lwwnk1rijvd22iz85t_thumb.jpg","5_view4_nc2fanhomgtlieie23es.jpg","5_view4_nc2fanhomgtlieie23es_thumb.jpg","","","4","1");
INSERT INTO phpn_rooms VALUES("15","5","Family Room 2","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with four Single Beds, a living room with two sofabeds, a livin...</span></p>","","1","4","2","0","750000.00","125000.00","0","0.00","0.00","0.00","1","2","2","100.0","a:17:{i:0;s:1:\"2\";i:1;s:1:\"5\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"11\";i:8;s:2:\"12\";i:9;s:2:\"13\";i:10;s:2:\"14\";i:11;s:2:\"15\";i:12;s:2:\"17\";i:13;s:2:\"22\";i:14;s:2:\"25\";i:15;s:2:\"27\";i:16;s:2:\"28\";}","5_icon_h3plda8n4vo6h8ucfdel.jpg","5_icon_h3plda8n4vo6h8ucfdel_thumb.jpg","5_view1_azq6yaw16jefl52ze9mi.jpg","5_view1_azq6yaw16jefl52ze9mi_thumb.jpg","5_view2_cbnljhyyvdjvda2blybq.jpg","5_view2_cbnljhyyvdjvda2blybq_thumb.jpg","5_view3_mp73u839goqk1e7jw5ir.jpg","5_view3_mp73u839goqk1e7jw5ir_thumb.jpg","5_view4_m3nudp7ynhp58v8240ak.jpg","5_view4_m3nudp7ynhp58v8240ak_thumb.jpg","","","5","1");
INSERT INTO phpn_rooms VALUES("24","5","Economic Room Triple Bed","<p>3 pcs of bed and count by person, high season only</p>","<p>include: 3 pcs of bed, bathroom, breakfast.</p>","3","1","0","0","60000.00","0.00","0","0.00","0.00","0.00","1","3","1","0.0","","","","","","","","","","","","","","6","1");
INSERT INTO phpn_rooms VALUES("25","5","Economic Room Double Bed","<p>2 pcs of bed, count by person, high season only</p>","<p>Include: 2 pcs of bed,  bathroom, breakfast.</p>","2","1","0","0","60000.00","0.00","0","0.00","0.00","0.00","1","2","1","0.0","","","","","","","","","","","","","","7","1");
INSERT INTO phpn_rooms VALUES("17","6","Superior Room - King Size Bed	","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A superior room for two people with one king sized bed and TV</span></p>","","2","2","1","0","350000.00","125000.00","0","0.00","0.00","0.00","1","1","1","60.0","","6_icon_lhatuwlj9zwdgskjok9c.jpg","6_icon_lhatuwlj9zwdgskjok9c_thumb.jpg","","","","","","","","","","","0","1");
INSERT INTO phpn_rooms VALUES("18","6","Superior Room - Two Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for two people with two single bed</span></p>","","18","2","1","0","350000.00","125000.00","0","0.00","0.00","0.00","1","2","1","60.0","a:12:{i:0;s:1:\"5\";i:1;s:1:\"7\";i:2;s:2:\"10\";i:3;s:2:\"13\";i:4;s:2:\"14\";i:5;s:2:\"15\";i:6;s:2:\"17\";i:7;s:2:\"19\";i:8;s:2:\"22\";i:9;s:2:\"24\";i:10;s:2:\"27\";i:11;s:2:\"28\";}","6_icon_ylxqt3dulh5omt8a8al8.jpg","6_icon_ylxqt3dulh5omt8a8al8_thumb.jpg","","","","","","","","","","","1","1");
INSERT INTO phpn_rooms VALUES("19","6","Economy Room - Two Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for two people with two single sized bed. The room is located b...</span></p>","","2","2","1","0","350000.00","120000.00","0","0.00","0.00","0.00","1","2","1","50.0","a:16:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:1:\"9\";i:6;s:2:\"10\";i:7;s:2:\"12\";i:8;s:2:\"13\";i:9;s:2:\"14\";i:10;s:2:\"15\";i:11;s:2:\"17\";i:12;s:2:\"18\";i:13;s:2:\"19\";i:14;s:2:\"22\";i:15;s:2:\"28\";}","","","","","","","","","","","","","2","1");
INSERT INTO phpn_rooms VALUES("20","7","Standard Fans Room - Two Single ...","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for 2 people with two single sized beds and fans.</span></p>","","3","2","1","0","150000.00","50000.00","1","5.00","10.00","15.00","1","2","1","30.0","a:8:{i:0;s:1:\"5\";i:1;s:2:\"10\";i:2;s:2:\"12\";i:3;s:2:\"13\";i:4;s:2:\"14\";i:5;s:2:\"15\";i:6;s:2:\"25\";i:7;s:2:\"28\";}","7_icon_g91es5g2k4ipwrvlr7oz.jpg","7_icon_g91es5g2k4ipwrvlr7oz_thumb.jpg","7_view1_rntmwftqny2w83bz7r2r.jpg","7_view1_rntmwftqny2w83bz7r2r_thumb.jpg","7_view2_wjgracd2j227putlamcx.jpg","7_view2_wjgracd2j227putlamcx_thumb.jpg","7_view3_pdusf6smxtdsdujn7wpp.jpg","7_view3_pdusf6smxtdsdujn7wpp_thumb.jpg","7_view4_zrwomq0wcmuy1oqfuj3r.jpg","7_view4_zrwomq0wcmuy1oqfuj3r_thumb.jpg","","","0","1");
INSERT INTO phpn_rooms VALUES("21","7","Standard Fans Room - King Sized ...","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with one king sized bed and fans.</span></p>","","3","2","1","0","150000.00","50000.00","1","5.00","10.00","15.00","1","1","1","30.0","a:8:{i:0;s:1:\"5\";i:1;s:2:\"10\";i:2;s:2:\"12\";i:3;s:2:\"13\";i:4;s:2:\"14\";i:5;s:2:\"15\";i:6;s:2:\"17\";i:7;s:2:\"28\";}","","","","","","","","","","","","","1","1");
INSERT INTO phpn_rooms VALUES("22","7","Standard AC Room - Two Single Beds","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for 2 people with two single beds and an AC.</span></p>","","3","2","1","0","220000.00","50000.00","1","5.00","10.00","15.00","1","2","1","20.0","a:10:{i:0;s:1:\"5\";i:1;s:2:\"10\";i:2;s:2:\"12\";i:3;s:2:\"13\";i:4;s:2:\"14\";i:5;s:2:\"15\";i:6;s:2:\"17\";i:7;s:2:\"18\";i:8;s:2:\"25\";i:9;s:2:\"28\";}","","","","","","","","","","","","","2","1");
INSERT INTO phpn_rooms VALUES("23","7","Standard AC Room - King Sized Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with a king sized bed and an AC</span></p>","","3","2","1","0","220000.00","50000.00","1","5.00","10.00","15.00","1","1","1","30.0","a:11:{i:0;s:1:\"1\";i:1;s:1:\"5\";i:2;s:2:\"10\";i:3;s:2:\"12\";i:4;s:2:\"13\";i:5;s:2:\"14\";i:6;s:2:\"15\";i:7;s:2:\"17\";i:8;s:2:\"18\";i:9;s:2:\"25\";i:10;s:2:\"28\";}","","","","","","","","","","","","","3","1");
INSERT INTO phpn_rooms VALUES("26","6","Economy Room Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for one people with one single sized bed</span></p>","<p>Include: 2 pcs of twin bed, bathroom, hot water, breakfast.</p>","1","1","0","0","60000.00","0.00","0","0.00","0.00","0.00","1","1","0","0.0","","","","","","","","","","","","","","3","1");



DROP TABLE IF EXISTS phpn_rooms_availabilities;

CREATE TABLE `phpn_rooms_availabilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned NOT NULL DEFAULT '0',
  `y` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - current year, 1 - next year',
  `m` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `d1` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d2` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d3` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d4` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d5` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d6` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d7` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d8` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d9` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d10` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d11` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d12` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d13` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d14` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d15` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d16` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d17` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d18` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d19` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d20` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d21` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d22` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d23` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d24` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d25` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d26` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d27` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d28` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d29` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d30` smallint(6) unsigned NOT NULL DEFAULT '0',
  `d31` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `y` (`y`),
  KEY `m` (`m`)
) ENGINE=MyISAM AUTO_INCREMENT=625 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms_availabilities VALUES("1","1","0","1","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("2","1","0","2","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("3","1","0","3","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("4","1","0","4","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("5","1","0","5","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("6","1","0","6","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("7","1","0","7","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("8","1","0","8","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("9","1","0","9","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("10","1","0","10","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("11","1","0","11","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("12","1","0","12","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("13","1","1","1","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("14","1","1","2","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("15","1","1","3","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("16","1","1","4","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("17","1","1","5","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("18","1","1","6","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("19","1","1","7","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("20","1","1","8","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("21","1","1","9","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("22","1","1","10","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("23","1","1","11","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","0");
INSERT INTO phpn_rooms_availabilities VALUES("24","1","1","12","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15");
INSERT INTO phpn_rooms_availabilities VALUES("25","2","0","1","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("26","2","0","2","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("27","2","0","3","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("28","2","0","4","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("29","2","0","5","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("30","2","0","6","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("31","2","0","7","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("32","2","0","8","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","2","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("33","2","0","9","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("34","2","0","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("35","2","0","11","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("36","2","0","12","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("37","2","1","1","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("38","2","1","2","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("39","2","1","3","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("40","2","1","4","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("41","2","1","5","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("42","2","1","6","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("43","2","1","7","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("44","2","1","8","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("45","2","1","9","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("46","2","1","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("47","2","1","11","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","0");
INSERT INTO phpn_rooms_availabilities VALUES("48","2","1","12","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10");
INSERT INTO phpn_rooms_availabilities VALUES("49","3","0","1","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("50","3","0","2","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("51","3","0","3","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("52","3","0","4","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("53","3","0","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("54","3","0","6","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("55","3","0","7","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("56","3","0","8","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("57","3","0","9","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("58","3","0","10","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("59","3","0","11","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("60","3","0","12","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("61","3","1","1","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("62","3","1","2","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("63","3","1","3","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("64","3","1","4","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("65","3","1","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("66","3","1","6","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("67","3","1","7","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("68","3","1","8","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("69","3","1","9","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("70","3","1","10","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("71","3","1","11","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","0");
INSERT INTO phpn_rooms_availabilities VALUES("72","3","1","12","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5","5");
INSERT INTO phpn_rooms_availabilities VALUES("73","4","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("74","4","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("75","4","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("76","4","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("77","4","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("78","4","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("79","4","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("80","4","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("81","4","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("82","4","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("83","4","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("84","4","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("85","4","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("86","4","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("87","4","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("88","4","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("89","4","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("90","4","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("91","4","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("92","4","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("93","4","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("94","4","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("95","4","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("96","4","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("97","5","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("98","5","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("99","5","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("100","5","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("101","5","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("102","5","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("103","5","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("104","5","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("105","5","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("106","5","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("107","5","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("108","5","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("109","5","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("110","5","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("111","5","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("112","5","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("113","5","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("114","5","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("115","5","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("116","5","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("117","5","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("118","5","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("119","5","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("120","5","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("121","6","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("122","6","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("123","6","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("124","6","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("125","6","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("126","6","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("127","6","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("128","6","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("129","6","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("130","6","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("131","6","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("132","6","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("133","6","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("134","6","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("135","6","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("136","6","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("137","6","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("138","6","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("139","6","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("140","6","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("141","6","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("142","6","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("143","6","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("144","6","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("145","7","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("146","7","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("147","7","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("148","7","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("149","7","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("150","7","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("151","7","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("152","7","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("153","7","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("154","7","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("155","7","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("156","7","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("157","7","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("158","7","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("159","7","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("160","7","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("161","7","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("162","7","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("163","7","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("164","7","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("165","7","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("166","7","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("167","7","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("168","7","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("169","8","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("170","8","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("171","8","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("172","8","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("173","8","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("174","8","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("175","8","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("176","8","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("177","8","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("178","8","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("179","8","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("180","8","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("181","8","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("182","8","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0","0","0");
INSERT INTO phpn_rooms_availabilities VALUES("183","8","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("184","8","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("185","8","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("186","8","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("187","8","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("188","8","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("189","8","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("190","8","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("191","8","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","0");
INSERT INTO phpn_rooms_availabilities VALUES("192","8","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("193","9","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("194","9","0","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("195","9","0","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("196","9","0","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("197","9","0","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("198","9","0","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("199","9","0","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("200","9","0","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("201","9","0","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("202","9","0","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("203","9","0","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("204","9","0","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("205","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("206","9","1","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("207","9","1","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("208","9","1","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("209","9","1","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("210","9","1","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("211","9","1","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("212","9","1","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("213","9","1","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("214","9","1","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("215","9","1","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("216","9","1","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("217","10","0","1","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("218","10","0","2","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("219","10","0","3","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("220","10","0","4","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("221","10","0","5","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("222","10","0","6","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("223","10","0","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("224","10","0","8","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("225","10","0","9","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("226","10","0","10","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("227","10","0","11","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("228","10","0","12","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("229","10","1","1","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("230","10","1","2","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("231","10","1","3","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("232","10","1","4","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("233","10","1","5","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("234","10","1","6","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("235","10","1","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("236","10","1","8","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("237","10","1","9","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("238","10","1","10","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("239","10","1","11","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("240","10","1","12","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7","7");
INSERT INTO phpn_rooms_availabilities VALUES("241","11","0","1","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("242","11","0","2","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("243","11","0","3","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("244","11","0","4","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("245","11","0","5","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("246","11","0","6","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("247","11","0","7","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("248","11","0","8","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("249","11","0","9","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("250","11","0","10","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("251","11","0","11","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("252","11","0","12","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("253","11","1","1","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("254","11","1","2","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("255","11","1","3","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("256","11","1","4","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("257","11","1","5","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("258","11","1","6","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("259","11","1","7","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("260","11","1","8","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("261","11","1","9","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("262","11","1","10","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("263","11","1","11","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("264","11","1","12","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16");
INSERT INTO phpn_rooms_availabilities VALUES("265","12","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("266","12","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("267","12","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("268","12","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("269","12","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("270","12","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("271","12","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("272","12","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("273","12","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("274","12","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("275","12","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("276","12","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("277","12","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("278","12","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("279","12","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("280","12","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("281","12","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("282","12","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("283","12","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("284","12","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("285","12","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("286","12","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("287","12","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("288","12","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("289","13","0","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("290","13","0","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("291","13","0","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("292","13","0","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("293","13","0","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("294","13","0","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("295","13","0","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("296","13","0","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("297","13","0","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("298","13","0","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("299","13","0","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("300","13","0","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("301","13","1","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("302","13","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("303","13","1","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("304","13","1","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("305","13","1","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("306","13","1","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("307","13","1","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("308","13","1","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("309","13","1","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("310","13","1","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("311","13","1","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("312","13","1","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("313","14","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("314","14","0","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("315","14","0","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("316","14","0","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("317","14","0","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("318","14","0","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("319","14","0","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("320","14","0","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("321","14","0","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("322","14","0","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("323","14","0","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("324","14","0","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("325","14","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("326","14","1","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("327","14","1","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("328","14","1","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("329","14","1","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("330","14","1","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("331","14","1","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("332","14","1","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("333","14","1","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("334","14","1","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("335","14","1","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("336","14","1","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("337","15","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("338","15","0","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("339","15","0","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("340","15","0","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("341","15","0","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("342","15","0","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("343","15","0","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("344","15","0","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("345","15","0","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("346","15","0","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("347","15","0","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("348","15","0","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("349","15","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("350","15","1","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("351","15","1","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("352","15","1","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("353","15","1","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("354","15","1","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("355","15","1","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("356","15","1","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("357","15","1","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("358","15","1","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("359","15","1","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("360","15","1","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("576","24","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("575","24","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("574","24","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("573","24","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("572","24","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("571","24","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("570","24","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("569","24","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("568","24","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("567","24","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("566","24","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("565","24","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("564","24","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("563","24","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("562","24","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("561","24","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("560","24","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("559","24","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("558","24","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("557","24","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("556","24","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("555","24","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("554","24","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("553","24","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("385","17","0","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("386","17","0","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("387","17","0","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("388","17","0","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("389","17","0","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("390","17","0","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("391","17","0","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("392","17","0","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("393","17","0","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("394","17","0","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("395","17","0","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("396","17","0","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("397","17","1","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("398","17","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("399","17","1","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("400","17","1","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("401","17","1","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("402","17","1","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("403","17","1","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("404","17","1","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("405","17","1","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("406","17","1","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("407","17","1","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("408","17","1","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("409","18","0","1","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("410","18","0","2","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("411","18","0","3","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("412","18","0","4","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("413","18","0","5","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("414","18","0","6","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("415","18","0","7","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("416","18","0","8","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("417","18","0","9","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("418","18","0","10","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("419","18","0","11","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("420","18","0","12","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("421","18","1","1","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("422","18","1","2","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("423","18","1","3","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("424","18","1","4","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("425","18","1","5","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("426","18","1","6","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("427","18","1","7","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("428","18","1","8","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("429","18","1","9","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("430","18","1","10","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("431","18","1","11","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("432","18","1","12","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18");
INSERT INTO phpn_rooms_availabilities VALUES("433","19","0","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("434","19","0","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("435","19","0","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("436","19","0","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("437","19","0","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("438","19","0","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("439","19","0","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("440","19","0","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("441","19","0","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("442","19","0","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("443","19","0","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("444","19","0","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("445","19","1","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("446","19","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("447","19","1","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("448","19","1","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("449","19","1","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("450","19","1","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("451","19","1","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("452","19","1","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("453","19","1","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("454","19","1","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("455","19","1","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("456","19","1","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("457","20","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("458","20","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("459","20","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("460","20","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("461","20","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("462","20","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("463","20","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("464","20","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("465","20","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("466","20","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("467","20","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("468","20","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("469","20","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("470","20","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("471","20","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("472","20","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("473","20","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("474","20","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("475","20","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("476","20","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("477","20","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("478","20","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("479","20","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("480","20","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("481","21","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("482","21","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("483","21","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("484","21","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("485","21","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("486","21","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("487","21","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("488","21","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("489","21","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("490","21","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("491","21","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("492","21","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("493","21","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("494","21","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("495","21","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("496","21","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("497","21","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("498","21","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("499","21","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("500","21","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("501","21","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("502","21","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("503","21","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("504","21","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("505","22","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("506","22","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("507","22","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("508","22","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("509","22","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("510","22","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("511","22","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("512","22","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("513","22","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("514","22","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("515","22","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("516","22","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("517","22","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("518","22","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("519","22","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("520","22","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("521","22","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("522","22","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("523","22","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("524","22","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("525","22","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("526","22","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("527","22","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("528","22","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("529","23","0","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("530","23","0","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("531","23","0","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("532","23","0","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("533","23","0","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("534","23","0","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("535","23","0","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("536","23","0","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("537","23","0","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("538","23","0","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("539","23","0","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("540","23","0","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("541","23","1","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("542","23","1","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("543","23","1","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("544","23","1","4","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("545","23","1","5","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("546","23","1","6","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("547","23","1","7","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("548","23","1","8","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("549","23","1","9","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("550","23","1","10","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("551","23","1","11","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("552","23","1","12","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3");
INSERT INTO phpn_rooms_availabilities VALUES("577","25","0","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("578","25","0","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("579","25","0","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("580","25","0","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("581","25","0","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("582","25","0","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("583","25","0","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("584","25","0","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("585","25","0","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("586","25","0","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("587","25","0","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("588","25","0","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("589","25","1","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("590","25","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("591","25","1","3","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("592","25","1","4","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("593","25","1","5","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("594","25","1","6","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("595","25","1","7","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("596","25","1","8","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("597","25","1","9","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("598","25","1","10","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("599","25","1","11","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("600","25","1","12","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2");
INSERT INTO phpn_rooms_availabilities VALUES("601","26","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("602","26","0","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("603","26","0","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("604","26","0","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("605","26","0","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("606","26","0","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("607","26","0","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("608","26","0","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("609","26","0","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("610","26","0","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("611","26","0","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("612","26","0","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("613","26","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("614","26","1","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("615","26","1","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("616","26","1","4","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("617","26","1","5","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("618","26","1","6","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("619","26","1","7","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("620","26","1","8","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("621","26","1","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("622","26","1","10","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("623","26","1","11","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO phpn_rooms_availabilities VALUES("624","26","1","12","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");



DROP TABLE IF EXISTS phpn_rooms_description;

CREATE TABLE `phpn_rooms_description` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `room_id` int(10) NOT NULL DEFAULT '0',
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'en',
  `room_type` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `room_short_description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `room_long_description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms_description VALUES("1","1","en","Single","<p>Rooms measuring 15 m² equipped with all the details expected of a superior 4 star hotel. Services: Wake up call service, Customer service, Laundry service and express laundry, Concierge service, Pillow menu.</p>","<p><strong>Description:</strong><br />Rooms measuring 15 m² equipped with all the details expected of a superior 4 star hotel.</p>\n<p><strong>Services: </strong>\n<ul class=\"services\">\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("37","10","en","Standart Room - Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with one King sized bed and a TV</span></p>","<p>Include: one single bed, hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("4","2","en","Double","<p>Modern and functional rooms measuring approximately 20-25 m² equipped with all the details expected of the hotel. The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres ideal for sports teams.</p>","<p><strong>Description</strong>:<br />Modern and functional rooms measuring approximately 20-25 m² equipped with all the details expected of the hotel. <br /><br />The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 meters ideal for sports teams and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).<br /><br /><strong>Services</strong>:\n<ul class=\"services\">\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("36","8","ID","Executive Double Room","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of NY. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of NY. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>\n<p>As you would expect from Guoman, all our 50 Executive Double bedrooms also benefit from fast, complimentary BT Wi-fi and entertainment at your fingertips with a 32-inch LCD TV. You’ll appreciate extra space to unwind and recharge for the day ahead with beautifully designed bathrooms featuring walk-in power showers and mist-free mirrors. Welcome details that provide an oasis of calm in the centre of London.</p>");
INSERT INTO phpn_rooms_description VALUES("7","3","en","Superior","<p>Spacious rooms with exquisite decor, measuring approximately 25-30 m&sup2; and equipped with all the details expected of the hotel hotel. The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres.</p>","<p><strong>Description</strong>:\n<br />\nSpacious rooms with exquisite decor, measuring approximately 25-30 m² and equipped with all the details expected of the hotel hotel.\n<br /><br />\nThe rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres ideal for sports teams and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).\n<br /><br />\n<strong>Services</strong>:\n<ul class=\"services\">\n<li>24 hour room service</li>\n<li>Wake up call service</li>\n<li>Serviexpress customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul>\n</p>");
INSERT INTO phpn_rooms_description VALUES("34","9","ID","Single Bed","<p>TEST</p>","<p>TEST</p>");
INSERT INTO phpn_rooms_description VALUES("35","7","ID","Junior Suite","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.</p>","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.<br /> For your comfort and convenience, most also include a fully equipped European-style kitchen with appliances by Sub-Zero, and a sitting area with a twin-size sofa bed. All of our New York hotel guests enjoy full access to our health club and spa.</p>");
INSERT INTO phpn_rooms_description VALUES("10","4","en","Luxury","<p>Spacious rooms with exquisite decor measuring approximately 25-30 m&sup2; and equipped with all the details expected of a superior 4 star Hotel. The rooms have a king or queen size bed or two single beds, and views of the streets.</p>","<p><strong>Description</strong>:\n<br />\nSpacious rooms with exquisite decor measuring approximately 25-30 m² and equipped with all the details expected of a superior 4 star Hotel.\n<br /><br />\nThe rooms have a king or queen size bed or two single beds, and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).\n<br /><br />\n<strong>Services</strong>:\n<br />\n<ul class=\"services\">\n<li>Private reception located on the 6th floor</li>\n<li>Welcome courtesy replaced daily</li>\n<li>Buffet breakfast in private room between 7am and 11am</li>\n<li>Free Open Bar available on the Luxury Service floor, from 16:30 to 22:30</li>\n<li>24 hour room service</li>\n<li>10% discount on lunches and dinners at our Diábolo restaurant, after consultation and booking at the Royal Service reception and subject to availability</li>\n<li>Free 30 minute use per stay of computers in the Business Centre (hall), including printer and 25 photocopies</li>\n<li>Free 8 minute Ultra Violet Ray session per stay</li>\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n<li>Turn down service</li>\n</ul>\n* The Luxury Service is closed from Friday at 12:00 until Sunday at 15:00.\n</p>");
INSERT INTO phpn_rooms_description VALUES("32","5","ID","Standard Room","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest.</p>","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest, flat screen TV, mini-fridge, executive desk, wireless internet, all tastefully decorated with climate control at your fingertips. Exercise in our state of the art fitness facility, swim in our heated indoor swimming pool or relax in our sauna, all free of cost for our guests.</p>");
INSERT INTO phpn_rooms_description VALUES("33","6","ID","Double Room with Two Double Beds","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests.</p>","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests. The standard double room is ideal for a brief stay of up to a few days. It doesn\'t contain a kitchen, so guests can\'t cook their own meals.</p>");
INSERT INTO phpn_rooms_description VALUES("13","5","en","Standard Room","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest.</p>","<p>Relax in our standard room, complete with two queen size beds, customized furniture, gel-infused pillow top mattresses topped with luxurious duvets and duvet covers clean for every guest, flat screen TV, mini-fridge, executive desk, wireless internet, all tastefully decorated with climate control at your fingertips. Exercise in our state of the art fitness facility, swim in our heated indoor swimming pool or relax in our sauna, all free of cost for our guests.</p>");
INSERT INTO phpn_rooms_description VALUES("16","6","en","Double Room with Two Double Beds","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests.</p>","<p>A standard hotel room is an elongated hotel room designed for two people, though some standard doubles can accommodate up to four. The standard double hotel room is available at many full-service hotels, with the exclusion of facilities that house only suites or extended-stay guests. The standard double room is ideal for a brief stay of up to a few days. It doesn\'t contain a kitchen, so guests can\'t cook their own meals.</p>");
INSERT INTO phpn_rooms_description VALUES("31","4","ID","Luxury","<p>Spacious rooms with exquisite decor measuring approximately 25-30 m&sup2; and equipped with all the details expected of a superior 4 star Hotel. The rooms have a king or queen size bed or two single beds, and views of the streets.</p>","<p><strong>Description</strong>:\n<br />\nSpacious rooms with exquisite decor measuring approximately 25-30 m² and equipped with all the details expected of a superior 4 star Hotel.\n<br /><br />\nThe rooms have a king or queen size bed or two single beds, and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).\n<br /><br />\n<strong>Services</strong>:\n<br />\n<ul class=\"services\">\n<li>Private reception located on the 6th floor</li>\n<li>Welcome courtesy replaced daily</li>\n<li>Buffet breakfast in private room between 7am and 11am</li>\n<li>Free Open Bar available on the Luxury Service floor, from 16:30 to 22:30</li>\n<li>24 hour room service</li>\n<li>10% discount on lunches and dinners at our Diábolo restaurant, after consultation and booking at the Royal Service reception and subject to availability</li>\n<li>Free 30 minute use per stay of computers in the Business Centre (hall), including printer and 25 photocopies</li>\n<li>Free 8 minute Ultra Violet Ray session per stay</li>\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n<li>Turn down service</li>\n</ul>\n* The Luxury Service is closed from Friday at 12:00 until Sunday at 15:00.\n</p>");
INSERT INTO phpn_rooms_description VALUES("19","7","en","Junior Suite","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.</p>","<p>Our spacious Junior hotel Suites are 460 square feet (43 square meters) featuring floor-to-ceiling windows overlooking Manhattan from our New York City hotel suites. Junior Suites offer luxurious king beds, 55-inch flat-panel HDTV with Blu-ray player and iPod docking station, marble baths, and a large walk-in closet.<br /> For your comfort and convenience, most also include a fully equipped European-style kitchen with appliances by Sub-Zero, and a sitting area with a twin-size sofa bed. All of our New York hotel guests enjoy full access to our health club and spa.</p>");
INSERT INTO phpn_rooms_description VALUES("30","3","ID","Superior","<p>Spacious rooms with exquisite decor, measuring approximately 25-30 m&sup2; and equipped with all the details expected of the hotel hotel. The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres.</p>","<p><strong>Description</strong>:\n<br />\nSpacious rooms with exquisite decor, measuring approximately 25-30 m² and equipped with all the details expected of the hotel hotel.\n<br /><br />\nThe rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres ideal for sports teams and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).\n<br /><br />\n<strong>Services</strong>:\n<ul class=\"services\">\n<li>24 hour room service</li>\n<li>Wake up call service</li>\n<li>Serviexpress customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul>\n</p>");
INSERT INTO phpn_rooms_description VALUES("22","8","en","Executive Double Room","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of NY. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>","<p>Stunning Executive Double bedrooms provide the perfect setting in the heart of NY. Exclusive features you won’t find in Double Cumberland Guest rooms include luxurious bathrobes and slippers, complimentary mineral water and full access to the Guoman Club Lounge with complimentary newspapers and refreshments available all day.</p>\n<p>As you would expect from Guoman, all our 50 Executive Double bedrooms also benefit from fast, complimentary BT Wi-fi and entertainment at your fingertips with a 32-inch LCD TV. You’ll appreciate extra space to unwind and recharge for the day ahead with beautifully designed bathrooms featuring walk-in power showers and mist-free mirrors. Welcome details that provide an oasis of calm in the centre of London.</p>");
INSERT INTO phpn_rooms_description VALUES("38","10","ID","Standart Room - Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with one King sized bed and a TV.</span></p>","<p>Include: one single bed, hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("39","11","en","Standard Room - Double Beds","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room with two single sized beds and a TV.</span></p>","<p>Include: one single bed, hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("40","11","ID","Standard Room - Double Beds","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room with two single sized beds and a TV.</span></p>","<p>Include: two single bed, hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("41","12","en","Deluxe Room - Two Single Beds w/ sofabed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with two single beds, a sofabed, and a TV. These units are loca...</span></p>","<p>Include: 2 pcs of bed, 1pcs of sofa bed,  hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("42","12","ID","Deluxe Room - Two Single Beds w/ sofabed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with two single beds, a sofabed, and a TV. These units are loca...</span></p>","<p>Include: 2 pcs of bed, 1pcs of sofa bed,  hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("29","2","ID","Double","<p>Modern and functional rooms measuring approximately 20-25 m² equipped with all the details expected of the hotel. The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 metres ideal for sports teams.</p>","<p><strong>Description</strong>:<br />Modern and functional rooms measuring approximately 20-25 m² equipped with all the details expected of the hotel. <br /><br />The rooms have a king or queen size bed or two single beds, in addition to beds measuring 1 by 2.2 meters ideal for sports teams and views of the streets of the quiet interior patios (request availability upon arrival at the hotel).<br /><br /><strong>Services</strong>:\n<ul class=\"services\">\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("25","9","en","Single Bed","<p>TEST</p>","<p>TEST</p>");
INSERT INTO phpn_rooms_description VALUES("28","1","ID","Single","<p>Rooms measuring 15 m² equipped with all the details expected of a superior 4 star hotel. Services: Wake up call service, Customer service, Laundry service and express laundry, Concierge service, Pillow menu.</p>","<p><strong>Description:</strong><br />Rooms measuring 15 m² equipped with all the details expected of a superior 4 star hotel.</p>\n<p><strong>Services: </strong>\n<ul class=\"services\">\n<li>Wake up call service</li>\n<li>Customer service</li>\n<li>Laundry service and express laundry</li>\n<li>Concierge service</li>\n<li>Pillow menu</li>\n</ul></p>");
INSERT INTO phpn_rooms_description VALUES("43","13","en","Deluxe Room - Four Single Beds","<table class=\"mgrid_table\" style=\"font-size: 12px; color: #222222; width: 382px; font-family: arial, verdana, sans-serif;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\n<tbody>\n<tr class=\"highlight_light\">\n<td align=\"left\"><label class=\"mgrid_label\" title=\"A unit with four single beds and a TV. These units are located on the top floor. A door connects this room to another room.\">A unit with four single beds and a TV. These units are located on the ...</label></td>\n</tr>\n</tbody>\n</table>","<p>Include: 4 pcs of bed, hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("44","13","ID","Deluxe Room - Four Single Beds","<table class=\"mgrid_table\" style=\"font-size: 12px; color: #222222; width: 382px; font-family: arial, verdana, sans-serif;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\n<tbody>\n<tr class=\"highlight_light\">\n<td align=\"left\"><label class=\"mgrid_label\" title=\"A unit with four single beds and a TV. These units are located on the top floor. A door connects this room to another room.\">A unit with four single beds and a TV. These units are located on the ...</label></td>\n</tr>\n</tbody>\n</table>","<p style=\"text-align: left;\">Include: 4 pcs of bed, hot water, breakfast, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("45","14","en","Family Room 1","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with two single sized bed and a sofabed, a living room, a kitch...</span></p>","<p>Include: 2 pcs of bed, 1 pcs of sofa bed, hot water, breakfast, kitchen, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("46","14","ID","Family Room 1","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with two single sized bed and a sofabed, a living room, a kitch...</span></p>","<p>Include: 2 pcs of bed, 1 pcs of sofa bed, hot water, breakfast, kitchen, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("47","15","en","Family Room 2","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with four Single Beds, a living room with two sofabeds, a livin...</span></p>","<p>Include: 2 pcs of bed, hot water, breakfast, kitchen, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("48","15","ID","Family Room 2","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A unit with four Single Beds, a living room with two sofabeds, a livin...</span></p>","<p>Include: 2 pcs of bed, hot water, breakfast, kitchen, TV, bathroom, 2 pcs of towel, 2 pcs of soap, 2 pcs of showercap, 2 pcs of glass.</p>");
INSERT INTO phpn_rooms_description VALUES("51","17","en","Superior Room - King Size Bed	","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A superior room for two people with one king sized bed and TV</span></p>","<p>Include: 1 pcs of bed, 2 pcs of glass, nakash table, rack for shoes, 1 set of hanging drawer and mirror, bath tube, wastafel, closet, 1 pcs for mat, 2 pcs of towel, hot water, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("52","17","ID","Superior Room - King Size Bed	","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A superior room for two people with one king sized bed and TV</span></p>","<p>Include: 1 pcs of bed, 2 pcs of glass, nakash table, rack for shoes, 1 set of hanging drawer and mirror, bath tube, wastafel, closet, 1 pcs for mat, 2 pcs of towel, hot water, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("53","18","en","Superior Room - Two Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for two people with two single bed</span></p>","<p>Include: 2 pcs of bed, 2 pcs of glass, nakash table, rack for shoes, 1 set of hanging drawer and mirror, bath tube, wastafel, closet, 1 pcs for mat, 2 pcs of towel, hot water, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("54","18","ID","Superior Room - Two Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for two people with two single bed</span></p>","");
INSERT INTO phpn_rooms_description VALUES("55","19","en","Economy Room - Two Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for two people with two single sized bed. The room is located b...</span></p>","<p>Include: 2 pcs of twin bed, bathroom, hot water, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("56","19","ID","Economy Room - Two Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for two people with two single sized bed. The room is located b...</span></p>","<p>Include: 2 pcs of twin bed, bathroom, hot water, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("57","20","en","Standard Fans Room - Two Single ...","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for 2 people with two single sized beds and fans.</span></p>","");
INSERT INTO phpn_rooms_description VALUES("58","20","ID","Standard Fans Room - Two Single ...","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for 2 people with two single sized beds and fans.</span></p>","");
INSERT INTO phpn_rooms_description VALUES("59","21","en","Standard Fans Room - King Sized ...","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with one king sized bed and fans.</span></p>","");
INSERT INTO phpn_rooms_description VALUES("60","21","ID","Standard Fans Room - King Sized ...","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with one king sized bed and fans.</span></p>","");
INSERT INTO phpn_rooms_description VALUES("61","22","en","Standard AC Room - Two Single Beds","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for 2 people with two single beds and an AC.</span></p>","");
INSERT INTO phpn_rooms_description VALUES("62","22","ID","Standard AC Room - Two Single Beds","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for 2 people with two single beds and an AC.</span></p>","");
INSERT INTO phpn_rooms_description VALUES("63","23","en","Standard AC Room - King Sized Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with a king sized bed and an AC</span></p>","");
INSERT INTO phpn_rooms_description VALUES("64","23","ID","Standard AC Room - King Sized Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A standard room for two people with a king sized bed and an AC</span></p>","");
INSERT INTO phpn_rooms_description VALUES("65","24","en","Economy Room Triple Bed","<p>3 pcs of bed and count by person, high season only</p>","<p>include: 3 pcs of bed, bathroom, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("66","24","ID","Economy Room Triple Bed","<p>3 pcs of bed and count by person, high season only</p>","<p>include: 3 pcs of bed, bathroom, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("67","25","en","Economy Room Double Bed","<p>2 pcs of bed, count by person, high season only</p>","<p>Include: 2 pcs of bed,  bathroom, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("68","25","ID","Economy Room Double Bed","<p>2 pcs of bed, count by person, high season only</p>","<p>Include: 2 pcs of bed,  bathroom, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("69","26","en","Economy Room Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for one people with one single sized bed</span></p>","<p>Include: 2 pcs of twin bed, bathroom, hot water, breakfast.</p>");
INSERT INTO phpn_rooms_description VALUES("70","26","ID","Economy Room Single Bed","<p><span style=\"color: #222222; font-family: arial, verdana, sans-serif;\">A room for one people with one single sized bed</span></p>","<p>Include: 2 pcs of twin bed, bathroom, hot water, breakfast.</p>");



DROP TABLE IF EXISTS phpn_rooms_prices;

CREATE TABLE `phpn_rooms_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date DEFAULT '0000-00-00',
  `date_to` date DEFAULT '0000-00-00',
  `adults` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `children` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extra_bed_charge` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `mon` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tue` decimal(10,2) NOT NULL DEFAULT '0.00',
  `wed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `thu` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fri` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sat` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sun` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_rooms_prices VALUES("1","1","0000-00-00","0000-00-00","1","0","22.00","55.00","55.00","55.00","55.00","55.00","55.00","55.00","1");
INSERT INTO phpn_rooms_prices VALUES("2","2","0000-00-00","0000-00-00","2","1","30.00","80.00","80.00","80.00","80.00","80.00","80.00","80.00","1");
INSERT INTO phpn_rooms_prices VALUES("3","3","0000-00-00","0000-00-00","3","1","50.00","140.00","140.00","140.00","140.00","140.00","140.00","140.00","1");
INSERT INTO phpn_rooms_prices VALUES("4","4","0000-00-00","0000-00-00","4","2","90.00","190.00","190.00","190.00","190.00","190.00","190.00","190.00","1");
INSERT INTO phpn_rooms_prices VALUES("5","5","0000-00-00","0000-00-00","1","0","22.00","55.00","55.00","55.00","55.00","55.00","55.00","55.00","1");
INSERT INTO phpn_rooms_prices VALUES("6","6","0000-00-00","0000-00-00","2","1","30.00","80.00","80.00","80.00","80.00","80.00","80.00","80.00","1");
INSERT INTO phpn_rooms_prices VALUES("7","7","0000-00-00","0000-00-00","3","1","50.00","140.00","140.00","140.00","140.00","140.00","140.00","140.00","1");
INSERT INTO phpn_rooms_prices VALUES("8","8","0000-00-00","0000-00-00","4","2","90.00","190.00","190.00","190.00","190.00","190.00","190.00","190.00","1");
INSERT INTO phpn_rooms_prices VALUES("9","9","0000-00-00","0000-00-00","2","2","0.00","0.00","0.00","0.00","0.00","0.00","0.00","0.00","1");
INSERT INTO phpn_rooms_prices VALUES("10","10","0000-00-00","0000-00-00","2","0","125000.00","350000.00","350000.00","350000.00","350000.00","350000.00","375000.00","375000.00","1");
INSERT INTO phpn_rooms_prices VALUES("11","11","0000-00-00","0000-00-00","2","0","125000.00","350000.00","350000.00","350000.00","350000.00","350000.00","375000.00","375000.00","1");
INSERT INTO phpn_rooms_prices VALUES("12","12","0000-00-00","0000-00-00","2","0","125000.00","400000.00","400000.00","400000.00","400000.00","400000.00","435000.00","435000.00","1");
INSERT INTO phpn_rooms_prices VALUES("13","13","0000-00-00","0000-00-00","4","0","125000.00","400000.00","400000.00","400000.00","400000.00","400000.00","435000.00","435000.00","1");
INSERT INTO phpn_rooms_prices VALUES("14","14","0000-00-00","0000-00-00","2","0","125000.00","485000.00","485000.00","485000.00","485000.00","485000.00","520000.00","520000.00","1");
INSERT INTO phpn_rooms_prices VALUES("15","15","0000-00-00","0000-00-00","4","0","125000.00","750000.00","750000.00","750000.00","750000.00","750000.00","860000.00","860000.00","1");
INSERT INTO phpn_rooms_prices VALUES("24","24","0000-00-00","0000-00-00","1","0","0.00","60000.00","60000.00","60000.00","60000.00","60000.00","70000.00","70000.00","1");
INSERT INTO phpn_rooms_prices VALUES("17","17","0000-00-00","0000-00-00","2","0","125000.00","350000.00","350000.00","350000.00","350000.00","350000.00","350000.00","350000.00","1");
INSERT INTO phpn_rooms_prices VALUES("18","18","0000-00-00","0000-00-00","2","0","125000.00","350000.00","350000.00","350000.00","350000.00","350000.00","350000.00","350000.00","1");
INSERT INTO phpn_rooms_prices VALUES("19","19","0000-00-00","0000-00-00","2","0","120000.00","60000.00","60000.00","60000.00","60000.00","60000.00","70000.00","70000.00","1");
INSERT INTO phpn_rooms_prices VALUES("20","20","0000-00-00","0000-00-00","2","0","50000.00","150000.00","150000.00","150000.00","150000.00","150000.00","150000.00","150000.00","1");
INSERT INTO phpn_rooms_prices VALUES("21","21","0000-00-00","0000-00-00","2","0","50000.00","150000.00","150000.00","150000.00","150000.00","150000.00","150000.00","150000.00","1");
INSERT INTO phpn_rooms_prices VALUES("22","22","0000-00-00","0000-00-00","2","0","50000.00","220000.00","220000.00","220000.00","220000.00","220000.00","220000.00","220000.00","1");
INSERT INTO phpn_rooms_prices VALUES("23","23","0000-00-00","0000-00-00","2","0","50000.00","220000.00","220000.00","220000.00","220000.00","220000.00","220000.00","220000.00","1");
INSERT INTO phpn_rooms_prices VALUES("25","25","0000-00-00","0000-00-00","1","0","0.00","60000.00","60000.00","60000.00","60000.00","60000.00","70000.00","70000.00","1");
INSERT INTO phpn_rooms_prices VALUES("27","26","0000-00-00","0000-00-00","1","0","0.00","60000.00","60000.00","60000.00","60000.00","60000.00","70000.00","70000.00","1");



DROP TABLE IF EXISTS phpn_search_wordlist;

CREATE TABLE `phpn_search_wordlist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `word_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `word_count` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `word_text` (`word_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS phpn_settings;

CREATE TABLE `phpn_settings` (
  `id` smallint(6) NOT NULL,
  `template` varchar(32) CHARACTER SET latin1 NOT NULL,
  `ssl_mode` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - no, 1 - entire site, 2 - admin, 3 - customer & payment modules',
  `seo_urls` tinyint(1) NOT NULL DEFAULT '1',
  `date_format` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dd/mm/yyyy',
  `time_zone` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `price_format` enum('european','american') CHARACTER SET latin1 NOT NULL,
  `week_start_day` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `admin_email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `mailer` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT 'php_mail_standard',
  `mailer_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mailer_wysiwyg_type` enum('none','tinymce') CHARACTER SET latin1 NOT NULL DEFAULT 'none',
  `smtp_secure` enum('ssl','tls','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ssl',
  `smtp_host` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_port` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_password` varchar(50) CHARACTER SET latin1 NOT NULL,
  `wysiwyg_type` enum('none','tinymce') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tinymce',
  `rss_feed` tinyint(1) NOT NULL DEFAULT '1',
  `rss_feed_type` enum('rss1','rss2','atom') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'rss1',
  `rss_last_ids` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_offline` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `caching_allowed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cache_lifetime` tinyint(3) unsigned NOT NULL DEFAULT '5' COMMENT 'in minutes',
  `offline_message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_rank` varchar(2) CHARACTER SET latin1 NOT NULL,
  `alexa_rank` varchar(12) CHARACTER SET latin1 NOT NULL,
  `cron_type` enum('batch','non-batch','stop') CHARACTER SET latin1 NOT NULL DEFAULT 'non-batch',
  `cron_run_last_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cron_run_period` enum('minute','hour') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'minute',
  `cron_run_period_value` smallint(6) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_settings VALUES("0","default","0","0","dd/mm/yyyy","7","european","2","promotion@pandu-hotel.com","php","php_mail_standard","tinymce","ssl","","","","","tinymce","1","rss2","1","0","0","5","Our website is currently offline for maintenance. Please visit us later.","-1","0","stop","2013-11-21 16:21:27","hour","24");



DROP TABLE IF EXISTS phpn_site_description;

CREATE TABLE `phpn_site_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  `header_text` text COLLATE utf8_unicode_ci NOT NULL,
  `slogan_text` text COLLATE utf8_unicode_ci NOT NULL,
  `footer_text` text COLLATE utf8_unicode_ci NOT NULL,
  `tag_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_description` text COLLATE utf8_unicode_ci NOT NULL,
  `tag_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_site_description VALUES("1","en","Pandu Hotel","Parapat, Tuktuk, Lombok","PanduHotel © <a class=\"footer_link\" href=\"http://www.panduhotel.com\">Pandu Hotel</a>","Pandu Hotel","Pandu Hotel Indonesia","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk");
INSERT INTO phpn_site_description VALUES("4","ID","Pandu Hotel","Parapat, Tuktuk, Lombok","PanduHotel © <a class=\"footer_link\" href=\"http://www.panduhotel.com\">Pandu Hotel</a>","Pandu Hotel","Pandu Hotel Indonesia","Eco Supervolcano Eruption Mount Toba Lake Indonesia, Toba Lake Danau Toba Pandu Hotel Tour Package, legenda sejarah hotel pandu danau toba, paket harga kamar hotel Pandu Danau Toba Pantai, Hotel Pandu Toba Lake Pandu Hotel, Panda Pandu Parapat Pandu Tuktuk Pandu Homestay, Hotel Pandu Panda Niagara Tuktuk");



DROP TABLE IF EXISTS phpn_states;

CREATE TABLE `phpn_states` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `country_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `abbrv` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `priority_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_states VALUES("1","224","AL","Alabama","1","1");
INSERT INTO phpn_states VALUES("2","224","AK","Alaska","1","2");
INSERT INTO phpn_states VALUES("3","224","AZ","Arizona","1","3");
INSERT INTO phpn_states VALUES("4","224","AR","Arkansas","1","4");
INSERT INTO phpn_states VALUES("5","224","CA","California","1","5");
INSERT INTO phpn_states VALUES("6","224","CO","Colorado","1","6");
INSERT INTO phpn_states VALUES("7","224","CT","Connecticut","1","7");
INSERT INTO phpn_states VALUES("8","224","DE","Delaware","1","8");
INSERT INTO phpn_states VALUES("9","224","DC","District of Columbia","1","9");
INSERT INTO phpn_states VALUES("10","224","FL","Florida","1","10");
INSERT INTO phpn_states VALUES("11","224","GA","Georgia","1","11");
INSERT INTO phpn_states VALUES("12","224","HI","Hawaii","1","12");
INSERT INTO phpn_states VALUES("13","224","ID","Idaho","1","13");
INSERT INTO phpn_states VALUES("14","224","IL","Illinois","1","14");
INSERT INTO phpn_states VALUES("15","224","IN","Indiana","1","15");
INSERT INTO phpn_states VALUES("16","224","IA","Iowa","1","16");
INSERT INTO phpn_states VALUES("17","224","KS","Kansas","1","17");
INSERT INTO phpn_states VALUES("18","224","KY","Kentucky","1","18");
INSERT INTO phpn_states VALUES("19","224","LA","Louisiana","1","19");
INSERT INTO phpn_states VALUES("20","224","ME","Maine","1","20");
INSERT INTO phpn_states VALUES("21","224","MD","Maryland","1","21");
INSERT INTO phpn_states VALUES("22","224","MA","Massachusetts","1","22");
INSERT INTO phpn_states VALUES("23","224","MI","Michigan","1","22");
INSERT INTO phpn_states VALUES("24","224","MN","Minnesota","1","23");
INSERT INTO phpn_states VALUES("25","224","MS","Mississippi","1","25");
INSERT INTO phpn_states VALUES("26","224","MT","Montana","1","27");
INSERT INTO phpn_states VALUES("27","224","MO","Missouri","1","26");
INSERT INTO phpn_states VALUES("28","224","NE","Nebraska","1","28");
INSERT INTO phpn_states VALUES("29","224","NV","Nevada","1","29");
INSERT INTO phpn_states VALUES("30","224","NH","New Hampshire","1","30");
INSERT INTO phpn_states VALUES("31","224","NJ","New Jersey","1","31");
INSERT INTO phpn_states VALUES("32","224","NM","New Mexico","1","32");
INSERT INTO phpn_states VALUES("33","224","NY","New York","1","33");
INSERT INTO phpn_states VALUES("34","224","NC","North Carolina","1","34");
INSERT INTO phpn_states VALUES("35","224","ND","North Dakota","1","35");
INSERT INTO phpn_states VALUES("36","224","OH","Ohio","1","36");
INSERT INTO phpn_states VALUES("37","224","OK","Oklahoma","1","37");
INSERT INTO phpn_states VALUES("38","224","OR","Oregon","1","38");
INSERT INTO phpn_states VALUES("39","224","PA","Pennsylvania","1","39");
INSERT INTO phpn_states VALUES("40","224","RI","Rhode Island","1","40");
INSERT INTO phpn_states VALUES("41","224","SC","South Carolina","1","41");
INSERT INTO phpn_states VALUES("42","224","SD","South Dakota","1","42");
INSERT INTO phpn_states VALUES("43","224","TN","Tennessee","1","43");
INSERT INTO phpn_states VALUES("44","224","TX","Texas","1","44");
INSERT INTO phpn_states VALUES("45","224","UT","Utah","1","45");
INSERT INTO phpn_states VALUES("46","224","VT","Vermont","1","46");
INSERT INTO phpn_states VALUES("47","224","VA","Virginia","1","47");
INSERT INTO phpn_states VALUES("48","224","WA","Washington","1","48");
INSERT INTO phpn_states VALUES("49","224","WV","West Virginia","1","49");
INSERT INTO phpn_states VALUES("50","224","WI","Wisconsin","1","50");
INSERT INTO phpn_states VALUES("51","224","WY","Wyoming","1","51");
INSERT INTO phpn_states VALUES("52","13","NSW","New South Wales","1","1");
INSERT INTO phpn_states VALUES("53","13","QLD","Queensland","1","2");
INSERT INTO phpn_states VALUES("54","13","SA","South Australia","1","3");
INSERT INTO phpn_states VALUES("55","13","TAS","Tasmania","1","4");
INSERT INTO phpn_states VALUES("56","13","VIC","Victoria","1","5");
INSERT INTO phpn_states VALUES("57","13","WA","Western Australia","1","6");
INSERT INTO phpn_states VALUES("58","39","AB","Alberta","1","1");
INSERT INTO phpn_states VALUES("59","39","BC","British Columbia","1","2");
INSERT INTO phpn_states VALUES("60","39","ON","Ontario","1","6");
INSERT INTO phpn_states VALUES("61","39","QC","Quebec","1","8");
INSERT INTO phpn_states VALUES("62","39","NS","Nova Scotia","1","5");
INSERT INTO phpn_states VALUES("63","39","NB","New Brunswick","1","4");
INSERT INTO phpn_states VALUES("64","39","MB","Manitoba","1","3");
INSERT INTO phpn_states VALUES("65","39","PE","Prince Edward Island","1","7");
INSERT INTO phpn_states VALUES("66","39","SK","Saskatchewan","1","9");
INSERT INTO phpn_states VALUES("67","39","NL","Newfoundland and Labrador","1","10");



DROP TABLE IF EXISTS phpn_testimonials;

CREATE TABLE `phpn_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author_country` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author_email` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `testimonial_text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `priority_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO phpn_testimonials VALUES("1","Roberto","IT","Rome","roberto@email.com","Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.","0","0");
INSERT INTO phpn_testimonials VALUES("2","Hantz","DE","Munich","hantz@email.com","Typi non habent claritatem insitam est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.","0","1");



DROP TABLE IF EXISTS phpn_vocabulary;

CREATE TABLE `phpn_vocabulary` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `language_id` varchar(3) CHARACTER SET latin1 NOT NULL,
  `key_value` varchar(50) CHARACTER SET latin1 NOT NULL,
  `key_text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voc_item` (`language_id`,`key_value`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6332 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_vocabulary VALUES("2","en","_2CO_NOTICE","2CheckOut.com Inc. (Ohio, USA) is an authorized retailer for goods and services.");
INSERT INTO phpn_vocabulary VALUES("5","en","_2CO_ORDER","2CO Order");
INSERT INTO phpn_vocabulary VALUES("5922","ID","_WELCOME_CUSTOMER_TEXT","<p>Hello <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>        \n<p>Welcome to Customer Account Panel, that allows you to view account status, manage your account settings and bookings.</p>\n<p>\n   _TODAY_<br />\n   _LAST_LOGIN_\n</p>				\n<p> <b>&#8226;</b> To view this account summary just click on a <a href=\'index.php?customer=home\'>Dashboard</a> link.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Edit My Account</a> menu allows you to change your personal info and account data.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>My Bookings</a> contains information about your orders.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("8","en","_ABBREVIATION","Abbreviation");
INSERT INTO phpn_vocabulary VALUES("11","en","_ABOUT_US","About Us");
INSERT INTO phpn_vocabulary VALUES("5921","ID","_WEEK_START_DAY","Week Start Day");
INSERT INTO phpn_vocabulary VALUES("14","en","_ACCESS","Access");
INSERT INTO phpn_vocabulary VALUES("17","en","_ACCESSIBLE_BY","Accessible By");
INSERT INTO phpn_vocabulary VALUES("5920","ID","_WEDNESDAY","Wednesday");
INSERT INTO phpn_vocabulary VALUES("20","en","_ACCOUNTS","Accounts");
INSERT INTO phpn_vocabulary VALUES("5919","ID","_WED","Wed");
INSERT INTO phpn_vocabulary VALUES("23","en","_ACCOUNTS_MANAGEMENT","Accounts");
INSERT INTO phpn_vocabulary VALUES("5917","ID","_WE","We");
INSERT INTO phpn_vocabulary VALUES("5918","ID","_WEB_SITE","Web Site");
INSERT INTO phpn_vocabulary VALUES("26","en","_ACCOUNT_ALREADY_RESET","Your account is already reset! Please check your email inbox for more information.");
INSERT INTO phpn_vocabulary VALUES("5914","ID","_VOC_NOT_FOUND","No keys found");
INSERT INTO phpn_vocabulary VALUES("5915","ID","_VOC_UPDATED","Vocabulary has been successfully updated. Click <a href=index.php>here</a> to refresh the site.");
INSERT INTO phpn_vocabulary VALUES("5916","ID","_VOTE_NOT_REGISTERED","Your vote has not been registered! You must be logged in before you can vote.");
INSERT INTO phpn_vocabulary VALUES("29","en","_ACCOUNT_CREATED_CONF_BY_ADMIN_MSG","Your account has been successfully created! In a few minutes you should receive an email, containing the details of your account. <br><br> After approval your registration by administrator, you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("5912","ID","_VOC_KEY_UPDATED","Vocabulary key has been successfully updated.");
INSERT INTO phpn_vocabulary VALUES("5913","ID","_VOC_KEY_VALUE_EMPTY","Key value cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("32","en","_ACCOUNT_CREATED_CONF_BY_EMAIL_MSG","Your account has been successfully created! In a few minutes you should receive an email, containing the details of your registration. <br><br> Complete this registration, using the confirmation code that was sent to the provided email address, and you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("5907","ID","_VISITOR","Visitor");
INSERT INTO phpn_vocabulary VALUES("5908","ID","_VISITORS_RATING","Visitors Rating");
INSERT INTO phpn_vocabulary VALUES("5909","ID","_VISUAL_SETTINGS","Visual Settings");
INSERT INTO phpn_vocabulary VALUES("5910","ID","_VOCABULARY","Vocabulary");
INSERT INTO phpn_vocabulary VALUES("5911","ID","_VOC_KEYS_UPDATED","Operation has been successfully completed. Updated: _KEYS_ keys. Click <a href=\'index.php?admin=vocabulary&filter_by=A\'>here</a> to refresh the site.");
INSERT INTO phpn_vocabulary VALUES("35","en","_ACCOUNT_CREATED_CONF_MSG","Your account has been successfully created. <b>You will receive now an email</b>, containing the details of your account (it may take a few minutes).<br><br>After approval by an administrator, you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("5901","ID","_VAT","VAT");
INSERT INTO phpn_vocabulary VALUES("5902","ID","_VAT_PERCENT","VAT Percent");
INSERT INTO phpn_vocabulary VALUES("5903","ID","_VERSION","Version");
INSERT INTO phpn_vocabulary VALUES("5904","ID","_VIDEO","Video");
INSERT INTO phpn_vocabulary VALUES("5905","ID","_VIEW_ALL","View All");
INSERT INTO phpn_vocabulary VALUES("5906","ID","_VIEW_WORD","View");
INSERT INTO phpn_vocabulary VALUES("38","en","_ACCOUNT_CREATED_MSG","Your account has been successfully created. <b>You will receive now a confirmation email</b>, containing the details of your account (it may take a few minutes). <br /><br />After completing the confirmation you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("5899","ID","_USE_THIS_PASSWORD","Use this password");
INSERT INTO phpn_vocabulary VALUES("5900","ID","_VALUE","Value");
INSERT INTO phpn_vocabulary VALUES("41","en","_ACCOUNT_CREATED_NON_CONFIRM_LINK","Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("5896","ID","_USER_EMAIL_EXISTS_ALERT","User with such email already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("5897","ID","_USER_EXISTS_ALERT","User with such username already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("5898","ID","_USER_NAME","User name");
INSERT INTO phpn_vocabulary VALUES("44","en","_ACCOUNT_CREATED_NON_CONFIRM_MSG","Your account has been successfully created! For your convenience in a few minutes you will receive an email, containing the details of your registration (no confirmation required). <br><br>You may log into your account now.");
INSERT INTO phpn_vocabulary VALUES("5894","ID","_USERNAME_LENGTH_ALERT","The length of username cannot be less than 4 characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5895","ID","_USERS","Users");
INSERT INTO phpn_vocabulary VALUES("47","en","_ACCOUNT_CREATE_MSG","This registration process requires confirmation via email! <br />Please fill out the form below with correct information.");
INSERT INTO phpn_vocabulary VALUES("50","en","_ACCOUNT_DETAILS","Account Details");
INSERT INTO phpn_vocabulary VALUES("5891","ID","_USERNAME","Username");
INSERT INTO phpn_vocabulary VALUES("5892","ID","_USERNAME_AND_PASSWORD","Username & Password");
INSERT INTO phpn_vocabulary VALUES("5893","ID","_USERNAME_EMPTY_ALERT","Username cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("53","en","_ACCOUNT_SUCCESSFULLY_RESET","You have successfully reset your account and username with temporary password have been sent to your email.");
INSERT INTO phpn_vocabulary VALUES("5890","ID","_USED_ON","Used On");
INSERT INTO phpn_vocabulary VALUES("56","en","_ACCOUNT_TYPE","Account type");
INSERT INTO phpn_vocabulary VALUES("5889","ID","_URL","URL");
INSERT INTO phpn_vocabulary VALUES("59","en","_ACCOUNT_WAS_CREATED","Your account has been created");
INSERT INTO phpn_vocabulary VALUES("5887","ID","_UPLOAD_AND_PROCCESS","Upload and Process");
INSERT INTO phpn_vocabulary VALUES("5888","ID","_UPLOAD_FROM_FILE","Upload from File");
INSERT INTO phpn_vocabulary VALUES("62","en","_ACCOUNT_WAS_DELETED","Your account has been successfully removed! In seconds, you will be automatically redirected to the homepage.");
INSERT INTO phpn_vocabulary VALUES("5886","ID","_UPLOAD","Upload");
INSERT INTO phpn_vocabulary VALUES("65","en","_ACCOUNT_WAS_UPDATED","Your account has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("5885","ID","_UPDATING_OPERATION_COMPLETED","Updating operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("68","en","_ACCOUT_CREATED_CONF_LINK","Already confirmed your registration? Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("5884","ID","_UPDATING_ACCOUNT_ERROR","An error occurred while updating your account! Please try again later or send information about this error to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("71","en","_ACCOUT_CREATED_CONF_MSG","Already confirmed your registration? Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("74","en","_ACTIONS","Action");
INSERT INTO phpn_vocabulary VALUES("5883","ID","_UPDATING_ACCOUNT","Updating Account");
INSERT INTO phpn_vocabulary VALUES("77","en","_ACTIONS_WORD","Action");
INSERT INTO phpn_vocabulary VALUES("5882","ID","_UP","Up");
INSERT INTO phpn_vocabulary VALUES("80","en","_ACTION_REQUIRED","ACTION REQUIRED");
INSERT INTO phpn_vocabulary VALUES("5879","ID","_UNIT_PRICE","Unit Price");
INSERT INTO phpn_vocabulary VALUES("5880","ID","_UNKNOWN","Unknown");
INSERT INTO phpn_vocabulary VALUES("5881","ID","_UNSUBSCRIBE","Unsubscribe");
INSERT INTO phpn_vocabulary VALUES("83","en","_ACTIVATION_EMAIL_ALREADY_SENT","The activation email has been already sent to your email. Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5876","ID","_UNDEFINED","undefined");
INSERT INTO phpn_vocabulary VALUES("5877","ID","_UNINSTALL","Uninstall");
INSERT INTO phpn_vocabulary VALUES("5878","ID","_UNITS","Units");
INSERT INTO phpn_vocabulary VALUES("86","en","_ACTIVATION_EMAIL_WAS_SENT","An email has been sent to _EMAIL_ with an activation key. Please check your mail to complete registration.");
INSERT INTO phpn_vocabulary VALUES("5875","ID","_UNCATEGORIZED","Uncategorized");
INSERT INTO phpn_vocabulary VALUES("89","en","_ACTIVE","Active");
INSERT INTO phpn_vocabulary VALUES("92","en","_ADD","Add");
INSERT INTO phpn_vocabulary VALUES("5873","ID","_TYPE","Type");
INSERT INTO phpn_vocabulary VALUES("5874","ID","_TYPE_CHARS","Type the characters you see in the picture");
INSERT INTO phpn_vocabulary VALUES("95","en","_ADDING_OPERATION_COMPLETED","The adding operation completed successfully!");
INSERT INTO phpn_vocabulary VALUES("5872","ID","_TUESDAY","Tuesday");
INSERT INTO phpn_vocabulary VALUES("98","en","_ADDITIONAL_INFO","Additional Info");
INSERT INTO phpn_vocabulary VALUES("5871","ID","_TUE","Tue");
INSERT INTO phpn_vocabulary VALUES("101","en","_ADDITIONAL_MODULES","Additional Modules");
INSERT INTO phpn_vocabulary VALUES("5870","ID","_TU","Tu");
INSERT INTO phpn_vocabulary VALUES("104","en","_ADDITIONAL_PAYMENT","Additional Payment");
INSERT INTO phpn_vocabulary VALUES("5868","ID","_TRY_LATER","An error occurred while executing. Please try again later!");
INSERT INTO phpn_vocabulary VALUES("5869","ID","_TRY_SYSTEM_SUGGESTION","Try out system suggestion");
INSERT INTO phpn_vocabulary VALUES("107","en","_ADDITIONAL_PAYMENT_TOOLTIP","To apply an additional payment or admin discount enter into this field an appropriate value (positive or negative).");
INSERT INTO phpn_vocabulary VALUES("110","en","_ADDRESS","Address");
INSERT INTO phpn_vocabulary VALUES("113","en","_ADDRESS_2","Address (line 2)");
INSERT INTO phpn_vocabulary VALUES("5866","ID","_TRASH_PAGES","Trash Pages");
INSERT INTO phpn_vocabulary VALUES("5867","ID","_TRUNCATE_RELATED_TABLES","Truncate related tables?");
INSERT INTO phpn_vocabulary VALUES("116","en","_ADDRESS_EMPTY_ALERT","Address cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5865","ID","_TRASH","Trash");
INSERT INTO phpn_vocabulary VALUES("119","en","_ADD_DEFAULT_PERIODS","Add Default Periods");
INSERT INTO phpn_vocabulary VALUES("122","en","_ADD_NEW","Add New");
INSERT INTO phpn_vocabulary VALUES("5864","ID","_TRANSLATE_VIA_GOOGLE","Translate via Google");
INSERT INTO phpn_vocabulary VALUES("125","en","_ADD_NEW_MENU","Add New Menu");
INSERT INTO phpn_vocabulary VALUES("5863","ID","_TRANSACTION","Transaction");
INSERT INTO phpn_vocabulary VALUES("128","en","_ADD_TO_CART","Add to Cart");
INSERT INTO phpn_vocabulary VALUES("131","en","_ADD_TO_MENU","Add To Menu");
INSERT INTO phpn_vocabulary VALUES("5862","ID","_TOTAL_ROOMS","Total Rooms");
INSERT INTO phpn_vocabulary VALUES("134","en","_ADMIN","Admin");
INSERT INTO phpn_vocabulary VALUES("5861","ID","_TOTAL_PRICE","Total Price");
INSERT INTO phpn_vocabulary VALUES("137","en","_ADMINISTRATOR_ONLY","Administrator Only");
INSERT INTO phpn_vocabulary VALUES("5860","ID","_TOTAL","Total");
INSERT INTO phpn_vocabulary VALUES("140","en","_ADMINS","Admins");
INSERT INTO phpn_vocabulary VALUES("5859","ID","_TOP_PANEL","Top Panel");
INSERT INTO phpn_vocabulary VALUES("143","en","_ADMINS_AND_CUSTOMERS","Customers & Admins");
INSERT INTO phpn_vocabulary VALUES("5858","ID","_TOP","Top");
INSERT INTO phpn_vocabulary VALUES("146","en","_ADMINS_MANAGEMENT","Admins Management");
INSERT INTO phpn_vocabulary VALUES("5856","ID","_TO","To");
INSERT INTO phpn_vocabulary VALUES("5857","ID","_TODAY","Today");
INSERT INTO phpn_vocabulary VALUES("149","en","_ADMINS_OWNERS_MANAGEMENT","Admins & Hotel Owners Management");
INSERT INTO phpn_vocabulary VALUES("5855","ID","_TIME_ZONE","Time Zone");
INSERT INTO phpn_vocabulary VALUES("152","en","_ADMIN_EMAIL","Admin Email");
INSERT INTO phpn_vocabulary VALUES("5852","ID","_THUMBNAIL","Thumbnail");
INSERT INTO phpn_vocabulary VALUES("5853","ID","_THURSDAY","Thursday");
INSERT INTO phpn_vocabulary VALUES("5854","ID","_TIME_PERIOD_OVERLAPPING_ALERT","This period of time (fully or partially) is already selected! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("155","en","_ADMIN_EMAIL_ALERT","This email is used as \"From\" address for the system email notifications. Make sure, that you write here a valid email address based on domain of your site");
INSERT INTO phpn_vocabulary VALUES("5849","ID","_TEXT","Text");
INSERT INTO phpn_vocabulary VALUES("5850","ID","_TH","Th");
INSERT INTO phpn_vocabulary VALUES("5851","ID","_THU","Thu");
INSERT INTO phpn_vocabulary VALUES("158","en","_ADMIN_EMAIL_EXISTS_ALERT","Administrator with such email already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("5848","ID","_TEST_MODE_ALERT_SHORT","Attention: Reservation Cart is running in Test Mode!");
INSERT INTO phpn_vocabulary VALUES("161","en","_ADMIN_EMAIL_IS_EMPTY","Admin email must not be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("164","en","_ADMIN_EMAIL_WRONG","Admin email in wrong format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5846","ID","_TEST_EMAIL","Test Email");
INSERT INTO phpn_vocabulary VALUES("5847","ID","_TEST_MODE_ALERT","Test Mode in Reservation Cart is turned ON! To change current mode click <a href=index.php?admin=mod_booking_settings>here</a>.");
INSERT INTO phpn_vocabulary VALUES("167","en","_ADMIN_FOLDER_CREATION_ERROR","Failed to create folder for this author in <b>images/upload/</b> directory. For stable work of the script please create this folder manually.");
INSERT INTO phpn_vocabulary VALUES("5845","ID","_REVIEWS_MANAGEMENT","Reviews Management");
INSERT INTO phpn_vocabulary VALUES("170","en","_ADMIN_LOGIN","Admin Login");
INSERT INTO phpn_vocabulary VALUES("5843","ID","_TERMS","Terms & Conditions");
INSERT INTO phpn_vocabulary VALUES("5844","ID","_REVIEWS","Reviews");
INSERT INTO phpn_vocabulary VALUES("173","en","_ADMIN_MAILER_ALERT","Select which mailer you prefer to use for the delivery of site emails.");
INSERT INTO phpn_vocabulary VALUES("176","en","_ADMIN_PANEL","Admin Panel");
INSERT INTO phpn_vocabulary VALUES("5842","ID","_TEMPLATE_IS_EMPTY","Template cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("179","en","_ADMIN_RESERVATION","Admin Reservation");
INSERT INTO phpn_vocabulary VALUES("5819","ID","_SUBSCRIBE_TO_NEWSLETTER","Newsletter Subscription");
INSERT INTO phpn_vocabulary VALUES("5820","ID","_SUBSCRIPTION_ALREADY_SENT","You have already subscribed to our newsletter. Please try again later or wait _WAIT_ seconds.");
INSERT INTO phpn_vocabulary VALUES("5821","ID","_SUBSCRIPTION_MANAGEMENT","Subscription Management");
INSERT INTO phpn_vocabulary VALUES("5822","ID","_SUBTOTAL","Subtotal");
INSERT INTO phpn_vocabulary VALUES("5823","ID","_SUN","Sun");
INSERT INTO phpn_vocabulary VALUES("5824","ID","_SUNDAY","Sunday");
INSERT INTO phpn_vocabulary VALUES("5825","ID","_SWITCH_TO_EXPORT","Switch to Export");
INSERT INTO phpn_vocabulary VALUES("5826","ID","_SWITCH_TO_NORMAL","Switch to Normal");
INSERT INTO phpn_vocabulary VALUES("5827","ID","_SYMBOL","Symbol");
INSERT INTO phpn_vocabulary VALUES("5828","ID","_SYMBOL_PLACEMENT","Symbol Placement");
INSERT INTO phpn_vocabulary VALUES("5829","ID","_SYSTEM","System");
INSERT INTO phpn_vocabulary VALUES("5830","ID","_SYSTEM_EMAIL_DELETE_ALERT","This email template is used by the system and cannot be deleted!");
INSERT INTO phpn_vocabulary VALUES("5831","ID","_SYSTEM_MODULE","System Module");
INSERT INTO phpn_vocabulary VALUES("5832","ID","_SYSTEM_MODULES","System Modules");
INSERT INTO phpn_vocabulary VALUES("5833","ID","_SYSTEM_MODULE_ACTIONS_BLOCKED","All operations with system module are blocked!");
INSERT INTO phpn_vocabulary VALUES("5834","ID","_SYSTEM_TEMPLATE","System Template");
INSERT INTO phpn_vocabulary VALUES("5835","ID","_TAG","Tag");
INSERT INTO phpn_vocabulary VALUES("5836","ID","_TAG_TITLE_IS_EMPTY","Tag &lt;TITLE&gt; cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5837","ID","_TARGET","Target");
INSERT INTO phpn_vocabulary VALUES("5838","ID","_TARGET_GROUP","Target Group");
INSERT INTO phpn_vocabulary VALUES("5839","ID","_TAXES","Taxes");
INSERT INTO phpn_vocabulary VALUES("5840","ID","_TEMPLATES_STYLES","Templates & Styles");
INSERT INTO phpn_vocabulary VALUES("5841","ID","_TEMPLATE_CODE","Template Code");
INSERT INTO phpn_vocabulary VALUES("182","en","_ADMIN_WELCOME_TEXT","<p>Welcome to Administrator Control Panel that allows you to add, edit or delete site content. With this Administrator Control Panel you can easy manage customers, reservations and perform a full hotel site management.</p><p><b>&#8226;</b> There are some modules for you: Backup & Restore, News. Installation or un-installation of them is possible from <a href=\'index.php?admin=modules\'>Modules Menu</a>.</p><p><b>&#8226;</b> In <a href=\'index.php?admin=languages\'>Languages Menu</a> you may add/remove language or change language settings and edit your vocabulary (the words and phrases, used by the system).</p><p><b>&#8226;</b> <a href=\'index.php?admin=settings\'>Settings Menu</a> allows you to define important settings for the site.</p><p><b>&#8226;</b> In <a href=\'index.php?admin=my_account\'>My Account</a> there is a possibility to change your info.</p><p><b>&#8226;</b> <a href=\'index.php?admin=menus\'>Menus</a> and <a href=\'index.php?admin=pages\'>Pages Management</a> are designed for creating and managing menus, links and pages.</p><p><b>&#8226;</b> To create and edit room types, seasons, prices, bookings and other hotel info, use <a href=\'index.php?admin=hotel_info\'>Hotel Management</a>, <a href=\'index.php?admin=rooms_management\'>Rooms Management</a> and <a href=\'index.php?admin=mod_booking_bookings\'>Bookings</a> menus.</p>");
INSERT INTO phpn_vocabulary VALUES("185","en","_ADULT","Adult");
INSERT INTO phpn_vocabulary VALUES("188","en","_ADULTS","Adults");
INSERT INTO phpn_vocabulary VALUES("191","en","_ADVANCED","Advanced");
INSERT INTO phpn_vocabulary VALUES("194","en","_AFTER","After");
INSERT INTO phpn_vocabulary VALUES("197","en","_AFTER_DISCOUNT","after discount");
INSERT INTO phpn_vocabulary VALUES("5818","ID","_SUBSCRIBE_EMAIL_EXISTS_ALERT","Someone with such email has already been subscribed to our newsletter. Please choose another email address for subscription.");
INSERT INTO phpn_vocabulary VALUES("200","en","_AGENT_COMMISION","Hotel Owner/Agent Commision");
INSERT INTO phpn_vocabulary VALUES("203","en","_AGREE_CONF_TEXT","I have read and AGREE with Terms & Conditions");
INSERT INTO phpn_vocabulary VALUES("5817","ID","_SUBSCRIBE","Subscribe");
INSERT INTO phpn_vocabulary VALUES("206","en","_ALBUM","Album");
INSERT INTO phpn_vocabulary VALUES("209","en","_ALBUM_CODE","Album Code");
INSERT INTO phpn_vocabulary VALUES("5816","ID","_SUBMIT_PAYMENT","Submit Payment");
INSERT INTO phpn_vocabulary VALUES("212","en","_ALBUM_NAME","Album Name");
INSERT INTO phpn_vocabulary VALUES("5815","ID","_SUBMIT_BOOKING","Submit Booking");
INSERT INTO phpn_vocabulary VALUES("215","en","_ALERT_CANCEL_BOOKING","Are you sure you want to cancel this booking?");
INSERT INTO phpn_vocabulary VALUES("5814","ID","_SUBMIT","Submit");
INSERT INTO phpn_vocabulary VALUES("218","en","_ALERT_REQUIRED_FILEDS","Items marked with an asterisk (*) are required");
INSERT INTO phpn_vocabulary VALUES("221","en","_ALL","All");
INSERT INTO phpn_vocabulary VALUES("5813","ID","_SUBJECT_EMPTY_ALERT","Subject cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("224","en","_ALLOW","Allow");
INSERT INTO phpn_vocabulary VALUES("5812","ID","_SUBJECT","Subject");
INSERT INTO phpn_vocabulary VALUES("227","en","_ALLOW_COMMENTS","Allow comments");
INSERT INTO phpn_vocabulary VALUES("5811","ID","_SU","Su");
INSERT INTO phpn_vocabulary VALUES("230","en","_ALL_AVAILABLE","All Available");
INSERT INTO phpn_vocabulary VALUES("5808","ID","_STATISTICS","Statistics");
INSERT INTO phpn_vocabulary VALUES("5809","ID","_STATUS","Status");
INSERT INTO phpn_vocabulary VALUES("5810","ID","_STOP","Stop");
INSERT INTO phpn_vocabulary VALUES("233","en","_ALREADY_HAVE_ACCOUNT","Already have an account? <a href=\'index.php?customer=login\'>Login here</a>");
INSERT INTO phpn_vocabulary VALUES("5807","ID","_STATE_PROVINCE","State/Province");
INSERT INTO phpn_vocabulary VALUES("236","en","_ALREADY_LOGGED","You are already logged in!");
INSERT INTO phpn_vocabulary VALUES("5806","ID","_STATES","States");
INSERT INTO phpn_vocabulary VALUES("239","en","_AMOUNT","Amount");
INSERT INTO phpn_vocabulary VALUES("5805","ID","_STATE","State");
INSERT INTO phpn_vocabulary VALUES("242","en","_ANSWER","Answer");
INSERT INTO phpn_vocabulary VALUES("245","en","_ANY","Any");
INSERT INTO phpn_vocabulary VALUES("5804","ID","_START_OVER","Start Over");
INSERT INTO phpn_vocabulary VALUES("248","en","_APPLY","Apply");
INSERT INTO phpn_vocabulary VALUES("251","en","_APPLY_TO_ALL_LANGUAGES","Apply to all languages");
INSERT INTO phpn_vocabulary VALUES("5803","ID","_START_FINISH_DATE_ERROR","Finish date must be later than start date! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("254","en","_APPLY_TO_ALL_PAGES","Apply changes to all pages");
INSERT INTO phpn_vocabulary VALUES("257","en","_APPROVE","Approve");
INSERT INTO phpn_vocabulary VALUES("5802","ID","_START_DATE","Start Date");
INSERT INTO phpn_vocabulary VALUES("260","en","_APPROVED","Approved");
INSERT INTO phpn_vocabulary VALUES("263","en","_APRIL","April");
INSERT INTO phpn_vocabulary VALUES("5801","ID","_STARS_5_1","5 stars to 1 star");
INSERT INTO phpn_vocabulary VALUES("266","en","_ARTICLE","Article");
INSERT INTO phpn_vocabulary VALUES("269","en","_ARTICLE_ID","Article ID");
INSERT INTO phpn_vocabulary VALUES("5800","ID","_STARS_1_5","1 star to 5 stars");
INSERT INTO phpn_vocabulary VALUES("272","en","_AUGUST","August");
INSERT INTO phpn_vocabulary VALUES("5799","ID","_STARS","Stars");
INSERT INTO phpn_vocabulary VALUES("275","en","_AUTHENTICATION","Authentication");
INSERT INTO phpn_vocabulary VALUES("5798","ID","_STANDARD_PRICE","Standard Price");
INSERT INTO phpn_vocabulary VALUES("278","en","_AUTHORIZE_NET_NOTICE","The Authorize.Net payment gateway service provider.");
INSERT INTO phpn_vocabulary VALUES("5797","ID","_STANDARD","Standard");
INSERT INTO phpn_vocabulary VALUES("281","en","_AUTHORIZE_NET_ORDER","Authorize.Net Order");
INSERT INTO phpn_vocabulary VALUES("5796","ID","_SORT_BY","Sort by");
INSERT INTO phpn_vocabulary VALUES("284","en","_AVAILABILITY","Availability");
INSERT INTO phpn_vocabulary VALUES("5789","ID","_SITE_PREVIEW","Site Preview");
INSERT INTO phpn_vocabulary VALUES("5790","ID","_SITE_RANKS","Site Ranks");
INSERT INTO phpn_vocabulary VALUES("5791","ID","_SITE_RSS","Site RSS");
INSERT INTO phpn_vocabulary VALUES("5792","ID","_SITE_SETTINGS","Site Settings");
INSERT INTO phpn_vocabulary VALUES("5793","ID","_SMTP_HOST","SMTP Host");
INSERT INTO phpn_vocabulary VALUES("5794","ID","_SMTP_PORT","SMTP Port");
INSERT INTO phpn_vocabulary VALUES("5795","ID","_SMTP_SECURE","SMTP Secure");
INSERT INTO phpn_vocabulary VALUES("287","en","_AVAILABILITY_ROOMS_NOTE","Define a maximum number of rooms available for booking for a specified day or date range (maximum availability _MAX_ rooms)<br>To edit room availability simply change the value in a day cell and then click \'Save Changes\' button");
INSERT INTO phpn_vocabulary VALUES("290","en","_AVAILABLE","available");
INSERT INTO phpn_vocabulary VALUES("293","en","_AVAILABLE_ROOMS","Available Rooms");
INSERT INTO phpn_vocabulary VALUES("296","en","_BACKUP","Backup");
INSERT INTO phpn_vocabulary VALUES("5788","ID","_SITE_OFFLINE_MESSAGE_ALERT","A message that displays in the Front-end if your site is offline");
INSERT INTO phpn_vocabulary VALUES("299","en","_BACKUPS_EXISTING","Existing Backups");
INSERT INTO phpn_vocabulary VALUES("302","en","_BACKUP_AND_RESTORE","Backup & Restore");
INSERT INTO phpn_vocabulary VALUES("305","en","_BACKUP_CHOOSE_MSG","Choose a backup from the list below");
INSERT INTO phpn_vocabulary VALUES("5787","ID","_SITE_OFFLINE_ALERT","Select whether access to the Site Front-end is available. If Yes, the Front-End will display the message below");
INSERT INTO phpn_vocabulary VALUES("308","en","_BACKUP_DELETE_ALERT","Are you sure you want to delete this backup?");
INSERT INTO phpn_vocabulary VALUES("5786","ID","_SITE_OFFLINE","Site Offline");
INSERT INTO phpn_vocabulary VALUES("311","en","_BACKUP_EMPTY_MSG","No existing backups found.");
INSERT INTO phpn_vocabulary VALUES("5785","ID","_SITE_INFO","Site Info");
INSERT INTO phpn_vocabulary VALUES("314","en","_BACKUP_EMPTY_NAME_ALERT","Name of backup file cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("317","en","_BACKUP_EXECUTING_ERROR","An error occurred while backup the system! Please check write permissions to backup folder or try again later.");
INSERT INTO phpn_vocabulary VALUES("5784","ID","_SITE_DEVELOPMENT_MODE_ALERT","The site is running in Development Mode! To turn it off change <b>SITE_MODE</b> value in <b>inc/settings.inc.php</b>");
INSERT INTO phpn_vocabulary VALUES("320","en","_BACKUP_INSTALLATION","Backup Installation");
INSERT INTO phpn_vocabulary VALUES("5783","ID","_SIMPLE","Simple");
INSERT INTO phpn_vocabulary VALUES("323","en","_BACKUP_RESTORE","Backup Restore");
INSERT INTO phpn_vocabulary VALUES("5782","ID","_SIDE_PANEL","Side Panel");
INSERT INTO phpn_vocabulary VALUES("326","en","_BACKUP_RESTORE_ALERT","Are you sure you want to restore this backup");
INSERT INTO phpn_vocabulary VALUES("5780","ID","_SHOW_META_TAGS","Show META tags");
INSERT INTO phpn_vocabulary VALUES("5781","ID","_SHOW_ON_DASHBOARD","Show on Dashboard");
INSERT INTO phpn_vocabulary VALUES("329","en","_BACKUP_RESTORE_NOTE","Remember: this action will rewrite all your current settings!");
INSERT INTO phpn_vocabulary VALUES("5778","ID","_SHOW","Show");
INSERT INTO phpn_vocabulary VALUES("5779","ID","_SHOW_IN_SEARCH","Show in Search");
INSERT INTO phpn_vocabulary VALUES("332","en","_BACKUP_RESTORING_ERROR","An error occurred while restoring file! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5776","ID","_SET_TIME","Set Time");
INSERT INTO phpn_vocabulary VALUES("5777","ID","_SHORT_DESCRIPTION","Short Description");
INSERT INTO phpn_vocabulary VALUES("335","en","_BACKUP_WAS_CREATED","Backup _FILE_NAME_ has been successfully created.");
INSERT INTO phpn_vocabulary VALUES("5775","ID","_SET_PERIODS","Set Periods");
INSERT INTO phpn_vocabulary VALUES("338","en","_BACKUP_WAS_DELETED","Backup _FILE_NAME_ has been successfully deleted.");
INSERT INTO phpn_vocabulary VALUES("5773","ID","_SET_ADMIN","Set Admin");
INSERT INTO phpn_vocabulary VALUES("5774","ID","_SET_DATE","Set date");
INSERT INTO phpn_vocabulary VALUES("341","en","_BACKUP_WAS_RESTORED","Backup _FILE_NAME_ has been successfully restored.");
INSERT INTO phpn_vocabulary VALUES("344","en","_BACKUP_YOUR_INSTALLATION","Backup your current Installation");
INSERT INTO phpn_vocabulary VALUES("5772","ID","_SETTINGS_SAVED","Changes were saved! Please refresh the <a href=index.php>Home Page</a> to see the results.");
INSERT INTO phpn_vocabulary VALUES("347","en","_BACK_TO_ADMIN_PANEL","Back to Admin Panel");
INSERT INTO phpn_vocabulary VALUES("5771","ID","_SETTINGS","Settings");
INSERT INTO phpn_vocabulary VALUES("350","en","_BANK_PAYMENT_INFO","Bank Payment Information");
INSERT INTO phpn_vocabulary VALUES("353","en","_BANK_TRANSFER","Bank Transfer");
INSERT INTO phpn_vocabulary VALUES("5770","ID","_SERVICES","Services");
INSERT INTO phpn_vocabulary VALUES("356","en","_BANNERS","Banners");
INSERT INTO phpn_vocabulary VALUES("5769","ID","_SERVICE","Service");
INSERT INTO phpn_vocabulary VALUES("359","en","_BANNERS_MANAGEMENT","Banners Management");
INSERT INTO phpn_vocabulary VALUES("5768","ID","_SERVER_LOCALE","Server Locale");
INSERT INTO phpn_vocabulary VALUES("362","en","_BANNERS_SETTINGS","Banners Settings");
INSERT INTO phpn_vocabulary VALUES("365","en","_BANNER_IMAGE","Banner Image");
INSERT INTO phpn_vocabulary VALUES("5767","ID","_SERVER_INFO","Server Info");
INSERT INTO phpn_vocabulary VALUES("368","en","_BAN_ITEM","Ban Item");
INSERT INTO phpn_vocabulary VALUES("5766","ID","_SEPTEMBER","September");
INSERT INTO phpn_vocabulary VALUES("371","en","_BAN_LIST","Ban List");
INSERT INTO phpn_vocabulary VALUES("374","en","_BATHROOMS","Bathrooms");
INSERT INTO phpn_vocabulary VALUES("5765","ID","_SEO_URLS","SEO URLs");
INSERT INTO phpn_vocabulary VALUES("377","en","_BEDS","Beds");
INSERT INTO phpn_vocabulary VALUES("380","en","_BEFORE","Before");
INSERT INTO phpn_vocabulary VALUES("383","en","_BILLING_ADDRESS","Billing Address");
INSERT INTO phpn_vocabulary VALUES("386","en","_BILLING_DETAILS","Billing Details");
INSERT INTO phpn_vocabulary VALUES("388","en","_BILLING_DETAILS_UPDATED","Your Billing Details has been updated.");
INSERT INTO phpn_vocabulary VALUES("5764","ID","_SEO_LINKS_ALERT","If you select this option, make sure SEO Links Section uncommented in .htaccess file");
INSERT INTO phpn_vocabulary VALUES("390","en","_BIRTH_DATE","Birth Date");
INSERT INTO phpn_vocabulary VALUES("5763","ID","_SEND_INVOICE","Send Invoice");
INSERT INTO phpn_vocabulary VALUES("393","en","_BIRTH_DATE_VALID_ALERT","Birth date has been entered in wrong format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5762","ID","_SEND_COPY_TO_ADMIN","Send a copy to admin");
INSERT INTO phpn_vocabulary VALUES("396","en","_BOOK","Book");
INSERT INTO phpn_vocabulary VALUES("5761","ID","_SENDING","Sending");
INSERT INTO phpn_vocabulary VALUES("399","en","_BOOKING","Booking");
INSERT INTO phpn_vocabulary VALUES("5760","ID","_SEND","Send");
INSERT INTO phpn_vocabulary VALUES("402","en","_BOOKINGS","Bookings");
INSERT INTO phpn_vocabulary VALUES("405","en","_BOOKINGS_MANAGEMENT","Bookings Management");
INSERT INTO phpn_vocabulary VALUES("5759","ID","_SELECT_REPORT_ALERT","Please select a report type!");
INSERT INTO phpn_vocabulary VALUES("408","en","_BOOKINGS_SETTINGS","Booking Settings");
INSERT INTO phpn_vocabulary VALUES("5758","ID","_SELECT_LOCATION","Select Location");
INSERT INTO phpn_vocabulary VALUES("411","en","_BOOKING_CANCELED","Booking Canceled");
INSERT INTO phpn_vocabulary VALUES("5757","ID","_SELECT_LANG_TO_UPDATE","Select a language to update");
INSERT INTO phpn_vocabulary VALUES("414","en","_BOOKING_CANCELED_SUCCESS","The booking _BOOKING_ has been successfully canceled from the system!");
INSERT INTO phpn_vocabulary VALUES("5756","ID","_SELECT_HOTEL","Select Hotel");
INSERT INTO phpn_vocabulary VALUES("417","en","_BOOKING_COMPLETED","Booking Completed");
INSERT INTO phpn_vocabulary VALUES("420","en","_BOOKING_DATE","Booking Date");
INSERT INTO phpn_vocabulary VALUES("5755","ID","_SELECT_FILE_TO_UPLOAD","Select a file to upload");
INSERT INTO phpn_vocabulary VALUES("423","en","_BOOKING_DESCRIPTION","Booking Description");
INSERT INTO phpn_vocabulary VALUES("5754","ID","_SELECTED_ROOMS","Selected Rooms");
INSERT INTO phpn_vocabulary VALUES("426","en","_BOOKING_DETAILS","Booking Details");
INSERT INTO phpn_vocabulary VALUES("5753","ID","_SELECT","select");
INSERT INTO phpn_vocabulary VALUES("429","en","_BOOKING_NUMBER","Booking Number");
INSERT INTO phpn_vocabulary VALUES("5752","ID","_SEC","Sec");
INSERT INTO phpn_vocabulary VALUES("432","en","_BOOKING_PRICE","Booking Price");
INSERT INTO phpn_vocabulary VALUES("435","en","_BOOKING_SETTINGS","Booking Settings");
INSERT INTO phpn_vocabulary VALUES("438","en","_BOOKING_STATUS","Booking Status");
INSERT INTO phpn_vocabulary VALUES("441","en","_BOOKING_SUBTOTAL","Booking Subtotal");
INSERT INTO phpn_vocabulary VALUES("444","en","_BOOKING_WAS_CANCELED_MSG","Your booking has been canceled.");
INSERT INTO phpn_vocabulary VALUES("5751","ID","_SEARCH_ROOM_TIPS","<b>Find more rooms by expanding your search options</b>:<br>- Reduce the number of adults in room to get more results<br>- Reduce the number of children in room to get more results<br>- Change your Check-in/Check-out dates<br>");
INSERT INTO phpn_vocabulary VALUES("447","en","_BOOKING_WAS_COMPLETED_MSG","Thank you for reservation rooms in our hotel! Your booking has been completed.");
INSERT INTO phpn_vocabulary VALUES("5750","ID","_SEARCH_RESULT_FOR","Search Results for");
INSERT INTO phpn_vocabulary VALUES("450","en","_BOOK_NOW","Book Now");
INSERT INTO phpn_vocabulary VALUES("5749","ID","_SEARCH_KEYWORDS","search keywords");
INSERT INTO phpn_vocabulary VALUES("453","en","_BOOK_ONE_NIGHT_ALERT","Sorry, but you must book at least one night.");
INSERT INTO phpn_vocabulary VALUES("5748","ID","_SEARCH","Search");
INSERT INTO phpn_vocabulary VALUES("456","en","_BOTTOM","Bottom");
INSERT INTO phpn_vocabulary VALUES("459","en","_BUTTON_BACK","Back");
INSERT INTO phpn_vocabulary VALUES("5747","ID","_SCHEDULED_CAMPAIGN","Scheduled Campaign");
INSERT INTO phpn_vocabulary VALUES("462","en","_BUTTON_CANCEL","Cancel");
INSERT INTO phpn_vocabulary VALUES("5746","ID","_SATURDAY","Saturday");
INSERT INTO phpn_vocabulary VALUES("465","en","_BUTTON_CHANGE","Change");
INSERT INTO phpn_vocabulary VALUES("5745","ID","_SAT","Sat");
INSERT INTO phpn_vocabulary VALUES("468","en","_BUTTON_CHANGE_PASSWORD","Change Password");
INSERT INTO phpn_vocabulary VALUES("5744","ID","_SAID","said");
INSERT INTO phpn_vocabulary VALUES("471","en","_BUTTON_CREATE","Create");
INSERT INTO phpn_vocabulary VALUES("5743","ID","_SA","Sa");
INSERT INTO phpn_vocabulary VALUES("474","en","_BUTTON_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("5742","ID","_RUN_EVERY","Run every");
INSERT INTO phpn_vocabulary VALUES("477","en","_BUTTON_LOGOUT","Logout");
INSERT INTO phpn_vocabulary VALUES("5741","ID","_RUN_CRON","Run cron");
INSERT INTO phpn_vocabulary VALUES("480","en","_BUTTON_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("483","en","_BUTTON_REWRITE","Rewrite Vocabulary");
INSERT INTO phpn_vocabulary VALUES("486","en","_BUTTON_SAVE_CHANGES","Save Changes");
INSERT INTO phpn_vocabulary VALUES("489","en","_BUTTON_UPDATE","Update");
INSERT INTO phpn_vocabulary VALUES("5740","ID","_RSS_FILE_ERROR","Cannot open RSS file to add new item! Please check your access rights to <b>feeds/</b> directory or try again later.");
INSERT INTO phpn_vocabulary VALUES("492","en","_CACHE_LIFETIME","Cache Lifetime");
INSERT INTO phpn_vocabulary VALUES("495","en","_CACHING","Caching");
INSERT INTO phpn_vocabulary VALUES("5739","ID","_RSS_FEED_TYPE","RSS Feed Type");
INSERT INTO phpn_vocabulary VALUES("498","en","_CAMPAIGNS","Campaigns");
INSERT INTO phpn_vocabulary VALUES("5738","ID","_ROWS","Rows");
INSERT INTO phpn_vocabulary VALUES("501","en","_CAMPAIGNS_MANAGEMENT","Campaigns Management");
INSERT INTO phpn_vocabulary VALUES("5735","ID","_ROOM_TYPE","Room Type");
INSERT INTO phpn_vocabulary VALUES("5736","ID","_ROOM_WAS_ADDED","Room has been successfully added to your reservation!");
INSERT INTO phpn_vocabulary VALUES("5737","ID","_ROOM_WAS_REMOVED","Selected room has been successfully removed from your Reservation Cart!");
INSERT INTO phpn_vocabulary VALUES("504","en","_CAMPAIGNS_TOOLTIP","Global - allows booking for any date and runs (visible) within a defined period of time only\n\nTargeted - allows booking in a specified period of time only and runs (visible) till the first date is beginning");
INSERT INTO phpn_vocabulary VALUES("507","en","_CANCELED","Canceled");
INSERT INTO phpn_vocabulary VALUES("5733","ID","_ROOM_PRICE","Room Price");
INSERT INTO phpn_vocabulary VALUES("5734","ID","_ROOM_PRICES_WERE_ADDED","Room prices for new period were successfully added!");
INSERT INTO phpn_vocabulary VALUES("510","en","_CANCELED_BY_ADMIN","This booking is canceled by administrator.");
INSERT INTO phpn_vocabulary VALUES("5732","ID","_ROOM_NUMBERS","Room Numbers");
INSERT INTO phpn_vocabulary VALUES("513","en","_CANCELED_BY_CUSTOMER","This booking has been canceled by customer.");
INSERT INTO phpn_vocabulary VALUES("516","en","_CAN_USE_TAGS_MSG","You can use some HTML tags, such as");
INSERT INTO phpn_vocabulary VALUES("5731","ID","_ROOM_NOT_FOUND","Room has not been found!");
INSERT INTO phpn_vocabulary VALUES("519","en","_CAPACITY","Capacity");
INSERT INTO phpn_vocabulary VALUES("522","en","_CART_WAS_UPDATED","Reservation cart has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("5730","ID","_ROOM_FACILITIES_MANAGEMENT","Room Facilities Management");
INSERT INTO phpn_vocabulary VALUES("525","en","_CATEGORIES","Categories");
INSERT INTO phpn_vocabulary VALUES("528","en","_CATEGORIES_MANAGEMENT","Categories Management");
INSERT INTO phpn_vocabulary VALUES("5729","ID","_ROOM_FACILITIES","Room Facilities");
INSERT INTO phpn_vocabulary VALUES("531","en","_CATEGORY","Category");
INSERT INTO phpn_vocabulary VALUES("5728","ID","_ROOM_DETAILS","Room Details");
INSERT INTO phpn_vocabulary VALUES("534","en","_CATEGORY_DESCRIPTION","Category Description");
INSERT INTO phpn_vocabulary VALUES("5726","ID","_ROOM_AREA","Room Area");
INSERT INTO phpn_vocabulary VALUES("5727","ID","_ROOM_DESCRIPTION","Room Description");
INSERT INTO phpn_vocabulary VALUES("537","en","_CC_CARD_HOLDER_NAME_EMPTY","No card holder\'s name provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5725","ID","_ROOMS_SETTINGS","Rooms Settings");
INSERT INTO phpn_vocabulary VALUES("540","en","_CC_CARD_INVALID_FORMAT","Credit card number has invalid format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5723","ID","_ROOMS_OCCUPANCY","Rooms Occupancy");
INSERT INTO phpn_vocabulary VALUES("5724","ID","_ROOMS_RESERVATION","Rooms Reservation");
INSERT INTO phpn_vocabulary VALUES("543","en","_CC_CARD_INVALID_NUMBER","Credit card number is invalid! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5721","ID","_ROOMS_LEFT","rooms left");
INSERT INTO phpn_vocabulary VALUES("5722","ID","_ROOMS_MANAGEMENT","Rooms Management");
INSERT INTO phpn_vocabulary VALUES("546","en","_CC_CARD_NO_CVV_NUMBER","No CVV Code provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5719","ID","_ROOMS_FACILITIES","Rooms Facilities");
INSERT INTO phpn_vocabulary VALUES("5720","ID","_ROOMS_LAST","last room");
INSERT INTO phpn_vocabulary VALUES("549","en","_CC_CARD_WRONG_EXPIRE_DATE","Credit card expiry date is wrong! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5718","ID","_ROOMS_COUNT","Number of Rooms (in the Hotel)");
INSERT INTO phpn_vocabulary VALUES("552","en","_CC_CARD_WRONG_LENGTH","Credit card number has a wrong length! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5716","ID","_ROOMS","Rooms");
INSERT INTO phpn_vocabulary VALUES("5717","ID","_ROOMS_AVAILABILITY","Rooms Availability");
INSERT INTO phpn_vocabulary VALUES("555","en","_CC_NO_CARD_NUMBER_PROVIDED","No card number provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5715","ID","_ROLES_MANAGEMENT","Roles Management");
INSERT INTO phpn_vocabulary VALUES("558","en","_CC_NUMBER_INVALID","Credit card number is invalid! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5714","ID","_ROLES_AND_PRIVILEGES","Roles & Privileges");
INSERT INTO phpn_vocabulary VALUES("561","en","_CC_UNKNOWN_CARD_TYPE","Unknown card type! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("564","en","_CHANGES_SAVED","Changes were saved.");
INSERT INTO phpn_vocabulary VALUES("5712","ID","_RIGHT","Right");
INSERT INTO phpn_vocabulary VALUES("5713","ID","_RIGHT_TO_LEFT","RTL (right-to-left)");
INSERT INTO phpn_vocabulary VALUES("567","en","_CHANGES_WERE_SAVED","Changes were successfully saved! Please refresh the <a href=index.php>Home Page</a> to see the results.");
INSERT INTO phpn_vocabulary VALUES("5711","ID","_RETYPE_PASSWORD","Retype Password");
INSERT INTO phpn_vocabulary VALUES("570","en","_CHANGE_CUSTOMER","Change Customer");
INSERT INTO phpn_vocabulary VALUES("5710","ID","_RESTORE","Restore");
INSERT INTO phpn_vocabulary VALUES("573","en","_CHANGE_ORDER","Change Order");
INSERT INTO phpn_vocabulary VALUES("5709","ID","_RESTAURANT","Restaurant");
INSERT INTO phpn_vocabulary VALUES("576","en","_CHANGE_YOUR_PASSWORD","Change your password");
INSERT INTO phpn_vocabulary VALUES("579","en","_CHARGE_TYPE","Charge Type");
INSERT INTO phpn_vocabulary VALUES("5708","ID","_RESET_ACCOUNT","Reset Account");
INSERT INTO phpn_vocabulary VALUES("582","en","_CHECKOUT","Checkout");
INSERT INTO phpn_vocabulary VALUES("5707","ID","_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("585","en","_CHECK_AVAILABILITY","Check Availability");
INSERT INTO phpn_vocabulary VALUES("5706","ID","_RESERVED","Reserved");
INSERT INTO phpn_vocabulary VALUES("588","en","_CHECK_IN","Check In");
INSERT INTO phpn_vocabulary VALUES("591","en","_CHECK_NOW","Check Now");
INSERT INTO phpn_vocabulary VALUES("5705","ID","_RESERVATION_DETAILS","Reservation Details");
INSERT INTO phpn_vocabulary VALUES("594","en","_CHECK_OUT","Check Out");
INSERT INTO phpn_vocabulary VALUES("597","en","_CHECK_STATUS","Check Status");
INSERT INTO phpn_vocabulary VALUES("600","en","_CHILD","Child");
INSERT INTO phpn_vocabulary VALUES("603","en","_CHILDREN","Children");
INSERT INTO phpn_vocabulary VALUES("5704","ID","_RESERVATION_CART_IS_EMPTY_ALERT","Your reservation cart is empty!");
INSERT INTO phpn_vocabulary VALUES("606","en","_CITY","City");
INSERT INTO phpn_vocabulary VALUES("5703","ID","_RESERVATION_CART","Reservation Cart");
INSERT INTO phpn_vocabulary VALUES("609","en","_CITY_EMPTY_ALERT","City cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("612","en","_CLEANED","Cleaned");
INSERT INTO phpn_vocabulary VALUES("5702","ID","_RESERVATIONS","Reservations");
INSERT INTO phpn_vocabulary VALUES("615","en","_CLEANUP","Cleanup");
INSERT INTO phpn_vocabulary VALUES("5699","ID","_RESEND_ACTIVATION_EMAIL","Resend Activation Email");
INSERT INTO phpn_vocabulary VALUES("5700","ID","_RESEND_ACTIVATION_EMAIL_MSG","Please enter your email address then click on Send button. You will receive the activation email shortly.");
INSERT INTO phpn_vocabulary VALUES("5701","ID","_RESERVATION","Reservation");
INSERT INTO phpn_vocabulary VALUES("618","en","_CLEANUP_TOOLTIP","The cleanup feature is used to remove pending (temporary) reservations from your web site. A pending reservation is one where the system is waiting for the payment gateway to callback with the transaction status.");
INSERT INTO phpn_vocabulary VALUES("5698","ID","_REPORTS","Reports");
INSERT INTO phpn_vocabulary VALUES("621","en","_CLEAN_CACHE","Clean Cache");
INSERT INTO phpn_vocabulary VALUES("5697","ID","_REMOVE_ROOM_FROM_CART","Remove room from the cart");
INSERT INTO phpn_vocabulary VALUES("624","en","_CLICK_FOR_MORE_INFO","Click for more information");
INSERT INTO phpn_vocabulary VALUES("627","en","_CLICK_TO_COPY","Click to copy");
INSERT INTO phpn_vocabulary VALUES("630","en","_CLICK_TO_EDIT","Click to edit");
INSERT INTO phpn_vocabulary VALUES("633","en","_CLICK_TO_INCREASE","Click to enlarge");
INSERT INTO phpn_vocabulary VALUES("5696","ID","_REMOVE_LAST_COUNTRY_ALERT","The country selected has not been deleted, because you must have at least one active country for correct work of the site!");
INSERT INTO phpn_vocabulary VALUES("636","en","_CLICK_TO_MANAGE","Click to manage");
INSERT INTO phpn_vocabulary VALUES("639","en","_CLICK_TO_SEE_DESCR","Click to see description");
INSERT INTO phpn_vocabulary VALUES("642","en","_CLICK_TO_SEE_PRICES","Click to see prices");
INSERT INTO phpn_vocabulary VALUES("645","en","_CLICK_TO_VIEW","Click to view");
INSERT INTO phpn_vocabulary VALUES("648","en","_CLOSE","Close");
INSERT INTO phpn_vocabulary VALUES("651","en","_CLOSE_META_TAGS","Close META tags");
INSERT INTO phpn_vocabulary VALUES("654","en","_CODE","Code");
INSERT INTO phpn_vocabulary VALUES("657","en","_COLLAPSE_PANEL","Collapse navigation panel");
INSERT INTO phpn_vocabulary VALUES("660","en","_COMMENTS","Comments");
INSERT INTO phpn_vocabulary VALUES("5694","ID","_REMOVE_ACCOUNT_ALERT","Are you sure you want to remove your account?");
INSERT INTO phpn_vocabulary VALUES("5695","ID","_REMOVE_ACCOUNT_WARNING","If you don\'t think you will use this site again and would like your account deleted, we can take care of this for you. Keep in mind, that you will not be able to reactivate your account or retrieve any of the content or information that was added. If you would like your account deleted, then click Remove button");
INSERT INTO phpn_vocabulary VALUES("663","en","_COMMENTS_AWAITING_MODERATION_ALERT","There are _COUNT_ comment/s awaiting your moderation. Click <a href=\'index.php?admin=mod_comments_management\'>here</a> for review.");
INSERT INTO phpn_vocabulary VALUES("5693","ID","_REMOVE_ACCOUNT","Remove Account");
INSERT INTO phpn_vocabulary VALUES("666","en","_COMMENTS_LINK","Comments (_COUNT_)");
INSERT INTO phpn_vocabulary VALUES("5692","ID","_REMOVED","Removed");
INSERT INTO phpn_vocabulary VALUES("669","en","_COMMENTS_MANAGEMENT","Comments Management");
INSERT INTO phpn_vocabulary VALUES("5691","ID","_REMOVE","Remove");
INSERT INTO phpn_vocabulary VALUES("672","en","_COMMENTS_SETTINGS","Comments Settings");
INSERT INTO phpn_vocabulary VALUES("5690","ID","_REMEMBER_ME","Remember Me");
INSERT INTO phpn_vocabulary VALUES("675","en","_COMMENT_DELETED_SUCCESS","Your comment has been successfully deleted.");
INSERT INTO phpn_vocabulary VALUES("678","en","_COMMENT_LENGTH_ALERT","The length of comment must be less than _LENGTH_ characters!");
INSERT INTO phpn_vocabulary VALUES("681","en","_COMMENT_POSTED_SUCCESS","Your comment has been successfully posted!");
INSERT INTO phpn_vocabulary VALUES("5688","ID","_REGISTRATION_FORM","Registration Form");
INSERT INTO phpn_vocabulary VALUES("5689","ID","_REGISTRATION_NOT_COMPLETED","Your registration process is not yet complete! Please check again your email for further instructions or click <a href=index.php?customer=resend_activation>here</a> to resend them again.");
INSERT INTO phpn_vocabulary VALUES("684","en","_COMMENT_SUBMITTED_SUCCESS","Your comment has been successfully submitted and will be posted after administrator\'s review!");
INSERT INTO phpn_vocabulary VALUES("687","en","_COMMENT_TEXT","Comment text");
INSERT INTO phpn_vocabulary VALUES("5687","ID","_REGISTRATION_CONFIRMATION","Registration Confirmation");
INSERT INTO phpn_vocabulary VALUES("690","en","_COMPANY","Company");
INSERT INTO phpn_vocabulary VALUES("693","en","_COMPLETED","Completed (Paid)");
INSERT INTO phpn_vocabulary VALUES("5686","ID","_REGISTRATION_CODE","Registration code");
INSERT INTO phpn_vocabulary VALUES("696","en","_CONFIRMATION","Confirmation");
INSERT INTO phpn_vocabulary VALUES("5685","ID","_REGISTRATIONS","Registrations");
INSERT INTO phpn_vocabulary VALUES("699","en","_CONFIRMATION_CODE","Confirmation Code");
INSERT INTO phpn_vocabulary VALUES("5682","ID","_REFUNDED","Refunded");
INSERT INTO phpn_vocabulary VALUES("5683","ID","_REGISTERED","Registered");
INSERT INTO phpn_vocabulary VALUES("5684","ID","_REGISTERED_FROM_IP","Registered from IP");
INSERT INTO phpn_vocabulary VALUES("702","en","_CONFIRMED_ALREADY_MSG","Your account has already been confirmed! <br /><br />Click <a href=\'index.php?customer=login\'>here</a> to continue.");
INSERT INTO phpn_vocabulary VALUES("5679","ID","_REASON","Reason");
INSERT INTO phpn_vocabulary VALUES("5680","ID","_RECORD_WAS_DELETED_COMMON","The record has been successfully deleted!");
INSERT INTO phpn_vocabulary VALUES("5681","ID","_REFRESH","Refresh");
INSERT INTO phpn_vocabulary VALUES("705","en","_CONFIRMED_SUCCESS_MSG","Thank you for confirming your registration! <br /><br />You may now log into your account. Click <a href=\'index.php?customer=login\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("708","en","_CONFIRM_PASSWORD","Confirm Password");
INSERT INTO phpn_vocabulary VALUES("5677","ID","_READ_MORE","Read more");
INSERT INTO phpn_vocabulary VALUES("5678","ID","_REAL_TIME_CAMPAIGN","Real Time Campaign");
INSERT INTO phpn_vocabulary VALUES("711","en","_CONFIRM_TERMS_CONDITIONS","You must confirm you agree to our Terms & Conditions!");
INSERT INTO phpn_vocabulary VALUES("5676","ID","_READY","Ready");
INSERT INTO phpn_vocabulary VALUES("714","en","_CONF_PASSWORD_IS_EMPTY","Confirm Password cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5674","ID","_RATINGS_SETTINGS","Ratings Settings");
INSERT INTO phpn_vocabulary VALUES("5675","ID","_REACTIVATION_EMAIL","Resend Activation Email");
INSERT INTO phpn_vocabulary VALUES("717","en","_CONF_PASSWORD_MATCH","Password must be match with Confirm Password");
INSERT INTO phpn_vocabulary VALUES("5670","ID","_RATE","Rate");
INSERT INTO phpn_vocabulary VALUES("5671","ID","_RATE_PER_NIGHT","Rate per night");
INSERT INTO phpn_vocabulary VALUES("5672","ID","_RATE_PER_NIGHT_AVG","Average rate per night");
INSERT INTO phpn_vocabulary VALUES("5673","ID","_RATINGS","Ratings");
INSERT INTO phpn_vocabulary VALUES("720","en","_CONTACTUS_DEFAULT_EMAIL_ALERT","You have to change default email address for Contact Us module. Click <a href=\'index.php?admin=mod_contact_us_settings\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("5669","ID","_QUESTIONS","Questions");
INSERT INTO phpn_vocabulary VALUES("723","en","_CONTACT_INFORMATION","Contact Information");
INSERT INTO phpn_vocabulary VALUES("5668","ID","_QUESTION","Question");
INSERT INTO phpn_vocabulary VALUES("726","en","_CONTACT_US","Contact us");
INSERT INTO phpn_vocabulary VALUES("5665","ID","_PUBLISH_YOUR_COMMENT","Publish your comment");
INSERT INTO phpn_vocabulary VALUES("5666","ID","_QTY","Qty");
INSERT INTO phpn_vocabulary VALUES("5667","ID","_QUANTITY","Quantity");
INSERT INTO phpn_vocabulary VALUES("729","en","_CONTACT_US_ALREADY_SENT","Your message has been already sent. Please try again later or wait _WAIT_ seconds.");
INSERT INTO phpn_vocabulary VALUES("5663","ID","_PUBLIC","Public");
INSERT INTO phpn_vocabulary VALUES("5664","ID","_PUBLISHED","Published");
INSERT INTO phpn_vocabulary VALUES("732","en","_CONTACT_US_EMAIL_SENT","Thank you for contacting us! Your message has been successfully sent.");
INSERT INTO phpn_vocabulary VALUES("735","en","_CONTACT_US_SETTINGS","Contact Us Settings");
INSERT INTO phpn_vocabulary VALUES("5662","ID","_PROMO_COUPON_NOTICE","If you have a promo code or discount coupon please enter it here");
INSERT INTO phpn_vocabulary VALUES("738","en","_CONTENT_TYPE","Content Type");
INSERT INTO phpn_vocabulary VALUES("741","en","_CONTINUE_RESERVATION","Continue Reservation");
INSERT INTO phpn_vocabulary VALUES("5661","ID","_PROMO_CODE_OR_COUPON","Promo Code or Discount Coupon");
INSERT INTO phpn_vocabulary VALUES("744","en","_COPY_TO_OTHERS","Copy to others");
INSERT INTO phpn_vocabulary VALUES("747","en","_COPY_TO_OTHER_LANGS","Copy to other languages");
INSERT INTO phpn_vocabulary VALUES("750","en","_COUNT","Count");
INSERT INTO phpn_vocabulary VALUES("5660","ID","_PROMO_AND_DISCOUNTS","Promo and Discounts");
INSERT INTO phpn_vocabulary VALUES("753","en","_COUNTRIES","Countries");
INSERT INTO phpn_vocabulary VALUES("5659","ID","_PRODUCT_DESCRIPTION","Product Description");
INSERT INTO phpn_vocabulary VALUES("756","en","_COUNTRIES_MANAGEMENT","Countries Management");
INSERT INTO phpn_vocabulary VALUES("759","en","_COUNTRY","Country");
INSERT INTO phpn_vocabulary VALUES("5658","ID","_PRODUCTS_MANAGEMENT","Products Management");
INSERT INTO phpn_vocabulary VALUES("762","en","_COUNTRY_EMPTY_ALERT","Country cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5657","ID","_PRODUCTS_COUNT","Products count");
INSERT INTO phpn_vocabulary VALUES("765","en","_COUPONS","Coupons");
INSERT INTO phpn_vocabulary VALUES("5656","ID","_PRODUCTS","Products");
INSERT INTO phpn_vocabulary VALUES("768","en","_COUPONS_MANAGEMENT","Coupons Management");
INSERT INTO phpn_vocabulary VALUES("5655","ID","_PRODUCT","Product");
INSERT INTO phpn_vocabulary VALUES("771","en","_COUPON_CODE","Coupon Code");
INSERT INTO phpn_vocabulary VALUES("5654","ID","_PRIVILEGES_MANAGEMENT","Privileges Management");
INSERT INTO phpn_vocabulary VALUES("774","en","_COUPON_WAS_APPLIED","The coupon _COUPON_CODE_ has been successfully applied!");
INSERT INTO phpn_vocabulary VALUES("5653","ID","_PRIVILEGES","Privileges");
INSERT INTO phpn_vocabulary VALUES("777","en","_COUPON_WAS_REMOVED","The coupon has been successfully removed!");
INSERT INTO phpn_vocabulary VALUES("5652","ID","_PRINT","Print");
INSERT INTO phpn_vocabulary VALUES("780","en","_CREATED_DATE","Date Created");
INSERT INTO phpn_vocabulary VALUES("5651","ID","_PRICE_L_H","price (from lowest)");
INSERT INTO phpn_vocabulary VALUES("783","en","_CREATE_ACCOUNT","Create account");
INSERT INTO phpn_vocabulary VALUES("786","en","_CREATE_ACCOUNT_NOTE","NOTE: <br>We recommend that your password should be at least 6 characters long and should be different from your username.<br><br>Your e-mail address must be valid. We use e-mail for communication purposes (order notifications, etc). Therefore, it is essential to provide a valid e-mail address to be able to use our services correctly.<br><br>All your private data is confidential. We will never sell, exchange or market it in any way. For further information on the responsibilities of both parts, you may refer to us.");
INSERT INTO phpn_vocabulary VALUES("5648","ID","_PRICE_FORMAT","Price Format");
INSERT INTO phpn_vocabulary VALUES("5649","ID","_PRICE_FORMAT_ALERT","Allows to display prices for visitor in appropriate format");
INSERT INTO phpn_vocabulary VALUES("5650","ID","_PRICE_H_L","price (from highest)");
INSERT INTO phpn_vocabulary VALUES("789","en","_CREATING_ACCOUNT_ERROR","An error occurred while creating your account! Please try again later or send information about this error to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("792","en","_CREATING_NEW_ACCOUNT","Creating new account");
INSERT INTO phpn_vocabulary VALUES("795","en","_CREDIT_CARD","Credit Card");
INSERT INTO phpn_vocabulary VALUES("5647","ID","_PRICE_EMPTY_ALERT","Field price cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("798","en","_CREDIT_CARD_EXPIRES","Expires");
INSERT INTO phpn_vocabulary VALUES("5646","ID","_PRICES","Prices");
INSERT INTO phpn_vocabulary VALUES("801","en","_CREDIT_CARD_HOLDER_NAME","Card Holder\'s Name");
INSERT INTO phpn_vocabulary VALUES("5645","ID","_PRICE","Price");
INSERT INTO phpn_vocabulary VALUES("804","en","_CREDIT_CARD_NUMBER","Credit Card Number");
INSERT INTO phpn_vocabulary VALUES("5644","ID","_PRE_PAYMENT","Pre-Payment");
INSERT INTO phpn_vocabulary VALUES("807","en","_CREDIT_CARD_TYPE","Credit Card Type");
INSERT INTO phpn_vocabulary VALUES("5642","ID","_PREVIEW","Preview");
INSERT INTO phpn_vocabulary VALUES("5643","ID","_PREVIOUS","Previous");
INSERT INTO phpn_vocabulary VALUES("810","en","_CRONJOB_HTACCESS_BLOCK","To block remote access to cron.php, in the server&#039;s .htaccess file or vhost configuration file add this section:");
INSERT INTO phpn_vocabulary VALUES("5639","ID","_PREBOOKING","Pre-Booking");
INSERT INTO phpn_vocabulary VALUES("5640","ID","_PREDEFINED_CONSTANTS","Predefined Constants");
INSERT INTO phpn_vocabulary VALUES("5641","ID","_PREFERRED_LANGUAGE","Preferred Language");
INSERT INTO phpn_vocabulary VALUES("813","en","_CRONJOB_NOTICE","Cron jobs allow you to automate certain commands or scripts on your site.<br /><br />uHotelBooking needs to periodically run cron.php to close expired discount campaigns or perform another important operations. The recommended way to run cron.php is to set up a cronjob if you run a Unix/Linux server. If for any reason you can&#039;t run a cronjob on your server, you can choose the Non-batch option below to have cron.php run by uHotelBooking itself: in this case cron.php will be run each time someone access your home page. <br /><br />Example of Batch Cron job command: <b>php &#36;HOME/public_html/cron.php >/dev/null 2>&1</b>");
INSERT INTO phpn_vocabulary VALUES("5638","ID","_POST_COM_REGISTERED_ALERT","Your need to be registered to post comments.");
INSERT INTO phpn_vocabulary VALUES("816","en","_CRON_JOBS","Cron Jobs");
INSERT INTO phpn_vocabulary VALUES("5637","ID","_POSTED_ON","Posted on");
INSERT INTO phpn_vocabulary VALUES("819","en","_CURRENCIES","Currencies");
INSERT INTO phpn_vocabulary VALUES("5632","ID","_PLACEMENT","Placement");
INSERT INTO phpn_vocabulary VALUES("5633","ID","_PLACE_ORDER","Place Order");
INSERT INTO phpn_vocabulary VALUES("5634","ID","_PLAY","Play");
INSERT INTO phpn_vocabulary VALUES("5635","ID","_POPULARITY","Popularity");
INSERT INTO phpn_vocabulary VALUES("5636","ID","_POPULAR_SEARCH","Popular Search");
INSERT INTO phpn_vocabulary VALUES("822","en","_CURRENCIES_DEFAULT_ALERT","Remember! After you change the default currency:<br>- Edit exchange rate to each currency manually (relatively to the new default currency)<br>- Redefine prices for all rooms in the new currency.");
INSERT INTO phpn_vocabulary VALUES("825","en","_CURRENCIES_MANAGEMENT","Currencies Management");
INSERT INTO phpn_vocabulary VALUES("5631","ID","_PICK_DATE","Open calendar and pick a date");
INSERT INTO phpn_vocabulary VALUES("828","en","_CURRENCY","Currency");
INSERT INTO phpn_vocabulary VALUES("831","en","_CURRENT_NEXT_YEARS","for current/next years");
INSERT INTO phpn_vocabulary VALUES("834","en","_CUSTOMER","Customer");
INSERT INTO phpn_vocabulary VALUES("5630","ID","_PHONE_EMPTY_ALERT","Phone field cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("837","en","_CUSTOMERS","Customers");
INSERT INTO phpn_vocabulary VALUES("5627","ID","_PERSON_PER_NIGHT","Person/Per Night");
INSERT INTO phpn_vocabulary VALUES("5628","ID","_PER_NIGHT","Per Night");
INSERT INTO phpn_vocabulary VALUES("5629","ID","_PHONE","Phone");
INSERT INTO phpn_vocabulary VALUES("840","en","_CUSTOMERS_AWAITING_MODERATION_ALERT","There are _COUNT_ customer/s awaiting your approval. Click <a href=\'index.php?admin=mod_customers_management\'>here</a> for review.");
INSERT INTO phpn_vocabulary VALUES("5626","ID","_PERSONAL_INFORMATION","Personal Information");
INSERT INTO phpn_vocabulary VALUES("843","en","_CUSTOMERS_MANAGEMENT","Customers Management");
INSERT INTO phpn_vocabulary VALUES("5625","ID","_PERSONAL_DETAILS","Personal Details");
INSERT INTO phpn_vocabulary VALUES("846","en","_CUSTOMERS_SETTINGS","Customers Settings");
INSERT INTO phpn_vocabulary VALUES("849","en","_CUSTOMER_DETAILS","Customer Details");
INSERT INTO phpn_vocabulary VALUES("5624","ID","_PERSONAL_DATA_SAVED","Your billing information has been updated.");
INSERT INTO phpn_vocabulary VALUES("852","en","_CUSTOMER_GROUP","Customer Group");
INSERT INTO phpn_vocabulary VALUES("5623","ID","_PERIODS","Periods");
INSERT INTO phpn_vocabulary VALUES("855","en","_CUSTOMER_GROUPS","Customer Groups");
INSERT INTO phpn_vocabulary VALUES("858","en","_CUSTOMER_LOGIN","Customer Login");
INSERT INTO phpn_vocabulary VALUES("861","en","_CUSTOMER_NAME","Customer Name");
INSERT INTO phpn_vocabulary VALUES("5622","ID","_PERFORM_OPERATION_COMMON_ALERT","Are you sure you want to perform this operation?");
INSERT INTO phpn_vocabulary VALUES("864","en","_CUSTOMER_PANEL","Customer Panel");
INSERT INTO phpn_vocabulary VALUES("867","en","_CUSTOMER_PAYMENT_MODULES","Customer & Payment Modules");
INSERT INTO phpn_vocabulary VALUES("870","en","_CVV_CODE","CVV Code");
INSERT INTO phpn_vocabulary VALUES("873","en","_DASHBOARD","Dashboard");
INSERT INTO phpn_vocabulary VALUES("5621","ID","_PERCENTAGE_MAX_ALOWED_VALUE","The maximum allowed value for percentage is 99%! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("876","en","_DATE","Date");
INSERT INTO phpn_vocabulary VALUES("5620","ID","_PEOPLE_STAYING","People Staying");
INSERT INTO phpn_vocabulary VALUES("879","en","_DATETIME_PRICE_FORMAT","Datetime & Price Settings");
INSERT INTO phpn_vocabulary VALUES("5619","ID","_PEOPLE_DEPARTING","People Departing");
INSERT INTO phpn_vocabulary VALUES("882","en","_DATE_AND_TIME_SETTINGS","Date & Time Settings");
INSERT INTO phpn_vocabulary VALUES("5618","ID","_PEOPLE_ARRIVING","People Arriving");
INSERT INTO phpn_vocabulary VALUES("885","en","_DATE_CREATED","Date Created");
INSERT INTO phpn_vocabulary VALUES("5617","ID","_PENDING","Pending");
INSERT INTO phpn_vocabulary VALUES("888","en","_DATE_EMPTY_ALERT","Date fields cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5616","ID","_PC_YEAR_TEXT","current year in YYYY format");
INSERT INTO phpn_vocabulary VALUES("891","en","_DATE_FORMAT","Date Format");
INSERT INTO phpn_vocabulary VALUES("894","en","_DATE_MODIFIED","Date Modified");
INSERT INTO phpn_vocabulary VALUES("5615","ID","_PC_WEB_SITE_URL_TEXT","web site url");
INSERT INTO phpn_vocabulary VALUES("897","en","_DATE_PAYMENT","Date of Payment");
INSERT INTO phpn_vocabulary VALUES("900","en","_DATE_PUBLISHED","Date Published");
INSERT INTO phpn_vocabulary VALUES("903","en","_DATE_SUBSCRIBED","Date Subscribed");
INSERT INTO phpn_vocabulary VALUES("5614","ID","_PC_WEB_SITE_BASED_URL_TEXT","web site base url");
INSERT INTO phpn_vocabulary VALUES("906","en","_DAY","Day");
INSERT INTO phpn_vocabulary VALUES("909","en","_DECEMBER","December");
INSERT INTO phpn_vocabulary VALUES("912","en","_DECIMALS","Decimals");
INSERT INTO phpn_vocabulary VALUES("5613","ID","_PC_USER_PASSWORD_TEXT","password for customer or admin");
INSERT INTO phpn_vocabulary VALUES("915","en","_DEFAULT","Default");
INSERT INTO phpn_vocabulary VALUES("5612","ID","_PC_USER_NAME_TEXT","username (login) of user");
INSERT INTO phpn_vocabulary VALUES("918","en","_DEFAULT_AVAILABILITY","Default Availability");
INSERT INTO phpn_vocabulary VALUES("5611","ID","_PC_USER_EMAIL_TEXT","email of user");
INSERT INTO phpn_vocabulary VALUES("921","en","_DEFAULT_CURRENCY_DELETE_ALERT","You cannot delete default currency!");
INSERT INTO phpn_vocabulary VALUES("5609","ID","_PC_REGISTRATION_CODE_TEXT","confirmation code for new account");
INSERT INTO phpn_vocabulary VALUES("5610","ID","_PC_STATUS_DESCRIPTION_TEXT","description of payment status");
INSERT INTO phpn_vocabulary VALUES("924","en","_DEFAULT_EMAIL_ALERT","You have to change default email address for site administrator. Click <a href=\'index.php?admin=settings&tabid=1_4\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("5608","ID","_PC_PERSONAL_INFORMATION_TEXT","personal information of customer: first name, last name etc.");
INSERT INTO phpn_vocabulary VALUES("927","en","_DEFAULT_HOTEL_DELETE_ALERT","You cannot delete default hotel!");
INSERT INTO phpn_vocabulary VALUES("5607","ID","_PC_LAST_NAME_TEXT","the last name of customer or admin");
INSERT INTO phpn_vocabulary VALUES("930","en","_DEFAULT_OWN_EMAIL_ALERT","You have to change your own email address. Click <a href=\'index.php?admin=my_account\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("5604","ID","_PC_EVENT_TEXT","the title of event");
INSERT INTO phpn_vocabulary VALUES("5605","ID","_PC_FIRST_NAME_TEXT","the first name of customer or admin");
INSERT INTO phpn_vocabulary VALUES("5606","ID","_PC_HOTEL_INFO_TEXT","information about hotel: name, address, telephone, fax etc.");
INSERT INTO phpn_vocabulary VALUES("933","en","_DEFAULT_PERIODS_ALERT","Default Periods are used to specify periods of time that could by easily fulfilled with default prices for each room on the Room Prices page (with just a single click).");
INSERT INTO phpn_vocabulary VALUES("5603","ID","_PC_BOOKING_NUMBER_TEXT","the number of order");
INSERT INTO phpn_vocabulary VALUES("936","en","_DEFAULT_PERIODS_WERE_ADDED","Default periods have been successfully added!");
INSERT INTO phpn_vocabulary VALUES("939","en","_DEFAULT_PRICE","Default Price");
INSERT INTO phpn_vocabulary VALUES("942","en","_DEFAULT_TEMPLATE","Default Template");
INSERT INTO phpn_vocabulary VALUES("5602","ID","_PC_BOOKING_DETAILS_TEXT","order details, list of purchased products etc.");
INSERT INTO phpn_vocabulary VALUES("945","en","_DEFINE","Define");
INSERT INTO phpn_vocabulary VALUES("5601","ID","_PC_BILLING_INFORMATION_TEXT","billing information: address, city, country etc.");
INSERT INTO phpn_vocabulary VALUES("948","en","_DELETE_WARNING","Are you sure you want to delete this record?");
INSERT INTO phpn_vocabulary VALUES("951","en","_DELETE_WARNING_COMMON","Are you sure you want to delete this record?");
INSERT INTO phpn_vocabulary VALUES("5600","ID","_PAY_ON_ARRIVAL","Pay on Arrival");
INSERT INTO phpn_vocabulary VALUES("954","en","_DELETE_WORD","Delete");
INSERT INTO phpn_vocabulary VALUES("5599","ID","_PAYPAL_ORDER","PayPal Order");
INSERT INTO phpn_vocabulary VALUES("957","en","_DELETING_ACCOUNT_ERROR","An error occurred while deleting your account! Please try again later or send email about this issue to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("5598","ID","_PAYPAL_NOTICE","Save time. Pay securely using your stored payment information.<br />Pay with <b>credit card</b>, <b>bank account</b> or <b>PayPal</b> account balance.");
INSERT INTO phpn_vocabulary VALUES("960","en","_DELETING_OPERATION_COMPLETED","Deleting operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("5597","ID","_PAYPAL","PayPal");
INSERT INTO phpn_vocabulary VALUES("963","en","_DESCRIPTION","Description");
INSERT INTO phpn_vocabulary VALUES("966","en","_DISABLED","disabled");
INSERT INTO phpn_vocabulary VALUES("5596","ID","_PAYMENT_TYPE","Payment Type");
INSERT INTO phpn_vocabulary VALUES("969","en","_DISCOUNT","Discount");
INSERT INTO phpn_vocabulary VALUES("5595","ID","_PAYMENT_SUM","Payment Sum");
INSERT INTO phpn_vocabulary VALUES("972","en","_DISCOUNT_BY_ADMIN","Discount By Administrator");
INSERT INTO phpn_vocabulary VALUES("5594","ID","_PAYMENT_REQUIRED","Payment Required");
INSERT INTO phpn_vocabulary VALUES("975","en","_DISCOUNT_CAMPAIGN","Discount Campaign");
INSERT INTO phpn_vocabulary VALUES("978","en","_DISCOUNT_CAMPAIGNS","Discount Campaigns");
INSERT INTO phpn_vocabulary VALUES("5590","ID","_PAYMENT_DETAILS","Payment Details");
INSERT INTO phpn_vocabulary VALUES("5591","ID","_PAYMENT_ERROR","Payment error");
INSERT INTO phpn_vocabulary VALUES("5592","ID","_PAYMENT_METHOD","Payment Method");
INSERT INTO phpn_vocabulary VALUES("5593","ID","_PAYMENT_METHODS","Payment Methods");
INSERT INTO phpn_vocabulary VALUES("981","en","_DISCOUNT_CAMPAIGN_TEXT","<span class=campaign_header>Super discount campaign!</span><br /><br />\nEnjoy special price cuts right now<br />_FROM_ _TO_:<br /> \n<b>_PERCENT_</b> on every room reservation in our Hotel!");
INSERT INTO phpn_vocabulary VALUES("5586","ID","_PAYMENT","Payment");
INSERT INTO phpn_vocabulary VALUES("5587","ID","_PAYMENTS","Payments");
INSERT INTO phpn_vocabulary VALUES("5588","ID","_PAYMENT_COMPANY_ACCOUNT","Payment Company Account");
INSERT INTO phpn_vocabulary VALUES("5589","ID","_PAYMENT_DATE","Payment Date");
INSERT INTO phpn_vocabulary VALUES("984","en","_DISCOUNT_STD_CAMPAIGN_TEXT","<span class=campaign_header>Super discount campaign!</span><br><br>Enjoy special price cuts in our Hotel for the specified periods of time below!");
INSERT INTO phpn_vocabulary VALUES("5585","ID","_PAYED_BY","Payed by");
INSERT INTO phpn_vocabulary VALUES("987","en","_DISPLAY_ON","Display on");
INSERT INTO phpn_vocabulary VALUES("990","en","_DOWN","Down");
INSERT INTO phpn_vocabulary VALUES("993","en","_DOWNLOAD","Download");
INSERT INTO phpn_vocabulary VALUES("996","en","_DOWNLOAD_INVOICE","Download Invoice");
INSERT INTO phpn_vocabulary VALUES("5584","ID","_PAST_TIME_ALERT","You cannot perform reservation in the past! Please re-enter dates.");
INSERT INTO phpn_vocabulary VALUES("999","en","_ECHECK","E-Check");
INSERT INTO phpn_vocabulary VALUES("1002","en","_EDIT_MENUS","Edit Menus");
INSERT INTO phpn_vocabulary VALUES("1005","en","_EDIT_MY_ACCOUNT","Edit My Account");
INSERT INTO phpn_vocabulary VALUES("5583","ID","_PASSWORD_SUCCESSFULLY_SENT","Your password has been successfully sent to the email address.");
INSERT INTO phpn_vocabulary VALUES("1008","en","_EDIT_PAGE","Edit Page");
INSERT INTO phpn_vocabulary VALUES("1011","en","_EDIT_WORD","Edit");
INSERT INTO phpn_vocabulary VALUES("1014","en","_EMAIL","Email");
INSERT INTO phpn_vocabulary VALUES("5582","ID","_PASSWORD_RECOVERY_MSG","To recover your password, please enter your e-mail address and a link will be emailed to you.");
INSERT INTO phpn_vocabulary VALUES("1017","en","_EMAILS_SENT_ERROR","An error occurred while sending emails or there are no emails to be sent! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5581","ID","_PASSWORD_NOT_CHANGED","Password has not been changed. Please try again!");
INSERT INTO phpn_vocabulary VALUES("1020","en","_EMAILS_SUCCESSFULLY_SENT","Status: _SENT_ emails from _TOTAL_ were successfully sent!");
INSERT INTO phpn_vocabulary VALUES("1023","en","_EMAIL_ADDRESS","E-mail address");
INSERT INTO phpn_vocabulary VALUES("5580","ID","_PASSWORD_IS_EMPTY","Passwords must not be empty and at least 6 characters!");
INSERT INTO phpn_vocabulary VALUES("1026","en","_EMAIL_BLOCKED","Your email is blocked! To resolve this problem, please contact the site administrator.");
INSERT INTO phpn_vocabulary VALUES("1029","en","_EMAIL_EMPTY_ALERT","Email cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1032","en","_EMAIL_FROM","Email Address (From)");
INSERT INTO phpn_vocabulary VALUES("1035","en","_EMAIL_IS_EMPTY","Email must not be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5579","ID","_PASSWORD_FORGOTTEN_PAGE_MSG","Use a valid administrator e-mail to restore your password to the Administrator Back-End.<br><br>Return to site <a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/password.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("1038","en","_EMAIL_IS_WRONG","Please enter a valid email address.");
INSERT INTO phpn_vocabulary VALUES("5578","ID","_PASSWORD_FORGOTTEN","Forgotten Password");
INSERT INTO phpn_vocabulary VALUES("1041","en","_EMAIL_NOTIFICATIONS","Send email notifications");
INSERT INTO phpn_vocabulary VALUES("5577","ID","_PASSWORD_DO_NOT_MATCH","Password and confirmation do not match!");
INSERT INTO phpn_vocabulary VALUES("1044","en","_EMAIL_NOT_EXISTS","This e-mail account does not exist in the system! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5575","ID","_PASSWORD_ALREADY_SENT","Password has been already sent to your email. Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5576","ID","_PASSWORD_CHANGED","Password has been changed.");
INSERT INTO phpn_vocabulary VALUES("1047","en","_EMAIL_SEND_ERROR","An error occurred while sending email. Please check your email settings and message recipients, then try again.");
INSERT INTO phpn_vocabulary VALUES("5574","ID","_PASSWORD","Password");
INSERT INTO phpn_vocabulary VALUES("1050","en","_EMAIL_SETTINGS","Email Settings");
INSERT INTO phpn_vocabulary VALUES("5573","ID","_PARTIAL_PRICE","Partial Price");
INSERT INTO phpn_vocabulary VALUES("1053","en","_EMAIL_SUCCESSFULLY_SENT","Email has been successfully sent!");
INSERT INTO phpn_vocabulary VALUES("1056","en","_EMAIL_TEMPLATES","Email Templates");
INSERT INTO phpn_vocabulary VALUES("5572","ID","_PARTIALLY_AVAILABLE","Partially Available");
INSERT INTO phpn_vocabulary VALUES("1059","en","_EMAIL_TEMPLATES_EDITOR","Email Templates Editor");
INSERT INTO phpn_vocabulary VALUES("5571","ID","_PARAMETER","Parameter");
INSERT INTO phpn_vocabulary VALUES("1062","en","_EMAIL_TO","Email Address (To)");
INSERT INTO phpn_vocabulary VALUES("5570","ID","_PAGE_UNKNOWN","Unknown page!");
INSERT INTO phpn_vocabulary VALUES("1065","en","_EMAIL_VALID_ALERT","Please enter a valid email address!");
INSERT INTO phpn_vocabulary VALUES("5569","ID","_PAGE_TITLE","Page Title");
INSERT INTO phpn_vocabulary VALUES("1068","en","_EMPTY","Empty");
INSERT INTO phpn_vocabulary VALUES("5568","ID","_PAGE_TEXT","Page text");
INSERT INTO phpn_vocabulary VALUES("1071","en","_ENTER_BOOKING_NUMBER","Enter your booking number");
INSERT INTO phpn_vocabulary VALUES("5567","ID","_PAGE_SAVED","Page has been successfully saved!");
INSERT INTO phpn_vocabulary VALUES("1074","en","_ENTER_CONFIRMATION_CODE","Enter Confirmation Code");
INSERT INTO phpn_vocabulary VALUES("5566","ID","_PAGE_RESTORE_WARNING","Are you sure you want to restore this page?");
INSERT INTO phpn_vocabulary VALUES("1077","en","_ENTER_EMAIL_ADDRESS","(Please enter ONLY real email address)");
INSERT INTO phpn_vocabulary VALUES("1080","en","_ENTIRE_SITE","Entire Site");
INSERT INTO phpn_vocabulary VALUES("1083","en","_EVENTS","Events");
INSERT INTO phpn_vocabulary VALUES("5564","ID","_PAGE_REMOVE_WARNING","Are you sure you want to move this page to the Trash?");
INSERT INTO phpn_vocabulary VALUES("5565","ID","_PAGE_RESTORED","Page has been successfully restored!");
INSERT INTO phpn_vocabulary VALUES("1086","en","_EVENT_REGISTRATION_COMPLETED","Thank you for your interest! You have just successfully registered to this event.");
INSERT INTO phpn_vocabulary VALUES("5563","ID","_PAGE_REMOVED","Page has been successfully removed!");
INSERT INTO phpn_vocabulary VALUES("1089","en","_EVENT_USER_ALREADY_REGISTERED","Member with such email is already registered to this event! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5562","ID","_PAGE_ORDER_CHANGED","Page order has been successfully changed!");
INSERT INTO phpn_vocabulary VALUES("1092","en","_EXPAND_PANEL","Expand navigation panel");
INSERT INTO phpn_vocabulary VALUES("1095","en","_EXPIRED","Expired");
INSERT INTO phpn_vocabulary VALUES("1098","en","_EXPORT","Export");
INSERT INTO phpn_vocabulary VALUES("5561","ID","_PAGE_NOT_SAVED","Page has not been saved!");
INSERT INTO phpn_vocabulary VALUES("1101","en","_EXTRAS","Extras");
INSERT INTO phpn_vocabulary VALUES("5560","ID","_PAGE_NOT_FOUND","No Pages Found");
INSERT INTO phpn_vocabulary VALUES("1104","en","_EXTRAS_MANAGEMENT","Extras Management");
INSERT INTO phpn_vocabulary VALUES("1107","en","_EXTRAS_SUBTOTAL","Extras Subtotal");
INSERT INTO phpn_vocabulary VALUES("1110","en","_EXTRA_BED","Extra Bed");
INSERT INTO phpn_vocabulary VALUES("5559","ID","_PAGE_NOT_EXISTS","The page you attempted to access does not exist");
INSERT INTO phpn_vocabulary VALUES("1113","en","_EXTRA_BEDS","Extra Beds");
INSERT INTO phpn_vocabulary VALUES("1116","en","_EXTRA_BED_CHARGE","Extra Bed Charge");
INSERT INTO phpn_vocabulary VALUES("5558","ID","_PAGE_NOT_DELETED","Page has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("1119","en","_FACILITIES","Facilities");
INSERT INTO phpn_vocabulary VALUES("1122","en","_FAQ","FAQ");
INSERT INTO phpn_vocabulary VALUES("5557","ID","_PAGE_NOT_CREATED","Page has not been created!");
INSERT INTO phpn_vocabulary VALUES("1125","en","_FAQ_MANAGEMENT","FAQ Management");
INSERT INTO phpn_vocabulary VALUES("1128","en","_FAQ_SETTINGS","FAQ Settings");
INSERT INTO phpn_vocabulary VALUES("1131","en","_FAX","Fax");
INSERT INTO phpn_vocabulary VALUES("5556","ID","_PAGE_MANAGEMENT","Pages Management");
INSERT INTO phpn_vocabulary VALUES("1134","en","_FEBRUARY","February");
INSERT INTO phpn_vocabulary VALUES("5555","ID","_PAGE_LINK_TOO_LONG","Menu link too long!");
INSERT INTO phpn_vocabulary VALUES("1137","en","_FIELD_CANNOT_BE_EMPTY","Field _FIELD_ cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5553","ID","_PAGE_HEADER_EMPTY","Page header cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5554","ID","_PAGE_KEY_EMPTY","Page key cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("1140","en","_FIELD_LENGTH_ALERT","The length of the field _FIELD_ must be less than _LENGTH_ characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5551","ID","_PAGE_EXPIRED","The page you requested has expired!");
INSERT INTO phpn_vocabulary VALUES("5552","ID","_PAGE_HEADER","Page Header");
INSERT INTO phpn_vocabulary VALUES("1143","en","_FIELD_LENGTH_EXCEEDED","_FIELD_ has exceeded the maximum allowed size: _LENGTH_ characters! Please re-enter. ");
INSERT INTO phpn_vocabulary VALUES("5549","ID","_PAGE_EDIT_PAGES","Edit Pages");
INSERT INTO phpn_vocabulary VALUES("5550","ID","_PAGE_EDIT_SYS_PAGES","Edit System Pages");
INSERT INTO phpn_vocabulary VALUES("1146","en","_FIELD_MIN_LENGTH_ALERT","The length of the field _FIELD_ cannot  be less than _LENGTH_ characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5548","ID","_PAGE_EDIT_HOME","Edit Home Page");
INSERT INTO phpn_vocabulary VALUES("1149","en","_FIELD_MUST_BE_ALPHA","_FIELD_ must be an alphabetic value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5547","ID","_PAGE_DELETE_WARNING","Are you sure you want to delete this page?");
INSERT INTO phpn_vocabulary VALUES("1152","en","_FIELD_MUST_BE_ALPHA_NUMERIC","_FIELD_ must be an alphanumeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5546","ID","_PAGE_DELETED","Page has been successfully deleted");
INSERT INTO phpn_vocabulary VALUES("1155","en","_FIELD_MUST_BE_BOOLEAN","Field _FIELD_ value must be \'yes\' or \'no\'! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5544","ID","_PAGE_ADD_NEW","Add New Page");
INSERT INTO phpn_vocabulary VALUES("5545","ID","_PAGE_CREATED","Page has been successfully created");
INSERT INTO phpn_vocabulary VALUES("1158","en","_FIELD_MUST_BE_EMAIL","_FIELD_ must be in valid email format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5542","ID","_PAGE","Page");
INSERT INTO phpn_vocabulary VALUES("5543","ID","_PAGES","Pages");
INSERT INTO phpn_vocabulary VALUES("1161","en","_FIELD_MUST_BE_FLOAT","Field _FIELD_ must be a float number value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5540","ID","_PACKAGES","Packages");
INSERT INTO phpn_vocabulary VALUES("5541","ID","_PACKAGES_MANAGEMENT","Packages Management");
INSERT INTO phpn_vocabulary VALUES("1164","en","_FIELD_MUST_BE_FLOAT_POSITIVE","Field _FIELD_ must be a positive float number value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5539","ID","_OWNER_NOT_ASSIGNED","You still has not been assigned to any hotel to see the reports.");
INSERT INTO phpn_vocabulary VALUES("1168","en","_FIELD_MUST_BE_IP_ADDRESS","_FIELD_ must be a valid IP Address! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5537","ID","_OUR_LOCATION","Our location");
INSERT INTO phpn_vocabulary VALUES("5538","ID","_OWNER","Owner");
INSERT INTO phpn_vocabulary VALUES("1171","en","_FIELD_MUST_BE_NUMERIC","Field _FIELD_ must be a numeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5535","ID","_ORDER_PRICE","Order Price");
INSERT INTO phpn_vocabulary VALUES("5536","ID","_OTHER","Other");
INSERT INTO phpn_vocabulary VALUES("1174","en","_FIELD_MUST_BE_NUMERIC_POSITIVE","Field _FIELD_ must be a positive numeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5534","ID","_ORDER_PLACED_MSG","Thank you! The order has been placed in our system and will be processed shortly. Your booking number is: _BOOKING_NUMBER_.");
INSERT INTO phpn_vocabulary VALUES("1177","en","_FIELD_MUST_BE_PASSWORD","_FIELD_ must be 6 characters at least and consist of letters and digits! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5533","ID","_ORDER_NOW","Order Now");
INSERT INTO phpn_vocabulary VALUES("1180","en","_FIELD_MUST_BE_POSITIVE_INT","Field _FIELD_ must be a positive integer value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5531","ID","_ORDER_DATE","Order Date");
INSERT INTO phpn_vocabulary VALUES("5532","ID","_ORDER_ERROR","Cannot complete your order! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("1183","en","_FIELD_MUST_BE_POSITIVE_INTEGER","Field _FIELD_ must be a positive integer number!");
INSERT INTO phpn_vocabulary VALUES("5527","ID","_OR","or");
INSERT INTO phpn_vocabulary VALUES("5528","ID","_ORDER","Order");
INSERT INTO phpn_vocabulary VALUES("5529","ID","_ORDERS","Orders");
INSERT INTO phpn_vocabulary VALUES("5530","ID","_ORDERS_COUNT","Orders count");
INSERT INTO phpn_vocabulary VALUES("1185","en","_FIELD_MUST_BE_SIZE_VALUE","Field _FIELD_ must be a valid HTML size property in \'px\', \'pt\', \'em\' or \'%\' units! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5526","ID","_OPERATION_WAS_ALREADY_COMPLETED","This operation has been already completed!");
INSERT INTO phpn_vocabulary VALUES("1188","en","_FIELD_MUST_BE_TEXT","_FIELD_ value must be a text! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5525","ID","_OPERATION_COMMON_COMPLETED","The operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("1191","en","_FIELD_MUST_BE_UNSIGNED_FLOAT","Field _FIELD_ must be an unsigned float value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5524","ID","_OPERATION_BLOCKED","This operation is blocked in Demo Version!");
INSERT INTO phpn_vocabulary VALUES("1194","en","_FIELD_MUST_BE_UNSIGNED_INT","Field _FIELD_ must be an unsigned integer value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5520","ID","_ONLINE_ORDER","On-line Order");
INSERT INTO phpn_vocabulary VALUES("5521","ID","_ONLY","Only");
INSERT INTO phpn_vocabulary VALUES("5522","ID","_OPEN","Open");
INSERT INTO phpn_vocabulary VALUES("5523","ID","_OPEN_ALERT_WINDOW","Open Alert Window");
INSERT INTO phpn_vocabulary VALUES("1197","en","_FIELD_VALUE_EXCEEDED","_FIELD_ has exceeded the maximum allowed value _MAX_! Please re-enter or change total rooms number <a href=\'index.php?admin=mod_rooms_management\'>here</a>.");
INSERT INTO phpn_vocabulary VALUES("5518","ID","_ON","On");
INSERT INTO phpn_vocabulary VALUES("5519","ID","_ONLINE","Online");
INSERT INTO phpn_vocabulary VALUES("1200","en","_FIELD_VALUE_MINIMUM","_FIELD_ value should not be less then _MIN_! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5517","ID","_OFFLINE_MESSAGE","Offline Message");
INSERT INTO phpn_vocabulary VALUES("1203","en","_FILED_UNIQUE_VALUE_ALERT","The field _FIELD_ accepts only unique values - please re-enter!");
INSERT INTO phpn_vocabulary VALUES("1206","en","_FILE_DELETING_ERROR","An error occurred while deleting file! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5516","ID","_OFFLINE_LOGIN_ALERT","To log into Admin Panel when site is offline, type in your browser: http://{your_site_address}/index.php?admin=login");
INSERT INTO phpn_vocabulary VALUES("1209","en","_FILTER_BY","Filter by");
INSERT INTO phpn_vocabulary VALUES("5515","ID","_OFF","Off");
INSERT INTO phpn_vocabulary VALUES("1212","en","_FINISH_DATE","Finish Date");
INSERT INTO phpn_vocabulary VALUES("5514","ID","_OCTOBER","October");
INSERT INTO phpn_vocabulary VALUES("1215","en","_FINISH_PUBLISHING","Finish Publishing");
INSERT INTO phpn_vocabulary VALUES("5513","ID","_OCCUPANCY","Occupancy");
INSERT INTO phpn_vocabulary VALUES("1218","en","_FIRST_NAME","First Name");
INSERT INTO phpn_vocabulary VALUES("1221","en","_FIRST_NAME_EMPTY_ALERT","First Name cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1224","en","_FIRST_NIGHT","First Night");
INSERT INTO phpn_vocabulary VALUES("5512","ID","_NO_WRITE_ACCESS_ALERT","Please check you have write access to following directories:");
INSERT INTO phpn_vocabulary VALUES("1227","en","_FIXED_SUM","Fixed Sum");
INSERT INTO phpn_vocabulary VALUES("1230","en","_FOLLOW_US","Follow Us");
INSERT INTO phpn_vocabulary VALUES("1233","en","_FOOTER_IS_EMPTY","Footer cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1236","en","_FORCE_SSL","Force SSL");
INSERT INTO phpn_vocabulary VALUES("5509","ID","_NO_ROOMS_FOUND","Sorry, there are no rooms that match your search. Please change your search criteria to see more rooms.");
INSERT INTO phpn_vocabulary VALUES("5510","ID","_NO_TEMPLATE","no template");
INSERT INTO phpn_vocabulary VALUES("5511","ID","_NO_USER_EMAIL_EXISTS_ALERT","It seems that you already booked rooms with us! <br>Please click <a href=index.php?customer=reset_account>here</a> to reset your username and get a temporary password. ");
INSERT INTO phpn_vocabulary VALUES("1239","en","_FORCE_SSL_ALERT","Force site access to always occur under SSL (https) for selected areas. You or site visitors will not be able to access selected areas under non-ssl. Note, you must have SSL enabled on your server to make this option works.");
INSERT INTO phpn_vocabulary VALUES("1242","en","_FORGOT_PASSWORD","Forgot your password?");
INSERT INTO phpn_vocabulary VALUES("5508","ID","_NO_RECORDS_UPDATED","No records were updated!");
INSERT INTO phpn_vocabulary VALUES("1245","en","_FORM","Form");
INSERT INTO phpn_vocabulary VALUES("1248","en","_FOR_BOOKING","for booking #");
INSERT INTO phpn_vocabulary VALUES("5507","ID","_NO_RECORDS_PROCESSED","No records found for processing!");
INSERT INTO phpn_vocabulary VALUES("1251","en","_FOUND_HOTELS","Found Hotels");
INSERT INTO phpn_vocabulary VALUES("1254","en","_FOUND_ROOMS","Found Rooms");
INSERT INTO phpn_vocabulary VALUES("1257","en","_FR","Fr");
INSERT INTO phpn_vocabulary VALUES("5506","ID","_NO_RECORDS_FOUND","No records found");
INSERT INTO phpn_vocabulary VALUES("1260","en","_FRI","Fri");
INSERT INTO phpn_vocabulary VALUES("1263","en","_FRIDAY","Friday");
INSERT INTO phpn_vocabulary VALUES("1266","en","_FROM","From");
INSERT INTO phpn_vocabulary VALUES("5505","ID","_NO_PAYMENT_METHODS_ALERT","No payment options are available! Please try later or contact our technical support.");
INSERT INTO phpn_vocabulary VALUES("1269","en","_FROM_TO_DATE_ALERT","Date \'To\' must be the same or later than date \'From\'! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5504","ID","_NO_NEWS","No news");
INSERT INTO phpn_vocabulary VALUES("1272","en","_FULLY_BOOKED","fully booked/unavailable");
INSERT INTO phpn_vocabulary VALUES("1275","en","_FULL_PRICE","Full Price");
INSERT INTO phpn_vocabulary VALUES("1278","en","_GALLERY","Gallery");
INSERT INTO phpn_vocabulary VALUES("1281","en","_GALLERY_MANAGEMENT","Gallery Management");
INSERT INTO phpn_vocabulary VALUES("5503","ID","_NO_DEFAULT_PERIODS","Default periods not yet defined for this hotel. Click <a href=_HREF_>here</a> to add them.");
INSERT INTO phpn_vocabulary VALUES("1284","en","_GALLERY_SETTINGS","Gallery Settings");
INSERT INTO phpn_vocabulary VALUES("1287","en","_GENERAL","General");
INSERT INTO phpn_vocabulary VALUES("5502","ID","_NO_CUSTOMER_FOUND","No customer found!");
INSERT INTO phpn_vocabulary VALUES("1290","en","_GENERAL_INFO","General Info");
INSERT INTO phpn_vocabulary VALUES("1293","en","_GENERAL_SETTINGS","General Settings");
INSERT INTO phpn_vocabulary VALUES("5501","ID","_NO_COMMENTS_YET","No comments yet.");
INSERT INTO phpn_vocabulary VALUES("1296","en","_GENERATE","Generate");
INSERT INTO phpn_vocabulary VALUES("1299","en","_GLOBAL","Global");
INSERT INTO phpn_vocabulary VALUES("1302","en","_GROUP","Group");
INSERT INTO phpn_vocabulary VALUES("1305","en","_GROUP_NAME","Group Name");
INSERT INTO phpn_vocabulary VALUES("5498","ID","_NOVEMBER","November");
INSERT INTO phpn_vocabulary VALUES("5499","ID","_NO_AVAILABLE","Not Available");
INSERT INTO phpn_vocabulary VALUES("5500","ID","_NO_BOOKING_FOUND","The number of booking you\'ve entered has not been found in our system! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1308","en","_GROUP_TIME_OVERLAPPING_ALERT","This period of time (fully or partially) is already chosen for selected group! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1311","en","_HDR_FOOTER_TEXT","Footer Text");
INSERT INTO phpn_vocabulary VALUES("5497","ID","_NOT_PAID_YET","Not paid yet");
INSERT INTO phpn_vocabulary VALUES("1314","en","_HDR_HEADER_TEXT","Header Text");
INSERT INTO phpn_vocabulary VALUES("5496","ID","_NOT_AVAILABLE","N/A");
INSERT INTO phpn_vocabulary VALUES("1317","en","_HDR_SLOGAN_TEXT","Slogan");
INSERT INTO phpn_vocabulary VALUES("1320","en","_HDR_TEMPLATE","Template");
INSERT INTO phpn_vocabulary VALUES("1323","en","_HDR_TEXT_DIRECTION","Text Direction");
INSERT INTO phpn_vocabulary VALUES("5495","ID","_NOT_AUTHORIZED","You are not authorized to view this page.");
INSERT INTO phpn_vocabulary VALUES("1326","en","_HEADER","Header");
INSERT INTO phpn_vocabulary VALUES("5494","ID","_NOT_ALLOWED","Not Allowed");
INSERT INTO phpn_vocabulary VALUES("1329","en","_HEADERS_AND_FOOTERS","Headers & Footers");
INSERT INTO phpn_vocabulary VALUES("1332","en","_HEADER_IS_EMPTY","Header cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1335","en","_HELP","Help");
INSERT INTO phpn_vocabulary VALUES("5493","ID","_NOTIFICATION_STATUS_CHANGED","Notification status changed");
INSERT INTO phpn_vocabulary VALUES("1338","en","_HIDDEN","Hidden");
INSERT INTO phpn_vocabulary VALUES("1341","en","_HIDE","Hide");
INSERT INTO phpn_vocabulary VALUES("1344","en","_HOME","Home");
INSERT INTO phpn_vocabulary VALUES("1347","en","_HOTEL","Hotel");
INSERT INTO phpn_vocabulary VALUES("5489","ID","_NO","No");
INSERT INTO phpn_vocabulary VALUES("5490","ID","_NONE","None");
INSERT INTO phpn_vocabulary VALUES("5491","ID","_NOTICE_MODULES_CODE","To add available modules to this page just copy and paste into the text:");
INSERT INTO phpn_vocabulary VALUES("5492","ID","_NOTIFICATION_MSG","Please send me information about specials and discounts!");
INSERT INTO phpn_vocabulary VALUES("1350","en","_HOTELOWNER_WELCOME_TEXT","Welcome to Hotel Owner Control Panel! With this Control Panel you can easily manage your hotels, customers, reservations and perform a full hotel site management.");
INSERT INTO phpn_vocabulary VALUES("5488","ID","_NIGHTS","Nights");
INSERT INTO phpn_vocabulary VALUES("1353","en","_HOTELS","Hotels");
INSERT INTO phpn_vocabulary VALUES("5487","ID","_NIGHT","Night");
INSERT INTO phpn_vocabulary VALUES("1356","en","_HOTELS_AND_ROMS","Hotels and Rooms");
INSERT INTO phpn_vocabulary VALUES("5486","ID","_NEXT","Next");
INSERT INTO phpn_vocabulary VALUES("1359","en","_HOTELS_INFO","Hotels Info");
INSERT INTO phpn_vocabulary VALUES("5485","ID","_NEWS_SETTINGS","News Settings");
INSERT INTO phpn_vocabulary VALUES("1362","en","_HOTELS_MANAGEMENT","Hotels Management");
INSERT INTO phpn_vocabulary VALUES("5483","ID","_NEWS_AND_EVENTS","News & Events");
INSERT INTO phpn_vocabulary VALUES("5484","ID","_NEWS_MANAGEMENT","News Management");
INSERT INTO phpn_vocabulary VALUES("1365","en","_HOTEL_DELETE_ALERT","Are you sure you want to delete this hotel? Remember: after completing this action all related data to this hotel could not be restored!");
INSERT INTO phpn_vocabulary VALUES("1368","en","_HOTEL_DESCRIPTION","Hotel Description");
INSERT INTO phpn_vocabulary VALUES("5482","ID","_NEWSLETTER_UNSUBSCRIBE_TEXT","<p>To unsubscribe from our newsletters, enter your email address below and click the unsubscribe button.</p>");
INSERT INTO phpn_vocabulary VALUES("1371","en","_HOTEL_INFO","Hotel Info");
INSERT INTO phpn_vocabulary VALUES("1374","en","_HOTEL_MANAGEMENT","Hotel Management");
INSERT INTO phpn_vocabulary VALUES("1377","en","_HOTEL_OWNER","Hotel Owner");
INSERT INTO phpn_vocabulary VALUES("5481","ID","_NEWSLETTER_UNSUBSCRIBE_SUCCESS","You have been successfully unsubscribed from our newsletter!");
INSERT INTO phpn_vocabulary VALUES("1380","en","_HOTEL_OWNERS","Hotel Owners");
INSERT INTO phpn_vocabulary VALUES("1383","en","_HOTEL_RESERVATION_ID","Hotel Reservation ID");
INSERT INTO phpn_vocabulary VALUES("1386","en","_HOUR","Hour");
INSERT INTO phpn_vocabulary VALUES("1389","en","_HOURS","hours");
INSERT INTO phpn_vocabulary VALUES("1392","en","_ICON_IMAGE","Icon image");
INSERT INTO phpn_vocabulary VALUES("5480","ID","_NEWSLETTER_SUBSCRIPTION_MANAGEMENT","Newsletter Subscription Management");
INSERT INTO phpn_vocabulary VALUES("1395","en","_IMAGE","Image");
INSERT INTO phpn_vocabulary VALUES("1398","en","_IMAGES","Images");
INSERT INTO phpn_vocabulary VALUES("1401","en","_IMAGE_VERIFICATION","Image verification");
INSERT INTO phpn_vocabulary VALUES("1404","en","_IMAGE_VERIFY_EMPTY","You must enter image verification code!");
INSERT INTO phpn_vocabulary VALUES("1407","en","_INCOME","Income");
INSERT INTO phpn_vocabulary VALUES("1410","en","_INFO_AND_STATISTICS","Information and Statistics");
INSERT INTO phpn_vocabulary VALUES("1413","en","_INITIAL_FEE","Initial Fee");
INSERT INTO phpn_vocabulary VALUES("1416","en","_INSTALL","Install");
INSERT INTO phpn_vocabulary VALUES("1419","en","_INSTALLED","Installed");
INSERT INTO phpn_vocabulary VALUES("5479","ID","_NEWSLETTER_SUBSCRIBE_TEXT","<p>To receive newsletters from our site, simply enter your email and click on \"Subscribe\" button.</p><p>If you later decide to stop your subscription or change the type of news you receive, simply follow the link at the end of the latest newsletter and update your profile or unsubscribe by ticking the checkbox below.</p>");
INSERT INTO phpn_vocabulary VALUES("1422","en","_INSTALL_PHP_EXISTS","File <b>install.php</b> and/or directory <b>install/</b> still exists. For security reasons please remove them immediately!");
INSERT INTO phpn_vocabulary VALUES("1425","en","_INTEGRATION","Integration");
INSERT INTO phpn_vocabulary VALUES("5477","ID","_NEWSLETTER_SUBSCRIBERS","Newsletter Subscribers");
INSERT INTO phpn_vocabulary VALUES("5478","ID","_NEWSLETTER_SUBSCRIBE_SUCCESS","Thank you for subscribing to our electronic newsletter. You will receive an e-mail to confirm your subscription.");
INSERT INTO phpn_vocabulary VALUES("1428","en","_INTEGRATION_MESSAGE","Copy the code below and put it in the appropriate place of your web site to get a <b>Search Availability</b> block.");
INSERT INTO phpn_vocabulary VALUES("1431","en","_INTERNAL_USE_TOOLTIP","For internal use only");
INSERT INTO phpn_vocabulary VALUES("5476","ID","_NEWSLETTER_PRE_UNSUBSCRIBE_ALERT","Please click on the \"Unsubscribe\" button to complete the process.");
INSERT INTO phpn_vocabulary VALUES("1434","en","_INVALID_FILE_SIZE","Invalid file size: _FILE_SIZE_ (max. allowed: _MAX_ALLOWED_)");
INSERT INTO phpn_vocabulary VALUES("1437","en","_INVALID_IMAGE_FILE_TYPE","Uploaded file is not a valid image! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5475","ID","_NEWSLETTER_PRE_SUBSCRIBE_ALERT","Please click on the \"Subscribe\" button to complete the process.");
INSERT INTO phpn_vocabulary VALUES("1440","en","_INVOICE","Invoice");
INSERT INTO phpn_vocabulary VALUES("1443","en","_INVOICE_SENT_SUCCESS","The invoice has been successfully sent to the customer!");
INSERT INTO phpn_vocabulary VALUES("1446","en","_IN_PRODUCTS","In Products");
INSERT INTO phpn_vocabulary VALUES("1449","en","_IP_ADDRESS","IP Address");
INSERT INTO phpn_vocabulary VALUES("1452","en","_IP_ADDRESS_BLOCKED","Your IP Address is blocked! To resolve this problem, please contact the site administrator.");
INSERT INTO phpn_vocabulary VALUES("1455","en","_IS_DEFAULT","Is default");
INSERT INTO phpn_vocabulary VALUES("1458","en","_ITEMS","Items");
INSERT INTO phpn_vocabulary VALUES("1461","en","_ITEMS_LC","items");
INSERT INTO phpn_vocabulary VALUES("1464","en","_ITEM_NAME","Item Name");
INSERT INTO phpn_vocabulary VALUES("1467","en","_JANUARY","January");
INSERT INTO phpn_vocabulary VALUES("1470","en","_JULY","July");
INSERT INTO phpn_vocabulary VALUES("5474","ID","_NEWSLETTER_PAGE_TEXT","<p>To receive newsletters from our site, simply enter your email and click on \"Subscribe\" button.</p><p>If you later decide to stop your subscription or change the type of news you receive, simply follow the link at the end of the latest newsletter and update your profile or unsubscribe by ticking the checkbox below.</p>");
INSERT INTO phpn_vocabulary VALUES("1473","en","_JUNE","June");
INSERT INTO phpn_vocabulary VALUES("5473","ID","_NEWS","News");
INSERT INTO phpn_vocabulary VALUES("1476","en","_KEY","Key");
INSERT INTO phpn_vocabulary VALUES("5472","ID","_NEVER","never");
INSERT INTO phpn_vocabulary VALUES("1479","en","_KEYWORDS","Keywords");
INSERT INTO phpn_vocabulary VALUES("1482","en","_KEY_DISPLAY_TYPE","Key display type");
INSERT INTO phpn_vocabulary VALUES("5471","ID","_NAME_Z_A","name (z-a)");
INSERT INTO phpn_vocabulary VALUES("1485","en","_LANGUAGE","Language");
INSERT INTO phpn_vocabulary VALUES("5470","ID","_NAME_A_Z","name (a-z)");
INSERT INTO phpn_vocabulary VALUES("1488","en","_LANGUAGES","Languages");
INSERT INTO phpn_vocabulary VALUES("5469","ID","_NAME","Name");
INSERT INTO phpn_vocabulary VALUES("1491","en","_LANGUAGES_SETTINGS","Languages Settings");
INSERT INTO phpn_vocabulary VALUES("5468","ID","_MY_ORDERS","My Orders");
INSERT INTO phpn_vocabulary VALUES("1494","en","_LANGUAGE_ADDED","New language has been successfully added!");
INSERT INTO phpn_vocabulary VALUES("5467","ID","_MY_BOOKINGS","My Bookings");
INSERT INTO phpn_vocabulary VALUES("1497","en","_LANGUAGE_ADD_NEW","Add New Language");
INSERT INTO phpn_vocabulary VALUES("5466","ID","_MY_ACCOUNT","My Account");
INSERT INTO phpn_vocabulary VALUES("1500","en","_LANGUAGE_EDIT","Edit Language");
INSERT INTO phpn_vocabulary VALUES("1503","en","_LANGUAGE_EDITED","Language data has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("1506","en","_LANGUAGE_NAME","Language Name");
INSERT INTO phpn_vocabulary VALUES("1509","en","_LANG_ABBREV_EMPTY","Language abbreviation cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5465","ID","_MUST_BE_LOGGED","You must be logged in to view this page! <a href=\'index.php?customer=login\'>Login</a> or <a href=\'index.php?customer=create_account\'>Create Account for free</a>.");
INSERT INTO phpn_vocabulary VALUES("1512","en","_LANG_DELETED","Language has been successfully deleted!");
INSERT INTO phpn_vocabulary VALUES("5464","ID","_MS_WATERMARK_TEXT","Watermark text that will be added to images");
INSERT INTO phpn_vocabulary VALUES("1515","en","_LANG_DELETE_LAST_ERROR","You cannot delete last language!");
INSERT INTO phpn_vocabulary VALUES("1518","en","_LANG_DELETE_WARNING","Are you sure you want to remove this language? This operation will delete all language vocabulary!");
INSERT INTO phpn_vocabulary VALUES("5463","ID","_MS_VIDEO_GALLERY_TYPE","Allowed types of Video Gallery");
INSERT INTO phpn_vocabulary VALUES("1521","en","_LANG_MISSED","Missed language to update! Please, try again.");
INSERT INTO phpn_vocabulary VALUES("1524","en","_LANG_NAME_EMPTY","Language name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5462","ID","_MS_VAT_VALUE","Specifies default VAT value for order (in %) &nbsp;[<a href=index.php?admin=countries_management>Define by Country</a>]");
INSERT INTO phpn_vocabulary VALUES("1527","en","_LANG_NAME_EXISTS","Language with such name already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("1530","en","_LANG_NOT_DELETED","Language has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("5461","ID","_MS_VAT_INCLUDED_IN_PRICE","Specifies whether VAT fee is included in room and extras prices or not");
INSERT INTO phpn_vocabulary VALUES("1533","en","_LANG_ORDER_CHANGED","Language order has been successfully changed!");
INSERT INTO phpn_vocabulary VALUES("5460","ID","_MS_USER_TYPE","Type of users, who can post comments");
INSERT INTO phpn_vocabulary VALUES("1536","en","_LAST_CURRENCY_ALERT","You cannot delete last active currency!");
INSERT INTO phpn_vocabulary VALUES("5459","ID","_MS_TWO_CHECKOUT_VENDOR","Specifies 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("1539","en","_LAST_HOTEL_ALERT","You cannot delete last active hotel record!");
INSERT INTO phpn_vocabulary VALUES("1542","en","_LAST_LOGGED_IP","Last logged IP");
INSERT INTO phpn_vocabulary VALUES("1545","en","_LAST_LOGIN","Last Login");
INSERT INTO phpn_vocabulary VALUES("5458","ID","_MS_SHOW_RESERVATION_FORM","Specifies whether to show Reservation Form on homepage or not");
INSERT INTO phpn_vocabulary VALUES("1548","en","_LAST_NAME","Last Name");
INSERT INTO phpn_vocabulary VALUES("1551","en","_LAST_NAME_EMPTY_ALERT","Last Name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("1554","en","_LAST_RUN","Last run");
INSERT INTO phpn_vocabulary VALUES("5457","ID","_MS_SHOW_NEWS_BLOCK","Defines whether to show News side block or not");
INSERT INTO phpn_vocabulary VALUES("1557","en","_LAYOUT","Layout");
INSERT INTO phpn_vocabulary VALUES("1560","en","_LEAVE_YOUR_COMMENT","Leave your comment");
INSERT INTO phpn_vocabulary VALUES("1563","en","_LEFT","Left");
INSERT INTO phpn_vocabulary VALUES("1566","en","_LEFT_TO_RIGHT","LTR (left-to-right)");
INSERT INTO phpn_vocabulary VALUES("5456","ID","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Defines whether to show Newsletter Subscription block or not");
INSERT INTO phpn_vocabulary VALUES("1569","en","_LEGEND","Legend");
INSERT INTO phpn_vocabulary VALUES("1572","en","_LEGEND_CANCELED","Order is canceled by admin and the room is available again in search");
INSERT INTO phpn_vocabulary VALUES("5455","ID","_MS_SHOW_FULLY_BOOKED_ROOMS","Specifies whether to allow showing of fully booked/unavailable rooms in search");
INSERT INTO phpn_vocabulary VALUES("1575","en","_LEGEND_COMPLETED","Money is paid (fully or partially) and order completed");
INSERT INTO phpn_vocabulary VALUES("5454","ID","_MS_SHOW_DEFAULT_PRICES","Specifies whether to show default prices  on the Front-End or not");
INSERT INTO phpn_vocabulary VALUES("1578","en","_LEGEND_PAYMENT_ERROR","An error occurred while processing customer payments");
INSERT INTO phpn_vocabulary VALUES("1581","en","_LEGEND_PENDING","The booking has been created, but not yet been confirmed and reserved");
INSERT INTO phpn_vocabulary VALUES("5452","ID","_MS_SEND_ORDER_COPY_TO_ADMIN","Specifies whether to allow sending a copy of order to admin or hotel owner");
INSERT INTO phpn_vocabulary VALUES("5453","ID","_MS_SHOW_BOOKING_STATUS_FORM","Specifies whether to show Booking Status Form on homepage or not");
INSERT INTO phpn_vocabulary VALUES("1584","en","_LEGEND_PREBOOKING","This is a system status for a booking that is in progress, or was not completed (room has been added to Reservation Cart, but still not reserved)");
INSERT INTO phpn_vocabulary VALUES("1587","en","_LEGEND_REFUNDED","Order has been refunded and the room is available again in search");
INSERT INTO phpn_vocabulary VALUES("5451","ID","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","Specifies the number of rooms/hotels that will be displayed on one page in the search availability results");
INSERT INTO phpn_vocabulary VALUES("1590","en","_LEGEND_RESERVED","Room is reserved, but order is not paid yet (pending)");
INSERT INTO phpn_vocabulary VALUES("5450","ID","_MS_ROTATION_TYPE","Different type of banner rotation");
INSERT INTO phpn_vocabulary VALUES("1593","en","_LICENSE","License");
INSERT INTO phpn_vocabulary VALUES("1596","en","_LINK","Link");
INSERT INTO phpn_vocabulary VALUES("1599","en","_LINK_PARAMETER","Link Parameter");
INSERT INTO phpn_vocabulary VALUES("5449","ID","_MS_ROTATE_DELAY","Defines banners rotation delay in seconds");
INSERT INTO phpn_vocabulary VALUES("1602","en","_LOADING","loading");
INSERT INTO phpn_vocabulary VALUES("1605","en","_LOCAL_TIME","Local Time");
INSERT INTO phpn_vocabulary VALUES("1608","en","_LOCATION","Location");
INSERT INTO phpn_vocabulary VALUES("1611","en","_LOCATIONS","Locations");
INSERT INTO phpn_vocabulary VALUES("1614","en","_LOCATION_NAME","Location Name");
INSERT INTO phpn_vocabulary VALUES("1617","en","_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("1620","en","_LOGINS","Logins");
INSERT INTO phpn_vocabulary VALUES("5447","ID","_MS_RESERVATION_INITIAL_FEE","Start (initial) fee - the sum that will be added to each booking (fixed value in default currency)");
INSERT INTO phpn_vocabulary VALUES("5448","ID","_MS_ROOMS_IN_SEARCH","Specifies what types of rooms to show in search result: all or available rooms only (without fully booked / unavailable)");
INSERT INTO phpn_vocabulary VALUES("1623","en","_LOGIN_PAGE_MSG","Use a valid administrator username and password to get access to the Administrator Back-End.<br><br>Return to site <a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/lock.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("5446","ID","_MS_RESERVATION EXPIRED_ALERT","Specifies whether to send email alert to customer when reservation has expired");
INSERT INTO phpn_vocabulary VALUES("1626","en","_LONG_DESCRIPTION","Long Description");
INSERT INTO phpn_vocabulary VALUES("1629","en","_LOOK_IN","Look in");
INSERT INTO phpn_vocabulary VALUES("1632","en","_MAILER","Mailer");
INSERT INTO phpn_vocabulary VALUES("1635","en","_MAIN","Main");
INSERT INTO phpn_vocabulary VALUES("5445","ID","_MS_REMEMBER_ME","Specifies whether to allow Remember Me feature");
INSERT INTO phpn_vocabulary VALUES("1638","en","_MAIN_ADMIN","Main Admin");
INSERT INTO phpn_vocabulary VALUES("1641","en","_MAKE_RESERVATION","Make а Reservation");
INSERT INTO phpn_vocabulary VALUES("1644","en","_MANAGE_TEMPLATES","Manage Templates");
INSERT INTO phpn_vocabulary VALUES("1647","en","_MAP_CODE","Map Code");
INSERT INTO phpn_vocabulary VALUES("5444","ID","_MS_REG_CONFIRMATION","Defines whether confirmation (which type of) is required for registration");
INSERT INTO phpn_vocabulary VALUES("1650","en","_MAP_OVERLAY","Map Overlay");
INSERT INTO phpn_vocabulary VALUES("1653","en","_MARCH","March");
INSERT INTO phpn_vocabulary VALUES("1656","en","_MASS_MAIL","Mass Mail");
INSERT INTO phpn_vocabulary VALUES("5443","ID","_MS_RATINGS_USER_TYPE","Type of users, who can rate hotels");
INSERT INTO phpn_vocabulary VALUES("1659","en","_MASS_MAIL_ALERT","Attention: shared hosting services usually have a limit of 200 emails per hour");
INSERT INTO phpn_vocabulary VALUES("5442","ID","_MS_PRE_PAYMENT_VALUE","Defines a pre-payment value for \'fixed sum\' or \'percentage\' types");
INSERT INTO phpn_vocabulary VALUES("1662","en","_MASS_MAIL_AND_TEMPLATES","Mass Mail & Templates");
INSERT INTO phpn_vocabulary VALUES("1665","en","_MAXIMUM_NIGHTS","Maximum Nights");
INSERT INTO phpn_vocabulary VALUES("5441","ID","_MS_PRE_PAYMENT_TYPE","Defines a pre-payment type (full price, first night only, fixed sum or percentage)");
INSERT INTO phpn_vocabulary VALUES("1668","en","_MAXIMUM_NIGHTS_ALERT","_IN_HOTEL_The maximum allowed stay for this period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5440","ID","_MS_PRE_MODERATION_ALLOW","Specifies whether to allow pre-moderation for comments");
INSERT INTO phpn_vocabulary VALUES("1671","en","_MAX_ADULTS","Max Adults");
INSERT INTO phpn_vocabulary VALUES("1674","en","_MAX_ADULTS_ACCOMMODATE","Maximum number of adults this room can accommodate");
INSERT INTO phpn_vocabulary VALUES("5439","ID","_MS_PREBOOKING_ORDERS_TIMEOUT","Defines a timeout for \'prebooking\' orders before automatic deleting (in hours)");
INSERT INTO phpn_vocabulary VALUES("1677","en","_MAX_CHARS","(max: _MAX_CHARS_ chars)");
INSERT INTO phpn_vocabulary VALUES("1680","en","_MAX_CHILDREN","Max Children");
INSERT INTO phpn_vocabulary VALUES("5438","ID","_MS_PAYPAL_EMAIL","Specifies PayPal (business) email ");
INSERT INTO phpn_vocabulary VALUES("1683","en","_MAX_CHILDREN_ACCOMMODATE","Maximum number of children this room can accommodate");
INSERT INTO phpn_vocabulary VALUES("1686","en","_MAX_EXTRA_BEDS","Maximum Extra Beds");
INSERT INTO phpn_vocabulary VALUES("5437","ID","_MS_PAYMENT_TYPE_POA","Specifies whether to allow \'Pay on Arrival\' (POA) payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("1689","en","_MAX_OCCUPANCY","Max. Occupancy");
INSERT INTO phpn_vocabulary VALUES("5436","ID","_MS_PAYMENT_TYPE_PAYPAL","Specifies whether to allow \'PayPal\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("1692","en","_MAX_RESERVATIONS_ERROR","You have reached the maximum number of permitted room reservations, that you have not yet finished! Please complete at least one of them to proceed reservation of new rooms.");
INSERT INTO phpn_vocabulary VALUES("5435","ID","_MS_PAYMENT_TYPE_ONLINE","Specifies whether to allow \'On-line Order\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("1695","en","_MAY","May");
INSERT INTO phpn_vocabulary VALUES("5433","ID","_MS_PAYMENT_TYPE_AUTHORIZE","Specifies whether to allow \'Authorize.Net\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("5434","ID","_MS_PAYMENT_TYPE_BANK_TRANSFER","Specifies whether to allow \'Bank Transfer\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("1698","en","_MD_BACKUP_AND_RESTORE","With Backup and Restore module you can dump all of your database tables to a file download or save to a file on the server, and to restore from an uploaded or previously saved database dump.");
INSERT INTO phpn_vocabulary VALUES("5432","ID","_MS_PAYMENT_TYPE_2CO","Specifies whether to allow \'2CO\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("1701","en","_MD_BANNERS","The Banners module allows administrator to display images on the site in random or rotation style.");
INSERT INTO phpn_vocabulary VALUES("5429","ID","_MS_NEWS_HEADER_LENGTH","Defines a length of news header in block");
INSERT INTO phpn_vocabulary VALUES("5430","ID","_MS_NEWS_RSS","Defines using of RSS for news");
INSERT INTO phpn_vocabulary VALUES("5431","ID","_MS_ONLINE_CREDIT_CARD_REQUIRED","Specifies whether collecting of credit card info is required for \'On-line Orders\'");
INSERT INTO phpn_vocabulary VALUES("1704","en","_MD_BOOKINGS","The Bookings module allows the site owner to define bookings for all rooms, then price them on an individual basis by accommodation and date. It also permits bookings to be taken from customers and managed via administrator panel.");
INSERT INTO phpn_vocabulary VALUES("5428","ID","_MS_NEWS_COUNT","Defines how many news will be shown in news block");
INSERT INTO phpn_vocabulary VALUES("1707","en","_MD_COMMENTS","The Comments module allows visitors to leave comments on articles and administrator of the site to moderate them.");
INSERT INTO phpn_vocabulary VALUES("5427","ID","_MS_MULTIPLE_ITEMS_PER_DAY","Specifies whether to allow users to rate multiple items per day or not");
INSERT INTO phpn_vocabulary VALUES("1710","en","_MD_CONTACT_US","Contact Us module allows easy create and place on-line contact form on site pages, using predefined code, like: {module:contact_us}.");
INSERT INTO phpn_vocabulary VALUES("5425","ID","_MS_MAX_CHILDREN_IN_SEARCH","Specifies the maximum number of children in the dropdown list of the Search Availability form");
INSERT INTO phpn_vocabulary VALUES("5426","ID","_MS_MINIMUM_NIGHTS","Defines a minimum number of nights per booking [<a href=index.php?admin=mod_booking_packages>Define by Package</a>]");
INSERT INTO phpn_vocabulary VALUES("1713","en","_MD_CUSTOMERS","The Customers module allows easy customers management on your site. Administrator could create, edit or delete customer accounts. Customers could register on the site and log into their accounts.");
INSERT INTO phpn_vocabulary VALUES("5424","ID","_MS_MAX_ADULTS_IN_SEARCH","Specifies the maximum number of adults in the dropdown list of the Search Availability form");
INSERT INTO phpn_vocabulary VALUES("1716","en","_MD_FAQ","The Frequently Asked Questions (faq) module allows admin users to create question and answer pairs which they want displayed on the \'faq\' page.");
INSERT INTO phpn_vocabulary VALUES("5422","ID","_MS_MAXIMUM_ALLOWED_RESERVATIONS","Specifies the maximum number of allowed room reservations (not completed) per customer");
INSERT INTO phpn_vocabulary VALUES("5423","ID","_MS_MAXIMUM_NIGHTS","Defines a maximum number of nights per booking [<a href=index.php?admin=mod_booking_packages>Define by Package</a>]");
INSERT INTO phpn_vocabulary VALUES("1719","en","_MD_GALLERY","The Gallery module allows administrator to create image or video albums, upload album content and display this content to be viewed by visitor of the site.");
INSERT INTO phpn_vocabulary VALUES("5421","ID","_MS_ITEMS_COUNT_IN_ALBUM","Specifies whether to show count of images/video under album name");
INSERT INTO phpn_vocabulary VALUES("1722","en","_MD_NEWS","The News and Events module allows administrator to post news and events on the site, display latest of them at the side block.");
INSERT INTO phpn_vocabulary VALUES("5420","ID","_MS_IS_SEND_DELAY","Specifies whether to allow time delay between sending emails.");
INSERT INTO phpn_vocabulary VALUES("1725","en","_MD_PAGES","Pages module allows administrator to easily create and maintain page content.");
INSERT INTO phpn_vocabulary VALUES("5417","ID","_MS_GALLERY_WRAPPER","Defines a wrapper type for gallery");
INSERT INTO phpn_vocabulary VALUES("5418","ID","_MS_IMAGE_GALLERY_TYPE","Allowed types of Image Gallery");
INSERT INTO phpn_vocabulary VALUES("5419","ID","_MS_IMAGE_VERIFICATION_ALLOW","Specifies whether to allow image verification (captcha)");
INSERT INTO phpn_vocabulary VALUES("1728","en","_MD_RATINGS","The Ratings module allows your users to rate the hotels. The number of votes and average rating will be shown at the appropriate hotel.");
INSERT INTO phpn_vocabulary VALUES("5415","ID","_MS_FIRST_NIGHT_CALCULATING_TYPE","Specifies a type of the \'first night\' value calculating: real or average");
INSERT INTO phpn_vocabulary VALUES("5416","ID","_MS_GALLERY_KEY","The keyword that will be replaced with gallery (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("1731","en","_MD_ROOMS","The Rooms module allows the site owner easily manage rooms in your hotel: create, edit or remove them, specify room facilities, define prices and availability for certain period of time, etc.");
INSERT INTO phpn_vocabulary VALUES("1737","en","_MEAL_PLANS","Meal Plans");
INSERT INTO phpn_vocabulary VALUES("5414","ID","_MS_FAQ_IS_ACTIVE","Defines whether FAQ module is active or not");
INSERT INTO phpn_vocabulary VALUES("1740","en","_MEAL_PLANS_MANAGEMENT","Meal Plans Management");
INSERT INTO phpn_vocabulary VALUES("1743","en","_MENUS","Menus");
INSERT INTO phpn_vocabulary VALUES("1746","en","_MENUS_AND_PAGES","Menus and Pages");
INSERT INTO phpn_vocabulary VALUES("5413","ID","_MS_EMAIL","The email address, that will be used to get sent information");
INSERT INTO phpn_vocabulary VALUES("1749","en","_MENUS_DISABLED_ALERT","Take in account that some menus may be disabled for this template.");
INSERT INTO phpn_vocabulary VALUES("5412","ID","_MS_DELETE_PENDING_TIME","The maximum pending time for deleting of comment in minutes");
INSERT INTO phpn_vocabulary VALUES("1752","en","_MENU_ADD","Add Menu");
INSERT INTO phpn_vocabulary VALUES("1755","en","_MENU_CREATED","Menu has been successfully created");
INSERT INTO phpn_vocabulary VALUES("5411","ID","_MS_DELAY_LENGTH","Defines a length of delay between sending emails (in seconds)");
INSERT INTO phpn_vocabulary VALUES("1758","en","_MENU_DELETED","Menu has been successfully deleted");
INSERT INTO phpn_vocabulary VALUES("1761","en","_MENU_DELETE_WARNING","Are you sure you want to delete this menu? Note: this will make all its menu links invisible to your site visitors!");
INSERT INTO phpn_vocabulary VALUES("1764","en","_MENU_EDIT","Edit Menu");
INSERT INTO phpn_vocabulary VALUES("5410","ID","_MS_DEFAULT_PAYMENT_SYSTEM","Specifies default payment processing system");
INSERT INTO phpn_vocabulary VALUES("1767","en","_MENU_LINK","Menu Link");
INSERT INTO phpn_vocabulary VALUES("1770","en","_MENU_LINK_TEXT","Menu Link (max. 40 chars)");
INSERT INTO phpn_vocabulary VALUES("1773","en","_MENU_MANAGEMENT","Menus Management");
INSERT INTO phpn_vocabulary VALUES("5409","ID","_MS_CUSTOMERS_IMAGE_VERIFICATION","Specifies whether to allow image verification (captcha) on customer registration page");
INSERT INTO phpn_vocabulary VALUES("1776","en","_MENU_MISSED","Missed menu to update! Please, try again.");
INSERT INTO phpn_vocabulary VALUES("1779","en","_MENU_NAME","Menu Name");
INSERT INTO phpn_vocabulary VALUES("1782","en","_MENU_NAME_EMPTY","Menu name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5408","ID","_MS_CUSTOMERS_CANCEL_RESERVATION","Specifies the number of days before customers may cancel a reservation");
INSERT INTO phpn_vocabulary VALUES("1785","en","_MENU_NOT_CREATED","Menu has not been created!");
INSERT INTO phpn_vocabulary VALUES("1788","en","_MENU_NOT_DELETED","Menu has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("1791","en","_MENU_NOT_FOUND","No Menus Found");
INSERT INTO phpn_vocabulary VALUES("5407","ID","_MS_CONTACT_US_KEY","The keyword that will be replaced with Contact Us form (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("1794","en","_MENU_NOT_SAVED","Menu has not been saved!");
INSERT INTO phpn_vocabulary VALUES("1797","en","_MENU_ORDER","Menu Order");
INSERT INTO phpn_vocabulary VALUES("5406","ID","_MS_COMMENTS_PAGE_SIZE","Defines how much comments will be shown on one page");
INSERT INTO phpn_vocabulary VALUES("1800","en","_MENU_ORDER_CHANGED","Menu order has been successfully changed");
INSERT INTO phpn_vocabulary VALUES("1803","en","_MENU_SAVED","Menu has been successfully saved");
INSERT INTO phpn_vocabulary VALUES("5405","ID","_MS_COMMENTS_LENGTH","The maximum length of a comment");
INSERT INTO phpn_vocabulary VALUES("1806","en","_MENU_TITLE","Menu Title");
INSERT INTO phpn_vocabulary VALUES("1809","en","_MENU_WORD","Menu");
INSERT INTO phpn_vocabulary VALUES("1812","en","_MESSAGE","Message");
INSERT INTO phpn_vocabulary VALUES("5404","ID","_MS_COMMENTS_ALLOW","Specifies whether to allow comments to articles");
INSERT INTO phpn_vocabulary VALUES("1815","en","_MESSAGE_EMPTY_ALERT","Message cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1818","en","_META_TAG","Meta Tag");
INSERT INTO phpn_vocabulary VALUES("1821","en","_META_TAGS","META Tags");
INSERT INTO phpn_vocabulary VALUES("1824","en","_METHOD","Method");
INSERT INTO phpn_vocabulary VALUES("1827","en","_MIN","Min");
INSERT INTO phpn_vocabulary VALUES("5403","ID","_MS_CHECK_PARTIALLY_OVERLAPPING","Specifies whether to allow check for partially overlapping dates for packages");
INSERT INTO phpn_vocabulary VALUES("1830","en","_MINIMUM_NIGHTS","Minimum Nights");
INSERT INTO phpn_vocabulary VALUES("5401","ID","_MS_BOOKING_MODE","Specifies which mode is turned ON for booking");
INSERT INTO phpn_vocabulary VALUES("5402","ID","_MS_BOOKING_NUMBER_TYPE","Specifies the type of booking numbers");
INSERT INTO phpn_vocabulary VALUES("1833","en","_MINIMUM_NIGHTS_ALERT","_IN_HOTEL_The minimum allowed stay for the period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking (package _PACKAGE_NAME_). Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1836","en","_MINUTES","minutes");
INSERT INTO phpn_vocabulary VALUES("1839","en","_MO","Mo");
INSERT INTO phpn_vocabulary VALUES("1842","en","_MODULES","Modules");
INSERT INTO phpn_vocabulary VALUES("5400","ID","_MS_BANNERS_IS_ACTIVE","Defines whether banners module is active or not");
INSERT INTO phpn_vocabulary VALUES("1845","en","_MODULES_MANAGEMENT","Modules Management");
INSERT INTO phpn_vocabulary VALUES("1848","en","_MODULES_NOT_FOUND","No modules found!");
INSERT INTO phpn_vocabulary VALUES("1851","en","_MODULE_INSTALLED","Module has been successfully installed!");
INSERT INTO phpn_vocabulary VALUES("5399","ID","_MS_BANNERS_CAPTION_HTML","Specifies whether to allow using of HTML in slideshow captions or not");
INSERT INTO phpn_vocabulary VALUES("1854","en","_MODULE_INSTALL_ALERT","Are you sure you want to install this module?");
INSERT INTO phpn_vocabulary VALUES("1857","en","_MODULE_UNINSTALLED","Module has been successfully un-installed!");
INSERT INTO phpn_vocabulary VALUES("1860","en","_MODULE_UNINSTALL_ALERT","Are you sure you want to un-install this module? All data, related to this module will be permanently deleted form the system!");
INSERT INTO phpn_vocabulary VALUES("5398","ID","_MS_BANK_TRANSFER_INFO","Specifies a required banking information: name of the bank, branch, account number etc.");
INSERT INTO phpn_vocabulary VALUES("1863","en","_MON","Mon");
INSERT INTO phpn_vocabulary VALUES("1866","en","_MONDAY","Monday");
INSERT INTO phpn_vocabulary VALUES("1869","en","_MONTH","Month");
INSERT INTO phpn_vocabulary VALUES("1872","en","_MONTHS","Months");
INSERT INTO phpn_vocabulary VALUES("5397","ID","_MS_AVAILABLE_UNTIL_APPROVAL","Specifies whether to show \'reserved\' rooms in search results until booking is complete");
INSERT INTO phpn_vocabulary VALUES("1875","en","_MS_ACTIVATE_BOOKINGS","Specifies whether booking module is active on a Whole Site, Front-End/Back-End only or inactive");
INSERT INTO phpn_vocabulary VALUES("5396","ID","_MS_AUTHORIZE_TRANSACTION_KEY","Specifies Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("1878","en","_MS_ADD_WATERMARK","Specifies whether to add watermark to rooms images or not");
INSERT INTO phpn_vocabulary VALUES("5395","ID","_MS_AUTHORIZE_LOGIN_ID","Specifies Authorize.Net API Login ID");
INSERT INTO phpn_vocabulary VALUES("1881","en","_MS_ADMIN_BOOKING_IN_PAST","Specifies whether to allow booking in the past for admins and hotel owners (from the beginning of current month)");
INSERT INTO phpn_vocabulary VALUES("5394","ID","_MS_ALLOW_SYSTEM_SUGGESTION","Specifies whether to show system suggestion feature on empty search results");
INSERT INTO phpn_vocabulary VALUES("1884","en","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","Specifies whether to allow changing customer password by Admin");
INSERT INTO phpn_vocabulary VALUES("5393","ID","_MS_ALLOW_EXTRA_BEDS","Specifies whether to allow extra beds in the rooms");
INSERT INTO phpn_vocabulary VALUES("1887","en","_MS_ADMIN_CHANGE_USER_PASSWORD","Specifies whether to allow changing user password by Admin");
INSERT INTO phpn_vocabulary VALUES("1890","en","_MS_ALBUMS_PER_LINE","Number of album icons per line");
INSERT INTO phpn_vocabulary VALUES("5392","ID","_MS_ALLOW_DEFAULT_PERIODS","Specifies whether to allow adding and management of default periods for rooms ");
INSERT INTO phpn_vocabulary VALUES("1893","en","_MS_ALBUM_ICON_HEIGHT","Album icon height");
INSERT INTO phpn_vocabulary VALUES("1896","en","_MS_ALBUM_ICON_WIDTH","Album icon width");
INSERT INTO phpn_vocabulary VALUES("5391","ID","_MS_ALLOW_CUST_RESET_PASSWORDS","Specifies whether to allow customers to restore their passwords");
INSERT INTO phpn_vocabulary VALUES("1899","en","_MS_ALBUM_ITEMS_NUMERATION","Specifies whether to show items numeration in albums");
INSERT INTO phpn_vocabulary VALUES("5390","ID","_MS_ALLOW_CUSTOMERS_REGISTRATION","Specifies whether to allow registration of new customers");
INSERT INTO phpn_vocabulary VALUES("1902","en","_MS_ALBUM_KEY","The keyword that will be replaced with a certain album images (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("5389","ID","_MS_ALLOW_CUSTOMERS_LOGIN","Specifies whether to allow existing customers to login");
INSERT INTO phpn_vocabulary VALUES("1905","en","_MS_ALERT_ADMIN_NEW_REGISTRATION","Specifies whether to alert admin on new customer registration");
INSERT INTO phpn_vocabulary VALUES("5388","ID","_MS_ALLOW_CHILDREN_IN_ROOM","Specifies whether to allow children in the room");
INSERT INTO phpn_vocabulary VALUES("1908","en","_MS_ALLOW_ADDING_BY_ADMIN","Specifies whether to allow adding new customers by Admin");
INSERT INTO phpn_vocabulary VALUES("5387","ID","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","Specifies whether to allow booking for customer without creating account");
INSERT INTO phpn_vocabulary VALUES("1911","en","_MS_ALLOW_BOOKING_WITHOUT_ACCOUNT","Specifies whether to allow booking for customer without creating account");
INSERT INTO phpn_vocabulary VALUES("1914","en","_MS_ALLOW_CHILDREN_IN_ROOM","Specifies whether to allow children in the room");
INSERT INTO phpn_vocabulary VALUES("5386","ID","_MS_ALLOW_ADDING_BY_ADMIN","Specifies whether to allow adding new customers by Admin");
INSERT INTO phpn_vocabulary VALUES("1917","en","_MS_ALLOW_CUSTOMERS_LOGIN","Specifies whether to allow existing customers to login");
INSERT INTO phpn_vocabulary VALUES("5385","ID","_MS_ALERT_ADMIN_NEW_REGISTRATION","Specifies whether to alert admin on new customer registration");
INSERT INTO phpn_vocabulary VALUES("1920","en","_MS_ALLOW_CUSTOMERS_REGISTRATION","Specifies whether to allow registration of new customers");
INSERT INTO phpn_vocabulary VALUES("1923","en","_MS_ALLOW_CUST_RESET_PASSWORDS","Specifies whether to allow customers to restore their passwords");
INSERT INTO phpn_vocabulary VALUES("5384","ID","_MS_ALBUM_KEY","The keyword that will be replaced with a certain album images (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("1926","en","_MS_ALLOW_DEFAULT_PERIODS","Specifies whether to allow adding and management of default periods for rooms ");
INSERT INTO phpn_vocabulary VALUES("5383","ID","_MS_ALBUM_ITEMS_NUMERATION","Specifies whether to show items numeration in albums");
INSERT INTO phpn_vocabulary VALUES("1929","en","_MS_ALLOW_EXTRA_BEDS","Specifies whether to allow extra beds in the rooms");
INSERT INTO phpn_vocabulary VALUES("5381","ID","_MS_ALBUM_ICON_HEIGHT","Album icon height");
INSERT INTO phpn_vocabulary VALUES("5382","ID","_MS_ALBUM_ICON_WIDTH","Album icon width");
INSERT INTO phpn_vocabulary VALUES("1932","en","_MS_ALLOW_SYSTEM_SUGGESTION","Specifies whether to show system suggestion feature on empty search results");
INSERT INTO phpn_vocabulary VALUES("5380","ID","_MS_ALBUMS_PER_LINE","Number of album icons per line");
INSERT INTO phpn_vocabulary VALUES("1935","en","_MS_AUTHORIZE_LOGIN_ID","Specifies Authorize.Net API Login ID");
INSERT INTO phpn_vocabulary VALUES("1938","en","_MS_AUTHORIZE_TRANSACTION_KEY","Specifies Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("5379","ID","_MS_ADMIN_CHANGE_USER_PASSWORD","Specifies whether to allow changing user password by Admin");
INSERT INTO phpn_vocabulary VALUES("1941","en","_MS_AVAILABLE_UNTIL_APPROVAL","Specifies whether to show \'reserved\' rooms in search results until booking is complete");
INSERT INTO phpn_vocabulary VALUES("5378","ID","_MS_ADMIN_CHANGE_CUSTOMER_PASSWORD","Specifies whether to allow changing customer password by Admin");
INSERT INTO phpn_vocabulary VALUES("1944","en","_MS_BANK_TRANSFER_INFO","Specifies a required banking information: name of the bank, branch, account number etc.");
INSERT INTO phpn_vocabulary VALUES("5377","ID","_MS_ADMIN_BOOKING_IN_PAST","Specifies whether to allow booking in the past for admins and hotel owners (from the beginning of current month)");
INSERT INTO phpn_vocabulary VALUES("1947","en","_MS_BANNERS_CAPTION_HTML","Specifies whether to allow using of HTML in slideshow captions or not");
INSERT INTO phpn_vocabulary VALUES("1950","en","_MS_BANNERS_IS_ACTIVE","Defines whether banners module is active or not");
INSERT INTO phpn_vocabulary VALUES("5376","ID","_MS_ADD_WATERMARK","Specifies whether to add watermark to rooms images or not");
INSERT INTO phpn_vocabulary VALUES("1953","en","_MS_BOOKING_MODE","Specifies which mode is turned ON for booking");
INSERT INTO phpn_vocabulary VALUES("1956","en","_MS_BOOKING_NUMBER_TYPE","Specifies the type of booking numbers");
INSERT INTO phpn_vocabulary VALUES("5373","ID","_MONTH","Month");
INSERT INTO phpn_vocabulary VALUES("5374","ID","_MONTHS","Months");
INSERT INTO phpn_vocabulary VALUES("5375","ID","_MS_ACTIVATE_BOOKINGS","Specifies whether booking module is active on a Whole Site, Front-End/Back-End only or inactive");
INSERT INTO phpn_vocabulary VALUES("1959","en","_MS_CHECK_PARTIALLY_OVERLAPPING","Specifies whether to allow check for partially overlapping dates for packages");
INSERT INTO phpn_vocabulary VALUES("5371","ID","_MON","Mon");
INSERT INTO phpn_vocabulary VALUES("5372","ID","_MONDAY","Monday");
INSERT INTO phpn_vocabulary VALUES("1962","en","_MS_COMMENTS_ALLOW","Specifies whether to allow comments to articles");
INSERT INTO phpn_vocabulary VALUES("1965","en","_MS_COMMENTS_LENGTH","The maximum length of a comment");
INSERT INTO phpn_vocabulary VALUES("1968","en","_MS_COMMENTS_PAGE_SIZE","Defines how much comments will be shown on one page");
INSERT INTO phpn_vocabulary VALUES("5369","ID","_MODULE_UNINSTALLED","Module has been successfully un-installed!");
INSERT INTO phpn_vocabulary VALUES("5370","ID","_MODULE_UNINSTALL_ALERT","Are you sure you want to un-install this module? All data, related to this module will be permanently deleted form the system!");
INSERT INTO phpn_vocabulary VALUES("1971","en","_MS_CONTACT_US_KEY","The keyword that will be replaced with Contact Us form (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("5368","ID","_MODULE_INSTALL_ALERT","Are you sure you want to install this module?");
INSERT INTO phpn_vocabulary VALUES("1974","en","_MS_CUSTOMERS_CANCEL_RESERVATION","Specifies the number of days before customers may cancel a reservation");
INSERT INTO phpn_vocabulary VALUES("5367","ID","_MODULE_INSTALLED","Module has been successfully installed!");
INSERT INTO phpn_vocabulary VALUES("1977","en","_MS_CUSTOMERS_IMAGE_VERIFICATION","Specifies whether to allow image verification (captcha) on customer registration page");
INSERT INTO phpn_vocabulary VALUES("5366","ID","_MODULES_NOT_FOUND","No modules found!");
INSERT INTO phpn_vocabulary VALUES("1980","en","_MS_DEFAULT_PAYMENT_SYSTEM","Specifies default payment processing system");
INSERT INTO phpn_vocabulary VALUES("5363","ID","_MO","Mo");
INSERT INTO phpn_vocabulary VALUES("5364","ID","_MODULES","Modules");
INSERT INTO phpn_vocabulary VALUES("5365","ID","_MODULES_MANAGEMENT","Modules Management");
INSERT INTO phpn_vocabulary VALUES("1983","en","_MS_DELAY_LENGTH","Defines a length of delay between sending emails (in seconds)");
INSERT INTO phpn_vocabulary VALUES("5362","ID","_MINUTES","minutes");
INSERT INTO phpn_vocabulary VALUES("1986","en","_MS_DELETE_PENDING_TIME","The maximum pending time for deleting of comment in minutes");
INSERT INTO phpn_vocabulary VALUES("1989","en","_MS_EMAIL","The email address, that will be used to get sent information");
INSERT INTO phpn_vocabulary VALUES("5361","ID","_MINIMUM_NIGHTS_ALERT","_IN_HOTEL_The minimum allowed stay for the period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking (package _PACKAGE_NAME_). Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("1992","en","_MS_FAQ_IS_ACTIVE","Defines whether FAQ module is active or not");
INSERT INTO phpn_vocabulary VALUES("5358","ID","_METHOD","Method");
INSERT INTO phpn_vocabulary VALUES("5359","ID","_MIN","Min");
INSERT INTO phpn_vocabulary VALUES("5360","ID","_MINIMUM_NIGHTS","Minimum Nights");
INSERT INTO phpn_vocabulary VALUES("1995","en","_MS_FIRST_NIGHT_CALCULATING_TYPE","Specifies a type of the \'first night\' value calculating: real or average");
INSERT INTO phpn_vocabulary VALUES("5356","ID","_META_TAG","Meta Tag");
INSERT INTO phpn_vocabulary VALUES("5357","ID","_META_TAGS","META Tags");
INSERT INTO phpn_vocabulary VALUES("1998","en","_MS_GALLERY_KEY","The keyword that will be replaced with gallery (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("5355","ID","_MESSAGE_EMPTY_ALERT","Message cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2001","en","_MS_GALLERY_WRAPPER","Defines a wrapper type for gallery");
INSERT INTO phpn_vocabulary VALUES("5354","ID","_MESSAGE","Message");
INSERT INTO phpn_vocabulary VALUES("2004","en","_MS_IMAGE_GALLERY_TYPE","Allowed types of Image Gallery");
INSERT INTO phpn_vocabulary VALUES("5352","ID","_MENU_TITLE","Menu Title");
INSERT INTO phpn_vocabulary VALUES("5353","ID","_MENU_WORD","Menu");
INSERT INTO phpn_vocabulary VALUES("2007","en","_MS_IMAGE_VERIFICATION_ALLOW","Specifies whether to allow image verification (captcha)");
INSERT INTO phpn_vocabulary VALUES("5351","ID","_MENU_SAVED","Menu has been successfully saved");
INSERT INTO phpn_vocabulary VALUES("2010","en","_MS_IS_SEND_DELAY","Specifies whether to allow time delay between sending emails.");
INSERT INTO phpn_vocabulary VALUES("5350","ID","_MENU_ORDER_CHANGED","Menu order has been successfully changed");
INSERT INTO phpn_vocabulary VALUES("2013","en","_MS_ITEMS_COUNT_IN_ALBUM","Specifies whether to show count of images/video under album name");
INSERT INTO phpn_vocabulary VALUES("5348","ID","_MENU_NOT_SAVED","Menu has not been saved!");
INSERT INTO phpn_vocabulary VALUES("5349","ID","_MENU_ORDER","Menu Order");
INSERT INTO phpn_vocabulary VALUES("2016","en","_MS_MAXIMUM_ALLOWED_RESERVATIONS","Specifies the maximum number of allowed room reservations (not completed) per customer");
INSERT INTO phpn_vocabulary VALUES("5346","ID","_MENU_NOT_DELETED","Menu has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("5347","ID","_MENU_NOT_FOUND","No Menus Found");
INSERT INTO phpn_vocabulary VALUES("2019","en","_MS_MAXIMUM_NIGHTS","Defines a maximum number of nights per booking [<a href=index.php?admin=mod_booking_packages>Define by Package</a>]");
INSERT INTO phpn_vocabulary VALUES("5344","ID","_MENU_NAME_EMPTY","Menu name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5345","ID","_MENU_NOT_CREATED","Menu has not been created!");
INSERT INTO phpn_vocabulary VALUES("2022","en","_MS_MAX_ADULTS_IN_SEARCH","Specifies the maximum number of adults in the dropdown list of the Search Availability form");
INSERT INTO phpn_vocabulary VALUES("5342","ID","_MENU_MISSED","Missed menu to update! Please, try again.");
INSERT INTO phpn_vocabulary VALUES("5343","ID","_MENU_NAME","Menu Name");
INSERT INTO phpn_vocabulary VALUES("2025","en","_MS_MAX_CHILDREN_IN_SEARCH","Specifies the maximum number of children in the dropdown list of the Search Availability form");
INSERT INTO phpn_vocabulary VALUES("5340","ID","_MENU_LINK_TEXT","Menu Link (max. 40 chars)");
INSERT INTO phpn_vocabulary VALUES("5341","ID","_MENU_MANAGEMENT","Menus Management");
INSERT INTO phpn_vocabulary VALUES("2028","en","_MS_MINIMUM_NIGHTS","Defines a minimum number of nights per booking [<a href=index.php?admin=mod_booking_packages>Define by Package</a>]");
INSERT INTO phpn_vocabulary VALUES("5338","ID","_MENU_EDIT","Edit Menu");
INSERT INTO phpn_vocabulary VALUES("5339","ID","_MENU_LINK","Menu Link");
INSERT INTO phpn_vocabulary VALUES("2031","en","_MS_MULTIPLE_ITEMS_PER_DAY","Specifies whether to allow users to rate multiple items per day or not");
INSERT INTO phpn_vocabulary VALUES("2034","en","_MS_NEWS_COUNT","Defines how many news will be shown in news block");
INSERT INTO phpn_vocabulary VALUES("5337","ID","_MENU_DELETE_WARNING","Are you sure you want to delete this menu? Note: this will make all its menu links invisible to your site visitors!");
INSERT INTO phpn_vocabulary VALUES("2037","en","_MS_NEWS_HEADER_LENGTH","Defines a length of news header in block");
INSERT INTO phpn_vocabulary VALUES("5336","ID","_MENU_DELETED","Menu has been successfully deleted");
INSERT INTO phpn_vocabulary VALUES("2040","en","_MS_NEWS_RSS","Defines using of RSS for news");
INSERT INTO phpn_vocabulary VALUES("5334","ID","_MENU_ADD","Add Menu");
INSERT INTO phpn_vocabulary VALUES("5335","ID","_MENU_CREATED","Menu has been successfully created");
INSERT INTO phpn_vocabulary VALUES("2043","en","_MS_ONLINE_CREDIT_CARD_REQUIRED","Specifies whether collecting of credit card info is required for \'On-line Orders\'");
INSERT INTO phpn_vocabulary VALUES("5333","ID","_MENUS_DISABLED_ALERT","Take in account that some menus may be disabled for this template.");
INSERT INTO phpn_vocabulary VALUES("2046","en","_MS_PAYMENT_TYPE_2CO","Specifies whether to allow \'2CO\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("5331","ID","_MENUS","Menus");
INSERT INTO phpn_vocabulary VALUES("5332","ID","_MENUS_AND_PAGES","Menus and Pages");
INSERT INTO phpn_vocabulary VALUES("2049","en","_MS_PAYMENT_TYPE_AUTHORIZE","Specifies whether to allow \'Authorize.Net\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("5329","ID","_MEAL_PLANS","Meal Plans");
INSERT INTO phpn_vocabulary VALUES("5330","ID","_MEAL_PLANS_MANAGEMENT","Meal Plans Management");
INSERT INTO phpn_vocabulary VALUES("2052","en","_MS_PAYMENT_TYPE_BANK_TRANSFER","Specifies whether to allow \'Bank Transfer\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2055","en","_MS_PAYMENT_TYPE_ONLINE","Specifies whether to allow \'On-line Order\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("5328","ID","_MD_ROOMS","The Rooms module allows the site owner easily manage rooms in your hotel: create, edit or remove them, specify room facilities, define prices and availability for certain period of time, etc.");
INSERT INTO phpn_vocabulary VALUES("2058","en","_MS_PAYMENT_TYPE_PAYPAL","Specifies whether to allow \'PayPal\' payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("2061","en","_MS_PAYMENT_TYPE_POA","Specifies whether to allow \'Pay on Arrival\' (POA) payment type and where apply it");
INSERT INTO phpn_vocabulary VALUES("5327","ID","_MD_RATINGS","The Ratings module allows your users to rate the hotels. The number of votes and average rating will be shown at the appropriate hotel.");
INSERT INTO phpn_vocabulary VALUES("2064","en","_MS_PAYPAL_EMAIL","Specifies PayPal (business) email ");
INSERT INTO phpn_vocabulary VALUES("5326","ID","_MD_PAGES","Pages module allows administrator to easily create and maintain page content.");
INSERT INTO phpn_vocabulary VALUES("2067","en","_MS_PREBOOKING_ORDERS_TIMEOUT","Defines a timeout for \'prebooking\' orders before automatic deleting (in hours)");
INSERT INTO phpn_vocabulary VALUES("2070","en","_MS_PRE_MODERATION_ALLOW","Specifies whether to allow pre-moderation for comments");
INSERT INTO phpn_vocabulary VALUES("5325","ID","_MD_NEWS","The News and Events module allows administrator to post news and events on the site, display latest of them at the side block.");
INSERT INTO phpn_vocabulary VALUES("2073","en","_MS_PRE_PAYMENT_TYPE","Defines a pre-payment type (full price, first night only, fixed sum or percentage)");
INSERT INTO phpn_vocabulary VALUES("2076","en","_MS_PRE_PAYMENT_VALUE","Defines a pre-payment value for \'fixed sum\' or \'percentage\' types");
INSERT INTO phpn_vocabulary VALUES("5324","ID","_MD_GALLERY","The Gallery module allows administrator to create image or video albums, upload album content and display this content to be viewed by visitor of the site.");
INSERT INTO phpn_vocabulary VALUES("2079","en","_MS_RATINGS_USER_TYPE","Type of users, who can rate hotels");
INSERT INTO phpn_vocabulary VALUES("2082","en","_MS_REG_CONFIRMATION","Defines whether confirmation (which type of) is required for registration");
INSERT INTO phpn_vocabulary VALUES("2085","en","_MS_REMEMBER_ME","Specifies whether to allow Remember Me feature");
INSERT INTO phpn_vocabulary VALUES("5323","ID","_MD_FAQ","The Frequently Asked Questions (faq) module allows admin users to create question and answer pairs which they want displayed on the \'faq\' page.");
INSERT INTO phpn_vocabulary VALUES("2088","en","_MS_RESERVATION EXPIRED_ALERT","Specifies whether to send email alert to customer when reservation has expired");
INSERT INTO phpn_vocabulary VALUES("2091","en","_MS_RESERVATION_INITIAL_FEE","Start (initial) fee - the sum that will be added to each booking (fixed value in default currency)");
INSERT INTO phpn_vocabulary VALUES("5322","ID","_MD_CUSTOMERS","The Customers module allows easy customers management on your site. Administrator could create, edit or delete customer accounts. Customers could register on the site and log into their accounts.");
INSERT INTO phpn_vocabulary VALUES("2094","en","_MS_ROOMS_IN_SEARCH","Specifies what types of rooms to show in search result: all or available rooms only (without fully booked / unavailable)");
INSERT INTO phpn_vocabulary VALUES("5321","ID","_MD_CONTACT_US","Contact Us module allows easy create and place on-line contact form on site pages, using predefined code, like: {module:contact_us}.");
INSERT INTO phpn_vocabulary VALUES("2097","en","_MS_ROTATE_DELAY","Defines banners rotation delay in seconds");
INSERT INTO phpn_vocabulary VALUES("2100","en","_MS_ROTATION_TYPE","Different type of banner rotation");
INSERT INTO phpn_vocabulary VALUES("5320","ID","_MD_COMMENTS","The Comments module allows visitors to leave comments on articles and administrator of the site to moderate them.");
INSERT INTO phpn_vocabulary VALUES("2103","en","_MS_SEARCH_AVAILABILITY_PAGE_SIZE","Specifies the number of rooms/hotels that will be displayed on one page in the search availability results");
INSERT INTO phpn_vocabulary VALUES("2106","en","_MS_SEND_ORDER_COPY_TO_ADMIN","Specifies whether to allow sending a copy of order to admin or hotel owner");
INSERT INTO phpn_vocabulary VALUES("5319","ID","_MD_BOOKINGS","The Bookings module allows the site owner to define bookings for all rooms, then price them on an individual basis by accommodation and date. It also permits bookings to be taken from customers and managed via administrator panel.");
INSERT INTO phpn_vocabulary VALUES("2109","en","_MS_SHOW_BOOKING_STATUS_FORM","Specifies whether to show Booking Status Form on homepage or not");
INSERT INTO phpn_vocabulary VALUES("2112","en","_MS_SHOW_DEFAULT_PRICES","Specifies whether to show default prices  on the Front-End or not");
INSERT INTO phpn_vocabulary VALUES("5318","ID","_MD_BANNERS","The Banners module allows administrator to display images on the site in random or rotation style.");
INSERT INTO phpn_vocabulary VALUES("2115","en","_MS_SHOW_FULLY_BOOKED_ROOMS","Specifies whether to allow showing of fully booked/unavailable rooms in search");
INSERT INTO phpn_vocabulary VALUES("2118","en","_MS_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Defines whether to show Newsletter Subscription block or not");
INSERT INTO phpn_vocabulary VALUES("5317","ID","_MD_BACKUP_AND_RESTORE","With Backup and Restore module you can dump all of your database tables to a file download or save to a file on the server, and to restore from an uploaded or previously saved database dump.");
INSERT INTO phpn_vocabulary VALUES("2121","en","_MS_SHOW_NEWS_BLOCK","Defines whether to show News side block or not");
INSERT INTO phpn_vocabulary VALUES("5316","ID","_MAY","May");
INSERT INTO phpn_vocabulary VALUES("2124","en","_MS_SHOW_RESERVATION_FORM","Specifies whether to show Reservation Form on homepage or not");
INSERT INTO phpn_vocabulary VALUES("2130","en","_MS_TWO_CHECKOUT_VENDOR","Specifies 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("2133","en","_MS_USER_TYPE","Type of users, who can post comments");
INSERT INTO phpn_vocabulary VALUES("5314","ID","_MAX_OCCUPANCY","Max. Occupancy");
INSERT INTO phpn_vocabulary VALUES("5315","ID","_MAX_RESERVATIONS_ERROR","You have reached the maximum number of permitted room reservations, that you have not yet finished! Please complete at least one of them to proceed reservation of new rooms.");
INSERT INTO phpn_vocabulary VALUES("2136","en","_MS_VAT_INCLUDED_IN_PRICE","Specifies whether VAT fee is included in room and extras prices or not");
INSERT INTO phpn_vocabulary VALUES("5312","ID","_MAX_CHILDREN_ACCOMMODATE","Maximum number of children this room can accommodate");
INSERT INTO phpn_vocabulary VALUES("5313","ID","_MAX_EXTRA_BEDS","Maximum Extra Beds");
INSERT INTO phpn_vocabulary VALUES("2139","en","_MS_VAT_VALUE","Specifies default VAT value for order (in %) &nbsp;[<a href=index.php?admin=countries_management>Define by Country</a>]");
INSERT INTO phpn_vocabulary VALUES("5311","ID","_MAX_CHILDREN","Max Children");
INSERT INTO phpn_vocabulary VALUES("2142","en","_MS_VIDEO_GALLERY_TYPE","Allowed types of Video Gallery");
INSERT INTO phpn_vocabulary VALUES("5310","ID","_MAX_CHARS","(max: _MAX_CHARS_ chars)");
INSERT INTO phpn_vocabulary VALUES("2145","en","_MS_WATERMARK_TEXT","Watermark text that will be added to images");
INSERT INTO phpn_vocabulary VALUES("5308","ID","_MAX_ADULTS","Max Adults");
INSERT INTO phpn_vocabulary VALUES("5309","ID","_MAX_ADULTS_ACCOMMODATE","Maximum number of adults this room can accommodate");
INSERT INTO phpn_vocabulary VALUES("2148","en","_MUST_BE_LOGGED","You must be logged in to view this page! <a href=\'index.php?customer=login\'>Login</a> or <a href=\'index.php?customer=create_account\'>Create Account for free</a>.");
INSERT INTO phpn_vocabulary VALUES("2151","en","_MY_ACCOUNT","My Account");
INSERT INTO phpn_vocabulary VALUES("2154","en","_MY_BOOKINGS","My Bookings");
INSERT INTO phpn_vocabulary VALUES("2157","en","_MY_ORDERS","My Orders");
INSERT INTO phpn_vocabulary VALUES("2160","en","_NAME","Name");
INSERT INTO phpn_vocabulary VALUES("5307","ID","_MAXIMUM_NIGHTS_ALERT","_IN_HOTEL_The maximum allowed stay for this period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2163","en","_NAME_A_Z","name (a-z)");
INSERT INTO phpn_vocabulary VALUES("2166","en","_NAME_Z_A","name (z-a)");
INSERT INTO phpn_vocabulary VALUES("2169","en","_NEVER","never");
INSERT INTO phpn_vocabulary VALUES("5306","ID","_MAXIMUM_NIGHTS","Maximum Nights");
INSERT INTO phpn_vocabulary VALUES("2172","en","_NEWS","News");
INSERT INTO phpn_vocabulary VALUES("5299","ID","_MANAGE_TEMPLATES","Manage Templates");
INSERT INTO phpn_vocabulary VALUES("5300","ID","_MAP_CODE","Map Code");
INSERT INTO phpn_vocabulary VALUES("5301","ID","_MAP_OVERLAY","Map Overlay");
INSERT INTO phpn_vocabulary VALUES("5302","ID","_MARCH","March");
INSERT INTO phpn_vocabulary VALUES("5303","ID","_MASS_MAIL","Mass Mail");
INSERT INTO phpn_vocabulary VALUES("5304","ID","_MASS_MAIL_ALERT","Attention: shared hosting services usually have a limit of 200 emails per hour");
INSERT INTO phpn_vocabulary VALUES("5305","ID","_MASS_MAIL_AND_TEMPLATES","Mass Mail & Templates");
INSERT INTO phpn_vocabulary VALUES("2175","en","_NEWSLETTER_PAGE_TEXT","<p>To receive newsletters from our site, simply enter your email and click on \"Subscribe\" button.</p><p>If you later decide to stop your subscription or change the type of news you receive, simply follow the link at the end of the latest newsletter and update your profile or unsubscribe by ticking the checkbox below.</p>");
INSERT INTO phpn_vocabulary VALUES("5297","ID","_MAIN_ADMIN","Main Admin");
INSERT INTO phpn_vocabulary VALUES("5298","ID","_MAKE_RESERVATION","Make а Reservation");
INSERT INTO phpn_vocabulary VALUES("2178","en","_NEWSLETTER_PRE_SUBSCRIBE_ALERT","Please click on the \"Subscribe\" button to complete the process.");
INSERT INTO phpn_vocabulary VALUES("5294","ID","_LOOK_IN","Look in");
INSERT INTO phpn_vocabulary VALUES("5295","ID","_MAILER","Mailer");
INSERT INTO phpn_vocabulary VALUES("5296","ID","_MAIN","Main");
INSERT INTO phpn_vocabulary VALUES("2181","en","_NEWSLETTER_PRE_UNSUBSCRIBE_ALERT","Please click on the \"Unsubscribe\" button to complete the process.");
INSERT INTO phpn_vocabulary VALUES("5293","ID","_LONG_DESCRIPTION","Long Description");
INSERT INTO phpn_vocabulary VALUES("2184","en","_NEWSLETTER_SUBSCRIBERS","Newsletter Subscribers");
INSERT INTO phpn_vocabulary VALUES("2187","en","_NEWSLETTER_SUBSCRIBE_SUCCESS","Thank you for subscribing to our electronic newsletter. You will receive an e-mail to confirm your subscription.");
INSERT INTO phpn_vocabulary VALUES("5285","ID","_LOADING","loading");
INSERT INTO phpn_vocabulary VALUES("5286","ID","_LOCAL_TIME","Local Time");
INSERT INTO phpn_vocabulary VALUES("5287","ID","_LOCATION","Location");
INSERT INTO phpn_vocabulary VALUES("5288","ID","_LOCATIONS","Locations");
INSERT INTO phpn_vocabulary VALUES("5289","ID","_LOCATION_NAME","Location Name");
INSERT INTO phpn_vocabulary VALUES("5290","ID","_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("5291","ID","_LOGINS","Logins");
INSERT INTO phpn_vocabulary VALUES("5292","ID","_LOGIN_PAGE_MSG","Use a valid administrator username and password to get access to the Administrator Back-End.<br><br>Return to site <a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/lock.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("2190","en","_NEWSLETTER_SUBSCRIBE_TEXT","<p>To receive newsletters from our site, simply enter your email and click on \"Subscribe\" button.</p><p>If you later decide to stop your subscription or change the type of news you receive, simply follow the link at the end of the latest newsletter and update your profile or unsubscribe by ticking the checkbox below.</p>");
INSERT INTO phpn_vocabulary VALUES("5284","ID","_LINK_PARAMETER","Link Parameter");
INSERT INTO phpn_vocabulary VALUES("2193","en","_NEWSLETTER_SUBSCRIPTION_MANAGEMENT","Newsletter Subscription Management");
INSERT INTO phpn_vocabulary VALUES("5282","ID","_LICENSE","License");
INSERT INTO phpn_vocabulary VALUES("5283","ID","_LINK","Link");
INSERT INTO phpn_vocabulary VALUES("2196","en","_NEWSLETTER_UNSUBSCRIBE_SUCCESS","You have been successfully unsubscribed from our newsletter!");
INSERT INTO phpn_vocabulary VALUES("5281","ID","_LEGEND_RESERVED","Room is reserved, but order is not paid yet (pending)");
INSERT INTO phpn_vocabulary VALUES("2199","en","_NEWSLETTER_UNSUBSCRIBE_TEXT","<p>To unsubscribe from our newsletters, enter your email address below and click the unsubscribe button.</p>");
INSERT INTO phpn_vocabulary VALUES("5280","ID","_LEGEND_REFUNDED","Order has been refunded and the room is available again in search");
INSERT INTO phpn_vocabulary VALUES("2202","en","_NEWS_AND_EVENTS","News & Events");
INSERT INTO phpn_vocabulary VALUES("2205","en","_NEWS_MANAGEMENT","News Management");
INSERT INTO phpn_vocabulary VALUES("2208","en","_NEWS_SETTINGS","News Settings");
INSERT INTO phpn_vocabulary VALUES("2211","en","_NEXT","Next");
INSERT INTO phpn_vocabulary VALUES("2214","en","_NIGHT","Night");
INSERT INTO phpn_vocabulary VALUES("2217","en","_NIGHTS","Nights");
INSERT INTO phpn_vocabulary VALUES("2220","en","_NO","No");
INSERT INTO phpn_vocabulary VALUES("2223","en","_NONE","None");
INSERT INTO phpn_vocabulary VALUES("5279","ID","_LEGEND_PREBOOKING","This is a system status for a booking that is in progress, or was not completed (room has been added to Reservation Cart, but still not reserved)");
INSERT INTO phpn_vocabulary VALUES("2226","en","_NOTICE_MODULES_CODE","To add available modules to this page just copy and paste into the text:");
INSERT INTO phpn_vocabulary VALUES("5278","ID","_LEGEND_PENDING","The booking has been created, but not yet been confirmed and reserved");
INSERT INTO phpn_vocabulary VALUES("2229","en","_NOTIFICATION_MSG","Please send me information about specials and discounts!");
INSERT INTO phpn_vocabulary VALUES("5277","ID","_LEGEND_PAYMENT_ERROR","An error occurred while processing customer payments");
INSERT INTO phpn_vocabulary VALUES("2232","en","_NOTIFICATION_STATUS_CHANGED","Notification status changed");
INSERT INTO phpn_vocabulary VALUES("2235","en","_NOT_ALLOWED","Not Allowed");
INSERT INTO phpn_vocabulary VALUES("2238","en","_NOT_AUTHORIZED","You are not authorized to view this page.");
INSERT INTO phpn_vocabulary VALUES("5276","ID","_LEGEND_COMPLETED","Money is paid (fully or partially) and order completed");
INSERT INTO phpn_vocabulary VALUES("2241","en","_NOT_AVAILABLE","N/A");
INSERT INTO phpn_vocabulary VALUES("2244","en","_NOT_PAID_YET","Not paid yet");
INSERT INTO phpn_vocabulary VALUES("2247","en","_NOVEMBER","November");
INSERT INTO phpn_vocabulary VALUES("2250","en","_NO_AVAILABLE","Not Available");
INSERT INTO phpn_vocabulary VALUES("5273","ID","_LEFT_TO_RIGHT","LTR (left-to-right)");
INSERT INTO phpn_vocabulary VALUES("5274","ID","_LEGEND","Legend");
INSERT INTO phpn_vocabulary VALUES("5275","ID","_LEGEND_CANCELED","Order is canceled by admin and the room is available again in search");
INSERT INTO phpn_vocabulary VALUES("2253","en","_NO_BOOKING_FOUND","The number of booking you\'ve entered has not been found in our system! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5272","ID","_LEFT","Left");
INSERT INTO phpn_vocabulary VALUES("2256","en","_NO_COMMENTS_YET","No comments yet.");
INSERT INTO phpn_vocabulary VALUES("2259","en","_NO_CUSTOMER_FOUND","No customer found!");
INSERT INTO phpn_vocabulary VALUES("5269","ID","_LAST_RUN","Last run");
INSERT INTO phpn_vocabulary VALUES("5270","ID","_LAYOUT","Layout");
INSERT INTO phpn_vocabulary VALUES("5271","ID","_LEAVE_YOUR_COMMENT","Leave your comment");
INSERT INTO phpn_vocabulary VALUES("2262","en","_NO_DEFAULT_PERIODS","Default periods not yet defined for this hotel. Click <a href=_HREF_>here</a> to add them.");
INSERT INTO phpn_vocabulary VALUES("2265","en","_NO_NEWS","No news");
INSERT INTO phpn_vocabulary VALUES("5266","ID","_LAST_LOGIN","Last Login");
INSERT INTO phpn_vocabulary VALUES("5267","ID","_LAST_NAME","Last Name");
INSERT INTO phpn_vocabulary VALUES("5268","ID","_LAST_NAME_EMPTY_ALERT","Last Name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("2268","en","_NO_PAYMENT_METHODS_ALERT","No payment options are available! Please try later or contact our technical support.");
INSERT INTO phpn_vocabulary VALUES("5265","ID","_LAST_LOGGED_IP","Last logged IP");
INSERT INTO phpn_vocabulary VALUES("2271","en","_NO_RECORDS_FOUND","No records found");
INSERT INTO phpn_vocabulary VALUES("5264","ID","_LAST_HOTEL_ALERT","You cannot delete last active hotel record!");
INSERT INTO phpn_vocabulary VALUES("2274","en","_NO_RECORDS_PROCESSED","No records found for processing!");
INSERT INTO phpn_vocabulary VALUES("5263","ID","_LAST_CURRENCY_ALERT","You cannot delete last active currency!");
INSERT INTO phpn_vocabulary VALUES("2277","en","_NO_RECORDS_UPDATED","No records were updated!");
INSERT INTO phpn_vocabulary VALUES("5262","ID","_LANG_ORDER_CHANGED","Language order has been successfully changed!");
INSERT INTO phpn_vocabulary VALUES("2280","en","_NO_ROOMS_FOUND","Sorry, there are no rooms that match your search. Please change your search criteria to see more rooms.");
INSERT INTO phpn_vocabulary VALUES("5261","ID","_LANG_NOT_DELETED","Language has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("2283","en","_NO_TEMPLATE","no template");
INSERT INTO phpn_vocabulary VALUES("5259","ID","_LANG_NAME_EMPTY","Language name cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5260","ID","_LANG_NAME_EXISTS","Language with such name already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("2286","en","_NO_USER_EMAIL_EXISTS_ALERT","It seems that you already booked rooms with us! <br>Please click <a href=index.php?customer=reset_account>here</a> to reset your username and get a temporary password. ");
INSERT INTO phpn_vocabulary VALUES("5258","ID","_LANG_MISSED","Missed language to update! Please, try again.");
INSERT INTO phpn_vocabulary VALUES("2289","en","_NO_WRITE_ACCESS_ALERT","Please check you have write access to following directories:");
INSERT INTO phpn_vocabulary VALUES("2292","en","_OCCUPANCY","Occupancy");
INSERT INTO phpn_vocabulary VALUES("2295","en","_OCTOBER","October");
INSERT INTO phpn_vocabulary VALUES("2298","en","_OFF","Off");
INSERT INTO phpn_vocabulary VALUES("5257","ID","_LANG_DELETE_WARNING","Are you sure you want to remove this language? This operation will delete all language vocabulary!");
INSERT INTO phpn_vocabulary VALUES("2301","en","_OFFLINE_LOGIN_ALERT","To log into Admin Panel when site is offline, type in your browser: http://{your_site_address}/index.php?admin=login");
INSERT INTO phpn_vocabulary VALUES("5256","ID","_LANG_DELETE_LAST_ERROR","You cannot delete last language!");
INSERT INTO phpn_vocabulary VALUES("2304","en","_OFFLINE_MESSAGE","Offline Message");
INSERT INTO phpn_vocabulary VALUES("2307","en","_ON","On");
INSERT INTO phpn_vocabulary VALUES("2310","en","_ONLINE","Online");
INSERT INTO phpn_vocabulary VALUES("5255","ID","_LANG_DELETED","Language has been successfully deleted!");
INSERT INTO phpn_vocabulary VALUES("2313","en","_ONLINE_ORDER","On-line Order");
INSERT INTO phpn_vocabulary VALUES("2316","en","_ONLY","Only");
INSERT INTO phpn_vocabulary VALUES("2319","en","_OPEN","Open");
INSERT INTO phpn_vocabulary VALUES("5254","ID","_LANG_ABBREV_EMPTY","Language abbreviation cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("2322","en","_OPEN_ALERT_WINDOW","Open Alert Window");
INSERT INTO phpn_vocabulary VALUES("5253","ID","_LANGUAGE_NAME","Language Name");
INSERT INTO phpn_vocabulary VALUES("2325","en","_OPERATION_BLOCKED","This operation is blocked in Demo Version!");
INSERT INTO phpn_vocabulary VALUES("2328","en","_OPERATION_COMMON_COMPLETED","The operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("5252","ID","_LANGUAGE_EDITED","Language data has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("2331","en","_OPERATION_WAS_ALREADY_COMPLETED","This operation has been already completed!");
INSERT INTO phpn_vocabulary VALUES("5251","ID","_LANGUAGE_EDIT","Edit Language");
INSERT INTO phpn_vocabulary VALUES("2334","en","_OR","or");
INSERT INTO phpn_vocabulary VALUES("2337","en","_ORDER","Order");
INSERT INTO phpn_vocabulary VALUES("5250","ID","_LANGUAGE_ADD_NEW","Add New Language");
INSERT INTO phpn_vocabulary VALUES("2340","en","_ORDERS","Orders");
INSERT INTO phpn_vocabulary VALUES("2343","en","_ORDERS_COUNT","Orders count");
INSERT INTO phpn_vocabulary VALUES("5249","ID","_LANGUAGE_ADDED","New language has been successfully added!");
INSERT INTO phpn_vocabulary VALUES("2346","en","_ORDER_DATE","Order Date");
INSERT INTO phpn_vocabulary VALUES("5247","ID","_LANGUAGES","Languages");
INSERT INTO phpn_vocabulary VALUES("5248","ID","_LANGUAGES_SETTINGS","Languages Settings");
INSERT INTO phpn_vocabulary VALUES("2349","en","_ORDER_ERROR","Cannot complete your order! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5246","ID","_LANGUAGE","Language");
INSERT INTO phpn_vocabulary VALUES("2352","en","_ORDER_NOW","Order Now");
INSERT INTO phpn_vocabulary VALUES("5242","ID","_JUNE","June");
INSERT INTO phpn_vocabulary VALUES("5243","ID","_KEY","Key");
INSERT INTO phpn_vocabulary VALUES("5244","ID","_KEYWORDS","Keywords");
INSERT INTO phpn_vocabulary VALUES("5245","ID","_KEY_DISPLAY_TYPE","Key display type");
INSERT INTO phpn_vocabulary VALUES("2355","en","_ORDER_PLACED_MSG","Thank you! The order has been placed in our system and will be processed shortly. Your booking number is: _BOOKING_NUMBER_.");
INSERT INTO phpn_vocabulary VALUES("5241","ID","_JULY","July");
INSERT INTO phpn_vocabulary VALUES("2358","en","_ORDER_PRICE","Order Price");
INSERT INTO phpn_vocabulary VALUES("5240","ID","_JANUARY","January");
INSERT INTO phpn_vocabulary VALUES("2361","en","_OTHER","Other");
INSERT INTO phpn_vocabulary VALUES("5239","ID","_ITEM_NAME","Item Name");
INSERT INTO phpn_vocabulary VALUES("2364","en","_OUR_LOCATION","Our location");
INSERT INTO phpn_vocabulary VALUES("5238","ID","_ITEMS_LC","items");
INSERT INTO phpn_vocabulary VALUES("2367","en","_OWNER","Owner");
INSERT INTO phpn_vocabulary VALUES("5236","ID","_IS_DEFAULT","Is default");
INSERT INTO phpn_vocabulary VALUES("5237","ID","_ITEMS","Items");
INSERT INTO phpn_vocabulary VALUES("2370","en","_OWNER_NOT_ASSIGNED","You still has not been assigned to any hotel to see the reports.");
INSERT INTO phpn_vocabulary VALUES("2373","en","_PACKAGES","Packages");
INSERT INTO phpn_vocabulary VALUES("2376","en","_PACKAGES_MANAGEMENT","Packages Management");
INSERT INTO phpn_vocabulary VALUES("2379","en","_PAGE","Page");
INSERT INTO phpn_vocabulary VALUES("2382","en","_PAGES","Pages");
INSERT INTO phpn_vocabulary VALUES("5235","ID","_IP_ADDRESS_BLOCKED","Your IP Address is blocked! To resolve this problem, please contact the site administrator.");
INSERT INTO phpn_vocabulary VALUES("2385","en","_PAGE_ADD_NEW","Add New Page");
INSERT INTO phpn_vocabulary VALUES("5234","ID","_IP_ADDRESS","IP Address");
INSERT INTO phpn_vocabulary VALUES("2388","en","_PAGE_CREATED","Page has been successfully created");
INSERT INTO phpn_vocabulary VALUES("5233","ID","_IN_PRODUCTS","In Products");
INSERT INTO phpn_vocabulary VALUES("2391","en","_PAGE_DELETED","Page has been successfully deleted");
INSERT INTO phpn_vocabulary VALUES("2394","en","_PAGE_DELETE_WARNING","Are you sure you want to delete this page?");
INSERT INTO phpn_vocabulary VALUES("2397","en","_PAGE_EDIT_HOME","Edit Home Page");
INSERT INTO phpn_vocabulary VALUES("5232","ID","_INVOICE_SENT_SUCCESS","The invoice has been successfully sent to the customer!");
INSERT INTO phpn_vocabulary VALUES("2400","en","_PAGE_EDIT_PAGES","Edit Pages");
INSERT INTO phpn_vocabulary VALUES("5231","ID","_INVOICE","Invoice");
INSERT INTO phpn_vocabulary VALUES("2403","en","_PAGE_EDIT_SYS_PAGES","Edit System Pages");
INSERT INTO phpn_vocabulary VALUES("2406","en","_PAGE_EXPIRED","The page you requested has expired!");
INSERT INTO phpn_vocabulary VALUES("5230","ID","_INVALID_IMAGE_FILE_TYPE","Uploaded file is not a valid image! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2409","en","_PAGE_HEADER","Page Header");
INSERT INTO phpn_vocabulary VALUES("2412","en","_PAGE_HEADER_EMPTY","Page header cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("5229","ID","_INVALID_FILE_SIZE","Invalid file size: _FILE_SIZE_ (max. allowed: _MAX_ALLOWED_)");
INSERT INTO phpn_vocabulary VALUES("2415","en","_PAGE_KEY_EMPTY","Page key cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("2418","en","_PAGE_LINK_TOO_LONG","Menu link too long!");
INSERT INTO phpn_vocabulary VALUES("5228","ID","_INTERNAL_USE_TOOLTIP","For internal use only");
INSERT INTO phpn_vocabulary VALUES("2421","en","_PAGE_MANAGEMENT","Pages Management");
INSERT INTO phpn_vocabulary VALUES("2424","en","_PAGE_NOT_CREATED","Page has not been created!");
INSERT INTO phpn_vocabulary VALUES("2427","en","_PAGE_NOT_DELETED","Page has not been deleted!");
INSERT INTO phpn_vocabulary VALUES("2430","en","_PAGE_NOT_EXISTS","The page you attempted to access does not exist");
INSERT INTO phpn_vocabulary VALUES("5227","ID","_INTEGRATION_MESSAGE","Copy the code below and put it in the appropriate place of your web site to get a <b>Search Availability</b> block.");
INSERT INTO phpn_vocabulary VALUES("2433","en","_PAGE_NOT_FOUND","No Pages Found");
INSERT INTO phpn_vocabulary VALUES("5226","ID","_INTEGRATION","Integration");
INSERT INTO phpn_vocabulary VALUES("2436","en","_PAGE_NOT_SAVED","Page has not been saved!");
INSERT INTO phpn_vocabulary VALUES("2439","en","_PAGE_ORDER_CHANGED","Page order has been successfully changed!");
INSERT INTO phpn_vocabulary VALUES("2442","en","_PAGE_REMOVED","Page has been successfully removed!");
INSERT INTO phpn_vocabulary VALUES("5225","ID","_INSTALL_PHP_EXISTS","File <b>install.php</b> and/or directory <b>install/</b> still exists. For security reasons please remove them immediately!");
INSERT INTO phpn_vocabulary VALUES("2445","en","_PAGE_REMOVE_WARNING","Are you sure you want to move this page to the Trash?");
INSERT INTO phpn_vocabulary VALUES("5223","ID","_INSTALL","Install");
INSERT INTO phpn_vocabulary VALUES("5224","ID","_INSTALLED","Installed");
INSERT INTO phpn_vocabulary VALUES("2448","en","_PAGE_RESTORED","Page has been successfully restored!");
INSERT INTO phpn_vocabulary VALUES("5222","ID","_INITIAL_FEE","Initial Fee");
INSERT INTO phpn_vocabulary VALUES("2451","en","_PAGE_RESTORE_WARNING","Are you sure you want to restore this page?");
INSERT INTO phpn_vocabulary VALUES("5221","ID","_INFO_AND_STATISTICS","Information and Statistics");
INSERT INTO phpn_vocabulary VALUES("2454","en","_PAGE_SAVED","Page has been successfully saved!");
INSERT INTO phpn_vocabulary VALUES("5220","ID","_INCOME","Income");
INSERT INTO phpn_vocabulary VALUES("2457","en","_PAGE_TEXT","Page text");
INSERT INTO phpn_vocabulary VALUES("2460","en","_PAGE_TITLE","Page Title");
INSERT INTO phpn_vocabulary VALUES("2463","en","_PAGE_UNKNOWN","Unknown page!");
INSERT INTO phpn_vocabulary VALUES("5219","ID","_IMAGE_VERIFY_EMPTY","You must enter image verification code!");
INSERT INTO phpn_vocabulary VALUES("2466","en","_PARAMETER","Parameter");
INSERT INTO phpn_vocabulary VALUES("2469","en","_PARTIALLY_AVAILABLE","Partially Available");
INSERT INTO phpn_vocabulary VALUES("5218","ID","_IMAGE_VERIFICATION","Image verification");
INSERT INTO phpn_vocabulary VALUES("2472","en","_PARTIAL_PRICE","Partial Price");
INSERT INTO phpn_vocabulary VALUES("5217","ID","_IMAGES","Images");
INSERT INTO phpn_vocabulary VALUES("2475","en","_PASSWORD","Password");
INSERT INTO phpn_vocabulary VALUES("5214","ID","_HOURS","hours");
INSERT INTO phpn_vocabulary VALUES("5215","ID","_ICON_IMAGE","Icon image");
INSERT INTO phpn_vocabulary VALUES("5216","ID","_IMAGE","Image");
INSERT INTO phpn_vocabulary VALUES("2478","en","_PASSWORD_ALREADY_SENT","Password has been already sent to your email. Please try again later.");
INSERT INTO phpn_vocabulary VALUES("5213","ID","_HOUR","Hour");
INSERT INTO phpn_vocabulary VALUES("2481","en","_PASSWORD_CHANGED","Password has been changed.");
INSERT INTO phpn_vocabulary VALUES("5212","ID","_HOTEL_RESERVATION_ID","Hotel Reservation ID");
INSERT INTO phpn_vocabulary VALUES("2484","en","_PASSWORD_DO_NOT_MATCH","Password and confirmation do not match!");
INSERT INTO phpn_vocabulary VALUES("5211","ID","_HOTEL_OWNERS","Hotel Owners");
INSERT INTO phpn_vocabulary VALUES("2487","en","_PASSWORD_FORGOTTEN","Forgotten Password");
INSERT INTO phpn_vocabulary VALUES("5207","ID","_HOTEL_DESCRIPTION","Hotel Description");
INSERT INTO phpn_vocabulary VALUES("5208","ID","_HOTEL_INFO","Hotel Info");
INSERT INTO phpn_vocabulary VALUES("5209","ID","_HOTEL_MANAGEMENT","Hotel Management");
INSERT INTO phpn_vocabulary VALUES("5210","ID","_HOTEL_OWNER","Hotel Owner");
INSERT INTO phpn_vocabulary VALUES("2490","en","_PASSWORD_FORGOTTEN_PAGE_MSG","Use a valid administrator e-mail to restore your password to the Administrator Back-End.<br><br>Return to site <a href=\'index.php\'>Home Page</a><br><br><img align=\'center\' src=\'images/password.png\' alt=\'\' width=\'92px\'>");
INSERT INTO phpn_vocabulary VALUES("2493","en","_PASSWORD_IS_EMPTY","Passwords must not be empty and at least 6 characters!");
INSERT INTO phpn_vocabulary VALUES("5205","ID","_HOTELS_MANAGEMENT","Hotels Management");
INSERT INTO phpn_vocabulary VALUES("5206","ID","_HOTEL_DELETE_ALERT","Are you sure you want to delete this hotel? Remember: after completing this action all related data to this hotel could not be restored!");
INSERT INTO phpn_vocabulary VALUES("2496","en","_PASSWORD_NOT_CHANGED","Password has not been changed. Please try again!");
INSERT INTO phpn_vocabulary VALUES("5204","ID","_HOTELS_INFO","Hotels Info");
INSERT INTO phpn_vocabulary VALUES("2499","en","_PASSWORD_RECOVERY_MSG","To recover your password, please enter your e-mail address and a link will be emailed to you.");
INSERT INTO phpn_vocabulary VALUES("5202","ID","_HOTELS","Hotels");
INSERT INTO phpn_vocabulary VALUES("5203","ID","_HOTELS_AND_ROMS","Hotels and Rooms");
INSERT INTO phpn_vocabulary VALUES("2502","en","_PASSWORD_SUCCESSFULLY_SENT","Your password has been successfully sent to the email address.");
INSERT INTO phpn_vocabulary VALUES("2505","en","_PAST_TIME_ALERT","You cannot perform reservation in the past! Please re-enter dates.");
INSERT INTO phpn_vocabulary VALUES("2508","en","_PAYED_BY","Payed by");
INSERT INTO phpn_vocabulary VALUES("2511","en","_PAYMENT","Payment");
INSERT INTO phpn_vocabulary VALUES("2514","en","_PAYMENTS","Payments");
INSERT INTO phpn_vocabulary VALUES("5201","ID","_HOTELOWNER_WELCOME_TEXT","Welcome to Hotel Owner Control Panel! With this Control Panel you can easily manage your hotels, customers, reservations and perform a full hotel site management.");
INSERT INTO phpn_vocabulary VALUES("2517","en","_PAYMENT_COMPANY_ACCOUNT","Payment Company Account");
INSERT INTO phpn_vocabulary VALUES("5200","ID","_HOTEL","Hotel");
INSERT INTO phpn_vocabulary VALUES("2520","en","_PAYMENT_DATE","Payment Date");
INSERT INTO phpn_vocabulary VALUES("5199","ID","_HOME","Home");
INSERT INTO phpn_vocabulary VALUES("2523","en","_PAYMENT_DETAILS","Payment Details");
INSERT INTO phpn_vocabulary VALUES("5198","ID","_HIDE","Hide");
INSERT INTO phpn_vocabulary VALUES("2526","en","_PAYMENT_ERROR","Payment error");
INSERT INTO phpn_vocabulary VALUES("5197","ID","_HIDDEN","Hidden");
INSERT INTO phpn_vocabulary VALUES("2529","en","_PAYMENT_METHOD","Payment Method");
INSERT INTO phpn_vocabulary VALUES("5196","ID","_HELP","Help");
INSERT INTO phpn_vocabulary VALUES("2532","en","_PAYMENT_METHODS","Payment Methods");
INSERT INTO phpn_vocabulary VALUES("2535","en","_PAYMENT_REQUIRED","Payment Required");
INSERT INTO phpn_vocabulary VALUES("2538","en","_PAYMENT_SUM","Payment Sum");
INSERT INTO phpn_vocabulary VALUES("5195","ID","_HEADER_IS_EMPTY","Header cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2541","en","_PAYMENT_TYPE","Payment Type");
INSERT INTO phpn_vocabulary VALUES("2544","en","_PAYPAL","PayPal");
INSERT INTO phpn_vocabulary VALUES("5191","ID","_HDR_TEMPLATE","Template");
INSERT INTO phpn_vocabulary VALUES("5192","ID","_HDR_TEXT_DIRECTION","Text Direction");
INSERT INTO phpn_vocabulary VALUES("5193","ID","_HEADER","Header");
INSERT INTO phpn_vocabulary VALUES("5194","ID","_HEADERS_AND_FOOTERS","Headers & Footers");
INSERT INTO phpn_vocabulary VALUES("2547","en","_PAYPAL_NOTICE","Save time. Pay securely using your stored payment information.<br />Pay with <b>credit card</b>, <b>bank account</b> or <b>PayPal</b> account balance.");
INSERT INTO phpn_vocabulary VALUES("5190","ID","_HDR_SLOGAN_TEXT","Slogan");
INSERT INTO phpn_vocabulary VALUES("2550","en","_PAYPAL_ORDER","PayPal Order");
INSERT INTO phpn_vocabulary VALUES("5189","ID","_HDR_HEADER_TEXT","Header Text");
INSERT INTO phpn_vocabulary VALUES("2553","en","_PAY_ON_ARRIVAL","Pay on Arrival");
INSERT INTO phpn_vocabulary VALUES("5188","ID","_HDR_FOOTER_TEXT","Footer Text");
INSERT INTO phpn_vocabulary VALUES("2556","en","_PC_BILLING_INFORMATION_TEXT","billing information: address, city, country etc.");
INSERT INTO phpn_vocabulary VALUES("2559","en","_PC_BOOKING_DETAILS_TEXT","order details, list of purchased products etc.");
INSERT INTO phpn_vocabulary VALUES("5187","ID","_GROUP_TIME_OVERLAPPING_ALERT","This period of time (fully or partially) is already chosen for selected group! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2562","en","_PC_BOOKING_NUMBER_TEXT","the number of order");
INSERT INTO phpn_vocabulary VALUES("5186","ID","_GROUP_NAME","Group Name");
INSERT INTO phpn_vocabulary VALUES("2565","en","_PC_EVENT_TEXT","the title of event");
INSERT INTO phpn_vocabulary VALUES("5185","ID","_GROUP","Group");
INSERT INTO phpn_vocabulary VALUES("2568","en","_PC_FIRST_NAME_TEXT","the first name of customer or admin");
INSERT INTO phpn_vocabulary VALUES("5183","ID","_GENERATE","Generate");
INSERT INTO phpn_vocabulary VALUES("5184","ID","_GLOBAL","Global");
INSERT INTO phpn_vocabulary VALUES("2571","en","_PC_HOTEL_INFO_TEXT","information about hotel: name, address, telephone, fax etc.");
INSERT INTO phpn_vocabulary VALUES("5182","ID","_GENERAL_SETTINGS","General Settings");
INSERT INTO phpn_vocabulary VALUES("2574","en","_PC_LAST_NAME_TEXT","the last name of customer or admin");
INSERT INTO phpn_vocabulary VALUES("5180","ID","_GENERAL","General");
INSERT INTO phpn_vocabulary VALUES("5181","ID","_GENERAL_INFO","General Info");
INSERT INTO phpn_vocabulary VALUES("2577","en","_PC_PERSONAL_INFORMATION_TEXT","personal information of customer: first name, last name etc.");
INSERT INTO phpn_vocabulary VALUES("5179","ID","_GALLERY_SETTINGS","Gallery Settings");
INSERT INTO phpn_vocabulary VALUES("2580","en","_PC_REGISTRATION_CODE_TEXT","confirmation code for new account");
INSERT INTO phpn_vocabulary VALUES("5178","ID","_GALLERY_MANAGEMENT","Gallery Management");
INSERT INTO phpn_vocabulary VALUES("2583","en","_PC_STATUS_DESCRIPTION_TEXT","description of payment status");
INSERT INTO phpn_vocabulary VALUES("5177","ID","_GALLERY","Gallery");
INSERT INTO phpn_vocabulary VALUES("2586","en","_PC_USER_EMAIL_TEXT","email of user");
INSERT INTO phpn_vocabulary VALUES("5176","ID","_FULL_PRICE","Full Price");
INSERT INTO phpn_vocabulary VALUES("2589","en","_PC_USER_NAME_TEXT","username (login) of user");
INSERT INTO phpn_vocabulary VALUES("5175","ID","_FULLY_BOOKED","fully booked/unavailable");
INSERT INTO phpn_vocabulary VALUES("2592","en","_PC_USER_PASSWORD_TEXT","password for customer or admin");
INSERT INTO phpn_vocabulary VALUES("2595","en","_PC_WEB_SITE_BASED_URL_TEXT","web site base url");
INSERT INTO phpn_vocabulary VALUES("2598","en","_PC_WEB_SITE_URL_TEXT","web site url");
INSERT INTO phpn_vocabulary VALUES("5174","ID","_FROM_TO_DATE_ALERT","Date \'To\' must be the same or later than date \'From\'! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2601","en","_PC_YEAR_TEXT","current year in YYYY format");
INSERT INTO phpn_vocabulary VALUES("5173","ID","_FROM","From");
INSERT INTO phpn_vocabulary VALUES("2604","en","_PENDING","Pending");
INSERT INTO phpn_vocabulary VALUES("5172","ID","_FRIDAY","Friday");
INSERT INTO phpn_vocabulary VALUES("2607","en","_PEOPLE_ARRIVING","People Arriving");
INSERT INTO phpn_vocabulary VALUES("5171","ID","_FRI","Fri");
INSERT INTO phpn_vocabulary VALUES("2610","en","_PEOPLE_DEPARTING","People Departing");
INSERT INTO phpn_vocabulary VALUES("5170","ID","_FR","Fr");
INSERT INTO phpn_vocabulary VALUES("2613","en","_PEOPLE_STAYING","People Staying");
INSERT INTO phpn_vocabulary VALUES("5168","ID","_FOUND_HOTELS","Found Hotels");
INSERT INTO phpn_vocabulary VALUES("5169","ID","_FOUND_ROOMS","Found Rooms");
INSERT INTO phpn_vocabulary VALUES("2616","en","_PERCENTAGE_MAX_ALOWED_VALUE","The maximum allowed value for percentage is 99%! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2619","en","_PERFORM_OPERATION_COMMON_ALERT","Are you sure you want to perform this operation?");
INSERT INTO phpn_vocabulary VALUES("5167","ID","_FOR_BOOKING","for booking #");
INSERT INTO phpn_vocabulary VALUES("2622","en","_PERIODS","Periods");
INSERT INTO phpn_vocabulary VALUES("5166","ID","_FORM","Form");
INSERT INTO phpn_vocabulary VALUES("2625","en","_PERSONAL_DATA_SAVED","Your billing information has been updated.");
INSERT INTO phpn_vocabulary VALUES("5165","ID","_FORGOT_PASSWORD","Forgot your password?");
INSERT INTO phpn_vocabulary VALUES("2628","en","_PERSONAL_DETAILS","Personal Details");
INSERT INTO phpn_vocabulary VALUES("2631","en","_PERSONAL_INFORMATION","Personal Information");
INSERT INTO phpn_vocabulary VALUES("2634","en","_PERSON_PER_NIGHT","Person/Per Night");
INSERT INTO phpn_vocabulary VALUES("2637","en","_PER_NIGHT","Per Night");
INSERT INTO phpn_vocabulary VALUES("2640","en","_PHONE","Phone");
INSERT INTO phpn_vocabulary VALUES("2643","en","_PHONE_EMPTY_ALERT","Phone field cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2646","en","_PICK_DATE","Open calendar and pick a date");
INSERT INTO phpn_vocabulary VALUES("5164","ID","_FORCE_SSL_ALERT","Force site access to always occur under SSL (https) for selected areas. You or site visitors will not be able to access selected areas under non-ssl. Note, you must have SSL enabled on your server to make this option works.");
INSERT INTO phpn_vocabulary VALUES("2649","en","_PLACEMENT","Placement");
INSERT INTO phpn_vocabulary VALUES("5163","ID","_FORCE_SSL","Force SSL");
INSERT INTO phpn_vocabulary VALUES("2652","en","_PLACE_ORDER","Place Order");
INSERT INTO phpn_vocabulary VALUES("2655","en","_PLAY","Play");
INSERT INTO phpn_vocabulary VALUES("2658","en","_POPULARITY","Popularity");
INSERT INTO phpn_vocabulary VALUES("5162","ID","_FOOTER_IS_EMPTY","Footer cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2661","en","_POPULAR_SEARCH","Popular Search");
INSERT INTO phpn_vocabulary VALUES("5161","ID","_FOLLOW_US","Follow Us");
INSERT INTO phpn_vocabulary VALUES("2664","en","_POSTED_ON","Posted on");
INSERT INTO phpn_vocabulary VALUES("5159","ID","_FIRST_NIGHT","First Night");
INSERT INTO phpn_vocabulary VALUES("5160","ID","_FIXED_SUM","Fixed Sum");
INSERT INTO phpn_vocabulary VALUES("2667","en","_POST_COM_REGISTERED_ALERT","Your need to be registered to post comments.");
INSERT INTO phpn_vocabulary VALUES("2670","en","_PREBOOKING","Pre-Booking");
INSERT INTO phpn_vocabulary VALUES("5158","ID","_FIRST_NAME_EMPTY_ALERT","First Name cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2673","en","_PREDEFINED_CONSTANTS","Predefined Constants");
INSERT INTO phpn_vocabulary VALUES("5157","ID","_FIRST_NAME","First Name");
INSERT INTO phpn_vocabulary VALUES("2676","en","_PREFERRED_LANGUAGE","Preferred Language");
INSERT INTO phpn_vocabulary VALUES("2679","en","_PREVIEW","Preview");
INSERT INTO phpn_vocabulary VALUES("5156","ID","_FINISH_PUBLISHING","Finish Publishing");
INSERT INTO phpn_vocabulary VALUES("2682","en","_PREVIOUS","Previous");
INSERT INTO phpn_vocabulary VALUES("5155","ID","_FINISH_DATE","Finish Date");
INSERT INTO phpn_vocabulary VALUES("2685","en","_PRE_PAYMENT","Pre-Payment");
INSERT INTO phpn_vocabulary VALUES("2688","en","_PRICE","Price");
INSERT INTO phpn_vocabulary VALUES("5154","ID","_FILTER_BY","Filter by");
INSERT INTO phpn_vocabulary VALUES("2691","en","_PRICES","Prices");
INSERT INTO phpn_vocabulary VALUES("2694","en","_PRICE_EMPTY_ALERT","Field price cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5153","ID","_FILE_DELETING_ERROR","An error occurred while deleting file! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("2697","en","_PRICE_FORMAT","Price Format");
INSERT INTO phpn_vocabulary VALUES("5152","ID","_FILED_UNIQUE_VALUE_ALERT","The field _FIELD_ accepts only unique values - please re-enter!");
INSERT INTO phpn_vocabulary VALUES("2700","en","_PRICE_FORMAT_ALERT","Allows to display prices for visitor in appropriate format");
INSERT INTO phpn_vocabulary VALUES("2703","en","_PRICE_H_L","price (from highest)");
INSERT INTO phpn_vocabulary VALUES("2706","en","_PRICE_L_H","price (from lowest)");
INSERT INTO phpn_vocabulary VALUES("2709","en","_PRINT","Print");
INSERT INTO phpn_vocabulary VALUES("5151","ID","_FIELD_VALUE_MINIMUM","_FIELD_ value should not be less then _MIN_! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2712","en","_PRIVILEGES","Privileges");
INSERT INTO phpn_vocabulary VALUES("2715","en","_PRIVILEGES_MANAGEMENT","Privileges Management");
INSERT INTO phpn_vocabulary VALUES("2718","en","_PRODUCT","Product");
INSERT INTO phpn_vocabulary VALUES("2721","en","_PRODUCTS","Products");
INSERT INTO phpn_vocabulary VALUES("2724","en","_PRODUCTS_COUNT","Products count");
INSERT INTO phpn_vocabulary VALUES("2727","en","_PRODUCTS_MANAGEMENT","Products Management");
INSERT INTO phpn_vocabulary VALUES("5150","ID","_FIELD_VALUE_EXCEEDED","_FIELD_ has exceeded the maximum allowed value _MAX_! Please re-enter or change total rooms number <a href=\'index.php?admin=mod_rooms_management\'>here</a>.");
INSERT INTO phpn_vocabulary VALUES("2730","en","_PRODUCT_DESCRIPTION","Product Description");
INSERT INTO phpn_vocabulary VALUES("2733","en","_PROMO_AND_DISCOUNTS","Promo and Discounts");
INSERT INTO phpn_vocabulary VALUES("2736","en","_PROMO_CODE_OR_COUPON","Promo Code or Discount Coupon");
INSERT INTO phpn_vocabulary VALUES("5149","ID","_FIELD_MUST_BE_UNSIGNED_INT","Field _FIELD_ must be an unsigned integer value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2739","en","_PROMO_COUPON_NOTICE","If you have a promo code or discount coupon please enter it here");
INSERT INTO phpn_vocabulary VALUES("2742","en","_PUBLIC","Public");
INSERT INTO phpn_vocabulary VALUES("5148","ID","_FIELD_MUST_BE_UNSIGNED_FLOAT","Field _FIELD_ must be an unsigned float value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2745","en","_PUBLISHED","Published");
INSERT INTO phpn_vocabulary VALUES("2748","en","_PUBLISH_YOUR_COMMENT","Publish your comment");
INSERT INTO phpn_vocabulary VALUES("2751","en","_QTY","Qty");
INSERT INTO phpn_vocabulary VALUES("5147","ID","_FIELD_MUST_BE_TEXT","_FIELD_ value must be a text! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2754","en","_QUANTITY","Quantity");
INSERT INTO phpn_vocabulary VALUES("2757","en","_QUESTION","Question");
INSERT INTO phpn_vocabulary VALUES("2760","en","_QUESTIONS","Questions");
INSERT INTO phpn_vocabulary VALUES("2763","en","_RATE","Rate");
INSERT INTO phpn_vocabulary VALUES("2766","en","_RATE_PER_NIGHT","Rate per night");
INSERT INTO phpn_vocabulary VALUES("5146","ID","_FIELD_MUST_BE_SIZE_VALUE","Field _FIELD_ must be a valid HTML size property in \'px\', \'pt\', \'em\' or \'%\' units! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2769","en","_RATE_PER_NIGHT_AVG","Average rate per night");
INSERT INTO phpn_vocabulary VALUES("2772","en","_RATINGS","Ratings");
INSERT INTO phpn_vocabulary VALUES("2775","en","_RATINGS_SETTINGS","Ratings Settings");
INSERT INTO phpn_vocabulary VALUES("5145","ID","_FIELD_MUST_BE_POSITIVE_INTEGER","Field _FIELD_ must be a positive integer number!");
INSERT INTO phpn_vocabulary VALUES("2778","en","_REACTIVATION_EMAIL","Resend Activation Email");
INSERT INTO phpn_vocabulary VALUES("2781","en","_READY","Ready");
INSERT INTO phpn_vocabulary VALUES("2784","en","_READ_MORE","Read more");
INSERT INTO phpn_vocabulary VALUES("2787","en","_REAL_TIME_CAMPAIGN","Real Time Campaign");
INSERT INTO phpn_vocabulary VALUES("5144","ID","_FIELD_MUST_BE_POSITIVE_INT","Field _FIELD_ must be a positive integer value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2790","en","_REASON","Reason");
INSERT INTO phpn_vocabulary VALUES("2793","en","_RECORD_WAS_DELETED_COMMON","The record has been successfully deleted!");
INSERT INTO phpn_vocabulary VALUES("2796","en","_REFRESH","Refresh");
INSERT INTO phpn_vocabulary VALUES("2799","en","_REFUNDED","Refunded");
INSERT INTO phpn_vocabulary VALUES("5143","ID","_FIELD_MUST_BE_PASSWORD","_FIELD_ must be 6 characters at least and consist of letters and digits! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2802","en","_REGISTERED","Registered");
INSERT INTO phpn_vocabulary VALUES("2805","en","_REGISTERED_FROM_IP","Registered from IP");
INSERT INTO phpn_vocabulary VALUES("2808","en","_REGISTRATIONS","Registrations");
INSERT INTO phpn_vocabulary VALUES("5142","ID","_FIELD_MUST_BE_NUMERIC_POSITIVE","Field _FIELD_ must be a positive numeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2811","en","_REGISTRATION_CODE","Registration code");
INSERT INTO phpn_vocabulary VALUES("2814","en","_REGISTRATION_CONFIRMATION","Registration Confirmation");
INSERT INTO phpn_vocabulary VALUES("5141","ID","_FIELD_MUST_BE_NUMERIC","Field _FIELD_ must be a numeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2817","en","_REGISTRATION_FORM","Registration Form");
INSERT INTO phpn_vocabulary VALUES("5139","ID","_FIELD_MUST_BE_FLOAT_POSITIVE","Field _FIELD_ must be a positive float number value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5140","ID","_FIELD_MUST_BE_IP_ADDRESS","_FIELD_ must be a valid IP Address! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2820","en","_REGISTRATION_NOT_COMPLETED","Your registration process is not yet complete! Please check again your email for further instructions or click <a href=index.php?customer=resend_activation>here</a> to resend them again.");
INSERT INTO phpn_vocabulary VALUES("2823","en","_REMEMBER_ME","Remember Me");
INSERT INTO phpn_vocabulary VALUES("2826","en","_REMOVE","Remove");
INSERT INTO phpn_vocabulary VALUES("2829","en","_REMOVED","Removed");
INSERT INTO phpn_vocabulary VALUES("5138","ID","_FIELD_MUST_BE_FLOAT","Field _FIELD_ must be a float number value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2832","en","_REMOVE_ACCOUNT","Remove Account");
INSERT INTO phpn_vocabulary VALUES("2835","en","_REMOVE_ACCOUNT_ALERT","Are you sure you want to remove your account?");
INSERT INTO phpn_vocabulary VALUES("5137","ID","_FIELD_MUST_BE_EMAIL","_FIELD_ must be in valid email format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2838","en","_REMOVE_ACCOUNT_WARNING","If you don\'t think you will use this site again and would like your account deleted, we can take care of this for you. Keep in mind, that you will not be able to reactivate your account or retrieve any of the content or information that was added. If you would like your account deleted, then click Remove button");
INSERT INTO phpn_vocabulary VALUES("5136","ID","_FIELD_MUST_BE_BOOLEAN","Field _FIELD_ value must be \'yes\' or \'no\'! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2841","en","_REMOVE_LAST_COUNTRY_ALERT","The country selected has not been deleted, because you must have at least one active country for correct work of the site!");
INSERT INTO phpn_vocabulary VALUES("2844","en","_REMOVE_ROOM_FROM_CART","Remove room from the cart");
INSERT INTO phpn_vocabulary VALUES("5135","ID","_FIELD_MUST_BE_ALPHA_NUMERIC","_FIELD_ must be an alphanumeric value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2847","en","_REPORTS","Reports");
INSERT INTO phpn_vocabulary VALUES("2850","en","_RESEND_ACTIVATION_EMAIL","Resend Activation Email");
INSERT INTO phpn_vocabulary VALUES("5134","ID","_FIELD_MUST_BE_ALPHA","_FIELD_ must be an alphabetic value! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2853","en","_RESEND_ACTIVATION_EMAIL_MSG","Please enter your email address then click on Send button. You will receive the activation email shortly.");
INSERT INTO phpn_vocabulary VALUES("2856","en","_RESERVATION","Reservation");
INSERT INTO phpn_vocabulary VALUES("5133","ID","_FIELD_MIN_LENGTH_ALERT","The length of the field _FIELD_ cannot  be less than _LENGTH_ characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2859","en","_RESERVATIONS","Reservations");
INSERT INTO phpn_vocabulary VALUES("2862","en","_RESERVATION_CART","Reservation Cart");
INSERT INTO phpn_vocabulary VALUES("5132","ID","_FIELD_LENGTH_EXCEEDED","_FIELD_ has exceeded the maximum allowed size: _LENGTH_ characters! Please re-enter. ");
INSERT INTO phpn_vocabulary VALUES("2865","en","_RESERVATION_CART_IS_EMPTY_ALERT","Your reservation cart is empty!");
INSERT INTO phpn_vocabulary VALUES("2868","en","_RESERVATION_DETAILS","Reservation Details");
INSERT INTO phpn_vocabulary VALUES("2871","en","_RESERVED","Reserved");
INSERT INTO phpn_vocabulary VALUES("2874","en","_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("2877","en","_RESET_ACCOUNT","Reset Account");
INSERT INTO phpn_vocabulary VALUES("5131","ID","_FIELD_LENGTH_ALERT","The length of the field _FIELD_ must be less than _LENGTH_ characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2880","en","_RESTAURANT","Restaurant");
INSERT INTO phpn_vocabulary VALUES("2883","en","_RESTORE","Restore");
INSERT INTO phpn_vocabulary VALUES("2886","en","_RETYPE_PASSWORD","Retype Password");
INSERT INTO phpn_vocabulary VALUES("5130","ID","_FIELD_CANNOT_BE_EMPTY","Field _FIELD_ cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2889","en","_RIGHT","Right");
INSERT INTO phpn_vocabulary VALUES("5129","ID","_FEBRUARY","February");
INSERT INTO phpn_vocabulary VALUES("2892","en","_RIGHT_TO_LEFT","RTL (right-to-left)");
INSERT INTO phpn_vocabulary VALUES("5128","ID","_FAX","Fax");
INSERT INTO phpn_vocabulary VALUES("2895","en","_ROLES_AND_PRIVILEGES","Roles & Privileges");
INSERT INTO phpn_vocabulary VALUES("2898","en","_ROLES_MANAGEMENT","Roles Management");
INSERT INTO phpn_vocabulary VALUES("5127","ID","_FAQ_SETTINGS","FAQ Settings");
INSERT INTO phpn_vocabulary VALUES("2901","en","_ROOMS","Rooms");
INSERT INTO phpn_vocabulary VALUES("5126","ID","_FAQ_MANAGEMENT","FAQ Management");
INSERT INTO phpn_vocabulary VALUES("2904","en","_ROOMS_AVAILABILITY","Rooms Availability");
INSERT INTO phpn_vocabulary VALUES("5125","ID","_FAQ","FAQ");
INSERT INTO phpn_vocabulary VALUES("2907","en","_ROOMS_COUNT","Number of Rooms (in the Hotel)");
INSERT INTO phpn_vocabulary VALUES("5124","ID","_FACILITIES","Facilities");
INSERT INTO phpn_vocabulary VALUES("2910","en","_ROOMS_FACILITIES","Rooms Facilities");
INSERT INTO phpn_vocabulary VALUES("2913","en","_ROOMS_LAST","last room");
INSERT INTO phpn_vocabulary VALUES("5123","ID","_EXTRA_BED_CHARGE","Extra Bed Charge");
INSERT INTO phpn_vocabulary VALUES("2916","en","_ROOMS_LEFT","rooms left");
INSERT INTO phpn_vocabulary VALUES("5122","ID","_EXTRA_BEDS","Extra Beds");
INSERT INTO phpn_vocabulary VALUES("2919","en","_ROOMS_MANAGEMENT","Rooms Management");
INSERT INTO phpn_vocabulary VALUES("5121","ID","_EXTRA_BED","Extra Bed");
INSERT INTO phpn_vocabulary VALUES("2922","en","_ROOMS_OCCUPANCY","Rooms Occupancy");
INSERT INTO phpn_vocabulary VALUES("5120","ID","_EXTRAS_SUBTOTAL","Extras Subtotal");
INSERT INTO phpn_vocabulary VALUES("2925","en","_ROOMS_RESERVATION","Rooms Reservation");
INSERT INTO phpn_vocabulary VALUES("5119","ID","_EXTRAS_MANAGEMENT","Extras Management");
INSERT INTO phpn_vocabulary VALUES("2928","en","_ROOMS_SETTINGS","Rooms Settings");
INSERT INTO phpn_vocabulary VALUES("5118","ID","_EXTRAS","Extras");
INSERT INTO phpn_vocabulary VALUES("2931","en","_ROOM_AREA","Room Area");
INSERT INTO phpn_vocabulary VALUES("5117","ID","_EXPORT","Export");
INSERT INTO phpn_vocabulary VALUES("2934","en","_ROOM_DESCRIPTION","Room Description");
INSERT INTO phpn_vocabulary VALUES("5116","ID","_EXPIRED","Expired");
INSERT INTO phpn_vocabulary VALUES("2937","en","_ROOM_DETAILS","Room Details");
INSERT INTO phpn_vocabulary VALUES("2940","en","_ROOM_FACILITIES","Room Facilities");
INSERT INTO phpn_vocabulary VALUES("5115","ID","_EXPAND_PANEL","Expand navigation panel");
INSERT INTO phpn_vocabulary VALUES("2943","en","_ROOM_FACILITIES_MANAGEMENT","Room Facilities Management");
INSERT INTO phpn_vocabulary VALUES("2946","en","_ROOM_NOT_FOUND","Room has not been found!");
INSERT INTO phpn_vocabulary VALUES("2949","en","_ROOM_NUMBERS","Room Numbers");
INSERT INTO phpn_vocabulary VALUES("5114","ID","_EVENT_USER_ALREADY_REGISTERED","Member with such email is already registered to this event! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("2952","en","_ROOM_PRICE","Room Price");
INSERT INTO phpn_vocabulary VALUES("2955","en","_ROOM_PRICES_WERE_ADDED","Room prices for new period were successfully added!");
INSERT INTO phpn_vocabulary VALUES("2958","en","_ROOM_TYPE","Room Type");
INSERT INTO phpn_vocabulary VALUES("5112","ID","_EVENTS","Events");
INSERT INTO phpn_vocabulary VALUES("5113","ID","_EVENT_REGISTRATION_COMPLETED","Thank you for your interest! You have just successfully registered to this event.");
INSERT INTO phpn_vocabulary VALUES("2961","en","_ROOM_WAS_ADDED","Room has been successfully added to your reservation!");
INSERT INTO phpn_vocabulary VALUES("5111","ID","_ENTIRE_SITE","Entire Site");
INSERT INTO phpn_vocabulary VALUES("2964","en","_ROOM_WAS_REMOVED","Selected room has been successfully removed from your Reservation Cart!");
INSERT INTO phpn_vocabulary VALUES("2967","en","_ROWS","Rows");
INSERT INTO phpn_vocabulary VALUES("5110","ID","_ENTER_EMAIL_ADDRESS","(Please enter ONLY real email address)");
INSERT INTO phpn_vocabulary VALUES("2970","en","_RSS_FEED_TYPE","RSS Feed Type");
INSERT INTO phpn_vocabulary VALUES("5107","ID","_EMPTY","Empty");
INSERT INTO phpn_vocabulary VALUES("5108","ID","_ENTER_BOOKING_NUMBER","Enter your booking number");
INSERT INTO phpn_vocabulary VALUES("5109","ID","_ENTER_CONFIRMATION_CODE","Enter Confirmation Code");
INSERT INTO phpn_vocabulary VALUES("2973","en","_RSS_FILE_ERROR","Cannot open RSS file to add new item! Please check your access rights to <b>feeds/</b> directory or try again later.");
INSERT INTO phpn_vocabulary VALUES("2976","en","_RUN_CRON","Run cron");
INSERT INTO phpn_vocabulary VALUES("2979","en","_RUN_EVERY","Run every");
INSERT INTO phpn_vocabulary VALUES("5106","ID","_EMAIL_VALID_ALERT","Please enter a valid email address!");
INSERT INTO phpn_vocabulary VALUES("2982","en","_SA","Sa");
INSERT INTO phpn_vocabulary VALUES("2985","en","_SAID","said");
INSERT INTO phpn_vocabulary VALUES("2988","en","_SAT","Sat");
INSERT INTO phpn_vocabulary VALUES("5105","ID","_EMAIL_TO","Email Address (To)");
INSERT INTO phpn_vocabulary VALUES("2991","en","_SATURDAY","Saturday");
INSERT INTO phpn_vocabulary VALUES("2994","en","_SCHEDULED_CAMPAIGN","Scheduled Campaign");
INSERT INTO phpn_vocabulary VALUES("2997","en","_SEARCH","Search");
INSERT INTO phpn_vocabulary VALUES("5104","ID","_EMAIL_TEMPLATES_EDITOR","Email Templates Editor");
INSERT INTO phpn_vocabulary VALUES("3000","en","_SEARCH_KEYWORDS","search keywords");
INSERT INTO phpn_vocabulary VALUES("3003","en","_SEARCH_RESULT_FOR","Search Results for");
INSERT INTO phpn_vocabulary VALUES("5101","ID","_EMAIL_SETTINGS","Email Settings");
INSERT INTO phpn_vocabulary VALUES("5102","ID","_EMAIL_SUCCESSFULLY_SENT","Email has been successfully sent!");
INSERT INTO phpn_vocabulary VALUES("5103","ID","_EMAIL_TEMPLATES","Email Templates");
INSERT INTO phpn_vocabulary VALUES("3006","en","_SEARCH_ROOM_TIPS","<b>Find more rooms by expanding your search options</b>:<br>- Reduce the number of adults in room to get more results<br>- Reduce the number of children in room to get more results<br>- Change your Check-in/Check-out dates<br>");
INSERT INTO phpn_vocabulary VALUES("3009","en","_SEC","Sec");
INSERT INTO phpn_vocabulary VALUES("5100","ID","_EMAIL_SEND_ERROR","An error occurred while sending email. Please check your email settings and message recipients, then try again.");
INSERT INTO phpn_vocabulary VALUES("3012","en","_SELECT","select");
INSERT INTO phpn_vocabulary VALUES("3015","en","_SELECTED_ROOMS","Selected Rooms");
INSERT INTO phpn_vocabulary VALUES("5099","ID","_EMAIL_NOT_EXISTS","This e-mail account does not exist in the system! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3018","en","_SELECT_FILE_TO_UPLOAD","Select a file to upload");
INSERT INTO phpn_vocabulary VALUES("3021","en","_SELECT_HOTEL","Select Hotel");
INSERT INTO phpn_vocabulary VALUES("5098","ID","_EMAIL_NOTIFICATIONS","Send email notifications");
INSERT INTO phpn_vocabulary VALUES("3024","en","_SELECT_LANG_TO_UPDATE","Select a language to update");
INSERT INTO phpn_vocabulary VALUES("5097","ID","_EMAIL_IS_WRONG","Please enter a valid email address.");
INSERT INTO phpn_vocabulary VALUES("3027","en","_SELECT_LOCATION","Select Location");
INSERT INTO phpn_vocabulary VALUES("3030","en","_SELECT_REPORT_ALERT","Please select a report type!");
INSERT INTO phpn_vocabulary VALUES("3033","en","_SEND","Send");
INSERT INTO phpn_vocabulary VALUES("5096","ID","_EMAIL_IS_EMPTY","Email must not be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3036","en","_SENDING","Sending");
INSERT INTO phpn_vocabulary VALUES("3039","en","_SEND_COPY_TO_ADMIN","Send a copy to admin");
INSERT INTO phpn_vocabulary VALUES("5095","ID","_EMAIL_FROM","Email Address (From)");
INSERT INTO phpn_vocabulary VALUES("3042","en","_SEND_INVOICE","Send Invoice");
INSERT INTO phpn_vocabulary VALUES("3045","en","_SEO_LINKS_ALERT","If you select this option, make sure SEO Links Section uncommented in .htaccess file");
INSERT INTO phpn_vocabulary VALUES("3048","en","_SEO_URLS","SEO URLs");
INSERT INTO phpn_vocabulary VALUES("5094","ID","_EMAIL_EMPTY_ALERT","Email cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3051","en","_SEPTEMBER","September");
INSERT INTO phpn_vocabulary VALUES("3054","en","_SERVER_INFO","Server Info");
INSERT INTO phpn_vocabulary VALUES("3057","en","_SERVER_LOCALE","Server Locale");
INSERT INTO phpn_vocabulary VALUES("3060","en","_SERVICE","Service");
INSERT INTO phpn_vocabulary VALUES("3063","en","_SERVICES","Services");
INSERT INTO phpn_vocabulary VALUES("5093","ID","_EMAIL_BLOCKED","Your email is blocked! To resolve this problem, please contact the site administrator.");
INSERT INTO phpn_vocabulary VALUES("3066","en","_SETTINGS","Settings");
INSERT INTO phpn_vocabulary VALUES("5092","ID","_EMAIL_ADDRESS","E-mail address");
INSERT INTO phpn_vocabulary VALUES("3069","en","_SETTINGS_SAVED","Changes were saved! Please refresh the <a href=index.php>Home Page</a> to see the results.");
INSERT INTO phpn_vocabulary VALUES("5091","ID","_EMAILS_SUCCESSFULLY_SENT","Status: _SENT_ emails from _TOTAL_ were successfully sent!");
INSERT INTO phpn_vocabulary VALUES("3072","en","_SET_ADMIN","Set Admin");
INSERT INTO phpn_vocabulary VALUES("3075","en","_SET_DATE","Set date");
INSERT INTO phpn_vocabulary VALUES("3078","en","_SET_PERIODS","Set Periods");
INSERT INTO phpn_vocabulary VALUES("3081","en","_SET_TIME","Set Time");
INSERT INTO phpn_vocabulary VALUES("3084","en","_SHORT_DESCRIPTION","Short Description");
INSERT INTO phpn_vocabulary VALUES("5090","ID","_EMAILS_SENT_ERROR","An error occurred while sending emails or there are no emails to be sent! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("3087","en","_SHOW","Show");
INSERT INTO phpn_vocabulary VALUES("5089","ID","_EMAIL","Email");
INSERT INTO phpn_vocabulary VALUES("3090","en","_SHOW_IN_SEARCH","Show in Search");
INSERT INTO phpn_vocabulary VALUES("6331","ID","_DISTANCE_OF_HOTEL_FROM_CENTER_POINT","Distance of the hotel form the {name_center_point} is {distance_center_point}");
INSERT INTO phpn_vocabulary VALUES("5088","ID","_EDIT_WORD","Edit");
INSERT INTO phpn_vocabulary VALUES("3093","en","_SHOW_META_TAGS","Show META tags");
INSERT INTO phpn_vocabulary VALUES("6330","ID","_KILOMETERS_SHORTENED","km.");
INSERT INTO phpn_vocabulary VALUES("5087","ID","_EDIT_PAGE","Edit Page");
INSERT INTO phpn_vocabulary VALUES("3096","en","_SHOW_ON_DASHBOARD","Show on Dashboard");
INSERT INTO phpn_vocabulary VALUES("6329","ID","_METERS_SHORTENED","m.");
INSERT INTO phpn_vocabulary VALUES("3099","en","_SIDE_PANEL","Side Panel");
INSERT INTO phpn_vocabulary VALUES("5086","ID","_EDIT_MY_ACCOUNT","Edit My Account");
INSERT INTO phpn_vocabulary VALUES("3102","en","_SIMPLE","Simple");
INSERT INTO phpn_vocabulary VALUES("6328","ID","_FOUND_PROPERTIES","Found Properties");
INSERT INTO phpn_vocabulary VALUES("5083","ID","_DOWNLOAD_INVOICE","Download Invoice");
INSERT INTO phpn_vocabulary VALUES("5084","ID","_ECHECK","E-Check");
INSERT INTO phpn_vocabulary VALUES("5085","ID","_EDIT_MENUS","Edit Menus");
INSERT INTO phpn_vocabulary VALUES("3105","en","_SITE_DEVELOPMENT_MODE_ALERT","The site is running in Development Mode! To turn it off change <b>SITE_MODE</b> value in <b>inc/settings.inc.php</b>");
INSERT INTO phpn_vocabulary VALUES("6325","ID","_TOTAL_PAYMENTS","Total Payments");
INSERT INTO phpn_vocabulary VALUES("6326","ID","_STARTING","Starting");
INSERT INTO phpn_vocabulary VALUES("6327","ID","_FOUND_VILLAS","Found Villas");
INSERT INTO phpn_vocabulary VALUES("5082","ID","_DOWNLOAD","Download");
INSERT INTO phpn_vocabulary VALUES("3108","en","_SITE_INFO","Site Info");
INSERT INTO phpn_vocabulary VALUES("6324","ID","_TOTAL_FUNDS","Total Funds");
INSERT INTO phpn_vocabulary VALUES("5081","ID","_DOWN","Down");
INSERT INTO phpn_vocabulary VALUES("3111","en","_SITE_OFFLINE","Site Offline");
INSERT INTO phpn_vocabulary VALUES("6323","ID","_GET_DISTANCE","Get Distance");
INSERT INTO phpn_vocabulary VALUES("5080","ID","_DISPLAY_ON","Display on");
INSERT INTO phpn_vocabulary VALUES("3114","en","_SITE_OFFLINE_ALERT","Select whether access to the Site Front-end is available. If Yes, the Front-End will display the message below");
INSERT INTO phpn_vocabulary VALUES("6321","ID","_NAME_CENTER_POINT","Name of Center Point");
INSERT INTO phpn_vocabulary VALUES("6322","ID","_DISTANCE_TO_CENTER_POINT","Distance to Center Point");
INSERT INTO phpn_vocabulary VALUES("3117","en","_SITE_OFFLINE_MESSAGE_ALERT","A message that displays in the Front-end if your site is offline");
INSERT INTO phpn_vocabulary VALUES("6320","ID","_COORDINATES_CENTER_POINT","Coordinates of Center Point");
INSERT INTO phpn_vocabulary VALUES("3120","en","_SITE_PREVIEW","Site Preview");
INSERT INTO phpn_vocabulary VALUES("3123","en","_SITE_RANKS","Site Ranks");
INSERT INTO phpn_vocabulary VALUES("3126","en","_SITE_RSS","Site RSS");
INSERT INTO phpn_vocabulary VALUES("6319","ID","_DISCOUNT_FOR_5TH_OR_MORE_NIGHTS","Discount for 5th+ nights");
INSERT INTO phpn_vocabulary VALUES("3129","en","_SITE_SETTINGS","Site Settings");
INSERT INTO phpn_vocabulary VALUES("3132","en","_SMTP_HOST","SMTP Host");
INSERT INTO phpn_vocabulary VALUES("6318","ID","_DISCOUNT_FOR_4TH_NIGHT","Discount for 4th night");
INSERT INTO phpn_vocabulary VALUES("5079","ID","_DISCOUNT_STD_CAMPAIGN_TEXT","<span class=campaign_header>Super discount campaign!</span><br><br>Enjoy special price cuts in our Hotel for the specified periods of time below!");
INSERT INTO phpn_vocabulary VALUES("3135","en","_SMTP_PORT","SMTP Port");
INSERT INTO phpn_vocabulary VALUES("3138","en","_SMTP_SECURE","SMTP Secure");
INSERT INTO phpn_vocabulary VALUES("6317","ID","_DISCOUNT_FOR_3RD_NIGHT","Discount for 3rd night");
INSERT INTO phpn_vocabulary VALUES("3141","en","_SORT_BY","Sort by");
INSERT INTO phpn_vocabulary VALUES("3144","en","_STANDARD","Standard");
INSERT INTO phpn_vocabulary VALUES("3147","en","_STANDARD_PRICE","Standard Price");
INSERT INTO phpn_vocabulary VALUES("3150","en","_STARS","Stars");
INSERT INTO phpn_vocabulary VALUES("6316","ID","_LONG_TERM_IF_YOU_BOOK_ROOM","If you book this room for long stay you get");
INSERT INTO phpn_vocabulary VALUES("3153","en","_STARS_1_5","1 star to 5 stars");
INSERT INTO phpn_vocabulary VALUES("3156","en","_STARS_5_1","5 stars to 1 star");
INSERT INTO phpn_vocabulary VALUES("3159","en","_START_DATE","Start Date");
INSERT INTO phpn_vocabulary VALUES("6315","ID","_YOU_NOT_DOUBLE_REVIEW","You cannot double-post a review for a hotel");
INSERT INTO phpn_vocabulary VALUES("5077","ID","_DISCOUNT_CAMPAIGNS","Discount Campaigns");
INSERT INTO phpn_vocabulary VALUES("5078","ID","_DISCOUNT_CAMPAIGN_TEXT","<span class=campaign_header>Super discount campaign!</span><br /><br />\nEnjoy special price cuts right now<br />_FROM_ _TO_:<br /> \n<b>_PERCENT_</b> on every room reservation in our Hotel!");
INSERT INTO phpn_vocabulary VALUES("3162","en","_START_FINISH_DATE_ERROR","Finish date must be later than start date! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6314","ID","_LONG_TERM_STAY_DISCOUNT","Long-Term Stay Discount");
INSERT INTO phpn_vocabulary VALUES("3165","en","_START_OVER","Start Over");
INSERT INTO phpn_vocabulary VALUES("5076","ID","_DISCOUNT_CAMPAIGN","Discount Campaign");
INSERT INTO phpn_vocabulary VALUES("3168","en","_STATE","State");
INSERT INTO phpn_vocabulary VALUES("3171","en","_STATES","States");
INSERT INTO phpn_vocabulary VALUES("5075","ID","_DISCOUNT_BY_ADMIN","Discount By Administrator");
INSERT INTO phpn_vocabulary VALUES("3174","en","_STATE_PROVINCE","State/Province");
INSERT INTO phpn_vocabulary VALUES("6313","ID","_YOU_NOT_REVIEW_THIS_HOTEL","Sorry, but you can not leave a review for this hotel, you\'ve never been there");
INSERT INTO phpn_vocabulary VALUES("5074","ID","_DISCOUNT","Discount");
INSERT INTO phpn_vocabulary VALUES("3177","en","_STATISTICS","Statistics");
INSERT INTO phpn_vocabulary VALUES("3180","en","_STATUS","Status");
INSERT INTO phpn_vocabulary VALUES("5073","ID","_DISABLED","disabled");
INSERT INTO phpn_vocabulary VALUES("3183","en","_STOP","Stop");
INSERT INTO phpn_vocabulary VALUES("3186","en","_SU","Su");
INSERT INTO phpn_vocabulary VALUES("5072","ID","_DESCRIPTION","Description");
INSERT INTO phpn_vocabulary VALUES("3189","en","_SUBJECT","Subject");
INSERT INTO phpn_vocabulary VALUES("6312","ID","_SELECT_DESTINATION_OR_HOTEL","Destination / Hotel Name");
INSERT INTO phpn_vocabulary VALUES("3192","en","_SUBJECT_EMPTY_ALERT","Subject cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("3195","en","_SUBMIT","Submit");
INSERT INTO phpn_vocabulary VALUES("5071","ID","_DELETING_OPERATION_COMPLETED","Deleting operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("3198","en","_SUBMIT_BOOKING","Submit Booking");
INSERT INTO phpn_vocabulary VALUES("6311","ID","_MINIMUM_PERIOD_ALERT","The minimum allowed period for search is _DAYS_ days. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3201","en","_SUBMIT_PAYMENT","Submit Payment");
INSERT INTO phpn_vocabulary VALUES("3204","en","_SUBSCRIBE","Subscribe");
INSERT INTO phpn_vocabulary VALUES("6310","ID","_ALL_REVIEWS","All Reviews");
INSERT INTO phpn_vocabulary VALUES("5069","ID","_DELETE_WORD","Delete");
INSERT INTO phpn_vocabulary VALUES("5070","ID","_DELETING_ACCOUNT_ERROR","An error occurred while deleting your account! Please try again later or send email about this issue to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("3207","en","_SUBSCRIBE_EMAIL_EXISTS_ALERT","Someone with such email has already been subscribed to our newsletter. Please choose another email address for subscription.");
INSERT INTO phpn_vocabulary VALUES("6307","ID","_REMOVED_FROM_WISHLIST","Item removed from wishlist!");
INSERT INTO phpn_vocabulary VALUES("6308","ID","_EX_HOTEL_OR_LOCATION","e.g. hotel, landmark or location");
INSERT INTO phpn_vocabulary VALUES("6309","ID","_MASS_MAIL_LOG","Mass Mail Log");
INSERT INTO phpn_vocabulary VALUES("3210","en","_SUBSCRIBE_TO_NEWSLETTER","Newsletter Subscription");
INSERT INTO phpn_vocabulary VALUES("6306","ID","_ADDED_TO_WISHLIST","Item successfully added to wishlist!");
INSERT INTO phpn_vocabulary VALUES("5066","ID","_DEFINE","Define");
INSERT INTO phpn_vocabulary VALUES("5067","ID","_DELETE_WARNING","Are you sure you want to delete this record?");
INSERT INTO phpn_vocabulary VALUES("5068","ID","_DELETE_WARNING_COMMON","Are you sure you want to delete this record?");
INSERT INTO phpn_vocabulary VALUES("3213","en","_SUBSCRIPTION_ALREADY_SENT","You have already subscribed to our newsletter. Please try again later or wait _WAIT_ seconds.");
INSERT INTO phpn_vocabulary VALUES("6304","ID","_PERCENTAGE","Percentage");
INSERT INTO phpn_vocabulary VALUES("6305","ID","_NOT_LOGGED_ALERT","You must be logged in to perform this operation!");
INSERT INTO phpn_vocabulary VALUES("5065","ID","_DEFAULT_TEMPLATE","Default Template");
INSERT INTO phpn_vocabulary VALUES("3216","en","_SUBSCRIPTION_MANAGEMENT","Subscription Management");
INSERT INTO phpn_vocabulary VALUES("6303","ID","_FIXED_PRICE","Fixed Price");
INSERT INTO phpn_vocabulary VALUES("3219","en","_SUBTOTAL","Subtotal");
INSERT INTO phpn_vocabulary VALUES("6302","ID","_NIGHT_5","5th night");
INSERT INTO phpn_vocabulary VALUES("3222","en","_SUN","Sun");
INSERT INTO phpn_vocabulary VALUES("5064","ID","_DEFAULT_PRICE","Default Price");
INSERT INTO phpn_vocabulary VALUES("3225","en","_SUNDAY","Sunday");
INSERT INTO phpn_vocabulary VALUES("6301","ID","_NIGHT_4","4th night");
INSERT INTO phpn_vocabulary VALUES("3228","en","_SWITCH_TO_EXPORT","Switch to Export");
INSERT INTO phpn_vocabulary VALUES("6300","ID","_NIGHT_3","3rd night");
INSERT INTO phpn_vocabulary VALUES("3231","en","_SWITCH_TO_NORMAL","Switch to Normal");
INSERT INTO phpn_vocabulary VALUES("5063","ID","_DEFAULT_PERIODS_WERE_ADDED","Default periods have been successfully added!");
INSERT INTO phpn_vocabulary VALUES("3234","en","_SYMBOL","Symbol");
INSERT INTO phpn_vocabulary VALUES("6299","ID","_DISCOUNT_TYPE","Discount Type");
INSERT INTO phpn_vocabulary VALUES("3237","en","_SYMBOL_PLACEMENT","Symbol Placement");
INSERT INTO phpn_vocabulary VALUES("3240","en","_SYSTEM","System");
INSERT INTO phpn_vocabulary VALUES("6298","ID","_DISCOUNTS","Discounts");
INSERT INTO phpn_vocabulary VALUES("3243","en","_SYSTEM_EMAIL_DELETE_ALERT","This email template is used by the system and cannot be deleted!");
INSERT INTO phpn_vocabulary VALUES("6296","ID","_ADD_TO_WISHLIST","Add to wishlist");
INSERT INTO phpn_vocabulary VALUES("6297","ID","_REMOVE_FROM_WISHLIST","Remove from wishlist");
INSERT INTO phpn_vocabulary VALUES("5062","ID","_DEFAULT_PERIODS_ALERT","Default Periods are used to specify periods of time that could by easily fulfilled with default prices for each room on the Room Prices page (with just a single click).");
INSERT INTO phpn_vocabulary VALUES("3246","en","_SYSTEM_MODULE","System Module");
INSERT INTO phpn_vocabulary VALUES("6295","ID","_WISHLIST","Wishlist");
INSERT INTO phpn_vocabulary VALUES("3249","en","_SYSTEM_MODULES","System Modules");
INSERT INTO phpn_vocabulary VALUES("3252","en","_SYSTEM_MODULE_ACTIONS_BLOCKED","All operations with system module are blocked!");
INSERT INTO phpn_vocabulary VALUES("6294","ID","_MAXIMUM_PERIOD_ALERT","The maximum allowed period for search is _DAYS_ days. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3255","en","_SYSTEM_TEMPLATE","System Template");
INSERT INTO phpn_vocabulary VALUES("5061","ID","_DEFAULT_OWN_EMAIL_ALERT","You have to change your own email address. Click <a href=\'index.php?admin=my_account\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("3258","en","_TAG","Tag");
INSERT INTO phpn_vocabulary VALUES("5060","ID","_DEFAULT_HOTEL_DELETE_ALERT","You cannot delete default hotel!");
INSERT INTO phpn_vocabulary VALUES("3261","en","_TAG_TITLE_IS_EMPTY","Tag &lt;TITLE&gt; cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6293","ID","_MS_SEARCH_AVAILABILITY_PERIOD","Specifies the maximum allowed period of time for search in years");
INSERT INTO phpn_vocabulary VALUES("3264","en","_TARGET","Target");
INSERT INTO phpn_vocabulary VALUES("3267","en","_TARGET_GROUP","Target Group");
INSERT INTO phpn_vocabulary VALUES("6292","ID","_MSN_SEARCH_AVAILABILITY_PERIOD","Maximum Allowed Search Period");
INSERT INTO phpn_vocabulary VALUES("3270","en","_TAXES","Taxes");
INSERT INTO phpn_vocabulary VALUES("6291","ID","_PHOTO","Photo");
INSERT INTO phpn_vocabulary VALUES("3273","en","_TEMPLATES_STYLES","Templates & Styles");
INSERT INTO phpn_vocabulary VALUES("6290","ID","_MY_REVIEWS","My Reviews");
INSERT INTO phpn_vocabulary VALUES("3276","en","_TEMPLATE_CODE","Template Code");
INSERT INTO phpn_vocabulary VALUES("5059","ID","_DEFAULT_EMAIL_ALERT","You have to change default email address for site administrator. Click <a href=\'index.php?admin=settings&tabid=1_4\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("3279","en","_TEMPLATE_IS_EMPTY","Template cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3282","en","_TERMS","Terms & Conditions");
INSERT INTO phpn_vocabulary VALUES("5058","ID","_DEFAULT_CURRENCY_DELETE_ALERT","You cannot delete default currency!");
INSERT INTO phpn_vocabulary VALUES("3285","en","_REVIEWS","Reviews");
INSERT INTO phpn_vocabulary VALUES("5057","ID","_DEFAULT_AVAILABILITY","Default Availability");
INSERT INTO phpn_vocabulary VALUES("3288","en","_REVIEWS_MANAGEMENT","Reviews Management");
INSERT INTO phpn_vocabulary VALUES("6289","ID","_MIN_MAX_NIGHTS_ALERT","The minimum and maximum allowed stay for the period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking (package _PACKAGE_NAME_). Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5056","ID","_DEFAULT","Default");
INSERT INTO phpn_vocabulary VALUES("3294","en","_TEST_EMAIL","Test Email");
INSERT INTO phpn_vocabulary VALUES("5053","ID","_DAY","Day");
INSERT INTO phpn_vocabulary VALUES("5054","ID","_DECEMBER","December");
INSERT INTO phpn_vocabulary VALUES("5055","ID","_DECIMALS","Decimals");
INSERT INTO phpn_vocabulary VALUES("3297","en","_TEST_MODE_ALERT","Test Mode in Reservation Cart is turned ON! To change current mode click <a href=index.php?admin=mod_booking_settings>here</a>.");
INSERT INTO phpn_vocabulary VALUES("6287","ID","_REVIEW_YOU_HAVE_REGISTERED","You have to be registered in to post your review.");
INSERT INTO phpn_vocabulary VALUES("6288","ID","_REVIEW_LINK_LOGIN","<a href=index.php?customer=login>Link</a> to login page.");
INSERT INTO phpn_vocabulary VALUES("5051","ID","_DATE_PUBLISHED","Date Published");
INSERT INTO phpn_vocabulary VALUES("5052","ID","_DATE_SUBSCRIBED","Date Subscribed");
INSERT INTO phpn_vocabulary VALUES("3300","en","_TEST_MODE_ALERT_SHORT","Attention: Reservation Cart is running in Test Mode!");
INSERT INTO phpn_vocabulary VALUES("6286","ID","_MSN_TESTIMONIALS_KEY","Testimonials Key");
INSERT INTO phpn_vocabulary VALUES("3303","en","_TEXT","Text");
INSERT INTO phpn_vocabulary VALUES("6285","ID","_TESTIMONIALS_SETTINGS","Testimonials Settings");
INSERT INTO phpn_vocabulary VALUES("3306","en","_TH","Th");
INSERT INTO phpn_vocabulary VALUES("5050","ID","_DATE_PAYMENT","Date of Payment");
INSERT INTO phpn_vocabulary VALUES("3309","en","_THU","Thu");
INSERT INTO phpn_vocabulary VALUES("3312","en","_THUMBNAIL","Thumbnail");
INSERT INTO phpn_vocabulary VALUES("5049","ID","_DATE_MODIFIED","Date Modified");
INSERT INTO phpn_vocabulary VALUES("3315","en","_THURSDAY","Thursday");
INSERT INTO phpn_vocabulary VALUES("6284","ID","_TESTIMONIALS_MANAGEMENT","Testimonials Management");
INSERT INTO phpn_vocabulary VALUES("5047","ID","_DATE_EMPTY_ALERT","Date fields cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5048","ID","_DATE_FORMAT","Date Format");
INSERT INTO phpn_vocabulary VALUES("3318","en","_TIME_PERIOD_OVERLAPPING_ALERT","This period of time (fully or partially) is already selected! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("6283","ID","_TESTIMONIALS","Testimonials");
INSERT INTO phpn_vocabulary VALUES("3321","en","_TIME_ZONE","Time Zone");
INSERT INTO phpn_vocabulary VALUES("3324","en","_TO","To");
INSERT INTO phpn_vocabulary VALUES("5046","ID","_DATE_CREATED","Date Created");
INSERT INTO phpn_vocabulary VALUES("3327","en","_TODAY","Today");
INSERT INTO phpn_vocabulary VALUES("3330","en","_TOP","Top");
INSERT INTO phpn_vocabulary VALUES("6282","ID","_MS_TESTIMONIALS_KEY","The keyword that will be replaced with a list of customer testimonials (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("3333","en","_TOP_PANEL","Top Panel");
INSERT INTO phpn_vocabulary VALUES("5045","ID","_DATE_AND_TIME_SETTINGS","Date & Time Settings");
INSERT INTO phpn_vocabulary VALUES("3336","en","_TOTAL","Total");
INSERT INTO phpn_vocabulary VALUES("3339","en","_TOTAL_PRICE","Total Price");
INSERT INTO phpn_vocabulary VALUES("5044","ID","_DATETIME_PRICE_FORMAT","Datetime & Price Settings");
INSERT INTO phpn_vocabulary VALUES("3342","en","_TOTAL_ROOMS","Total Rooms");
INSERT INTO phpn_vocabulary VALUES("5043","ID","_DATE","Date");
INSERT INTO phpn_vocabulary VALUES("3345","en","_TRANSACTION","Transaction");
INSERT INTO phpn_vocabulary VALUES("3348","en","_TRANSLATE_VIA_GOOGLE","Translate via Google");
INSERT INTO phpn_vocabulary VALUES("6281","ID","_MD_TESTIMONIALS","The Testimonials Module allows the administrator of the site to add/edit customer testimonials, manage them and show on the Hotel Site frontend.");
INSERT INTO phpn_vocabulary VALUES("5042","ID","_DASHBOARD","Dashboard");
INSERT INTO phpn_vocabulary VALUES("3351","en","_TRASH","Trash");
INSERT INTO phpn_vocabulary VALUES("5041","ID","_CVV_CODE","CVV Code");
INSERT INTO phpn_vocabulary VALUES("3354","en","_TRASH_PAGES","Trash Pages");
INSERT INTO phpn_vocabulary VALUES("6280","ID","_REVIEWS_SETTINGS","Reviews Settings");
INSERT INTO phpn_vocabulary VALUES("5040","ID","_CUSTOMER_PAYMENT_MODULES","Customer & Payment Modules");
INSERT INTO phpn_vocabulary VALUES("3357","en","_TRUNCATE_RELATED_TABLES","Truncate related tables?");
INSERT INTO phpn_vocabulary VALUES("6279","ID","_GUEST_RATINGS","guest ratings");
INSERT INTO phpn_vocabulary VALUES("5038","ID","_CUSTOMER_NAME","Customer Name");
INSERT INTO phpn_vocabulary VALUES("5039","ID","_CUSTOMER_PANEL","Customer Panel");
INSERT INTO phpn_vocabulary VALUES("3360","en","_TRY_LATER","An error occurred while executing. Please try again later!");
INSERT INTO phpn_vocabulary VALUES("3363","en","_TRY_SYSTEM_SUGGESTION","Try out system suggestion");
INSERT INTO phpn_vocabulary VALUES("3366","en","_TU","Tu");
INSERT INTO phpn_vocabulary VALUES("6278","ID","_PROPERTY_NOT_REVIEWED_YET","Not yet reviewed by any member... You can be the FIRST one to write a review for _PROPERTY_NAME_.");
INSERT INTO phpn_vocabulary VALUES("5037","ID","_CUSTOMER_LOGIN","Customer Login");
INSERT INTO phpn_vocabulary VALUES("3369","en","_TUE","Tue");
INSERT INTO phpn_vocabulary VALUES("3372","en","_TUESDAY","Tuesday");
INSERT INTO phpn_vocabulary VALUES("6277","ID","_ADD_REVIEW","Add Review");
INSERT INTO phpn_vocabulary VALUES("3375","en","_TYPE","Type");
INSERT INTO phpn_vocabulary VALUES("6276","ID","_BY","by");
INSERT INTO phpn_vocabulary VALUES("5036","ID","_CUSTOMER_GROUPS","Customer Groups");
INSERT INTO phpn_vocabulary VALUES("3378","en","_TYPE_CHARS","Type the characters you see in the picture");
INSERT INTO phpn_vocabulary VALUES("6275","ID","_AVERAGE_RATINGS","Average Ratings");
INSERT INTO phpn_vocabulary VALUES("5035","ID","_CUSTOMER_GROUP","Customer Group");
INSERT INTO phpn_vocabulary VALUES("3381","en","_UNCATEGORIZED","Uncategorized");
INSERT INTO phpn_vocabulary VALUES("3384","en","_UNDEFINED","undefined");
INSERT INTO phpn_vocabulary VALUES("6274","ID","_X_OUT_OF_Y","_X_ out of _Y_");
INSERT INTO phpn_vocabulary VALUES("5034","ID","_CUSTOMER_DETAILS","Customer Details");
INSERT INTO phpn_vocabulary VALUES("3387","en","_UNINSTALL","Uninstall");
INSERT INTO phpn_vocabulary VALUES("3390","en","_UNITS","Units");
INSERT INTO phpn_vocabulary VALUES("5033","ID","_CUSTOMERS_SETTINGS","Customers Settings");
INSERT INTO phpn_vocabulary VALUES("3393","en","_UNIT_PRICE","Unit Price");
INSERT INTO phpn_vocabulary VALUES("3396","en","_UNKNOWN","Unknown");
INSERT INTO phpn_vocabulary VALUES("6273","ID","_RATINGS_BASED_ON_REVIEWS","Ratings based on _REVIEWS_ Verified Reviews");
INSERT INTO phpn_vocabulary VALUES("3399","en","_UNSUBSCRIBE","Unsubscribe");
INSERT INTO phpn_vocabulary VALUES("5032","ID","_CUSTOMERS_MANAGEMENT","Customers Management");
INSERT INTO phpn_vocabulary VALUES("3402","en","_UP","Up");
INSERT INTO phpn_vocabulary VALUES("6272","ID","_NEUTRAL","Neutral");
INSERT INTO phpn_vocabulary VALUES("3405","en","_UPDATING_ACCOUNT","Updating Account");
INSERT INTO phpn_vocabulary VALUES("5030","ID","_CUSTOMERS","Customers");
INSERT INTO phpn_vocabulary VALUES("5031","ID","_CUSTOMERS_AWAITING_MODERATION_ALERT","There are _COUNT_ customer/s awaiting your approval. Click <a href=\'index.php?admin=mod_customers_management\'>here</a> for review.");
INSERT INTO phpn_vocabulary VALUES("3408","en","_UPDATING_ACCOUNT_ERROR","An error occurred while updating your account! Please try again later or send information about this error to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("6269","ID","_NOT_RECOMMENDED","Not Recommended");
INSERT INTO phpn_vocabulary VALUES("6270","ID","_RECOMMENDED_TO_EVERYONE","Recommended to Everyone");
INSERT INTO phpn_vocabulary VALUES("6271","ID","_PERCENT_OF_GUESTS_RECOMMEND","_PERCENT_ of guests recommend");
INSERT INTO phpn_vocabulary VALUES("5029","ID","_CUSTOMER","Customer");
INSERT INTO phpn_vocabulary VALUES("3411","en","_UPDATING_OPERATION_COMPLETED","Updating operation has been successfully completed!");
INSERT INTO phpn_vocabulary VALUES("6267","ID","_TITLE","Title");
INSERT INTO phpn_vocabulary VALUES("6268","ID","_EVALUATION","Evaluation");
INSERT INTO phpn_vocabulary VALUES("5028","ID","_CURRENT_NEXT_YEARS","for current/next years");
INSERT INTO phpn_vocabulary VALUES("3414","en","_UPLOAD","Upload");
INSERT INTO phpn_vocabulary VALUES("5027","ID","_CURRENCY","Currency");
INSERT INTO phpn_vocabulary VALUES("3417","en","_UPLOAD_AND_PROCCESS","Upload and Process");
INSERT INTO phpn_vocabulary VALUES("6266","ID","_CAR_AGENCY_PAYMENT_GATEWAYS","Agency Payment Gateways");
INSERT INTO phpn_vocabulary VALUES("3420","en","_UPLOAD_FROM_FILE","Upload from File");
INSERT INTO phpn_vocabulary VALUES("3423","en","_URL","URL");
INSERT INTO phpn_vocabulary VALUES("5026","ID","_CURRENCIES_MANAGEMENT","Currencies Management");
INSERT INTO phpn_vocabulary VALUES("3426","en","_USED_ON","Used On");
INSERT INTO phpn_vocabulary VALUES("3429","en","_USERNAME","Username");
INSERT INTO phpn_vocabulary VALUES("6265","ID","_WRONG_EMPTY_FIELD_DROP_OFF","Fields dropping off may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3432","en","_USERNAME_AND_PASSWORD","Username & Password");
INSERT INTO phpn_vocabulary VALUES("3435","en","_USERNAME_EMPTY_ALERT","Username cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6264","ID","_WRONG_EMPTY_FIELD_PICK_UP","Fields picking up may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5025","ID","_CURRENCIES_DEFAULT_ALERT","Remember! After you change the default currency:<br>- Edit exchange rate to each currency manually (relatively to the new default currency)<br>- Redefine prices for all rooms in the new currency.");
INSERT INTO phpn_vocabulary VALUES("3438","en","_USERNAME_LENGTH_ALERT","The length of username cannot be less than 4 characters! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6263","ID","_WRONG_EMPTY_FIELDS_PICK_UP_AND_DROP_OFF","Fields picking up and dropping off may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5024","ID","_CURRENCIES","Currencies");
INSERT INTO phpn_vocabulary VALUES("3441","en","_USERS","Users");
INSERT INTO phpn_vocabulary VALUES("6262","ID","_VALUE_FOR_PRICE","Value for Price");
INSERT INTO phpn_vocabulary VALUES("5023","ID","_CRON_JOBS","Cron Jobs");
INSERT INTO phpn_vocabulary VALUES("3444","en","_USER_EMAIL_EXISTS_ALERT","User with such email already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("6260","ID","_SERVICE_AND_STAFF","Service & Staff");
INSERT INTO phpn_vocabulary VALUES("6261","ID","_SLEEP_QUALITY","Sleep Quality");
INSERT INTO phpn_vocabulary VALUES("3447","en","_USER_EXISTS_ALERT","User with such username already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("6258","ID","_CLEANLINESS","Cleanliness");
INSERT INTO phpn_vocabulary VALUES("6259","ID","_ROOM_COMFORT","Room Comfort");
INSERT INTO phpn_vocabulary VALUES("3450","en","_USER_NAME","User name");
INSERT INTO phpn_vocabulary VALUES("6257","ID","_REVIEW","review");
INSERT INTO phpn_vocabulary VALUES("3453","en","_USE_THIS_PASSWORD","Use this password");
INSERT INTO phpn_vocabulary VALUES("3456","en","_VALUE","Value");
INSERT INTO phpn_vocabulary VALUES("3459","en","_VAT","VAT");
INSERT INTO phpn_vocabulary VALUES("3462","en","_VAT_PERCENT","VAT Percent");
INSERT INTO phpn_vocabulary VALUES("6256","ID","_WRONG_EMPTY_FIELD","Fields picking up or/and dropping off are empty. Please select them.");
INSERT INTO phpn_vocabulary VALUES("3465","en","_VERSION","Version");
INSERT INTO phpn_vocabulary VALUES("3468","en","_VIDEO","Video");
INSERT INTO phpn_vocabulary VALUES("3471","en","_VIEW_ALL","View All");
INSERT INTO phpn_vocabulary VALUES("6255","ID","_NEGATIVE_COMMENTS","Negative Comments");
INSERT INTO phpn_vocabulary VALUES("3474","en","_VIEW_WORD","View");
INSERT INTO phpn_vocabulary VALUES("3477","en","_VISITOR","Visitor");
INSERT INTO phpn_vocabulary VALUES("6254","ID","_POSITIVE_COMMENTS","Positive Comments");
INSERT INTO phpn_vocabulary VALUES("3480","en","_VISITORS_RATING","Visitors Rating");
INSERT INTO phpn_vocabulary VALUES("3483","en","_VISUAL_SETTINGS","Visual Settings");
INSERT INTO phpn_vocabulary VALUES("3486","en","_VOCABULARY","Vocabulary");
INSERT INTO phpn_vocabulary VALUES("5022","ID","_CRONJOB_NOTICE","Cron jobs allow you to automate certain commands or scripts on your site.<br /><br />uHotelBooking needs to periodically run cron.php to close expired discount campaigns or perform another important operations. The recommended way to run cron.php is to set up a cronjob if you run a Unix/Linux server. If for any reason you can&#039;t run a cronjob on your server, you can choose the Non-batch option below to have cron.php run by uHotelBooking itself: in this case cron.php will be run each time someone access your home page. <br /><br />Example of Batch Cron job command: <b>php &#36;HOME/public_html/cron.php >/dev/null 2>&1</b>");
INSERT INTO phpn_vocabulary VALUES("3489","en","_VOC_KEYS_UPDATED","Operation has been successfully completed. Updated: _KEYS_ keys. Click <a href=\'index.php?admin=vocabulary&filter_by=A\'>here</a> to refresh the site.");
INSERT INTO phpn_vocabulary VALUES("6252","ID","_LEGEND_CAR_REFUNDED","Order has been refunded and vehicle is available again in search");
INSERT INTO phpn_vocabulary VALUES("6253","ID","_LEGEND_CAR_CANCELED","Order is canceled by admin and vehicle is available again in search");
INSERT INTO phpn_vocabulary VALUES("3492","en","_VOC_KEY_UPDATED","Vocabulary key has been successfully updated.");
INSERT INTO phpn_vocabulary VALUES("5021","ID","_CRONJOB_HTACCESS_BLOCK","To block remote access to cron.php, in the server&#039;s .htaccess file or vhost configuration file add this section:");
INSERT INTO phpn_vocabulary VALUES("3495","en","_VOC_KEY_VALUE_EMPTY","Key value cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6251","ID","_VEHICLE_CATEGORY_DELETE_ALERT","You cannot delete this category, because it still includes some vehicles! Please remove all vehicles from this category before deleting operation.");
INSERT INTO phpn_vocabulary VALUES("3498","en","_VOC_NOT_FOUND","No keys found");
INSERT INTO phpn_vocabulary VALUES("6250","ID","_BOOKED","booked");
INSERT INTO phpn_vocabulary VALUES("5019","ID","_CREDIT_CARD_NUMBER","Credit Card Number");
INSERT INTO phpn_vocabulary VALUES("5020","ID","_CREDIT_CARD_TYPE","Credit Card Type");
INSERT INTO phpn_vocabulary VALUES("3501","en","_VOC_UPDATED","Vocabulary has been successfully updated. Click <a href=index.php>here</a> to refresh the site.");
INSERT INTO phpn_vocabulary VALUES("6247","ID","_JUST_BOOKED","just booked");
INSERT INTO phpn_vocabulary VALUES("6248","ID","_AGO","ago");
INSERT INTO phpn_vocabulary VALUES("6249","ID","_ROOM","room");
INSERT INTO phpn_vocabulary VALUES("5018","ID","_CREDIT_CARD_HOLDER_NAME","Card Holder\'s Name");
INSERT INTO phpn_vocabulary VALUES("3504","en","_VOTE_NOT_REGISTERED","Your vote has not been registered! You must be logged in before you can vote.");
INSERT INTO phpn_vocabulary VALUES("5017","ID","_CREDIT_CARD_EXPIRES","Expires");
INSERT INTO phpn_vocabulary VALUES("3507","en","_WE","We");
INSERT INTO phpn_vocabulary VALUES("3510","en","_WEB_SITE","Web Site");
INSERT INTO phpn_vocabulary VALUES("5016","ID","_CREDIT_CARD","Credit Card");
INSERT INTO phpn_vocabulary VALUES("3513","en","_WED","Wed");
INSERT INTO phpn_vocabulary VALUES("3516","en","_WEDNESDAY","Wednesday");
INSERT INTO phpn_vocabulary VALUES("6246","ID","_LAST_BOOKINGS_MESSAGE","A visitor from {user_location} {type_booking} {count_rooms} at {hotel_name} in {hotel_location}");
INSERT INTO phpn_vocabulary VALUES("5015","ID","_CREATING_NEW_ACCOUNT","Creating new account");
INSERT INTO phpn_vocabulary VALUES("3519","en","_WEEK_START_DAY","Week Start Day");
INSERT INTO phpn_vocabulary VALUES("6245","ID","_LAST_BOOKINGS","Last Bookings");
INSERT INTO phpn_vocabulary VALUES("5014","ID","_CREATING_ACCOUNT_ERROR","An error occurred while creating your account! Please try again later or send information about this error to administration of the site.");
INSERT INTO phpn_vocabulary VALUES("3522","en","_WELCOME_CUSTOMER_TEXT","<p>Hello <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>        \n<p>Welcome to Customer Account Panel, that allows you to view account status, manage your account settings and bookings.</p>\n<p>\n   _TODAY_<br />\n   _LAST_LOGIN_\n</p>				\n<p> <b>&#8226;</b> To view this account summary just click on a <a href=\'index.php?customer=home\'>Dashboard</a> link.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Edit My Account</a> menu allows you to change your personal info and account data.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>My Bookings</a> contains information about your orders.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("6233","ID","_TYPE_YOUR_PICKUP_LOCATION","type your picking up location");
INSERT INTO phpn_vocabulary VALUES("6234","ID","_TYPE_YOUR_DROPPING_OFF_LOCATION","type your dropping off location");
INSERT INTO phpn_vocabulary VALUES("6235","ID","_PLEASE_SELECT_LOCATION","Please select both locations within a same country");
INSERT INTO phpn_vocabulary VALUES("6236","ID","_LOWEST_HIGHEST","Lowest to Highest");
INSERT INTO phpn_vocabulary VALUES("6237","ID","_HIGHEST_LOWEST","Highest to Lowest");
INSERT INTO phpn_vocabulary VALUES("6238","ID","_FOUND_AGENCIES","Found Agencies");
INSERT INTO phpn_vocabulary VALUES("6239","ID","_TOTAL_CARS","Total Cars");
INSERT INTO phpn_vocabulary VALUES("6240","ID","_DAYS","Days");
INSERT INTO phpn_vocabulary VALUES("6241","ID","_PERIOD","Period");
INSERT INTO phpn_vocabulary VALUES("6242","ID","_VOTE","vote");
INSERT INTO phpn_vocabulary VALUES("6243","ID","_VOTES","votes");
INSERT INTO phpn_vocabulary VALUES("6244","ID","_MOST_POPULAR_HOTELS","Most Popular Hotels");
INSERT INTO phpn_vocabulary VALUES("3525","en","_WHAT_IS_CVV","What is CVV");
INSERT INTO phpn_vocabulary VALUES("5013","ID","_CREATE_ACCOUNT_NOTE","NOTE: <br>We recommend that your password should be at least 6 characters long and should be different from your username.<br><br>Your e-mail address must be valid. We use e-mail for communication purposes (order notifications, etc). Therefore, it is essential to provide a valid e-mail address to be able to use our services correctly.<br><br>All your private data is confidential. We will never sell, exchange or market it in any way. For further information on the responsibilities of both parts, you may refer to us.");
INSERT INTO phpn_vocabulary VALUES("3528","en","_WHOLE_SITE","Whole site");
INSERT INTO phpn_vocabulary VALUES("5011","ID","_CREATED_DATE","Date Created");
INSERT INTO phpn_vocabulary VALUES("5012","ID","_CREATE_ACCOUNT","Create account");
INSERT INTO phpn_vocabulary VALUES("3531","en","_WIDGET_INTEGRATION_MESSAGE","You may integrate Hotel Site search engine with another existing web site.");
INSERT INTO phpn_vocabulary VALUES("6232","ID","_LEGEND_CAR_RESERVED","Car is reserved, but order is not paid yet (pending)");
INSERT INTO phpn_vocabulary VALUES("5010","ID","_COUPON_WAS_REMOVED","The coupon has been successfully removed!");
INSERT INTO phpn_vocabulary VALUES("3534","en","_WIDGET_INTEGRATION_MESSAGE_HINT","<b>Hint</b>: To list all available hotels, leave hsJsKey value empty or enter hotel IDs separated by commas, for ex.: var hsHotelIDs = \"2,9,12\";");
INSERT INTO phpn_vocabulary VALUES("6229","ID","_DESCENDING","Descending");
INSERT INTO phpn_vocabulary VALUES("6230","ID","_NO_MATCHES_FOUND","No matches found");
INSERT INTO phpn_vocabulary VALUES("6231","ID","_LEGEND_CAR_PENDING","The car reservation has been created, but not yet been confirmed and reserved");
INSERT INTO phpn_vocabulary VALUES("5009","ID","_COUPON_WAS_APPLIED","The coupon _COUPON_CODE_ has been successfully applied!");
INSERT INTO phpn_vocabulary VALUES("3537","en","_WITHOUT_ACCOUNT","without account");
INSERT INTO phpn_vocabulary VALUES("6228","ID","_ASCENDING","Ascending");
INSERT INTO phpn_vocabulary VALUES("5007","ID","_COUPONS_MANAGEMENT","Coupons Management");
INSERT INTO phpn_vocabulary VALUES("5008","ID","_COUPON_CODE","Coupon Code");
INSERT INTO phpn_vocabulary VALUES("3540","en","_WRONG_BOOKING_NUMBER","The booking number you\'ve entered was not found! Please enter a valid booking number.");
INSERT INTO phpn_vocabulary VALUES("6226","ID","_PRICE_RANGE","Price range/per day");
INSERT INTO phpn_vocabulary VALUES("6227","ID","_MANUFACTURER","Manufacturer");
INSERT INTO phpn_vocabulary VALUES("5006","ID","_COUPONS","Coupons");
INSERT INTO phpn_vocabulary VALUES("3543","en","_WRONG_CHECKOUT_DATE_ALERT","Wrong date selected! Please choose a valid check-out date.");
INSERT INTO phpn_vocabulary VALUES("6225","ID","_NO_CARS_FOUND","No results found for your search conditions");
INSERT INTO phpn_vocabulary VALUES("5005","ID","_COUNTRY_EMPTY_ALERT","Country cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3546","en","_WRONG_CODE_ALERT","Sorry, the code you have entered is invalid! Please try again.");
INSERT INTO phpn_vocabulary VALUES("5004","ID","_COUNTRY","Country");
INSERT INTO phpn_vocabulary VALUES("3549","en","_WRONG_CONFIRMATION_CODE","Wrong confirmation code or your registration is already confirmed!");
INSERT INTO phpn_vocabulary VALUES("6223","ID","_PREORDERING","Pre-Ordering");
INSERT INTO phpn_vocabulary VALUES("6224","ID","_LEGEND_PREORDERING","This is a system status for a reservation that is in progress, or was not completed (car has been added to Reservation Cart, but still not reserved)");
INSERT INTO phpn_vocabulary VALUES("5002","ID","_COUNTRIES","Countries");
INSERT INTO phpn_vocabulary VALUES("5003","ID","_COUNTRIES_MANAGEMENT","Countries Management");
INSERT INTO phpn_vocabulary VALUES("3552","en","_WRONG_COUPON_CODE","This coupon code is invalid or has expired!");
INSERT INTO phpn_vocabulary VALUES("6222","ID","_VEHICLE_CATEGORY","Vehicle Category");
INSERT INTO phpn_vocabulary VALUES("5000","ID","_COPY_TO_OTHER_LANGS","Copy to other languages");
INSERT INTO phpn_vocabulary VALUES("5001","ID","_COUNT","Count");
INSERT INTO phpn_vocabulary VALUES("3555","en","_WRONG_FILE_TYPE","Uploaded file is not a valid PHP vocabulary file! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6220","ID","_AGENCY_CAR_TYPES","Agency Car Types");
INSERT INTO phpn_vocabulary VALUES("6221","ID","_VEHICLE_CATEGORIES","Vehicle Categories");
INSERT INTO phpn_vocabulary VALUES("3558","en","_WRONG_LOGIN","Wrong username or password!");
INSERT INTO phpn_vocabulary VALUES("4998","ID","_CONTINUE_RESERVATION","Continue Reservation");
INSERT INTO phpn_vocabulary VALUES("4999","ID","_COPY_TO_OTHERS","Copy to others");
INSERT INTO phpn_vocabulary VALUES("3561","en","_WRONG_PARAMETER_PASSED","Wrong parameters passed - cannot complete operation!");
INSERT INTO phpn_vocabulary VALUES("4997","ID","_CONTENT_TYPE","Content Type");
INSERT INTO phpn_vocabulary VALUES("3564","en","_WYSIWYG_EDITOR","WYSIWYG Editor");
INSERT INTO phpn_vocabulary VALUES("3567","en","_YEAR","Year");
INSERT INTO phpn_vocabulary VALUES("3570","en","_YES","Yes");
INSERT INTO phpn_vocabulary VALUES("4996","ID","_CONTACT_US_SETTINGS","Contact Us Settings");
INSERT INTO phpn_vocabulary VALUES("3573","en","_YOUR_EMAIL","Your Email");
INSERT INTO phpn_vocabulary VALUES("3576","en","_YOUR_NAME","Your Name");
INSERT INTO phpn_vocabulary VALUES("3579","en","_YOU_ARE_LOGGED_AS","You are logged in as");
INSERT INTO phpn_vocabulary VALUES("6219","ID","_AGENCYOWNER_WELCOME_TEXT","<p>Welcome to Car Agency Control Panel that allows you to manage information related to your transportation agency. With this Control Panel you can easy manage agency info, vehicles, vehicle types and perform a full car reservations management.</p>");
INSERT INTO phpn_vocabulary VALUES("4995","ID","_CONTACT_US_EMAIL_SENT","Thank you for contacting us! Your message has been successfully sent.");
INSERT INTO phpn_vocabulary VALUES("3582","en","_ZIPCODE_EMPTY_ALERT","Zip/Postal code cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("6217","ID","_TIME_INTERVAL","Time Interval");
INSERT INTO phpn_vocabulary VALUES("6218","ID","_AGENCY_NAME","Agency Name");
INSERT INTO phpn_vocabulary VALUES("3585","en","_ZIP_CODE","Zip/Postal code");
INSERT INTO phpn_vocabulary VALUES("6216","ID","_STEAM","Steam");
INSERT INTO phpn_vocabulary VALUES("3587","en","_PROPERTY_TYPE","Property Type");
INSERT INTO phpn_vocabulary VALUES("6215","ID","_PETROL","Petrol");
INSERT INTO phpn_vocabulary VALUES("3590","en","_PROPERTY_TYPES","Property Types");
INSERT INTO phpn_vocabulary VALUES("3593","en","_VILLA","Villa");
INSERT INTO phpn_vocabulary VALUES("4994","ID","_CONTACT_US_ALREADY_SENT","Your message has been already sent. Please try again later or wait _WAIT_ seconds.");
INSERT INTO phpn_vocabulary VALUES("3596","en","_VILLAS","Villas");
INSERT INTO phpn_vocabulary VALUES("6214","ID","_HYBRID_ELECTRIC","Hybrid Electric");
INSERT INTO phpn_vocabulary VALUES("3599","en","_DETAILS","Details");
INSERT INTO phpn_vocabulary VALUES("6213","ID","_MAKES","Makes");
INSERT INTO phpn_vocabulary VALUES("4993","ID","_CONTACT_US","Contact us");
INSERT INTO phpn_vocabulary VALUES("3602","en","_FACILITIES_MANAGEMENT","Facilities Management");
INSERT INTO phpn_vocabulary VALUES("6212","ID","_GASOLINE","Gasoline");
INSERT INTO phpn_vocabulary VALUES("4992","ID","_CONTACT_INFORMATION","Contact Information");
INSERT INTO phpn_vocabulary VALUES("3605","en","_HOTEL_FACILITIES","Hotel Facilities");
INSERT INTO phpn_vocabulary VALUES("6211","ID","_ETHANOL_FFV","Ethanol FFV");
INSERT INTO phpn_vocabulary VALUES("3608","en","_FEATURED_OFFERS","Featured Offers");
INSERT INTO phpn_vocabulary VALUES("6210","ID","_ELECTRIC","Electric");
INSERT INTO phpn_vocabulary VALUES("3611","en","_FEATURED_OFFERS_WITH_BR","Featured<br>Offers");
INSERT INTO phpn_vocabulary VALUES("6209","ID","_DIESEL","Diesel");
INSERT INTO phpn_vocabulary VALUES("3614","en","_FEATURED_OFFERS_TEXT","Start your search with a look at the best rates on our site.");
INSERT INTO phpn_vocabulary VALUES("6207","ID","_BIODIESEL","Biodiesel");
INSERT INTO phpn_vocabulary VALUES("6208","ID","_CNG","CNG");
INSERT INTO phpn_vocabulary VALUES("4991","ID","_CONTACTUS_DEFAULT_EMAIL_ALERT","You have to change default email address for Contact Us module. Click <a href=\'index.php?admin=mod_contact_us_settings\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("3617","en","_TODAY_TOP_DEALS","Today\'s Top Deals");
INSERT INTO phpn_vocabulary VALUES("6206","ID","_FUEL_TYPE","Fuel Type");
INSERT INTO phpn_vocabulary VALUES("4990","ID","_CONF_PASSWORD_MATCH","Password must be match with Confirm Password");
INSERT INTO phpn_vocabulary VALUES("3620","en","_TODAY_TOP_DEALS_WITH_BR","Today\'s Top<br/>Deals");
INSERT INTO phpn_vocabulary VALUES("6205","ID","_TIME","Time");
INSERT INTO phpn_vocabulary VALUES("3623","en","_TODAY_TOP_DEALS_TEXT","Start your search with a look at the best rates on our site.");
INSERT INTO phpn_vocabulary VALUES("6203","ID","_CAR_NOT_FOUND","Car has not been found!");
INSERT INTO phpn_vocabulary VALUES("6204","ID","_CARS_RESERVATION","Cars Reservation");
INSERT INTO phpn_vocabulary VALUES("4989","ID","_CONF_PASSWORD_IS_EMPTY","Confirm Password cannot be empty!");
INSERT INTO phpn_vocabulary VALUES("3626","en","_EARLY_BOOKING","Early Booking");
INSERT INTO phpn_vocabulary VALUES("4988","ID","_CONFIRM_TERMS_CONDITIONS","You must confirm you agree to our Terms & Conditions!");
INSERT INTO phpn_vocabulary VALUES("3629","en","_HOT_DEALS","Hot Deals");
INSERT INTO phpn_vocabulary VALUES("3632","en","_COPYRIGHT","Copyright");
INSERT INTO phpn_vocabulary VALUES("6202","ID","_CAR_WAS_REMOVED","Selected car has been successfully removed from your Reservation Cart!");
INSERT INTO phpn_vocabulary VALUES("4987","ID","_CONFIRM_PASSWORD","Confirm Password");
INSERT INTO phpn_vocabulary VALUES("3635","en","_ALL_RIGHTS_RESERVED","All rights reserved");
INSERT INTO phpn_vocabulary VALUES("6201","ID","_REMOVE_CAR_FROM_CART","Remove car from the cart");
INSERT INTO phpn_vocabulary VALUES("3638","en","_LETS_SOCIALIZE","Let\'s socialize");
INSERT INTO phpn_vocabulary VALUES("6200","ID","_SELECTED_CARS","Selected Cars");
INSERT INTO phpn_vocabulary VALUES("3641","en","_TARGET_HOTEL","Target Hotel");
INSERT INTO phpn_vocabulary VALUES("3644","en","_WONDERFUL","Wonderful!");
INSERT INTO phpn_vocabulary VALUES("3647","en","_VERY_GOOD","Very Good!");
INSERT INTO phpn_vocabulary VALUES("6199","ID","_CAR_WAS_ADDED","Car has been successfully added to your reservation!");
INSERT INTO phpn_vocabulary VALUES("3650","en","_NOT_GOOD","Not Good");
INSERT INTO phpn_vocabulary VALUES("3653","en","_SUMMARY","Summary");
INSERT INTO phpn_vocabulary VALUES("6198","ID","_CARS_IN_RENTAL","Cars In Rental");
INSERT INTO phpn_vocabulary VALUES("3656","en","_MAPS","Maps");
INSERT INTO phpn_vocabulary VALUES("4986","ID","_CONFIRMED_SUCCESS_MSG","Thank you for confirming your registration! <br /><br />You may now log into your account. Click <a href=\'index.php?customer=login\'>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("3659","en","_PREFERENCES","Preferences");
INSERT INTO phpn_vocabulary VALUES("6197","ID","_CAR_RETURNING","Car Returning");
INSERT INTO phpn_vocabulary VALUES("3662","en","_CUSTOMER_SUPPORT","Customer support");
INSERT INTO phpn_vocabulary VALUES("6196","ID","_CAR_PICKING_UP","Car Picking Up");
INSERT INTO phpn_vocabulary VALUES("3665","en","_NEED_ASSISTANCE","Need Assistance?");
INSERT INTO phpn_vocabulary VALUES("6195","ID","_PAID_SUM","Paid Sum");
INSERT INTO phpn_vocabulary VALUES("3668","en","_NEED_ASSISTANCE_TEXT","Our team is 24/7 at your service to help you with your booking issues or answer any related questions");
INSERT INTO phpn_vocabulary VALUES("6193","ID","_CAR_TYPE","Car Type");
INSERT INTO phpn_vocabulary VALUES("6194","ID","_MY_CAR_RENTALS","My Car Rentals");
INSERT INTO phpn_vocabulary VALUES("4984","ID","_CONFIRMATION_CODE","Confirmation Code");
INSERT INTO phpn_vocabulary VALUES("4985","ID","_CONFIRMED_ALREADY_MSG","Your account has already been confirmed! <br /><br />Click <a href=\'index.php?customer=login\'>here</a> to continue.");
INSERT INTO phpn_vocabulary VALUES("3671","en","_LAST_MINUTE","Last Minute");
INSERT INTO phpn_vocabulary VALUES("4983","ID","_CONFIRMATION","Confirmation");
INSERT INTO phpn_vocabulary VALUES("3674","en","_ALERT_SAME_HOTEL_ROOMS","You may add to this reservation cart only rooms from the same hotel!");
INSERT INTO phpn_vocabulary VALUES("6192","ID","_CAR_WAS_COMPLETED_MSG","Thank you for reservation car! Your booking has been completed.");
INSERT INTO phpn_vocabulary VALUES("4980","ID","_COMMENT_TEXT","Comment text");
INSERT INTO phpn_vocabulary VALUES("4981","ID","_COMPANY","Company");
INSERT INTO phpn_vocabulary VALUES("4982","ID","_COMPLETED","Completed (Paid)");
INSERT INTO phpn_vocabulary VALUES("3677","en","_INFO","Info");
INSERT INTO phpn_vocabulary VALUES("6191","ID","_CAR_BOOKING_COMPLETED","Car booking was completed");
INSERT INTO phpn_vocabulary VALUES("3680","en","_TODAY_CHECKIN","Today\'s Check-in");
INSERT INTO phpn_vocabulary VALUES("3683","en","_TODAY_CHECKOUT","Today\'s Check-out");
INSERT INTO phpn_vocabulary VALUES("3686","en","_FROM_PER_NIGHT","from per night");
INSERT INTO phpn_vocabulary VALUES("3689","en","_ROOM_LEFT","room left");
INSERT INTO phpn_vocabulary VALUES("6190","ID","_LAST_HOTEL_PROPERTY_ALERT","This property type cannot be deleted, because it is participating in one property at least.");
INSERT INTO phpn_vocabulary VALUES("4979","ID","_COMMENT_SUBMITTED_SUCCESS","Your comment has been successfully submitted and will be posted after administrator\'s review!");
INSERT INTO phpn_vocabulary VALUES("3692","en","_CHECK_HOTELS","Check Hotels");
INSERT INTO phpn_vocabulary VALUES("3695","en","_CHECK_VILLAS","Check Villas");
INSERT INTO phpn_vocabulary VALUES("3698","en","_AGENCIES","Agencies");
INSERT INTO phpn_vocabulary VALUES("4978","ID","_COMMENT_POSTED_SUCCESS","Your comment has been successfully posted!");
INSERT INTO phpn_vocabulary VALUES("3701","en","_MS_ALLOW_AGENCIES","Allow special type of customers - agencies");
INSERT INTO phpn_vocabulary VALUES("6188","ID","_RENTAL_TO","Rental To");
INSERT INTO phpn_vocabulary VALUES("6189","ID","_CAR_OWNER_NOT_ASSIGNED","You still has not been assigned to any car agency to see the reports.");
INSERT INTO phpn_vocabulary VALUES("3704","en","_AGENCY_DETAILS","Agency Details");
INSERT INTO phpn_vocabulary VALUES("6187","ID","_RENTAL_FROM","Rental From");
INSERT INTO phpn_vocabulary VALUES("3707","en","_BALANCE","Balance");
INSERT INTO phpn_vocabulary VALUES("6186","ID","_VEHICLE","Vehicle");
INSERT INTO phpn_vocabulary VALUES("4977","ID","_COMMENT_LENGTH_ALERT","The length of comment must be less than _LENGTH_ characters!");
INSERT INTO phpn_vocabulary VALUES("3710","en","_ACCOUNT_BALANCE","Account Balance");
INSERT INTO phpn_vocabulary VALUES("3713","en","_PAY_WITH_BALANCE","Pay with Balance");
INSERT INTO phpn_vocabulary VALUES("4976","ID","_COMMENT_DELETED_SUCCESS","Your comment has been successfully deleted.");
INSERT INTO phpn_vocabulary VALUES("3716","en","_LOGO","Logo");
INSERT INTO phpn_vocabulary VALUES("6185","ID","_EX_OTELS_VILLAS_OR_APARTMENTS","Ex.: hotels, villas or apartments");
INSERT INTO phpn_vocabulary VALUES("3719","en","_AGENCY_LOGIN","Agency Login");
INSERT INTO phpn_vocabulary VALUES("4975","ID","_COMMENTS_SETTINGS","Comments Settings");
INSERT INTO phpn_vocabulary VALUES("4965","ID","_CLICK_TO_SEE_PRICES","Click to see prices");
INSERT INTO phpn_vocabulary VALUES("4966","ID","_CLICK_TO_VIEW","Click to view");
INSERT INTO phpn_vocabulary VALUES("4967","ID","_CLOSE","Close");
INSERT INTO phpn_vocabulary VALUES("4968","ID","_CLOSE_META_TAGS","Close META tags");
INSERT INTO phpn_vocabulary VALUES("4969","ID","_CODE","Code");
INSERT INTO phpn_vocabulary VALUES("4970","ID","_COLLAPSE_PANEL","Collapse navigation panel");
INSERT INTO phpn_vocabulary VALUES("4971","ID","_COMMENTS","Comments");
INSERT INTO phpn_vocabulary VALUES("4972","ID","_COMMENTS_AWAITING_MODERATION_ALERT","There are _COUNT_ comment/s awaiting your moderation. Click <a href=\'index.php?admin=mod_comments_management\'>here</a> for review.");
INSERT INTO phpn_vocabulary VALUES("4973","ID","_COMMENTS_LINK","Comments (_COUNT_)");
INSERT INTO phpn_vocabulary VALUES("4974","ID","_COMMENTS_MANAGEMENT","Comments Management");
INSERT INTO phpn_vocabulary VALUES("3723","en","_WELCOME_AGENCY_TEXT","<p>Hello <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>\n<p>Welcome to Agency Account Panel, that allows you to view account status, manage your account settings and bookings.</p>\n<p>\n   _TODAY_<br />\n   _LAST_LOGIN_\n</p>				\n<p> <b>&#8226;</b> To view this account summary just click on a <a href=\'index.php?customer=home\'>Dashboard</a> link.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Edit My Account</a> menu allows you to change your personal info and account data.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>My Bookings</a> contains information about your orders.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("6171","ID","_DISTANCE_EXTRA_PRICE","Distance Extra Price");
INSERT INTO phpn_vocabulary VALUES("6172","ID","_CAR_HALF_HOUR_ALERT","Sorry, but you need to reserve a car for at least half an hour.");
INSERT INTO phpn_vocabulary VALUES("6173","ID","_GUESTS","Guests");
INSERT INTO phpn_vocabulary VALUES("6174","ID","_GEAR","Gear");
INSERT INTO phpn_vocabulary VALUES("6175","ID","_BAGGAGE","Baggage");
INSERT INTO phpn_vocabulary VALUES("6176","ID","_PER_DAY","per day");
INSERT INTO phpn_vocabulary VALUES("6177","ID","_PER_HOUR","per hour");
INSERT INTO phpn_vocabulary VALUES("6178","ID","_AUTOMATIC","Automatic");
INSERT INTO phpn_vocabulary VALUES("6179","ID","_MANUAL","Manual");
INSERT INTO phpn_vocabulary VALUES("6180","ID","_TIPTRONIC","Tiptronic");
INSERT INTO phpn_vocabulary VALUES("6181","ID","_DISTANCE_UNITS","km");
INSERT INTO phpn_vocabulary VALUES("6182","ID","_VEHICLE_DETAILS","Vehicle Details");
INSERT INTO phpn_vocabulary VALUES("6183","ID","_AGES","Ages");
INSERT INTO phpn_vocabulary VALUES("6184","ID","_RESERVATION_NUMBER","Reservation Number");
INSERT INTO phpn_vocabulary VALUES("3725","en","_INFORMATION","Information");
INSERT INTO phpn_vocabulary VALUES("3728","en","_STARTING_FROM","Starting from");
INSERT INTO phpn_vocabulary VALUES("6170","ID","_DEFAULT_DISTANCE","Default Distance");
INSERT INTO phpn_vocabulary VALUES("4964","ID","_CLICK_TO_SEE_DESCR","Click to see description");
INSERT INTO phpn_vocabulary VALUES("3731","en","_AGENCY","Agency");
INSERT INTO phpn_vocabulary VALUES("6169","ID","_MILEAGE","Mileage");
INSERT INTO phpn_vocabulary VALUES("3734","en","_AGENCY_PANEL","Agency Panel");
INSERT INTO phpn_vocabulary VALUES("4963","ID","_CLICK_TO_MANAGE","Click to manage");
INSERT INTO phpn_vocabulary VALUES("3737","en","_COUPON_FOR_SINGLE_HOTEL_ALERT","This discount coupon can be applied only for single hotel!");
INSERT INTO phpn_vocabulary VALUES("6167","ID","_VEHICLES","Vehicles");
INSERT INTO phpn_vocabulary VALUES("6168","ID","_REGISTRATION_NUMBER","Registration Number");
INSERT INTO phpn_vocabulary VALUES("4961","ID","_CLICK_TO_EDIT","Click to edit");
INSERT INTO phpn_vocabulary VALUES("4962","ID","_CLICK_TO_INCREASE","Click to enlarge");
INSERT INTO phpn_vocabulary VALUES("3740","en","_NOT_ENOUGH_MONEY_ALERT","Not enough money in your account balance: ");
INSERT INTO phpn_vocabulary VALUES("6165","ID","_VEHICLE_TYPE","Vehicle Type");
INSERT INTO phpn_vocabulary VALUES("6166","ID","_VEHICLE_TYPES","Vehicle Types");
INSERT INTO phpn_vocabulary VALUES("4960","ID","_CLICK_TO_COPY","Click to copy");
INSERT INTO phpn_vocabulary VALUES("3743","en","_CLICK_TO_PAY","Click To Pay");
INSERT INTO phpn_vocabulary VALUES("6164","ID","_CAR_AGENCY","Car Agency");
INSERT INTO phpn_vocabulary VALUES("3746","en","_YOU_MAY_ALSO_LIKE","You May Also Like");
INSERT INTO phpn_vocabulary VALUES("6163","ID","_AVAILABLE_CARS","Available Cars");
INSERT INTO phpn_vocabulary VALUES("4959","ID","_CLICK_FOR_MORE_INFO","Click for more information");
INSERT INTO phpn_vocabulary VALUES("3749","en","_MS_ALLOW_PAYMENT_WITH_BALANCE","Specifies whether to allow agencies to pay for bookings with their balance");
INSERT INTO phpn_vocabulary VALUES("6161","ID","_PICKING_UP","Picking up");
INSERT INTO phpn_vocabulary VALUES("6162","ID","_RENT_A_CAR","Rent a Car");
INSERT INTO phpn_vocabulary VALUES("4958","ID","_CLEAN_CACHE","Clean Cache");
INSERT INTO phpn_vocabulary VALUES("3752","en","_ALL_NEWS","All News");
INSERT INTO phpn_vocabulary VALUES("6160","ID","_DROPPING_OFF","Dropping off");
INSERT INTO phpn_vocabulary VALUES("3755","en","_LOW_BALANCE","low balance");
INSERT INTO phpn_vocabulary VALUES("3758","en","_ADD_FUNDS","Add Funds");
INSERT INTO phpn_vocabulary VALUES("6159","ID","_PICK_UP_DATE","Pick up date");
INSERT INTO phpn_vocabulary VALUES("3761","en","_FUNDS","Funds");
INSERT INTO phpn_vocabulary VALUES("3764","en","_ADDED_BY","Added By");
INSERT INTO phpn_vocabulary VALUES("6158","ID","_DROP_OFF_DATE","Drop off date");
INSERT INTO phpn_vocabulary VALUES("3767","en","_DATE_ADDED","Date Added");
INSERT INTO phpn_vocabulary VALUES("3770","en","_MS_SPECIAL_TAX","Special Tax per Guest");
INSERT INTO phpn_vocabulary VALUES("6157","ID","_MAP_CODE_TOOLTIP","Used only if longitude and latitude are not defined");
INSERT INTO phpn_vocabulary VALUES("4957","ID","_CLEANUP_TOOLTIP","The cleanup feature is used to remove pending (temporary) reservations from your web site. A pending reservation is one where the system is waiting for the payment gateway to callback with the transaction status.");
INSERT INTO phpn_vocabulary VALUES("3773","en","_GUEST_TAX","Guest Tax");
INSERT INTO phpn_vocabulary VALUES("6156","ID","_LONGITUDE","Longitude");
INSERT INTO phpn_vocabulary VALUES("4956","ID","_CLEANUP","Cleanup");
INSERT INTO phpn_vocabulary VALUES("3776","en","_CANNOT_REMOVE_FUNDS_ALERT","You cannot remove these funds because it may lead to negative balance for this customer.");
INSERT INTO phpn_vocabulary VALUES("6153","ID","_API_KEY","API Key");
INSERT INTO phpn_vocabulary VALUES("6154","ID","_PAYMENT_INFO","Payment Info");
INSERT INTO phpn_vocabulary VALUES("6155","ID","_LATITUDE","Latitude");
INSERT INTO phpn_vocabulary VALUES("4954","ID","_CITY_EMPTY_ALERT","City cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4955","ID","_CLEANED","Cleaned");
INSERT INTO phpn_vocabulary VALUES("3779","en","_FUNDS_INFORMATION","Funds Information");
INSERT INTO phpn_vocabulary VALUES("6152","ID","_API_LOGIN","API Login");
INSERT INTO phpn_vocabulary VALUES("4953","ID","_CITY","City");
INSERT INTO phpn_vocabulary VALUES("3782","en","_MSN_BANNERS_IS_ACTIVE","Activate Banners");
INSERT INTO phpn_vocabulary VALUES("6151","ID","_MODEL","Model");
INSERT INTO phpn_vocabulary VALUES("4952","ID","_CHILDREN","Children");
INSERT INTO phpn_vocabulary VALUES("3785","en","_MSN_ROTATION_TYPE","Rotation Type");
INSERT INTO phpn_vocabulary VALUES("6150","ID","_MAKE","Make");
INSERT INTO phpn_vocabulary VALUES("4951","ID","_CHILD","Child");
INSERT INTO phpn_vocabulary VALUES("3788","en","_MSN_ROTATE_DELAY","Rotation Delay");
INSERT INTO phpn_vocabulary VALUES("6149","ID","_PRICE_PER_HOUR","Price per Hour");
INSERT INTO phpn_vocabulary VALUES("4950","ID","_CHECK_STATUS","Check Status");
INSERT INTO phpn_vocabulary VALUES("3791","en","_MSN_BANNERS_CAPTION_HTML","HTML in Slideshow Caption");
INSERT INTO phpn_vocabulary VALUES("6148","ID","_PRICE_PER_DAY","Price per Day");
INSERT INTO phpn_vocabulary VALUES("4949","ID","_CHECK_OUT","Check Out");
INSERT INTO phpn_vocabulary VALUES("3794","en","_MSN_ACTIVATE_BOOKINGS","Activate Bookings");
INSERT INTO phpn_vocabulary VALUES("6147","ID","_TRANSMISSION","Transmission");
INSERT INTO phpn_vocabulary VALUES("4948","ID","_CHECK_NOW","Check Now");
INSERT INTO phpn_vocabulary VALUES("3797","en","_MSN_PAYMENT_TYPE_POA","&#8226; \'POA\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("6146","ID","_DOORS","Doors");
INSERT INTO phpn_vocabulary VALUES("4947","ID","_CHECK_IN","Check In");
INSERT INTO phpn_vocabulary VALUES("3800","en","_MSN_PAYMENT_TYPE_ONLINE","&#8226; \'On-line Order\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("6145","ID","_LUGGAGES","Luggages");
INSERT INTO phpn_vocabulary VALUES("4946","ID","_CHECK_AVAILABILITY","Check Availability");
INSERT INTO phpn_vocabulary VALUES("3803","en","_MSN_ONLINE_CREDIT_CARD_REQUIRED","&#8226; Credit Cards for \'On-line Orders\'");
INSERT INTO phpn_vocabulary VALUES("6143","ID","_CAR_AGENCIES","Car Agencies");
INSERT INTO phpn_vocabulary VALUES("6144","ID","_PASSENGERS","Passengers");
INSERT INTO phpn_vocabulary VALUES("4944","ID","_CHARGE_TYPE","Charge Type");
INSERT INTO phpn_vocabulary VALUES("4945","ID","_CHECKOUT","Checkout");
INSERT INTO phpn_vocabulary VALUES("3806","en","_MSN_PAYMENT_TYPE_BANK_TRANSFER","&#8226; \'Bank Transfer\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("6142","ID","_CAR_AGENCY_OWNERS","Car Agency Owners");
INSERT INTO phpn_vocabulary VALUES("4943","ID","_CHANGE_YOUR_PASSWORD","Change your password");
INSERT INTO phpn_vocabulary VALUES("3809","en","_MSN_BANK_TRANSFER_INFO","&#8226; \'Bank Transfer\' Info");
INSERT INTO phpn_vocabulary VALUES("6141","ID","_CAR_AGENCY_OWNER","Car Agency Owner");
INSERT INTO phpn_vocabulary VALUES("4942","ID","_CHANGE_ORDER","Change Order");
INSERT INTO phpn_vocabulary VALUES("3812","en","_MSN_PAYMENT_TYPE_PAYPAL","&#8226; PayPal Payment Type");
INSERT INTO phpn_vocabulary VALUES("6140","ID","_EVENTS_NEW_USER_REGISTERED","Events - new user has been registered");
INSERT INTO phpn_vocabulary VALUES("4941","ID","_CHANGE_CUSTOMER","Change Customer");
INSERT INTO phpn_vocabulary VALUES("3815","en","_MSN_PAYPAL_EMAIL","&nbsp; PayPal Email");
INSERT INTO phpn_vocabulary VALUES("6139","ID","_ADMIN_COPY","admin copy");
INSERT INTO phpn_vocabulary VALUES("3818","en","_MSN_PAYMENT_TYPE_2CO","&#8226; 2CO Payment Type	");
INSERT INTO phpn_vocabulary VALUES("6138","ID","_CARS","Cars");
INSERT INTO phpn_vocabulary VALUES("3821","en","_MSN_TWO_CHECKOUT_VENDOR","&#8226; 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("6137","ID","_AGENCY_ADMINS","Agency Admins");
INSERT INTO phpn_vocabulary VALUES("4940","ID","_CHANGES_WERE_SAVED","Changes were successfully saved! Please refresh the <a href=index.php>Home Page</a> to see the results.");
INSERT INTO phpn_vocabulary VALUES("3824","en","_MSN_PAYMENT_TYPE_AUTHORIZE","&#8226; Authorize.Net Payment Type");
INSERT INTO phpn_vocabulary VALUES("6136","ID","_GOOD","Good");
INSERT INTO phpn_vocabulary VALUES("4939","ID","_CHANGES_SAVED","Changes were saved.");
INSERT INTO phpn_vocabulary VALUES("3827","en","_MSN_AUTHORIZE_LOGIN_ID","&nbsp; Authorize.Net Login ID");
INSERT INTO phpn_vocabulary VALUES("6135","ID","_TRANSPORTATION_AGENCIES_MANAGEMENT","Transportation Agencies Management");
INSERT INTO phpn_vocabulary VALUES("3830","en","_MSN_AUTHORIZE_TRANSACTION_KEY","&nbsp; Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("6134","ID","_TRANSPORTATION_AGENCIES","Transportation Agencies");
INSERT INTO phpn_vocabulary VALUES("4938","ID","_CC_UNKNOWN_CARD_TYPE","Unknown card type! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3833","en","_MSN_DEFAULT_PAYMENT_SYSTEM","&nbsp; Default Payment System");
INSERT INTO phpn_vocabulary VALUES("6133","ID","_AGENCIES_INFO","Agencies Info");
INSERT INTO phpn_vocabulary VALUES("4937","ID","_CC_NUMBER_INVALID","Credit card number is invalid! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3836","en","_MSN_SEPARATE_GATEWAYS","Hotel Owners Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("6132","ID","_AGENCY_INFO","Agency Info");
INSERT INTO phpn_vocabulary VALUES("3839","en","_MSN_SEND_ORDER_COPY_TO_ADMIN","Send Order Copy To Admin");
INSERT INTO phpn_vocabulary VALUES("4936","ID","_CC_NO_CARD_NUMBER_PROVIDED","No card number provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3842","en","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","Allow Booking Without Account");
INSERT INTO phpn_vocabulary VALUES("6131","ID","_MD_CAR_RENTAL","The Car Rental module allows customers to search for cars and vehicles in required location, rent them and pay online.");
INSERT INTO phpn_vocabulary VALUES("4935","ID","_CC_CARD_WRONG_LENGTH","Credit card number has a wrong length! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3845","en","_MSN_PRE_PAYMENT_TYPE","Pre-Payment Type");
INSERT INTO phpn_vocabulary VALUES("3848","en","_MSN_PRE_PAYMENT_VALUE","Pre-Payment Value");
INSERT INTO phpn_vocabulary VALUES("6130","ID","_HOTEL_PAYMENT_GATEWAYS","Hotel Payment Gateways");
INSERT INTO phpn_vocabulary VALUES("3851","en","_MSN_VAT_VALUE","VAT Default Value");
INSERT INTO phpn_vocabulary VALUES("6129","ID","_CAR_RESERVATIONS","Car Reservations");
INSERT INTO phpn_vocabulary VALUES("4934","ID","_CC_CARD_WRONG_EXPIRE_DATE","Credit card expiry date is wrong! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3854","en","_MSN_MINIMUM_NIGHTS","Minimum Nights Stay");
INSERT INTO phpn_vocabulary VALUES("6128","ID","_CAR_INVENTORY","Car Inventory");
INSERT INTO phpn_vocabulary VALUES("3857","en","_MSN_MAXIMUM_NIGHTS","Maximum Nights Stay");
INSERT INTO phpn_vocabulary VALUES("6127","ID","_TYPES_AND_RATES","Types and Rates");
INSERT INTO phpn_vocabulary VALUES("4933","ID","_CC_CARD_NO_CVV_NUMBER","No CVV Code provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3860","en","_MSN_BOOKING_MODE","Payment Mode");
INSERT INTO phpn_vocabulary VALUES("3863","en","_MSN_SHOW_FULLY_BOOKED_ROOMS","Show Fully Booked Rooms");
INSERT INTO phpn_vocabulary VALUES("6126","ID","_TRAVEL_AGENCIES","Travel Agencies");
INSERT INTO phpn_vocabulary VALUES("4932","ID","_CC_CARD_INVALID_NUMBER","Credit card number is invalid! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3866","en","_MSN_PREBOOKING_ORDERS_TIMEOUT","\'Prebooking\' Orders Timeout");
INSERT INTO phpn_vocabulary VALUES("6124","ID","_CAR_RENTAL","Car Rental");
INSERT INTO phpn_vocabulary VALUES("6125","ID","_CAR_RENTAL_SETTINGS","Car Rental Settings");
INSERT INTO phpn_vocabulary VALUES("3869","en","_MSN_CUSTOMERS_CANCEL_RESERVATION","Customers May Cancel Reservation");
INSERT INTO phpn_vocabulary VALUES("4931","ID","_CC_CARD_INVALID_FORMAT","Credit card number has invalid format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3872","en","_MSN_SHOW_RESERVATION_FORM","Show Reservation Form");
INSERT INTO phpn_vocabulary VALUES("6123","ID","_MS_SEPARATE_GATEWAYS","Specifies whether to allow using separate payment gateways for each hotel");
INSERT INTO phpn_vocabulary VALUES("4930","ID","_CC_CARD_HOLDER_NAME_EMPTY","No card holder\'s name provided! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("3875","en","_MSN_RESERVATION_INITIAL_FEE","Booking Initial Fee");
INSERT INTO phpn_vocabulary VALUES("4929","ID","_CATEGORY_DESCRIPTION","Category Description");
INSERT INTO phpn_vocabulary VALUES("3878","en","_MSN_BOOKING_NUMBER_TYPE","Type of Booking Numbers");
INSERT INTO phpn_vocabulary VALUES("6122","ID","_MS_REVIEWS_KEY","The keyword that will be replaced with a list of customer reviews (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("4928","ID","_CATEGORY","Category");
INSERT INTO phpn_vocabulary VALUES("3881","en","_MSN_VAT_INCLUDED_IN_PRICE","Include VAT in Price");
INSERT INTO phpn_vocabulary VALUES("4927","ID","_CATEGORIES_MANAGEMENT","Categories Management");
INSERT INTO phpn_vocabulary VALUES("3884","en","_MSN_SHOW_BOOKING_STATUS_FORM","Show Booking Status Form");
INSERT INTO phpn_vocabulary VALUES("4926","ID","_CATEGORIES","Categories");
INSERT INTO phpn_vocabulary VALUES("3887","en","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","Maximum Allowed Reservations");
INSERT INTO phpn_vocabulary VALUES("6121","ID","_MD_REVIEWS","The Reviews Module allows the administrator of the site to add/edit visitor reviews, manage them and show on the Hotel Site frontend.");
INSERT INTO phpn_vocabulary VALUES("4925","ID","_CART_WAS_UPDATED","Reservation cart has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("3890","en","_MSN_FIRST_NIGHT_CALCULATING_TYPE","First Night Calculating Type");
INSERT INTO phpn_vocabulary VALUES("4924","ID","_CAPACITY","Capacity");
INSERT INTO phpn_vocabulary VALUES("3893","en","_MSN_AVAILABLE_UNTIL_APPROVAL","Available Until Approval");
INSERT INTO phpn_vocabulary VALUES("6120","ID","_MS_TC_DEFAULT_CONTACT_EMAIL","Specifies a default contact email when Car Agency info is not shown");
INSERT INTO phpn_vocabulary VALUES("4923","ID","_CAN_USE_TAGS_MSG","You can use some HTML tags, such as");
INSERT INTO phpn_vocabulary VALUES("3896","en","_MSN_RESERVATION_EXPIRED_ALERT","Reservation Expired Alert");
INSERT INTO phpn_vocabulary VALUES("4922","ID","_CANCELED_BY_CUSTOMER","This booking has been canceled by customer.");
INSERT INTO phpn_vocabulary VALUES("3899","en","_MSN_ADMIN_BOOKING_IN_PAST","Allow Booking in the Past");
INSERT INTO phpn_vocabulary VALUES("6119","ID","_MS_TC_DEFAULT_CONTACT_PHONE","Specifies a default contact phone number when Car Agency info is not shown");
INSERT INTO phpn_vocabulary VALUES("4921","ID","_CANCELED_BY_ADMIN","This booking is canceled by administrator.");
INSERT INTO phpn_vocabulary VALUES("3902","en","_MSN_ALLOW_PAYMENT_WITH_BALANCE","Allow Agencies to pay with their balance");
INSERT INTO phpn_vocabulary VALUES("6118","ID","_MSN_TC_DEFAULT_CONTACT_EMAIL","Default Contact Email");
INSERT INTO phpn_vocabulary VALUES("4920","ID","_CANCELED","Canceled");
INSERT INTO phpn_vocabulary VALUES("3905","en","_MSN_SPECIAL_TAX","Special Tax per Guest");
INSERT INTO phpn_vocabulary VALUES("3908","en","_MSN_COMMENTS_ALLOW","Allow Comments");
INSERT INTO phpn_vocabulary VALUES("6117","ID","_MSN_TC_DEFAULT_CONTACT_PHONE","Default Contact Phone");
INSERT INTO phpn_vocabulary VALUES("3911","en","_MSN_USER_TYPE","User Type");
INSERT INTO phpn_vocabulary VALUES("3914","en","_MSN_COMMENTS_LENGTH","Comment Length");
INSERT INTO phpn_vocabulary VALUES("6116","ID","_MSN_AGENCY_IN_SEARCH_RESULT","Agency Name in Search Result");
INSERT INTO phpn_vocabulary VALUES("3917","en","_MSN_IMAGE_VERIFICATION_ALLOW","Image Verification");
INSERT INTO phpn_vocabulary VALUES("4919","ID","_CAMPAIGNS_TOOLTIP","Global - allows booking for any date and runs (visible) within a defined period of time only\n\nTargeted - allows booking in a specified period of time only and runs (visible) till the first date is beginning");
INSERT INTO phpn_vocabulary VALUES("3920","en","_MSN_COMMENTS_PAGE_SIZE","Comments Per Page");
INSERT INTO phpn_vocabulary VALUES("6115","ID","_MS_AGENCY_IN_SEARCH_RESULT","Specifies whether to show agency name in search results");
INSERT INTO phpn_vocabulary VALUES("4918","ID","_CAMPAIGNS_MANAGEMENT","Campaigns Management");
INSERT INTO phpn_vocabulary VALUES("3923","en","_MSN_PRE_MODERATION_ALLOW","Comments Pre-Moderation");
INSERT INTO phpn_vocabulary VALUES("6114","ID","_MSN_REVIEWS_KEY","Reviews Key");
INSERT INTO phpn_vocabulary VALUES("4917","ID","_CAMPAIGNS","Campaigns");
INSERT INTO phpn_vocabulary VALUES("3926","en","_MSN_DELETE_PENDING_TIME","Pending Time");
INSERT INTO phpn_vocabulary VALUES("4916","ID","_CACHING","Caching");
INSERT INTO phpn_vocabulary VALUES("3929","en","_MSN_CONTACT_US_KEY","Contact Key");
INSERT INTO phpn_vocabulary VALUES("6113","ID","_MS_RESERVATION_NUMBER_TYPE","Specifies the type of reservation numbers");
INSERT INTO phpn_vocabulary VALUES("3932","en","_MSN_EMAIL","Contact Email");
INSERT INTO phpn_vocabulary VALUES("4915","ID","_CACHE_LIFETIME","Cache Lifetime");
INSERT INTO phpn_vocabulary VALUES("3935","en","_MSN_IS_SEND_DELAY","Sending Delay");
INSERT INTO phpn_vocabulary VALUES("6112","ID","_MSN_RESERVATION_NUMBER_TYPE","Type of Reservation Numbers");
INSERT INTO phpn_vocabulary VALUES("4914","ID","_BUTTON_UPDATE","Update");
INSERT INTO phpn_vocabulary VALUES("3938","en","_MSN_DELAY_LENGTH","Delay Length");
INSERT INTO phpn_vocabulary VALUES("4913","ID","_BUTTON_SAVE_CHANGES","Save Changes");
INSERT INTO phpn_vocabulary VALUES("3941","en","_MSN_ALLOW_ADDING_BY_ADMIN","Admin Creates Patients");
INSERT INTO phpn_vocabulary VALUES("4912","ID","_BUTTON_REWRITE","Rewrite Vocabulary");
INSERT INTO phpn_vocabulary VALUES("3944","en","_MSN_REG_CONFIRMATION","Confirmation Type");
INSERT INTO phpn_vocabulary VALUES("6111","ID","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Specifies whether to allow reservation for customer without creating account");
INSERT INTO phpn_vocabulary VALUES("4911","ID","_BUTTON_RESET","Reset");
INSERT INTO phpn_vocabulary VALUES("3947","en","_MSN_CUSTOMERS_IMAGE_VERIFICATION","Image Verification");
INSERT INTO phpn_vocabulary VALUES("4910","ID","_BUTTON_LOGOUT","Logout");
INSERT INTO phpn_vocabulary VALUES("3950","en","_MSN_ALLOW_CUSTOMERS_LOGIN","Allow Customers to Login");
INSERT INTO phpn_vocabulary VALUES("6110","ID","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Allow Reservation Without Account");
INSERT INTO phpn_vocabulary VALUES("4909","ID","_BUTTON_LOGIN","Login");
INSERT INTO phpn_vocabulary VALUES("3953","en","_MSN_ALLOW_CUSTOMERS_REGISTRATION","Allow Customers to Register");
INSERT INTO phpn_vocabulary VALUES("4908","ID","_BUTTON_CREATE","Create");
INSERT INTO phpn_vocabulary VALUES("3956","en","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","Admin Changes Customers Password");
INSERT INTO phpn_vocabulary VALUES("6109","ID","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","Specifies the number of months that will be shown on room occupancy tab");
INSERT INTO phpn_vocabulary VALUES("4907","ID","_BUTTON_CHANGE_PASSWORD","Change Password");
INSERT INTO phpn_vocabulary VALUES("3959","en","_MSN_ALLOW_CUST_RESET_PASSWORDS","Allow Reset Passwords");
INSERT INTO phpn_vocabulary VALUES("6108","ID","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","Rooms Occupancy Months");
INSERT INTO phpn_vocabulary VALUES("4906","ID","_BUTTON_CHANGE","Change");
INSERT INTO phpn_vocabulary VALUES("3962","en","_MSN_ALERT_ADMIN_NEW_REGISTRATION","Alert Admin On New Registration");
INSERT INTO phpn_vocabulary VALUES("6107","ID","_MS_SHOW_ROOMS_OCCUPANCY","Specifies whether to show occupancy on the room\'s description page");
INSERT INTO phpn_vocabulary VALUES("4904","ID","_BUTTON_BACK","Back");
INSERT INTO phpn_vocabulary VALUES("4905","ID","_BUTTON_CANCEL","Cancel");
INSERT INTO phpn_vocabulary VALUES("3965","en","_MSN_REMEMBER_ME","Remember Me");
INSERT INTO phpn_vocabulary VALUES("4903","ID","_BOTTOM","Bottom");
INSERT INTO phpn_vocabulary VALUES("3968","en","_MSN_ALLOW_AGENCIES","Allow Agencies");
INSERT INTO phpn_vocabulary VALUES("6106","ID","_MSN_SHOW_ROOMS_OCCUPANCY","Show Rooms Occupancy");
INSERT INTO phpn_vocabulary VALUES("3971","en","_MSN_FAQ_IS_ACTIVE","Activate FAQ");
INSERT INTO phpn_vocabulary VALUES("4902","ID","_BOOK_ONE_NIGHT_ALERT","Sorry, but you must book at least one night.");
INSERT INTO phpn_vocabulary VALUES("3974","en","_MSN_ACTIVATE_CAR_RENTAL","Activate Car Rental");
INSERT INTO phpn_vocabulary VALUES("6105","ID","_MSN_CHECK_PARTIALLY_OVERLAPPING","Check Partially Overlapping");
INSERT INTO phpn_vocabulary VALUES("4901","ID","_BOOK_NOW","Book Now");
INSERT INTO phpn_vocabulary VALUES("3977","en","_MS_ACTIVATE_CAR_RENTAL","Specifies whether car rental module is active or inactive");
INSERT INTO phpn_vocabulary VALUES("6104","ID","_MSN_MAX_CHILDREN_IN_SEARCH","Maximum Adults in Search Box");
INSERT INTO phpn_vocabulary VALUES("3980","en","_MSN_TC_SEPARATE_GATEWAYS","Car Agencies Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("4900","ID","_BOOKING_WAS_COMPLETED_MSG","Thank you for reservation rooms in our hotel! Your booking has been completed.");
INSERT INTO phpn_vocabulary VALUES("3983","en","_MS_TC_SEPARATE_GATEWAYS","Specifies whether to allow using separate payment gateways for car rental agency");
INSERT INTO phpn_vocabulary VALUES("6102","ID","_MSN_WATERMARK_TEXT","Watermark Text");
INSERT INTO phpn_vocabulary VALUES("6103","ID","_MSN_MAX_ADULTS_IN_SEARCH","Maximum Adults in Search Box");
INSERT INTO phpn_vocabulary VALUES("4899","ID","_BOOKING_WAS_CANCELED_MSG","Your booking has been canceled.");
INSERT INTO phpn_vocabulary VALUES("3986","en","_MSN_GALLERY_KEY","Gallery Key");
INSERT INTO phpn_vocabulary VALUES("6101","ID","_MSN_ADD_WATERMARK","Add Watermark to Images");
INSERT INTO phpn_vocabulary VALUES("4898","ID","_BOOKING_SUBTOTAL","Booking Subtotal");
INSERT INTO phpn_vocabulary VALUES("3989","en","_MSN_ALBUM_KEY","Album Key");
INSERT INTO phpn_vocabulary VALUES("4897","ID","_BOOKING_STATUS","Booking Status");
INSERT INTO phpn_vocabulary VALUES("3992","en","_MSN_IMAGE_GALLERY_TYPE","Image Gallery Type");
INSERT INTO phpn_vocabulary VALUES("6100","ID","_MSN_ALLOW_DEFAULT_PERIODS","Allow Default Periods");
INSERT INTO phpn_vocabulary VALUES("4896","ID","_BOOKING_SETTINGS","Booking Settings");
INSERT INTO phpn_vocabulary VALUES("3995","en","_MSN_ALBUM_ICON_WIDTH","Album Icon Width");
INSERT INTO phpn_vocabulary VALUES("6099","ID","_MSN_SHOW_DEFAULT_PRICES","Show Default Prices");
INSERT INTO phpn_vocabulary VALUES("4895","ID","_BOOKING_PRICE","Booking Price");
INSERT INTO phpn_vocabulary VALUES("3998","en","_MSN_ALBUM_ICON_HEIGHT","Album Icon Height");
INSERT INTO phpn_vocabulary VALUES("4894","ID","_BOOKING_NUMBER","Booking Number");
INSERT INTO phpn_vocabulary VALUES("4001","en","_MSN_ALBUMS_PER_LINE","Albums Per Line");
INSERT INTO phpn_vocabulary VALUES("6098","ID","_MSN_ALLOW_EXTRA_BEDS","Allow Extra Beds in Rooms");
INSERT INTO phpn_vocabulary VALUES("4893","ID","_BOOKING_DETAILS","Booking Details");
INSERT INTO phpn_vocabulary VALUES("4004","en","_MSN_VIDEO_GALLERY_TYPE","Video Gallery Type");
INSERT INTO phpn_vocabulary VALUES("4892","ID","_BOOKING_DESCRIPTION","Booking Description");
INSERT INTO phpn_vocabulary VALUES("4007","en","_MSN_GALLERY_WRAPPER","HTML Wrapping Tag");
INSERT INTO phpn_vocabulary VALUES("6097","ID","_MSN_ALLOW_SYSTEM_SUGGESTION","Allow System Suggestion");
INSERT INTO phpn_vocabulary VALUES("4891","ID","_BOOKING_DATE","Booking Date");
INSERT INTO phpn_vocabulary VALUES("4010","en","_MSN_ITEMS_COUNT_IN_ALBUM","Show Items Count in Album");
INSERT INTO phpn_vocabulary VALUES("6096","ID","_MSN_ALLOW_CHILDREN_IN_ROOM","Allow Children in Room");
INSERT INTO phpn_vocabulary VALUES("4890","ID","_BOOKING_COMPLETED","Booking Completed");
INSERT INTO phpn_vocabulary VALUES("4013","en","_MSN_ALBUM_ITEMS_NUMERATION","Show Items Numeration in Album");
INSERT INTO phpn_vocabulary VALUES("6095","ID","_MSN_ROOMS_IN_SEARCH","Show Rooms In Search");
INSERT INTO phpn_vocabulary VALUES("4016","en","_MSN_NEWS_COUNT","News Count");
INSERT INTO phpn_vocabulary VALUES("4889","ID","_BOOKING_CANCELED_SUCCESS","The booking _BOOKING_ has been successfully canceled from the system!");
INSERT INTO phpn_vocabulary VALUES("4019","en","_MSN_NEWS_HEADER_LENGTH","News Header Length");
INSERT INTO phpn_vocabulary VALUES("6094","ID","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","Search Availability Page Size");
INSERT INTO phpn_vocabulary VALUES("4022","en","_MSN_NEWS_RSS","News RSS");
INSERT INTO phpn_vocabulary VALUES("4888","ID","_BOOKING_CANCELED","Booking Canceled");
INSERT INTO phpn_vocabulary VALUES("4025","en","_MSN_SHOW_NEWS_BLOCK","News Block");
INSERT INTO phpn_vocabulary VALUES("6093","ID","_MSN_MULTIPLE_ITEMS_PER_DAY","Multiple Items per Day");
INSERT INTO phpn_vocabulary VALUES("4887","ID","_BOOKINGS_SETTINGS","Booking Settings");
INSERT INTO phpn_vocabulary VALUES("4028","en","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Newsletter Subscription");
INSERT INTO phpn_vocabulary VALUES("6092","ID","_MSN_RATINGS_USER_TYPE","User Type");
INSERT INTO phpn_vocabulary VALUES("4886","ID","_BOOKINGS_MANAGEMENT","Bookings Management");
INSERT INTO phpn_vocabulary VALUES("4031","en","_MSN_RATINGS_USER_TYPE","User Type");
INSERT INTO phpn_vocabulary VALUES("4885","ID","_BOOKINGS","Bookings");
INSERT INTO phpn_vocabulary VALUES("4034","en","_MSN_MULTIPLE_ITEMS_PER_DAY","Multiple Items per Day");
INSERT INTO phpn_vocabulary VALUES("6091","ID","_MSN_SHOW_NEWSLETTER_SUBSCRIBE_BLOCK","Newsletter Subscription");
INSERT INTO phpn_vocabulary VALUES("4884","ID","_BOOKING","Booking");
INSERT INTO phpn_vocabulary VALUES("4037","en","_MSN_SEARCH_AVAILABILITY_PAGE_SIZE","Search Availability Page Size");
INSERT INTO phpn_vocabulary VALUES("6089","ID","_MSN_NEWS_RSS","News RSS");
INSERT INTO phpn_vocabulary VALUES("6090","ID","_MSN_SHOW_NEWS_BLOCK","News Block");
INSERT INTO phpn_vocabulary VALUES("4883","ID","_BOOK","Book");
INSERT INTO phpn_vocabulary VALUES("4040","en","_MSN_ROOMS_IN_SEARCH","Show Rooms In Search");
INSERT INTO phpn_vocabulary VALUES("6088","ID","_MSN_NEWS_HEADER_LENGTH","News Header Length");
INSERT INTO phpn_vocabulary VALUES("4043","en","_MSN_ALLOW_CHILDREN_IN_ROOM","Allow Children in Room");
INSERT INTO phpn_vocabulary VALUES("6087","ID","_MSN_NEWS_COUNT","News Count");
INSERT INTO phpn_vocabulary VALUES("4882","ID","_BIRTH_DATE_VALID_ALERT","Birth date has been entered in wrong format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4046","en","_MSN_ALLOW_SYSTEM_SUGGESTION","Allow System Suggestion");
INSERT INTO phpn_vocabulary VALUES("4881","ID","_BIRTH_DATE","Birth Date");
INSERT INTO phpn_vocabulary VALUES("4049","en","_MSN_ALLOW_EXTRA_BEDS","Allow Extra Beds in Rooms");
INSERT INTO phpn_vocabulary VALUES("6086","ID","_MSN_ALBUM_ITEMS_NUMERATION","Show Items Numeration in Album");
INSERT INTO phpn_vocabulary VALUES("4880","ID","_BILLING_DETAILS_UPDATED","Your Billing Details has been updated.");
INSERT INTO phpn_vocabulary VALUES("4052","en","_MSN_SHOW_DEFAULT_PRICES","Show Default Prices");
INSERT INTO phpn_vocabulary VALUES("6085","ID","_MSN_ITEMS_COUNT_IN_ALBUM","Show Items Count in Album");
INSERT INTO phpn_vocabulary VALUES("4879","ID","_BILLING_DETAILS","Billing Details");
INSERT INTO phpn_vocabulary VALUES("4055","en","_MSN_ALLOW_DEFAULT_PERIODS","Allow Default Periods");
INSERT INTO phpn_vocabulary VALUES("6084","ID","_MSN_GALLERY_WRAPPER","HTML Wrapping Tag");
INSERT INTO phpn_vocabulary VALUES("4878","ID","_BILLING_ADDRESS","Billing Address");
INSERT INTO phpn_vocabulary VALUES("4058","en","_MSN_ADD_WATERMARK","Add Watermark to Images");
INSERT INTO phpn_vocabulary VALUES("6083","ID","_MSN_VIDEO_GALLERY_TYPE","Video Gallery Type");
INSERT INTO phpn_vocabulary VALUES("4877","ID","_BEFORE","Before");
INSERT INTO phpn_vocabulary VALUES("4061","en","_MSN_WATERMARK_TEXT","Watermark Text");
INSERT INTO phpn_vocabulary VALUES("6082","ID","_MSN_ALBUMS_PER_LINE","Albums Per Line");
INSERT INTO phpn_vocabulary VALUES("4876","ID","_BEDS","Beds");
INSERT INTO phpn_vocabulary VALUES("4064","en","_MSN_MAX_ADULTS_IN_SEARCH","Maximum Adults in Search Box");
INSERT INTO phpn_vocabulary VALUES("6081","ID","_MSN_ALBUM_ICON_HEIGHT","Album Icon Height");
INSERT INTO phpn_vocabulary VALUES("4875","ID","_BATHROOMS","Bathrooms");
INSERT INTO phpn_vocabulary VALUES("4067","en","_MSN_MAX_CHILDREN_IN_SEARCH","Maximum Adults in Search Box");
INSERT INTO phpn_vocabulary VALUES("6080","ID","_MSN_ALBUM_ICON_WIDTH","Album Icon Width");
INSERT INTO phpn_vocabulary VALUES("4874","ID","_BAN_LIST","Ban List");
INSERT INTO phpn_vocabulary VALUES("4070","en","_MSN_CHECK_PARTIALLY_OVERLAPPING","Check Partially Overlapping");
INSERT INTO phpn_vocabulary VALUES("6079","ID","_MSN_IMAGE_GALLERY_TYPE","Image Gallery Type");
INSERT INTO phpn_vocabulary VALUES("4872","ID","_BANNER_IMAGE","Banner Image");
INSERT INTO phpn_vocabulary VALUES("4873","ID","_BAN_ITEM","Ban Item");
INSERT INTO phpn_vocabulary VALUES("4073","en","_MSN_SHOW_ROOMS_OCCUPANCY","Show Rooms Occupancy");
INSERT INTO phpn_vocabulary VALUES("4076","en","_MS_SHOW_ROOMS_OCCUPANCY","Specifies whether to show occupancy on the room\'s description page");
INSERT INTO phpn_vocabulary VALUES("6077","ID","_MSN_GALLERY_KEY","Gallery Key");
INSERT INTO phpn_vocabulary VALUES("6078","ID","_MSN_ALBUM_KEY","Album Key");
INSERT INTO phpn_vocabulary VALUES("4870","ID","_BANNERS_MANAGEMENT","Banners Management");
INSERT INTO phpn_vocabulary VALUES("4871","ID","_BANNERS_SETTINGS","Banners Settings");
INSERT INTO phpn_vocabulary VALUES("4079","en","_MSN_SHOW_ROOMS_OCCUPANCY_MONTHS","Rooms Occupancy Months");
INSERT INTO phpn_vocabulary VALUES("4869","ID","_BANNERS","Banners");
INSERT INTO phpn_vocabulary VALUES("4082","en","_MS_SHOW_ROOMS_OCCUPANCY_MONTHS","Specifies the number of months that will be shown on room occupancy tab");
INSERT INTO phpn_vocabulary VALUES("6075","ID","_MSN_TC_SEPARATE_GATEWAYS","Car Agencies Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("6076","ID","_MS_TC_SEPARATE_GATEWAYS","Specifies whether to allow using separate payment gateways for car rental agency");
INSERT INTO phpn_vocabulary VALUES("4867","ID","_BANK_PAYMENT_INFO","Bank Payment Information");
INSERT INTO phpn_vocabulary VALUES("4868","ID","_BANK_TRANSFER","Bank Transfer");
INSERT INTO phpn_vocabulary VALUES("4085","en","_MSN_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Allow Reservation Without Account");
INSERT INTO phpn_vocabulary VALUES("4866","ID","_BACK_TO_ADMIN_PANEL","Back to Admin Panel");
INSERT INTO phpn_vocabulary VALUES("4088","en","_MS_ALLOW_RESERVATION_WITHOUT_ACCOUNT","Specifies whether to allow reservation for customer without creating account");
INSERT INTO phpn_vocabulary VALUES("6073","ID","_MSN_ACTIVATE_CAR_RENTAL","Activate Car Rental");
INSERT INTO phpn_vocabulary VALUES("6074","ID","_MS_ACTIVATE_CAR_RENTAL","Specifies whether car rental module is active or inactive");
INSERT INTO phpn_vocabulary VALUES("4865","ID","_BACKUP_YOUR_INSTALLATION","Backup your current Installation");
INSERT INTO phpn_vocabulary VALUES("4091","en","_MSN_RESERVATION_NUMBER_TYPE","Type of Reservation Numbers");
INSERT INTO phpn_vocabulary VALUES("6072","ID","_MSN_FAQ_IS_ACTIVE","Activate FAQ");
INSERT INTO phpn_vocabulary VALUES("4864","ID","_BACKUP_WAS_RESTORED","Backup _FILE_NAME_ has been successfully restored.");
INSERT INTO phpn_vocabulary VALUES("4094","en","_MS_RESERVATION_NUMBER_TYPE","Specifies the type of reservation numbers");
INSERT INTO phpn_vocabulary VALUES("6071","ID","_MSN_ALLOW_AGENCIES","Allow Agencies");
INSERT INTO phpn_vocabulary VALUES("4097","en","_MSN_REVIEWS_KEY","Reviews Key");
INSERT INTO phpn_vocabulary VALUES("4863","ID","_BACKUP_WAS_DELETED","Backup _FILE_NAME_ has been successfully deleted.");
INSERT INTO phpn_vocabulary VALUES("4100","en","_MS_AGENCY_IN_SEARCH_RESULT","Specifies whether to show agency name in search results");
INSERT INTO phpn_vocabulary VALUES("6069","ID","_MSN_ALERT_ADMIN_NEW_REGISTRATION","Alert Admin On New Registration");
INSERT INTO phpn_vocabulary VALUES("6070","ID","_MSN_REMEMBER_ME","Remember Me");
INSERT INTO phpn_vocabulary VALUES("4862","ID","_BACKUP_WAS_CREATED","Backup _FILE_NAME_ has been successfully created.");
INSERT INTO phpn_vocabulary VALUES("4103","en","_MSN_AGENCY_IN_SEARCH_RESULT","Agency Name in Search Result");
INSERT INTO phpn_vocabulary VALUES("6068","ID","_MSN_ALLOW_CUST_RESET_PASSWORDS","Allow Reset Passwords");
INSERT INTO phpn_vocabulary VALUES("4106","en","_MSN_TC_DEFAULT_CONTACT_PHONE","Default Contact Phone");
INSERT INTO phpn_vocabulary VALUES("6067","ID","_MSN_ADMIN_CHANGE_CUSTOMER_PASSWORD","Admin Changes Customers Password");
INSERT INTO phpn_vocabulary VALUES("4861","ID","_BACKUP_RESTORING_ERROR","An error occurred while restoring file! Please try again later.");
INSERT INTO phpn_vocabulary VALUES("4109","en","_MSN_TC_DEFAULT_CONTACT_EMAIL","Default Contact Email");
INSERT INTO phpn_vocabulary VALUES("6066","ID","_MSN_ALLOW_CUSTOMERS_REGISTRATION","Allow Customers to Register");
INSERT INTO phpn_vocabulary VALUES("4112","en","_MS_TC_DEFAULT_CONTACT_PHONE","Specifies a default contact phone number when Car Agency info is not shown");
INSERT INTO phpn_vocabulary VALUES("6065","ID","_MSN_ALLOW_CUSTOMERS_LOGIN","Allow Customers to Login");
INSERT INTO phpn_vocabulary VALUES("4860","ID","_BACKUP_RESTORE_NOTE","Remember: this action will rewrite all your current settings!");
INSERT INTO phpn_vocabulary VALUES("4115","en","_MS_TC_DEFAULT_CONTACT_EMAIL","Specifies a default contact email when Car Agency info is not shown");
INSERT INTO phpn_vocabulary VALUES("6063","ID","_MSN_REG_CONFIRMATION","Confirmation Type");
INSERT INTO phpn_vocabulary VALUES("6064","ID","_MSN_CUSTOMERS_IMAGE_VERIFICATION","Image Verification");
INSERT INTO phpn_vocabulary VALUES("4118","en","_MD_REVIEWS","The Reviews Module allows the administrator of the site to add/edit visitor reviews, manage them and show on the Hotel Site frontend.");
INSERT INTO phpn_vocabulary VALUES("6059","ID","_MSN_EMAIL","Contact Email");
INSERT INTO phpn_vocabulary VALUES("6060","ID","_MSN_IS_SEND_DELAY","Sending Delay");
INSERT INTO phpn_vocabulary VALUES("6061","ID","_MSN_DELAY_LENGTH","Delay Length");
INSERT INTO phpn_vocabulary VALUES("6062","ID","_MSN_ALLOW_ADDING_BY_ADMIN","Admin Creates Patients");
INSERT INTO phpn_vocabulary VALUES("4121","en","_MS_REVIEWS_KEY","The keyword that will be replaced with a list of customer reviews (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("6057","ID","_MSN_DELETE_PENDING_TIME","Pending Time");
INSERT INTO phpn_vocabulary VALUES("6058","ID","_MSN_CONTACT_US_KEY","Contact Key");
INSERT INTO phpn_vocabulary VALUES("4124","en","_MS_SEPARATE_GATEWAYS","Specifies whether to allow using separate payment gateways for each hotel");
INSERT INTO phpn_vocabulary VALUES("6056","ID","_MSN_PRE_MODERATION_ALLOW","Comments Pre-Moderation");
INSERT INTO phpn_vocabulary VALUES("4127","en","_CAR_RENTAL","Car Rental");
INSERT INTO phpn_vocabulary VALUES("6055","ID","_MSN_COMMENTS_PAGE_SIZE","Comments Per Page");
INSERT INTO phpn_vocabulary VALUES("4130","en","_CAR_RENTAL_SETTINGS","Car Rental Settings");
INSERT INTO phpn_vocabulary VALUES("6054","ID","_MSN_IMAGE_VERIFICATION_ALLOW","Image Verification");
INSERT INTO phpn_vocabulary VALUES("4133","en","_TRAVEL_AGENCIES","Travel Agencies");
INSERT INTO phpn_vocabulary VALUES("4136","en","_TYPES_AND_RATES","Types and Rates");
INSERT INTO phpn_vocabulary VALUES("6053","ID","_MSN_COMMENTS_LENGTH","Comment Length");
INSERT INTO phpn_vocabulary VALUES("4139","en","_CAR_INVENTORY","Car Inventory");
INSERT INTO phpn_vocabulary VALUES("6052","ID","_MSN_USER_TYPE","User Type");
INSERT INTO phpn_vocabulary VALUES("4142","en","_CAR_RESERVATIONS","Car Reservations");
INSERT INTO phpn_vocabulary VALUES("6051","ID","_MSN_COMMENTS_ALLOW","Allow Comments");
INSERT INTO phpn_vocabulary VALUES("4145","en","_HOTEL_PAYMENT_GATEWAYS","Hotel Payment Gateways");
INSERT INTO phpn_vocabulary VALUES("6050","ID","_MSN_SPECIAL_TAX","Special Tax per Guest");
INSERT INTO phpn_vocabulary VALUES("4148","en","_MD_CAR_RENTAL","The Car Rental module allows customers to search for cars and vehicles in required location, rent them and pay online.");
INSERT INTO phpn_vocabulary VALUES("6049","ID","_MSN_ALLOW_PAYMENT_WITH_BALANCE","Allow Agencies to pay with their balance");
INSERT INTO phpn_vocabulary VALUES("4151","en","_AGENCY_INFO","Agency Info");
INSERT INTO phpn_vocabulary VALUES("6048","ID","_MSN_ADMIN_BOOKING_IN_PAST","Allow Booking in the Past");
INSERT INTO phpn_vocabulary VALUES("4154","en","_AGENCIES_INFO","Agencies Info");
INSERT INTO phpn_vocabulary VALUES("4157","en","_TRANSPORTATION_AGENCIES","Transportation Agencies");
INSERT INTO phpn_vocabulary VALUES("6047","ID","_MSN_RESERVATION_EXPIRED_ALERT","Reservation Expired Alert");
INSERT INTO phpn_vocabulary VALUES("4160","en","_TRANSPORTATION_AGENCIES_MANAGEMENT","Transportation Agencies Management");
INSERT INTO phpn_vocabulary VALUES("4163","en","_GOOD","Good");
INSERT INTO phpn_vocabulary VALUES("6046","ID","_MSN_AVAILABLE_UNTIL_APPROVAL","Available Until Approval");
INSERT INTO phpn_vocabulary VALUES("4166","en","_AGENCY_ADMINS","Agency Admins");
INSERT INTO phpn_vocabulary VALUES("4169","en","_CARS","Cars");
INSERT INTO phpn_vocabulary VALUES("4172","en","_ADMIN_COPY","admin copy");
INSERT INTO phpn_vocabulary VALUES("6045","ID","_MSN_FIRST_NIGHT_CALCULATING_TYPE","First Night Calculating Type");
INSERT INTO phpn_vocabulary VALUES("4175","en","_EVENTS_NEW_USER_REGISTERED","Events - new user has been registered");
INSERT INTO phpn_vocabulary VALUES("6044","ID","_MSN_MAXIMUM_ALLOWED_RESERVATIONS","Maximum Allowed Reservations");
INSERT INTO phpn_vocabulary VALUES("4178","en","_CAR_AGENCY_OWNER","Car Agency Owner");
INSERT INTO phpn_vocabulary VALUES("4181","en","_CAR_AGENCY_OWNERS","Car Agency Owners");
INSERT INTO phpn_vocabulary VALUES("6043","ID","_MSN_SHOW_BOOKING_STATUS_FORM","Show Booking Status Form");
INSERT INTO phpn_vocabulary VALUES("4859","ID","_ADMIN_WELCOME_TEXT","<p>Welcome to Administrator Control Panel that allows you to add, edit or delete site content. With this Administrator Control Panel you can easy manage customers, reservations and perform a full hotel site management.</p><p><b>&#8226;</b> There are some modules for you: Backup & Restore, News. Installation or un-installation of them is possible from <a href=\'index.php?admin=modules\'>Modules Menu</a>.</p><p><b>&#8226;</b> In <a href=\'index.php?admin=languages\'>Languages Menu</a> you may add/remove language or change language settings and edit your vocabulary (the words and phrases, used by the system).</p><p><b>&#8226;</b> <a href=\'index.php?admin=settings\'>Settings Menu</a> allows you to define important settings for the site.</p><p><b>&#8226;</b> In <a href=\'index.php?admin=my_account\'>My Account</a> there is a possibility to change your info.</p><p><b>&#8226;</b> <a href=\'index.php?admin=menus\'>Menus</a> and <a href=\'index.php?admin=pages\'>Pages Management</a> are designed for creating and managing menus, links and pages.</p><p><b>&#8226;</b> To create and edit room types, seasons, prices, bookings and other hotel info, use <a href=\'index.php?admin=hotel_info\'>Hotel Management</a>, <a href=\'index.php?admin=rooms_management\'>Rooms Management</a> and <a href=\'index.php?admin=mod_booking_bookings\'>Bookings</a> menus.</p>");
INSERT INTO phpn_vocabulary VALUES("4184","en","_CAR_AGENCIES","Car Agencies");
INSERT INTO phpn_vocabulary VALUES("4187","en","_PASSENGERS","Passengers");
INSERT INTO phpn_vocabulary VALUES("6042","ID","_MSN_VAT_INCLUDED_IN_PRICE","Include VAT in Price");
INSERT INTO phpn_vocabulary VALUES("4190","en","_LUGGAGES","Luggages");
INSERT INTO phpn_vocabulary VALUES("4858","ID","_BACKUP_RESTORE_ALERT","Are you sure you want to restore this backup");
INSERT INTO phpn_vocabulary VALUES("4193","en","_DOORS","Doors");
INSERT INTO phpn_vocabulary VALUES("4196","en","_TRANSMISSION","Transmission");
INSERT INTO phpn_vocabulary VALUES("6041","ID","_MSN_BOOKING_NUMBER_TYPE","Type of Booking Numbers");
INSERT INTO phpn_vocabulary VALUES("4857","ID","_BACKUP_RESTORE","Backup Restore");
INSERT INTO phpn_vocabulary VALUES("4199","en","_PRICE_PER_DAY","Price per Day");
INSERT INTO phpn_vocabulary VALUES("4202","en","_PRICE_PER_HOUR","Price per Hour");
INSERT INTO phpn_vocabulary VALUES("4856","ID","_BACKUP_INSTALLATION","Backup Installation");
INSERT INTO phpn_vocabulary VALUES("4205","en","_MAKE","Make");
INSERT INTO phpn_vocabulary VALUES("6040","ID","_MSN_RESERVATION_INITIAL_FEE","Booking Initial Fee");
INSERT INTO phpn_vocabulary VALUES("4208","en","_MODEL","Model");
INSERT INTO phpn_vocabulary VALUES("4211","en","_API_LOGIN","API Login");
INSERT INTO phpn_vocabulary VALUES("4214","en","_API_KEY","API Key");
INSERT INTO phpn_vocabulary VALUES("6039","ID","_MSN_SHOW_RESERVATION_FORM","Show Reservation Form");
INSERT INTO phpn_vocabulary VALUES("4217","en","_PAYMENT_INFO","Payment Info");
INSERT INTO phpn_vocabulary VALUES("4220","en","_LATITUDE","Latitude");
INSERT INTO phpn_vocabulary VALUES("4223","en","_LONGITUDE","Longitude");
INSERT INTO phpn_vocabulary VALUES("6038","ID","_MSN_CUSTOMERS_CANCEL_RESERVATION","Customers May Cancel Reservation");
INSERT INTO phpn_vocabulary VALUES("4855","ID","_BACKUP_EXECUTING_ERROR","An error occurred while backup the system! Please check write permissions to backup folder or try again later.");
INSERT INTO phpn_vocabulary VALUES("4226","en","_MAP_CODE_TOOLTIP","Used only if longitude and latitude are not defined");
INSERT INTO phpn_vocabulary VALUES("6037","ID","_MSN_PREBOOKING_ORDERS_TIMEOUT","\'Prebooking\' Orders Timeout");
INSERT INTO phpn_vocabulary VALUES("4854","ID","_BACKUP_EMPTY_NAME_ALERT","Name of backup file cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4229","en","_DROP_OFF_DATE","Drop off date");
INSERT INTO phpn_vocabulary VALUES("4232","en","_PICK_UP_DATE","Pick up date");
INSERT INTO phpn_vocabulary VALUES("6036","ID","_MSN_SHOW_FULLY_BOOKED_ROOMS","Show Fully Booked Rooms");
INSERT INTO phpn_vocabulary VALUES("4853","ID","_BACKUP_EMPTY_MSG","No existing backups found.");
INSERT INTO phpn_vocabulary VALUES("4235","en","_DROPPING_OFF","Dropping off");
INSERT INTO phpn_vocabulary VALUES("4238","en","_PICKING_UP","Picking up");
INSERT INTO phpn_vocabulary VALUES("6035","ID","_MSN_BOOKING_MODE","Payment Mode");
INSERT INTO phpn_vocabulary VALUES("4241","en","_RENT_A_CAR","Rent a Car");
INSERT INTO phpn_vocabulary VALUES("4852","ID","_BACKUP_DELETE_ALERT","Are you sure you want to delete this backup?");
INSERT INTO phpn_vocabulary VALUES("4244","en","_AVAILABLE_CARS","Available Cars");
INSERT INTO phpn_vocabulary VALUES("6034","ID","_MSN_MAXIMUM_NIGHTS","Maximum Nights Stay");
INSERT INTO phpn_vocabulary VALUES("4247","en","_CAR_AGENCY","Car Agency");
INSERT INTO phpn_vocabulary VALUES("4250","en","_VEHICLE_TYPE","Vehicle Type");
INSERT INTO phpn_vocabulary VALUES("6033","ID","_MSN_MINIMUM_NIGHTS","Minimum Nights Stay");
INSERT INTO phpn_vocabulary VALUES("4851","ID","_BACKUP_CHOOSE_MSG","Choose a backup from the list below");
INSERT INTO phpn_vocabulary VALUES("4253","en","_VEHICLE_TYPES","Vehicle Types");
INSERT INTO phpn_vocabulary VALUES("4256","en","_VEHICLES","Vehicles");
INSERT INTO phpn_vocabulary VALUES("6032","ID","_MSN_VAT_VALUE","VAT Default Value");
INSERT INTO phpn_vocabulary VALUES("4850","ID","_BACKUP_AND_RESTORE","Backup & Restore");
INSERT INTO phpn_vocabulary VALUES("4259","en","_REGISTRATION_NUMBER","Registration Number");
INSERT INTO phpn_vocabulary VALUES("4849","ID","_BACKUPS_EXISTING","Existing Backups");
INSERT INTO phpn_vocabulary VALUES("4262","en","_MILEAGE","Mileage");
INSERT INTO phpn_vocabulary VALUES("6031","ID","_MSN_PRE_PAYMENT_VALUE","Pre-Payment Value");
INSERT INTO phpn_vocabulary VALUES("4848","ID","_BACKUP","Backup");
INSERT INTO phpn_vocabulary VALUES("4265","en","_DEFAULT_DISTANCE","Default Distance");
INSERT INTO phpn_vocabulary VALUES("6030","ID","_MSN_PRE_PAYMENT_TYPE","Pre-Payment Type");
INSERT INTO phpn_vocabulary VALUES("4847","ID","_AVAILABLE_ROOMS","Available Rooms");
INSERT INTO phpn_vocabulary VALUES("4268","en","_DISTANCE_EXTRA_PRICE","Distance Extra Price");
INSERT INTO phpn_vocabulary VALUES("4846","ID","_AVAILABLE","available");
INSERT INTO phpn_vocabulary VALUES("4271","en","_CAR_HALF_HOUR_ALERT","Sorry, but you need to reserve a car for at least half an hour.");
INSERT INTO phpn_vocabulary VALUES("6029","ID","_MSN_ALLOW_BOOKING_WITHOUT_ACCOUNT","Allow Booking Without Account");
INSERT INTO phpn_vocabulary VALUES("4274","en","_GUESTS","Guests");
INSERT INTO phpn_vocabulary VALUES("4277","en","_GEAR","Gear");
INSERT INTO phpn_vocabulary VALUES("6028","ID","_MSN_SEND_ORDER_COPY_TO_ADMIN","Send Order Copy To Admin");
INSERT INTO phpn_vocabulary VALUES("4280","en","_BAGGAGE","Baggage");
INSERT INTO phpn_vocabulary VALUES("4283","en","_PER_DAY","per day");
INSERT INTO phpn_vocabulary VALUES("4286","en","_PER_HOUR","per hour");
INSERT INTO phpn_vocabulary VALUES("6027","ID","_MSN_SEPARATE_GATEWAYS","Hotel Owners Separate Gateways");
INSERT INTO phpn_vocabulary VALUES("4289","en","_AUTOMATIC","Automatic");
INSERT INTO phpn_vocabulary VALUES("4292","en","_MANUAL","Manual");
INSERT INTO phpn_vocabulary VALUES("4295","en","_TIPTRONIC","Tiptronic");
INSERT INTO phpn_vocabulary VALUES("6026","ID","_MSN_DEFAULT_PAYMENT_SYSTEM","&nbsp; Default Payment System");
INSERT INTO phpn_vocabulary VALUES("4298","en","_DISTANCE_UNITS","km");
INSERT INTO phpn_vocabulary VALUES("4845","ID","_AVAILABILITY_ROOMS_NOTE","Define a maximum number of rooms available for booking for a specified day or date range (maximum availability _MAX_ rooms)<br>To edit room availability simply change the value in a day cell and then click \'Save Changes\' button");
INSERT INTO phpn_vocabulary VALUES("4301","en","_VEHICLE_DETAILS","Vehicle Details");
INSERT INTO phpn_vocabulary VALUES("4304","en","_AGES","Ages");
INSERT INTO phpn_vocabulary VALUES("6025","ID","_MSN_AUTHORIZE_TRANSACTION_KEY","&nbsp; Authorize.Net Transaction Key");
INSERT INTO phpn_vocabulary VALUES("4844","ID","_AVAILABILITY","Availability");
INSERT INTO phpn_vocabulary VALUES("4307","en","_RESERVATION_NUMBER","Reservation Number");
INSERT INTO phpn_vocabulary VALUES("4310","en","_EX_OTELS_VILLAS_OR_APARTMENTS","Ex.: hotels, villas or apartments");
INSERT INTO phpn_vocabulary VALUES("6024","ID","_MSN_AUTHORIZE_LOGIN_ID","&nbsp; Authorize.Net Login ID");
INSERT INTO phpn_vocabulary VALUES("4843","ID","_AUTHORIZE_NET_ORDER","Authorize.Net Order");
INSERT INTO phpn_vocabulary VALUES("4313","en","_VEHICLE","Vehicle");
INSERT INTO phpn_vocabulary VALUES("4316","en","_RENTAL_FROM","Rental From");
INSERT INTO phpn_vocabulary VALUES("6023","ID","_MSN_PAYMENT_TYPE_AUTHORIZE","&#8226; Authorize.Net Payment Type");
INSERT INTO phpn_vocabulary VALUES("4319","en","_RENTAL_TO","Rental To");
INSERT INTO phpn_vocabulary VALUES("4842","ID","_AUTHORIZE_NET_NOTICE","The Authorize.Net payment gateway service provider.");
INSERT INTO phpn_vocabulary VALUES("4322","en","_CAR_OWNER_NOT_ASSIGNED","You still has not been assigned to any car agency to see the reports.");
INSERT INTO phpn_vocabulary VALUES("6021","ID","_MSN_PAYMENT_TYPE_2CO","&#8226; 2CO Payment Type	");
INSERT INTO phpn_vocabulary VALUES("6022","ID","_MSN_TWO_CHECKOUT_VENDOR","&#8226; 2CO Vendor ID");
INSERT INTO phpn_vocabulary VALUES("4840","ID","_AUGUST","August");
INSERT INTO phpn_vocabulary VALUES("4841","ID","_AUTHENTICATION","Authentication");
INSERT INTO phpn_vocabulary VALUES("4325","en","_LAST_HOTEL_PROPERTY_ALERT","This property type cannot be deleted, because it is participating in one property at least.");
INSERT INTO phpn_vocabulary VALUES("6019","ID","_MSN_PAYMENT_TYPE_PAYPAL","&#8226; PayPal Payment Type");
INSERT INTO phpn_vocabulary VALUES("6020","ID","_MSN_PAYPAL_EMAIL","&nbsp; PayPal Email");
INSERT INTO phpn_vocabulary VALUES("4837","ID","_APRIL","April");
INSERT INTO phpn_vocabulary VALUES("4838","ID","_ARTICLE","Article");
INSERT INTO phpn_vocabulary VALUES("4839","ID","_ARTICLE_ID","Article ID");
INSERT INTO phpn_vocabulary VALUES("4328","en","_CAR_BOOKING_COMPLETED","Car booking was completed");
INSERT INTO phpn_vocabulary VALUES("6018","ID","_MSN_BANK_TRANSFER_INFO","&#8226; \'Bank Transfer\' Info");
INSERT INTO phpn_vocabulary VALUES("4836","ID","_APPROVED","Approved");
INSERT INTO phpn_vocabulary VALUES("4331","en","_CAR_WAS_COMPLETED_MSG","Thank you for reservation car! Your booking has been completed.");
INSERT INTO phpn_vocabulary VALUES("4835","ID","_APPROVE","Approve");
INSERT INTO phpn_vocabulary VALUES("4334","en","_CAR_TYPE","Car Type");
INSERT INTO phpn_vocabulary VALUES("6017","ID","_MSN_PAYMENT_TYPE_BANK_TRANSFER","&#8226; \'Bank Transfer\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("4834","ID","_APPLY_TO_ALL_PAGES","Apply changes to all pages");
INSERT INTO phpn_vocabulary VALUES("4337","en","_MY_CAR_RENTALS","My Car Rentals");
INSERT INTO phpn_vocabulary VALUES("4340","en","_PAID_SUM","Paid Sum");
INSERT INTO phpn_vocabulary VALUES("4833","ID","_APPLY_TO_ALL_LANGUAGES","Apply to all languages");
INSERT INTO phpn_vocabulary VALUES("4343","en","_CAR_PICKING_UP","Car Picking Up");
INSERT INTO phpn_vocabulary VALUES("6016","ID","_MSN_ONLINE_CREDIT_CARD_REQUIRED","&#8226; Credit Cards for \'On-line Orders\'");
INSERT INTO phpn_vocabulary VALUES("4832","ID","_APPLY","Apply");
INSERT INTO phpn_vocabulary VALUES("4346","en","_CAR_RETURNING","Car Returning");
INSERT INTO phpn_vocabulary VALUES("4831","ID","_ANY","Any");
INSERT INTO phpn_vocabulary VALUES("4349","en","_CARS_IN_RENTAL","Cars In Rental");
INSERT INTO phpn_vocabulary VALUES("4830","ID","_ANSWER","Answer");
INSERT INTO phpn_vocabulary VALUES("4352","en","_CAR_WAS_ADDED","Car has been successfully added to your reservation!");
INSERT INTO phpn_vocabulary VALUES("6015","ID","_MSN_PAYMENT_TYPE_ONLINE","&#8226; \'On-line Order\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("4829","ID","_AMOUNT","Amount");
INSERT INTO phpn_vocabulary VALUES("4355","en","_SELECTED_CARS","Selected Cars");
INSERT INTO phpn_vocabulary VALUES("6014","ID","_MSN_PAYMENT_TYPE_POA","&#8226; \'POA\' Payment Type");
INSERT INTO phpn_vocabulary VALUES("4828","ID","_ALREADY_LOGGED","You are already logged in!");
INSERT INTO phpn_vocabulary VALUES("4358","en","_REMOVE_CAR_FROM_CART","Remove car from the cart");
INSERT INTO phpn_vocabulary VALUES("4361","en","_CAR_WAS_REMOVED","Selected car has been successfully removed from your Reservation Cart!");
INSERT INTO phpn_vocabulary VALUES("6012","ID","_MSN_BANNERS_CAPTION_HTML","HTML in Slideshow Caption");
INSERT INTO phpn_vocabulary VALUES("6013","ID","_MSN_ACTIVATE_BOOKINGS","Activate Bookings");
INSERT INTO phpn_vocabulary VALUES("4826","ID","_ALL_AVAILABLE","All Available");
INSERT INTO phpn_vocabulary VALUES("4827","ID","_ALREADY_HAVE_ACCOUNT","Already have an account? <a href=\'index.php?customer=login\'>Login here</a>");
INSERT INTO phpn_vocabulary VALUES("4364","en","_CAR_NOT_FOUND","Car has not been found!");
INSERT INTO phpn_vocabulary VALUES("6011","ID","_MSN_ROTATE_DELAY","Rotation Delay");
INSERT INTO phpn_vocabulary VALUES("4825","ID","_ALLOW_COMMENTS","Allow comments");
INSERT INTO phpn_vocabulary VALUES("4367","en","_CARS_RESERVATION","Cars Reservation");
INSERT INTO phpn_vocabulary VALUES("4824","ID","_ALLOW","Allow");
INSERT INTO phpn_vocabulary VALUES("4370","en","_TIME","Time");
INSERT INTO phpn_vocabulary VALUES("6010","ID","_MSN_ROTATION_TYPE","Rotation Type");
INSERT INTO phpn_vocabulary VALUES("4823","ID","_ALL","All");
INSERT INTO phpn_vocabulary VALUES("4373","en","_FUEL_TYPE","Fuel Type");
INSERT INTO phpn_vocabulary VALUES("4376","en","_BIODIESEL","Biodiesel");
INSERT INTO phpn_vocabulary VALUES("4379","en","_CNG","CNG");
INSERT INTO phpn_vocabulary VALUES("6009","ID","_MSN_BANNERS_IS_ACTIVE","Activate Banners");
INSERT INTO phpn_vocabulary VALUES("4382","en","_DIESEL","Diesel");
INSERT INTO phpn_vocabulary VALUES("4385","en","_ELECTRIC","Electric");
INSERT INTO phpn_vocabulary VALUES("6008","ID","_FUNDS_INFORMATION","Funds Information");
INSERT INTO phpn_vocabulary VALUES("4822","ID","_ALERT_REQUIRED_FILEDS","Items marked with an asterisk (*) are required");
INSERT INTO phpn_vocabulary VALUES("4388","en","_ETHANOL_FFV","Ethanol FFV");
INSERT INTO phpn_vocabulary VALUES("4391","en","_GASOLINE","Gasoline");
INSERT INTO phpn_vocabulary VALUES("4394","en","_MAKES","Makes");
INSERT INTO phpn_vocabulary VALUES("4397","en","_HYBRID_ELECTRIC","Hybrid Electric");
INSERT INTO phpn_vocabulary VALUES("4821","ID","_ALERT_CANCEL_BOOKING","Are you sure you want to cancel this booking?");
INSERT INTO phpn_vocabulary VALUES("4400","en","_PETROL","Petrol");
INSERT INTO phpn_vocabulary VALUES("4403","en","_STEAM","Steam");
INSERT INTO phpn_vocabulary VALUES("6007","ID","_CANNOT_REMOVE_FUNDS_ALERT","You cannot remove these funds because it may lead to negative balance for this customer.");
INSERT INTO phpn_vocabulary VALUES("4820","ID","_ALBUM_NAME","Album Name");
INSERT INTO phpn_vocabulary VALUES("4406","en","_TIME_INTERVAL","Time Interval");
INSERT INTO phpn_vocabulary VALUES("6006","ID","_GUEST_TAX","Guest Tax");
INSERT INTO phpn_vocabulary VALUES("4819","ID","_ALBUM_CODE","Album Code");
INSERT INTO phpn_vocabulary VALUES("4409","en","_AGENCY_NAME","Agency Name");
INSERT INTO phpn_vocabulary VALUES("4818","ID","_ALBUM","Album");
INSERT INTO phpn_vocabulary VALUES("4412","en","_AGENCYOWNER_WELCOME_TEXT","<p>Welcome to Car Agency Control Panel that allows you to manage information related to your transportation agency. With this Control Panel you can easy manage agency info, vehicles, vehicle types and perform a full car reservations management.</p>");
INSERT INTO phpn_vocabulary VALUES("5999","ID","_ALL_NEWS","All News");
INSERT INTO phpn_vocabulary VALUES("6000","ID","_LOW_BALANCE","low balance");
INSERT INTO phpn_vocabulary VALUES("6001","ID","_ADD_FUNDS","Add Funds");
INSERT INTO phpn_vocabulary VALUES("6002","ID","_FUNDS","Funds");
INSERT INTO phpn_vocabulary VALUES("6003","ID","_ADDED_BY","Added By");
INSERT INTO phpn_vocabulary VALUES("6004","ID","_DATE_ADDED","Date Added");
INSERT INTO phpn_vocabulary VALUES("6005","ID","_MS_SPECIAL_TAX","Special Tax per Guest");
INSERT INTO phpn_vocabulary VALUES("4813","ID","_ADVANCED","Advanced");
INSERT INTO phpn_vocabulary VALUES("4814","ID","_AFTER","After");
INSERT INTO phpn_vocabulary VALUES("4815","ID","_AFTER_DISCOUNT","after discount");
INSERT INTO phpn_vocabulary VALUES("4816","ID","_AGENT_COMMISION","Hotel Owner/Agent Commision");
INSERT INTO phpn_vocabulary VALUES("4817","ID","_AGREE_CONF_TEXT","I have read and AGREE with Terms & Conditions");
INSERT INTO phpn_vocabulary VALUES("4415","en","_AGENCY_CAR_TYPES","Agency Car Types");
INSERT INTO phpn_vocabulary VALUES("4812","ID","_ADULTS","Adults");
INSERT INTO phpn_vocabulary VALUES("4418","en","_VEHICLE_CATEGORIES","Vehicle Categories");
INSERT INTO phpn_vocabulary VALUES("5998","ID","_MS_ALLOW_PAYMENT_WITH_BALANCE","Specifies whether to allow agencies to pay for bookings with their balance");
INSERT INTO phpn_vocabulary VALUES("4811","ID","_ADULT","Adult");
INSERT INTO phpn_vocabulary VALUES("4421","en","_VEHICLE_CATEGORY","Vehicle Category");
INSERT INTO phpn_vocabulary VALUES("4810","ID","_ADMIN_RESERVATION","Admin Reservation");
INSERT INTO phpn_vocabulary VALUES("4424","en","_PREORDERING","Pre-Ordering");
INSERT INTO phpn_vocabulary VALUES("5997","ID","_YOU_MAY_ALSO_LIKE","You May Also Like");
INSERT INTO phpn_vocabulary VALUES("4809","ID","_ADMIN_PANEL","Admin Panel");
INSERT INTO phpn_vocabulary VALUES("4427","en","_LEGEND_PREORDERING","This is a system status for a reservation that is in progress, or was not completed (car has been added to Reservation Cart, but still not reserved)");
INSERT INTO phpn_vocabulary VALUES("5995","ID","_NOT_ENOUGH_MONEY_ALERT","Not enough money in your account balance: ");
INSERT INTO phpn_vocabulary VALUES("5996","ID","_CLICK_TO_PAY","Click To Pay");
INSERT INTO phpn_vocabulary VALUES("4807","ID","_ADMIN_LOGIN","Admin Login");
INSERT INTO phpn_vocabulary VALUES("4808","ID","_ADMIN_MAILER_ALERT","Select which mailer you prefer to use for the delivery of site emails.");
INSERT INTO phpn_vocabulary VALUES("4430","en","_NO_CARS_FOUND","No results found for your search conditions");
INSERT INTO phpn_vocabulary VALUES("5994","ID","_COUPON_FOR_SINGLE_HOTEL_ALERT","This discount coupon can be applied only for single hotel!");
INSERT INTO phpn_vocabulary VALUES("4433","en","_PRICE_RANGE","Price range/per day");
INSERT INTO phpn_vocabulary VALUES("5993","ID","_AGENCY_PANEL","Agency Panel");
INSERT INTO phpn_vocabulary VALUES("4436","en","_MANUFACTURER","Manufacturer");
INSERT INTO phpn_vocabulary VALUES("5992","ID","_AGENCY","Agency");
INSERT INTO phpn_vocabulary VALUES("4439","en","_ASCENDING","Ascending");
INSERT INTO phpn_vocabulary VALUES("4806","ID","_ADMIN_FOLDER_CREATION_ERROR","Failed to create folder for this author in <b>images/upload/</b> directory. For stable work of the script please create this folder manually.");
INSERT INTO phpn_vocabulary VALUES("4442","en","_DESCENDING","Descending");
INSERT INTO phpn_vocabulary VALUES("5991","ID","_STARTING_FROM","Starting from");
INSERT INTO phpn_vocabulary VALUES("4445","en","_NO_MATCHES_FOUND","No matches found");
INSERT INTO phpn_vocabulary VALUES("4805","ID","_ADMIN_EMAIL_WRONG","Admin email in wrong format! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4448","en","_LEGEND_CAR_PENDING","The car reservation has been created, but not yet been confirmed and reserved");
INSERT INTO phpn_vocabulary VALUES("5990","ID","_INFORMATION","Information");
INSERT INTO phpn_vocabulary VALUES("4804","ID","_ADMIN_EMAIL_IS_EMPTY","Admin email must not be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4451","en","_LEGEND_CAR_RESERVED","Car is reserved, but order is not paid yet (pending)");
INSERT INTO phpn_vocabulary VALUES("4803","ID","_ADMIN_EMAIL_EXISTS_ALERT","Administrator with such email already exists! Please choose another.");
INSERT INTO phpn_vocabulary VALUES("4454","en","_TYPE_YOUR_PICKUP_LOCATION","type your picking up location");
INSERT INTO phpn_vocabulary VALUES("4457","en","_TYPE_YOUR_DROPPING_OFF_LOCATION","type your dropping off location");
INSERT INTO phpn_vocabulary VALUES("4460","en","_PLEASE_SELECT_LOCATION","Please select both locations within a same country");
INSERT INTO phpn_vocabulary VALUES("4463","en","_LOWEST_HIGHEST","Lowest to Highest");
INSERT INTO phpn_vocabulary VALUES("4802","ID","_ADMIN_EMAIL_ALERT","This email is used as \"From\" address for the system email notifications. Make sure, that you write here a valid email address based on domain of your site");
INSERT INTO phpn_vocabulary VALUES("4466","en","_HIGHEST_LOWEST","Highest to Lowest");
INSERT INTO phpn_vocabulary VALUES("4801","ID","_ADMIN_EMAIL","Admin Email");
INSERT INTO phpn_vocabulary VALUES("4469","en","_FOUND_AGENCIES","Found Agencies");
INSERT INTO phpn_vocabulary VALUES("4472","en","_TOTAL_CARS","Total Cars");
INSERT INTO phpn_vocabulary VALUES("4475","en","_DAYS","Days");
INSERT INTO phpn_vocabulary VALUES("4800","ID","_ADMINS_OWNERS_MANAGEMENT","Admins & Hotel Owners Management");
INSERT INTO phpn_vocabulary VALUES("4478","en","_PERIOD","Period");
INSERT INTO phpn_vocabulary VALUES("4481","en","_VOTE","vote");
INSERT INTO phpn_vocabulary VALUES("4484","en","_VOTES","votes");
INSERT INTO phpn_vocabulary VALUES("4799","ID","_ADMINS_MANAGEMENT","Admins Management");
INSERT INTO phpn_vocabulary VALUES("4487","en","_MOST_POPULAR_HOTELS","Most Popular Hotels");
INSERT INTO phpn_vocabulary VALUES("4490","en","_LAST_BOOKINGS","Last Bookings");
INSERT INTO phpn_vocabulary VALUES("4798","ID","_ADMINS_AND_CUSTOMERS","Customers & Admins");
INSERT INTO phpn_vocabulary VALUES("4493","en","_LAST_BOOKINGS_MESSAGE","A visitor from {user_location} {type_booking} {count_rooms} at {hotel_name} in {hotel_location}");
INSERT INTO phpn_vocabulary VALUES("5987","ID","_LOGO","Logo");
INSERT INTO phpn_vocabulary VALUES("5988","ID","_AGENCY_LOGIN","Agency Login");
INSERT INTO phpn_vocabulary VALUES("5989","ID","_WELCOME_AGENCY_TEXT","<p>Hello <b>_FIRST_NAME_ _LAST_NAME_</b>!</p>\n<p>Welcome to Agency Account Panel, that allows you to view account status, manage your account settings and bookings.</p>\n<p>\n   _TODAY_<br />\n   _LAST_LOGIN_\n</p>				\n<p> <b>&#8226;</b> To view this account summary just click on a <a href=\'index.php?customer=home\'>Dashboard</a> link.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_account\'>Edit My Account</a> menu allows you to change your personal info and account data.</p>\n<p> <b>&#8226;</b> <a href=\'index.php?customer=my_bookings\'>My Bookings</a> contains information about your orders.</p>\n<p><br /></p>");
INSERT INTO phpn_vocabulary VALUES("4796","ID","_ADMINISTRATOR_ONLY","Administrator Only");
INSERT INTO phpn_vocabulary VALUES("4797","ID","_ADMINS","Admins");
INSERT INTO phpn_vocabulary VALUES("4496","en","_JUST_BOOKED","just booked");
INSERT INTO phpn_vocabulary VALUES("4499","en","_AGO","ago");
INSERT INTO phpn_vocabulary VALUES("4795","ID","_ADMIN","Admin");
INSERT INTO phpn_vocabulary VALUES("4502","en","_ROOM","room");
INSERT INTO phpn_vocabulary VALUES("5986","ID","_PAY_WITH_BALANCE","Pay with Balance");
INSERT INTO phpn_vocabulary VALUES("4505","en","_BOOKED","booked");
INSERT INTO phpn_vocabulary VALUES("4794","ID","_ADD_TO_MENU","Add To Menu");
INSERT INTO phpn_vocabulary VALUES("4508","en","_VEHICLE_CATEGORY_DELETE_ALERT","You cannot delete this category, because it still includes some vehicles! Please remove all vehicles from this category before deleting operation.");
INSERT INTO phpn_vocabulary VALUES("5982","ID","_MS_ALLOW_AGENCIES","Allow special type of customers - agencies");
INSERT INTO phpn_vocabulary VALUES("5983","ID","_AGENCY_DETAILS","Agency Details");
INSERT INTO phpn_vocabulary VALUES("5984","ID","_BALANCE","Balance");
INSERT INTO phpn_vocabulary VALUES("5985","ID","_ACCOUNT_BALANCE","Account Balance");
INSERT INTO phpn_vocabulary VALUES("4790","ID","_ADD_DEFAULT_PERIODS","Add Default Periods");
INSERT INTO phpn_vocabulary VALUES("4791","ID","_ADD_NEW","Add New");
INSERT INTO phpn_vocabulary VALUES("4792","ID","_ADD_NEW_MENU","Add New Menu");
INSERT INTO phpn_vocabulary VALUES("4793","ID","_ADD_TO_CART","Add to Cart");
INSERT INTO phpn_vocabulary VALUES("4511","en","_LEGEND_CAR_REFUNDED","Order has been refunded and vehicle is available again in search");
INSERT INTO phpn_vocabulary VALUES("5980","ID","_CHECK_VILLAS","Check Villas");
INSERT INTO phpn_vocabulary VALUES("5981","ID","_AGENCIES","Agencies");
INSERT INTO phpn_vocabulary VALUES("4789","ID","_ADDRESS_EMPTY_ALERT","Address cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4514","en","_LEGEND_CAR_CANCELED","Order is canceled by admin and vehicle is available again in search");
INSERT INTO phpn_vocabulary VALUES("5978","ID","_ROOM_LEFT","room left");
INSERT INTO phpn_vocabulary VALUES("5979","ID","_CHECK_HOTELS","Check Hotels");
INSERT INTO phpn_vocabulary VALUES("4787","ID","_ADDRESS","Address");
INSERT INTO phpn_vocabulary VALUES("4788","ID","_ADDRESS_2","Address (line 2)");
INSERT INTO phpn_vocabulary VALUES("4517","en","_POSITIVE_COMMENTS","Positive Comments");
INSERT INTO phpn_vocabulary VALUES("5977","ID","_FROM_PER_NIGHT","from per night");
INSERT INTO phpn_vocabulary VALUES("4520","en","_NEGATIVE_COMMENTS","Negative Comments");
INSERT INTO phpn_vocabulary VALUES("4523","en","_WRONG_EMPTY_FIELD","Fields picking up or/and dropping off are empty. Please select them.");
INSERT INTO phpn_vocabulary VALUES("5976","ID","_TODAY_CHECKOUT","Today\'s Check-out");
INSERT INTO phpn_vocabulary VALUES("4526","en","_REVIEW","review");
INSERT INTO phpn_vocabulary VALUES("5975","ID","_TODAY_CHECKIN","Today\'s Check-in");
INSERT INTO phpn_vocabulary VALUES("4786","ID","_ADDITIONAL_PAYMENT_TOOLTIP","To apply an additional payment or admin discount enter into this field an appropriate value (positive or negative).");
INSERT INTO phpn_vocabulary VALUES("4529","en","_CLEANLINESS","Cleanliness");
INSERT INTO phpn_vocabulary VALUES("5974","ID","_INFO","Info");
INSERT INTO phpn_vocabulary VALUES("4532","en","_ROOM_COMFORT","Room Comfort");
INSERT INTO phpn_vocabulary VALUES("4785","ID","_ADDITIONAL_PAYMENT","Additional Payment");
INSERT INTO phpn_vocabulary VALUES("4535","en","_SERVICE_AND_STAFF","Service & Staff");
INSERT INTO phpn_vocabulary VALUES("4538","en","_SLEEP_QUALITY","Sleep Quality");
INSERT INTO phpn_vocabulary VALUES("5973","ID","_ALERT_SAME_HOTEL_ROOMS","You may add to this reservation cart only rooms from the same hotel!");
INSERT INTO phpn_vocabulary VALUES("4784","ID","_ADDITIONAL_MODULES","Additional Modules");
INSERT INTO phpn_vocabulary VALUES("4541","en","_VALUE_FOR_PRICE","Value for Price");
INSERT INTO phpn_vocabulary VALUES("5972","ID","_LAST_MINUTE","Last Minute");
INSERT INTO phpn_vocabulary VALUES("4783","ID","_ADDITIONAL_INFO","Additional Info");
INSERT INTO phpn_vocabulary VALUES("4544","en","_WRONG_EMPTY_FIELDS_PICK_UP_AND_DROP_OFF","Fields picking up and dropping off may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4547","en","_WRONG_EMPTY_FIELD_PICK_UP","Fields picking up may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5971","ID","_NEED_ASSISTANCE_TEXT","Our team is 24/7 at your service to help you with your booking issues or answer any related questions");
INSERT INTO phpn_vocabulary VALUES("4781","ID","_ADD","Add");
INSERT INTO phpn_vocabulary VALUES("4782","ID","_ADDING_OPERATION_COMPLETED","The adding operation completed successfully!");
INSERT INTO phpn_vocabulary VALUES("4550","en","_WRONG_EMPTY_FIELD_DROP_OFF","Fields dropping off may not be empty, please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5970","ID","_NEED_ASSISTANCE","Need Assistance?");
INSERT INTO phpn_vocabulary VALUES("4780","ID","_ACTIVE","Active");
INSERT INTO phpn_vocabulary VALUES("4553","en","_CAR_AGENCY_PAYMENT_GATEWAYS","Agency Payment Gateways");
INSERT INTO phpn_vocabulary VALUES("5969","ID","_CUSTOMER_SUPPORT","Customer support");
INSERT INTO phpn_vocabulary VALUES("4556","en","_TITLE","Title");
INSERT INTO phpn_vocabulary VALUES("5968","ID","_PREFERENCES","Preferences");
INSERT INTO phpn_vocabulary VALUES("4559","en","_EVALUATION","Evaluation");
INSERT INTO phpn_vocabulary VALUES("5967","ID","_MAPS","Maps");
INSERT INTO phpn_vocabulary VALUES("4562","en","_NOT_RECOMMENDED","Not Recommended");
INSERT INTO phpn_vocabulary VALUES("5966","ID","_SUMMARY","Summary");
INSERT INTO phpn_vocabulary VALUES("4779","ID","_ACTIVATION_EMAIL_WAS_SENT","An email has been sent to _EMAIL_ with an activation key. Please check your mail to complete registration.");
INSERT INTO phpn_vocabulary VALUES("4565","en","_RECOMMENDED_TO_EVERYONE","Recommended to Everyone");
INSERT INTO phpn_vocabulary VALUES("5965","ID","_NOT_GOOD","Not Good");
INSERT INTO phpn_vocabulary VALUES("4568","en","_PERCENT_OF_GUESTS_RECOMMEND","_PERCENT_ of guests recommend");
INSERT INTO phpn_vocabulary VALUES("5964","ID","_VERY_GOOD","Very Good!");
INSERT INTO phpn_vocabulary VALUES("4571","en","_NEUTRAL","Neutral");
INSERT INTO phpn_vocabulary VALUES("5963","ID","_WONDERFUL","Wonderful!");
INSERT INTO phpn_vocabulary VALUES("4778","ID","_ACTIVATION_EMAIL_ALREADY_SENT","The activation email has been already sent to your email. Please try again later.");
INSERT INTO phpn_vocabulary VALUES("4574","en","_RATINGS_BASED_ON_REVIEWS","Ratings based on _REVIEWS_ Verified Reviews");
INSERT INTO phpn_vocabulary VALUES("5962","ID","_TARGET_HOTEL","Target Hotel");
INSERT INTO phpn_vocabulary VALUES("4776","ID","_ACTIONS_WORD","Action");
INSERT INTO phpn_vocabulary VALUES("4777","ID","_ACTION_REQUIRED","ACTION REQUIRED");
INSERT INTO phpn_vocabulary VALUES("4577","en","_X_OUT_OF_Y","_X_ out of _Y_");
INSERT INTO phpn_vocabulary VALUES("5961","ID","_LETS_SOCIALIZE","Let\'s socialize");
INSERT INTO phpn_vocabulary VALUES("4775","ID","_ACTIONS","Action");
INSERT INTO phpn_vocabulary VALUES("4580","en","_AVERAGE_RATINGS","Average Ratings");
INSERT INTO phpn_vocabulary VALUES("4583","en","_BY","by");
INSERT INTO phpn_vocabulary VALUES("5960","ID","_ALL_RIGHTS_RESERVED","All rights reserved");
INSERT INTO phpn_vocabulary VALUES("4586","en","_ADD_REVIEW","Add Review");
INSERT INTO phpn_vocabulary VALUES("5959","ID","_COPYRIGHT","Copyright");
INSERT INTO phpn_vocabulary VALUES("4589","en","_PROPERTY_NOT_REVIEWED_YET","Not yet reviewed by any member... You can be the FIRST one to write a review for _PROPERTY_NAME_.");
INSERT INTO phpn_vocabulary VALUES("5957","ID","_EARLY_BOOKING","Early Booking");
INSERT INTO phpn_vocabulary VALUES("5958","ID","_HOT_DEALS","Hot Deals");
INSERT INTO phpn_vocabulary VALUES("4774","ID","_ACCOUT_CREATED_CONF_MSG","Already confirmed your registration? Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("4592","en","_GUEST_RATINGS","guest ratings");
INSERT INTO phpn_vocabulary VALUES("5956","ID","_TODAY_TOP_DEALS_TEXT","Start your search with a look at the best rates on our site.");
INSERT INTO phpn_vocabulary VALUES("4595","en","_REVIEWS_SETTINGS","Reviews Settings");
INSERT INTO phpn_vocabulary VALUES("4773","ID","_ACCOUT_CREATED_CONF_LINK","Already confirmed your registration? Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("4598","en","_MD_TESTIMONIALS","The Testimonials Module allows the administrator of the site to add/edit customer testimonials, manage them and show on the Hotel Site frontend.");
INSERT INTO phpn_vocabulary VALUES("5954","ID","_TODAY_TOP_DEALS","Today\'s Top Deals");
INSERT INTO phpn_vocabulary VALUES("5955","ID","_TODAY_TOP_DEALS_WITH_BR","Today\'s Top<br/>Deals");
INSERT INTO phpn_vocabulary VALUES("4772","ID","_ACCOUNT_WAS_UPDATED","Your account has been successfully updated!");
INSERT INTO phpn_vocabulary VALUES("4601","en","_MS_TESTIMONIALS_KEY","The keyword that will be replaced with a list of customer testimonials (copy and paste it into the page)");
INSERT INTO phpn_vocabulary VALUES("5952","ID","_FEATURED_OFFERS_WITH_BR","Featured<br>Offers");
INSERT INTO phpn_vocabulary VALUES("5953","ID","_FEATURED_OFFERS_TEXT","Start your search with a look at the best rates on our site.");
INSERT INTO phpn_vocabulary VALUES("4770","ID","_ACCOUNT_WAS_CREATED","Your account has been created");
INSERT INTO phpn_vocabulary VALUES("4771","ID","_ACCOUNT_WAS_DELETED","Your account has been successfully removed! In seconds, you will be automatically redirected to the homepage.");
INSERT INTO phpn_vocabulary VALUES("4604","en","_TESTIMONIALS","Testimonials");
INSERT INTO phpn_vocabulary VALUES("5951","ID","_FEATURED_OFFERS","Featured Offers");
INSERT INTO phpn_vocabulary VALUES("4769","ID","_ACCOUNT_TYPE","Account type");
INSERT INTO phpn_vocabulary VALUES("4607","en","_TESTIMONIALS_MANAGEMENT","Testimonials Management");
INSERT INTO phpn_vocabulary VALUES("5950","ID","_HOTEL_FACILITIES","Hotel Facilities");
INSERT INTO phpn_vocabulary VALUES("4610","en","_TESTIMONIALS_SETTINGS","Testimonials Settings");
INSERT INTO phpn_vocabulary VALUES("5949","ID","_FACILITIES_MANAGEMENT","Facilities Management");
INSERT INTO phpn_vocabulary VALUES("4613","en","_MSN_TESTIMONIALS_KEY","Testimonials Key");
INSERT INTO phpn_vocabulary VALUES("5948","ID","_DETAILS","Details");
INSERT INTO phpn_vocabulary VALUES("4768","ID","_ACCOUNT_SUCCESSFULLY_RESET","You have successfully reset your account and username with temporary password have been sent to your email.");
INSERT INTO phpn_vocabulary VALUES("4616","en","_REVIEW_YOU_HAVE_REGISTERED","You have to be registered in to post your review.");
INSERT INTO phpn_vocabulary VALUES("5946","ID","_VILLA","Villa");
INSERT INTO phpn_vocabulary VALUES("5947","ID","_VILLAS","Villas");
INSERT INTO phpn_vocabulary VALUES("4767","ID","_ACCOUNT_DETAILS","Account Details");
INSERT INTO phpn_vocabulary VALUES("4619","en","_REVIEW_LINK_LOGIN","<a href=index.php?customer=login>Link</a> to login page.");
INSERT INTO phpn_vocabulary VALUES("5944","ID","_PROPERTY_TYPE","Property Type");
INSERT INTO phpn_vocabulary VALUES("5945","ID","_PROPERTY_TYPES","Property Types");
INSERT INTO phpn_vocabulary VALUES("4622","en","_MIN_MAX_NIGHTS_ALERT","The minimum and maximum allowed stay for the period of time from _FROM_ to _TO_ is _NIGHTS_ nights per booking (package _PACKAGE_NAME_). Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5941","ID","_YOU_ARE_LOGGED_AS","You are logged in as");
INSERT INTO phpn_vocabulary VALUES("5942","ID","_ZIPCODE_EMPTY_ALERT","Zip/Postal code cannot be empty! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5943","ID","_ZIP_CODE","Zip/Postal code");
INSERT INTO phpn_vocabulary VALUES("4766","ID","_ACCOUNT_CREATE_MSG","This registration process requires confirmation via email! <br />Please fill out the form below with correct information.");
INSERT INTO phpn_vocabulary VALUES("4625","en","_MY_REVIEWS","My Reviews");
INSERT INTO phpn_vocabulary VALUES("4628","en","_PHOTO","Photo");
INSERT INTO phpn_vocabulary VALUES("5940","ID","_YOUR_NAME","Your Name");
INSERT INTO phpn_vocabulary VALUES("4631","en","_MSN_SEARCH_AVAILABILITY_PERIOD","Maximum Allowed Search Period");
INSERT INTO phpn_vocabulary VALUES("5938","ID","_YES","Yes");
INSERT INTO phpn_vocabulary VALUES("5939","ID","_YOUR_EMAIL","Your Email");
INSERT INTO phpn_vocabulary VALUES("4634","en","_MS_SEARCH_AVAILABILITY_PERIOD","Specifies the maximum allowed period of time for search in years");
INSERT INTO phpn_vocabulary VALUES("5936","ID","_WYSIWYG_EDITOR","WYSIWYG Editor");
INSERT INTO phpn_vocabulary VALUES("5937","ID","_YEAR","Year");
INSERT INTO phpn_vocabulary VALUES("4765","ID","_ACCOUNT_CREATED_NON_CONFIRM_MSG","Your account has been successfully created! For your convenience in a few minutes you will receive an email, containing the details of your registration (no confirmation required). <br><br>You may log into your account now.");
INSERT INTO phpn_vocabulary VALUES("4637","en","_MAXIMUM_PERIOD_ALERT","The maximum allowed period for search is _DAYS_ days. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4764","ID","_ACCOUNT_CREATED_NON_CONFIRM_LINK","Click <a href=index.php?customer=login>here</a> to proceed.");
INSERT INTO phpn_vocabulary VALUES("4640","en","_WISHLIST","Wishlist");
INSERT INTO phpn_vocabulary VALUES("5935","ID","_WRONG_PARAMETER_PASSED","Wrong parameters passed - cannot complete operation!");
INSERT INTO phpn_vocabulary VALUES("4643","en","_ADD_TO_WISHLIST","Add to wishlist");
INSERT INTO phpn_vocabulary VALUES("5934","ID","_WRONG_LOGIN","Wrong username or password!");
INSERT INTO phpn_vocabulary VALUES("4646","en","_REMOVE_FROM_WISHLIST","Remove from wishlist");
INSERT INTO phpn_vocabulary VALUES("4649","en","_DISCOUNTS","Discounts");
INSERT INTO phpn_vocabulary VALUES("4652","en","_DISCOUNT_TYPE","Discount Type");
INSERT INTO phpn_vocabulary VALUES("4655","en","_NIGHT_3","3rd night");
INSERT INTO phpn_vocabulary VALUES("5933","ID","_WRONG_FILE_TYPE","Uploaded file is not a valid PHP vocabulary file! Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("4658","en","_NIGHT_4","4th night");
INSERT INTO phpn_vocabulary VALUES("4661","en","_NIGHT_5","5th night");
INSERT INTO phpn_vocabulary VALUES("4664","en","_FIXED_PRICE","Fixed Price");
INSERT INTO phpn_vocabulary VALUES("5932","ID","_WRONG_COUPON_CODE","This coupon code is invalid or has expired!");
INSERT INTO phpn_vocabulary VALUES("4667","en","_PERCENTAGE","Percentage");
INSERT INTO phpn_vocabulary VALUES("4763","ID","_ACCOUNT_CREATED_MSG","Your account has been successfully created. <b>You will receive now a confirmation email</b>, containing the details of your account (it may take a few minutes). <br /><br />After completing the confirmation you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("4670","en","_NOT_LOGGED_ALERT","You must be logged in to perform this operation!");
INSERT INTO phpn_vocabulary VALUES("5931","ID","_WRONG_CONFIRMATION_CODE","Wrong confirmation code or your registration is already confirmed!");
INSERT INTO phpn_vocabulary VALUES("4673","en","_ADDED_TO_WISHLIST","Item successfully added to wishlist!");
INSERT INTO phpn_vocabulary VALUES("4676","en","_REMOVED_FROM_WISHLIST","Item removed from wishlist!");
INSERT INTO phpn_vocabulary VALUES("5930","ID","_WRONG_CODE_ALERT","Sorry, the code you have entered is invalid! Please try again.");
INSERT INTO phpn_vocabulary VALUES("4679","en","_EX_HOTEL_OR_LOCATION","e.g. hotel, landmark or location");
INSERT INTO phpn_vocabulary VALUES("5929","ID","_WRONG_CHECKOUT_DATE_ALERT","Wrong date selected! Please choose a valid check-out date.");
INSERT INTO phpn_vocabulary VALUES("4682","en","_MASS_MAIL_LOG","Mass Mail Log");
INSERT INTO phpn_vocabulary VALUES("4762","ID","_ACCOUNT_CREATED_CONF_MSG","Your account has been successfully created. <b>You will receive now an email</b>, containing the details of your account (it may take a few minutes).<br><br>After approval by an administrator, you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("4685","en","_ALL_REVIEWS","All Reviews");
INSERT INTO phpn_vocabulary VALUES("4688","en","_MINIMUM_PERIOD_ALERT","The minimum allowed period for search is _DAYS_ days. Please re-enter.");
INSERT INTO phpn_vocabulary VALUES("5928","ID","_WRONG_BOOKING_NUMBER","The booking number you\'ve entered was not found! Please enter a valid booking number.");
INSERT INTO phpn_vocabulary VALUES("4691","en","_SELECT_DESTINATION_OR_HOTEL","Destination / Hotel Name");
INSERT INTO phpn_vocabulary VALUES("5927","ID","_WITHOUT_ACCOUNT","without account");
INSERT INTO phpn_vocabulary VALUES("4694","en","_YOU_NOT_REVIEW_THIS_HOTEL","Sorry, but you can not leave a review for this hotel, you\'ve never been there");
INSERT INTO phpn_vocabulary VALUES("4697","en","_LONG_TERM_STAY_DISCOUNT","Long-Term Stay Discount");
INSERT INTO phpn_vocabulary VALUES("5926","ID","_WIDGET_INTEGRATION_MESSAGE_HINT","<b>Hint</b>: To list all available hotels, leave hsJsKey value empty or enter hotel IDs separated by commas, for ex.: var hsHotelIDs = \"2,9,12\";");
INSERT INTO phpn_vocabulary VALUES("4761","ID","_ACCOUNT_CREATED_CONF_BY_EMAIL_MSG","Your account has been successfully created! In a few minutes you should receive an email, containing the details of your registration. <br><br> Complete this registration, using the confirmation code that was sent to the provided email address, and you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("4700","en","_YOU_NOT_DOUBLE_REVIEW","You cannot double-post a review for a hotel");
INSERT INTO phpn_vocabulary VALUES("4703","en","_LONG_TERM_IF_YOU_BOOK_ROOM","If you book this room for long stay you get");
INSERT INTO phpn_vocabulary VALUES("5925","ID","_WIDGET_INTEGRATION_MESSAGE","You may integrate Hotel Site search engine with another existing web site.");
INSERT INTO phpn_vocabulary VALUES("4706","en","_DISCOUNT_FOR_3RD_NIGHT","Discount for 3rd night");
INSERT INTO phpn_vocabulary VALUES("5924","ID","_WHOLE_SITE","Whole site");
INSERT INTO phpn_vocabulary VALUES("4709","en","_DISCOUNT_FOR_4TH_NIGHT","Discount for 4th night");
INSERT INTO phpn_vocabulary VALUES("5923","ID","_WHAT_IS_CVV","What is CVV");
INSERT INTO phpn_vocabulary VALUES("4760","ID","_ACCOUNT_CREATED_CONF_BY_ADMIN_MSG","Your account has been successfully created! In a few minutes you should receive an email, containing the details of your account. <br><br> After approval your registration by administrator, you will be able to log into your account.");
INSERT INTO phpn_vocabulary VALUES("4712","en","_DISCOUNT_FOR_5TH_OR_MORE_NIGHTS","Discount for 5th+ nights");
INSERT INTO phpn_vocabulary VALUES("4715","en","_COORDINATES_CENTER_POINT","Coordinates of Center Point");
INSERT INTO phpn_vocabulary VALUES("4759","ID","_ACCOUNT_ALREADY_RESET","Your account is already reset! Please check your email inbox for more information.");
INSERT INTO phpn_vocabulary VALUES("4718","en","_NAME_CENTER_POINT","Name of Center Point");
INSERT INTO phpn_vocabulary VALUES("4758","ID","_ACCOUNTS_MANAGEMENT","Accounts");
INSERT INTO phpn_vocabulary VALUES("4721","en","_DISTANCE_TO_CENTER_POINT","Distance to Center Point");
INSERT INTO phpn_vocabulary VALUES("4757","ID","_ACCOUNTS","Accounts");
INSERT INTO phpn_vocabulary VALUES("4724","en","_GET_DISTANCE","Get Distance");
INSERT INTO phpn_vocabulary VALUES("4727","en","_TOTAL_FUNDS","Total Funds");
INSERT INTO phpn_vocabulary VALUES("4756","ID","_ACCESSIBLE_BY","Accessible By");
INSERT INTO phpn_vocabulary VALUES("4730","en","_TOTAL_PAYMENTS","Total Payments");
INSERT INTO phpn_vocabulary VALUES("4733","en","_STARTING","Starting");
INSERT INTO phpn_vocabulary VALUES("4755","ID","_ACCESS","Access");
INSERT INTO phpn_vocabulary VALUES("4736","en","_FOUND_VILLAS","Found Villas");
INSERT INTO phpn_vocabulary VALUES("4754","ID","_ABOUT_US","About Us");
INSERT INTO phpn_vocabulary VALUES("4739","en","_FOUND_PROPERTIES","Found Properties");
INSERT INTO phpn_vocabulary VALUES("4742","en","_METERS_SHORTENED","m.");
INSERT INTO phpn_vocabulary VALUES("4753","ID","_ABBREVIATION","Abbreviation");
INSERT INTO phpn_vocabulary VALUES("4745","en","_KILOMETERS_SHORTENED","km.");
INSERT INTO phpn_vocabulary VALUES("4752","ID","_2CO_ORDER","2CO Order");
INSERT INTO phpn_vocabulary VALUES("4748","en","_DISTANCE_OF_HOTEL_FROM_CENTER_POINT","Distance of the hotel form the {name_center_point} is {distance_center_point}");
INSERT INTO phpn_vocabulary VALUES("4751","ID","_2CO_NOTICE","2CheckOut.com Inc. (Ohio, USA) is an authorized retailer for goods and services.");



DROP TABLE IF EXISTS phpn_wishlist;

CREATE TABLE `phpn_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - hotel, 2 - room, 3 - car',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `item_id` (`item_id`),
  KEY `item_type` (`item_type`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phpn_wishlist VALUES("1","1","1","1","2016-04-25 00:00:00");
INSERT INTO phpn_wishlist VALUES("2","5","11","2","2017-02-16 19:42:41");
INSERT INTO phpn_wishlist VALUES("3","5","10","2","2017-02-16 22:07:56");



